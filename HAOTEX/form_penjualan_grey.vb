﻿Imports MySql.Data.MySqlClient

Public Class form_penjualan_grey

    Private Sub form_penjualan_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call awal()
    End Sub

    Private Sub awal()
        Call isidtpawal()
        dtp_hari_ini.Text = Today
        dtp_akhir.Text = Today
        Call isidgv()
    End Sub

    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(-3)
        dtp_awal.Text = tanggal
    End Sub

    Private Sub headertable()
        dgv1.Columns(0).HeaderText = "ID Grey"
        dgv1.Columns(1).HeaderText = "ID Jual"
        dgv1.Columns(2).HeaderText = "Tanggal"
        dgv1.Columns(3).HeaderText = "Jatuh Tempo"
        dgv1.Columns(4).HeaderText = "Surat Jalan"
        dgv1.Columns(5).HeaderText = "Jenis Kain"
        dgv1.Columns(6).HeaderText = "Customer"
        dgv1.Columns(7).HeaderText = "Harga"
        dgv1.Columns(8).HeaderText = "Qty"
        dgv1.Columns(9).HeaderText = "Total Harga"
        dgv1.Columns(10).HeaderText = "Gudang"
        dgv1.Columns(11).HeaderText = "Stok"
        dgv1.Columns(12).HeaderText = "Asal SJ"
        dgv1.Columns(13).HeaderText = "Keterangan"
        dgv1.Columns(0).Width = 70
        dgv1.Columns(1).Width = 70
        dgv1.Columns(2).Width = 80
        dgv1.Columns(3).Width = 100
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 150
        dgv1.Columns(6).Width = 120
        dgv1.Columns(7).Width = 80
        dgv1.Columns(8).Width = 80
        dgv1.Columns(9).Width = 150
        dgv1.Columns(10).Width = 120
        dgv1.Columns(11).Width = 80
        dgv1.Columns(12).Width = 100
        dgv1.Columns(13).Width = 200
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(9).DefaultCellStyle.Format = "C"
    End Sub

    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Id_Grey,Id_Jual,Tanggal,Jatuh_Tempo,SJ,Jenis_Kain,Customer,Harga,Qty,Jumlah,Gudang,Stok,Asal_SJ,Keterangan FROM tbpenjualangrey WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpenjualangrey")
                            dgv1.DataSource = dsx.Tables("tbpenjualangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_awal.Text = dtp_akhir.Text
        End If
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_customer.Text = "< Customer >"
        Call isidgv()
    End Sub

    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_customer.Text = "< Customer >"
        Call isidgv()
    End Sub

    Private Sub isidgv_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Id_Jual,Tanggal,Jatuh_Tempo,SJ,Jenis_Kain,Customer,Harga,Qty,Jumlah,Gudang,Stok,Asal_SJ,Keterangan FROM tbpenjualangrey WHERE Customer like '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpenjualangrey")
                        dgv1.DataSource = dsx.Tables("tbpenjualangrey")
                        Call headertable()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_jeniskain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Id_Jual,Tanggal,Jatuh_Tempo,SJ,Jenis_Kain,Customer,Harga,Qty,Jumlah,Gudang,Stok,Asal_SJ,Keterangan FROM tbpenjualangrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpenjualangrey")
                        dgv1.DataSource = dsx.Tables("tbpenjualangrey")
                        Call headertable()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_customer_jeniskain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Id_Jual,Tanggal,Jatuh_Tempo,SJ,Jenis_Kain,Customer,Harga,Qty,Jumlah,Gudang,Stok,Asal_SJ,Keterangan FROM tbpenjualangrey WHERE Customer like '%" & txt_cari_customer.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpenjualangrey")
                        dgv1.DataSource = dsx.Tables("tbpenjualangrey")
                        Call headertable()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub txt_cari_customer_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_customer.GotFocus
        If txt_cari_customer.Text = "< Customer >" Then
            txt_cari_customer.Text = ""
        End If
    End Sub

    Private Sub txt_cari_customer_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_customer.LostFocus
        If txt_cari_customer.Text = "" Then
            txt_cari_customer.Text = "< Customer >"
        End If
    End Sub

    Private Sub txt_cari_customer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_customer.TextChanged
        Try
            If txt_cari_customer.Text = "< Customer >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_customer.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_customer.Text = "< Customer >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf txt_cari_customer.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_customer.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_customer()
            ElseIf Not txt_cari_customer.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_customer_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub

    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub

    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_customer.Text = "< Customer >" Then

            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "< Customer >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_customer.Text = "" Then
                Call isidgv_customer()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_customer()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "< Customer >" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_customer_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        Call awal()
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_customer.Text = "< Customer >"
        dgv1.Focus()
    End Sub

    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_penjualan_grey.MdiParent = form_menu_utama
        form_input_penjualan_grey.Show()
        form_input_penjualan_grey.Label1.Text = "Input Penjualan Grey Baru"
        form_input_penjualan_grey.Focus()
    End Sub

    Private Sub simpan_tbhistoripenjualangrey()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbhistoripenjualangrey (Id_Grey,Id_Jual,Tanggal,Jatuh_Tempo,SJ,Jenis_Kain,Customer,Harga,QTY,Jumlah,Gudang,Stok,Asal_SJ,Keterangan,Event,Tanggal_Histori,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    form_input_penjualan_grey.dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    form_input_penjualan_grey.dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    dtp_hari_ini.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (form_input_penjualan_grey.txt_id_grey.Text))
                    .Parameters.AddWithValue("@1", (form_input_penjualan_grey.txt_id_jual.Text))
                    .Parameters.AddWithValue("@2", (form_input_penjualan_grey.dtp_tanggal.Text))
                    .Parameters.AddWithValue("@3", (form_input_penjualan_grey.dtp_jatuh_tempo.Text))
                    .Parameters.AddWithValue("@4", (form_input_penjualan_grey.txt_surat_jalan.Text))
                    .Parameters.AddWithValue("@5", (form_input_penjualan_grey.txt_jenis_kain.Text))
                    .Parameters.AddWithValue("@6", (form_input_penjualan_grey.txt_customer.Text))
                    .Parameters.AddWithValue("@7", (form_input_penjualan_grey.txt_harga.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@8", (form_input_penjualan_grey.txt_qty.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@9", (form_input_penjualan_grey.txt_jumlah.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@10", (form_input_penjualan_grey.txt_penyimpanan.Text))
                    .Parameters.AddWithValue("@11", (form_input_penjualan_grey.cb_stok.Text))
                    .Parameters.AddWithValue("@12", (form_input_penjualan_grey.txt_asal_sj.Text))
                    .Parameters.AddWithValue("@13", (form_input_penjualan_grey.txt_keterangan.Text))
                    .Parameters.AddWithValue("@14", ("HAPUS"))
                    .Parameters.AddWithValue("@15", (dtp_hari_ini.Text))
                    .Parameters.AddWithValue("@16", (""))
                    .Parameters.AddWithValue("@17", (""))
                    .Parameters.AddWithValue("@18", (""))
                    .Parameters.AddWithValue("@19", (0))
                    .Parameters.AddWithValue("@20", (0))
                    .ExecuteNonQuery()
                End With
                form_input_penjualan_grey.dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                form_input_penjualan_grey.dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                dtp_hari_ini.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub

    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Diubah")
        Else
            Try
                form_input_penjualan_grey.MdiParent = form_menu_utama
                form_input_penjualan_grey.Show()
                form_input_penjualan_grey.Label1.Text = "Ubah Penjualan Grey"
                form_input_penjualan_grey.btn_simpan_baru.Visible = False
                form_input_penjualan_grey.txt_penyimpanan.ReadOnly = True
                form_input_penjualan_grey.cb_stok.Enabled = False
                form_input_penjualan_grey.txt_jenis_kain.ReadOnly = True
                form_input_penjualan_grey.txt_customer.ReadOnly = True
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_penjualan_grey.txt_id_grey.Text = .Cells(0).Value.ToString
                    form_input_penjualan_grey.txt_id_jual.Text = .Cells(1).Value.ToString
                    form_input_penjualan_grey.dtp_tanggal.Text = .Cells(2).Value.ToString
                    form_input_penjualan_grey.dtp_jatuh_tempo.Text = .Cells(3).Value.ToString
                    form_input_penjualan_grey.txt_surat_jalan.Text = .Cells(4).Value.ToString
                    form_input_penjualan_grey.txt_jenis_kain.Text = .Cells(5).Value.ToString
                    form_input_penjualan_grey.txt_customer.Text = .Cells(6).Value.ToString
                    form_input_penjualan_grey.txt_harga.Text = .Cells(7).Value.ToString
                    form_input_penjualan_grey.txt_qty.Text = .Cells(8).Value.ToString
                    form_input_penjualan_grey.txt_qty_awal.Text = .Cells(8).Value.ToString
                    form_input_penjualan_grey.txt_jumlah.Text = .Cells(9).Value.ToString
                    form_input_penjualan_grey.txt_jumlah_awal.Text = .Cells(9).Value.ToString
                    form_input_penjualan_grey.txt_penyimpanan.Text = .Cells(10).Value.ToString
                    form_input_penjualan_grey.cb_stok.Text = .Cells(11).Value.ToString
                    form_input_penjualan_grey.txt_asal_sj.Text = .Cells(12).Value.ToString
                    form_input_penjualan_grey.txt_keterangan.Text = .Cells(13).Value.ToString
                End With
                Call isi_stok()
                form_input_penjualan_grey.Focus()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub update_tbstoktitipangrey()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok FROM tbstoktitipangrey WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "' AND SJ='" & form_input_penjualan_grey.txt_asal_sj.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim s As Double
                        s = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstoktitipangrey SET Stok=@1 WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "' AND SJ='" & form_input_penjualan_grey.txt_asal_sj.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (s + Val(form_input_penjualan_grey.txt_qty.Text.Replace(",", "."))))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub update_tbstokprosesgrey()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "' AND SJ='" & form_input_penjualan_grey.txt_asal_sj.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim s As Double
                        s = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "' AND SJ='" & form_input_penjualan_grey.txt_asal_sj.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (s + Val(form_input_penjualan_grey.txt_qty.Text.Replace(",", "."))))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub update_tbstokexreturcelup()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok FROM tbstokexreturcelup WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "' AND SJ='" & form_input_penjualan_grey.txt_asal_sj.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim s As Double
                        s = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexreturcelup SET Stok=@1 WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "' AND SJ='" & form_input_penjualan_grey.txt_asal_sj.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (s + Val(form_input_penjualan_grey.txt_qty.Text.Replace(",", "."))))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub simpan_tbhistorititipangrey()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbhistoristoktitipangrey (Tanggal_Histori,Event,Keterangan,Id_Grey,SJ,Gudang,Jenis_Kain,Stok,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_hari_ini.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (dtp_hari_ini.Text))
                    .Parameters.AddWithValue("@1", ("TAMBAH"))
                    .Parameters.AddWithValue("@2", ("Hapus Jual Grey"))
                    .Parameters.AddWithValue("@3", (form_input_penjualan_grey.txt_id_grey.Text))
                    .Parameters.AddWithValue("@4", (form_input_penjualan_grey.txt_asal_sj.Text))
                    .Parameters.AddWithValue("@5", (form_input_penjualan_grey.txt_penyimpanan.Text))
                    .Parameters.AddWithValue("@6", (form_input_penjualan_grey.txt_jenis_kain.Text))
                    .Parameters.AddWithValue("@7", (form_input_penjualan_grey.txt_qty.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@8", (form_input_penjualan_grey.txt_harga.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@9", (""))
                    .Parameters.AddWithValue("@10", (""))
                    .Parameters.AddWithValue("@11", (0))
                    .ExecuteNonQuery()
                    dtp_hari_ini.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub Simpan_tbhistoristokprosesgrey()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbhistoristokprosesgrey (Tanggal_Histori,Event,Keterangan,id_Grey,SJ,Gudang,Jenis_Kain,Stok,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_hari_ini.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (dtp_hari_ini.Text))
                    .Parameters.AddWithValue("@1", ("TAMBAH"))
                    .Parameters.AddWithValue("@2", ("Hapus Jual Grey"))
                    .Parameters.AddWithValue("@3", (form_input_penjualan_grey.txt_id_grey.Text))
                    .Parameters.AddWithValue("@4", (form_input_penjualan_grey.txt_asal_sj.Text))
                    .Parameters.AddWithValue("@5", (form_input_penjualan_grey.txt_penyimpanan.Text))
                    .Parameters.AddWithValue("@6", (form_input_penjualan_grey.txt_jenis_kain.Text))
                    .Parameters.AddWithValue("@7", (form_input_penjualan_grey.txt_qty.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@8", (form_input_penjualan_grey.txt_harga.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@9", (""))
                    .Parameters.AddWithValue("@10", (""))
                    .Parameters.AddWithValue("@11", (0))
                    .ExecuteNonQuery()
                    dtp_hari_ini.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub Simpan_tbhistoristokexreturcelup()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbhistoristokexreturcelup (Tanggal_Histori,Event,Keterangan,id_Grey,SJ,Gudang,Jenis_Kain,Stok,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_hari_ini.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (dtp_hari_ini.Text))
                    .Parameters.AddWithValue("@1", ("TAMBAH"))
                    .Parameters.AddWithValue("@2", ("Hapus Jual Grey"))
                    .Parameters.AddWithValue("@3", (form_input_penjualan_grey.txt_id_grey.Text))
                    .Parameters.AddWithValue("@4", (form_input_penjualan_grey.txt_asal_sj.Text))
                    .Parameters.AddWithValue("@5", (form_input_penjualan_grey.txt_penyimpanan.Text))
                    .Parameters.AddWithValue("@6", (form_input_penjualan_grey.txt_jenis_kain.Text))
                    .Parameters.AddWithValue("@7", (form_input_penjualan_grey.txt_qty.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@8", (form_input_penjualan_grey.txt_harga.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@9", (""))
                    .Parameters.AddWithValue("@10", (""))
                    .Parameters.AddWithValue("@11", (0))
                    .ExecuteNonQuery()
                    dtp_hari_ini.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub simpan_tbhistoripiutang_hapusjualgrey()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbhistoripiutang (Tanggal,Event,Keterangan,Nama,Piutang,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_hari_ini.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (dtp_hari_ini.Text))
                    .Parameters.AddWithValue("@1", ("KURANG"))
                    .Parameters.AddWithValue("@2", ("Hapus Jual Grey"))
                    .Parameters.AddWithValue("@3", (form_input_penjualan_grey.txt_customer.Text))
                    .Parameters.AddWithValue("@4", (form_input_penjualan_grey.txt_jumlah.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@5", (""))
                    .Parameters.AddWithValue("@6", (""))
                    .Parameters.AddWithValue("@7", (0))
                    .ExecuteNonQuery()
                    dtp_hari_ini.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub isi_stok()
        If form_input_penjualan_grey.cb_stok.Text = "Titipan" Then
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Stok FROM tbstoktitipangrey WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using drx As MySqlDataReader = cmdx.ExecuteReader
                        drx.Read()
                        If drx.HasRows Then
                            Dim s As Double
                            s = drx.Item(0)
                            form_input_penjualan_grey.txt_stok_tersedia.Text = s + Val(form_input_penjualan_grey.txt_qty.Text.Replace(",", "."))
                        End If
                    End Using
                End Using
            End Using
        ElseIf form_input_penjualan_grey.cb_stok.Text = "Proses" Then
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using drx As MySqlDataReader = cmdx.ExecuteReader
                        drx.Read()
                        If drx.HasRows Then
                            Dim s As Double
                            s = drx.Item(0)
                            form_input_penjualan_grey.txt_stok_tersedia.Text = s + Val(form_input_penjualan_grey.txt_qty.Text.Replace(",", "."))
                        End If
                    End Using
                End Using
            End Using
        ElseIf form_input_penjualan_grey.cb_stok.Text = "Ex Retur Celup" Then
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Stok FROM tbstokexreturcelup WHERE Id_Grey='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Gudang='" & form_input_penjualan_grey.txt_penyimpanan.Text & "' AND Jenis_Kain='" & form_input_penjualan_grey.txt_jenis_kain.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using drx As MySqlDataReader = cmdx.ExecuteReader
                        drx.Read()
                        If drx.HasRows Then
                            Dim s As Double
                            s = drx.Item(0)
                            form_input_penjualan_grey.txt_stok_tersedia.Text = s + Val(form_input_penjualan_grey.txt_qty.Text.Replace(",", "."))
                        End If
                    End Using
                End Using
            End Using
        End If
    End Sub

    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dihapus")
        Else
            Try
                form_input_penjualan_grey.MdiParent = form_menu_utama
                form_input_penjualan_grey.Show()
                form_input_penjualan_grey.Label1.Text = "Hapus Penjualan Grey"
                form_input_penjualan_grey.btn_simpan_baru.Visible = False
                form_input_penjualan_grey.btn_simpan_tutup.Visible = False
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_penjualan_grey.txt_id_grey.Text = .Cells(0).Value.ToString
                    form_input_penjualan_grey.txt_id_jual.Text = .Cells(1).Value.ToString
                    form_input_penjualan_grey.dtp_tanggal.Text = .Cells(2).Value.ToString
                    form_input_penjualan_grey.dtp_jatuh_tempo.Text = .Cells(3).Value.ToString
                    form_input_penjualan_grey.txt_surat_jalan.Text = .Cells(4).Value.ToString
                    form_input_penjualan_grey.txt_jenis_kain.Text = .Cells(5).Value.ToString
                    form_input_penjualan_grey.txt_customer.Text = .Cells(6).Value.ToString
                    form_input_penjualan_grey.txt_harga.Text = .Cells(7).Value.ToString
                    form_input_penjualan_grey.txt_qty.Text = .Cells(8).Value.ToString
                    form_input_penjualan_grey.txt_jumlah.Text = .Cells(9).Value.ToString
                    form_input_penjualan_grey.txt_penyimpanan.Text = .Cells(10).Value.ToString
                    form_input_penjualan_grey.cb_stok.Text = .Cells(11).Value.ToString
                    form_input_penjualan_grey.txt_asal_sj.Text = .Cells(12).Value.ToString
                    form_input_penjualan_grey.txt_keterangan.Text = .Cells(13).Value.ToString
                End With
                Call isi_stok()
                form_input_penjualan_grey.Enabled = False
                form_input_penjualan_grey.Focus()

                If MsgBox("Yakin PENJUALAN GREY Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Penjualan Grey") = vbYes Then
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "DELETE FROM tbpenjualangrey WHERE Id_Grey ='" & form_input_penjualan_grey.txt_id_grey.Text & "' AND Id_Jual ='" & form_input_penjualan_grey.txt_id_jual.Text & "'"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            cmdy.ExecuteNonQuery()
                        End Using
                    End Using
                    Call simpan_tbhistoripenjualangrey()
                    If form_input_penjualan_grey.cb_stok.Text = "Titipan" Then
                        Call update_tbstoktitipangrey()
                        Call simpan_tbhistorititipangrey()
                    ElseIf form_input_penjualan_grey.cb_stok.Text = "Proses" Then
                        Call update_tbstokprosesgrey()
                        Call Simpan_tbhistoristokprosesgrey()
                    ElseIf form_input_penjualan_grey.cb_stok.Text = "Ex Retur Celup" Then
                        Call update_tbstokexreturcelup()
                        Call Simpan_tbhistoristokexreturcelup()
                    End If
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Sisa_Piutang FROM tbpiutang WHERE Nama='" & form_input_penjualan_grey.txt_customer.Text & "'"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using drx As MySqlDataReader = cmdx.ExecuteReader
                                drx.Read()
                                If drx.HasRows Then
                                    Dim s As Double
                                    s = drx.Item(0)
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "UPDATE tbpiutang SET Sisa_Piutang=@1 WHERE Nama='" & form_input_penjualan_grey.txt_customer.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            With cmdy
                                                .Parameters.Clear()
                                                .Parameters.AddWithValue("@1", (s - Val(form_input_penjualan_grey.txt_jumlah.Text.Replace(",", "."))))
                                                .ExecuteNonQuery()
                                            End With
                                        End Using
                                    End Using
                                End If
                            End Using
                        End Using
                    End Using
                    Call simpan_tbhistoripiutang_hapusjualgrey()
                    form_input_penjualan_grey.Close()
                    ts_perbarui.PerformClick()
                    MsgBox("PENJUALAN GREY berhasil di HAPUS")
                Else
                    form_input_penjualan_grey.Close()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub reportpenjualangrey()
        Dim dtreportkontrakgrey As New DataTable
        With dtreportkontrakgrey
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
            .Columns.Add("DataColumn13")
            .Columns.Add("DataColumn14")
            .Columns.Add("DataColumn15")
            .Columns.Add("DataColumn16")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            dtreportkontrakgrey.Rows.Add(row.Cells(0).Value, row.Cells(1).Value, row.Cells(2).FormattedValue, _
                                         row.Cells(3).FormattedValue, row.Cells(4).Value, row.Cells(5).Value, _
                                         row.Cells(6).Value, row.Cells(7).Value, row.Cells(8).Value, row.Cells(9).FormattedValue, _
                                         row.Cells(10).Value, row.Cells(11).Value, row.Cells(12).Value + " / " + row.Cells(13).Value, _
                                         dtp_awal.Text, dtp_akhir.Text, dtp_hari_ini.Text)
        Next

        form_report_penjualan_grey.ReportViewer1.LocalReport.DataSources.Item(0).Value = dtreportkontrakgrey
        form_report_penjualan_grey.ShowDialog()
        form_report_penjualan_grey.Dispose()
    End Sub

    Private Sub ts_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportpenjualangrey()
        End If
    End Sub
End Class