﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_report_po_packing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ds_haotex = New HAOTEX.ds_haotex()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.HeaderFooterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PO_PackingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.ds_haotex, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HeaderFooterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PO_PackingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ds_haotex
        '
        Me.ds_haotex.DataSetName = "ds_haotex"
        Me.ds_haotex.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "HeaderFooter"
        ReportDataSource1.Value = Me.HeaderFooterBindingSource
        ReportDataSource2.Name = "PO_Packing"
        ReportDataSource2.Value = Me.PO_PackingBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "HAOTEX.report_po_packing.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(284, 262)
        Me.ReportViewer1.TabIndex = 0
        '
        'HeaderFooterBindingSource
        '
        Me.HeaderFooterBindingSource.DataMember = "HeaderFooter"
        Me.HeaderFooterBindingSource.DataSource = Me.ds_haotex
        '
        'PO_PackingBindingSource
        '
        Me.PO_PackingBindingSource.DataMember = "PO_Packing"
        Me.PO_PackingBindingSource.DataSource = Me.ds_haotex
        '
        'form_report_po_packing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "form_report_po_packing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "REPORT PO PACKING"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.ds_haotex, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HeaderFooterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PO_PackingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents ds_haotex As HAOTEX.ds_haotex
    Friend WithEvents HeaderFooterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PO_PackingBindingSource As System.Windows.Forms.BindingSource
End Class
