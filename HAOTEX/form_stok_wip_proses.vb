﻿Imports MySql.Data.MySqlClient

Public Class form_stok_wip_proses

    Private Sub form_stok_proses_grey_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If txt_form.Text = "form_input_surat_jalan_proses_baris_1" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_2" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_3" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_4" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_5" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_6" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_7" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_8" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_9" Or _
            txt_form.Text = "form_input_surat_jalan_proses_baris_10" Then
            form_input_surat_jalan_proses.Focus()
            form_input_surat_jalan_proses.Label23.Focus()
        End If
    End Sub
    Private Sub form_stok_wip_proses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "No PO"
        dgv1.Columns(2).HeaderText = "Gudang"
        dgv1.Columns(3).HeaderText = "Jenis Kain"
        dgv1.Columns(4).HeaderText = "Warna"
        dgv1.Columns(5).HeaderText = "Resep"
        dgv1.Columns(6).HeaderText = "Stok"
        dgv1.Columns(7).HeaderText = "Satuan"
        dgv1.Columns(8).HeaderText = "Harga"
        dgv1.Columns(9).HeaderText = "ID Grey"
        dgv1.Columns(10).HeaderText = "ID Beli"
        dgv1.Columns(11).HeaderText = "ID PO Proses"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 80
        dgv1.Columns(2).Width = 140
        dgv1.Columns(3).Width = 140
        dgv1.Columns(4).Width = 90
        dgv1.Columns(5).Width = 90
        dgv1.Columns(6).Width = 100
        dgv1.Columns(7).Width = 70
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(6).DefaultCellStyle.Format = "N"
        dgv1.Columns(8).Visible = False
        dgv1.Columns(9).Visible = False
        dgv1.Columns(10).Visible = False
        dgv1.Columns(11).Visible = False
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Id_Grey,Id_Beli,Id_Po_Proses FROM tbstokwipproses WHERE NOT Stok=0 ORDER BY Gudang"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokwipproses")
                            dgv1.DataSource = dsx.Tables("tbstokwipproses")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.GotFocus
        If txt_cari_gudang.Text = "< Gudang >" Then
            txt_cari_gudang.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.LostFocus
        If txt_cari_gudang.Text = "" Then
            txt_cari_gudang.Text = "< Gudang >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub isidgv_gudang()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Id_Grey,Id_Beli,Id_Po_Proses FROM tbstokwipproses WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND NOT Stok=0 ORDER BY Gudang"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokwipproses")
                            dgv1.DataSource = dsx.Tables("tbstokwipproses")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_jeniskain()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Id_Grey,Id_Beli,Id_Po_Proses FROM tbstokwipproses WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokwipproses")
                            dgv1.DataSource = dsx.Tables("tbstokwipproses")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_gudang_jeniskain()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Id_Grey,Id_Beli,Id_Po_Proses FROM tbstokwipproses WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Gudang"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokwipproses")
                            dgv1.DataSource = dsx.Tables("tbstokwipproses")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.TextChanged
        Try
            If txt_cari_gudang.Text = "< Gudang >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_gudang.Text = "< Gudang >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_gudang.Text = "< Gudang >" Then

            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgv1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv1.MouseDoubleClick
        Try
            If txt_form.Text = "form_input_surat_jalan_proses_baris_1" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_1.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_1.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_1.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_1.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_1.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_1.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_1.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_1.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_1.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_1.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_1.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_1.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_1.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_1.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_1.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_1.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_1.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_2" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_2.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_2.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_2.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_2.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_2.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_2.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_2.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_2.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_2.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_2.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_2.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_2.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_2.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_2.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_2.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_2.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_3" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_3.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_3.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_3.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_3.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_3.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_3.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_3.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_3.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_3.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_3.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_3.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_3.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_3.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_3.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_3.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_3.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_3.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_4" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_4.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_4.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_4.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_4.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_4.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_4.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_4.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_4.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_4.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_4.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_4.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_4.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_4.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_4.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_4.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_4.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_4.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_5" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_5.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_5.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_5.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_5.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_5.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_5.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_5.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_5.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_5.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_5.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_5.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_5.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_5.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_5.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_5.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_5.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_5.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_6" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_6.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_6.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_6.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_6.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_6.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_6.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_6.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_6.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_6.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_6.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_6.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_6.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_6.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_6.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_6.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_6.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_6.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_7" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_7.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_7.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_7.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_7.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_7.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_7.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_7.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_7.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_7.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_7.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_7.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_7.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_7.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_7.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_7.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_7.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_7.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_8" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_8.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_8.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_8.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_8.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_8.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_8.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_8.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_8.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_8.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_8.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_8.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_8.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_8.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_8.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_8.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_8.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_8.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_9" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_9.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_9.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_9.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_9.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_9.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_9.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_9.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_9.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_9.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_9.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_9.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_9.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_9.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_9.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_9.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_9.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_9.Focus()
                End If
            ElseIf txt_form.Text = "form_input_surat_jalan_proses_baris_10" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                    form_input_surat_jalan_proses.Show()
                    form_input_surat_jalan_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_surat_jalan_proses.cb_satuan_10.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_asal_satuan_wip_10.Text = .Cells(7).Value.ToString
                        form_input_surat_jalan_proses.txt_id_grey_10.Text = .Cells(9).Value.ToString
                        form_input_surat_jalan_proses.txt_no_po_10.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_proses.txt_jenis_kain_10.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_proses.txt_warna_10.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_proses.txt_resep_10.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_proses.txt_qty_asal_10.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_meter_10.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_proses.txt_harga_10.Text = .Cells(8).Value.ToString
                        form_input_surat_jalan_proses.txt_id_beli_10.Text = .Cells(10).Value.ToString
                        form_input_surat_jalan_proses.txt_id_po_proses_10.Text = .Cells(11).Value.ToString
                    End With
                    Me.Close()
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & form_input_surat_jalan_proses.txt_id_beli_10.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_surat_jalan_proses.txt_sisa_qty_10.Text = drz.Item(0).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_10.Text = drz.Item(1).ToString
                                    form_input_surat_jalan_proses.txt_asal_satuan_rubah_10.Text = drz.Item(1).ToString
                                End If
                            End Using
                        End Using
                    End Using
                    form_input_surat_jalan_proses.txt_partai_10.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class