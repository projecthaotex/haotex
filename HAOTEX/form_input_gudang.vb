﻿Imports MySql.Data.MySqlClient

Public Class form_input_gudang

    Private Sub form_input_gudang_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If TxtForm.Text = "form_input_surat_jalan_proses" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_1" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_2" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_3" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_4" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_5" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_6" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_7" Or _
            TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_8" Then
            form_input_surat_jalan_proses.Focus()
            form_input_surat_jalan_proses.Label16.Focus()
        ElseIf TxtForm.Text = "form_input_po_packing" Then
            form_input_po_packing.Focus()
            form_input_po_packing.Label16.Focus()
        ElseIf TxtForm.Text = "form_input_po_proses" Then
            form_input_po_proses.Focus()
            form_input_po_proses.Label1.Focus()
        ElseIf TxtForm.Text = "form_input_hasil_packing" Then
            form_input_hasil_packing.Focus()
            form_input_hasil_packing.txt_sj_packing.Focus()
        ElseIf TxtForm.Text = "form_edit_surat_jalan_proses" Then
            form_edit_surat_jalan_proses.Focus()
            form_edit_surat_jalan_proses.Label1.Focus()
        ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_1" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_2" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_3" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_4" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_5" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_6" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_7" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_8" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_9" Or _
            TxtForm.Text = "form_input_pembelian_grey_baru_10" Then
            form_input_pembelian_grey_baru.Focus()
            form_input_pembelian_grey_baru.Label1.Focus()
        ElseIf TxtForm.Text = "form_input_pembelian_grey" Then
            form_input_pembelian_grey.Focus()
            form_input_pembelian_grey.Label1.Focus()
        End If
    End Sub

    Private Sub form_input_gudang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
        TxtCari.Text = ""
    End Sub

    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Nama_Gudang FROM tbgudang ORDER BY Nama_Gudang"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbgudang")
                            Dgv1.DataSource = dsx.Tables("tbgudang")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub cari_nama_gudang()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Nama_Gudang FROM tbgudang WHERE Nama_Gudang like '" & TxtCari.Text & "%' ORDER BY Nama_Gudang"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbgudang")
                            Dgv1.DataSource = dsx.Tables("tbgudang")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TxtCari_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCari.TextChanged
        Call cari_nama_gudang()
    End Sub

    Private Sub Dgv1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Dgv1.MouseClick
        Dim i As Integer
        i = Me.Dgv1.CurrentRow.Index
        With Dgv1.Rows.Item(i)
            TxtCari.Text = .Cells(0).Value.ToString
        End With
    End Sub

    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Try
            If TxtCari.Text = "" Then
                MsgBox("NAMA GUDANG belum diinput")
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Gudang from tbgudang WHERE Nama_Gudang ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("NAMA GUDANG belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama.MdiParent = form_menu_utama
                                    form_input_nama.Show()
                                    form_input_nama.txt_frm.Text = "form_input_gudang"
                                    form_input_nama.Label1.Text = "Input Gudang Baru"
                                    form_input_nama.Label2.Text = "Nama Gudang"
                                    form_input_nama.txt_nama.Text = TxtCari.Text
                                    form_input_nama.txt_nama.Focus()
                                    form_input_nama.Focus()
                                End If
                            Else
                                If TxtForm.Text = "form_input_po_proses" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_gudang.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_po_proses.txt_no_po.Focus()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_1" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_1.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_2" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_2.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_3" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_3.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_4" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_4.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_5" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_5.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_6" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_6.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_7" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_7.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_8" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_8.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_9" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_9.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_10" Then
                                    form_input_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_input_surat_jalan_proses.Show()
                                    form_input_surat_jalan_proses.Focus()
                                    form_input_surat_jalan_proses.txt_gudang_packing_10.Text = TxtCari.Text
                                    form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_gudang.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_hasil_packing" Then
                                    form_input_hasil_packing.MdiParent = form_menu_utama
                                    form_input_hasil_packing.Show()
                                    form_input_hasil_packing.Focus()
                                    form_input_hasil_packing.txt_gudang.Text = TxtCari.Text
                                    form_input_hasil_packing.txt_sj_packing.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_edit_surat_jalan_proses" Then
                                    form_edit_surat_jalan_proses.MdiParent = form_menu_utama
                                    form_edit_surat_jalan_proses.Show()
                                    form_edit_surat_jalan_proses.Focus()
                                    form_edit_surat_jalan_proses.txt_gudang_packing.Text = TxtCari.Text
                                    form_edit_surat_jalan_proses.txt_keterangan.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_1" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_1.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_1.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_2" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_2.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_2.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_3" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_3.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_3.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_4" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_4.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_4.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_5" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_5.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_5.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_6" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_6.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_6.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_7" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_7.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_7.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_8" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_gudang_8.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_keterangan_8.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey" Then
                                    form_input_pembelian_grey.MdiParent = form_menu_utama
                                    form_input_pembelian_grey.Show()
                                    form_input_pembelian_grey.Focus()
                                    form_input_pembelian_grey.txt_penyimpanan.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey.txt_keterangan.Focus()
                                End If
                            End If
                        End Using
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ts_keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_keluar.Click
        If TxtForm.Text = "form_input_pembelian_grey" Then
            form_input_pembelian_grey.txt_keterangan.Focus()
            form_input_pembelian_grey.Focus()
            Me.Close()
        Else
            Me.Close()
        End If
    End Sub

    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_nama.MdiParent = form_menu_utama
        form_input_nama.Show()
        form_input_nama.txt_frm.Text = "form_input_gudang"
        form_input_nama.Label1.Text = "Input Gudang Baru"
        form_input_nama.Label2.Text = "Nama Gudang"
        form_input_nama.Focus()
    End Sub

    Private Sub btn_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_perbarui.Click
        Call isidgv()
    End Sub

    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        If TxtCari.Text = "" Then
            MsgBox("NAMA GUDANG yang akan dirubah belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Gudang from tbgudang WHERE Nama_Gudang ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("NAMA GUDANG belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama.MdiParent = form_menu_utama
                                    form_input_nama.Show()
                                    form_input_nama.txt_frm.Text = "form_input_gudang"
                                    form_input_nama.Label1.Text = "Input Gudang Baru"
                                    form_input_nama.Label2.Text = "Nama Gudang"
                                    form_input_nama.txt_nama.Text = TxtCari.Text
                                    form_input_nama.txt_nama.Focus()
                                    form_input_nama.Focus()
                                End If
                            Else
                                form_input_nama.MdiParent = form_menu_utama
                                form_input_nama.Show()
                                form_input_nama.btn_simpan_baru.Visible = False
                                form_input_nama.txt_frm.Text = "form_input_gudang"
                                form_input_nama.Label1.Text = "Ubah Nama Gudang"
                                form_input_nama.Label2.Text = "Nama Gudang"
                                form_input_nama.txt_nama.Text = TxtCari.Text
                                form_input_nama.txt_nama.Focus()
                                form_input_nama.Focus()
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        If TxtCari.Text = "" Then
            MsgBox("NAMA GUDANG yang akan dihapus belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Gudang FROM tbgudang WHERE Nama_Gudang ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                If MsgBox("Yakin NAMA GUDANG Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Data") = vbYes Then
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "DELETE FROM tbgudang WHERE Nama_Gudang='" & TxtCari.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            cmdy.ExecuteNonQuery()
                                        End Using
                                        TxtCari.Text = ""
                                        MsgBox("NAMA GUDANG berhasil di Hapus")
                                    End Using
                                End If
                            Else
                                MsgBox("NAMA GUDANG Belum Tersimpan Di Database")
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

End Class