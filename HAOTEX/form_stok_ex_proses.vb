﻿Imports MySql.Data.MySqlClient

Public Class form_stok_ex_proses

    Private Sub form_stok_ex_proses_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If txt_form.Text = "form_input_po_packing_baris_1" Or _
            txt_form.Text = "form_input_po_packing_baris_2" Or _
            txt_form.Text = "form_input_po_packing_baris_3" Or _
            txt_form.Text = "form_input_po_packing_baris_4" Or _
            txt_form.Text = "form_input_po_packing_baris_5" Or _
            txt_form.Text = "form_input_po_packing_baris_6" Or _
            txt_form.Text = "form_input_po_packing_baris_7" Or _
            txt_form.Text = "form_input_po_packing_baris_8" Or _
            txt_form.Text = "form_input_po_packing_baris_9" Or _
            txt_form.Text = "form_input_po_packing_baris_10" Then
            form_input_po_packing.Focus()
            form_input_po_packing.Label2.Focus()
        End If
    End Sub
    Private Sub form_stok_ex_proses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "No SJ"
        dgv1.Columns(2).HeaderText = "Gudang"
        dgv1.Columns(3).HeaderText = "Jenis Kain"
        dgv1.Columns(4).HeaderText = "Warna"
        dgv1.Columns(5).HeaderText = "Resep"
        dgv1.Columns(6).HeaderText = "Partai"
        dgv1.Columns(7).HeaderText = "Gulung"
        dgv1.Columns(8).HeaderText = "STOK"
        dgv1.Columns(9).HeaderText = "Satuan"
        dgv1.Columns(10).HeaderText = "ID Grey"
        dgv1.Columns(11).HeaderText = "ID SJ Proses"
        dgv1.Columns(12).HeaderText = "Harga"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 80
        dgv1.Columns(2).Width = 140
        dgv1.Columns(3).Width = 140
        dgv1.Columns(4).Width = 90
        dgv1.Columns(5).Width = 90
        dgv1.Columns(6).Width = 90
        dgv1.Columns(7).Width = 80
        dgv1.Columns(8).Width = 100
        dgv1.Columns(9).Width = 70
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(8).DefaultCellStyle.Format = "N"
        dgv1.Columns(12).DefaultCellStyle.Format = "C"
        dgv1.Columns(10).Visible = False
        dgv1.Columns(11).Visible = False
        dgv1.Columns(12).Visible = False
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Proses,Harga FROM tbstokexproses WHERE NOT Stok=0 ORDER BY Gudang"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokexproses")
                            dgv1.DataSource = dsx.Tables("tbstokexproses")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.GotFocus
        If txt_cari_gudang.Text = "< Gudang >" Then
            txt_cari_gudang.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.LostFocus
        If txt_cari_gudang.Text = "" Then
            txt_cari_gudang.Text = "< Gudang >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub isidgv_gudang()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Proses,Harga FROM tbstokexproses WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND NOT Stok=0 ORDER BY Gudang"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbstokexproses")
                        dgv1.DataSource = dsx.Tables("tbstokexproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
    End Sub
    Private Sub isidgv_jeniskain()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Proses,Harga FROM tbstokexproses WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_Kain"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbstokexproses")
                        dgv1.DataSource = dsx.Tables("tbstokexproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
    End Sub
    Private Sub isidgv_gudang_jeniskain()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Proses,Harga FROM tbstokexproses WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Gudang"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbstokexproses")
                        dgv1.DataSource = dsx.Tables("tbstokexproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.TextChanged
        Try
            If txt_cari_gudang.Text = "< Gudang >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_gudang.Text = "< Gudang >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_gudang.Text = "< Gudang >" Then

            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgv1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv1.MouseDoubleClick
        Try
            If txt_form.Text = "form_input_po_packing_baris_1" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_1.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_1.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_1.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_1.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_1.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_1.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_1.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_1.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_1.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_1.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_1.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_1.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_1.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_1.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_2" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_2.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_2.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_2.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_2.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_2.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_2.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_2.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_2.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_2.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_2.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_2.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_2.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_2.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_2.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_3" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_3.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_3.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_3.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_3.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_3.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_3.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_3.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_3.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_3.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_3.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_3.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_3.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_3.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_3.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_4" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_4.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_4.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_4.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_4.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_4.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_4.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_4.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_4.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_4.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_4.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_4.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_4.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_4.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_4.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_5" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_5.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_5.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_5.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_5.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_5.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_5.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_5.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_5.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_5.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_5.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_5.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_5.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_5.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_5.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_6" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_6.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_6.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_6.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_6.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_6.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_6.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_6.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_6.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_6.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_6.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_6.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_6.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_6.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_6.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_7" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_7.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_7.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_7.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_7.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_7.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_7.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_7.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_7.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_7.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_7.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_7.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_7.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_7.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_7.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_8" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_8.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_8.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_8.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_8.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_8.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_8.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_8.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_8.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_8.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_8.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_8.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_8.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_8.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_8.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_9" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_9.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_9.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_9.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_9.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_9.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_9.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_9.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_9.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_9.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_9.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_9.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_9.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_9.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_9.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            ElseIf txt_form.Text = "form_input_po_packing_baris_10" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_packing.cb_satuan_10.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_asal_satuan_10.Text = .Cells(9).Value.ToString
                        form_input_po_packing.txt_qty_asal_10.Text = .Cells(8).Value.ToString
                        form_input_po_packing.txt_meter_10.Text = .Cells(8).Value.ToString
                        form_input_po_packing.dtp_tanggal_sj_10.Text = .Cells(0).FormattedValue.ToString
                        form_input_po_packing.txt_asal_sj_10.Text = .Cells(1).Value.ToString
                        form_input_po_packing.txt_jenis_kain_10.Text = .Cells(3).Value.ToString
                        form_input_po_packing.txt_warna_10.Text = .Cells(4).Value.ToString
                        form_input_po_packing.txt_resep_10.Text = .Cells(5).Value.ToString
                        form_input_po_packing.txt_partai_10.Text = .Cells(6).Value.ToString
                        form_input_po_packing.txt_gulung_10.Text = .Cells(7).Value.ToString
                        form_input_po_packing.txt_id_grey_10.Text = .Cells(10).Value.ToString
                        form_input_po_packing.txt_id_sj_proses_10.Text = .Cells(11).Value.ToString
                        form_input_po_packing.txt_harga_10.Text = .Cells(12).Value.ToString
                    End With
                    Me.Close()
                    form_input_po_packing.MdiParent = form_menu_utama
                    form_input_po_packing.Show()
                    form_input_po_packing.Label2.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class