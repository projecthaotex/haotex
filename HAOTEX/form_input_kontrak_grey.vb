﻿Imports MySql.Data.MySqlClient

Public Class form_input_kontrak_grey
    Private Sub form_input_kontrak_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isikodegreybaru()
    End Sub
    Private Sub isikodegreybaru()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey.Text = y.ToString
        Else
            txt_id_grey.Text = x.ToString
        End If
    End Sub
    Private Sub ngosongkeun()
        dtp_tanggal.Text = Today
        txt_no_kontrak.Text = ""
        txt_jenis_kain.Text = ""
        txt_supplier.Text = ""
        txt_harga.Text = ""
        txt_total.Text = ""
        txt_keterangan.Text = ""
        cb_satuan.Text = "Meter"
        Call isikodegreybaru()
    End Sub
    Private Sub txt_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain.GotFocus
        form_input_jenis_kain.MdiParent = form_menu_utama
        form_input_jenis_kain.Show()
        form_input_jenis_kain.TxtForm.Text = "form_input_kontrak_grey"
        form_input_jenis_kain.Focus()
    End Sub
    Private Sub txt_supplier_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_kontrak_grey"
        form_input_supplier.Focus()
    End Sub
    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Me.Close()
    End Sub

    Private Sub insert_tbkontrakgrey()
        Dim harga As String = Me.txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim total As String = txt_total.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbkontrakgrey (Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey.Text)
                    .Parameters.AddWithValue("@1", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@2", txt_no_kontrak.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@4", txt_supplier.Text)
                    .Parameters.AddWithValue("@5", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@6", total.Replace(",", "."))
                    .Parameters.AddWithValue("@7", 0)
                    .Parameters.AddWithValue("@8", total.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan.Text)
                    .Parameters.AddWithValue("@10", txt_keterangan.Text)
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub update_tbkontrakgrey()
        Dim harga As String = Me.txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim total As String = txt_total.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbkontrakgrey SET Tanggal=@1,No_Kontrak=@2,Jenis_Kain=@3,Supplier=@4,Harga=@5,Total=@6,Dikirim=@7,Sisa=@8,Satuan=@9,Keterangan=@10,Tambah1=@11,Tambah2=@12,Tambah3=@13,Tambah4=@14,Tambah5=@15 WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@2", txt_no_kontrak.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@4", txt_supplier.Text)
                    .Parameters.AddWithValue("@5", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@6", total.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_dikirim.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@8", total.Replace(",", ".") - Val(txt_dikirim.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@9", cb_satuan.Text)
                    .Parameters.AddWithValue("@10", txt_keterangan.Text)
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub btn_simpan_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_baru.Click
        Try
            If Label1.Text = "Input Kontrak Baru" Then
                If txt_no_kontrak.Text = "" Then
                    MsgBox("NO KONTRAK Belum Diinput")
                    txt_no_kontrak.Focus()
                ElseIf txt_jenis_kain.Text = "" Then
                    MsgBox("JENIS KAIN Belum Diinput")
                    txt_jenis_kain.Focus()
                ElseIf txt_supplier.Text = "" Then
                    MsgBox("NAMA SUPPLIER Belum Diinput")
                    txt_supplier.Focus()
                ElseIf Val(txt_total.Text) = 0 Then
                    MsgBox("TOTAL Meter Belum Diinput")
                    txt_total.Focus()
                ElseIf Val(txt_harga.Text) = 0 Then
                    If cb_satuan.Text = "Meter" Then
                        MsgBox("HARGA per METER Belum Diinput")
                    Else
                        MsgBox("HARGA per YARD Belum Diinput")
                    End If
                    txt_harga.Focus()
                Else
                    Call insert_tbkontrakgrey()
                    MsgBox("KONTRAK Baru Berhasil Disimpan")
                    Call ngosongkeun()
                    form_kontrak_grey.ts_perbarui.PerformClick()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_simpan_tutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_tutup.Click
        Try
            Dim harga As String = Me.txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim total As String = txt_total.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_no_kontrak.Text = "" Then
                MsgBox("NO KONTRAK tidak boleh kosong")
                txt_no_kontrak.Focus()
            ElseIf txt_jenis_kain.Text = "" Then
                MsgBox("JENIS KAIN tidak boleh kosong")
                txt_jenis_kain.Focus()
            ElseIf txt_supplier.Text = "" Then
                MsgBox("NAMA SUPPLIER tidak boleh kosong")
                txt_supplier.Focus()
            ElseIf Val(txt_total.Text) = 0 Then
                MsgBox("TOTAL Meter Belum Diinput")
                txt_total.Focus()
            ElseIf Val(txt_harga.Text) = 0 Then
                If cb_satuan.Text = "Meter" Then
                    MsgBox("HARGA per METER Belum Diinput")
                Else
                    MsgBox("HARGA per YARD Belum Diinput")
                End If
                txt_harga.Focus()
            Else
                If Label1.Text = "Input Kontrak Baru" Then
                    Call insert_tbkontrakgrey()
                    MsgBox("KONTRAK Baru Berhasil Disimpan")
                    Call ngosongkeun()
                    form_kontrak_grey.ts_perbarui.PerformClick()
                    Me.Close()
                ElseIf Label1.Text = "Ubah Kontrak" Then
                    If Val(total.Replace(",", ".")) < Val(txt_dikirim.Text.Replace(",", ".")) Then
                        MsgBox("Total QTY Tidak Boleh Kurang Dari Jumlah Yang Dikirim")
                        txt_total.Focus()
                    Else
                        Call update_tbkontrakgrey()
                        MsgBox("KONTRAK Berhasil Di UBAH")
                        form_kontrak_grey.ts_perbarui.PerformClick()
                        Me.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txt_total_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total.Text = String.Empty Then
                        txt_total.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total.Select(txt_total.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total.Text = String.Empty Then
                        txt_total.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total.Select(txt_total.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If e.KeyChar = Chr(13) Then
            txt_harga.Focus()
        End If
    End Sub
    Private Sub txt_harga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If Me.txt_harga.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If Me.txt_harga.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If Me.txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If Me.txt_harga.Text = String.Empty Then
                        Me.txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        Me.txt_harga.Select(Me.txt_harga.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If Me.txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If Me.txt_harga.Text = String.Empty Then
                        Me.txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        Me.txt_harga.Select(Me.txt_harga.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If e.KeyChar = Chr(13) Then
            txt_keterangan.Focus()
        End If
    End Sub
    Private Sub txt_total_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_total.TextChanged
        If txt_total.Text <> String.Empty Then
            Dim temp As String = txt_total.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total.Select(txt_total.Text.Length, 0)
            ElseIf txt_total.Text = "-"c Then

            Else
                txt_total.Text = CDec(temp).ToString("N0")
                txt_total.Select(txt_total.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_harga_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_harga.TextChanged
        If txt_harga.Text <> String.Empty Then
            Dim temp As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga.Select(txt_harga.Text.Length, 0)
            ElseIf txt_harga.Text = "-"c Then

            Else
                txt_harga.Text = CDec(temp).ToString("N0")
                txt_harga.Select(txt_harga.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_no_kontrak_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_no_kontrak.KeyPress
        If txt_jenis_kain.Enabled = False Then
            If e.KeyChar = Chr(13) Then
                txt_harga.Focus()
            End If
        Else
            If e.KeyChar = Chr(13) Then
                txt_jenis_kain.Focus()
            End If
        End If
    End Sub
End Class