﻿Imports MySql.Data.MySqlClient

Public Class form_hasil_packing

    Private Sub form_hasil_packing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call awal()
        dtp_hari_ini.Text = Today
    End Sub
    Private Sub awal()
        Call isidtpawal()
        Call isidgv()
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(-3)
        dtp_awal.Text = tanggal
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Gudang"
        dgv1.Columns(2).HeaderText = "SJ Packing"
        dgv1.Columns(3).HeaderText = "No PO"
        dgv1.Columns(4).HeaderText = "Customer"
        dgv1.Columns(5).HeaderText = "Jenis Kain"
        dgv1.Columns(6).HeaderText = "Warna"
        dgv1.Columns(7).HeaderText = "Gulung"
        dgv1.Columns(8).HeaderText = "QTY"
        dgv1.Columns(9).HeaderText = "Satuan"
        dgv1.Columns(10).HeaderText = "Susut"
        dgv1.Columns(11).HeaderText = "Harga"
        dgv1.Columns(12).HeaderText = "Total Harga"
        dgv1.Columns(13).HeaderText = "Grade A"
        dgv1.Columns(14).HeaderText = "Keluar SJ"
        dgv1.Columns(15).HeaderText = "Gulung"
        dgv1.Columns(16).HeaderText = "Grade B"
        dgv1.Columns(17).HeaderText = "Keluar SJ"
        dgv1.Columns(18).HeaderText = "Gulung"
        dgv1.Columns(19).HeaderText = "Claim Jadi"
        dgv1.Columns(20).HeaderText = "Keluar SJ"
        dgv1.Columns(21).HeaderText = "Gulung"
        dgv1.Columns(22).HeaderText = "Claim Celup"
        dgv1.Columns(23).HeaderText = "Keluar SJ"
        dgv1.Columns(24).HeaderText = "Gulung"
        dgv1.Columns(25).HeaderText = "Keterangan"
        dgv1.Columns(26).HeaderText = "Status"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 120
        dgv1.Columns(2).Width = 100
        dgv1.Columns(3).Width = 100
        dgv1.Columns(4).Width = 120
        dgv1.Columns(5).Width = 130
        dgv1.Columns(6).Width = 100
        dgv1.Columns(7).Width = 60
        dgv1.Columns(8).Width = 100
        dgv1.Columns(9).Width = 80
        dgv1.Columns(10).Width = 80
        dgv1.Columns(11).Width = 100
        dgv1.Columns(12).Width = 120
        dgv1.Columns(13).Width = 100
        dgv1.Columns(14).Width = 100
        dgv1.Columns(15).Width = 60
        dgv1.Columns(16).Width = 100
        dgv1.Columns(17).Width = 100
        dgv1.Columns(18).Width = 60
        dgv1.Columns(19).Width = 100
        dgv1.Columns(20).Width = 100
        dgv1.Columns(21).Width = 60
        dgv1.Columns(22).Width = 100
        dgv1.Columns(23).Width = 100
        dgv1.Columns(24).Width = 60
        dgv1.Columns(25).Width = 120
        dgv1.Columns(26).Width = 80
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(13).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(14).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(15).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(16).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(17).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(18).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(19).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(20).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(21).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(22).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(23).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(24).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Format = "N"
        dgv1.Columns(11).DefaultCellStyle.Format = "C"
        dgv1.Columns(12).DefaultCellStyle.Format = "C"
        dgv1.Columns(13).DefaultCellStyle.Format = "N"
        dgv1.Columns(14).DefaultCellStyle.Format = "N"
        dgv1.Columns(16).DefaultCellStyle.Format = "N"
        dgv1.Columns(17).DefaultCellStyle.Format = "N"
        dgv1.Columns(19).DefaultCellStyle.Format = "N"
        dgv1.Columns(20).DefaultCellStyle.Format = "N"
        dgv1.Columns(22).DefaultCellStyle.Format = "N"
        dgv1.Columns(23).DefaultCellStyle.Format = "N"
        dgv1.Columns(27).Visible = False
        dgv1.Columns(28).Visible = False
        dgv1.Columns(29).Visible = False
    End Sub
    Private Sub dgv1_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgv1.CellFormatting
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(26).Value = "Tutup" Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Red
            ElseIf Not (dgv1.Rows(i).Cells(14).Value + dgv1.Rows(i).Cells(17).Value + dgv1.Rows(i).Cells(20).Value + dgv1.Rows(i).Cells(23).Value) = 0 Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Blue
            Else
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Black
            End If
        Next
    End Sub
    Private Sub hitungjumlah()
        Dim jumlahtotal, belumsj, sudahsj, jumlahclaimcelup, sudahproses As Double
        jumlahtotal = 0
        sudahsj = 0
        belumsj = 0
        jumlahclaimcelup = 0
        sudahproses = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            jumlahtotal = jumlahtotal + Val(dgv1.Rows(i).Cells(13).Value + dgv1.Rows(i).Cells(16).Value + dgv1.Rows(i).Cells(19).Value)
            sudahsj = sudahsj + Val(dgv1.Rows(i).Cells(14).Value + dgv1.Rows(i).Cells(17).Value + dgv1.Rows(i).Cells(20).Value)
            belumsj = jumlahtotal - sudahsj
            jumlahclaimcelup = jumlahclaimcelup + Val(dgv1.Rows(i).Cells(22).Value)
            sudahproses = sudahproses + Val(dgv1.Rows(i).Cells(23).Value)
        Next
        txt_jumlah_total.Text = jumlahtotal
        txt_keluar_sj.Text = sudahsj
        txt_belum_sj.Text = belumsj
        txt_jumlah_claim_celup.Text = jumlahclaimcelup
        txt_sudah_proses.Text = sudahproses
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbkeluarbarangjadi")
                            dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_awal.Text = dtp_akhir.Text
        End If
        Call isidgv()
    End Sub
    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
        Call isidgv()
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_hasil_packing.MdiParent = form_menu_utama
        form_input_hasil_packing.Show()
        form_input_hasil_packing.Focus()
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        Call isidgv()
        txt_cari_sj_packing.Text = ""
        txt_cari_gudang_packing.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_customer.Text = ""
        Panel2.Enabled = True
        btn_cari.Text = "CARI"
        btn_cari.Image = HAOTEX.My.Resources.search
        dgv1.Focus()
    End Sub

    Private Sub isidgv_surat_jalan_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE SJ_Packing LIKE '" & txt_cari_sj_packing.Text & "' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_gudang_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_gudang_packing_Jenis_Kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_gudang_packing_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_gudang_packing_jenis_kain_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%'  AND Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_jenis_kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_jenis_kain_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing FROM tbkeluarbarangjadi WHERE Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv1.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        If btn_cari.Text = "CARI" Then
            btn_cari.Text = "RESET"
            btn_cari.Image = HAOTEX.My.Resources.refresh
            Panel2.Enabled = False
            If txt_cari_sj_packing.Text = "" And txt_cari_gudang_packing.Text = "" _
                And txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv()
            ElseIf Not txt_cari_sj_packing.Text = "" Then
                Call isidgv_surat_jalan_packing()
            ElseIf Not txt_cari_gudang_packing.Text = "" And txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing()
            ElseIf Not txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing_Jenis_Kain()
            ElseIf Not txt_cari_gudang_packing.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing_customer()
            ElseIf Not txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing_jenis_kain_customer()
            ElseIf txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv_jenis_kain()
            ElseIf txt_cari_gudang_packing.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_customer()
            ElseIf txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_jenis_kain_customer()
            End If
        Else
            ts_perbarui.PerformClick()
        End If
    End Sub

    Private Sub txt_cari_sj_packing_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_sj_packing.GotFocus
        txt_cari_gudang_packing.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_customer.Text = ""
    End Sub
    Private Sub txt_cari_gudang_packing_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang_packing.GotFocus, txt_cari_customer.GotFocus, txt_cari_jenis_kain.GotFocus
        txt_cari_sj_packing.Text = ""
    End Sub

    Private Sub ts_print_hasil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print_hasil.Click
        Try
            If dgv1.RowCount = 0 Then
                MsgBox("Tidak Terdapat Data Untuk Ditampilkan")
            Else
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    txt_id_grey.Text = .Cells(29).Value
                    txt_sj_packing.Text = .Cells(2).Value
                    txt_baris_packing.Text = .Cells(3).Value
                    txt_gudang.Text = .Cells(1).Value
                    txt_customer.Text = .Cells(6).Value
                    txt_jenis_kain.Text = .Cells(7).Value
                    txt_warna.Text = .Cells(8).Value
                End With
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx As String = "SELECT * FROM tbkeluarbarangjadi WHERE Id_Grey='" & txt_id_grey.Text & "' AND SJ_Packing='" & txt_sj_packing.Text & "' AND Baris_SJ='" & txt_baris_packing.Text & "' AND Gudang='" & txt_gudang.Text & "' AND Customer='" & txt_customer.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND Warna='" & txt_warna.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                form_print_hasil_packing.MdiParent = form_menu_utama
                                form_print_hasil_packing.Show()
                                form_print_hasil_packing.Focus()
                                form_print_hasil_packing.dtp_awal.Text = drx.Item(0)
                                form_print_hasil_packing.dtp_jatuh_tempo.Text = drx.Item(1)
                                form_print_hasil_packing.txt_gudang.Text = drx.Item(2)
                                form_print_hasil_packing.txt_sj_packing.Text = drx.Item(3)
                                form_print_hasil_packing.txt_baris_sj.Text = drx.Item(4)
                                form_print_hasil_packing.txt_no_po.Text = drx.Item(5)
                                form_print_hasil_packing.txt_baris_po.Text = drx.Item(6)
                                form_print_hasil_packing.txt_customer.Text = drx.Item(7)
                                form_print_hasil_packing.txt_jenis_kain.Text = drx.Item(8)
                                form_print_hasil_packing.txt_warna.Text = drx.Item(9)
                                form_print_hasil_packing.txt_gulung.Text = drx.Item(10)
                                form_print_hasil_packing.txt_meter.Text = drx.Item(11)
                                form_print_hasil_packing.txt_yards.Text = drx.Item(12)
                                form_print_hasil_packing.txt_susut.Text = drx.Item(13)
                                form_print_hasil_packing.txt_harga.Text = drx.Item(14)
                                form_print_hasil_packing.txt_total_harga.Text = drx.Item(15)
                                form_print_hasil_packing.txt_grade_a.Text = drx.Item(16)
                                form_print_hasil_packing.txt_gl_grade_a.Text = drx.Item(18)
                                form_print_hasil_packing.txt_grade_b.Text = drx.Item(19)
                                form_print_hasil_packing.txt_gl_grade_b.Text = drx.Item(21)
                                form_print_hasil_packing.txt_claim_jadi.Text = drx.Item(22)
                                form_print_hasil_packing.txt_gl_claim_jadi.Text = drx.Item(24)
                                form_print_hasil_packing.txt_claim_celup.Text = drx.Item(25)
                                form_print_hasil_packing.txt_gl_claim_celup.Text = drx.Item(27)
                                form_print_hasil_packing.txt_keterangan.Text = drx.Item(28)
                                If drx.Item(29) = "Tutup" Then
                                    form_print_hasil_packing.Label45.Text = "Tutup"
                                    form_print_hasil_packing.Label45.ForeColor = Color.Red
                                Else
                                    form_print_hasil_packing.Label45.Text = "Belum Tutup"
                                End If
                                form_print_hasil_packing.M1.Text = drx.Item(31)
                                form_print_hasil_packing.M2.Text = drx.Item(32)
                                form_print_hasil_packing.M3.Text = drx.Item(33)
                                form_print_hasil_packing.M4.Text = drx.Item(34)
                                form_print_hasil_packing.M5.Text = drx.Item(35)
                                form_print_hasil_packing.M6.Text = drx.Item(36)
                                form_print_hasil_packing.M7.Text = drx.Item(37)
                                form_print_hasil_packing.M8.Text = drx.Item(38)
                                form_print_hasil_packing.M9.Text = drx.Item(39)
                                form_print_hasil_packing.M10.Text = drx.Item(40)
                                form_print_hasil_packing.M11.Text = drx.Item(41)
                                form_print_hasil_packing.M12.Text = drx.Item(42)
                                form_print_hasil_packing.M13.Text = drx.Item(43)
                                form_print_hasil_packing.M14.Text = drx.Item(44)
                                form_print_hasil_packing.M15.Text = drx.Item(45)
                                form_print_hasil_packing.M16.Text = drx.Item(46)
                                form_print_hasil_packing.M17.Text = drx.Item(47)
                                form_print_hasil_packing.M18.Text = drx.Item(48)
                                form_print_hasil_packing.M19.Text = drx.Item(49)
                                form_print_hasil_packing.M20.Text = drx.Item(50)
                                form_print_hasil_packing.M21.Text = drx.Item(51)
                                form_print_hasil_packing.M22.Text = drx.Item(52)
                                form_print_hasil_packing.M23.Text = drx.Item(53)
                                form_print_hasil_packing.M24.Text = drx.Item(54)
                                form_print_hasil_packing.M25.Text = drx.Item(55)
                                form_print_hasil_packing.K1.Text = drx.Item(56)
                                form_print_hasil_packing.K2.Text = drx.Item(57)
                                form_print_hasil_packing.K3.Text = drx.Item(58)
                                form_print_hasil_packing.K4.Text = drx.Item(59)
                                form_print_hasil_packing.K5.Text = drx.Item(60)
                                form_print_hasil_packing.K6.Text = drx.Item(61)
                                form_print_hasil_packing.K7.Text = drx.Item(62)
                                form_print_hasil_packing.K8.Text = drx.Item(63)
                                form_print_hasil_packing.K9.Text = drx.Item(64)
                                form_print_hasil_packing.K10.Text = drx.Item(65)
                                form_print_hasil_packing.K11.Text = drx.Item(66)
                                form_print_hasil_packing.K12.Text = drx.Item(67)
                                form_print_hasil_packing.K13.Text = drx.Item(68)
                                form_print_hasil_packing.K14.Text = drx.Item(69)
                                form_print_hasil_packing.K15.Text = drx.Item(70)
                                form_print_hasil_packing.K16.Text = drx.Item(71)
                                form_print_hasil_packing.K17.Text = drx.Item(72)
                                form_print_hasil_packing.K18.Text = drx.Item(73)
                                form_print_hasil_packing.K19.Text = drx.Item(74)
                                form_print_hasil_packing.K20.Text = drx.Item(75)
                                form_print_hasil_packing.K21.Text = drx.Item(76)
                                form_print_hasil_packing.K22.Text = drx.Item(77)
                                form_print_hasil_packing.K23.Text = drx.Item(78)
                                form_print_hasil_packing.K24.Text = drx.Item(79)
                                form_print_hasil_packing.K25.Text = drx.Item(80)
                                form_print_hasil_packing.Y1.Text = drx.Item(81)
                                form_print_hasil_packing.Y2.Text = drx.Item(82)
                                form_print_hasil_packing.Y3.Text = drx.Item(83)
                                form_print_hasil_packing.Y4.Text = drx.Item(84)
                                form_print_hasil_packing.Y5.Text = drx.Item(85)
                                form_print_hasil_packing.Y6.Text = drx.Item(86)
                                form_print_hasil_packing.Y7.Text = drx.Item(87)
                                form_print_hasil_packing.Y8.Text = drx.Item(88)
                                form_print_hasil_packing.Y9.Text = drx.Item(89)
                                form_print_hasil_packing.Y10.Text = drx.Item(90)
                                form_print_hasil_packing.Y11.Text = drx.Item(91)
                                form_print_hasil_packing.Y12.Text = drx.Item(92)
                                form_print_hasil_packing.Y13.Text = drx.Item(93)
                                form_print_hasil_packing.Y14.Text = drx.Item(94)
                                form_print_hasil_packing.Y15.Text = drx.Item(95)
                                form_print_hasil_packing.Y16.Text = drx.Item(96)
                                form_print_hasil_packing.Y17.Text = drx.Item(97)
                                form_print_hasil_packing.Y18.Text = drx.Item(98)
                                form_print_hasil_packing.Y19.Text = drx.Item(99)
                                form_print_hasil_packing.Y20.Text = drx.Item(100)
                                form_print_hasil_packing.Y21.Text = drx.Item(101)
                                form_print_hasil_packing.Y22.Text = drx.Item(102)
                                form_print_hasil_packing.Y23.Text = drx.Item(103)
                                form_print_hasil_packing.Y24.Text = drx.Item(104)
                                form_print_hasil_packing.Y25.Text = drx.Item(105)
                                form_print_hasil_packing.A1.Text = drx.Item(106)
                                form_print_hasil_packing.A2.Text = drx.Item(107)
                                form_print_hasil_packing.A3.Text = drx.Item(108)
                                form_print_hasil_packing.A4.Text = drx.Item(109)
                                form_print_hasil_packing.A5.Text = drx.Item(110)
                                form_print_hasil_packing.A6.Text = drx.Item(111)
                                form_print_hasil_packing.A7.Text = drx.Item(112)
                                form_print_hasil_packing.A8.Text = drx.Item(113)
                                form_print_hasil_packing.A9.Text = drx.Item(114)
                                form_print_hasil_packing.A10.Text = drx.Item(115)
                                form_print_hasil_packing.A11.Text = drx.Item(116)
                                form_print_hasil_packing.A12.Text = drx.Item(117)
                                form_print_hasil_packing.A13.Text = drx.Item(118)
                                form_print_hasil_packing.A14.Text = drx.Item(119)
                                form_print_hasil_packing.A15.Text = drx.Item(120)
                                form_print_hasil_packing.A16.Text = drx.Item(121)
                                form_print_hasil_packing.A17.Text = drx.Item(122)
                                form_print_hasil_packing.A18.Text = drx.Item(123)
                                form_print_hasil_packing.A19.Text = drx.Item(124)
                                form_print_hasil_packing.A20.Text = drx.Item(125)
                                form_print_hasil_packing.A21.Text = drx.Item(126)
                                form_print_hasil_packing.A22.Text = drx.Item(127)
                                form_print_hasil_packing.A23.Text = drx.Item(128)
                                form_print_hasil_packing.A24.Text = drx.Item(129)
                                form_print_hasil_packing.A25.Text = drx.Item(130)
                                form_print_hasil_packing.B1.Text = drx.Item(131)
                                form_print_hasil_packing.B2.Text = drx.Item(132)
                                form_print_hasil_packing.B3.Text = drx.Item(133)
                                form_print_hasil_packing.B4.Text = drx.Item(134)
                                form_print_hasil_packing.B5.Text = drx.Item(135)
                                form_print_hasil_packing.B6.Text = drx.Item(136)
                                form_print_hasil_packing.B7.Text = drx.Item(137)
                                form_print_hasil_packing.B8.Text = drx.Item(138)
                                form_print_hasil_packing.B9.Text = drx.Item(139)
                                form_print_hasil_packing.B10.Text = drx.Item(140)
                                form_print_hasil_packing.B11.Text = drx.Item(141)
                                form_print_hasil_packing.B12.Text = drx.Item(142)
                                form_print_hasil_packing.B13.Text = drx.Item(143)
                                form_print_hasil_packing.B14.Text = drx.Item(144)
                                form_print_hasil_packing.B15.Text = drx.Item(145)
                                form_print_hasil_packing.B16.Text = drx.Item(146)
                                form_print_hasil_packing.B17.Text = drx.Item(147)
                                form_print_hasil_packing.B18.Text = drx.Item(148)
                                form_print_hasil_packing.B19.Text = drx.Item(149)
                                form_print_hasil_packing.B20.Text = drx.Item(150)
                                form_print_hasil_packing.B21.Text = drx.Item(151)
                                form_print_hasil_packing.B22.Text = drx.Item(152)
                                form_print_hasil_packing.B23.Text = drx.Item(153)
                                form_print_hasil_packing.B24.Text = drx.Item(154)
                                form_print_hasil_packing.B25.Text = drx.Item(155)
                                form_print_hasil_packing.C1.Text = drx.Item(156)
                                form_print_hasil_packing.C2.Text = drx.Item(157)
                                form_print_hasil_packing.C3.Text = drx.Item(158)
                                form_print_hasil_packing.C4.Text = drx.Item(159)
                                form_print_hasil_packing.C5.Text = drx.Item(160)
                                form_print_hasil_packing.C6.Text = drx.Item(161)
                                form_print_hasil_packing.C7.Text = drx.Item(162)
                                form_print_hasil_packing.C8.Text = drx.Item(163)
                                form_print_hasil_packing.C9.Text = drx.Item(164)
                                form_print_hasil_packing.C10.Text = drx.Item(165)
                                form_print_hasil_packing.C11.Text = drx.Item(166)
                                form_print_hasil_packing.C12.Text = drx.Item(167)
                                form_print_hasil_packing.C13.Text = drx.Item(168)
                                form_print_hasil_packing.C14.Text = drx.Item(169)
                                form_print_hasil_packing.C15.Text = drx.Item(170)
                                form_print_hasil_packing.C16.Text = drx.Item(171)
                                form_print_hasil_packing.C17.Text = drx.Item(172)
                                form_print_hasil_packing.C18.Text = drx.Item(173)
                                form_print_hasil_packing.C19.Text = drx.Item(174)
                                form_print_hasil_packing.C20.Text = drx.Item(175)
                                form_print_hasil_packing.C21.Text = drx.Item(176)
                                form_print_hasil_packing.C22.Text = drx.Item(177)
                                form_print_hasil_packing.C23.Text = drx.Item(178)
                                form_print_hasil_packing.C24.Text = drx.Item(179)
                                form_print_hasil_packing.C25.Text = drx.Item(180)
                                form_print_hasil_packing.D1.Text = drx.Item(181)
                                form_print_hasil_packing.D2.Text = drx.Item(182)
                                form_print_hasil_packing.D3.Text = drx.Item(183)
                                form_print_hasil_packing.D4.Text = drx.Item(184)
                                form_print_hasil_packing.D5.Text = drx.Item(185)
                                form_print_hasil_packing.D6.Text = drx.Item(186)
                                form_print_hasil_packing.D7.Text = drx.Item(187)
                                form_print_hasil_packing.D8.Text = drx.Item(188)
                                form_print_hasil_packing.D9.Text = drx.Item(189)
                                form_print_hasil_packing.D10.Text = drx.Item(190)
                                form_print_hasil_packing.D11.Text = drx.Item(191)
                                form_print_hasil_packing.D12.Text = drx.Item(192)
                                form_print_hasil_packing.D13.Text = drx.Item(193)
                                form_print_hasil_packing.D14.Text = drx.Item(194)
                                form_print_hasil_packing.D15.Text = drx.Item(195)
                                form_print_hasil_packing.D16.Text = drx.Item(196)
                                form_print_hasil_packing.D17.Text = drx.Item(197)
                                form_print_hasil_packing.D18.Text = drx.Item(198)
                                form_print_hasil_packing.D19.Text = drx.Item(199)
                                form_print_hasil_packing.D20.Text = drx.Item(200)
                                form_print_hasil_packing.D21.Text = drx.Item(201)
                                form_print_hasil_packing.D22.Text = drx.Item(202)
                                form_print_hasil_packing.D23.Text = drx.Item(203)
                                form_print_hasil_packing.D24.Text = drx.Item(204)
                                form_print_hasil_packing.D25.Text = drx.Item(205)
                                form_print_hasil_packing.E1.Text = drx.Item(206)
                                form_print_hasil_packing.E2.Text = drx.Item(207)
                                form_print_hasil_packing.E3.Text = drx.Item(208)
                                form_print_hasil_packing.E4.Text = drx.Item(209)
                                form_print_hasil_packing.E5.Text = drx.Item(210)
                                form_print_hasil_packing.E6.Text = drx.Item(211)
                                form_print_hasil_packing.E7.Text = drx.Item(212)
                                form_print_hasil_packing.E8.Text = drx.Item(213)
                                form_print_hasil_packing.E9.Text = drx.Item(214)
                                form_print_hasil_packing.E10.Text = drx.Item(215)
                                form_print_hasil_packing.E11.Text = drx.Item(216)
                                form_print_hasil_packing.E12.Text = drx.Item(217)
                                form_print_hasil_packing.E13.Text = drx.Item(218)
                                form_print_hasil_packing.E14.Text = drx.Item(219)
                                form_print_hasil_packing.E15.Text = drx.Item(220)
                                form_print_hasil_packing.E16.Text = drx.Item(221)
                                form_print_hasil_packing.E17.Text = drx.Item(222)
                                form_print_hasil_packing.E18.Text = drx.Item(223)
                                form_print_hasil_packing.E19.Text = drx.Item(224)
                                form_print_hasil_packing.E20.Text = drx.Item(225)
                                form_print_hasil_packing.E21.Text = drx.Item(226)
                                form_print_hasil_packing.E22.Text = drx.Item(227)
                                form_print_hasil_packing.E23.Text = drx.Item(228)
                                form_print_hasil_packing.E24.Text = drx.Item(229)
                                form_print_hasil_packing.E25.Text = drx.Item(230)
                                form_print_hasil_packing.AW1.Text = drx.Item(231)
                                form_print_hasil_packing.AW2.Text = drx.Item(232)
                                form_print_hasil_packing.AW3.Text = drx.Item(233)
                                form_print_hasil_packing.AW4.Text = drx.Item(234)
                                form_print_hasil_packing.AW5.Text = drx.Item(235)
                                form_print_hasil_packing.AW6.Text = drx.Item(236)
                                form_print_hasil_packing.AW7.Text = drx.Item(237)
                                form_print_hasil_packing.AW8.Text = drx.Item(238)
                                form_print_hasil_packing.AW9.Text = drx.Item(239)
                                form_print_hasil_packing.AW10.Text = drx.Item(240)
                                form_print_hasil_packing.AW11.Text = drx.Item(241)
                                form_print_hasil_packing.AW12.Text = drx.Item(242)
                                form_print_hasil_packing.AW13.Text = drx.Item(243)
                                form_print_hasil_packing.AW14.Text = drx.Item(244)
                                form_print_hasil_packing.AW15.Text = drx.Item(245)
                                form_print_hasil_packing.AW16.Text = drx.Item(246)
                                form_print_hasil_packing.AW17.Text = drx.Item(247)
                                form_print_hasil_packing.AW18.Text = drx.Item(248)
                                form_print_hasil_packing.AW19.Text = drx.Item(249)
                                form_print_hasil_packing.AW20.Text = drx.Item(250)
                                form_print_hasil_packing.AW21.Text = drx.Item(251)
                                form_print_hasil_packing.AW22.Text = drx.Item(252)
                                form_print_hasil_packing.AW23.Text = drx.Item(253)
                                form_print_hasil_packing.AW24.Text = drx.Item(254)
                                form_print_hasil_packing.AW25.Text = drx.Item(255)
                                form_print_hasil_packing.BW1.Text = drx.Item(256)
                                form_print_hasil_packing.BW2.Text = drx.Item(257)
                                form_print_hasil_packing.BW3.Text = drx.Item(258)
                                form_print_hasil_packing.BW4.Text = drx.Item(259)
                                form_print_hasil_packing.BW5.Text = drx.Item(260)
                                form_print_hasil_packing.BW6.Text = drx.Item(261)
                                form_print_hasil_packing.BW7.Text = drx.Item(262)
                                form_print_hasil_packing.BW8.Text = drx.Item(263)
                                form_print_hasil_packing.BW9.Text = drx.Item(264)
                                form_print_hasil_packing.BW10.Text = drx.Item(265)
                                form_print_hasil_packing.BW11.Text = drx.Item(266)
                                form_print_hasil_packing.BW12.Text = drx.Item(267)
                                form_print_hasil_packing.BW13.Text = drx.Item(268)
                                form_print_hasil_packing.BW14.Text = drx.Item(269)
                                form_print_hasil_packing.BW15.Text = drx.Item(270)
                                form_print_hasil_packing.BW16.Text = drx.Item(271)
                                form_print_hasil_packing.BW17.Text = drx.Item(272)
                                form_print_hasil_packing.BW18.Text = drx.Item(273)
                                form_print_hasil_packing.BW19.Text = drx.Item(274)
                                form_print_hasil_packing.BW20.Text = drx.Item(275)
                                form_print_hasil_packing.BW21.Text = drx.Item(276)
                                form_print_hasil_packing.BW22.Text = drx.Item(277)
                                form_print_hasil_packing.BW23.Text = drx.Item(278)
                                form_print_hasil_packing.BW24.Text = drx.Item(279)
                                form_print_hasil_packing.BW25.Text = drx.Item(280)
                                form_print_hasil_packing.CW1.Text = drx.Item(281)
                                form_print_hasil_packing.CW2.Text = drx.Item(282)
                                form_print_hasil_packing.CW3.Text = drx.Item(283)
                                form_print_hasil_packing.CW4.Text = drx.Item(284)
                                form_print_hasil_packing.CW5.Text = drx.Item(285)
                                form_print_hasil_packing.CW6.Text = drx.Item(286)
                                form_print_hasil_packing.CW7.Text = drx.Item(287)
                                form_print_hasil_packing.CW8.Text = drx.Item(288)
                                form_print_hasil_packing.CW9.Text = drx.Item(289)
                                form_print_hasil_packing.CW10.Text = drx.Item(290)
                                form_print_hasil_packing.CW11.Text = drx.Item(291)
                                form_print_hasil_packing.CW12.Text = drx.Item(292)
                                form_print_hasil_packing.CW13.Text = drx.Item(293)
                                form_print_hasil_packing.CW14.Text = drx.Item(294)
                                form_print_hasil_packing.CW15.Text = drx.Item(295)
                                form_print_hasil_packing.CW16.Text = drx.Item(296)
                                form_print_hasil_packing.CW17.Text = drx.Item(297)
                                form_print_hasil_packing.CW18.Text = drx.Item(298)
                                form_print_hasil_packing.CW19.Text = drx.Item(299)
                                form_print_hasil_packing.CW20.Text = drx.Item(300)
                                form_print_hasil_packing.CW21.Text = drx.Item(301)
                                form_print_hasil_packing.CW22.Text = drx.Item(302)
                                form_print_hasil_packing.CW23.Text = drx.Item(303)
                                form_print_hasil_packing.CW24.Text = drx.Item(304)
                                form_print_hasil_packing.CW25.Text = drx.Item(305)
                                form_print_hasil_packing.DW1.Text = drx.Item(306)
                                form_print_hasil_packing.DW2.Text = drx.Item(307)
                                form_print_hasil_packing.DW3.Text = drx.Item(308)
                                form_print_hasil_packing.DW4.Text = drx.Item(309)
                                form_print_hasil_packing.DW5.Text = drx.Item(310)
                                form_print_hasil_packing.DW6.Text = drx.Item(311)
                                form_print_hasil_packing.DW7.Text = drx.Item(312)
                                form_print_hasil_packing.DW8.Text = drx.Item(313)
                                form_print_hasil_packing.DW9.Text = drx.Item(314)
                                form_print_hasil_packing.DW10.Text = drx.Item(315)
                                form_print_hasil_packing.DW11.Text = drx.Item(316)
                                form_print_hasil_packing.DW12.Text = drx.Item(317)
                                form_print_hasil_packing.DW13.Text = drx.Item(318)
                                form_print_hasil_packing.DW14.Text = drx.Item(319)
                                form_print_hasil_packing.DW15.Text = drx.Item(320)
                                form_print_hasil_packing.DW16.Text = drx.Item(321)
                                form_print_hasil_packing.DW17.Text = drx.Item(322)
                                form_print_hasil_packing.DW18.Text = drx.Item(323)
                                form_print_hasil_packing.DW19.Text = drx.Item(324)
                                form_print_hasil_packing.DW20.Text = drx.Item(325)
                                form_print_hasil_packing.DW21.Text = drx.Item(326)
                                form_print_hasil_packing.DW22.Text = drx.Item(327)
                                form_print_hasil_packing.DW23.Text = drx.Item(328)
                                form_print_hasil_packing.DW24.Text = drx.Item(329)
                                form_print_hasil_packing.DW25.Text = drx.Item(330)
                                form_print_hasil_packing.EW1.Text = drx.Item(331)
                                form_print_hasil_packing.EW2.Text = drx.Item(332)
                                form_print_hasil_packing.EW3.Text = drx.Item(333)
                                form_print_hasil_packing.EW4.Text = drx.Item(334)
                                form_print_hasil_packing.EW5.Text = drx.Item(335)
                                form_print_hasil_packing.EW6.Text = drx.Item(336)
                                form_print_hasil_packing.EW7.Text = drx.Item(337)
                                form_print_hasil_packing.EW8.Text = drx.Item(338)
                                form_print_hasil_packing.EW9.Text = drx.Item(339)
                                form_print_hasil_packing.EW10.Text = drx.Item(340)
                                form_print_hasil_packing.EW11.Text = drx.Item(341)
                                form_print_hasil_packing.EW12.Text = drx.Item(342)
                                form_print_hasil_packing.EW13.Text = drx.Item(343)
                                form_print_hasil_packing.EW14.Text = drx.Item(344)
                                form_print_hasil_packing.EW15.Text = drx.Item(345)
                                form_print_hasil_packing.EW16.Text = drx.Item(346)
                                form_print_hasil_packing.EW17.Text = drx.Item(347)
                                form_print_hasil_packing.EW18.Text = drx.Item(348)
                                form_print_hasil_packing.EW19.Text = drx.Item(349)
                                form_print_hasil_packing.EW20.Text = drx.Item(350)
                                form_print_hasil_packing.EW21.Text = drx.Item(351)
                                form_print_hasil_packing.EW22.Text = drx.Item(352)
                                form_print_hasil_packing.EW23.Text = drx.Item(353)
                                form_print_hasil_packing.EW24.Text = drx.Item(354)
                                form_print_hasil_packing.EW25.Text = drx.Item(355)
                                form_print_hasil_packing.S1.Text = drx.Item(356)
                                form_print_hasil_packing.S2.Text = drx.Item(357)
                                form_print_hasil_packing.S3.Text = drx.Item(358)
                                form_print_hasil_packing.S4.Text = drx.Item(359)
                                form_print_hasil_packing.S5.Text = drx.Item(360)
                                form_print_hasil_packing.S6.Text = drx.Item(361)
                                form_print_hasil_packing.S7.Text = drx.Item(362)
                                form_print_hasil_packing.S8.Text = drx.Item(363)
                                form_print_hasil_packing.S9.Text = drx.Item(364)
                                form_print_hasil_packing.S10.Text = drx.Item(365)
                                form_print_hasil_packing.S11.Text = drx.Item(366)
                                form_print_hasil_packing.S12.Text = drx.Item(367)
                                form_print_hasil_packing.S13.Text = drx.Item(368)
                                form_print_hasil_packing.S14.Text = drx.Item(369)
                                form_print_hasil_packing.S15.Text = drx.Item(370)
                                form_print_hasil_packing.S16.Text = drx.Item(371)
                                form_print_hasil_packing.S17.Text = drx.Item(372)
                                form_print_hasil_packing.S18.Text = drx.Item(373)
                                form_print_hasil_packing.S19.Text = drx.Item(374)
                                form_print_hasil_packing.S20.Text = drx.Item(375)
                                form_print_hasil_packing.S21.Text = drx.Item(376)
                                form_print_hasil_packing.S22.Text = drx.Item(377)
                                form_print_hasil_packing.S23.Text = drx.Item(378)
                                form_print_hasil_packing.S24.Text = drx.Item(379)
                                form_print_hasil_packing.S25.Text = drx.Item(380)
                                form_print_hasil_packing.ComboBox1.Text = drx.Item(384)
                            Else
                                MsgBox("Data Tidak Ditemukan Klik Tombol Perbarui Untuk Coba Lagi")
                            End If
                        End Using
                    End Using
                End Using
                txt_id_grey.Text = ""
                txt_sj_packing.Text = ""
                txt_baris_packing.Text = ""
                txt_gudang.Text = ""
                txt_customer.Text = ""
                txt_jenis_kain.Text = ""
                txt_warna.Text = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_keluar_sj_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_keluar_sj.TextChanged
        If txt_keluar_sj.Text <> String.Empty Then
            Dim temp As String = txt_keluar_sj.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_keluar_sj.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_keluar_sj.Select(txt_keluar_sj.Text.Length, 0)
            ElseIf txt_keluar_sj.Text = "-"c Then

            Else
                txt_keluar_sj.Text = CDec(temp).ToString("N0")
                txt_keluar_sj.Select(txt_keluar_sj.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_jumlah_total_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_total.TextChanged
        If txt_jumlah_total.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_total.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_total.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_total.Select(txt_jumlah_total.Text.Length, 0)
            ElseIf txt_jumlah_total.Text = "-"c Then

            Else
                txt_jumlah_total.Text = CDec(temp).ToString("N0")
                txt_jumlah_total.Select(txt_jumlah_total.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_belum_sj_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_belum_sj.TextChanged
        If txt_belum_sj.Text <> String.Empty Then
            Dim temp As String = txt_belum_sj.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_belum_sj.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_belum_sj.Select(txt_belum_sj.Text.Length, 0)
            ElseIf txt_belum_sj.Text = "-"c Then

            Else
                txt_belum_sj.Text = CDec(temp).ToString("N0")
                txt_belum_sj.Select(txt_belum_sj.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sudah_proses_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sudah_proses.TextChanged
        If txt_sudah_proses.Text <> String.Empty Then
            Dim temp As String = txt_sudah_proses.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sudah_proses.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sudah_proses.Select(txt_sudah_proses.Text.Length, 0)
            ElseIf txt_sudah_proses.Text = "-"c Then

            Else
                txt_sudah_proses.Text = CDec(temp).ToString("N0")
                txt_sudah_proses.Select(txt_sudah_proses.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_jumlah_claim_celup_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_claim_celup.TextChanged
        If txt_jumlah_claim_celup.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_claim_celup.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_claim_celup.Select(txt_jumlah_claim_celup.Text.Length, 0)
            ElseIf txt_jumlah_claim_celup.Text = "-"c Then

            Else
                txt_jumlah_claim_celup.Text = CDec(temp).ToString("N0")
                txt_jumlah_claim_celup.Select(txt_jumlah_claim_celup.Text.Length, 0)
            End If
        End If
    End Sub

End Class