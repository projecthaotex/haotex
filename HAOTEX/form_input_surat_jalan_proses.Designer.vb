﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_surat_jalan_proses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_input_surat_jalan_proses))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.dtp_jatuh_tempo = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_surat_jalan_proses = New System.Windows.Forms.TextBox()
        Me.dtp_tanggal = New System.Windows.Forms.DateTimePicker()
        Me.txt_gudang = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txt_harga_10 = New System.Windows.Forms.TextBox()
        Me.txt_harga_9 = New System.Windows.Forms.TextBox()
        Me.txt_harga_8 = New System.Windows.Forms.TextBox()
        Me.txt_harga_7 = New System.Windows.Forms.TextBox()
        Me.txt_harga_6 = New System.Windows.Forms.TextBox()
        Me.txt_harga_5 = New System.Windows.Forms.TextBox()
        Me.txt_harga_4 = New System.Windows.Forms.TextBox()
        Me.txt_harga_3 = New System.Windows.Forms.TextBox()
        Me.txt_harga_2 = New System.Windows.Forms.TextBox()
        Me.txt_harga_1 = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_id_grey_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_1 = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_qty_asal_10 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_9 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_8 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_7 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_6 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_5 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_4 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_3 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_2 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_1 = New System.Windows.Forms.TextBox()
        Me.Panel_10 = New System.Windows.Forms.Panel()
        Me.txt_meter_10 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_10 = New System.Windows.Forms.ComboBox()
        Me.txt_gudang_packing_10 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_10 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_10 = New System.Windows.Forms.TextBox()
        Me.txt_partai_10 = New System.Windows.Forms.TextBox()
        Me.txt_kiloan_10 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_10 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_10 = New System.Windows.Forms.TextBox()
        Me.txt_resep_10 = New System.Windows.Forms.TextBox()
        Me.txt_warna_10 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_10 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_10 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_10 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_10 = New System.Windows.Forms.Label()
        Me.Panel_9 = New System.Windows.Forms.Panel()
        Me.txt_meter_9 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_9 = New System.Windows.Forms.ComboBox()
        Me.txt_partai_9 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_packing_9 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_9 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_9 = New System.Windows.Forms.TextBox()
        Me.txt_kiloan_9 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_9 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_9 = New System.Windows.Forms.TextBox()
        Me.txt_resep_9 = New System.Windows.Forms.TextBox()
        Me.txt_warna_9 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_9 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_9 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_9 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_9 = New System.Windows.Forms.Label()
        Me.Panel_8 = New System.Windows.Forms.Panel()
        Me.txt_meter_8 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_8 = New System.Windows.Forms.ComboBox()
        Me.txt_gudang_packing_8 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_8 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_8 = New System.Windows.Forms.TextBox()
        Me.txt_kiloan_8 = New System.Windows.Forms.TextBox()
        Me.txt_partai_8 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_8 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_8 = New System.Windows.Forms.TextBox()
        Me.txt_resep_8 = New System.Windows.Forms.TextBox()
        Me.txt_warna_8 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_8 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_8 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_8 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_8 = New System.Windows.Forms.Label()
        Me.Panel_7 = New System.Windows.Forms.Panel()
        Me.txt_meter_7 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_7 = New System.Windows.Forms.ComboBox()
        Me.txt_kiloan_7 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_7 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_packing_7 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_7 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_7 = New System.Windows.Forms.TextBox()
        Me.txt_partai_7 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_7 = New System.Windows.Forms.TextBox()
        Me.txt_resep_7 = New System.Windows.Forms.TextBox()
        Me.txt_warna_7 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_7 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_7 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_7 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_7 = New System.Windows.Forms.Label()
        Me.Panel_6 = New System.Windows.Forms.Panel()
        Me.txt_meter_6 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_6 = New System.Windows.Forms.ComboBox()
        Me.txt_gudang_packing_6 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_6 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_6 = New System.Windows.Forms.TextBox()
        Me.txt_partai_6 = New System.Windows.Forms.TextBox()
        Me.txt_kiloan_6 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_6 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_6 = New System.Windows.Forms.TextBox()
        Me.txt_resep_6 = New System.Windows.Forms.TextBox()
        Me.txt_warna_6 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_6 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_6 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_6 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_6 = New System.Windows.Forms.Label()
        Me.Panel_5 = New System.Windows.Forms.Panel()
        Me.txt_meter_5 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_5 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_5 = New System.Windows.Forms.TextBox()
        Me.txt_kiloan_5 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_5 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_packing_5 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_5 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_5 = New System.Windows.Forms.TextBox()
        Me.txt_partai_5 = New System.Windows.Forms.TextBox()
        Me.txt_resep_5 = New System.Windows.Forms.TextBox()
        Me.txt_warna_5 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_5 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_5 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_5 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_5 = New System.Windows.Forms.Label()
        Me.Panel_4 = New System.Windows.Forms.Panel()
        Me.txt_meter_4 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_4 = New System.Windows.Forms.ComboBox()
        Me.txt_gudang_packing_4 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_4 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_4 = New System.Windows.Forms.TextBox()
        Me.txt_partai_4 = New System.Windows.Forms.TextBox()
        Me.txt_kiloan_4 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_4 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_4 = New System.Windows.Forms.TextBox()
        Me.txt_resep_4 = New System.Windows.Forms.TextBox()
        Me.txt_warna_4 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_4 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_4 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_4 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_4 = New System.Windows.Forms.Label()
        Me.Panel_3 = New System.Windows.Forms.Panel()
        Me.txt_meter_3 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_3 = New System.Windows.Forms.ComboBox()
        Me.txt_kiloan_3 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_3 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_3 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_packing_3 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_3 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_3 = New System.Windows.Forms.TextBox()
        Me.txt_partai_3 = New System.Windows.Forms.TextBox()
        Me.txt_resep_3 = New System.Windows.Forms.TextBox()
        Me.txt_warna_3 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_3 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_3 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_3 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_3 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel_1 = New System.Windows.Forms.Panel()
        Me.txt_meter_1 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_1 = New System.Windows.Forms.ComboBox()
        Me.txt_kiloan_1 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_1 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_1 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_packing_1 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_1 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_1 = New System.Windows.Forms.TextBox()
        Me.txt_partai_1 = New System.Windows.Forms.TextBox()
        Me.txt_resep_1 = New System.Windows.Forms.TextBox()
        Me.txt_warna_1 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_1 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_1 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_1 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_1 = New System.Windows.Forms.Label()
        Me.Panel_2 = New System.Windows.Forms.Panel()
        Me.txt_meter_2 = New System.Windows.Forms.TextBox()
        Me.cb_satuan_2 = New System.Windows.Forms.ComboBox()
        Me.txt_kiloan_2 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_2 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_2 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_packing_2 = New System.Windows.Forms.TextBox()
        Me.txt_gramasi_2 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_2 = New System.Windows.Forms.TextBox()
        Me.txt_partai_2 = New System.Windows.Forms.TextBox()
        Me.txt_resep_2 = New System.Windows.Forms.TextBox()
        Me.txt_warna_2 = New System.Windows.Forms.TextBox()
        Me.txt_harga_proses_2 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_2 = New System.Windows.Forms.TextBox()
        Me.txt_no_po_2 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_2 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btn_batal_10 = New System.Windows.Forms.Button()
        Me.btn_batal_9 = New System.Windows.Forms.Button()
        Me.btn_batal_8 = New System.Windows.Forms.Button()
        Me.btn_batal_7 = New System.Windows.Forms.Button()
        Me.btn_batal_6 = New System.Windows.Forms.Button()
        Me.btn_batal_5 = New System.Windows.Forms.Button()
        Me.btn_batal_4 = New System.Windows.Forms.Button()
        Me.btn_batal_3 = New System.Windows.Forms.Button()
        Me.btn_batal_2 = New System.Windows.Forms.Button()
        Me.btn_batal_1 = New System.Windows.Forms.Button()
        Me.btn_hapus_10 = New System.Windows.Forms.Button()
        Me.btn_selesai_10 = New System.Windows.Forms.Button()
        Me.btn_hapus_9 = New System.Windows.Forms.Button()
        Me.btn_selesai_9 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_9 = New System.Windows.Forms.Button()
        Me.btn_hapus_8 = New System.Windows.Forms.Button()
        Me.btn_selesai_8 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_8 = New System.Windows.Forms.Button()
        Me.btn_hapus_7 = New System.Windows.Forms.Button()
        Me.btn_selesai_7 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_7 = New System.Windows.Forms.Button()
        Me.btn_hapus_6 = New System.Windows.Forms.Button()
        Me.btn_selesai_6 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_6 = New System.Windows.Forms.Button()
        Me.btn_hapus_5 = New System.Windows.Forms.Button()
        Me.btn_selesai_5 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_5 = New System.Windows.Forms.Button()
        Me.btn_hapus_4 = New System.Windows.Forms.Button()
        Me.btn_selesai_4 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_4 = New System.Windows.Forms.Button()
        Me.btn_hapus_3 = New System.Windows.Forms.Button()
        Me.btn_selesai_3 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_3 = New System.Windows.Forms.Button()
        Me.btn_hapus_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_2 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_1 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_1 = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_sisa_qty_10 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_9 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_8 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_7 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_6 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_5 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_4 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_3 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_2 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty_1 = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txt_id_sj_proses_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_1 = New System.Windows.Forms.TextBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_id_po_proses_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_1 = New System.Windows.Forms.TextBox()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.txt_id_beli_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_9 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txt_id_beli_1 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_5 = New System.Windows.Forms.TextBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txt_asal_satuan_10 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_9 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_5 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_1 = New System.Windows.Forms.TextBox()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txt_id_hutang_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_1 = New System.Windows.Forms.TextBox()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txt_asal_satuan_wip_10 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_9 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_5 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_wip_1 = New System.Windows.Forms.TextBox()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txt_asal_satuan_rubah_10 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_9 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_5 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_rubah_1 = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel_10.SuspendLayout()
        Me.Panel_9.SuspendLayout()
        Me.Panel_8.SuspendLayout()
        Me.Panel_7.SuspendLayout()
        Me.Panel_6.SuspendLayout()
        Me.Panel_5.SuspendLayout()
        Me.Panel_4.SuspendLayout()
        Me.Panel_3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel_1.SuspendLayout()
        Me.Panel_2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel13.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.Panel15.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.Controls.Add(Me.Panel9)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Location = New System.Drawing.Point(-3, 7)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1342, 151)
        Me.Panel1.TabIndex = 45
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.ComboBox1)
        Me.Panel9.Controls.Add(Me.dtp_jatuh_tempo)
        Me.Panel9.Controls.Add(Me.Label1)
        Me.Panel9.Controls.Add(Me.txt_surat_jalan_proses)
        Me.Panel9.Controls.Add(Me.dtp_tanggal)
        Me.Panel9.Controls.Add(Me.txt_gudang)
        Me.Panel9.Controls.Add(Me.Label21)
        Me.Panel9.Controls.Add(Me.Label3)
        Me.Panel9.Controls.Add(Me.Label22)
        Me.Panel9.Location = New System.Drawing.Point(521, 30)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(300, 117)
        Me.Panel9.TabIndex = 79
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"15", "30", "45", "60", "75", "90", "105", "120", "135", "150", "165", "180", "195", "210", "225", "240", "255", "270", "285", "300", "315", "330", "345", "360", "375"})
        Me.ComboBox1.Location = New System.Drawing.Point(120, 85)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(59, 21)
        Me.ComboBox1.TabIndex = 76
        Me.ComboBox1.Text = "15"
        '
        'dtp_jatuh_tempo
        '
        Me.dtp_jatuh_tempo.Enabled = False
        Me.dtp_jatuh_tempo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo.Location = New System.Drawing.Point(185, 85)
        Me.dtp_jatuh_tempo.Name = "dtp_jatuh_tempo"
        Me.dtp_jatuh_tempo.Size = New System.Drawing.Size(99, 20)
        Me.dtp_jatuh_tempo.TabIndex = 74
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 87)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 16)
        Me.Label1.TabIndex = 75
        Me.Label1.Text = "Jatuh Tempo"
        '
        'txt_surat_jalan_proses
        '
        Me.txt_surat_jalan_proses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_surat_jalan_proses.Location = New System.Drawing.Point(121, 7)
        Me.txt_surat_jalan_proses.Name = "txt_surat_jalan_proses"
        Me.txt_surat_jalan_proses.Size = New System.Drawing.Size(164, 20)
        Me.txt_surat_jalan_proses.TabIndex = 0
        '
        'dtp_tanggal
        '
        Me.dtp_tanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal.Location = New System.Drawing.Point(120, 59)
        Me.dtp_tanggal.Name = "dtp_tanggal"
        Me.dtp_tanggal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_tanggal.TabIndex = 5
        '
        'txt_gudang
        '
        Me.txt_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang.Location = New System.Drawing.Point(121, 33)
        Me.txt_gudang.Name = "txt_gudang"
        Me.txt_gudang.Size = New System.Drawing.Size(164, 20)
        Me.txt_gudang.TabIndex = 4
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(14, 61)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(66, 16)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Tanggal"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(16, 35)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Gudang"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(15, 9)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(54, 16)
        Me.Label22.TabIndex = 25
        Me.Label22.Text = "No. SJ"
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label23.Font = New System.Drawing.Font("Cambria", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.Window
        Me.Label23.Location = New System.Drawing.Point(521, 4)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(300, 26)
        Me.Label23.TabIndex = 78
        Me.Label23.Text = "SURAT JALAN PROSES"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Label25)
        Me.Panel7.Controls.Add(Me.txt_harga_10)
        Me.Panel7.Controls.Add(Me.txt_harga_9)
        Me.Panel7.Controls.Add(Me.txt_harga_8)
        Me.Panel7.Controls.Add(Me.txt_harga_7)
        Me.Panel7.Controls.Add(Me.txt_harga_6)
        Me.Panel7.Controls.Add(Me.txt_harga_5)
        Me.Panel7.Controls.Add(Me.txt_harga_4)
        Me.Panel7.Controls.Add(Me.txt_harga_3)
        Me.Panel7.Controls.Add(Me.txt_harga_2)
        Me.Panel7.Controls.Add(Me.txt_harga_1)
        Me.Panel7.Location = New System.Drawing.Point(949, 1)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(117, 139)
        Me.Panel7.TabIndex = 61
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(40, 117)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(36, 13)
        Me.Label25.TabIndex = 11
        Me.Label25.Text = "Harga"
        '
        'txt_harga_10
        '
        Me.txt_harga_10.Location = New System.Drawing.Point(58, 94)
        Me.txt_harga_10.Name = "txt_harga_10"
        Me.txt_harga_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_10.TabIndex = 9
        '
        'txt_harga_9
        '
        Me.txt_harga_9.Location = New System.Drawing.Point(58, 74)
        Me.txt_harga_9.Name = "txt_harga_9"
        Me.txt_harga_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_9.TabIndex = 8
        '
        'txt_harga_8
        '
        Me.txt_harga_8.Location = New System.Drawing.Point(58, 54)
        Me.txt_harga_8.Name = "txt_harga_8"
        Me.txt_harga_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_8.TabIndex = 7
        '
        'txt_harga_7
        '
        Me.txt_harga_7.Location = New System.Drawing.Point(58, 34)
        Me.txt_harga_7.Name = "txt_harga_7"
        Me.txt_harga_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_7.TabIndex = 6
        '
        'txt_harga_6
        '
        Me.txt_harga_6.Location = New System.Drawing.Point(58, 14)
        Me.txt_harga_6.Name = "txt_harga_6"
        Me.txt_harga_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_6.TabIndex = 5
        '
        'txt_harga_5
        '
        Me.txt_harga_5.Location = New System.Drawing.Point(8, 94)
        Me.txt_harga_5.Name = "txt_harga_5"
        Me.txt_harga_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_5.TabIndex = 4
        '
        'txt_harga_4
        '
        Me.txt_harga_4.Location = New System.Drawing.Point(8, 74)
        Me.txt_harga_4.Name = "txt_harga_4"
        Me.txt_harga_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_4.TabIndex = 3
        '
        'txt_harga_3
        '
        Me.txt_harga_3.Location = New System.Drawing.Point(8, 54)
        Me.txt_harga_3.Name = "txt_harga_3"
        Me.txt_harga_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_3.TabIndex = 2
        '
        'txt_harga_2
        '
        Me.txt_harga_2.Location = New System.Drawing.Point(8, 34)
        Me.txt_harga_2.Name = "txt_harga_2"
        Me.txt_harga_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_2.TabIndex = 1
        '
        'txt_harga_1
        '
        Me.txt_harga_1.Location = New System.Drawing.Point(8, 14)
        Me.txt_harga_1.Name = "txt_harga_1"
        Me.txt_harga_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_1.TabIndex = 0
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label4)
        Me.Panel6.Controls.Add(Me.txt_id_grey_10)
        Me.Panel6.Controls.Add(Me.txt_id_grey_9)
        Me.Panel6.Controls.Add(Me.txt_id_grey_8)
        Me.Panel6.Controls.Add(Me.txt_id_grey_7)
        Me.Panel6.Controls.Add(Me.txt_id_grey_6)
        Me.Panel6.Controls.Add(Me.txt_id_grey_5)
        Me.Panel6.Controls.Add(Me.txt_id_grey_4)
        Me.Panel6.Controls.Add(Me.txt_id_grey_3)
        Me.Panel6.Controls.Add(Me.txt_id_grey_2)
        Me.Panel6.Controls.Add(Me.txt_id_grey_1)
        Me.Panel6.Location = New System.Drawing.Point(117, 2)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(114, 153)
        Me.Panel6.TabIndex = 60
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(36, 134)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(43, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "ID Grey"
        '
        'txt_id_grey_10
        '
        Me.txt_id_grey_10.Location = New System.Drawing.Point(57, 106)
        Me.txt_id_grey_10.Name = "txt_id_grey_10"
        Me.txt_id_grey_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_10.TabIndex = 9
        '
        'txt_id_grey_9
        '
        Me.txt_id_grey_9.Location = New System.Drawing.Point(57, 83)
        Me.txt_id_grey_9.Name = "txt_id_grey_9"
        Me.txt_id_grey_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_9.TabIndex = 8
        '
        'txt_id_grey_8
        '
        Me.txt_id_grey_8.Location = New System.Drawing.Point(57, 60)
        Me.txt_id_grey_8.Name = "txt_id_grey_8"
        Me.txt_id_grey_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_8.TabIndex = 7
        '
        'txt_id_grey_7
        '
        Me.txt_id_grey_7.Location = New System.Drawing.Point(57, 37)
        Me.txt_id_grey_7.Name = "txt_id_grey_7"
        Me.txt_id_grey_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_7.TabIndex = 6
        '
        'txt_id_grey_6
        '
        Me.txt_id_grey_6.Location = New System.Drawing.Point(57, 14)
        Me.txt_id_grey_6.Name = "txt_id_grey_6"
        Me.txt_id_grey_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_6.TabIndex = 5
        '
        'txt_id_grey_5
        '
        Me.txt_id_grey_5.Location = New System.Drawing.Point(7, 106)
        Me.txt_id_grey_5.Name = "txt_id_grey_5"
        Me.txt_id_grey_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_5.TabIndex = 4
        '
        'txt_id_grey_4
        '
        Me.txt_id_grey_4.Location = New System.Drawing.Point(7, 83)
        Me.txt_id_grey_4.Name = "txt_id_grey_4"
        Me.txt_id_grey_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_4.TabIndex = 3
        '
        'txt_id_grey_3
        '
        Me.txt_id_grey_3.Location = New System.Drawing.Point(7, 60)
        Me.txt_id_grey_3.Name = "txt_id_grey_3"
        Me.txt_id_grey_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_3.TabIndex = 2
        '
        'txt_id_grey_2
        '
        Me.txt_id_grey_2.Location = New System.Drawing.Point(7, 37)
        Me.txt_id_grey_2.Name = "txt_id_grey_2"
        Me.txt_id_grey_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_2.TabIndex = 1
        '
        'txt_id_grey_1
        '
        Me.txt_id_grey_1.Location = New System.Drawing.Point(7, 14)
        Me.txt_id_grey_1.Name = "txt_id_grey_1"
        Me.txt_id_grey_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_grey_1.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_10)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_9)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_8)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_7)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_6)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_5)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_4)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_3)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_2)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_1)
        Me.Panel5.Location = New System.Drawing.Point(2, 2)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(112, 153)
        Me.Panel5.TabIndex = 59
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 135)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "QTY Awal"
        '
        'txt_qty_asal_10
        '
        Me.txt_qty_asal_10.Location = New System.Drawing.Point(56, 106)
        Me.txt_qty_asal_10.Name = "txt_qty_asal_10"
        Me.txt_qty_asal_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_10.TabIndex = 9
        '
        'txt_qty_asal_9
        '
        Me.txt_qty_asal_9.Location = New System.Drawing.Point(56, 83)
        Me.txt_qty_asal_9.Name = "txt_qty_asal_9"
        Me.txt_qty_asal_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_9.TabIndex = 8
        '
        'txt_qty_asal_8
        '
        Me.txt_qty_asal_8.Location = New System.Drawing.Point(56, 60)
        Me.txt_qty_asal_8.Name = "txt_qty_asal_8"
        Me.txt_qty_asal_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_8.TabIndex = 7
        '
        'txt_qty_asal_7
        '
        Me.txt_qty_asal_7.Location = New System.Drawing.Point(56, 37)
        Me.txt_qty_asal_7.Name = "txt_qty_asal_7"
        Me.txt_qty_asal_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_7.TabIndex = 6
        '
        'txt_qty_asal_6
        '
        Me.txt_qty_asal_6.Location = New System.Drawing.Point(56, 14)
        Me.txt_qty_asal_6.Name = "txt_qty_asal_6"
        Me.txt_qty_asal_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_6.TabIndex = 5
        '
        'txt_qty_asal_5
        '
        Me.txt_qty_asal_5.Location = New System.Drawing.Point(6, 106)
        Me.txt_qty_asal_5.Name = "txt_qty_asal_5"
        Me.txt_qty_asal_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_5.TabIndex = 4
        '
        'txt_qty_asal_4
        '
        Me.txt_qty_asal_4.Location = New System.Drawing.Point(6, 83)
        Me.txt_qty_asal_4.Name = "txt_qty_asal_4"
        Me.txt_qty_asal_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_4.TabIndex = 3
        '
        'txt_qty_asal_3
        '
        Me.txt_qty_asal_3.Location = New System.Drawing.Point(6, 60)
        Me.txt_qty_asal_3.Name = "txt_qty_asal_3"
        Me.txt_qty_asal_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_3.TabIndex = 2
        '
        'txt_qty_asal_2
        '
        Me.txt_qty_asal_2.Location = New System.Drawing.Point(6, 37)
        Me.txt_qty_asal_2.Name = "txt_qty_asal_2"
        Me.txt_qty_asal_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_2.TabIndex = 1
        '
        'txt_qty_asal_1
        '
        Me.txt_qty_asal_1.Location = New System.Drawing.Point(6, 14)
        Me.txt_qty_asal_1.Name = "txt_qty_asal_1"
        Me.txt_qty_asal_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_1.TabIndex = 0
        '
        'Panel_10
        '
        Me.Panel_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_10.Controls.Add(Me.txt_meter_10)
        Me.Panel_10.Controls.Add(Me.cb_satuan_10)
        Me.Panel_10.Controls.Add(Me.txt_gudang_packing_10)
        Me.Panel_10.Controls.Add(Me.txt_gramasi_10)
        Me.Panel_10.Controls.Add(Me.txt_gulung_10)
        Me.Panel_10.Controls.Add(Me.txt_partai_10)
        Me.Panel_10.Controls.Add(Me.txt_kiloan_10)
        Me.Panel_10.Controls.Add(Me.txt_total_harga_10)
        Me.Panel_10.Controls.Add(Me.txt_keterangan_10)
        Me.Panel_10.Controls.Add(Me.txt_resep_10)
        Me.Panel_10.Controls.Add(Me.txt_warna_10)
        Me.Panel_10.Controls.Add(Me.txt_harga_proses_10)
        Me.Panel_10.Controls.Add(Me.txt_jenis_kain_10)
        Me.Panel_10.Controls.Add(Me.txt_no_po_10)
        Me.Panel_10.Controls.Add(Me.txt_no_urut_10)
        Me.Panel_10.Location = New System.Drawing.Point(-3, 508)
        Me.Panel_10.Name = "Panel_10"
        Me.Panel_10.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_10.TabIndex = 55
        '
        'txt_meter_10
        '
        Me.txt_meter_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_10.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_10.Name = "txt_meter_10"
        Me.txt_meter_10.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_10.TabIndex = 84
        Me.txt_meter_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_10
        '
        Me.cb_satuan_10.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_10.FormattingEnabled = True
        Me.cb_satuan_10.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_10.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_10.Name = "cb_satuan_10"
        Me.cb_satuan_10.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_10.TabIndex = 506
        Me.cb_satuan_10.Text = "Meter"
        '
        'txt_gudang_packing_10
        '
        Me.txt_gudang_packing_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_10.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_10.Name = "txt_gudang_packing_10"
        Me.txt_gudang_packing_10.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_10.TabIndex = 90
        '
        'txt_gramasi_10
        '
        Me.txt_gramasi_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_10.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_10.Name = "txt_gramasi_10"
        Me.txt_gramasi_10.ReadOnly = True
        Me.txt_gramasi_10.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_10.TabIndex = 90
        Me.txt_gramasi_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_10
        '
        Me.txt_gulung_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_10.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_10.Name = "txt_gulung_10"
        Me.txt_gulung_10.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_10.TabIndex = 90
        Me.txt_gulung_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_10
        '
        Me.txt_partai_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_10.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_10.Name = "txt_partai_10"
        Me.txt_partai_10.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_10.TabIndex = 90
        '
        'txt_kiloan_10
        '
        Me.txt_kiloan_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_10.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_10.Name = "txt_kiloan_10"
        Me.txt_kiloan_10.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_10.TabIndex = 9
        Me.txt_kiloan_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_10
        '
        Me.txt_total_harga_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_10.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_10.Name = "txt_total_harga_10"
        Me.txt_total_harga_10.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_10.TabIndex = 8
        Me.txt_total_harga_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_10
        '
        Me.txt_keterangan_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_10.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_10.Name = "txt_keterangan_10"
        Me.txt_keterangan_10.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_10.TabIndex = 7
        '
        'txt_resep_10
        '
        Me.txt_resep_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_10.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_10.Name = "txt_resep_10"
        Me.txt_resep_10.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_10.TabIndex = 5
        '
        'txt_warna_10
        '
        Me.txt_warna_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_10.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_10.Name = "txt_warna_10"
        Me.txt_warna_10.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_10.TabIndex = 4
        '
        'txt_harga_proses_10
        '
        Me.txt_harga_proses_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_10.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_10.Name = "txt_harga_proses_10"
        Me.txt_harga_proses_10.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_10.TabIndex = 3
        Me.txt_harga_proses_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_10
        '
        Me.txt_jenis_kain_10.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_10.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_10.Name = "txt_jenis_kain_10"
        Me.txt_jenis_kain_10.ReadOnly = True
        Me.txt_jenis_kain_10.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_10.TabIndex = 2
        '
        'txt_no_po_10
        '
        Me.txt_no_po_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_10.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_10.Name = "txt_no_po_10"
        Me.txt_no_po_10.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_10.TabIndex = 1
        '
        'txt_no_urut_10
        '
        Me.txt_no_urut_10.AutoSize = True
        Me.txt_no_urut_10.Location = New System.Drawing.Point(14, 11)
        Me.txt_no_urut_10.Name = "txt_no_urut_10"
        Me.txt_no_urut_10.Size = New System.Drawing.Size(19, 13)
        Me.txt_no_urut_10.TabIndex = 0
        Me.txt_no_urut_10.Text = "10"
        '
        'Panel_9
        '
        Me.Panel_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_9.Controls.Add(Me.txt_meter_9)
        Me.Panel_9.Controls.Add(Me.cb_satuan_9)
        Me.Panel_9.Controls.Add(Me.txt_partai_9)
        Me.Panel_9.Controls.Add(Me.txt_gudang_packing_9)
        Me.Panel_9.Controls.Add(Me.txt_gramasi_9)
        Me.Panel_9.Controls.Add(Me.txt_gulung_9)
        Me.Panel_9.Controls.Add(Me.txt_kiloan_9)
        Me.Panel_9.Controls.Add(Me.txt_total_harga_9)
        Me.Panel_9.Controls.Add(Me.txt_keterangan_9)
        Me.Panel_9.Controls.Add(Me.txt_resep_9)
        Me.Panel_9.Controls.Add(Me.txt_warna_9)
        Me.Panel_9.Controls.Add(Me.txt_harga_proses_9)
        Me.Panel_9.Controls.Add(Me.txt_jenis_kain_9)
        Me.Panel_9.Controls.Add(Me.txt_no_po_9)
        Me.Panel_9.Controls.Add(Me.txt_no_urut_9)
        Me.Panel_9.Location = New System.Drawing.Point(-3, 473)
        Me.Panel_9.Name = "Panel_9"
        Me.Panel_9.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_9.TabIndex = 54
        '
        'txt_meter_9
        '
        Me.txt_meter_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_9.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_9.Name = "txt_meter_9"
        Me.txt_meter_9.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_9.TabIndex = 88
        Me.txt_meter_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_9
        '
        Me.cb_satuan_9.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_9.FormattingEnabled = True
        Me.cb_satuan_9.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_9.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_9.Name = "cb_satuan_9"
        Me.cb_satuan_9.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_9.TabIndex = 507
        Me.cb_satuan_9.Text = "Meter"
        '
        'txt_partai_9
        '
        Me.txt_partai_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_9.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_9.Name = "txt_partai_9"
        Me.txt_partai_9.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_9.TabIndex = 91
        '
        'txt_gudang_packing_9
        '
        Me.txt_gudang_packing_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_9.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_9.Name = "txt_gudang_packing_9"
        Me.txt_gudang_packing_9.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_9.TabIndex = 84
        '
        'txt_gramasi_9
        '
        Me.txt_gramasi_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_9.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_9.Name = "txt_gramasi_9"
        Me.txt_gramasi_9.ReadOnly = True
        Me.txt_gramasi_9.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_9.TabIndex = 84
        Me.txt_gramasi_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_9
        '
        Me.txt_gulung_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_9.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_9.Name = "txt_gulung_9"
        Me.txt_gulung_9.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_9.TabIndex = 84
        Me.txt_gulung_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_kiloan_9
        '
        Me.txt_kiloan_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_9.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_9.Name = "txt_kiloan_9"
        Me.txt_kiloan_9.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_9.TabIndex = 9
        Me.txt_kiloan_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_9
        '
        Me.txt_total_harga_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_9.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_9.Name = "txt_total_harga_9"
        Me.txt_total_harga_9.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_9.TabIndex = 8
        Me.txt_total_harga_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_9
        '
        Me.txt_keterangan_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_9.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_9.Name = "txt_keterangan_9"
        Me.txt_keterangan_9.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_9.TabIndex = 7
        '
        'txt_resep_9
        '
        Me.txt_resep_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_9.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_9.Name = "txt_resep_9"
        Me.txt_resep_9.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_9.TabIndex = 5
        '
        'txt_warna_9
        '
        Me.txt_warna_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_9.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_9.Name = "txt_warna_9"
        Me.txt_warna_9.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_9.TabIndex = 4
        '
        'txt_harga_proses_9
        '
        Me.txt_harga_proses_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_9.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_9.Name = "txt_harga_proses_9"
        Me.txt_harga_proses_9.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_9.TabIndex = 3
        Me.txt_harga_proses_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_9
        '
        Me.txt_jenis_kain_9.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_9.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_9.Name = "txt_jenis_kain_9"
        Me.txt_jenis_kain_9.ReadOnly = True
        Me.txt_jenis_kain_9.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_9.TabIndex = 2
        '
        'txt_no_po_9
        '
        Me.txt_no_po_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_9.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_9.Name = "txt_no_po_9"
        Me.txt_no_po_9.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_9.TabIndex = 1
        '
        'txt_no_urut_9
        '
        Me.txt_no_urut_9.AutoSize = True
        Me.txt_no_urut_9.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_9.Name = "txt_no_urut_9"
        Me.txt_no_urut_9.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_9.TabIndex = 0
        Me.txt_no_urut_9.Text = "9"
        '
        'Panel_8
        '
        Me.Panel_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_8.Controls.Add(Me.txt_meter_8)
        Me.Panel_8.Controls.Add(Me.cb_satuan_8)
        Me.Panel_8.Controls.Add(Me.txt_gudang_packing_8)
        Me.Panel_8.Controls.Add(Me.txt_gramasi_8)
        Me.Panel_8.Controls.Add(Me.txt_gulung_8)
        Me.Panel_8.Controls.Add(Me.txt_kiloan_8)
        Me.Panel_8.Controls.Add(Me.txt_partai_8)
        Me.Panel_8.Controls.Add(Me.txt_total_harga_8)
        Me.Panel_8.Controls.Add(Me.txt_keterangan_8)
        Me.Panel_8.Controls.Add(Me.txt_resep_8)
        Me.Panel_8.Controls.Add(Me.txt_warna_8)
        Me.Panel_8.Controls.Add(Me.txt_harga_proses_8)
        Me.Panel_8.Controls.Add(Me.txt_jenis_kain_8)
        Me.Panel_8.Controls.Add(Me.txt_no_po_8)
        Me.Panel_8.Controls.Add(Me.txt_no_urut_8)
        Me.Panel_8.Location = New System.Drawing.Point(-3, 438)
        Me.Panel_8.Name = "Panel_8"
        Me.Panel_8.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_8.TabIndex = 56
        '
        'txt_meter_8
        '
        Me.txt_meter_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_8.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_8.Name = "txt_meter_8"
        Me.txt_meter_8.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_8.TabIndex = 87
        Me.txt_meter_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_8
        '
        Me.cb_satuan_8.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_8.FormattingEnabled = True
        Me.cb_satuan_8.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_8.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_8.Name = "cb_satuan_8"
        Me.cb_satuan_8.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_8.TabIndex = 509
        Me.cb_satuan_8.Text = "Meter"
        '
        'txt_gudang_packing_8
        '
        Me.txt_gudang_packing_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_8.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_8.Name = "txt_gudang_packing_8"
        Me.txt_gudang_packing_8.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_8.TabIndex = 91
        '
        'txt_gramasi_8
        '
        Me.txt_gramasi_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_8.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_8.Name = "txt_gramasi_8"
        Me.txt_gramasi_8.ReadOnly = True
        Me.txt_gramasi_8.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_8.TabIndex = 91
        Me.txt_gramasi_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_8
        '
        Me.txt_gulung_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_8.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_8.Name = "txt_gulung_8"
        Me.txt_gulung_8.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_8.TabIndex = 85
        Me.txt_gulung_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_kiloan_8
        '
        Me.txt_kiloan_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_8.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_8.Name = "txt_kiloan_8"
        Me.txt_kiloan_8.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_8.TabIndex = 9
        Me.txt_kiloan_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_8
        '
        Me.txt_partai_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_8.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_8.Name = "txt_partai_8"
        Me.txt_partai_8.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_8.TabIndex = 84
        '
        'txt_total_harga_8
        '
        Me.txt_total_harga_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_8.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_8.Name = "txt_total_harga_8"
        Me.txt_total_harga_8.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_8.TabIndex = 8
        Me.txt_total_harga_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_8
        '
        Me.txt_keterangan_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_8.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_8.Name = "txt_keterangan_8"
        Me.txt_keterangan_8.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_8.TabIndex = 7
        '
        'txt_resep_8
        '
        Me.txt_resep_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_8.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_8.Name = "txt_resep_8"
        Me.txt_resep_8.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_8.TabIndex = 5
        '
        'txt_warna_8
        '
        Me.txt_warna_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_8.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_8.Name = "txt_warna_8"
        Me.txt_warna_8.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_8.TabIndex = 4
        '
        'txt_harga_proses_8
        '
        Me.txt_harga_proses_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_8.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_8.Name = "txt_harga_proses_8"
        Me.txt_harga_proses_8.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_8.TabIndex = 3
        Me.txt_harga_proses_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_8
        '
        Me.txt_jenis_kain_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_8.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_8.Name = "txt_jenis_kain_8"
        Me.txt_jenis_kain_8.ReadOnly = True
        Me.txt_jenis_kain_8.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_8.TabIndex = 2
        '
        'txt_no_po_8
        '
        Me.txt_no_po_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_8.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_8.Name = "txt_no_po_8"
        Me.txt_no_po_8.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_8.TabIndex = 1
        '
        'txt_no_urut_8
        '
        Me.txt_no_urut_8.AutoSize = True
        Me.txt_no_urut_8.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_8.Name = "txt_no_urut_8"
        Me.txt_no_urut_8.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_8.TabIndex = 0
        Me.txt_no_urut_8.Text = "8"
        '
        'Panel_7
        '
        Me.Panel_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_7.Controls.Add(Me.txt_meter_7)
        Me.Panel_7.Controls.Add(Me.cb_satuan_7)
        Me.Panel_7.Controls.Add(Me.txt_kiloan_7)
        Me.Panel_7.Controls.Add(Me.txt_gulung_7)
        Me.Panel_7.Controls.Add(Me.txt_gudang_packing_7)
        Me.Panel_7.Controls.Add(Me.txt_gramasi_7)
        Me.Panel_7.Controls.Add(Me.txt_total_harga_7)
        Me.Panel_7.Controls.Add(Me.txt_partai_7)
        Me.Panel_7.Controls.Add(Me.txt_keterangan_7)
        Me.Panel_7.Controls.Add(Me.txt_resep_7)
        Me.Panel_7.Controls.Add(Me.txt_warna_7)
        Me.Panel_7.Controls.Add(Me.txt_harga_proses_7)
        Me.Panel_7.Controls.Add(Me.txt_jenis_kain_7)
        Me.Panel_7.Controls.Add(Me.txt_no_po_7)
        Me.Panel_7.Controls.Add(Me.txt_no_urut_7)
        Me.Panel_7.Location = New System.Drawing.Point(-3, 403)
        Me.Panel_7.Name = "Panel_7"
        Me.Panel_7.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_7.TabIndex = 53
        '
        'txt_meter_7
        '
        Me.txt_meter_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_7.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_7.Name = "txt_meter_7"
        Me.txt_meter_7.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_7.TabIndex = 86
        Me.txt_meter_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_7
        '
        Me.cb_satuan_7.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_7.FormattingEnabled = True
        Me.cb_satuan_7.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_7.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_7.Name = "cb_satuan_7"
        Me.cb_satuan_7.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_7.TabIndex = 508
        Me.cb_satuan_7.Text = "Meter"
        '
        'txt_kiloan_7
        '
        Me.txt_kiloan_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_7.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_7.Name = "txt_kiloan_7"
        Me.txt_kiloan_7.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_7.TabIndex = 9
        Me.txt_kiloan_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_7
        '
        Me.txt_gulung_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_7.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_7.Name = "txt_gulung_7"
        Me.txt_gulung_7.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_7.TabIndex = 86
        Me.txt_gulung_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gudang_packing_7
        '
        Me.txt_gudang_packing_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_7.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_7.Name = "txt_gudang_packing_7"
        Me.txt_gudang_packing_7.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_7.TabIndex = 85
        '
        'txt_gramasi_7
        '
        Me.txt_gramasi_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_7.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_7.Name = "txt_gramasi_7"
        Me.txt_gramasi_7.ReadOnly = True
        Me.txt_gramasi_7.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_7.TabIndex = 85
        Me.txt_gramasi_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_7
        '
        Me.txt_total_harga_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_7.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_7.Name = "txt_total_harga_7"
        Me.txt_total_harga_7.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_7.TabIndex = 8
        Me.txt_total_harga_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_7
        '
        Me.txt_partai_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_7.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_7.Name = "txt_partai_7"
        Me.txt_partai_7.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_7.TabIndex = 85
        '
        'txt_keterangan_7
        '
        Me.txt_keterangan_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_7.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_7.Name = "txt_keterangan_7"
        Me.txt_keterangan_7.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_7.TabIndex = 7
        '
        'txt_resep_7
        '
        Me.txt_resep_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_7.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_7.Name = "txt_resep_7"
        Me.txt_resep_7.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_7.TabIndex = 5
        '
        'txt_warna_7
        '
        Me.txt_warna_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_7.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_7.Name = "txt_warna_7"
        Me.txt_warna_7.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_7.TabIndex = 4
        '
        'txt_harga_proses_7
        '
        Me.txt_harga_proses_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_7.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_7.Name = "txt_harga_proses_7"
        Me.txt_harga_proses_7.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_7.TabIndex = 3
        Me.txt_harga_proses_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_7
        '
        Me.txt_jenis_kain_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_7.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_7.Name = "txt_jenis_kain_7"
        Me.txt_jenis_kain_7.ReadOnly = True
        Me.txt_jenis_kain_7.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_7.TabIndex = 2
        '
        'txt_no_po_7
        '
        Me.txt_no_po_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_7.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_7.Name = "txt_no_po_7"
        Me.txt_no_po_7.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_7.TabIndex = 1
        '
        'txt_no_urut_7
        '
        Me.txt_no_urut_7.AutoSize = True
        Me.txt_no_urut_7.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_7.Name = "txt_no_urut_7"
        Me.txt_no_urut_7.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_7.TabIndex = 0
        Me.txt_no_urut_7.Text = "7"
        '
        'Panel_6
        '
        Me.Panel_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_6.Controls.Add(Me.txt_meter_6)
        Me.Panel_6.Controls.Add(Me.cb_satuan_6)
        Me.Panel_6.Controls.Add(Me.txt_gudang_packing_6)
        Me.Panel_6.Controls.Add(Me.txt_gramasi_6)
        Me.Panel_6.Controls.Add(Me.txt_gulung_6)
        Me.Panel_6.Controls.Add(Me.txt_partai_6)
        Me.Panel_6.Controls.Add(Me.txt_kiloan_6)
        Me.Panel_6.Controls.Add(Me.txt_total_harga_6)
        Me.Panel_6.Controls.Add(Me.txt_keterangan_6)
        Me.Panel_6.Controls.Add(Me.txt_resep_6)
        Me.Panel_6.Controls.Add(Me.txt_warna_6)
        Me.Panel_6.Controls.Add(Me.txt_harga_proses_6)
        Me.Panel_6.Controls.Add(Me.txt_jenis_kain_6)
        Me.Panel_6.Controls.Add(Me.txt_no_po_6)
        Me.Panel_6.Controls.Add(Me.txt_no_urut_6)
        Me.Panel_6.Location = New System.Drawing.Point(-3, 368)
        Me.Panel_6.Name = "Panel_6"
        Me.Panel_6.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_6.TabIndex = 52
        '
        'txt_meter_6
        '
        Me.txt_meter_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_6.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_6.Name = "txt_meter_6"
        Me.txt_meter_6.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_6.TabIndex = 89
        Me.txt_meter_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_6
        '
        Me.cb_satuan_6.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_6.FormattingEnabled = True
        Me.cb_satuan_6.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_6.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_6.Name = "cb_satuan_6"
        Me.cb_satuan_6.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_6.TabIndex = 511
        Me.cb_satuan_6.Text = "Meter"
        '
        'txt_gudang_packing_6
        '
        Me.txt_gudang_packing_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_6.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_6.Name = "txt_gudang_packing_6"
        Me.txt_gudang_packing_6.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_6.TabIndex = 92
        '
        'txt_gramasi_6
        '
        Me.txt_gramasi_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_6.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_6.Name = "txt_gramasi_6"
        Me.txt_gramasi_6.ReadOnly = True
        Me.txt_gramasi_6.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_6.TabIndex = 92
        Me.txt_gramasi_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_6
        '
        Me.txt_gulung_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_6.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_6.Name = "txt_gulung_6"
        Me.txt_gulung_6.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_6.TabIndex = 91
        Me.txt_gulung_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_6
        '
        Me.txt_partai_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_6.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_6.Name = "txt_partai_6"
        Me.txt_partai_6.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_6.TabIndex = 92
        '
        'txt_kiloan_6
        '
        Me.txt_kiloan_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_6.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_6.Name = "txt_kiloan_6"
        Me.txt_kiloan_6.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_6.TabIndex = 9
        Me.txt_kiloan_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_6
        '
        Me.txt_total_harga_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_6.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_6.Name = "txt_total_harga_6"
        Me.txt_total_harga_6.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_6.TabIndex = 8
        Me.txt_total_harga_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_6
        '
        Me.txt_keterangan_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_6.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_6.Name = "txt_keterangan_6"
        Me.txt_keterangan_6.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_6.TabIndex = 7
        '
        'txt_resep_6
        '
        Me.txt_resep_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_6.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_6.Name = "txt_resep_6"
        Me.txt_resep_6.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_6.TabIndex = 5
        '
        'txt_warna_6
        '
        Me.txt_warna_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_6.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_6.Name = "txt_warna_6"
        Me.txt_warna_6.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_6.TabIndex = 4
        '
        'txt_harga_proses_6
        '
        Me.txt_harga_proses_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_6.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_6.Name = "txt_harga_proses_6"
        Me.txt_harga_proses_6.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_6.TabIndex = 3
        Me.txt_harga_proses_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_6
        '
        Me.txt_jenis_kain_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_6.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_6.Name = "txt_jenis_kain_6"
        Me.txt_jenis_kain_6.ReadOnly = True
        Me.txt_jenis_kain_6.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_6.TabIndex = 2
        '
        'txt_no_po_6
        '
        Me.txt_no_po_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_6.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_6.Name = "txt_no_po_6"
        Me.txt_no_po_6.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_6.TabIndex = 1
        '
        'txt_no_urut_6
        '
        Me.txt_no_urut_6.AutoSize = True
        Me.txt_no_urut_6.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_6.Name = "txt_no_urut_6"
        Me.txt_no_urut_6.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_6.TabIndex = 0
        Me.txt_no_urut_6.Text = "6"
        '
        'Panel_5
        '
        Me.Panel_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_5.Controls.Add(Me.txt_meter_5)
        Me.Panel_5.Controls.Add(Me.cb_satuan_5)
        Me.Panel_5.Controls.Add(Me.txt_gulung_5)
        Me.Panel_5.Controls.Add(Me.txt_kiloan_5)
        Me.Panel_5.Controls.Add(Me.txt_total_harga_5)
        Me.Panel_5.Controls.Add(Me.txt_gudang_packing_5)
        Me.Panel_5.Controls.Add(Me.txt_gramasi_5)
        Me.Panel_5.Controls.Add(Me.txt_keterangan_5)
        Me.Panel_5.Controls.Add(Me.txt_partai_5)
        Me.Panel_5.Controls.Add(Me.txt_resep_5)
        Me.Panel_5.Controls.Add(Me.txt_warna_5)
        Me.Panel_5.Controls.Add(Me.txt_harga_proses_5)
        Me.Panel_5.Controls.Add(Me.txt_jenis_kain_5)
        Me.Panel_5.Controls.Add(Me.txt_no_po_5)
        Me.Panel_5.Controls.Add(Me.txt_no_urut_5)
        Me.Panel_5.Location = New System.Drawing.Point(-3, 333)
        Me.Panel_5.Name = "Panel_5"
        Me.Panel_5.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_5.TabIndex = 51
        '
        'txt_meter_5
        '
        Me.txt_meter_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_5.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_5.Name = "txt_meter_5"
        Me.txt_meter_5.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_5.TabIndex = 80
        Me.txt_meter_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_5
        '
        Me.cb_satuan_5.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_5.FormattingEnabled = True
        Me.cb_satuan_5.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_5.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_5.Name = "cb_satuan_5"
        Me.cb_satuan_5.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_5.TabIndex = 502
        Me.cb_satuan_5.Text = "Meter"
        '
        'txt_gulung_5
        '
        Me.txt_gulung_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_5.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_5.Name = "txt_gulung_5"
        Me.txt_gulung_5.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_5.TabIndex = 92
        Me.txt_gulung_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_kiloan_5
        '
        Me.txt_kiloan_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_5.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_5.Name = "txt_kiloan_5"
        Me.txt_kiloan_5.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_5.TabIndex = 9
        Me.txt_kiloan_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_5
        '
        Me.txt_total_harga_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_5.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_5.Name = "txt_total_harga_5"
        Me.txt_total_harga_5.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_5.TabIndex = 8
        Me.txt_total_harga_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gudang_packing_5
        '
        Me.txt_gudang_packing_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_5.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_5.Name = "txt_gudang_packing_5"
        Me.txt_gudang_packing_5.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_5.TabIndex = 86
        '
        'txt_gramasi_5
        '
        Me.txt_gramasi_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_5.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_5.Name = "txt_gramasi_5"
        Me.txt_gramasi_5.ReadOnly = True
        Me.txt_gramasi_5.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_5.TabIndex = 86
        Me.txt_gramasi_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_5
        '
        Me.txt_keterangan_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_5.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_5.Name = "txt_keterangan_5"
        Me.txt_keterangan_5.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_5.TabIndex = 7
        '
        'txt_partai_5
        '
        Me.txt_partai_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_5.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_5.Name = "txt_partai_5"
        Me.txt_partai_5.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_5.TabIndex = 86
        '
        'txt_resep_5
        '
        Me.txt_resep_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_5.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_5.Name = "txt_resep_5"
        Me.txt_resep_5.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_5.TabIndex = 5
        '
        'txt_warna_5
        '
        Me.txt_warna_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_5.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_5.Name = "txt_warna_5"
        Me.txt_warna_5.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_5.TabIndex = 4
        '
        'txt_harga_proses_5
        '
        Me.txt_harga_proses_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_5.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_5.Name = "txt_harga_proses_5"
        Me.txt_harga_proses_5.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_5.TabIndex = 3
        Me.txt_harga_proses_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_5
        '
        Me.txt_jenis_kain_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_5.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_5.Name = "txt_jenis_kain_5"
        Me.txt_jenis_kain_5.ReadOnly = True
        Me.txt_jenis_kain_5.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_5.TabIndex = 2
        '
        'txt_no_po_5
        '
        Me.txt_no_po_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_5.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_5.Name = "txt_no_po_5"
        Me.txt_no_po_5.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_5.TabIndex = 1
        '
        'txt_no_urut_5
        '
        Me.txt_no_urut_5.AutoSize = True
        Me.txt_no_urut_5.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_5.Name = "txt_no_urut_5"
        Me.txt_no_urut_5.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_5.TabIndex = 0
        Me.txt_no_urut_5.Text = "5"
        '
        'Panel_4
        '
        Me.Panel_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_4.Controls.Add(Me.txt_meter_4)
        Me.Panel_4.Controls.Add(Me.cb_satuan_4)
        Me.Panel_4.Controls.Add(Me.txt_gudang_packing_4)
        Me.Panel_4.Controls.Add(Me.txt_gramasi_4)
        Me.Panel_4.Controls.Add(Me.txt_gulung_4)
        Me.Panel_4.Controls.Add(Me.txt_partai_4)
        Me.Panel_4.Controls.Add(Me.txt_kiloan_4)
        Me.Panel_4.Controls.Add(Me.txt_total_harga_4)
        Me.Panel_4.Controls.Add(Me.txt_keterangan_4)
        Me.Panel_4.Controls.Add(Me.txt_resep_4)
        Me.Panel_4.Controls.Add(Me.txt_warna_4)
        Me.Panel_4.Controls.Add(Me.txt_harga_proses_4)
        Me.Panel_4.Controls.Add(Me.txt_jenis_kain_4)
        Me.Panel_4.Controls.Add(Me.txt_no_po_4)
        Me.Panel_4.Controls.Add(Me.txt_no_urut_4)
        Me.Panel_4.Location = New System.Drawing.Point(-3, 298)
        Me.Panel_4.Name = "Panel_4"
        Me.Panel_4.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_4.TabIndex = 50
        '
        'txt_meter_4
        '
        Me.txt_meter_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_meter_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_4.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_4.Name = "txt_meter_4"
        Me.txt_meter_4.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_4.TabIndex = 83
        Me.txt_meter_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_4
        '
        Me.cb_satuan_4.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_4.FormattingEnabled = True
        Me.cb_satuan_4.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_4.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_4.Name = "cb_satuan_4"
        Me.cb_satuan_4.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_4.TabIndex = 505
        Me.cb_satuan_4.Text = "Meter"
        '
        'txt_gudang_packing_4
        '
        Me.txt_gudang_packing_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_packing_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_4.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_4.Name = "txt_gudang_packing_4"
        Me.txt_gudang_packing_4.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_4.TabIndex = 93
        '
        'txt_gramasi_4
        '
        Me.txt_gramasi_4.BackColor = System.Drawing.SystemColors.Control
        Me.txt_gramasi_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_4.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_4.Name = "txt_gramasi_4"
        Me.txt_gramasi_4.ReadOnly = True
        Me.txt_gramasi_4.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_4.TabIndex = 93
        Me.txt_gramasi_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_4
        '
        Me.txt_gulung_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gulung_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_4.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_4.Name = "txt_gulung_4"
        Me.txt_gulung_4.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_4.TabIndex = 93
        Me.txt_gulung_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_4
        '
        Me.txt_partai_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_partai_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_4.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_4.Name = "txt_partai_4"
        Me.txt_partai_4.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_4.TabIndex = 93
        '
        'txt_kiloan_4
        '
        Me.txt_kiloan_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_kiloan_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_4.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_4.Name = "txt_kiloan_4"
        Me.txt_kiloan_4.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_4.TabIndex = 9
        Me.txt_kiloan_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_4
        '
        Me.txt_total_harga_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_harga_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_4.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_4.Name = "txt_total_harga_4"
        Me.txt_total_harga_4.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_4.TabIndex = 8
        Me.txt_total_harga_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_4
        '
        Me.txt_keterangan_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_keterangan_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_4.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_4.Name = "txt_keterangan_4"
        Me.txt_keterangan_4.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_4.TabIndex = 7
        '
        'txt_resep_4
        '
        Me.txt_resep_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_4.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_4.Name = "txt_resep_4"
        Me.txt_resep_4.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_4.TabIndex = 5
        '
        'txt_warna_4
        '
        Me.txt_warna_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_4.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_4.Name = "txt_warna_4"
        Me.txt_warna_4.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_4.TabIndex = 4
        '
        'txt_harga_proses_4
        '
        Me.txt_harga_proses_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_proses_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_4.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_4.Name = "txt_harga_proses_4"
        Me.txt_harga_proses_4.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_4.TabIndex = 3
        Me.txt_harga_proses_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_4
        '
        Me.txt_jenis_kain_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_4.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_4.Name = "txt_jenis_kain_4"
        Me.txt_jenis_kain_4.ReadOnly = True
        Me.txt_jenis_kain_4.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_4.TabIndex = 2
        '
        'txt_no_po_4
        '
        Me.txt_no_po_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_po_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_4.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_4.Name = "txt_no_po_4"
        Me.txt_no_po_4.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_4.TabIndex = 1
        '
        'txt_no_urut_4
        '
        Me.txt_no_urut_4.AutoSize = True
        Me.txt_no_urut_4.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_4.Name = "txt_no_urut_4"
        Me.txt_no_urut_4.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_4.TabIndex = 0
        Me.txt_no_urut_4.Text = "4"
        '
        'Panel_3
        '
        Me.Panel_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_3.Controls.Add(Me.txt_meter_3)
        Me.Panel_3.Controls.Add(Me.cb_satuan_3)
        Me.Panel_3.Controls.Add(Me.txt_kiloan_3)
        Me.Panel_3.Controls.Add(Me.txt_total_harga_3)
        Me.Panel_3.Controls.Add(Me.txt_keterangan_3)
        Me.Panel_3.Controls.Add(Me.txt_gudang_packing_3)
        Me.Panel_3.Controls.Add(Me.txt_gramasi_3)
        Me.Panel_3.Controls.Add(Me.txt_gulung_3)
        Me.Panel_3.Controls.Add(Me.txt_partai_3)
        Me.Panel_3.Controls.Add(Me.txt_resep_3)
        Me.Panel_3.Controls.Add(Me.txt_warna_3)
        Me.Panel_3.Controls.Add(Me.txt_harga_proses_3)
        Me.Panel_3.Controls.Add(Me.txt_jenis_kain_3)
        Me.Panel_3.Controls.Add(Me.txt_no_po_3)
        Me.Panel_3.Controls.Add(Me.txt_no_urut_3)
        Me.Panel_3.Location = New System.Drawing.Point(-3, 263)
        Me.Panel_3.Name = "Panel_3"
        Me.Panel_3.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_3.TabIndex = 49
        '
        'txt_meter_3
        '
        Me.txt_meter_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_3.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_3.Name = "txt_meter_3"
        Me.txt_meter_3.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_3.TabIndex = 82
        Me.txt_meter_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_3
        '
        Me.cb_satuan_3.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_3.FormattingEnabled = True
        Me.cb_satuan_3.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_3.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_3.Name = "cb_satuan_3"
        Me.cb_satuan_3.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_3.TabIndex = 504
        Me.cb_satuan_3.Text = "Meter"
        '
        'txt_kiloan_3
        '
        Me.txt_kiloan_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_3.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_3.Name = "txt_kiloan_3"
        Me.txt_kiloan_3.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_3.TabIndex = 9
        Me.txt_kiloan_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_3
        '
        Me.txt_total_harga_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_3.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_3.Name = "txt_total_harga_3"
        Me.txt_total_harga_3.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_3.TabIndex = 8
        Me.txt_total_harga_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_3
        '
        Me.txt_keterangan_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_3.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_3.Name = "txt_keterangan_3"
        Me.txt_keterangan_3.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_3.TabIndex = 7
        '
        'txt_gudang_packing_3
        '
        Me.txt_gudang_packing_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_3.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_3.Name = "txt_gudang_packing_3"
        Me.txt_gudang_packing_3.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_3.TabIndex = 87
        '
        'txt_gramasi_3
        '
        Me.txt_gramasi_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_3.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_3.Name = "txt_gramasi_3"
        Me.txt_gramasi_3.ReadOnly = True
        Me.txt_gramasi_3.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_3.TabIndex = 87
        Me.txt_gramasi_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_3
        '
        Me.txt_gulung_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_3.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_3.Name = "txt_gulung_3"
        Me.txt_gulung_3.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_3.TabIndex = 87
        Me.txt_gulung_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_3
        '
        Me.txt_partai_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_3.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_3.Name = "txt_partai_3"
        Me.txt_partai_3.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_3.TabIndex = 87
        '
        'txt_resep_3
        '
        Me.txt_resep_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_3.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_3.Name = "txt_resep_3"
        Me.txt_resep_3.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_3.TabIndex = 5
        '
        'txt_warna_3
        '
        Me.txt_warna_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_3.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_3.Name = "txt_warna_3"
        Me.txt_warna_3.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_3.TabIndex = 4
        '
        'txt_harga_proses_3
        '
        Me.txt_harga_proses_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_3.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_3.Name = "txt_harga_proses_3"
        Me.txt_harga_proses_3.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_3.TabIndex = 3
        Me.txt_harga_proses_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_3
        '
        Me.txt_jenis_kain_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_3.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_3.Name = "txt_jenis_kain_3"
        Me.txt_jenis_kain_3.ReadOnly = True
        Me.txt_jenis_kain_3.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_3.TabIndex = 2
        '
        'txt_no_po_3
        '
        Me.txt_no_po_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_3.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_3.Name = "txt_no_po_3"
        Me.txt_no_po_3.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_3.TabIndex = 1
        '
        'txt_no_urut_3
        '
        Me.txt_no_urut_3.AutoSize = True
        Me.txt_no_urut_3.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_3.Name = "txt_no_urut_3"
        Me.txt_no_urut_3.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_3.TabIndex = 0
        Me.txt_no_urut_3.Text = "3"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label26)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.Label17)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Location = New System.Drawing.Point(-3, 164)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1342, 30)
        Me.Panel2.TabIndex = 46
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.Window
        Me.Label26.Location = New System.Drawing.Point(619, 8)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(47, 13)
        Me.Label26.TabIndex = 15
        Me.Label26.Text = "Satuan"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.Window
        Me.Label15.Location = New System.Drawing.Point(769, 8)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 13)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Gramasi"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.Window
        Me.Label19.Location = New System.Drawing.Point(1208, 8)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 13)
        Me.Label19.TabIndex = 13
        Me.Label19.Text = "Keterangan"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.Window
        Me.Label18.Location = New System.Drawing.Point(939, 8)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 13)
        Me.Label18.TabIndex = 12
        Me.Label18.Text = "Total Harga"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.Window
        Me.Label17.Location = New System.Drawing.Point(854, 8)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(41, 13)
        Me.Label17.TabIndex = 11
        Me.Label17.Text = "Harga"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.Window
        Me.Label16.Location = New System.Drawing.Point(1047, 8)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(101, 13)
        Me.Label16.TabIndex = 10
        Me.Label16.Text = "Gudang Packing"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.Window
        Me.Label14.Location = New System.Drawing.Point(705, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(22, 13)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Kg"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Window
        Me.Label13.Location = New System.Drawing.Point(559, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(39, 13)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Meter"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.Window
        Me.Label12.Location = New System.Drawing.Point(488, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "Gulung"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.Window
        Me.Label11.Location = New System.Drawing.Point(427, 8)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Partai"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Window
        Me.Label10.Location = New System.Drawing.Point(350, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Resep"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Window
        Me.Label9.Location = New System.Drawing.Point(260, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(44, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Warna"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Window
        Me.Label8.Location = New System.Drawing.Point(141, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Jenis Kain"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Window
        Me.Label6.Location = New System.Drawing.Point(49, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "No PO"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Window
        Me.Label5.Location = New System.Drawing.Point(10, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(27, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "No."
        '
        'Panel_1
        '
        Me.Panel_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_1.Controls.Add(Me.txt_meter_1)
        Me.Panel_1.Controls.Add(Me.cb_satuan_1)
        Me.Panel_1.Controls.Add(Me.txt_kiloan_1)
        Me.Panel_1.Controls.Add(Me.txt_total_harga_1)
        Me.Panel_1.Controls.Add(Me.txt_keterangan_1)
        Me.Panel_1.Controls.Add(Me.txt_gudang_packing_1)
        Me.Panel_1.Controls.Add(Me.txt_gramasi_1)
        Me.Panel_1.Controls.Add(Me.txt_gulung_1)
        Me.Panel_1.Controls.Add(Me.txt_partai_1)
        Me.Panel_1.Controls.Add(Me.txt_resep_1)
        Me.Panel_1.Controls.Add(Me.txt_warna_1)
        Me.Panel_1.Controls.Add(Me.txt_harga_proses_1)
        Me.Panel_1.Controls.Add(Me.txt_jenis_kain_1)
        Me.Panel_1.Controls.Add(Me.txt_no_po_1)
        Me.Panel_1.Controls.Add(Me.txt_no_urut_1)
        Me.Panel_1.Location = New System.Drawing.Point(-3, 193)
        Me.Panel_1.Name = "Panel_1"
        Me.Panel_1.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_1.TabIndex = 47
        '
        'txt_meter_1
        '
        Me.txt_meter_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_1.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_1.Name = "txt_meter_1"
        Me.txt_meter_1.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_1.TabIndex = 85
        Me.txt_meter_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_1
        '
        Me.cb_satuan_1.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_1.FormattingEnabled = True
        Me.cb_satuan_1.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_1.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_1.Name = "cb_satuan_1"
        Me.cb_satuan_1.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_1.TabIndex = 510
        Me.cb_satuan_1.Text = "Meter"
        '
        'txt_kiloan_1
        '
        Me.txt_kiloan_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_1.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_1.Name = "txt_kiloan_1"
        Me.txt_kiloan_1.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_1.TabIndex = 9
        Me.txt_kiloan_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_1
        '
        Me.txt_total_harga_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_1.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_1.Name = "txt_total_harga_1"
        Me.txt_total_harga_1.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_1.TabIndex = 8
        Me.txt_total_harga_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_1
        '
        Me.txt_keterangan_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_1.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_1.Name = "txt_keterangan_1"
        Me.txt_keterangan_1.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_1.TabIndex = 7
        '
        'txt_gudang_packing_1
        '
        Me.txt_gudang_packing_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_1.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_1.Name = "txt_gudang_packing_1"
        Me.txt_gudang_packing_1.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_1.TabIndex = 89
        '
        'txt_gramasi_1
        '
        Me.txt_gramasi_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_1.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_1.Name = "txt_gramasi_1"
        Me.txt_gramasi_1.ReadOnly = True
        Me.txt_gramasi_1.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_1.TabIndex = 89
        Me.txt_gramasi_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_1
        '
        Me.txt_gulung_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_1.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_1.Name = "txt_gulung_1"
        Me.txt_gulung_1.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_1.TabIndex = 89
        Me.txt_gulung_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_1
        '
        Me.txt_partai_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_1.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_1.Name = "txt_partai_1"
        Me.txt_partai_1.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_1.TabIndex = 89
        '
        'txt_resep_1
        '
        Me.txt_resep_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_1.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_1.Name = "txt_resep_1"
        Me.txt_resep_1.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_1.TabIndex = 5
        '
        'txt_warna_1
        '
        Me.txt_warna_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_1.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_1.Name = "txt_warna_1"
        Me.txt_warna_1.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_1.TabIndex = 4
        '
        'txt_harga_proses_1
        '
        Me.txt_harga_proses_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_1.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_1.Name = "txt_harga_proses_1"
        Me.txt_harga_proses_1.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_1.TabIndex = 3
        Me.txt_harga_proses_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_1
        '
        Me.txt_jenis_kain_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_1.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_1.Name = "txt_jenis_kain_1"
        Me.txt_jenis_kain_1.ReadOnly = True
        Me.txt_jenis_kain_1.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_1.TabIndex = 2
        '
        'txt_no_po_1
        '
        Me.txt_no_po_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_1.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_1.Name = "txt_no_po_1"
        Me.txt_no_po_1.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_1.TabIndex = 1
        '
        'txt_no_urut_1
        '
        Me.txt_no_urut_1.AutoSize = True
        Me.txt_no_urut_1.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_1.Name = "txt_no_urut_1"
        Me.txt_no_urut_1.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_1.TabIndex = 0
        Me.txt_no_urut_1.Text = "1"
        '
        'Panel_2
        '
        Me.Panel_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_2.Controls.Add(Me.txt_meter_2)
        Me.Panel_2.Controls.Add(Me.cb_satuan_2)
        Me.Panel_2.Controls.Add(Me.txt_kiloan_2)
        Me.Panel_2.Controls.Add(Me.txt_total_harga_2)
        Me.Panel_2.Controls.Add(Me.txt_keterangan_2)
        Me.Panel_2.Controls.Add(Me.txt_gudang_packing_2)
        Me.Panel_2.Controls.Add(Me.txt_gramasi_2)
        Me.Panel_2.Controls.Add(Me.txt_gulung_2)
        Me.Panel_2.Controls.Add(Me.txt_partai_2)
        Me.Panel_2.Controls.Add(Me.txt_resep_2)
        Me.Panel_2.Controls.Add(Me.txt_warna_2)
        Me.Panel_2.Controls.Add(Me.txt_harga_proses_2)
        Me.Panel_2.Controls.Add(Me.txt_jenis_kain_2)
        Me.Panel_2.Controls.Add(Me.txt_no_po_2)
        Me.Panel_2.Controls.Add(Me.txt_no_urut_2)
        Me.Panel_2.Location = New System.Drawing.Point(-3, 228)
        Me.Panel_2.Name = "Panel_2"
        Me.Panel_2.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_2.TabIndex = 48
        '
        'txt_meter_2
        '
        Me.txt_meter_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_2.Location = New System.Drawing.Point(548, 7)
        Me.txt_meter_2.Name = "txt_meter_2"
        Me.txt_meter_2.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_2.TabIndex = 81
        Me.txt_meter_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_satuan_2
        '
        Me.cb_satuan_2.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_2.FormattingEnabled = True
        Me.cb_satuan_2.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_2.Location = New System.Drawing.Point(617, 6)
        Me.cb_satuan_2.Name = "cb_satuan_2"
        Me.cb_satuan_2.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_2.TabIndex = 503
        Me.cb_satuan_2.Text = "Meter"
        '
        'txt_kiloan_2
        '
        Me.txt_kiloan_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kiloan_2.Location = New System.Drawing.Point(676, 7)
        Me.txt_kiloan_2.Name = "txt_kiloan_2"
        Me.txt_kiloan_2.Size = New System.Drawing.Size(80, 20)
        Me.txt_kiloan_2.TabIndex = 9
        Me.txt_kiloan_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_2
        '
        Me.txt_total_harga_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_2.Location = New System.Drawing.Point(923, 7)
        Me.txt_total_harga_2.Name = "txt_total_harga_2"
        Me.txt_total_harga_2.Size = New System.Drawing.Size(107, 20)
        Me.txt_total_harga_2.TabIndex = 8
        Me.txt_total_harga_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_2
        '
        Me.txt_keterangan_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_2.Location = New System.Drawing.Point(1165, 7)
        Me.txt_keterangan_2.Name = "txt_keterangan_2"
        Me.txt_keterangan_2.Size = New System.Drawing.Size(159, 20)
        Me.txt_keterangan_2.TabIndex = 7
        '
        'txt_gudang_packing_2
        '
        Me.txt_gudang_packing_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing_2.Location = New System.Drawing.Point(1039, 7)
        Me.txt_gudang_packing_2.Name = "txt_gudang_packing_2"
        Me.txt_gudang_packing_2.Size = New System.Drawing.Size(117, 20)
        Me.txt_gudang_packing_2.TabIndex = 88
        '
        'txt_gramasi_2
        '
        Me.txt_gramasi_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi_2.Location = New System.Drawing.Point(765, 7)
        Me.txt_gramasi_2.Name = "txt_gramasi_2"
        Me.txt_gramasi_2.ReadOnly = True
        Me.txt_gramasi_2.Size = New System.Drawing.Size(60, 20)
        Me.txt_gramasi_2.TabIndex = 88
        Me.txt_gramasi_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_2
        '
        Me.txt_gulung_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_2.Location = New System.Drawing.Point(484, 7)
        Me.txt_gulung_2.Name = "txt_gulung_2"
        Me.txt_gulung_2.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_2.TabIndex = 88
        Me.txt_gulung_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_2
        '
        Me.txt_partai_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_2.Location = New System.Drawing.Point(420, 7)
        Me.txt_partai_2.Name = "txt_partai_2"
        Me.txt_partai_2.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_2.TabIndex = 88
        '
        'txt_resep_2
        '
        Me.txt_resep_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_2.Location = New System.Drawing.Point(331, 7)
        Me.txt_resep_2.Name = "txt_resep_2"
        Me.txt_resep_2.Size = New System.Drawing.Size(80, 20)
        Me.txt_resep_2.TabIndex = 5
        '
        'txt_warna_2
        '
        Me.txt_warna_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_2.Location = New System.Drawing.Point(242, 7)
        Me.txt_warna_2.Name = "txt_warna_2"
        Me.txt_warna_2.Size = New System.Drawing.Size(80, 20)
        Me.txt_warna_2.TabIndex = 4
        '
        'txt_harga_proses_2
        '
        Me.txt_harga_proses_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_proses_2.Location = New System.Drawing.Point(834, 7)
        Me.txt_harga_proses_2.Name = "txt_harga_proses_2"
        Me.txt_harga_proses_2.Size = New System.Drawing.Size(80, 20)
        Me.txt_harga_proses_2.TabIndex = 3
        Me.txt_harga_proses_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_jenis_kain_2
        '
        Me.txt_jenis_kain_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_2.Location = New System.Drawing.Point(113, 7)
        Me.txt_jenis_kain_2.Name = "txt_jenis_kain_2"
        Me.txt_jenis_kain_2.ReadOnly = True
        Me.txt_jenis_kain_2.Size = New System.Drawing.Size(120, 20)
        Me.txt_jenis_kain_2.TabIndex = 2
        '
        'txt_no_po_2
        '
        Me.txt_no_po_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po_2.Location = New System.Drawing.Point(39, 7)
        Me.txt_no_po_2.Name = "txt_no_po_2"
        Me.txt_no_po_2.Size = New System.Drawing.Size(65, 20)
        Me.txt_no_po_2.TabIndex = 1
        '
        'txt_no_urut_2
        '
        Me.txt_no_urut_2.AutoSize = True
        Me.txt_no_urut_2.Location = New System.Drawing.Point(17, 11)
        Me.txt_no_urut_2.Name = "txt_no_urut_2"
        Me.txt_no_urut_2.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_2.TabIndex = 0
        Me.txt_no_urut_2.Text = "2"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btn_batal_10)
        Me.Panel3.Controls.Add(Me.btn_batal_9)
        Me.Panel3.Controls.Add(Me.btn_batal_8)
        Me.Panel3.Controls.Add(Me.btn_batal_7)
        Me.Panel3.Controls.Add(Me.btn_batal_6)
        Me.Panel3.Controls.Add(Me.btn_batal_5)
        Me.Panel3.Controls.Add(Me.btn_batal_4)
        Me.Panel3.Controls.Add(Me.btn_batal_3)
        Me.Panel3.Controls.Add(Me.btn_batal_2)
        Me.Panel3.Controls.Add(Me.btn_batal_1)
        Me.Panel3.Controls.Add(Me.btn_hapus_10)
        Me.Panel3.Controls.Add(Me.btn_selesai_10)
        Me.Panel3.Controls.Add(Me.btn_hapus_9)
        Me.Panel3.Controls.Add(Me.btn_selesai_9)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_9)
        Me.Panel3.Controls.Add(Me.btn_hapus_8)
        Me.Panel3.Controls.Add(Me.btn_selesai_8)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_8)
        Me.Panel3.Controls.Add(Me.btn_hapus_7)
        Me.Panel3.Controls.Add(Me.btn_selesai_7)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_7)
        Me.Panel3.Controls.Add(Me.btn_hapus_6)
        Me.Panel3.Controls.Add(Me.btn_selesai_6)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_6)
        Me.Panel3.Controls.Add(Me.btn_hapus_5)
        Me.Panel3.Controls.Add(Me.btn_selesai_5)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_5)
        Me.Panel3.Controls.Add(Me.btn_hapus_4)
        Me.Panel3.Controls.Add(Me.btn_selesai_4)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_4)
        Me.Panel3.Controls.Add(Me.btn_hapus_3)
        Me.Panel3.Controls.Add(Me.btn_selesai_3)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_3)
        Me.Panel3.Controls.Add(Me.btn_hapus_2)
        Me.Panel3.Controls.Add(Me.btn_selesai_2)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_2)
        Me.Panel3.Controls.Add(Me.btn_selesai_1)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_1)
        Me.Panel3.Location = New System.Drawing.Point(424, 250)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(488, 361)
        Me.Panel3.TabIndex = 57
        '
        'btn_batal_10
        '
        Me.btn_batal_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_10.Image = CType(resources.GetObject("btn_batal_10.Image"), System.Drawing.Image)
        Me.btn_batal_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_10.Location = New System.Drawing.Point(325, 332)
        Me.btn_batal_10.Name = "btn_batal_10"
        Me.btn_batal_10.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_10.TabIndex = 58
        Me.btn_batal_10.Text = "Batal"
        Me.btn_batal_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_10.UseVisualStyleBackColor = False
        '
        'btn_batal_9
        '
        Me.btn_batal_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_9.Image = CType(resources.GetObject("btn_batal_9.Image"), System.Drawing.Image)
        Me.btn_batal_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_9.Location = New System.Drawing.Point(384, 296)
        Me.btn_batal_9.Name = "btn_batal_9"
        Me.btn_batal_9.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_9.TabIndex = 57
        Me.btn_batal_9.Text = "Batal"
        Me.btn_batal_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_9.UseVisualStyleBackColor = False
        '
        'btn_batal_8
        '
        Me.btn_batal_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_8.Image = CType(resources.GetObject("btn_batal_8.Image"), System.Drawing.Image)
        Me.btn_batal_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_8.Location = New System.Drawing.Point(384, 260)
        Me.btn_batal_8.Name = "btn_batal_8"
        Me.btn_batal_8.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_8.TabIndex = 56
        Me.btn_batal_8.Text = "Batal"
        Me.btn_batal_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_8.UseVisualStyleBackColor = False
        '
        'btn_batal_7
        '
        Me.btn_batal_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_7.Image = CType(resources.GetObject("btn_batal_7.Image"), System.Drawing.Image)
        Me.btn_batal_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_7.Location = New System.Drawing.Point(384, 226)
        Me.btn_batal_7.Name = "btn_batal_7"
        Me.btn_batal_7.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_7.TabIndex = 55
        Me.btn_batal_7.Text = "Batal"
        Me.btn_batal_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_7.UseVisualStyleBackColor = False
        '
        'btn_batal_6
        '
        Me.btn_batal_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_6.Image = CType(resources.GetObject("btn_batal_6.Image"), System.Drawing.Image)
        Me.btn_batal_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_6.Location = New System.Drawing.Point(384, 191)
        Me.btn_batal_6.Name = "btn_batal_6"
        Me.btn_batal_6.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_6.TabIndex = 54
        Me.btn_batal_6.Text = "Batal"
        Me.btn_batal_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_6.UseVisualStyleBackColor = False
        '
        'btn_batal_5
        '
        Me.btn_batal_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_5.Image = CType(resources.GetObject("btn_batal_5.Image"), System.Drawing.Image)
        Me.btn_batal_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_5.Location = New System.Drawing.Point(384, 157)
        Me.btn_batal_5.Name = "btn_batal_5"
        Me.btn_batal_5.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_5.TabIndex = 53
        Me.btn_batal_5.Text = "Batal"
        Me.btn_batal_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_5.UseVisualStyleBackColor = False
        '
        'btn_batal_4
        '
        Me.btn_batal_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_4.Image = CType(resources.GetObject("btn_batal_4.Image"), System.Drawing.Image)
        Me.btn_batal_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_4.Location = New System.Drawing.Point(384, 122)
        Me.btn_batal_4.Name = "btn_batal_4"
        Me.btn_batal_4.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_4.TabIndex = 52
        Me.btn_batal_4.Text = "Batal"
        Me.btn_batal_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_4.UseVisualStyleBackColor = False
        '
        'btn_batal_3
        '
        Me.btn_batal_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_3.Image = CType(resources.GetObject("btn_batal_3.Image"), System.Drawing.Image)
        Me.btn_batal_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_3.Location = New System.Drawing.Point(384, 86)
        Me.btn_batal_3.Name = "btn_batal_3"
        Me.btn_batal_3.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_3.TabIndex = 51
        Me.btn_batal_3.Text = "Batal"
        Me.btn_batal_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_3.UseVisualStyleBackColor = False
        '
        'btn_batal_2
        '
        Me.btn_batal_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_2.Image = CType(resources.GetObject("btn_batal_2.Image"), System.Drawing.Image)
        Me.btn_batal_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_2.Location = New System.Drawing.Point(384, 50)
        Me.btn_batal_2.Name = "btn_batal_2"
        Me.btn_batal_2.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_2.TabIndex = 50
        Me.btn_batal_2.Text = "Batal"
        Me.btn_batal_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_2.UseVisualStyleBackColor = False
        '
        'btn_batal_1
        '
        Me.btn_batal_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_1.Image = CType(resources.GetObject("btn_batal_1.Image"), System.Drawing.Image)
        Me.btn_batal_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_1.Location = New System.Drawing.Point(342, 14)
        Me.btn_batal_1.Name = "btn_batal_1"
        Me.btn_batal_1.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_1.TabIndex = 49
        Me.btn_batal_1.Text = "Batal"
        Me.btn_batal_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_1.UseVisualStyleBackColor = False
        '
        'btn_hapus_10
        '
        Me.btn_hapus_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_10.Image = CType(resources.GetObject("btn_hapus_10.Image"), System.Drawing.Image)
        Me.btn_hapus_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_10.Location = New System.Drawing.Point(212, 332)
        Me.btn_hapus_10.Name = "btn_hapus_10"
        Me.btn_hapus_10.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_10.TabIndex = 37
        Me.btn_hapus_10.Text = "Hapus"
        Me.btn_hapus_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_10.UseVisualStyleBackColor = False
        '
        'btn_selesai_10
        '
        Me.btn_selesai_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_10.Image = CType(resources.GetObject("btn_selesai_10.Image"), System.Drawing.Image)
        Me.btn_selesai_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_10.Location = New System.Drawing.Point(99, 332)
        Me.btn_selesai_10.Name = "btn_selesai_10"
        Me.btn_selesai_10.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_10.TabIndex = 36
        Me.btn_selesai_10.Text = "Selesai"
        Me.btn_selesai_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_10.UseVisualStyleBackColor = False
        '
        'btn_hapus_9
        '
        Me.btn_hapus_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_9.Image = CType(resources.GetObject("btn_hapus_9.Image"), System.Drawing.Image)
        Me.btn_hapus_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_9.Location = New System.Drawing.Point(271, 296)
        Me.btn_hapus_9.Name = "btn_hapus_9"
        Me.btn_hapus_9.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_9.TabIndex = 34
        Me.btn_hapus_9.Text = "Hapus"
        Me.btn_hapus_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_9.UseVisualStyleBackColor = False
        '
        'btn_selesai_9
        '
        Me.btn_selesai_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_9.Image = CType(resources.GetObject("btn_selesai_9.Image"), System.Drawing.Image)
        Me.btn_selesai_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_9.Location = New System.Drawing.Point(158, 296)
        Me.btn_selesai_9.Name = "btn_selesai_9"
        Me.btn_selesai_9.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_9.TabIndex = 33
        Me.btn_selesai_9.Text = "Selesai"
        Me.btn_selesai_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_9.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_9
        '
        Me.btn_tambah_baris_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_9.Image = CType(resources.GetObject("btn_tambah_baris_9.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_9.Location = New System.Drawing.Point(10, 296)
        Me.btn_tambah_baris_9.Name = "btn_tambah_baris_9"
        Me.btn_tambah_baris_9.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_9.TabIndex = 32
        Me.btn_tambah_baris_9.Text = "Tambah Baris"
        Me.btn_tambah_baris_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_9.UseVisualStyleBackColor = False
        '
        'btn_hapus_8
        '
        Me.btn_hapus_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_8.Image = CType(resources.GetObject("btn_hapus_8.Image"), System.Drawing.Image)
        Me.btn_hapus_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_8.Location = New System.Drawing.Point(271, 260)
        Me.btn_hapus_8.Name = "btn_hapus_8"
        Me.btn_hapus_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_8.TabIndex = 31
        Me.btn_hapus_8.Text = "Hapus"
        Me.btn_hapus_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_8.UseVisualStyleBackColor = False
        '
        'btn_selesai_8
        '
        Me.btn_selesai_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_8.Image = CType(resources.GetObject("btn_selesai_8.Image"), System.Drawing.Image)
        Me.btn_selesai_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_8.Location = New System.Drawing.Point(158, 260)
        Me.btn_selesai_8.Name = "btn_selesai_8"
        Me.btn_selesai_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_8.TabIndex = 30
        Me.btn_selesai_8.Text = "Selesai"
        Me.btn_selesai_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_8.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_8
        '
        Me.btn_tambah_baris_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_8.Image = CType(resources.GetObject("btn_tambah_baris_8.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_8.Location = New System.Drawing.Point(10, 260)
        Me.btn_tambah_baris_8.Name = "btn_tambah_baris_8"
        Me.btn_tambah_baris_8.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_8.TabIndex = 29
        Me.btn_tambah_baris_8.Text = "Tambah Baris"
        Me.btn_tambah_baris_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_8.UseVisualStyleBackColor = False
        '
        'btn_hapus_7
        '
        Me.btn_hapus_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_7.Image = CType(resources.GetObject("btn_hapus_7.Image"), System.Drawing.Image)
        Me.btn_hapus_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_7.Location = New System.Drawing.Point(271, 226)
        Me.btn_hapus_7.Name = "btn_hapus_7"
        Me.btn_hapus_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_7.TabIndex = 28
        Me.btn_hapus_7.Text = "Hapus"
        Me.btn_hapus_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_7.UseVisualStyleBackColor = False
        '
        'btn_selesai_7
        '
        Me.btn_selesai_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_7.Image = CType(resources.GetObject("btn_selesai_7.Image"), System.Drawing.Image)
        Me.btn_selesai_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_7.Location = New System.Drawing.Point(158, 226)
        Me.btn_selesai_7.Name = "btn_selesai_7"
        Me.btn_selesai_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_7.TabIndex = 27
        Me.btn_selesai_7.Text = "Selesai"
        Me.btn_selesai_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_7.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_7
        '
        Me.btn_tambah_baris_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_7.Image = CType(resources.GetObject("btn_tambah_baris_7.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_7.Location = New System.Drawing.Point(10, 226)
        Me.btn_tambah_baris_7.Name = "btn_tambah_baris_7"
        Me.btn_tambah_baris_7.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_7.TabIndex = 26
        Me.btn_tambah_baris_7.Text = "Tambah Baris"
        Me.btn_tambah_baris_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_7.UseVisualStyleBackColor = False
        '
        'btn_hapus_6
        '
        Me.btn_hapus_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_6.Image = CType(resources.GetObject("btn_hapus_6.Image"), System.Drawing.Image)
        Me.btn_hapus_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_6.Location = New System.Drawing.Point(271, 191)
        Me.btn_hapus_6.Name = "btn_hapus_6"
        Me.btn_hapus_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_6.TabIndex = 25
        Me.btn_hapus_6.Text = "Hapus"
        Me.btn_hapus_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_6.UseVisualStyleBackColor = False
        '
        'btn_selesai_6
        '
        Me.btn_selesai_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_6.Image = CType(resources.GetObject("btn_selesai_6.Image"), System.Drawing.Image)
        Me.btn_selesai_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_6.Location = New System.Drawing.Point(158, 191)
        Me.btn_selesai_6.Name = "btn_selesai_6"
        Me.btn_selesai_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_6.TabIndex = 24
        Me.btn_selesai_6.Text = "Selesai"
        Me.btn_selesai_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_6.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_6
        '
        Me.btn_tambah_baris_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_6.Image = CType(resources.GetObject("btn_tambah_baris_6.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_6.Location = New System.Drawing.Point(10, 191)
        Me.btn_tambah_baris_6.Name = "btn_tambah_baris_6"
        Me.btn_tambah_baris_6.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_6.TabIndex = 23
        Me.btn_tambah_baris_6.Text = "Tambah Baris"
        Me.btn_tambah_baris_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_6.UseVisualStyleBackColor = False
        '
        'btn_hapus_5
        '
        Me.btn_hapus_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_5.Image = CType(resources.GetObject("btn_hapus_5.Image"), System.Drawing.Image)
        Me.btn_hapus_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_5.Location = New System.Drawing.Point(271, 157)
        Me.btn_hapus_5.Name = "btn_hapus_5"
        Me.btn_hapus_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_5.TabIndex = 22
        Me.btn_hapus_5.Text = "Hapus"
        Me.btn_hapus_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_5.UseVisualStyleBackColor = False
        '
        'btn_selesai_5
        '
        Me.btn_selesai_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_5.Image = CType(resources.GetObject("btn_selesai_5.Image"), System.Drawing.Image)
        Me.btn_selesai_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_5.Location = New System.Drawing.Point(158, 157)
        Me.btn_selesai_5.Name = "btn_selesai_5"
        Me.btn_selesai_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_5.TabIndex = 21
        Me.btn_selesai_5.Text = "Selesai"
        Me.btn_selesai_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_5.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_5
        '
        Me.btn_tambah_baris_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_5.Image = CType(resources.GetObject("btn_tambah_baris_5.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_5.Location = New System.Drawing.Point(10, 157)
        Me.btn_tambah_baris_5.Name = "btn_tambah_baris_5"
        Me.btn_tambah_baris_5.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_5.TabIndex = 20
        Me.btn_tambah_baris_5.Text = "Tambah Baris"
        Me.btn_tambah_baris_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_5.UseVisualStyleBackColor = False
        '
        'btn_hapus_4
        '
        Me.btn_hapus_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_4.Image = CType(resources.GetObject("btn_hapus_4.Image"), System.Drawing.Image)
        Me.btn_hapus_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_4.Location = New System.Drawing.Point(271, 122)
        Me.btn_hapus_4.Name = "btn_hapus_4"
        Me.btn_hapus_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_4.TabIndex = 19
        Me.btn_hapus_4.Text = "Hapus"
        Me.btn_hapus_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_4.UseVisualStyleBackColor = False
        '
        'btn_selesai_4
        '
        Me.btn_selesai_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_4.Image = CType(resources.GetObject("btn_selesai_4.Image"), System.Drawing.Image)
        Me.btn_selesai_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_4.Location = New System.Drawing.Point(158, 122)
        Me.btn_selesai_4.Name = "btn_selesai_4"
        Me.btn_selesai_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_4.TabIndex = 18
        Me.btn_selesai_4.Text = "Selesai"
        Me.btn_selesai_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_4.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_4
        '
        Me.btn_tambah_baris_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_4.Image = CType(resources.GetObject("btn_tambah_baris_4.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_4.Location = New System.Drawing.Point(10, 122)
        Me.btn_tambah_baris_4.Name = "btn_tambah_baris_4"
        Me.btn_tambah_baris_4.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_4.TabIndex = 17
        Me.btn_tambah_baris_4.Text = "Tambah Baris"
        Me.btn_tambah_baris_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_4.UseVisualStyleBackColor = False
        '
        'btn_hapus_3
        '
        Me.btn_hapus_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_3.Image = CType(resources.GetObject("btn_hapus_3.Image"), System.Drawing.Image)
        Me.btn_hapus_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_3.Location = New System.Drawing.Point(271, 86)
        Me.btn_hapus_3.Name = "btn_hapus_3"
        Me.btn_hapus_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_3.TabIndex = 16
        Me.btn_hapus_3.Text = "Hapus"
        Me.btn_hapus_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_3.UseVisualStyleBackColor = False
        '
        'btn_selesai_3
        '
        Me.btn_selesai_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_3.Image = CType(resources.GetObject("btn_selesai_3.Image"), System.Drawing.Image)
        Me.btn_selesai_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_3.Location = New System.Drawing.Point(158, 86)
        Me.btn_selesai_3.Name = "btn_selesai_3"
        Me.btn_selesai_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_3.TabIndex = 15
        Me.btn_selesai_3.Text = "Selesai"
        Me.btn_selesai_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_3.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_3
        '
        Me.btn_tambah_baris_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_3.Image = CType(resources.GetObject("btn_tambah_baris_3.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_3.Location = New System.Drawing.Point(10, 86)
        Me.btn_tambah_baris_3.Name = "btn_tambah_baris_3"
        Me.btn_tambah_baris_3.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_3.TabIndex = 14
        Me.btn_tambah_baris_3.Text = "Tambah Baris"
        Me.btn_tambah_baris_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_3.UseVisualStyleBackColor = False
        '
        'btn_hapus_2
        '
        Me.btn_hapus_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_2.Image = CType(resources.GetObject("btn_hapus_2.Image"), System.Drawing.Image)
        Me.btn_hapus_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_2.Location = New System.Drawing.Point(271, 50)
        Me.btn_hapus_2.Name = "btn_hapus_2"
        Me.btn_hapus_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_2.TabIndex = 13
        Me.btn_hapus_2.Text = "Hapus"
        Me.btn_hapus_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_2
        '
        Me.btn_selesai_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_2.Image = CType(resources.GetObject("btn_selesai_2.Image"), System.Drawing.Image)
        Me.btn_selesai_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_2.Location = New System.Drawing.Point(158, 50)
        Me.btn_selesai_2.Name = "btn_selesai_2"
        Me.btn_selesai_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_2.TabIndex = 12
        Me.btn_selesai_2.Text = "Selesai"
        Me.btn_selesai_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_2.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_2
        '
        Me.btn_tambah_baris_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_2.Image = CType(resources.GetObject("btn_tambah_baris_2.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_2.Location = New System.Drawing.Point(10, 50)
        Me.btn_tambah_baris_2.Name = "btn_tambah_baris_2"
        Me.btn_tambah_baris_2.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_2.TabIndex = 11
        Me.btn_tambah_baris_2.Text = "Tambah Baris"
        Me.btn_tambah_baris_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_1
        '
        Me.btn_selesai_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_1.Image = CType(resources.GetObject("btn_selesai_1.Image"), System.Drawing.Image)
        Me.btn_selesai_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_1.Location = New System.Drawing.Point(229, 14)
        Me.btn_selesai_1.Name = "btn_selesai_1"
        Me.btn_selesai_1.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_1.TabIndex = 4
        Me.btn_selesai_1.Text = "Selesai"
        Me.btn_selesai_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_1.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_1
        '
        Me.btn_tambah_baris_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_1.Image = CType(resources.GetObject("btn_tambah_baris_1.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_1.Location = New System.Drawing.Point(81, 14)
        Me.btn_tambah_baris_1.Name = "btn_tambah_baris_1"
        Me.btn_tambah_baris_1.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_1.TabIndex = 3
        Me.btn_tambah_baris_1.Text = "Tambah Baris"
        Me.btn_tambah_baris_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_1.UseVisualStyleBackColor = False
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label20)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_10)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_9)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_8)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_7)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_6)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_5)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_4)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_3)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_2)
        Me.Panel4.Controls.Add(Me.txt_sisa_qty_1)
        Me.Panel4.Location = New System.Drawing.Point(234, 2)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(108, 153)
        Me.Panel4.TabIndex = 6
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(28, 133)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(52, 13)
        Me.Label20.TabIndex = 10
        Me.Label20.Text = "Sisa QTY"
        '
        'txt_sisa_qty_10
        '
        Me.txt_sisa_qty_10.Location = New System.Drawing.Point(54, 106)
        Me.txt_sisa_qty_10.Name = "txt_sisa_qty_10"
        Me.txt_sisa_qty_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_10.TabIndex = 9
        '
        'txt_sisa_qty_9
        '
        Me.txt_sisa_qty_9.Location = New System.Drawing.Point(54, 83)
        Me.txt_sisa_qty_9.Name = "txt_sisa_qty_9"
        Me.txt_sisa_qty_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_9.TabIndex = 8
        '
        'txt_sisa_qty_8
        '
        Me.txt_sisa_qty_8.Location = New System.Drawing.Point(54, 60)
        Me.txt_sisa_qty_8.Name = "txt_sisa_qty_8"
        Me.txt_sisa_qty_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_8.TabIndex = 7
        '
        'txt_sisa_qty_7
        '
        Me.txt_sisa_qty_7.Location = New System.Drawing.Point(54, 37)
        Me.txt_sisa_qty_7.Name = "txt_sisa_qty_7"
        Me.txt_sisa_qty_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_7.TabIndex = 6
        '
        'txt_sisa_qty_6
        '
        Me.txt_sisa_qty_6.Location = New System.Drawing.Point(54, 14)
        Me.txt_sisa_qty_6.Name = "txt_sisa_qty_6"
        Me.txt_sisa_qty_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_6.TabIndex = 5
        '
        'txt_sisa_qty_5
        '
        Me.txt_sisa_qty_5.Location = New System.Drawing.Point(4, 106)
        Me.txt_sisa_qty_5.Name = "txt_sisa_qty_5"
        Me.txt_sisa_qty_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_5.TabIndex = 4
        '
        'txt_sisa_qty_4
        '
        Me.txt_sisa_qty_4.Location = New System.Drawing.Point(4, 83)
        Me.txt_sisa_qty_4.Name = "txt_sisa_qty_4"
        Me.txt_sisa_qty_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_4.TabIndex = 3
        '
        'txt_sisa_qty_3
        '
        Me.txt_sisa_qty_3.Location = New System.Drawing.Point(4, 60)
        Me.txt_sisa_qty_3.Name = "txt_sisa_qty_3"
        Me.txt_sisa_qty_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_3.TabIndex = 2
        '
        'txt_sisa_qty_2
        '
        Me.txt_sisa_qty_2.Location = New System.Drawing.Point(4, 37)
        Me.txt_sisa_qty_2.Name = "txt_sisa_qty_2"
        Me.txt_sisa_qty_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_2.TabIndex = 1
        '
        'txt_sisa_qty_1
        '
        Me.txt_sisa_qty_1.Location = New System.Drawing.Point(4, 14)
        Me.txt_sisa_qty_1.Name = "txt_sisa_qty_1"
        Me.txt_sisa_qty_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_sisa_qty_1.TabIndex = 0
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label24)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_10)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_9)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_8)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_7)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_6)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_5)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_4)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_3)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_2)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_1)
        Me.Panel8.Location = New System.Drawing.Point(1042, 2)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(120, 141)
        Me.Panel8.TabIndex = 74
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(26, 117)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(68, 13)
        Me.Label24.TabIndex = 10
        Me.Label24.Text = "ID SJ Proses"
        '
        'txt_id_sj_proses_10
        '
        Me.txt_id_sj_proses_10.Location = New System.Drawing.Point(60, 94)
        Me.txt_id_sj_proses_10.Name = "txt_id_sj_proses_10"
        Me.txt_id_sj_proses_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_10.TabIndex = 9
        '
        'txt_id_sj_proses_9
        '
        Me.txt_id_sj_proses_9.Location = New System.Drawing.Point(60, 74)
        Me.txt_id_sj_proses_9.Name = "txt_id_sj_proses_9"
        Me.txt_id_sj_proses_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_9.TabIndex = 8
        '
        'txt_id_sj_proses_8
        '
        Me.txt_id_sj_proses_8.Location = New System.Drawing.Point(60, 54)
        Me.txt_id_sj_proses_8.Name = "txt_id_sj_proses_8"
        Me.txt_id_sj_proses_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_8.TabIndex = 7
        '
        'txt_id_sj_proses_7
        '
        Me.txt_id_sj_proses_7.Location = New System.Drawing.Point(60, 34)
        Me.txt_id_sj_proses_7.Name = "txt_id_sj_proses_7"
        Me.txt_id_sj_proses_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_7.TabIndex = 6
        '
        'txt_id_sj_proses_6
        '
        Me.txt_id_sj_proses_6.Location = New System.Drawing.Point(60, 14)
        Me.txt_id_sj_proses_6.Name = "txt_id_sj_proses_6"
        Me.txt_id_sj_proses_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_6.TabIndex = 5
        '
        'txt_id_sj_proses_5
        '
        Me.txt_id_sj_proses_5.Location = New System.Drawing.Point(10, 94)
        Me.txt_id_sj_proses_5.Name = "txt_id_sj_proses_5"
        Me.txt_id_sj_proses_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_5.TabIndex = 4
        '
        'txt_id_sj_proses_4
        '
        Me.txt_id_sj_proses_4.Location = New System.Drawing.Point(10, 74)
        Me.txt_id_sj_proses_4.Name = "txt_id_sj_proses_4"
        Me.txt_id_sj_proses_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_4.TabIndex = 3
        '
        'txt_id_sj_proses_3
        '
        Me.txt_id_sj_proses_3.Location = New System.Drawing.Point(10, 54)
        Me.txt_id_sj_proses_3.Name = "txt_id_sj_proses_3"
        Me.txt_id_sj_proses_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_3.TabIndex = 2
        '
        'txt_id_sj_proses_2
        '
        Me.txt_id_sj_proses_2.Location = New System.Drawing.Point(10, 34)
        Me.txt_id_sj_proses_2.Name = "txt_id_sj_proses_2"
        Me.txt_id_sj_proses_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_2.TabIndex = 1
        '
        'txt_id_sj_proses_1
        '
        Me.txt_id_sj_proses_1.Location = New System.Drawing.Point(10, 14)
        Me.txt_id_sj_proses_1.Name = "txt_id_sj_proses_1"
        Me.txt_id_sj_proses_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_1.TabIndex = 0
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.Label7)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_10)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_9)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_8)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_7)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_6)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_5)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_4)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_3)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_2)
        Me.Panel10.Controls.Add(Me.txt_id_po_proses_1)
        Me.Panel10.Location = New System.Drawing.Point(1133, 2)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(116, 134)
        Me.Panel10.TabIndex = 111
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(23, 113)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "ID PO Proses"
        '
        'txt_id_po_proses_10
        '
        Me.txt_id_po_proses_10.Location = New System.Drawing.Point(58, 89)
        Me.txt_id_po_proses_10.Name = "txt_id_po_proses_10"
        Me.txt_id_po_proses_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_10.TabIndex = 9
        '
        'txt_id_po_proses_9
        '
        Me.txt_id_po_proses_9.Location = New System.Drawing.Point(58, 69)
        Me.txt_id_po_proses_9.Name = "txt_id_po_proses_9"
        Me.txt_id_po_proses_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_9.TabIndex = 8
        '
        'txt_id_po_proses_8
        '
        Me.txt_id_po_proses_8.Location = New System.Drawing.Point(58, 49)
        Me.txt_id_po_proses_8.Name = "txt_id_po_proses_8"
        Me.txt_id_po_proses_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_8.TabIndex = 7
        '
        'txt_id_po_proses_7
        '
        Me.txt_id_po_proses_7.Location = New System.Drawing.Point(58, 29)
        Me.txt_id_po_proses_7.Name = "txt_id_po_proses_7"
        Me.txt_id_po_proses_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_7.TabIndex = 6
        '
        'txt_id_po_proses_6
        '
        Me.txt_id_po_proses_6.Location = New System.Drawing.Point(58, 9)
        Me.txt_id_po_proses_6.Name = "txt_id_po_proses_6"
        Me.txt_id_po_proses_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_6.TabIndex = 5
        '
        'txt_id_po_proses_5
        '
        Me.txt_id_po_proses_5.Location = New System.Drawing.Point(8, 89)
        Me.txt_id_po_proses_5.Name = "txt_id_po_proses_5"
        Me.txt_id_po_proses_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_5.TabIndex = 4
        '
        'txt_id_po_proses_4
        '
        Me.txt_id_po_proses_4.Location = New System.Drawing.Point(8, 69)
        Me.txt_id_po_proses_4.Name = "txt_id_po_proses_4"
        Me.txt_id_po_proses_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_4.TabIndex = 3
        '
        'txt_id_po_proses_3
        '
        Me.txt_id_po_proses_3.Location = New System.Drawing.Point(8, 49)
        Me.txt_id_po_proses_3.Name = "txt_id_po_proses_3"
        Me.txt_id_po_proses_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_3.TabIndex = 2
        '
        'txt_id_po_proses_2
        '
        Me.txt_id_po_proses_2.Location = New System.Drawing.Point(8, 29)
        Me.txt_id_po_proses_2.Name = "txt_id_po_proses_2"
        Me.txt_id_po_proses_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_2.TabIndex = 1
        '
        'txt_id_po_proses_1
        '
        Me.txt_id_po_proses_1.Location = New System.Drawing.Point(8, 9)
        Me.txt_id_po_proses_1.Name = "txt_id_po_proses_1"
        Me.txt_id_po_proses_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_proses_1.TabIndex = 0
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.txt_id_beli_10)
        Me.Panel11.Controls.Add(Me.txt_id_beli_9)
        Me.Panel11.Controls.Add(Me.Label27)
        Me.Panel11.Controls.Add(Me.txt_id_beli_1)
        Me.Panel11.Controls.Add(Me.txt_id_beli_2)
        Me.Panel11.Controls.Add(Me.txt_id_beli_3)
        Me.Panel11.Controls.Add(Me.txt_id_beli_4)
        Me.Panel11.Controls.Add(Me.txt_id_beli_6)
        Me.Panel11.Controls.Add(Me.txt_id_beli_7)
        Me.Panel11.Controls.Add(Me.txt_id_beli_8)
        Me.Panel11.Controls.Add(Me.txt_id_beli_5)
        Me.Panel11.Location = New System.Drawing.Point(1221, 2)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(115, 129)
        Me.Panel11.TabIndex = 112
        '
        'txt_id_beli_10
        '
        Me.txt_id_beli_10.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_10.Location = New System.Drawing.Point(57, 86)
        Me.txt_id_beli_10.Name = "txt_id_beli_10"
        Me.txt_id_beli_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_10.TabIndex = 80
        Me.txt_id_beli_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_9
        '
        Me.txt_id_beli_9.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_9.Location = New System.Drawing.Point(57, 66)
        Me.txt_id_beli_9.Name = "txt_id_beli_9"
        Me.txt_id_beli_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_9.TabIndex = 79
        Me.txt_id_beli_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(38, 112)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(38, 13)
        Me.Label27.TabIndex = 78
        Me.Label27.Text = "ID Beli"
        '
        'txt_id_beli_1
        '
        Me.txt_id_beli_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_1.Location = New System.Drawing.Point(7, 6)
        Me.txt_id_beli_1.Name = "txt_id_beli_1"
        Me.txt_id_beli_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_1.TabIndex = 77
        Me.txt_id_beli_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_2
        '
        Me.txt_id_beli_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_2.Location = New System.Drawing.Point(7, 26)
        Me.txt_id_beli_2.Name = "txt_id_beli_2"
        Me.txt_id_beli_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_2.TabIndex = 77
        Me.txt_id_beli_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_3
        '
        Me.txt_id_beli_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_3.Location = New System.Drawing.Point(7, 46)
        Me.txt_id_beli_3.Name = "txt_id_beli_3"
        Me.txt_id_beli_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_3.TabIndex = 77
        Me.txt_id_beli_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_4
        '
        Me.txt_id_beli_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_4.Location = New System.Drawing.Point(7, 66)
        Me.txt_id_beli_4.Name = "txt_id_beli_4"
        Me.txt_id_beli_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_4.TabIndex = 77
        Me.txt_id_beli_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_6
        '
        Me.txt_id_beli_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_6.Location = New System.Drawing.Point(57, 6)
        Me.txt_id_beli_6.Name = "txt_id_beli_6"
        Me.txt_id_beli_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_6.TabIndex = 77
        Me.txt_id_beli_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_7
        '
        Me.txt_id_beli_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_7.Location = New System.Drawing.Point(57, 26)
        Me.txt_id_beli_7.Name = "txt_id_beli_7"
        Me.txt_id_beli_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_7.TabIndex = 77
        Me.txt_id_beli_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_8
        '
        Me.txt_id_beli_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_8.Location = New System.Drawing.Point(57, 46)
        Me.txt_id_beli_8.Name = "txt_id_beli_8"
        Me.txt_id_beli_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_8.TabIndex = 77
        Me.txt_id_beli_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_5
        '
        Me.txt_id_beli_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_5.Location = New System.Drawing.Point(7, 86)
        Me.txt_id_beli_5.Name = "txt_id_beli_5"
        Me.txt_id_beli_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_beli_5.TabIndex = 77
        Me.txt_id_beli_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.Label28)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_10)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_9)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_8)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_7)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_6)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_5)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_4)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_3)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_2)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_1)
        Me.Panel12.Location = New System.Drawing.Point(351, 2)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(109, 134)
        Me.Panel12.TabIndex = 113
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(22, 113)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(64, 13)
        Me.Label28.TabIndex = 10
        Me.Label28.Text = "Asal Satuan"
        '
        'txt_asal_satuan_10
        '
        Me.txt_asal_satuan_10.Location = New System.Drawing.Point(54, 89)
        Me.txt_asal_satuan_10.Name = "txt_asal_satuan_10"
        Me.txt_asal_satuan_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_10.TabIndex = 9
        '
        'txt_asal_satuan_9
        '
        Me.txt_asal_satuan_9.Location = New System.Drawing.Point(54, 69)
        Me.txt_asal_satuan_9.Name = "txt_asal_satuan_9"
        Me.txt_asal_satuan_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_9.TabIndex = 8
        '
        'txt_asal_satuan_8
        '
        Me.txt_asal_satuan_8.Location = New System.Drawing.Point(54, 49)
        Me.txt_asal_satuan_8.Name = "txt_asal_satuan_8"
        Me.txt_asal_satuan_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_8.TabIndex = 7
        '
        'txt_asal_satuan_7
        '
        Me.txt_asal_satuan_7.Location = New System.Drawing.Point(54, 29)
        Me.txt_asal_satuan_7.Name = "txt_asal_satuan_7"
        Me.txt_asal_satuan_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_7.TabIndex = 6
        '
        'txt_asal_satuan_6
        '
        Me.txt_asal_satuan_6.Location = New System.Drawing.Point(54, 9)
        Me.txt_asal_satuan_6.Name = "txt_asal_satuan_6"
        Me.txt_asal_satuan_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_6.TabIndex = 5
        '
        'txt_asal_satuan_5
        '
        Me.txt_asal_satuan_5.Location = New System.Drawing.Point(4, 89)
        Me.txt_asal_satuan_5.Name = "txt_asal_satuan_5"
        Me.txt_asal_satuan_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_5.TabIndex = 4
        '
        'txt_asal_satuan_4
        '
        Me.txt_asal_satuan_4.Location = New System.Drawing.Point(4, 69)
        Me.txt_asal_satuan_4.Name = "txt_asal_satuan_4"
        Me.txt_asal_satuan_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_4.TabIndex = 3
        '
        'txt_asal_satuan_3
        '
        Me.txt_asal_satuan_3.Location = New System.Drawing.Point(4, 49)
        Me.txt_asal_satuan_3.Name = "txt_asal_satuan_3"
        Me.txt_asal_satuan_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_3.TabIndex = 2
        '
        'txt_asal_satuan_2
        '
        Me.txt_asal_satuan_2.Location = New System.Drawing.Point(4, 29)
        Me.txt_asal_satuan_2.Name = "txt_asal_satuan_2"
        Me.txt_asal_satuan_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_2.TabIndex = 1
        '
        'txt_asal_satuan_1
        '
        Me.txt_asal_satuan_1.Location = New System.Drawing.Point(4, 9)
        Me.txt_asal_satuan_1.Name = "txt_asal_satuan_1"
        Me.txt_asal_satuan_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_1.TabIndex = 0
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Label29)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_10)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_9)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_8)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_7)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_6)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_5)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_4)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_3)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_2)
        Me.Panel13.Controls.Add(Me.txt_id_hutang_1)
        Me.Panel13.Location = New System.Drawing.Point(466, 2)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(90, 134)
        Me.Panel13.TabIndex = 114
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 113)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(56, 13)
        Me.Label29.TabIndex = 10
        Me.Label29.Text = "ID Hutang"
        '
        'txt_id_hutang_10
        '
        Me.txt_id_hutang_10.Location = New System.Drawing.Point(45, 89)
        Me.txt_id_hutang_10.Name = "txt_id_hutang_10"
        Me.txt_id_hutang_10.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_10.TabIndex = 9
        '
        'txt_id_hutang_9
        '
        Me.txt_id_hutang_9.Location = New System.Drawing.Point(45, 69)
        Me.txt_id_hutang_9.Name = "txt_id_hutang_9"
        Me.txt_id_hutang_9.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_9.TabIndex = 8
        '
        'txt_id_hutang_8
        '
        Me.txt_id_hutang_8.Location = New System.Drawing.Point(45, 49)
        Me.txt_id_hutang_8.Name = "txt_id_hutang_8"
        Me.txt_id_hutang_8.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_8.TabIndex = 7
        '
        'txt_id_hutang_7
        '
        Me.txt_id_hutang_7.Location = New System.Drawing.Point(45, 29)
        Me.txt_id_hutang_7.Name = "txt_id_hutang_7"
        Me.txt_id_hutang_7.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_7.TabIndex = 6
        '
        'txt_id_hutang_6
        '
        Me.txt_id_hutang_6.Location = New System.Drawing.Point(45, 9)
        Me.txt_id_hutang_6.Name = "txt_id_hutang_6"
        Me.txt_id_hutang_6.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_6.TabIndex = 5
        '
        'txt_id_hutang_5
        '
        Me.txt_id_hutang_5.Location = New System.Drawing.Point(5, 89)
        Me.txt_id_hutang_5.Name = "txt_id_hutang_5"
        Me.txt_id_hutang_5.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_5.TabIndex = 4
        '
        'txt_id_hutang_4
        '
        Me.txt_id_hutang_4.Location = New System.Drawing.Point(5, 69)
        Me.txt_id_hutang_4.Name = "txt_id_hutang_4"
        Me.txt_id_hutang_4.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_4.TabIndex = 3
        '
        'txt_id_hutang_3
        '
        Me.txt_id_hutang_3.Location = New System.Drawing.Point(5, 49)
        Me.txt_id_hutang_3.Name = "txt_id_hutang_3"
        Me.txt_id_hutang_3.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_3.TabIndex = 2
        '
        'txt_id_hutang_2
        '
        Me.txt_id_hutang_2.Location = New System.Drawing.Point(5, 29)
        Me.txt_id_hutang_2.Name = "txt_id_hutang_2"
        Me.txt_id_hutang_2.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_2.TabIndex = 1
        '
        'txt_id_hutang_1
        '
        Me.txt_id_hutang_1.Location = New System.Drawing.Point(5, 9)
        Me.txt_id_hutang_1.Name = "txt_id_hutang_1"
        Me.txt_id_hutang_1.Size = New System.Drawing.Size(40, 20)
        Me.txt_id_hutang_1.TabIndex = 0
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.Label30)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_10)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_9)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_8)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_7)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_6)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_5)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_4)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_3)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_2)
        Me.Panel14.Controls.Add(Me.txt_asal_satuan_wip_1)
        Me.Panel14.Location = New System.Drawing.Point(873, 1)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(90, 134)
        Me.Panel14.TabIndex = 115
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(1, 113)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(88, 13)
        Me.Label30.TabIndex = 10
        Me.Label30.Text = "Asal Satuan WIP"
        '
        'txt_asal_satuan_wip_10
        '
        Me.txt_asal_satuan_wip_10.Location = New System.Drawing.Point(45, 89)
        Me.txt_asal_satuan_wip_10.Name = "txt_asal_satuan_wip_10"
        Me.txt_asal_satuan_wip_10.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_10.TabIndex = 9
        '
        'txt_asal_satuan_wip_9
        '
        Me.txt_asal_satuan_wip_9.Location = New System.Drawing.Point(45, 69)
        Me.txt_asal_satuan_wip_9.Name = "txt_asal_satuan_wip_9"
        Me.txt_asal_satuan_wip_9.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_9.TabIndex = 8
        '
        'txt_asal_satuan_wip_8
        '
        Me.txt_asal_satuan_wip_8.Location = New System.Drawing.Point(45, 49)
        Me.txt_asal_satuan_wip_8.Name = "txt_asal_satuan_wip_8"
        Me.txt_asal_satuan_wip_8.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_8.TabIndex = 7
        '
        'txt_asal_satuan_wip_7
        '
        Me.txt_asal_satuan_wip_7.Location = New System.Drawing.Point(45, 29)
        Me.txt_asal_satuan_wip_7.Name = "txt_asal_satuan_wip_7"
        Me.txt_asal_satuan_wip_7.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_7.TabIndex = 6
        '
        'txt_asal_satuan_wip_6
        '
        Me.txt_asal_satuan_wip_6.Location = New System.Drawing.Point(45, 9)
        Me.txt_asal_satuan_wip_6.Name = "txt_asal_satuan_wip_6"
        Me.txt_asal_satuan_wip_6.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_6.TabIndex = 5
        '
        'txt_asal_satuan_wip_5
        '
        Me.txt_asal_satuan_wip_5.Location = New System.Drawing.Point(5, 89)
        Me.txt_asal_satuan_wip_5.Name = "txt_asal_satuan_wip_5"
        Me.txt_asal_satuan_wip_5.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_5.TabIndex = 4
        '
        'txt_asal_satuan_wip_4
        '
        Me.txt_asal_satuan_wip_4.Location = New System.Drawing.Point(5, 69)
        Me.txt_asal_satuan_wip_4.Name = "txt_asal_satuan_wip_4"
        Me.txt_asal_satuan_wip_4.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_4.TabIndex = 3
        '
        'txt_asal_satuan_wip_3
        '
        Me.txt_asal_satuan_wip_3.Location = New System.Drawing.Point(5, 49)
        Me.txt_asal_satuan_wip_3.Name = "txt_asal_satuan_wip_3"
        Me.txt_asal_satuan_wip_3.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_3.TabIndex = 2
        '
        'txt_asal_satuan_wip_2
        '
        Me.txt_asal_satuan_wip_2.Location = New System.Drawing.Point(5, 29)
        Me.txt_asal_satuan_wip_2.Name = "txt_asal_satuan_wip_2"
        Me.txt_asal_satuan_wip_2.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_2.TabIndex = 1
        '
        'txt_asal_satuan_wip_1
        '
        Me.txt_asal_satuan_wip_1.Location = New System.Drawing.Point(5, 9)
        Me.txt_asal_satuan_wip_1.Name = "txt_asal_satuan_wip_1"
        Me.txt_asal_satuan_wip_1.Size = New System.Drawing.Size(40, 20)
        Me.txt_asal_satuan_wip_1.TabIndex = 0
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.Label31)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_10)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_9)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_8)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_7)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_6)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_5)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_4)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_3)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_2)
        Me.Panel15.Controls.Add(Me.txt_asal_satuan_rubah_1)
        Me.Panel15.Location = New System.Drawing.Point(763, 2)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(109, 134)
        Me.Panel15.TabIndex = 116
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(5, 113)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(99, 13)
        Me.Label31.TabIndex = 10
        Me.Label31.Text = "Asal Satuan Rubah"
        '
        'txt_asal_satuan_rubah_10
        '
        Me.txt_asal_satuan_rubah_10.Location = New System.Drawing.Point(54, 89)
        Me.txt_asal_satuan_rubah_10.Name = "txt_asal_satuan_rubah_10"
        Me.txt_asal_satuan_rubah_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_10.TabIndex = 9
        '
        'txt_asal_satuan_rubah_9
        '
        Me.txt_asal_satuan_rubah_9.Location = New System.Drawing.Point(54, 69)
        Me.txt_asal_satuan_rubah_9.Name = "txt_asal_satuan_rubah_9"
        Me.txt_asal_satuan_rubah_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_9.TabIndex = 8
        '
        'txt_asal_satuan_rubah_8
        '
        Me.txt_asal_satuan_rubah_8.Location = New System.Drawing.Point(54, 49)
        Me.txt_asal_satuan_rubah_8.Name = "txt_asal_satuan_rubah_8"
        Me.txt_asal_satuan_rubah_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_8.TabIndex = 7
        '
        'txt_asal_satuan_rubah_7
        '
        Me.txt_asal_satuan_rubah_7.Location = New System.Drawing.Point(54, 29)
        Me.txt_asal_satuan_rubah_7.Name = "txt_asal_satuan_rubah_7"
        Me.txt_asal_satuan_rubah_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_7.TabIndex = 6
        '
        'txt_asal_satuan_rubah_6
        '
        Me.txt_asal_satuan_rubah_6.Location = New System.Drawing.Point(54, 9)
        Me.txt_asal_satuan_rubah_6.Name = "txt_asal_satuan_rubah_6"
        Me.txt_asal_satuan_rubah_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_6.TabIndex = 5
        '
        'txt_asal_satuan_rubah_5
        '
        Me.txt_asal_satuan_rubah_5.Location = New System.Drawing.Point(4, 89)
        Me.txt_asal_satuan_rubah_5.Name = "txt_asal_satuan_rubah_5"
        Me.txt_asal_satuan_rubah_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_5.TabIndex = 4
        '
        'txt_asal_satuan_rubah_4
        '
        Me.txt_asal_satuan_rubah_4.Location = New System.Drawing.Point(4, 69)
        Me.txt_asal_satuan_rubah_4.Name = "txt_asal_satuan_rubah_4"
        Me.txt_asal_satuan_rubah_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_4.TabIndex = 3
        '
        'txt_asal_satuan_rubah_3
        '
        Me.txt_asal_satuan_rubah_3.Location = New System.Drawing.Point(4, 49)
        Me.txt_asal_satuan_rubah_3.Name = "txt_asal_satuan_rubah_3"
        Me.txt_asal_satuan_rubah_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_3.TabIndex = 2
        '
        'txt_asal_satuan_rubah_2
        '
        Me.txt_asal_satuan_rubah_2.Location = New System.Drawing.Point(4, 29)
        Me.txt_asal_satuan_rubah_2.Name = "txt_asal_satuan_rubah_2"
        Me.txt_asal_satuan_rubah_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_2.TabIndex = 1
        '
        'txt_asal_satuan_rubah_1
        '
        Me.txt_asal_satuan_rubah_1.Location = New System.Drawing.Point(4, 9)
        Me.txt_asal_satuan_rubah_1.Name = "txt_asal_satuan_rubah_1"
        Me.txt_asal_satuan_rubah_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_rubah_1.TabIndex = 0
        '
        'form_input_surat_jalan_proses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1336, 618)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel13)
        Me.Controls.Add(Me.Panel12)
        Me.Controls.Add(Me.Panel11)
        Me.Controls.Add(Me.Panel10)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel_10)
        Me.Controls.Add(Me.Panel_9)
        Me.Controls.Add(Me.Panel_8)
        Me.Controls.Add(Me.Panel_7)
        Me.Controls.Add(Me.Panel_6)
        Me.Controls.Add(Me.Panel_5)
        Me.Controls.Add(Me.Panel_4)
        Me.Controls.Add(Me.Panel_3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel_1)
        Me.Controls.Add(Me.Panel_2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel14)
        Me.Controls.Add(Me.Panel15)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_surat_jalan_proses"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM INPUT SURAT JALAN PROSES BARU"
        Me.Panel1.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel_10.ResumeLayout(False)
        Me.Panel_10.PerformLayout()
        Me.Panel_9.ResumeLayout(False)
        Me.Panel_9.PerformLayout()
        Me.Panel_8.ResumeLayout(False)
        Me.Panel_8.PerformLayout()
        Me.Panel_7.ResumeLayout(False)
        Me.Panel_7.PerformLayout()
        Me.Panel_6.ResumeLayout(False)
        Me.Panel_6.PerformLayout()
        Me.Panel_5.ResumeLayout(False)
        Me.Panel_5.PerformLayout()
        Me.Panel_4.ResumeLayout(False)
        Me.Panel_4.PerformLayout()
        Me.Panel_3.ResumeLayout(False)
        Me.Panel_3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel_1.ResumeLayout(False)
        Me.Panel_1.PerformLayout()
        Me.Panel_2.ResumeLayout(False)
        Me.Panel_2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents txt_harga_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txt_id_grey_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txt_qty_asal_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_10 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_10 As System.Windows.Forms.Label
    Friend WithEvents Panel_9 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_9 As System.Windows.Forms.Label
    Friend WithEvents Panel_8 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_8 As System.Windows.Forms.Label
    Friend WithEvents Panel_7 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_7 As System.Windows.Forms.Label
    Friend WithEvents Panel_6 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_6 As System.Windows.Forms.Label
    Friend WithEvents Panel_5 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_5 As System.Windows.Forms.Label
    Friend WithEvents Panel_4 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_4 As System.Windows.Forms.Label
    Friend WithEvents Panel_3 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_3 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel_1 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_1 As System.Windows.Forms.Label
    Friend WithEvents Panel_2 As System.Windows.Forms.Panel
    Friend WithEvents txt_kiloan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_proses_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_2 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btn_hapus_10 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_10 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_9 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_9 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_9 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_8 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_8 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_8 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_7 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_7 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_7 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_6 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_6 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_6 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_5 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_5 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_5 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_4 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_4 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_4 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_3 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_3 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_3 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_2 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_1 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_1 As System.Windows.Forms.Button
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_gulung_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_3 As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_gramasi_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gramasi_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_packing_2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents txt_sisa_qty_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents txt_id_sj_proses_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents dtp_jatuh_tempo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_surat_jalan_proses As System.Windows.Forms.TextBox
    Friend WithEvents dtp_tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_id_po_proses_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_1 As System.Windows.Forms.TextBox
    Friend WithEvents cb_satuan_10 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_9 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_8 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_7 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_6 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_5 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_4 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_3 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_1 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_2 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_meter_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_3 As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txt_meter_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_2 As System.Windows.Forms.TextBox
    Friend WithEvents btn_batal_10 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_9 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_8 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_7 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_6 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_5 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_4 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_3 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_2 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_1 As System.Windows.Forms.Button
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents txt_id_beli_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_9 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txt_id_beli_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_5 As System.Windows.Forms.TextBox
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_satuan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txt_id_hutang_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_satuan_wip_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_wip_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel15 As System.Windows.Forms.Panel
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_satuan_rubah_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_rubah_1 As System.Windows.Forms.TextBox
End Class
