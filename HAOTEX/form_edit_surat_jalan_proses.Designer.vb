﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_edit_surat_jalan_proses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_harga_wip_proses = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_sisa_meter = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_meter = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_gudang_packing = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_resep = New System.Windows.Forms.TextBox()
        Me.dtp_tanggal = New System.Windows.Forms.DateTimePicker()
        Me.txt_no_po = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_baris_po = New System.Windows.Forms.TextBox()
        Me.txt_surat_jalan = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_total_harga_1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_warna = New System.Windows.Forms.TextBox()
        Me.txt_qty_po_proses = New System.Windows.Forms.TextBox()
        Me.txt_sj_pembelian = New System.Windows.Forms.TextBox()
        Me.txt_gudang_awal = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dtp_jatuh_tempo_1 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_1 = New System.Windows.Forms.DateTimePicker()
        Me.txt_id_grey = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_partai = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_gulung = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_gudang = New System.Windows.Forms.TextBox()
        Me.txt_keterangan = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dtp_jatuh_tempo = New System.Windows.Forms.DateTimePicker()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_total_harga = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_harga = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_gramasi = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_kg = New System.Windows.Forms.TextBox()
        Me.txt_baris_sj = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btn_simpan_tutup = New System.Windows.Forms.Button()
        Me.btn_batal = New System.Windows.Forms.Button()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_harga_wip_proses
        '
        Me.txt_harga_wip_proses.Location = New System.Drawing.Point(114, 43)
        Me.txt_harga_wip_proses.Name = "txt_harga_wip_proses"
        Me.txt_harga_wip_proses.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_wip_proses.TabIndex = 83
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(264, 340)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(27, 13)
        Me.Label13.TabIndex = 105
        Me.Label13.Text = "Sisa"
        '
        'txt_sisa_meter
        '
        Me.txt_sisa_meter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_meter.Location = New System.Drawing.Point(293, 336)
        Me.txt_sisa_meter.Name = "txt_sisa_meter"
        Me.txt_sisa_meter.ReadOnly = True
        Me.txt_sisa_meter.Size = New System.Drawing.Size(110, 20)
        Me.txt_sisa_meter.TabIndex = 76
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(38, 340)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(34, 13)
        Me.Label11.TabIndex = 104
        Me.Label11.Text = "Meter"
        '
        'txt_meter
        '
        Me.txt_meter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter.Location = New System.Drawing.Point(131, 336)
        Me.txt_meter.Name = "txt_meter"
        Me.txt_meter.Size = New System.Drawing.Size(110, 20)
        Me.txt_meter.TabIndex = 103
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(38, 448)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(87, 13)
        Me.Label14.TabIndex = 102
        Me.Label14.Text = "Gudang Packing"
        '
        'txt_gudang_packing
        '
        Me.txt_gudang_packing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_packing.Location = New System.Drawing.Point(131, 444)
        Me.txt_gudang_packing.Name = "txt_gudang_packing"
        Me.txt_gudang_packing.Size = New System.Drawing.Size(272, 20)
        Me.txt_gudang_packing.TabIndex = 101
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(38, 259)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(38, 13)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Resep"
        '
        'txt_resep
        '
        Me.txt_resep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep.Location = New System.Drawing.Point(131, 255)
        Me.txt_resep.Name = "txt_resep"
        Me.txt_resep.ReadOnly = True
        Me.txt_resep.Size = New System.Drawing.Size(271, 20)
        Me.txt_resep.TabIndex = 40
        '
        'dtp_tanggal
        '
        Me.dtp_tanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal.Location = New System.Drawing.Point(131, 14)
        Me.dtp_tanggal.Name = "dtp_tanggal"
        Me.dtp_tanggal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_tanggal.TabIndex = 16
        '
        'txt_no_po
        '
        Me.txt_no_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po.Location = New System.Drawing.Point(131, 147)
        Me.txt_no_po.Name = "txt_no_po"
        Me.txt_no_po.ReadOnly = True
        Me.txt_no_po.Size = New System.Drawing.Size(271, 20)
        Me.txt_no_po.TabIndex = 100
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(38, 475)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Keterangan"
        '
        'txt_baris_po
        '
        Me.txt_baris_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_baris_po.Location = New System.Drawing.Point(131, 174)
        Me.txt_baris_po.Name = "txt_baris_po"
        Me.txt_baris_po.ReadOnly = True
        Me.txt_baris_po.Size = New System.Drawing.Size(271, 20)
        Me.txt_baris_po.TabIndex = 0
        '
        'txt_surat_jalan
        '
        Me.txt_surat_jalan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_surat_jalan.Location = New System.Drawing.Point(131, 93)
        Me.txt_surat_jalan.Name = "txt_surat_jalan"
        Me.txt_surat_jalan.ReadOnly = True
        Me.txt_surat_jalan.Size = New System.Drawing.Size(271, 20)
        Me.txt_surat_jalan.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(38, 70)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Gudang"
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain.Location = New System.Drawing.Point(131, 201)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.ReadOnly = True
        Me.txt_jenis_kain.Size = New System.Drawing.Size(271, 20)
        Me.txt_jenis_kain.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(38, 313)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Gulung"
        '
        'txt_total_harga_1
        '
        Me.txt_total_harga_1.Location = New System.Drawing.Point(13, 43)
        Me.txt_total_harga_1.Name = "txt_total_harga_1"
        Me.txt_total_harga_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_total_harga_1.TabIndex = 82
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 25)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Label1"
        '
        'txt_warna
        '
        Me.txt_warna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna.Location = New System.Drawing.Point(131, 228)
        Me.txt_warna.Name = "txt_warna"
        Me.txt_warna.ReadOnly = True
        Me.txt_warna.Size = New System.Drawing.Size(271, 20)
        Me.txt_warna.TabIndex = 15
        '
        'txt_qty_po_proses
        '
        Me.txt_qty_po_proses.Location = New System.Drawing.Point(128, 19)
        Me.txt_qty_po_proses.Name = "txt_qty_po_proses"
        Me.txt_qty_po_proses.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_po_proses.TabIndex = 86
        '
        'txt_sj_pembelian
        '
        Me.txt_sj_pembelian.Location = New System.Drawing.Point(22, 19)
        Me.txt_sj_pembelian.Name = "txt_sj_pembelian"
        Me.txt_sj_pembelian.Size = New System.Drawing.Size(100, 20)
        Me.txt_sj_pembelian.TabIndex = 85
        '
        'txt_gudang_awal
        '
        Me.txt_gudang_awal.Location = New System.Drawing.Point(215, 43)
        Me.txt_gudang_awal.Name = "txt_gudang_awal"
        Me.txt_gudang_awal.Size = New System.Drawing.Size(100, 20)
        Me.txt_gudang_awal.TabIndex = 84
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dtp_jatuh_tempo_1)
        Me.Panel2.Controls.Add(Me.dtp_tanggal_1)
        Me.Panel2.Controls.Add(Me.txt_qty_po_proses)
        Me.Panel2.Controls.Add(Me.txt_sj_pembelian)
        Me.Panel2.Controls.Add(Me.txt_gudang_awal)
        Me.Panel2.Controls.Add(Me.txt_harga_wip_proses)
        Me.Panel2.Controls.Add(Me.txt_total_harga_1)
        Me.Panel2.Controls.Add(Me.txt_id_grey)
        Me.Panel2.Location = New System.Drawing.Point(5, 50)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(428, 70)
        Me.Panel2.TabIndex = 93
        '
        'dtp_jatuh_tempo_1
        '
        Me.dtp_jatuh_tempo_1.CustomFormat = "dd/MM/yyyy"
        Me.dtp_jatuh_tempo_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo_1.Location = New System.Drawing.Point(316, 43)
        Me.dtp_jatuh_tempo_1.Name = "dtp_jatuh_tempo_1"
        Me.dtp_jatuh_tempo_1.Size = New System.Drawing.Size(100, 20)
        Me.dtp_jatuh_tempo_1.TabIndex = 120
        '
        'dtp_tanggal_1
        '
        Me.dtp_tanggal_1.CustomFormat = "dd/MM/yyyy"
        Me.dtp_tanggal_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_1.Location = New System.Drawing.Point(234, 19)
        Me.dtp_tanggal_1.Name = "dtp_tanggal_1"
        Me.dtp_tanggal_1.Size = New System.Drawing.Size(100, 20)
        Me.dtp_tanggal_1.TabIndex = 87
        '
        'txt_id_grey
        '
        Me.txt_id_grey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey.Location = New System.Drawing.Point(340, 19)
        Me.txt_id_grey.Name = "txt_id_grey"
        Me.txt_id_grey.ReadOnly = True
        Me.txt_id_grey.Size = New System.Drawing.Size(66, 20)
        Me.txt_id_grey.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(38, 286)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "Partai"
        '
        'txt_partai
        '
        Me.txt_partai.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai.Location = New System.Drawing.Point(131, 282)
        Me.txt_partai.Name = "txt_partai"
        Me.txt_partai.Size = New System.Drawing.Size(271, 20)
        Me.txt_partai.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(38, 232)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Warna"
        '
        'txt_gulung
        '
        Me.txt_gulung.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung.Location = New System.Drawing.Point(131, 309)
        Me.txt_gulung.Name = "txt_gulung"
        Me.txt_gulung.Size = New System.Drawing.Size(272, 20)
        Me.txt_gulung.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(38, 205)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Jenis Kain"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(38, 178)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(48, 13)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Baris PO"
        '
        'txt_gudang
        '
        Me.txt_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang.Location = New System.Drawing.Point(131, 66)
        Me.txt_gudang.Name = "txt_gudang"
        Me.txt_gudang.ReadOnly = True
        Me.txt_gudang.Size = New System.Drawing.Size(271, 20)
        Me.txt_gudang.TabIndex = 19
        '
        'txt_keterangan
        '
        Me.txt_keterangan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan.Location = New System.Drawing.Point(131, 471)
        Me.txt_keterangan.Name = "txt_keterangan"
        Me.txt_keterangan.Size = New System.Drawing.Size(271, 20)
        Me.txt_keterangan.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(38, 151)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "No PO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(38, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Tanggal"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Window
        Me.Panel1.Controls.Add(Me.dtp_jatuh_tempo)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Controls.Add(Me.Label22)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.txt_total_harga)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.txt_harga)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.txt_gramasi)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.txt_kg)
        Me.Panel1.Controls.Add(Me.txt_baris_sj)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.txt_sisa_meter)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.txt_meter)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.txt_gudang_packing)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txt_resep)
        Me.Panel1.Controls.Add(Me.dtp_tanggal)
        Me.Panel1.Controls.Add(Me.txt_no_po)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txt_baris_po)
        Me.Panel1.Controls.Add(Me.txt_surat_jalan)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txt_jenis_kain)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txt_warna)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txt_partai)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txt_gulung)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txt_gudang)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_keterangan)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(-1, 57)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(441, 505)
        Me.Panel1.TabIndex = 90
        '
        'dtp_jatuh_tempo
        '
        Me.dtp_jatuh_tempo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo.Location = New System.Drawing.Point(131, 40)
        Me.dtp_jatuh_tempo.Name = "dtp_jatuh_tempo"
        Me.dtp_jatuh_tempo.Size = New System.Drawing.Size(99, 20)
        Me.dtp_jatuh_tempo.TabIndex = 118
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(38, 44)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(69, 13)
        Me.Label23.TabIndex = 119
        Me.Label23.Text = "Jatuh Tempo"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(131, 421)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(24, 13)
        Me.Label22.TabIndex = 117
        Me.Label22.Text = "Rp."
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(131, 394)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(24, 13)
        Me.Label21.TabIndex = 116
        Me.Label21.Text = "Rp."
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(38, 421)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(63, 13)
        Me.Label19.TabIndex = 115
        Me.Label19.Text = "Total Harga"
        '
        'txt_total_harga
        '
        Me.txt_total_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga.Location = New System.Drawing.Point(157, 417)
        Me.txt_total_harga.Name = "txt_total_harga"
        Me.txt_total_harga.Size = New System.Drawing.Size(246, 20)
        Me.txt_total_harga.TabIndex = 114
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(38, 394)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(36, 13)
        Me.Label20.TabIndex = 113
        Me.Label20.Text = "Harga"
        '
        'txt_harga
        '
        Me.txt_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga.Location = New System.Drawing.Point(157, 390)
        Me.txt_harga.Name = "txt_harga"
        Me.txt_harga.Size = New System.Drawing.Size(245, 20)
        Me.txt_harga.TabIndex = 112
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(264, 367)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(45, 13)
        Me.Label18.TabIndex = 111
        Me.Label18.Text = "Gramasi"
        '
        'txt_gramasi
        '
        Me.txt_gramasi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gramasi.Location = New System.Drawing.Point(314, 363)
        Me.txt_gramasi.Name = "txt_gramasi"
        Me.txt_gramasi.ReadOnly = True
        Me.txt_gramasi.Size = New System.Drawing.Size(89, 20)
        Me.txt_gramasi.TabIndex = 110
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(38, 367)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(20, 13)
        Me.Label16.TabIndex = 109
        Me.Label16.Text = "Kg"
        '
        'txt_kg
        '
        Me.txt_kg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_kg.Location = New System.Drawing.Point(131, 363)
        Me.txt_kg.Name = "txt_kg"
        Me.txt_kg.Size = New System.Drawing.Size(110, 20)
        Me.txt_kg.TabIndex = 108
        '
        'txt_baris_sj
        '
        Me.txt_baris_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_baris_sj.Location = New System.Drawing.Point(131, 120)
        Me.txt_baris_sj.Name = "txt_baris_sj"
        Me.txt_baris_sj.ReadOnly = True
        Me.txt_baris_sj.Size = New System.Drawing.Size(271, 20)
        Me.txt_baris_sj.TabIndex = 106
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(38, 124)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(45, 13)
        Me.Label15.TabIndex = 107
        Me.Label15.Text = "Baris SJ"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(38, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Surat Jalan"
        '
        'btn_simpan_tutup
        '
        Me.btn_simpan_tutup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_tutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_tutup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_tutup.Image = Global.HAOTEX.My.Resources.Resources.save1
        Me.btn_simpan_tutup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_tutup.Location = New System.Drawing.Point(172, 578)
        Me.btn_simpan_tutup.Name = "btn_simpan_tutup"
        Me.btn_simpan_tutup.Size = New System.Drawing.Size(127, 23)
        Me.btn_simpan_tutup.TabIndex = 91
        Me.btn_simpan_tutup.Text = "Simpan & Tutup"
        Me.btn_simpan_tutup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_tutup.UseMnemonic = False
        Me.btn_simpan_tutup.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Image = Global.HAOTEX.My.Resources.Resources.action_delete
        Me.btn_batal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal.Location = New System.Drawing.Point(338, 578)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(64, 23)
        Me.btn_batal.TabIndex = 92
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal.UseMnemonic = False
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'form_edit_surat_jalan_proses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(439, 619)
        Me.Controls.Add(Me.btn_simpan_tutup)
        Me.Controls.Add(Me.btn_batal)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_edit_surat_jalan_proses"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_harga_wip_proses As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txt_sisa_meter As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_meter As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_gudang_packing As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txt_resep As System.Windows.Forms.TextBox
    Friend WithEvents dtp_tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_no_po As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_baris_po As System.Windows.Forms.TextBox
    Friend WithEvents txt_surat_jalan As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btn_simpan_tutup As System.Windows.Forms.Button
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_total_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_warna As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_po_proses As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_pembelian As System.Windows.Forms.TextBox
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents txt_gudang_awal As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dtp_tanggal_1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_id_grey As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_partai As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_gulung As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_gudang As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_kg As System.Windows.Forms.TextBox
    Friend WithEvents txt_baris_sj As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_gramasi As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txt_total_harga As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txt_harga As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents dtp_jatuh_tempo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents dtp_jatuh_tempo_1 As System.Windows.Forms.DateTimePicker
End Class
