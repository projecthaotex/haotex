﻿Imports MySql.Data.MySqlClient

Public Class form_hutang

    Private Sub form_hutang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub

    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Nama"
        dgv1.Columns(1).HeaderText = "Sisa Hutang"
        dgv1.Columns(0).Width = 150
        dgv1.Columns(1).Width = 150
        dgv1.Columns(1).DefaultCellStyle.Format = "C"
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Nama,Sisa_Hutang FROM tbhutang WHERE NOT Sisa_Hutang=0 ORDER BY Sisa_Hutang ASC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbhutang")
                            dgv1.DataSource = dsx.Tables("tbhutang")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub isidgv_Nama()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Nama,Sisa_Hutang FROM tbhutang WHERE NOT Sisa_Hutang=0 AND Nama like '%" & txt_cari_nama.Text & "%'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbhutang")
                        dgv1.DataSource = dsx.Tables("tbhutang")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub txt_cari_nama_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_nama.GotFocus
        If txt_cari_nama.Text = "< Nama >" Then
            txt_cari_nama.Text = ""
        End If
    End Sub

    Private Sub txt_cari_nama_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_nama.LostFocus
        If txt_cari_nama.Text = "" Then
            txt_cari_nama.Text = "< Nama >"
        End If
    End Sub

    Private Sub txt_cari_nama_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_nama.TextChanged
        If txt_cari_nama.Text = "< Nama >" Then

        ElseIf txt_cari_nama.Text = "" Then
            Call isidgv()
        Else
            Call isidgv_Nama()
        End If
    End Sub
End Class