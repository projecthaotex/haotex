﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_stok_claim_celup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_customer = New System.Windows.Forms.TextBox()
        Me.txt_form = New System.Windows.Forms.TextBox()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.Panel1.SuspendLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_cari_customer)
        Me.Panel1.Location = New System.Drawing.Point(12, 11)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(960, 42)
        Me.Panel1.TabIndex = 28
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(543, 10)
        Me.txt_cari_jenis_kain.MaxLength = 100
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 9
        Me.txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Me.txt_cari_jenis_kain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_cari_customer
        '
        Me.txt_cari_customer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_customer.Location = New System.Drawing.Point(244, 10)
        Me.txt_cari_customer.MaxLength = 100
        Me.txt_cari_customer.Name = "txt_cari_customer"
        Me.txt_cari_customer.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_customer.TabIndex = 8
        Me.txt_cari_customer.Text = "< Customer >"
        Me.txt_cari_customer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_form
        '
        Me.txt_form.Location = New System.Drawing.Point(800, 16)
        Me.txt_form.Name = "txt_form"
        Me.txt_form.Size = New System.Drawing.Size(100, 20)
        Me.txt_form.TabIndex = 27
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(12, 56)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(960, 345)
        Me.dgv1.TabIndex = 26
        '
        'form_stok_claim_celup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 413)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txt_form)
        Me.Controls.Add(Me.dgv1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_stok_claim_celup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM STOK CLAIM CELUP"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_customer As System.Windows.Forms.TextBox
    Friend WithEvents txt_form As System.Windows.Forms.TextBox
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
End Class
