﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_po_proses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtp_akhir = New System.Windows.Forms.DateTimePicker()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_cari_no_po = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dtp_po = New System.Windows.Forms.DateTimePicker()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_gudang = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_baru = New System.Windows.Forms.ToolStripButton()
        Me.ts_ubah = New System.Windows.Forms.ToolStripButton()
        Me.ts_hapus = New System.Windows.Forms.ToolStripButton()
        Me.ts_perbarui = New System.Windows.Forms.ToolStripButton()
        Me.ts_print = New System.Windows.Forms.ToolStripButton()
        Me.ts_print_po = New System.Windows.Forms.ToolStripButton()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.txt_note_po = New System.Windows.Forms.RichTextBox()
        Me.btn_update_note_po = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txt_total_qty_yard = New System.Windows.Forms.TextBox()
        Me.txt_keluar_sj_yard = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_belum_sj_yard = New System.Windows.Forms.TextBox()
        Me.txt_keluar_sj = New System.Windows.Forms.TextBox()
        Me.txt_belum_sj = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_total_qty = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label6.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Window
        Me.Label6.Location = New System.Drawing.Point(12, 31)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(1281, 34)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "PO PROSES"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 77)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "s/d"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Dari"
        '
        'dtp_akhir
        '
        Me.dtp_akhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_akhir.Location = New System.Drawing.Point(43, 73)
        Me.dtp_akhir.Name = "dtp_akhir"
        Me.dtp_akhir.Size = New System.Drawing.Size(99, 20)
        Me.dtp_akhir.TabIndex = 5
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(43, 46)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tanggal"
        '
        'txt_cari_no_po
        '
        Me.txt_cari_no_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_no_po.Location = New System.Drawing.Point(101, 168)
        Me.txt_cari_no_po.Name = "txt_cari_no_po"
        Me.txt_cari_no_po.Size = New System.Drawing.Size(84, 20)
        Me.txt_cari_no_po.TabIndex = 1
        Me.txt_cari_no_po.Text = "< No PO >"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.dtp_po)
        Me.Panel1.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_cari_gudang)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.dtp_akhir)
        Me.Panel1.Controls.Add(Me.dtp_awal)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txt_cari_no_po)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 67)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 465)
        Me.Panel1.TabIndex = 14
        '
        'dtp_po
        '
        Me.dtp_po.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_po.Location = New System.Drawing.Point(14, 168)
        Me.dtp_po.Name = "dtp_po"
        Me.dtp_po.Size = New System.Drawing.Size(81, 20)
        Me.dtp_po.TabIndex = 10
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(14, 255)
        Me.txt_cari_jenis_kain.MaxLength = 100
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 9
        Me.txt_cari_jenis_kain.Text = "< Jenis Kain >"
        '
        'txt_cari_gudang
        '
        Me.txt_cari_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_gudang.Location = New System.Drawing.Point(14, 212)
        Me.txt_cari_gudang.MaxLength = 100
        Me.txt_cari_gudang.Name = "txt_cari_gudang"
        Me.txt_cari_gudang.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_gudang.TabIndex = 8
        Me.txt_cari_gudang.Text = "< Gudang >"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 141)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Pencarian :"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_baru, Me.ts_ubah, Me.ts_hapus, Me.ts_perbarui, Me.ts_print, Me.ts_print_po})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1305, 25)
        Me.ToolStrip1.TabIndex = 13
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_baru
        '
        Me.ts_baru.Image = Global.HAOTEX.My.Resources.Resources.application_add
        Me.ts_baru.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_baru.Name = "ts_baru"
        Me.ts_baru.Size = New System.Drawing.Size(51, 22)
        Me.ts_baru.Text = "Baru"
        '
        'ts_ubah
        '
        Me.ts_ubah.Image = Global.HAOTEX.My.Resources.Resources.application_edit
        Me.ts_ubah.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_ubah.Name = "ts_ubah"
        Me.ts_ubah.Size = New System.Drawing.Size(55, 22)
        Me.ts_ubah.Text = "Ubah"
        '
        'ts_hapus
        '
        Me.ts_hapus.Image = Global.HAOTEX.My.Resources.Resources.application_remove
        Me.ts_hapus.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_hapus.Name = "ts_hapus"
        Me.ts_hapus.Size = New System.Drawing.Size(61, 22)
        Me.ts_hapus.Text = "Hapus"
        '
        'ts_perbarui
        '
        Me.ts_perbarui.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.ts_perbarui.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_perbarui.Name = "ts_perbarui"
        Me.ts_perbarui.Size = New System.Drawing.Size(71, 22)
        Me.ts_perbarui.Text = "Perbarui"
        '
        'ts_print
        '
        Me.ts_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print.Name = "ts_print"
        Me.ts_print.Size = New System.Drawing.Size(52, 22)
        Me.ts_print.Text = "Print"
        '
        'ts_print_po
        '
        Me.ts_print_po.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print_po.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print_po.Name = "ts_print_po"
        Me.ts_print_po.Size = New System.Drawing.Size(71, 22)
        Me.ts_print_po.Text = "Print PO"
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(214, 67)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(1079, 465)
        Me.dgv1.TabIndex = 12
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1310, 6)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(104, 20)
        Me.dtp_hari_ini.TabIndex = 17
        '
        'txt_note_po
        '
        Me.txt_note_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_po.Location = New System.Drawing.Point(6, 3)
        Me.txt_note_po.Name = "txt_note_po"
        Me.txt_note_po.Size = New System.Drawing.Size(309, 58)
        Me.txt_note_po.TabIndex = 19
        Me.txt_note_po.Text = ""
        '
        'btn_update_note_po
        '
        Me.btn_update_note_po.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_update_note_po.Location = New System.Drawing.Point(321, 12)
        Me.btn_update_note_po.Name = "btn_update_note_po"
        Me.btn_update_note_po.Size = New System.Drawing.Size(100, 40)
        Me.btn_update_note_po.TabIndex = 20
        Me.btn_update_note_po.Text = "Ubah Note PO"
        Me.btn_update_note_po.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.txt_total_qty_yard)
        Me.Panel3.Controls.Add(Me.txt_keluar_sj_yard)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.txt_belum_sj_yard)
        Me.Panel3.Controls.Add(Me.txt_keluar_sj)
        Me.Panel3.Controls.Add(Me.txt_belum_sj)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.txt_total_qty)
        Me.Panel3.Location = New System.Drawing.Point(443, 534)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(850, 66)
        Me.Panel3.TabIndex = 21
        '
        'txt_total_qty_yard
        '
        Me.txt_total_qty_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_qty_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_qty_yard.Location = New System.Drawing.Point(701, 30)
        Me.txt_total_qty_yard.Name = "txt_total_qty_yard"
        Me.txt_total_qty_yard.ReadOnly = True
        Me.txt_total_qty_yard.Size = New System.Drawing.Size(120, 20)
        Me.txt_total_qty_yard.TabIndex = 34
        Me.txt_total_qty_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keluar_sj_yard
        '
        Me.txt_keluar_sj_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_keluar_sj_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keluar_sj_yard.Location = New System.Drawing.Point(576, 30)
        Me.txt_keluar_sj_yard.Name = "txt_keluar_sj_yard"
        Me.txt_keluar_sj_yard.ReadOnly = True
        Me.txt_keluar_sj_yard.Size = New System.Drawing.Size(120, 20)
        Me.txt_keluar_sj_yard.TabIndex = 33
        Me.txt_keluar_sj_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(576, 14)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(121, 13)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "Sudah Keluar SJ Proses"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(452, 14)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(119, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Belum Keluar SJ Proses"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(706, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(110, 13)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Total Quantity ( Yard )"
        '
        'txt_belum_sj_yard
        '
        Me.txt_belum_sj_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_belum_sj_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_belum_sj_yard.Location = New System.Drawing.Point(451, 30)
        Me.txt_belum_sj_yard.Name = "txt_belum_sj_yard"
        Me.txt_belum_sj_yard.ReadOnly = True
        Me.txt_belum_sj_yard.Size = New System.Drawing.Size(120, 20)
        Me.txt_belum_sj_yard.TabIndex = 29
        Me.txt_belum_sj_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keluar_sj
        '
        Me.txt_keluar_sj.BackColor = System.Drawing.SystemColors.Window
        Me.txt_keluar_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keluar_sj.Location = New System.Drawing.Point(152, 30)
        Me.txt_keluar_sj.Name = "txt_keluar_sj"
        Me.txt_keluar_sj.ReadOnly = True
        Me.txt_keluar_sj.Size = New System.Drawing.Size(120, 20)
        Me.txt_keluar_sj.TabIndex = 28
        Me.txt_keluar_sj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_belum_sj
        '
        Me.txt_belum_sj.BackColor = System.Drawing.SystemColors.Window
        Me.txt_belum_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_belum_sj.Location = New System.Drawing.Point(27, 30)
        Me.txt_belum_sj.Name = "txt_belum_sj"
        Me.txt_belum_sj.ReadOnly = True
        Me.txt_belum_sj.Size = New System.Drawing.Size(120, 20)
        Me.txt_belum_sj.TabIndex = 27
        Me.txt_belum_sj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(152, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(121, 13)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Sudah Keluar SJ Proses"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(119, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Belum Keluar SJ Proses"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(288, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(98, 13)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "Total QTY ( Meter )"
        '
        'txt_total_qty
        '
        Me.txt_total_qty.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_qty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_qty.Location = New System.Drawing.Point(277, 30)
        Me.txt_total_qty.Name = "txt_total_qty"
        Me.txt_total_qty.ReadOnly = True
        Me.txt_total_qty.Size = New System.Drawing.Size(120, 20)
        Me.txt_total_qty.TabIndex = 22
        Me.txt_total_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.btn_update_note_po)
        Me.Panel4.Controls.Add(Me.txt_note_po)
        Me.Panel4.Location = New System.Drawing.Point(12, 534)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(428, 66)
        Me.Panel4.TabIndex = 22
        '
        'form_po_proses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 609)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.Panel4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_po_proses"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtp_akhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_cari_no_po As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ts_print As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_perbarui As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_hapus As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_ubah As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_baru As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_note_po As System.Windows.Forms.RichTextBox
    Friend WithEvents btn_update_note_po As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_total_qty As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_gudang As System.Windows.Forms.TextBox
    Friend WithEvents ts_print_po As System.Windows.Forms.ToolStripButton
    Friend WithEvents txt_keluar_sj As System.Windows.Forms.TextBox
    Friend WithEvents txt_belum_sj As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_total_qty_yard As System.Windows.Forms.TextBox
    Friend WithEvents txt_keluar_sj_yard As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_belum_sj_yard As System.Windows.Forms.TextBox
    Friend WithEvents dtp_po As System.Windows.Forms.DateTimePicker
End Class
