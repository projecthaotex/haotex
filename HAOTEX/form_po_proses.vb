﻿Imports MySql.Data.MySqlClient

Public Class form_po_proses
    Private Sub form_po_proses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call awal()
        dtp_hari_ini.Text = Today
    End Sub
    Private Sub awal()
        Call isidtpawal()
        Call isidgv()
        Call hitungjumlah()
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(-3)
        dtp_awal.Text = tanggal
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Gudang"
        dgv1.Columns(2).HeaderText = "No PO"
        dgv1.Columns(3).HeaderText = "Jenis Kain"
        dgv1.Columns(4).HeaderText = "Asal SJ"
        dgv1.Columns(5).HeaderText = "QTY"
        dgv1.Columns(6).HeaderText = "Satuan"
        dgv1.Columns(7).HeaderText = "Warna"
        dgv1.Columns(8).HeaderText = "Resep"
        dgv1.Columns(9).HeaderText = "TL"
        dgv1.Columns(10).HeaderText = "Hand Feel"
        dgv1.Columns(11).HeaderText = "Keterangan"
        dgv1.Columns(12).HeaderText = "Note"
        dgv1.Columns(13).HeaderText = "Status"
        dgv1.RowHeadersWidth = 50
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 120
        dgv1.Columns(2).Width = 100
        dgv1.Columns(3).Width = 120
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 100
        dgv1.Columns(6).Width = 70
        dgv1.Columns(7).Width = 100
        dgv1.Columns(8).Width = 100
        dgv1.Columns(9).Width = 100
        dgv1.Columns(10).Width = 100
        dgv1.Columns(11).Width = 120
        dgv1.Columns(12).Width = 120
        dgv1.Columns(11).Width = 100
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Format = "N"
    End Sub
    Private Sub dgv1_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgv1.CellFormatting
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(13).Value = "Closed" Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Red
            End If
        Next
        dgv1.Rows(e.RowIndex).HeaderCell.Value = CStr(e.RowIndex + 1)
    End Sub
    Private Sub hitungjumlah()
        Dim jumlahtotal, belumsj, keluarsj, jumlahtotalyard, belumsjyard, keluarsjyard As Double
        jumlahtotal = 0
        belumsj = 0
        keluarsj = 0
        jumlahtotalyard = 0
        belumsjyard = 0
        keluarsjyard = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(6).Value = "Meter" Then
                jumlahtotal = jumlahtotal + Val(dgv1.Rows(i).Cells(5).Value)
            ElseIf dgv1.Rows(i).Cells(6).Value = "Yard" Then
                jumlahtotalyard = jumlahtotalyard + Val(dgv1.Rows(i).Cells(5).Value)
            End If

            If dgv1.Rows(i).Cells(13).Value = "Closed" Then
                If dgv1.Rows(i).Cells(6).Value = "Meter" Then
                    keluarsj = keluarsj + Val(dgv1.Rows(i).Cells(5).Value)
                ElseIf dgv1.Rows(i).Cells(6).Value = "Yard" Then
                    keluarsjyard = keluarsjyard + Val(dgv1.Rows(i).Cells(5).Value)
                End If
            Else
                If dgv1.Rows(i).Cells(6).Value = "Meter" Then
                    belumsj = belumsj + Val(dgv1.Rows(i).Cells(5).Value)
                ElseIf dgv1.Rows(i).Cells(6).Value = "Yard" Then
                    belumsjyard = belumsjyard + Val(dgv1.Rows(i).Cells(5).Value)
                End If
            End If
        Next
        txt_total_qty.Text = jumlahtotal
        txt_belum_sj.Text = belumsj
        txt_keluar_sj.Text = keluarsj
        txt_total_qty_yard.Text = jumlahtotalyard
        txt_belum_sj_yard.Text = belumsjyard
        txt_keluar_sj_yard.Text = keluarsjyard
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,QTY,Satuan,Warna,Resep,Tarik_Lebar,Hand_Fill,Asal_Ket,Keterangan,Status,Id_grey,Id_beli,Id_Po_Proses FROM tbpoproses WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY No_PO"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpoproses")
                            dgv1.DataSource = dsx.Tables("tbpoproses")
                            Call headertabel()
                            Call hitungjumlah()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_awal.Text = dtp_akhir.Text
        End If
        txt_cari_no_po.Text = "< No PO >"
        txt_cari_gudang.Text = "< Gudang >"
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Call isidgv()
    End Sub
    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
        txt_cari_no_po.Text = "< No PO >"
        txt_cari_gudang.Text = "< Gudang >"
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Call isidgv()
    End Sub

    Private Sub isidgv_No_PO()
        dtp_po.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT tbpoproses.Tanggal,tbpoproses.Gudang,No_PO,tbpoproses.Jenis_Kain,tbpoproses.Asal_SJ,tbpoproses.QTY,tbpoproses.Satuan,Warna,Resep,Tarik_Lebar,Hand_Fill,tbpoproses.Asal_Ket,tbpoproses.Keterangan,Status,tbpembeliangrey.Tanggal AS Tgl_SJ FROM tbpoproses INNER JOIN tbpembeliangrey ON tbpoproses.Id_Beli=tbpembeliangrey.Id_Beli WHERE No_PO like '" & txt_cari_no_po.Text & "' AND tbpoproses.Tanggal='" & dtp_po.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpoproses")
                        dgv1.DataSource = dsx.Tables("tbpoproses")
                        Call hitungjumlah()
                    End Using
                End Using
            End Using
        End Using
        dtp_po.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isi_note_po()
        dtp_po.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Note FROM tbnotepoproses WHERE No_PO = '" & txt_cari_no_po.Text & "' AND Tanggal='" & dtp_po.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_note_po.Text = drx(0).ToString
                    Else
                        txt_note_po.Text = ""
                        btn_update_note_po.Enabled = False
                    End If
                End Using
            End Using
        End Using
        dtp_po.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub txt_cari_no_po_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_no_po.GotFocus
        If txt_cari_no_po.Text = "< No PO >" Then
            txt_cari_gudang.Text = "< Gudang >"
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
            txt_cari_no_po.Text = ""
        End If
    End Sub
    Private Sub txt_cari_no_po_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_no_po.LostFocus
        If txt_cari_no_po.Text = "" Then
            txt_cari_no_po.Text = "< No PO >"
        End If
    End Sub
    Private Sub simpan_note_po_proses()
        dtp_po.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Note FROM tbnotepoproses WHERE No_PO='" & txt_cari_no_po.Text & "' AND Tanggal='" & dtp_po.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbnotepoproses SET Note=@1 WHERE No_PO='" & txt_cari_no_po.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_note_po.Text))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        dtp_po.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub txt_cari_no_po_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_no_po.TextChanged
        If txt_cari_no_po.Text = "< No PO >" Then
            btn_update_note_po.Enabled = False
            txt_note_po.Text = ""
            ts_hapus.Enabled = True
            ts_ubah.Enabled = True
        ElseIf txt_cari_no_po.Text = "" Then
            btn_update_note_po.Enabled = False
            txt_note_po.Text = ""
            ts_hapus.Enabled = False
            ts_ubah.Enabled = False
            Call awal()
        Else
            Call isidgv_No_PO()
            btn_update_note_po.Enabled = True
            ts_hapus.Enabled = False
            ts_ubah.Enabled = False
            Call isi_note_po()
        End If
    End Sub
    Private Sub dtp_po_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_po.TextChanged
        If txt_cari_no_po.Text = "" Or txt_cari_no_po.Text = "< No PO >" Then
        Else
            Call isidgv_No_PO()
            btn_update_note_po.Enabled = True
            Call isi_note_po()
        End If
    End Sub
    Private Sub btn_update_note_po_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_update_note_po.Click
        Call simpan_note_po_proses()
        MsgBox("NOTE PO PROSES Berhasil Diubah")
    End Sub

    Private Sub txt_total_qty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_qty.TextChanged
        If txt_total_qty.Text <> String.Empty Then
            Dim temp As String = txt_total_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_qty.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_qty.Select(txt_total_qty.Text.Length, 0)
            ElseIf txt_total_qty.Text = "-"c Then

            Else
                txt_total_qty.Text = CDec(temp).ToString("N0")
                txt_total_qty.Select(txt_total_qty.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_belum_sj_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_belum_sj.TextChanged
        If txt_belum_sj.Text <> String.Empty Then
            Dim temp As String = txt_belum_sj.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_belum_sj.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_belum_sj.Select(txt_belum_sj.Text.Length, 0)
            ElseIf txt_belum_sj.Text = "-"c Then

            Else
                txt_belum_sj.Text = CDec(temp).ToString("N0")
                txt_belum_sj.Select(txt_belum_sj.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_keluar_sj_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_keluar_sj.TextChanged
        If txt_keluar_sj.Text <> String.Empty Then
            Dim temp As String = txt_keluar_sj.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_keluar_sj.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_keluar_sj.Select(txt_keluar_sj.Text.Length, 0)
            ElseIf txt_keluar_sj.Text = "-"c Then

            Else
                txt_keluar_sj.Text = CDec(temp).ToString("N0")
                txt_keluar_sj.Select(txt_keluar_sj.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_qty_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_qty_yard.TextChanged
        If txt_total_qty_yard.Text <> String.Empty Then
            Dim temp As String = txt_total_qty_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_qty_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_qty_yard.Select(txt_total_qty_yard.Text.Length, 0)
            ElseIf txt_total_qty_yard.Text = "-"c Then

            Else
                txt_total_qty_yard.Text = CDec(temp).ToString("N0")
                txt_total_qty_yard.Select(txt_total_qty_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_belum_sj_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_belum_sj_yard.TextChanged
        If txt_belum_sj_yard.Text <> String.Empty Then
            Dim temp As String = txt_belum_sj_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_belum_sj_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_belum_sj_yard.Select(txt_belum_sj_yard.Text.Length, 0)
            ElseIf txt_belum_sj_yard.Text = "-"c Then

            Else
                txt_belum_sj_yard.Text = CDec(temp).ToString("N0")
                txt_belum_sj_yard.Select(txt_belum_sj_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_keluar_sj_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_keluar_sj_yard.TextChanged
        If txt_keluar_sj_yard.Text <> String.Empty Then
            Dim temp As String = txt_keluar_sj_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_keluar_sj_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_keluar_sj_yard.Select(txt_keluar_sj_yard.Text.Length, 0)
            ElseIf txt_keluar_sj_yard.Text = "-"c Then

            Else
                txt_keluar_sj_yard.Text = CDec(temp).ToString("N0")
                txt_keluar_sj_yard.Select(txt_keluar_sj_yard.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub isidgv_gudang()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,QTY,Satuan,Warna,Resep,Tarik_Lebar,Hand_Fill,Asal_Ket,Keterangan,Status,Id_grey,Id_beli,Id_Po_Proses FROM tbpoproses WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY No_PO"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpoproses")
                        dgv1.DataSource = dsx.Tables("tbpoproses")
                        Call headertabel()
                        Call hitungjumlah()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_jenis_kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,QTY,Satuan,Warna,Resep,Tarik_Lebar,Hand_Fill,Asal_Ket,Keterangan,Status,Id_grey,Id_beli,Id_Po_Proses FROM tbpoproses WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY No_PO"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpoproses")
                        dgv1.DataSource = dsx.Tables("tbpoproses")
                        Call headertabel()
                        Call hitungjumlah()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_gudang_jenis_kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,QTY,Satuan,Warna,Resep,Tarik_Lebar,Hand_Fill,Asal_Ket,Keterangan,Status,Id_grey,Id_beli,Id_Po_Proses FROM tbpoproses WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY No_PO"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpoproses")
                        dgv1.DataSource = dsx.Tables("tbpoproses")
                        Call headertabel()
                        Call hitungjumlah()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.GotFocus
        If txt_cari_gudang.Text = "< Gudang >" Then
            txt_cari_no_po.Text = "< No PO >"
            txt_cari_gudang.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.LostFocus
        If txt_cari_gudang.Text = "" Then
            txt_cari_gudang.Text = "< Gudang >"
        End If
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.TextChanged
        Try
            If txt_cari_gudang.Text = "< Gudang >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call hitungjumlah()
            ElseIf txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_gudang.Text = "< Gudang >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jenis_kain()
            ElseIf txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jenis_kain()
            ElseIf Not txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_gudang_jenis_kain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_no_po.Text = "< No PO >"
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_gudang.Text = "< Gudang >" Then
                Call hitungjumlah()
            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv_jenis_kain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang_jenis_kain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        Try
            If dgv1.RowCount = 0 Then
                MsgBox("Tidak Terdapat Data Untuk DiUBAH")
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx As String = "SELECT * FROM tbsuratjalanproses WHERE Id_Po_Proses='" & dgv1.CurrentRow.Cells(16).Value.ToString & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                MsgBox("PO PROSES Yang Dipilih Sudah Keluar SURAT JALAN PROSES, Tidak Bisa DIUBAH")
                            Else
                                form_edit_po_proses.MdiParent = form_menu_utama
                                form_edit_po_proses.Show()
                                form_edit_po_proses.Focus()
                                form_edit_po_proses.Label1.Text = "Ubah PO Proses"
                                form_edit_po_proses.Label1.Focus()
                                Dim i As Integer
                                i = dgv1.CurrentRow.Index
                                With dgv1.Rows.Item(i)
                                    form_edit_po_proses.cb_satuan.Text = .Cells(6).Value.ToString
                                    form_edit_po_proses.dtp_tanggal.Text = .Cells(0).Value.ToString
                                    form_edit_po_proses.dtp_tanggal_1.Text = .Cells(0).Value.ToString
                                    form_edit_po_proses.txt_gudang.Text = .Cells(1).Value.ToString
                                    form_edit_po_proses.txt_no_po.Text = .Cells(2).Value.ToString
                                    form_edit_po_proses.txt_jenis_kain.Text = .Cells(3).Value.ToString
                                    form_edit_po_proses.txt_surat_jalan.Text = .Cells(4).Value.ToString
                                    form_edit_po_proses.txt_warna.Text = .Cells(7).Value.ToString
                                    form_edit_po_proses.txt_resep.Text = .Cells(8).Value.ToString
                                    form_edit_po_proses.txt_tarik_lebar.Text = .Cells(9).Value.ToString
                                    form_edit_po_proses.txt_hand_fill.Text = .Cells(10).Value.ToString
                                    form_edit_po_proses.txt_keterangan.Text = .Cells(11).Value.ToString
                                    form_edit_po_proses.txt_note.Text = .Cells(12).Value.ToString
                                    form_edit_po_proses.txt_id_grey.Text = .Cells(14).Value.ToString
                                    form_edit_po_proses.txt_id_beli.Text = .Cells(15).Value.ToString
                                    form_edit_po_proses.txt_id_po_proses.Text = .Cells(16).Value.ToString
                                    Using conz As New MySqlConnection(sLocalConn)
                                        conz.Open()
                                        Dim sqlz As String = "SELECT Stok,Satuan,Harga FROM tbstokprosesgrey WHERE Id_Beli='" & .Cells(15).Value.ToString & "'"
                                        Using cmdz As New MySqlCommand(sqlz, conz)
                                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                                drz.Read()
                                                If drz.HasRows Then
                                                    form_edit_po_proses.txt_asal_qty.Text = Val(drz.Item(0).ToString.Replace(",", "."))
                                                    form_edit_po_proses.txt_asal_satuan.Text = drz.Item(1)
                                                    form_edit_po_proses.txt_harga.Text = drz.Item(2)
                                                End If
                                            End Using
                                        End Using
                                    End Using
                                    If form_edit_po_proses.txt_asal_satuan.Text = form_edit_po_proses.cb_satuan.Text Then
                                        form_edit_po_proses.txt_sisa_qty.Text = Math.Round(Val(form_edit_po_proses.txt_asal_qty.Text.Replace(",", ".")) + Val(.Cells(5).Value.ToString.Replace(",", ".")), 0)
                                        form_edit_po_proses.txt_qty.Text = .Cells(5).Value.ToString
                                    ElseIf form_edit_po_proses.txt_asal_satuan.Text = "Meter" And form_edit_po_proses.cb_satuan.Text = "Yard" Then
                                        form_edit_po_proses.txt_sisa_qty.Text = Math.Round((Val(form_edit_po_proses.txt_asal_qty.Text.Replace(",", ".")) * 1.0936) + Val(.Cells(5).Value.ToString.Replace(",", ".")), 0)
                                        form_edit_po_proses.txt_qty.Text = .Cells(5).Value.ToString
                                    ElseIf form_edit_po_proses.txt_asal_satuan.Text = "Yard" And form_edit_po_proses.cb_satuan.Text = "Meter" Then
                                        form_edit_po_proses.txt_sisa_qty.Text = Math.Round((Val(form_edit_po_proses.txt_asal_qty.Text.Replace(",", ".")) * 0.9144) + Val(.Cells(5).Value.ToString.Replace(",", ".")), 0)
                                        form_edit_po_proses.txt_qty.Text = .Cells(5).Value.ToString
                                    End If
                                End With
                            End If
                        End Using
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        Try
            If dgv1.RowCount = 0 Then
                MsgBox("Tidak Terdapat Data Untuk DiHAPUS")
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx As String = "SELECT * FROM tbsuratjalanproses WHERE Id_Po_Proses='" & dgv1.CurrentRow.Cells(16).Value.ToString & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                MsgBox("PO Yang Dipilih Sudah Keluar Surat Jalan Proses, Tidak Bisa DIHAPUS")
                            Else
                                form_edit_po_proses.MdiParent = form_menu_utama
                                form_edit_po_proses.Show()
                                form_edit_po_proses.Label1.Text = "HAPUS PO Proses"
                                form_edit_po_proses.btn_simpan_tutup.Visible = False
                                form_edit_po_proses.btn_batal.Visible = False
                                Dim i As Integer
                                i = dgv1.CurrentRow.Index
                                With dgv1.Rows.Item(i)
                                    form_edit_po_proses.cb_satuan.Text = .Cells(6).Value.ToString
                                    form_edit_po_proses.dtp_tanggal.Text = .Cells(0).Value.ToString
                                    form_edit_po_proses.dtp_tanggal_1.Text = .Cells(0).Value.ToString
                                    form_edit_po_proses.txt_gudang.Text = .Cells(1).Value.ToString
                                    form_edit_po_proses.txt_no_po.Text = .Cells(2).Value.ToString
                                    form_edit_po_proses.txt_jenis_kain.Text = .Cells(3).Value.ToString
                                    form_edit_po_proses.txt_surat_jalan.Text = .Cells(4).Value.ToString
                                    form_edit_po_proses.txt_warna.Text = .Cells(7).Value.ToString
                                    form_edit_po_proses.txt_resep.Text = .Cells(8).Value.ToString
                                    form_edit_po_proses.txt_tarik_lebar.Text = .Cells(9).Value.ToString
                                    form_edit_po_proses.txt_hand_fill.Text = .Cells(10).Value.ToString
                                    form_edit_po_proses.txt_keterangan.Text = .Cells(11).Value.ToString
                                    form_edit_po_proses.txt_note.Text = .Cells(12).Value.ToString
                                    form_edit_po_proses.txt_id_grey.Text = .Cells(14).Value.ToString
                                    form_edit_po_proses.txt_id_beli.Text = .Cells(15).Value.ToString
                                    form_edit_po_proses.txt_id_po_proses.Text = .Cells(16).Value.ToString
                                    Using conz As New MySqlConnection(sLocalConn)
                                        conz.Open()
                                        Dim sqlz As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & .Cells(15).Value.ToString & "'"
                                        Using cmdz As New MySqlCommand(sqlz, conz)
                                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                                drz.Read()
                                                If drz.HasRows Then
                                                    form_edit_po_proses.txt_asal_qty.Text = Val(drz.Item(0).ToString.Replace(",", "."))
                                                    form_edit_po_proses.txt_asal_satuan.Text = drz.Item(1)
                                                End If
                                            End Using
                                        End Using
                                    End Using
                                    If form_edit_po_proses.txt_asal_satuan.Text = form_edit_po_proses.cb_satuan.Text Then
                                        form_edit_po_proses.txt_sisa_qty.Text = Math.Round(Val(form_edit_po_proses.txt_asal_qty.Text.Replace(",", ".")) + Val(.Cells(5).Value.ToString.Replace(",", ".")), 0)
                                        form_edit_po_proses.txt_qty.Text = .Cells(5).Value.ToString
                                    ElseIf form_edit_po_proses.txt_asal_satuan.Text = "Meter" And form_edit_po_proses.cb_satuan.Text = "Yard" Then
                                        form_edit_po_proses.txt_sisa_qty.Text = Math.Round((Val(form_edit_po_proses.txt_asal_qty.Text.Replace(",", ".")) * 1.0936) + Val(.Cells(5).Value.ToString.Replace(",", ".")), 0)
                                        form_edit_po_proses.txt_qty.Text = .Cells(5).Value.ToString
                                    ElseIf form_edit_po_proses.txt_asal_satuan.Text = "Yard" And form_edit_po_proses.cb_satuan.Text = "Meter" Then
                                        form_edit_po_proses.txt_sisa_qty.Text = Math.Round((Val(form_edit_po_proses.txt_asal_qty.Text.Replace(",", ".")) * 0.9144) + Val(.Cells(5).Value.ToString.Replace(",", ".")), 0)
                                        form_edit_po_proses.txt_qty.Text = .Cells(5).Value.ToString
                                    End If
                                End With
                                If MsgBox("Yakin PO PROSES Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus PO PROSES") = vbYes Then
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses='" & form_edit_po_proses.txt_id_po_proses.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            cmdy.ExecuteNonQuery()
                                        End Using
                                    End Using
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses='" & form_edit_po_proses.txt_id_po_proses.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            cmdy.ExecuteNonQuery()
                                        End Using
                                    End Using
                                    Using conz As New MySqlConnection(sLocalConn)
                                        conz.Open()
                                        Dim sqlz As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Beli='" & form_edit_po_proses.txt_id_beli.Text & "'"
                                        Using cmdz As New MySqlCommand(sqlz, conz)
                                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                                drz.Read()
                                                If drz.HasRows Then
                                                    Dim s As Double
                                                    s = drz.Item(0)
                                                    Using cony As New MySqlConnection(sLocalConn)
                                                        cony.Open()
                                                        Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & form_edit_po_proses.txt_id_beli.Text & "'"
                                                        Using cmdy As New MySqlCommand(sqly, cony)
                                                            With cmdy
                                                                .Parameters.Clear()
                                                                Dim qty As String = form_edit_po_proses.txt_sisa_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                                                                If form_edit_po_proses.txt_asal_satuan.Text = form_edit_po_proses.cb_satuan.Text Then
                                                                    .Parameters.AddWithValue("@1", Math.Round(Val(qty.Replace(",", ".")), 0))
                                                                ElseIf form_edit_po_proses.txt_asal_satuan.Text = "Meter" And form_edit_po_proses.cb_satuan.Text = "Yard" Then
                                                                    .Parameters.AddWithValue("@1", Math.Round(Val(qty.Replace(",", ".")) * 0.9144, 0))
                                                                ElseIf form_edit_po_proses.txt_asal_satuan.Text = "Yard" And form_edit_po_proses.cb_satuan.Text = "Meter" Then
                                                                    .Parameters.AddWithValue("@1", Math.Round(Val(qty.Replace(",", ".")) * 1.0936, 0))
                                                                End If
                                                                .ExecuteNonQuery()
                                                            End With
                                                        End Using
                                                    End Using
                                                End If
                                            End Using
                                        End Using
                                    End Using
                                    ts_perbarui.PerformClick()
                                    form_edit_po_proses.Close()
                                    MsgBox("PO Proses berhasil di HAPUS")
                                Else
                                    form_edit_po_proses.Close()
                                End If
                            End If
                        End Using
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_po_proses.MdiParent = form_menu_utama
        form_input_po_proses.Show()
        form_input_po_proses.Focus()
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        txt_cari_no_po.Text = "< No PO >"
        txt_cari_gudang.Text = "< Gudang >"
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        dtp_po.Text = Today
        Call isidgv()
        dgv1.Focus()
    End Sub

    Private Sub reportpoproses()
        dgv1.Columns(0).DefaultCellStyle.Format = "D"
        dgv1.Columns(14).DefaultCellStyle.Format = "dd/MM"
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
        End With
        tbheaderfooter.Rows.Add(dgv1.Rows(0).Cells(0).FormattedValue, dgv1.Rows(0).Cells(1).Value, dgv1.Rows(0).Cells(2).Value, _
                                         txt_note_po.Text)

        Dim dtreportpoproses As New DataTable
        With dtreportpoproses
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            dtreportpoproses.Rows.Add(row.Index + 1, row.Cells(14).FormattedValue, row.Cells(4).Value, row.Cells(3).Value, _
                                      row.Cells(7).Value, row.Cells(8).Value, row.Cells(5).FormattedValue, row.Cells(6).Value, _
                                      row.Cells(9).Value, row.Cells(10).Value, row.Cells(11).Value, row.Cells(12).Value)
        Next

        form_report_po_proses.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_po_proses.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportpoproses
        form_report_po_proses.ShowDialog()
        form_report_po_proses.Dispose()
        dgv1.Columns(0).DefaultCellStyle.Format = Nothing
    End Sub
    Private Sub reportpoprosesall()
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
        End With
        tbheaderfooter.Rows.Add(dtp_awal.Text, dtp_akhir.Text, dtp_hari_ini.Text, txt_total_qty.Text, _
                                txt_belum_sj.Text, txt_keluar_sj.Text, txt_total_qty_yard.Text, _
                                txt_belum_sj_yard.Text, txt_keluar_sj_yard.Text)

        Dim dtreportpoproses As New DataTable
        With dtreportpoproses
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            dtreportpoproses.Rows.Add(row.Cells(0).FormattedValue, row.Cells(1).Value, row.Cells(2).Value, row.Cells(3).Value, _
                                      row.Cells(4).Value, row.Cells(5).FormattedValue, row.Cells(6).Value, row.Cells(7).Value, _
                                      row.Cells(8).Value, row.Cells(9).Value, row.Cells(10).Value, row.Cells(11).Value)
        Next

        form_report_po_proses_all.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_po_proses_all.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportpoproses
        form_report_po_proses_all.ShowDialog()
        form_report_po_proses_all.Dispose()
    End Sub
    Private Sub ts_print_po_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print_po.Click
        If txt_cari_no_po.Text = "< No PO >" Or txt_cari_no_po.Text = "" Then
            MsgBox("No PO Belum di INPUT")
            txt_cari_no_po.Focus()
        ElseIf dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportpoproses()
        End If
    End Sub
    Private Sub ts_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportpoprosesall()
        End If
    End Sub
End Class