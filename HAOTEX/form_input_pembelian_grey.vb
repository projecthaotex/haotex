﻿Imports MySql.Data.MySqlClient

Public Class form_input_pembelian_grey

    Private Sub txt_penyimpanan_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_penyimpanan.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_supplier_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey"
        form_input_supplier.Focus()
    End Sub
    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Me.Close()
    End Sub

    Private Sub txt_harga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga.Select(txt_harga.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga.Select(txt_harga.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_harga.TextChanged
        If txt_harga.Text <> String.Empty Then
            Dim temp As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga.Select(txt_harga.Text.Length, 0)
            ElseIf txt_harga.Text = "-"c Then

            Else
                txt_harga.Text = CDec(temp).ToString("N0")
                txt_harga.Select(txt_harga.Text.Length, 0)
            End If
        End If
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_jumlah.Text = Val(harga.Replace(",", ".")) * Val(qty.Replace(",", "."))
    End Sub
    Private Sub txt_qty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty.Text = String.Empty Then
                        txt_qty.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty.Select(txt_qty.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty.Text = String.Empty Then
                        txt_qty.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty.Select(txt_qty.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_qty.TextChanged
        If txt_qty.Text <> String.Empty Then
            Dim temp As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty.Select(txt_qty.Text.Length, 0)
            ElseIf txt_qty.Text = "-"c Then

            Else
                txt_qty.Text = CDec(temp).ToString("N0")
                txt_qty.Select(txt_qty.Text.Length, 0)
            End If
        End If
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_jumlah.Text = Val(harga.Replace(",", ".")) * Val(qty.Replace(",", "."))
    End Sub
    Private Sub txt_jumlah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_jumlah.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_jumlah.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_jumlah.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_jumlah.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_jumlah.Text = String.Empty Then
                        txt_jumlah.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_jumlah.Select(txt_jumlah.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_jumlah.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_jumlah.Text = String.Empty Then
                        txt_jumlah.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_jumlah.Select(txt_jumlah.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_jumlah_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah.TextChanged
        If txt_jumlah.Text <> String.Empty Then
            Dim temp As String = txt_jumlah.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah.Select(txt_jumlah.Text.Length, 0)
            ElseIf txt_jumlah.Text = "-"c Then

            Else
                txt_jumlah.Text = CDec(temp).ToString("N0")
                txt_jumlah.Select(txt_jumlah.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak.TextChanged
        If txt_sisa_kontrak.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak.Select(txt_sisa_kontrak.Text.Length, 0)
            ElseIf txt_sisa_kontrak.Text = "-"c Then

            Else
                txt_sisa_kontrak.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak.Select(txt_sisa_kontrak.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub simpan_tbpembeliangrey()
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah As String = txt_jumlah.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey.Text)
                    .Parameters.AddWithValue("@2", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@7", txt_supplier.Text)
                    .Parameters.AddWithValue("@8", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_penyimpanan.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey()
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah As String = txt_jumlah.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli.Text)
                    .Parameters.AddWithValue("@2", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier.Text)
                    .Parameters.AddWithValue("@5", txt_penyimpanan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub update_tbkontrak_grey()
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q, qa As Double
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_satuan.Text = "Meter" And txt_satuan_awal.Text = "Meter" And cb_satuan.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - Val(txt_qty_awal.Text.Replace(",", "."))) + Val(qty.Replace(",", ".")), 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - Val(txt_qty_awal.Text.Replace(",", "."))) + Val(qty.Replace(",", "."))), 2))
                    ElseIf txt_satuan.Text = "Meter" And txt_satuan_awal.Text = "Meter" And cb_satuan.Text = "Yard" Then
                        q = Math.Round(Val(qty.Replace(",", ".")) * 0.9144, 0)
                        qa = Math.Round(Val(txt_qty_awal.Text.Replace(",", ".")), 0)
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q, 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q), 2))
                    ElseIf txt_satuan.Text = "Meter" And txt_satuan_awal.Text = "Yard" And cb_satuan.Text = "Meter" Then
                        q = Math.Round(Val(qty.Replace(",", ".")), 0)
                        qa = Math.Round(Val(txt_qty_awal.Text.Replace(",", ".")) * 0.9144, 0)
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q, 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q), 2))
                    ElseIf txt_satuan.Text = "Meter" And txt_satuan_awal.Text = "Yard" And cb_satuan.Text = "Yard" Then
                        q = Math.Round(Val(qty.Replace(",", ".")) * 0.9144, 0)
                        qa = Math.Round(Val(txt_qty_awal.Text.Replace(",", ".")) * 0.9144, 0)
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q, 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q), 2))
                    ElseIf txt_satuan.Text = "Yard" And txt_satuan_awal.Text = "Yard" And cb_satuan.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - Val(txt_qty_awal.Text.Replace(",", "."))) + Val(qty.Replace(",", ".")), 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - Val(txt_qty_awal.Text.Replace(",", "."))) + Val(qty.Replace(",", "."))), 2))
                    ElseIf txt_satuan.Text = "Yard" And txt_satuan_awal.Text = "Yard" And cb_satuan.Text = "Meter" Then
                        q = Math.Round(Val(qty.Replace(",", ".")) * 1.0936, 0)
                        qa = Math.Round(Val(txt_qty_awal.Text.Replace(",", ".")), 0)
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q, 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q), 2))
                    ElseIf txt_satuan.Text = "Yard" And txt_satuan_awal.Text = "Meter" And cb_satuan.Text = "Yard" Then
                        q = Math.Round(Val(qty.Replace(",", ".")), 0)
                        qa = Math.Round(Val(txt_qty_awal.Text.Replace(",", ".")) * 1.0936, 0)
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q, 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q), 2))
                    ElseIf txt_satuan.Text = "Yard" And txt_satuan_awal.Text = "Meter" And cb_satuan.Text = "Meter" Then
                        q = Math.Round(Val(qty.Replace(",", ".")) * 1.0936, 0)
                        qa = Math.Round(Val(txt_qty_awal.Text.Replace(",", ".")) * 1.0936, 0)
                        .Parameters.AddWithValue("@1", Math.Round((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q, 2))
                        .Parameters.AddWithValue("@2", Math.Round(Val(txt_total_kontrak.Text.Replace(",", ".")) - ((Val(txt_dikirim_awal.Text.Replace(",", ".")) - qa) + q), 2))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub simpan_tbdetilhutang()
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah As String = txt_jumlah.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@6", txt_supplier.Text)
                    .Parameters.AddWithValue("@7", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan.Text)
                    .Parameters.AddWithValue("@10", jumlah.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub btn_simpan_tutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_tutup.Click
        Try
            Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak As String = txt_sisa_kontrak.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_surat_jalan.Text = "" Then
                MsgBox("SURAT JALAN tidak boleh kosong")
                txt_surat_jalan.Focus()
            ElseIf txt_jenis_kain.Text = "" Then
                MsgBox("JENIS KAIN tidak boleh kosong")
                txt_jenis_kain.Focus()
            ElseIf txt_supplier.Text = "" Then
                MsgBox("NAMA SUPPLIER tidak boleh kosong")
                txt_supplier.Focus()
            ElseIf Val(txt_harga.Text) = 0 Then
                MsgBox("HARGA tidak boleh kosong")
                txt_harga.Focus()
            ElseIf Val(txt_qty.Text) = 0 Then
                MsgBox("QUANTITY tidak boleh kosong")
                txt_qty.Focus()
            ElseIf Val(qty.Replace(",", ".")) > Val(sisa_kontrak.Replace(",", ".")) And Not txt_no_kontrak.Text = "-" Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty.Focus()
            ElseIf txt_penyimpanan.Text = "" Then
                MsgBox("GUDANG tidak boleh kosong")
                txt_penyimpanan.Focus()
            Else
                Call hapus_tbpembeliangrey()
                Call hapus_tbstokproses()
                Call hapus_tbdetil_hutang()
                Call simpan_tbpembeliangrey()
                Call simpan_tbstokprosesgrey()
                Call simpan_tbdetilhutang()
                If Not txt_no_kontrak.Text = "-" Then
                    Call update_tbkontrak_grey()
                End If
                MsgBox("PEMBELIAN GREY Berhasil DIUBAH")
                form_pembelian_grey.ts_perbarui.PerformClick()
                form_kontrak_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim tanggal As DateTime
        tanggal = dtp_tanggal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub dtp_tanggal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_tanggal.ValueChanged
        Dim tanggal As DateTime
        tanggal = dtp_tanggal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub txt_id_beli_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Asal='" & txt_id_beli.Text & "' AND Keterangan='Pembelian Grey'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_hutang.Text = drx(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub cb_satuan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan.TextChanged
        If txt_konversi.Text = "tidak" Then

        Else
            Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak As String = txt_sisa_kontrak.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim q, s, h As Double
            If Not txt_no_kontrak.Text = "-" Then
                If cb_satuan.Text = "Yard" Then
                    s = Math.Round(Val(sisa_kontrak.Replace(",", ".")) * 1.0936, 0)
                    q = Math.Round(Val(qty.Replace(",", ".")) * 1.0936, 0)
                    h = Math.Round(Val(harga.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan.Text = "Meter" Then
                    s = Math.Round(Val(sisa_kontrak.Replace(",", ".")) * 0.9144, 0)
                    q = Math.Round(Val(qty.Replace(",", ".")) * 0.9144, 0)
                    h = Math.Round(Val(harga.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga.Text = h
                txt_sisa_kontrak.Text = s
                txt_qty.Text = q
                txt_harga.Focus()
            End If
        End If
    End Sub
End Class