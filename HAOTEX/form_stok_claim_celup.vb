﻿Imports MySql.Data.MySqlClient

Public Class form_stok_claim_celup

    Private Sub form_stok_claim_grey_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If txt_form.Text = "form_input_surat_jalan_penjualan" Then
            form_input_surat_jalan_penjualan.Focus()
        End If
    End Sub
    Private Sub form_stok_claim_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Gudang"
        dgv1.Columns(2).HeaderText = "SJ Packing"
        dgv1.Columns(3).HeaderText = "Customer"
        dgv1.Columns(4).HeaderText = "Jenis Kain"
        dgv1.Columns(5).HeaderText = "Warna"
        dgv1.Columns(6).HeaderText = "Gulung"
        dgv1.Columns(7).HeaderText = "Stok"
        dgv1.Columns(8).HeaderText = "Satuan"
        dgv1.Columns(0).Width = 70
        dgv1.Columns(1).Width = 120
        dgv1.Columns(2).Width = 100
        dgv1.Columns(3).Width = 70
        dgv1.Columns(4).Width = 130
        dgv1.Columns(5).Width = 150
        dgv1.Columns(6).Width = 100
        dgv1.Columns(7).Width = 70
        dgv1.Columns(8).Width = 80
        dgv1.Columns(9).Width = 100
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Format = "N"
        dgv1.Columns(9).DefaultCellStyle.Format = "C"
        dgv1.Columns(9).Visible = False
        dgv1.Columns(10).Visible = False
        dgv1.Columns(11).Visible = False
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Celup FROM tbstokclaimcelup WHERE NOT Stok=0 ORDER BY Customer"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokclaimcelup")
                            dgv1.DataSource = dsx.Tables("tbstokclaimcelup")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_customer.GotFocus
        If txt_cari_customer.Text = "< Customer >" Then
            txt_cari_customer.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_customer.LostFocus
        If txt_cari_customer.Text = "" Then
            txt_cari_customer.Text = "< Customer >"
        End If
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_customer.TextChanged
        Try
            If txt_cari_customer.Text = "< Customer >" Then

            ElseIf txt_cari_customer.Text = "" Then
                If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                    Call isidgv()
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Celup FROM tbstokclaimcelup WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_Kain"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimcelup")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimcelup")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If
            Else
                If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Celup FROM tbstokclaimcelup WHERE Customer like '%" & txt_cari_customer.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimcelup")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimcelup")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Celup FROM tbstokclaimcelup WHERE Customer like '%" & txt_cari_customer.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimcelup")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimcelup")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_jenis_kain.Text = "" Then
                If txt_cari_customer.Text = "< Customer >" Then
                    Call isidgv()
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Celup FROM tbstokclaimcelup WHERE Customer like '%" & txt_cari_customer.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimcelup")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimcelup")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If
            Else
                If txt_cari_customer.Text = "< Customer >" Then
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Celup FROM tbstokclaimcelup WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_Kain"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimcelup")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimcelup")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Celup FROM tbstokclaimcelup WHERE Customer like '%" & txt_cari_customer.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimcelup")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimcelup")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgv1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv1.MouseDoubleClick
        Try
            If txt_form.Text = "form_input_surat_jalan_penjualan" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                    form_input_surat_jalan_penjualan.Show()
                    form_input_surat_jalan_penjualan.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        'form_input_surat_jalan_penjualan.txt_id_grey.Text = .Cells(0).Value.ToString
                        form_input_surat_jalan_penjualan.txt_gudang_1.Text = .Cells(1).Value.ToString
                        form_input_surat_jalan_penjualan.txt_sj_packing_1.Text = .Cells(2).Value.ToString
                        'form_input_surat_jalan_penjualan.txt_baris_sj_packing.Text = .Cells(3).Value.ToString
                        form_input_surat_jalan_penjualan.txt_customer.Text = .Cells(4).Value.ToString
                        form_input_surat_jalan_penjualan.txt_jenis_kain_1.Text = .Cells(5).Value.ToString
                        form_input_surat_jalan_penjualan.txt_warna_1.Text = .Cells(6).Value.ToString
                        form_input_surat_jalan_penjualan.txt_warna_1.Text = .Cells(7).Value.ToString
                        'form_input_surat_jalan_penjualan.txt_yards.Text = .Cells(8).Value.ToString
                        'form_input_surat_jalan_penjualan.txt_harga.Text = Math.Ceiling((.Cells(9).Value + (0.05 * .Cells(9).Value)) / 100.0) * 100
                        'form_input_surat_jalan_penjualan.txt_keterangan.Focus()
                    End With
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class