﻿Imports MySql.Data.MySqlClient

Public Class form_input_customer

    Private Sub form_input_customer_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If TxtForm.Text = "form_input_po_packing_customer_1" Or _
            TxtForm.Text = "form_input_po_packing_customer_2" Or _
            TxtForm.Text = "form_input_po_packing_customer_3" Or _
            TxtForm.Text = "form_input_po_packing_customer_4" Or _
            TxtForm.Text = "form_input_po_packing_customer_5" Or _
            TxtForm.Text = "form_input_po_packing_customer_6" Or _
            TxtForm.Text = "form_input_po_packing_customer_7" Or _
            TxtForm.Text = "form_input_po_packing_customer_8" Or _
            TxtForm.Text = "form_input_po_packing_customer_9" Or _
            TxtForm.Text = "form_input_po_packing_customer_10" Or _
            TxtForm.Text = "form_input_surat_jalan_penjualan" Then
            form_input_po_packing.Focus()
            form_input_po_packing.Label16.Focus()
        End If
    End Sub
    Private Sub form_input_customer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
        TxtCari.Text = ""
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Nama_Customer FROM tbcustomer ORDER BY Nama_Customer"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbcustomer")
                            Dgv1.DataSource = dsx.Tables("tbcustomer")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_nama_customer()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Nama_Customer FROM tbcustomer WHERE Nama_Customer like '%" & TxtCari.Text & "%' ORDER BY Nama_Customer"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbcustomer")
                            Dgv1.DataSource = dsx.Tables("tbcustomer")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub TxtCari_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCari.TextChanged
        Call cari_nama_customer()
    End Sub
    Private Sub Dgv1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Dgv1.MouseClick
        Dim i As Integer
        i = Me.Dgv1.CurrentRow.Index
        With Dgv1.Rows.Item(i)
            TxtCari.Text = .Cells(0).Value.ToString
        End With
    End Sub
    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Try
            If TxtCari.Text = "" Then
                MsgBox("NAMA CUSTOMER belum diinput")
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Customer from tbcustomer WHERE Nama_Customer ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("NAMA CUSTOMER belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama.MdiParent = form_menu_utama
                                    form_input_nama.Show()
                                    form_input_nama.txt_frm.Text = "form_input_customer"
                                    form_input_nama.Label1.Text = "Input Customer Baru"
                                    form_input_nama.Label2.Text = "Nama Customer"
                                    form_input_nama.txt_nama.Text = TxtCari.Text
                                    form_input_nama.txt_nama.Focus()
                                    form_input_nama.Focus()
                                End If
                            Else
                                If TxtForm.Text = "form_input_penjualan_grey" Then
                                    form_input_penjualan_grey.MdiParent = form_menu_utama
                                    form_input_penjualan_grey.Show()
                                    form_input_penjualan_grey.Focus()
                                    form_input_penjualan_grey.txt_customer.Text = TxtCari.Text
                                    form_input_penjualan_grey.cb_stok.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_1" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_1.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_2" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_2.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_3" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_3.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_4" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_4.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_5" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_5.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_6" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_6.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_7" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_7.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_8" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_8.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_9" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_9.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_po_packing_customer_10" Then
                                    form_input_po_packing.MdiParent = form_menu_utama
                                    form_input_po_packing.Show()
                                    form_input_po_packing.Focus()
                                    form_input_po_packing.txt_customer_10.Text = TxtCari.Text
                                    form_input_po_packing.txt_no_po.Focus()
                                    Me.Close()
                                ElseIf TxtForm.Text = "form_input_surat_jalan_penjualan" Then
                                    form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                                    form_input_surat_jalan_penjualan.Show()
                                    form_input_surat_jalan_penjualan.Focus()
                                    form_input_surat_jalan_penjualan.txt_customer.Text = TxtCari.Text
                                    form_input_surat_jalan_penjualan.txt_sj_penjualan.Focus()
                                    Me.Close()
                                End If
                            End If
                        End Using
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ts_keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_keluar.Click
        If TxtForm.Text = "form_input_penjualan_grey" Then
            form_input_penjualan_grey.Focus()
            form_input_penjualan_grey.txt_surat_jalan.Focus()
            Me.Close()
        ElseIf TxtForm.Text = "form_input_surat_jalan_penjualan" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_sj_penjualan.Focus()
            Me.Close()
        Else
            Me.Close()
        End If
        Me.Close()
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_nama.MdiParent = form_menu_utama
        form_input_nama.Show()
        form_input_nama.txt_frm.Text = "form_input_customer"
        form_input_nama.Label1.Text = "Input Customer Baru"
        form_input_nama.Label2.Text = "Nama Customer"
        form_input_nama.Focus()
    End Sub
    Private Sub btn_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_perbarui.Click
        Call isidgv()
    End Sub
    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        If TxtCari.Text = "" Then
            MsgBox("NAMA CUSTOMER yang akan dirubah belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Customer from tbcustomer WHERE Nama_Customer ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("NAMA CUSTOMER belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama.MdiParent = form_menu_utama
                                    form_input_nama.Show()
                                    form_input_nama.txt_frm.Text = "form_input_customer"
                                    form_input_nama.Label1.Text = "Input Customer Baru"
                                    form_input_nama.Label2.Text = "Nama Customer"
                                    form_input_nama.txt_nama.Text = TxtCari.Text
                                    form_input_nama.txt_nama.Focus()
                                    form_input_nama.Focus()
                                End If
                            Else
                                form_input_nama.MdiParent = form_menu_utama
                                form_input_nama.Show()
                                form_input_nama.btn_simpan_baru.Visible = False
                                form_input_nama.txt_frm.Text = "form_input_customer"
                                form_input_nama.Label1.Text = "Ubah Nama Customer"
                                form_input_nama.Label2.Text = "Nama Customer"
                                form_input_nama.txt_nama.Text = TxtCari.Text
                                form_input_nama.txt_nama.Focus()
                                form_input_nama.Focus()
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        If TxtCari.Text = "" Then
            MsgBox("NAMA CUSTOMER yang akan dihapus belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Customer FROM tbcustomer WHERE Nama_Customer ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                If MsgBox("Yakin NAMA GUDANG Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Data") = vbYes Then
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "DELETE FROM tbcustomer WHERE Nama_Customer='" & TxtCari.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            cmdy.ExecuteNonQuery()
                                        End Using
                                        TxtCari.Text = ""
                                        MsgBox("NAMA CUSTOMER berhasil di Hapus")
                                    End Using
                                End If
                            Else
                                MsgBox("NAMA CUSTOMER Belum Tersimpan Di Database")
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

End Class