﻿Imports MySql.Data.MySqlClient

Public Class form_input_po_packing

    Private Sub form_input_po_packing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel_2.Visible = False
        Panel_3.Visible = False
        Panel_4.Visible = False
        Panel_5.Visible = False
        Panel_6.Visible = False
        Panel_7.Visible = False
        Panel_8.Visible = False
        Panel_9.Visible = False
        Panel_10.Visible = False
        btn_hapus_2.Visible = False
        btn_hapus_3.Visible = False
        btn_hapus_4.Visible = False
        btn_hapus_5.Visible = False
        btn_hapus_6.Visible = False
        btn_hapus_7.Visible = False
        btn_hapus_8.Visible = False
        btn_hapus_9.Visible = False
        btn_hapus_10.Visible = False
        btn_tambah_baris_2.Visible = False
        btn_tambah_baris_3.Visible = False
        btn_tambah_baris_4.Visible = False
        btn_tambah_baris_5.Visible = False
        btn_tambah_baris_6.Visible = False
        btn_tambah_baris_7.Visible = False
        btn_tambah_baris_8.Visible = False
        btn_tambah_baris_9.Visible = False
        btn_selesai_2.Visible = False
        btn_selesai_3.Visible = False
        btn_selesai_4.Visible = False
        btn_selesai_5.Visible = False
        btn_selesai_6.Visible = False
        btn_selesai_7.Visible = False
        btn_selesai_8.Visible = False
        btn_selesai_9.Visible = False
        btn_selesai_10.Visible = False
        btn_batal_2.Visible = False
        btn_batal_3.Visible = False
        btn_batal_4.Visible = False
        btn_batal_5.Visible = False
        btn_batal_6.Visible = False
        btn_batal_7.Visible = False
        btn_batal_8.Visible = False
        btn_batal_9.Visible = False
        btn_batal_10.Visible = False
    End Sub

    Private Sub txt_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang.GotFocus
        If txt_gudang.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_po_packing"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_customer_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_1.GotFocus
        If txt_customer_1.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_1"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_2.GotFocus
        If txt_customer_2.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_2"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_3.GotFocus
        If txt_customer_3.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_3"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_4.GotFocus
        If txt_customer_4.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_4"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_5.GotFocus
        If txt_customer_5.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_5"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_6.GotFocus
        If txt_customer_6.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_6"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_7.GotFocus
        If txt_customer_7.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_7"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_8.GotFocus
        If txt_customer_8.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_8"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_9.GotFocus
        If txt_customer_9.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_9"
            form_input_customer.Focus()
        End If
    End Sub
    Private Sub txt_customer_10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer_10.GotFocus
        If txt_customer_10.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_po_packing_customer_10"
            form_input_customer.Focus()
        End If
    End Sub

    Private Sub txt_asal_sj_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_1.GotFocus, _
            txt_jenis_kain_1.GotFocus, txt_warna_1.GotFocus, txt_resep_1.GotFocus, txt_partai_1.GotFocus
        If txt_gudang.Text = "" Then
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_po_packing"
            form_input_gudang.Focus()
        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_1"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_2.GotFocus, _
        txt_jenis_kain_2.GotFocus, txt_warna_2.GotFocus, txt_resep_2.GotFocus, txt_partai_2.GotFocus, txt_gulung_2.GotFocus
        If txt_asal_sj_2.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_2"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_3.GotFocus, _
        txt_jenis_kain_3.GotFocus, txt_warna_3.GotFocus, txt_resep_3.GotFocus, txt_partai_3.GotFocus, txt_gulung_3.GotFocus
        If txt_asal_sj_3.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_3"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_4.GotFocus, _
        txt_jenis_kain_4.GotFocus, txt_warna_4.GotFocus, txt_resep_4.GotFocus, txt_partai_4.GotFocus, txt_gulung_4.GotFocus
        If txt_asal_sj_4.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_4"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_5.GotFocus, _
        txt_jenis_kain_5.GotFocus, txt_warna_5.GotFocus, txt_resep_5.GotFocus, txt_partai_5.GotFocus, txt_gulung_5.GotFocus
        If txt_asal_sj_5.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_5"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_6.GotFocus, _
        txt_jenis_kain_6.GotFocus, txt_warna_6.GotFocus, txt_resep_6.GotFocus, txt_partai_6.GotFocus, txt_gulung_6.GotFocus
        If txt_asal_sj_6.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_6"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_7.GotFocus, _
        txt_jenis_kain_7.GotFocus, txt_warna_7.GotFocus, txt_resep_7.GotFocus, txt_partai_7.GotFocus, txt_gulung_7.GotFocus
        If txt_asal_sj_7.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_7"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_8.GotFocus, _
        txt_jenis_kain_8.GotFocus, txt_warna_8.GotFocus, txt_resep_8.GotFocus, txt_partai_8.GotFocus, txt_gulung_8.GotFocus
        If txt_asal_sj_8.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_8"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_9.GotFocus, _
        txt_jenis_kain_9.GotFocus, txt_warna_9.GotFocus, txt_resep_9.GotFocus, txt_partai_9.GotFocus, txt_gulung_9.GotFocus
        If txt_asal_sj_9.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_9"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_10.GotFocus, _
        txt_jenis_kain_10.GotFocus, txt_warna_10.GotFocus, txt_resep_10.GotFocus, txt_partai_10.GotFocus, txt_gulung_10.GotFocus
        If txt_asal_sj_10.ReadOnly = True Then

        Else
            form_stok_ex_proses.MdiParent = form_menu_utama
            form_stok_ex_proses.Show()
            form_stok_ex_proses.txt_form.Text = "form_input_po_packing_baris_10"
            form_stok_ex_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_ex_proses.txt_cari_gudang.ReadOnly = True
            form_stok_ex_proses.Focus()
        End If
    End Sub

    Private Sub btn_hapus_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_2.Click
        txt_asal_sj_2.Text = ""
        txt_asal_sj_2.ReadOnly = False
        txt_jenis_kain_2.Text = ""
        txt_jenis_kain_2.ReadOnly = False
        txt_warna_2.Text = ""
        txt_warna_2.ReadOnly = False
        txt_resep_2.Text = ""
        txt_resep_2.ReadOnly = False
        txt_partai_2.Text = ""
        txt_partai_2.ReadOnly = False
        txt_gulung_2.Text = ""
        txt_gulung_2.ReadOnly = False
        txt_meter_2.Text = ""
        txt_customer_2.Text = ""
        txt_aksesoris_2.Text = ""
        txt_keterangan_2.Text = ""
        txt_qty_asal_2.Text = ""
        txt_harga_2.Text = ""
        txt_id_grey_2.Text = ""
    End Sub
    Private Sub btn_hapus_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_3.Click
        txt_asal_sj_3.Text = ""
        txt_asal_sj_3.ReadOnly = False
        txt_jenis_kain_3.Text = ""
        txt_jenis_kain_3.ReadOnly = False
        txt_warna_3.Text = ""
        txt_warna_3.ReadOnly = False
        txt_resep_3.Text = ""
        txt_resep_3.ReadOnly = False
        txt_partai_3.Text = ""
        txt_partai_3.ReadOnly = False
        txt_gulung_3.Text = ""
        txt_gulung_3.ReadOnly = False
        txt_meter_3.Text = ""
        txt_customer_3.Text = ""
        txt_aksesoris_3.Text = ""
        txt_keterangan_3.Text = ""
        txt_qty_asal_3.Text = ""
        txt_harga_3.Text = ""
        txt_id_grey_3.Text = ""
    End Sub
    Private Sub btn_hapus_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_4.Click
        txt_asal_sj_4.Text = ""
        txt_asal_sj_4.ReadOnly = False
        txt_jenis_kain_4.Text = ""
        txt_jenis_kain_4.ReadOnly = False
        txt_warna_4.Text = ""
        txt_warna_4.ReadOnly = False
        txt_resep_4.Text = ""
        txt_resep_4.ReadOnly = False
        txt_partai_4.Text = ""
        txt_partai_4.ReadOnly = False
        txt_gulung_4.Text = ""
        txt_gulung_4.ReadOnly = False
        txt_meter_4.Text = ""
        txt_customer_4.Text = ""
        txt_aksesoris_4.Text = ""
        txt_keterangan_4.Text = ""
        txt_qty_asal_4.Text = ""
        txt_harga_4.Text = ""
        txt_id_grey_4.Text = ""
    End Sub
    Private Sub btn_hapus_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_5.Click
        txt_asal_sj_5.Text = ""
        txt_asal_sj_5.ReadOnly = False
        txt_jenis_kain_5.Text = ""
        txt_jenis_kain_5.ReadOnly = False
        txt_warna_5.Text = ""
        txt_warna_5.ReadOnly = False
        txt_resep_5.Text = ""
        txt_resep_5.ReadOnly = False
        txt_partai_5.Text = ""
        txt_partai_5.ReadOnly = False
        txt_gulung_5.Text = ""
        txt_gulung_5.ReadOnly = False
        txt_meter_5.Text = ""
        txt_customer_5.Text = ""
        txt_aksesoris_5.Text = ""
        txt_keterangan_5.Text = ""
        txt_qty_asal_5.Text = ""
        txt_harga_5.Text = ""
        txt_id_grey_5.Text = ""
    End Sub
    Private Sub btn_hapus_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_6.Click
        txt_asal_sj_6.Text = ""
        txt_asal_sj_6.ReadOnly = False
        txt_jenis_kain_6.Text = ""
        txt_jenis_kain_6.ReadOnly = False
        txt_warna_6.Text = ""
        txt_warna_6.ReadOnly = False
        txt_resep_6.Text = ""
        txt_resep_6.ReadOnly = False
        txt_partai_6.Text = ""
        txt_partai_6.ReadOnly = False
        txt_gulung_6.Text = ""
        txt_gulung_6.ReadOnly = False
        txt_meter_6.Text = ""
        txt_customer_6.Text = ""
        txt_aksesoris_6.Text = ""
        txt_keterangan_6.Text = ""
        txt_qty_asal_6.Text = ""
        txt_harga_6.Text = ""
        txt_id_grey_6.Text = ""
    End Sub
    Private Sub btn_hapus_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_7.Click
        txt_asal_sj_7.Text = ""
        txt_asal_sj_7.ReadOnly = False
        txt_jenis_kain_7.Text = ""
        txt_jenis_kain_7.ReadOnly = False
        txt_warna_7.Text = ""
        txt_warna_7.ReadOnly = False
        txt_resep_7.Text = ""
        txt_resep_7.ReadOnly = False
        txt_partai_7.Text = ""
        txt_partai_7.ReadOnly = False
        txt_gulung_7.Text = ""
        txt_gulung_7.ReadOnly = False
        txt_meter_7.Text = ""
        txt_customer_7.Text = ""
        txt_aksesoris_7.Text = ""
        txt_keterangan_7.Text = ""
        txt_qty_asal_7.Text = ""
        txt_harga_7.Text = ""
        txt_id_grey_7.Text = ""
    End Sub
    Private Sub btn_hapus_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_8.Click
        txt_asal_sj_8.Text = ""
        txt_asal_sj_8.ReadOnly = False
        txt_jenis_kain_8.Text = ""
        txt_jenis_kain_8.ReadOnly = False
        txt_warna_8.Text = ""
        txt_warna_8.ReadOnly = False
        txt_resep_8.Text = ""
        txt_resep_8.ReadOnly = False
        txt_partai_8.Text = ""
        txt_partai_8.ReadOnly = False
        txt_gulung_8.Text = ""
        txt_gulung_8.ReadOnly = False
        txt_meter_8.Text = ""
        txt_customer_8.Text = ""
        txt_aksesoris_8.Text = ""
        txt_keterangan_8.Text = ""
        txt_qty_asal_8.Text = ""
        txt_harga_8.Text = ""
        txt_id_grey_8.Text = ""
    End Sub
    Private Sub btn_hapus_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_9.Click
        txt_asal_sj_9.Text = ""
        txt_asal_sj_9.ReadOnly = False
        txt_jenis_kain_9.Text = ""
        txt_jenis_kain_9.ReadOnly = False
        txt_warna_9.Text = ""
        txt_warna_9.ReadOnly = False
        txt_resep_9.Text = ""
        txt_resep_9.ReadOnly = False
        txt_partai_9.Text = ""
        txt_partai_9.ReadOnly = False
        txt_gulung_9.Text = ""
        txt_gulung_9.ReadOnly = False
        txt_meter_9.Text = ""
        txt_customer_9.Text = ""
        txt_aksesoris_9.Text = ""
        txt_keterangan_9.Text = ""
        txt_qty_asal_9.Text = ""
        txt_harga_9.Text = ""
        txt_id_grey_9.Text = ""
    End Sub
    Private Sub btn_hapus_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_10.Click
        txt_asal_sj_10.Text = ""
        txt_asal_sj_10.ReadOnly = False
        txt_jenis_kain_10.Text = ""
        txt_jenis_kain_10.ReadOnly = False
        txt_warna_10.Text = ""
        txt_warna_10.ReadOnly = False
        txt_resep_10.Text = ""
        txt_resep_10.ReadOnly = False
        txt_partai_10.Text = ""
        txt_partai_10.ReadOnly = False
        txt_gulung_10.Text = ""
        txt_gulung_10.ReadOnly = False
        txt_meter_10.Text = ""
        txt_customer_10.Text = ""
        txt_aksesoris_10.Text = ""
        txt_keterangan_10.Text = ""
        txt_qty_asal_10.Text = ""
        txt_harga_10.Text = ""
        txt_id_grey_10.Text = ""
    End Sub

    Private Sub txt_gulung_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_1.KeyPress
        If txt_gulung_1.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_2.KeyPress
        If txt_gulung_2.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_3.KeyPress
        If txt_gulung_3.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_4.KeyPress
        If txt_gulung_4.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_5.KeyPress
        If txt_gulung_5.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_6.KeyPress
        If txt_gulung_6.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_7.KeyPress
        If txt_gulung_7.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_8.KeyPress
        If txt_gulung_8.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_9.KeyPress
        If txt_gulung_9.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_10.KeyPress
        If txt_gulung_10.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_gulung_10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub

    Private Sub txt_meter_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_1.Text = String.Empty Then
                        txt_meter_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_1.Select(txt_meter_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_1.Text = String.Empty Then
                        txt_meter_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_1.Select(txt_meter_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_2.Text = String.Empty Then
                        txt_meter_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_2.Select(txt_meter_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_2.Text = String.Empty Then
                        txt_meter_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_2.Select(txt_meter_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_3.Text = String.Empty Then
                        txt_meter_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_3.Select(txt_meter_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_3.Text = String.Empty Then
                        txt_meter_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_3.Select(txt_meter_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_4.Text = String.Empty Then
                        txt_meter_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_4.Select(txt_meter_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_4.Text = String.Empty Then
                        txt_meter_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_4.Select(txt_meter_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_5.Text = String.Empty Then
                        txt_meter_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_5.Select(txt_meter_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_5.Text = String.Empty Then
                        txt_meter_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_5.Select(txt_meter_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_6.Text = String.Empty Then
                        txt_meter_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_6.Select(txt_meter_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_6.Text = String.Empty Then
                        txt_meter_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_6.Select(txt_meter_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_7.Text = String.Empty Then
                        txt_meter_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_7.Select(txt_meter_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_7.Text = String.Empty Then
                        txt_meter_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_7.Select(txt_meter_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_8.Text = String.Empty Then
                        txt_meter_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_8.Select(txt_meter_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_8.Text = String.Empty Then
                        txt_meter_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_8.Select(txt_meter_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_9.Text = String.Empty Then
                        txt_meter_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_9.Select(txt_meter_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_9.Text = String.Empty Then
                        txt_meter_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_9.Select(txt_meter_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_10.Text = String.Empty Then
                        txt_meter_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_10.Select(txt_meter_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_10.Text = String.Empty Then
                        txt_meter_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_10.Select(txt_meter_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_meter_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_1.TextChanged
        If txt_meter_1.Text <> String.Empty Then
            Dim temp As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_1.Select(txt_meter_1.Text.Length, 0)
            ElseIf txt_meter_1.Text = "-"c Then

            Else
                txt_meter_1.Text = CDec(temp).ToString("N0")
                txt_meter_1.Select(txt_meter_1.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_2.TextChanged
        If txt_meter_2.Text <> String.Empty Then
            Dim temp As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_2.Select(txt_meter_2.Text.Length, 0)
            ElseIf txt_meter_2.Text = "-"c Then

            Else
                txt_meter_2.Text = CDec(temp).ToString("N0")
                txt_meter_2.Select(txt_meter_2.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_3.TextChanged
        If txt_meter_3.Text <> String.Empty Then
            Dim temp As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_3.Select(txt_meter_3.Text.Length, 0)
            ElseIf txt_meter_3.Text = "-"c Then

            Else
                txt_meter_3.Text = CDec(temp).ToString("N0")
                txt_meter_3.Select(txt_meter_3.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_4.TextChanged
        If txt_meter_4.Text <> String.Empty Then
            Dim temp As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_4.Select(txt_meter_4.Text.Length, 0)
            ElseIf txt_meter_4.Text = "-"c Then

            Else
                txt_meter_4.Text = CDec(temp).ToString("N0")
                txt_meter_4.Select(txt_meter_4.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_5.TextChanged
        If txt_meter_5.Text <> String.Empty Then
            Dim temp As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_5.Select(txt_meter_5.Text.Length, 0)
            ElseIf txt_meter_5.Text = "-"c Then

            Else
                txt_meter_5.Text = CDec(temp).ToString("N0")
                txt_meter_5.Select(txt_meter_5.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_6.TextChanged
        If txt_meter_6.Text <> String.Empty Then
            Dim temp As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_6.Select(txt_meter_6.Text.Length, 0)
            ElseIf txt_meter_6.Text = "-"c Then

            Else
                txt_meter_6.Text = CDec(temp).ToString("N0")
                txt_meter_6.Select(txt_meter_6.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_7.TextChanged
        If txt_meter_7.Text <> String.Empty Then
            Dim temp As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_7.Select(txt_meter_7.Text.Length, 0)
            ElseIf txt_meter_7.Text = "-"c Then

            Else
                txt_meter_7.Text = CDec(temp).ToString("N0")
                txt_meter_7.Select(txt_meter_7.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_8.TextChanged
        If txt_meter_8.Text <> String.Empty Then
            Dim temp As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_8.Select(txt_meter_8.Text.Length, 0)
            ElseIf txt_meter_8.Text = "-"c Then

            Else
                txt_meter_8.Text = CDec(temp).ToString("N0")
                txt_meter_8.Select(txt_meter_8.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_9.TextChanged
        If txt_meter_9.Text <> String.Empty Then
            Dim temp As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_9.Select(txt_meter_9.Text.Length, 0)
            ElseIf txt_meter_9.Text = "-"c Then

            Else
                txt_meter_9.Text = CDec(temp).ToString("N0")
                txt_meter_9.Select(txt_meter_9.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_10.TextChanged
        If txt_meter_10.Text <> String.Empty Then
            Dim temp As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_10.Select(txt_meter_10.Text.Length, 0)
            ElseIf txt_meter_10.Text = "-"c Then

            Else
                txt_meter_10.Text = CDec(temp).ToString("N0")
                txt_meter_10.Select(txt_meter_10.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gudang.TextChanged
        txt_asal_sj_1.Text = ""
        txt_asal_sj_1.ReadOnly = False
        txt_jenis_kain_1.Text = ""
        txt_jenis_kain_1.ReadOnly = False
        txt_warna_1.Text = ""
        txt_warna_1.ReadOnly = False
        txt_resep_1.Text = ""
        txt_resep_1.ReadOnly = False
        txt_partai_1.Text = ""
        txt_partai_1.ReadOnly = False
        txt_gulung_1.Text = ""
        txt_gulung_1.ReadOnly = False
        txt_meter_1.Text = ""
        txt_customer_1.Text = ""
        txt_aksesoris_1.Text = ""
        txt_keterangan_1.Text = ""
        txt_qty_asal_1.Text = ""
        txt_harga_1.Text = ""
        txt_id_grey_1.Text = ""
    End Sub
    Private Sub btn_tambah_baris_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_1.Click
        Try
            If txt_gudang.Text = "" Then
                MsgBox("Input GUDANG Terlebih Dahulu")
                txt_gudang.Focus()
            ElseIf txt_no_po.Text = "" Then
                MsgBox("Input NO PO Terlebih Dahulu")
                txt_no_po.Focus()
            ElseIf txt_asal_sj_1.Text = "" And txt_jenis_kain_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_1.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_1.Focus()
            ElseIf txt_customer_1.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_1.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_1.Enabled = False
                btn_tambah_baris_1.Visible = False
                btn_selesai_1.Visible = False
                btn_batal_1.Visible = False
                Panel_2.Visible = True
                btn_hapus_2.Visible = True
                btn_tambah_baris_2.Visible = True
                btn_selesai_2.Visible = True
                btn_batal_2.Visible = True
                Call isiidpopacking_1()
                Call simpan_tbpopacking_baris_1()
                Call update_tbstokexproses_baris_1()
                Call simpan_tbstokwippacking_baris_1()
                Call update_status_tbsuratjalan_1()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_2.Click
        Try
            If txt_asal_sj_2.Text = "" And txt_jenis_kain_2.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_2.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_2.Focus()
            ElseIf txt_customer_2.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_2.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_2.Enabled = False
                btn_tambah_baris_2.Visible = False
                btn_selesai_2.Visible = False
                btn_batal_2.Visible = False
                btn_hapus_2.Visible = False
                Panel_3.Visible = True
                btn_hapus_3.Visible = True
                btn_tambah_baris_3.Visible = True
                btn_selesai_3.Visible = True
                btn_batal_3.Visible = True
                Call isiidpopacking_2()
                Call simpan_tbpopacking_baris_2()
                Call update_tbstokexproses_baris_2()
                Call simpan_tbstokwippacking_baris_2()
                Call update_status_tbsuratjalan_2()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_3.Click
        Try
            If txt_asal_sj_3.Text = "" And txt_jenis_kain_3.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_3.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_3.Focus()
            ElseIf txt_customer_3.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_3.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_3.Enabled = False
                btn_tambah_baris_3.Visible = False
                btn_selesai_3.Visible = False
                btn_batal_3.Visible = False
                btn_hapus_3.Visible = False
                Panel_4.Visible = True
                btn_hapus_4.Visible = True
                btn_tambah_baris_4.Visible = True
                btn_selesai_4.Visible = True
                btn_batal_4.Visible = True
                Call isiidpopacking_3()
                Call simpan_tbpopacking_baris_3()
                Call update_tbstokexproses_baris_3()
                Call simpan_tbstokwippacking_baris_3()
                Call update_status_tbsuratjalan_3()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_4.Click
        Try
            If txt_asal_sj_4.Text = "" And txt_jenis_kain_4.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_4.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_4.Focus()
            ElseIf txt_customer_4.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_4.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_4.Enabled = False
                btn_tambah_baris_4.Visible = False
                btn_selesai_4.Visible = False
                btn_batal_4.Visible = False
                btn_hapus_4.Visible = False
                Panel_5.Visible = True
                btn_hapus_5.Visible = True
                btn_tambah_baris_5.Visible = True
                btn_selesai_5.Visible = True
                btn_batal_5.Visible = True
                Call isiidpopacking_4()
                Call simpan_tbpopacking_baris_4()
                Call update_tbstokexproses_baris_4()
                Call simpan_tbstokwippacking_baris_4()
                Call update_status_tbsuratjalan_4()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_5.Click
        Try
            If txt_asal_sj_5.Text = "" And txt_jenis_kain_5.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_5.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_5.Focus()
            ElseIf txt_customer_5.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_5.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_5.Enabled = False
                btn_tambah_baris_5.Visible = False
                btn_selesai_5.Visible = False
                btn_batal_5.Visible = False
                btn_hapus_5.Visible = False
                Panel_6.Visible = True
                btn_hapus_6.Visible = True
                btn_tambah_baris_6.Visible = True
                btn_selesai_6.Visible = True
                btn_batal_6.Visible = True
                Call isiidpopacking_5()
                Call simpan_tbpopacking_baris_5()
                Call update_tbstokexproses_baris_5()
                Call simpan_tbstokwippacking_baris_5()
                Call update_status_tbsuratjalan_5()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_6.Click
        Try
            If txt_asal_sj_6.Text = "" And txt_jenis_kain_6.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_6.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_6.Focus()
            ElseIf txt_customer_6.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_6.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_6.Enabled = False
                btn_tambah_baris_6.Visible = False
                btn_selesai_6.Visible = False
                btn_batal_6.Visible = False
                btn_hapus_6.Visible = False
                Panel_7.Visible = True
                btn_hapus_7.Visible = True
                btn_tambah_baris_7.Visible = True
                btn_selesai_7.Visible = True
                btn_batal_7.Visible = True
                Call isiidpopacking_6()
                Call simpan_tbpopacking_baris_6()
                Call update_tbstokexproses_baris_6()
                Call simpan_tbstokwippacking_baris_6()
                Call update_status_tbsuratjalan_6()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_7.Click
        Try
            If txt_asal_sj_7.Text = "" And txt_jenis_kain_7.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_7.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_7.Focus()
            ElseIf txt_customer_7.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_7.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_7.Enabled = False
                btn_tambah_baris_7.Visible = False
                btn_selesai_7.Visible = False
                btn_batal_7.Visible = False
                btn_hapus_7.Visible = False
                Panel_8.Visible = True
                btn_hapus_8.Visible = True
                btn_tambah_baris_8.Visible = True
                btn_selesai_8.Visible = True
                btn_batal_8.Visible = True
                Call isiidpopacking_7()
                Call simpan_tbpopacking_baris_7()
                Call update_tbstokexproses_baris_7()
                Call simpan_tbstokwippacking_baris_7()
                Call update_status_tbsuratjalan_7()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_8.Click
        Try
            If txt_asal_sj_8.Text = "" And txt_jenis_kain_8.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_8.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_8.Focus()
            ElseIf txt_customer_8.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_8.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_8.Enabled = False
                btn_tambah_baris_8.Visible = False
                btn_selesai_8.Visible = False
                btn_batal_8.Visible = False
                btn_hapus_8.Visible = False
                Panel_9.Visible = True
                btn_hapus_9.Visible = True
                btn_tambah_baris_9.Visible = True
                btn_selesai_9.Visible = True
                btn_batal_9.Visible = True
                Call isiidpopacking_8()
                Call simpan_tbpopacking_baris_8()
                Call update_tbstokexproses_baris_8()
                Call simpan_tbstokwippacking_baris_8()
                Call update_status_tbsuratjalan_8()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_9.Click
        Try
            If txt_asal_sj_9.Text = "" And txt_jenis_kain_9.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_9.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_9.Focus()
            ElseIf txt_customer_9.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_9.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_9.Enabled = False
                btn_tambah_baris_9.Visible = False
                btn_selesai_9.Visible = False
                btn_batal_9.Visible = False
                btn_hapus_9.Visible = False
                Panel_10.Visible = True
                btn_hapus_10.Visible = True
                btn_selesai_10.Visible = True
                btn_batal_10.Visible = True
                Call isiidpopacking_9()
                Call simpan_tbpopacking_baris_9()
                Call update_tbstokexproses_baris_9()
                Call simpan_tbstokwippacking_baris_9()
                Call update_status_tbsuratjalan_9()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_selesai_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_1.Click
        Try
            If txt_gudang.Text = "" Then
                MsgBox("Input GUDANG Terlebih Dahulu")
                txt_gudang.Focus()
            ElseIf txt_no_po.Text = "" Then
                MsgBox("Input NO PO Terlebih Dahulu")
                txt_no_po.Focus()
            ElseIf txt_asal_sj_1.Text = "" And txt_jenis_kain_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_meter_1.Text) = 0 Then
                MsgBox("QTY Tidak Boleh Kosong")
                txt_meter_1.Focus()
            ElseIf txt_customer_1.Text = "" Then
                MsgBox("CUSTOMER Tidak Boleh Kosong")
                txt_customer_1.Focus()
            Else
                Call isiidpopacking_1()
                Call simpan_tbpopacking_baris_1()
                Call update_tbstokexproses_baris_1()
                Call simpan_tbstokwippacking_baris_1()
                Call update_status_tbsuratjalan_1()
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_2.Click
        Try
            If txt_asal_sj_2.Text = "" And txt_jenis_kain_2.Text = "" _
                And txt_warna_2.Text = "" And txt_resep_2.Text = "" And txt_partai_2.Text = "" _
                And txt_gulung_2.Text = "" And txt_meter_2.Text = "" And txt_customer_2.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_2.Text = "" And txt_jenis_kain_2.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_2.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_2.Focus()
                ElseIf txt_customer_2.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_2.Focus()
                Else
                    Call isiidpopacking_2()
                    Call simpan_tbpopacking_baris_2()
                    Call update_tbstokexproses_baris_2()
                    Call simpan_tbstokwippacking_baris_2()
                    Call update_status_tbsuratjalan_2()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_3.Click
        Try
            If txt_asal_sj_3.Text = "" And txt_jenis_kain_3.Text = "" _
                And txt_warna_3.Text = "" And txt_resep_3.Text = "" And txt_partai_3.Text = "" _
                And txt_gulung_3.Text = "" And txt_meter_3.Text = "" And txt_customer_3.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_3.Text = "" And txt_jenis_kain_3.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_3.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_3.Focus()
                ElseIf txt_customer_3.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_3.Focus()
                Else
                    Call isiidpopacking_3()
                    Call simpan_tbpopacking_baris_3()
                    Call update_tbstokexproses_baris_3()
                    Call simpan_tbstokwippacking_baris_3()
                    Call update_status_tbsuratjalan_3()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_4.Click
        Try
            If txt_asal_sj_4.Text = "" And txt_jenis_kain_4.Text = "" _
                And txt_warna_4.Text = "" And txt_resep_4.Text = "" And txt_partai_4.Text = "" _
                And txt_gulung_4.Text = "" And txt_meter_4.Text = "" And txt_customer_4.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_4.Text = "" And txt_jenis_kain_4.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_4.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_4.Focus()
                ElseIf txt_customer_4.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_4.Focus()
                Else
                    Call isiidpopacking_4()
                    Call simpan_tbpopacking_baris_4()
                    Call update_tbstokexproses_baris_4()
                    Call simpan_tbstokwippacking_baris_4()
                    Call update_status_tbsuratjalan_4()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_5.Click
        Try
            If txt_asal_sj_5.Text = "" And txt_jenis_kain_5.Text = "" _
                And txt_warna_5.Text = "" And txt_resep_5.Text = "" And txt_partai_5.Text = "" _
                And txt_gulung_5.Text = "" And txt_meter_5.Text = "" And txt_customer_5.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_5.Text = "" And txt_jenis_kain_5.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_5.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_5.Focus()
                ElseIf txt_customer_5.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_5.Focus()
                Else
                    Call isiidpopacking_5()
                    Call simpan_tbpopacking_baris_5()
                    Call update_tbstokexproses_baris_5()
                    Call simpan_tbstokwippacking_baris_5()
                    Call update_status_tbsuratjalan_5()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_6.Click
        Try
            If txt_asal_sj_6.Text = "" And txt_jenis_kain_6.Text = "" _
                And txt_warna_6.Text = "" And txt_resep_6.Text = "" And txt_partai_6.Text = "" _
                And txt_gulung_6.Text = "" And txt_meter_6.Text = "" And txt_customer_6.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_6.Text = "" And txt_jenis_kain_6.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_6.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_6.Focus()
                ElseIf txt_customer_6.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_6.Focus()
                Else
                    Call isiidpopacking_6()
                    Call simpan_tbpopacking_baris_6()
                    Call update_tbstokexproses_baris_6()
                    Call simpan_tbstokwippacking_baris_6()
                    Call update_status_tbsuratjalan_6()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_7.Click
        Try
            If txt_asal_sj_7.Text = "" And txt_jenis_kain_7.Text = "" _
                And txt_warna_7.Text = "" And txt_resep_7.Text = "" And txt_partai_7.Text = "" _
                And txt_gulung_7.Text = "" And txt_meter_7.Text = "" And txt_customer_7.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_7.Text = "" And txt_jenis_kain_7.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_7.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_7.Focus()
                ElseIf txt_customer_7.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_7.Focus()
                Else
                    Call isiidpopacking_7()
                    Call simpan_tbpopacking_baris_7()
                    Call update_tbstokexproses_baris_7()
                    Call simpan_tbstokwippacking_baris_7()
                    Call update_status_tbsuratjalan_7()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_8.Click
        Try
            If txt_asal_sj_8.Text = "" And txt_jenis_kain_8.Text = "" _
                And txt_warna_8.Text = "" And txt_resep_8.Text = "" And txt_partai_8.Text = "" _
                And txt_gulung_8.Text = "" And txt_meter_8.Text = "" And txt_customer_8.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_8.Text = "" And txt_jenis_kain_8.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_8.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_8.Focus()
                ElseIf txt_customer_8.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_8.Focus()
                Else
                    Call isiidpopacking_8()
                    Call simpan_tbpopacking_baris_8()
                    Call update_tbstokexproses_baris_8()
                    Call simpan_tbstokwippacking_baris_8()
                    Call update_status_tbsuratjalan_8()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_9.Click
        Try
            If txt_asal_sj_9.Text = "" And txt_jenis_kain_9.Text = "" _
                And txt_warna_9.Text = "" And txt_resep_9.Text = "" And txt_partai_9.Text = "" _
                And txt_gulung_9.Text = "" And txt_meter_9.Text = "" And txt_customer_9.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_9.Text = "" And txt_jenis_kain_9.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_9.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_9.Focus()
                ElseIf txt_customer_9.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_9.Focus()
                Else
                    Call isiidpopacking_9()
                    Call simpan_tbpopacking_baris_9()
                    Call update_tbstokexproses_baris_9()
                    Call simpan_tbstokwippacking_baris_9()
                    Call update_status_tbsuratjalan_9()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_10.Click
        Try
            If txt_asal_sj_10.Text = "" And txt_jenis_kain_10.Text = "" _
                And txt_warna_10.Text = "" And txt_resep_10.Text = "" And txt_partai_10.Text = "" _
                And txt_gulung_10.Text = "" And txt_meter_10.Text = "" And txt_customer_10.Text = "" Then
                MsgBox("PO Packing Baru Berhasil Disimpan")
                form_po_packing.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_10.Text = "" And txt_jenis_kain_10.Text = "" Then
                    MsgBox("Baris 1 Belum Diisi Data")
                ElseIf Val(txt_meter_10.Text) = 0 Then
                    MsgBox("QTY Tidak Boleh Kosong")
                    txt_meter_10.Focus()
                ElseIf txt_customer_10.Text = "" Then
                    MsgBox("CUSTOMER Tidak Boleh Kosong")
                    txt_customer_10.Focus()
                Else
                    Call isiidpopacking_10()
                    Call simpan_tbpopacking_baris_10()
                    Call update_tbstokexproses_baris_10()
                    Call simpan_tbstokwippacking_baris_10()
                    Call update_status_tbsuratjalan_10()
                    MsgBox("PO Packing Baru Berhasil Disimpan")
                    form_po_packing.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub update_tbstokexproses_baris_1()
        Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_1 As Double
                        m_1 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_1.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_1 - Math.Round(Val(meter_1.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_1.Text = "Yard" Then
                                        q_1 = Math.Round(Val(meter_1.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_1 - Math.Round(q_1, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_1.Text = "Meter" Then
                                        q_1 = Math.Round(Val(meter_1.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_1 - Math.Round(q_1, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_2()
        Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_2 As Double
                        m_2 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_2.Text Then
                                        .Parameters.AddWithValue("@1", m_2 - Math.Round(Val(meter_2.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_2.Text = "Yard" Then
                                        q_2 = Math.Round(Val(meter_2.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_2 - Math.Round(q_2, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_2.Text = "Meter" Then
                                        q_2 = Math.Round(Val(meter_2.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_2 - Math.Round(q_2, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_3()
        Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_3 As Double
                        m_3 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_3.Text Then
                                        .Parameters.AddWithValue("@1", m_3 - Math.Round(Val(meter_3.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_3.Text = "Yard" Then
                                        q_3 = Math.Round(Val(meter_3.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_3 - Math.Round(q_3, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_3.Text = "Meter" Then
                                        q_3 = Math.Round(Val(meter_3.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_3 - Math.Round(q_3, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_4()
        Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_4 As Double
                        m_4 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_4.Text Then
                                        .Parameters.AddWithValue("@1", m_4 - Math.Round(Val(meter_4.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_4.Text = "Yard" Then
                                        q_4 = Math.Round(Val(meter_4.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_4 - Math.Round(q_4, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_4.Text = "Meter" Then
                                        q_4 = Math.Round(Val(meter_4.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_4 - Math.Round(q_4, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_5()
        Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_5 As Double
                        m_5 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_5.Text Then
                                        .Parameters.AddWithValue("@1", m_5 - Math.Round(Val(meter_5.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_5.Text = "Yard" Then
                                        q_5 = Math.Round(Val(meter_5.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_5 - Math.Round(q_5, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_5.Text = "Meter" Then
                                        q_5 = Math.Round(Val(meter_5.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_5 - Math.Round(q_5, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_6()
        Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_6 As Double
                        m_6 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_6.Text Then
                                        .Parameters.AddWithValue("@1", m_6 - Math.Round(Val(meter_6.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_6.Text = "Yard" Then
                                        q_6 = Math.Round(Val(meter_6.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_6 - Math.Round(q_6, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_6.Text = "Meter" Then
                                        q_6 = Math.Round(Val(meter_6.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_6 - Math.Round(q_6, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_7()
        Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_7 As Double
                        m_7 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_7.Text Then
                                        .Parameters.AddWithValue("@1", m_7 - Math.Round(Val(meter_7.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_7.Text = "Yard" Then
                                        q_7 = Math.Round(Val(meter_7.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_7 - Math.Round(q_7, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_7.Text = "Meter" Then
                                        q_7 = Math.Round(Val(meter_7.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_7 - Math.Round(q_7, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_8()
        Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_8 As Double
                        m_8 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_8.Text Then
                                        .Parameters.AddWithValue("@1", m_8 - Math.Round(Val(meter_8.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_8.Text = "Yard" Then
                                        q_8 = Math.Round(Val(meter_8.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_8 - Math.Round(q_8, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_8.Text = "Meter" Then
                                        q_8 = Math.Round(Val(meter_8.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_8 - Math.Round(q_8, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_9()
        Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_9 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_9 As Double
                        m_9 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_9.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_9.Text Then
                                        .Parameters.AddWithValue("@1", m_9 - Math.Round(Val(meter_9.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_9.Text = "Yard" Then
                                        q_9 = Math.Round(Val(meter_9.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_9 - Math.Round(q_9, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_9.Text = "Meter" Then
                                        q_9 = Math.Round(Val(meter_9.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_9 - Math.Round(q_9, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokexproses_baris_10()
        Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_10 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_10 As Double
                        m_10 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses ='" & txt_id_sj_proses_10.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_10.Text Then
                                        .Parameters.AddWithValue("@1", m_10 - Math.Round(Val(meter_10.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_10.Text = "Yard" Then
                                        q_10 = Math.Round(Val(meter_10.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_10 - Math.Round(q_10, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_10.Text = "Meter" Then
                                        q_10 = Math.Round(Val(meter_10.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", m_10 - Math.Round(q_10, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub hapus_update_tbstokexproses_baris_1()
        Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_1 As Double
                        m_1 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_1.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_1 + Math.Round(Val(meter_1.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_1.Text = "Yard" Then
                                        q_1 = Math.Round(Val(meter_1.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_1 + Math.Round(q_1, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_1.Text = "Meter" Then
                                        q_1 = Math.Round(Val(meter_1.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_1 + Math.Round(q_1, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_2()
        Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_2 As Double
                        m_2 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_2.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_2 + Math.Round(Val(meter_2.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_2.Text = "Yard" Then
                                        q_2 = Math.Round(Val(meter_2.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_2 + Math.Round(q_2, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_2.Text = "Meter" Then
                                        q_2 = Math.Round(Val(meter_2.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_2 + Math.Round(q_2, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_3()
        Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_3 As Double
                        m_3 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_3.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_3 + Math.Round(Val(meter_3.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_3.Text = "Yard" Then
                                        q_3 = Math.Round(Val(meter_3.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_3 + Math.Round(q_3, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_3.Text = "Meter" Then
                                        q_3 = Math.Round(Val(meter_3.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_3 + Math.Round(q_3, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_4()
        Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_4 As Double
                        m_4 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_4.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_4 + Math.Round(Val(meter_4.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_4.Text = "Yard" Then
                                        q_4 = Math.Round(Val(meter_4.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_4 + Math.Round(q_4, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_4.Text = "Meter" Then
                                        q_4 = Math.Round(Val(meter_4.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_4 + Math.Round(q_4, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_5()
        Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_5 As Double
                        m_5 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_5.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_5 + Math.Round(Val(meter_5.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_5.Text = "Yard" Then
                                        q_5 = Math.Round(Val(meter_5.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_5 + Math.Round(q_5, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_5.Text = "Meter" Then
                                        q_5 = Math.Round(Val(meter_5.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_5 + Math.Round(q_5, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_6()
        Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_6 As Double
                        m_6 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_6.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_6 + Math.Round(Val(meter_6.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_6.Text = "Yard" Then
                                        q_6 = Math.Round(Val(meter_6.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_6 + Math.Round(q_6, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_6.Text = "Meter" Then
                                        q_6 = Math.Round(Val(meter_6.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_6 + Math.Round(q_6, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_7()
        Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_7 As Double
                        m_7 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_7.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_7 + Math.Round(Val(meter_7.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_7.Text = "Yard" Then
                                        q_7 = Math.Round(Val(meter_7.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_7 + Math.Round(q_7, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_7.Text = "Meter" Then
                                        q_7 = Math.Round(Val(meter_7.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_7 + Math.Round(q_7, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_8()
        Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_8 As Double
                        m_8 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_8.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_8 + Math.Round(Val(meter_8.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_8.Text = "Yard" Then
                                        q_8 = Math.Round(Val(meter_8.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_8 + Math.Round(q_8, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_8.Text = "Meter" Then
                                        q_8 = Math.Round(Val(meter_8.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_8 + Math.Round(q_8, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_9()
        Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_9 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_9 As Double
                        m_9 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_9.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_9.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_9 + Math.Round(Val(meter_9.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_9.Text = "Yard" Then
                                        q_9 = Math.Round(Val(meter_9.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_9 + Math.Round(q_9, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_9.Text = "Meter" Then
                                        q_9 = Math.Round(Val(meter_9.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_9 + Math.Round(q_9, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokexproses_baris_10()
        Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_10 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokexproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_10 As Double
                        m_10 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokexproses SET Stok=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_10.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_10.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(m_10 + Math.Round(Val(meter_10.Replace(",", ".")), 2), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_10.Text = "Yard" Then
                                        q_10 = Math.Round(Val(meter_10.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_10 + Math.Round(q_10, 2), 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_10.Text = "Meter" Then
                                        q_10 = Math.Round(Val(meter_10.Replace(",", ".")) * 1.09361, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(m_10 + Math.Round(q_10, 2), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
   
    Private Sub simpan_tbpopacking_baris_1()
        Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_1.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_1.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_1.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@6", txt_warna_1.Text)
                    .Parameters.AddWithValue("@7", txt_resep_1.Text)
                    .Parameters.AddWithValue("@8", txt_partai_1.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_1.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_1.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@12", txt_customer_1.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_1.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_1.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_1.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_1.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_1.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_1.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_2()
        Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_2.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_2.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_2.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@6", txt_warna_2.Text)
                    .Parameters.AddWithValue("@7", txt_resep_2.Text)
                    .Parameters.AddWithValue("@8", txt_partai_2.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_2.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_2.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@12", txt_customer_2.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_2.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_2.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_2.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_2.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_2.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_2.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_3()
        Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_3.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_3.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_3.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@6", txt_warna_3.Text)
                    .Parameters.AddWithValue("@7", txt_resep_3.Text)
                    .Parameters.AddWithValue("@8", txt_partai_3.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_3.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_3.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@12", txt_customer_3.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_3.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_3.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_3.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_3.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_3.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_3.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_4()
        Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_4.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_4.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_4.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@6", txt_warna_4.Text)
                    .Parameters.AddWithValue("@7", txt_resep_4.Text)
                    .Parameters.AddWithValue("@8", txt_partai_4.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_4.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_4.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@12", txt_customer_4.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_4.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_4.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_4.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_4.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_4.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_4.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_5()
        Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_5.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_5.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_5.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@6", txt_warna_5.Text)
                    .Parameters.AddWithValue("@7", txt_resep_5.Text)
                    .Parameters.AddWithValue("@8", txt_partai_5.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_5.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_5.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@12", txt_customer_5.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_5.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_5.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_5.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_5.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_5.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_5.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_6()
        Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_6.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_6.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_6.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@6", txt_warna_6.Text)
                    .Parameters.AddWithValue("@7", txt_resep_6.Text)
                    .Parameters.AddWithValue("@8", txt_partai_6.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_6.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_6.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@12", txt_customer_6.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_6.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_6.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_6.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_6.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_6.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_6.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_7()
        Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_7.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_7.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_7.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@6", txt_warna_7.Text)
                    .Parameters.AddWithValue("@7", txt_resep_7.Text)
                    .Parameters.AddWithValue("@8", txt_partai_7.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_7.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_7.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@12", txt_customer_7.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_7.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_7.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_7.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_7.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_7.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_7.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_8()
        Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_8.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_8.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_8.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@6", txt_warna_8.Text)
                    .Parameters.AddWithValue("@7", txt_resep_8.Text)
                    .Parameters.AddWithValue("@8", txt_partai_8.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_8.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_8.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@12", txt_customer_8.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_8.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_8.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_8.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_8.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_8.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_8.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_9()
        Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_9.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_9.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_9.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@6", txt_warna_9.Text)
                    .Parameters.AddWithValue("@7", txt_resep_9.Text)
                    .Parameters.AddWithValue("@8", txt_partai_9.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_9.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_9.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_9.Text)
                    .Parameters.AddWithValue("@12", txt_customer_9.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_9.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_9.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_9.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_9.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_9.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_9.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpopacking_baris_10()
        Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpopacking (Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Meter_Keluar,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_tanggal_sj_10.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", dtp_tanggal_sj_10.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_10.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@6", txt_warna_10.Text)
                    .Parameters.AddWithValue("@7", txt_resep_10.Text)
                    .Parameters.AddWithValue("@8", txt_partai_10.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_10.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", meter_10.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_10.Text)
                    .Parameters.AddWithValue("@12", txt_customer_10.Text)
                    .Parameters.AddWithValue("@13", txt_aksesoris_10.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_10.Text)
                    .Parameters.AddWithValue("@15", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@16", txt_id_sj_proses_10.Text)
                    .Parameters.AddWithValue("@17", txt_id_po_packing_10.Text)
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_harga_10.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", "")
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", "")
                    .Parameters.AddWithValue("@24", 0)
                    .Parameters.AddWithValue("@25", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_sj_10.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub

    Private Sub hapus_tbpopacking_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpopacking_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpopacking WHERE Id_Po_Packing ='" & txt_id_po_packing_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub simpan_tbstokwippacking_baris_1()
        Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_1.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@6", txt_warna_1.Text)
                    .Parameters.AddWithValue("@7", txt_resep_1.Text)
                    .Parameters.AddWithValue("@8", txt_partai_1.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_1.Text)
                    .Parameters.AddWithValue("@10", meter_1.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@12", txt_customer_1.Text)
                    .Parameters.AddWithValue("@13", txt_harga_1.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_2()
        Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_2.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@6", txt_warna_2.Text)
                    .Parameters.AddWithValue("@7", txt_resep_2.Text)
                    .Parameters.AddWithValue("@8", txt_partai_2.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_2.Text)
                    .Parameters.AddWithValue("@10", meter_2.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@12", txt_customer_2.Text)
                    .Parameters.AddWithValue("@13", txt_harga_2.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_3()
        Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_3.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@6", txt_warna_3.Text)
                    .Parameters.AddWithValue("@7", txt_resep_3.Text)
                    .Parameters.AddWithValue("@8", txt_partai_3.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_3.Text)
                    .Parameters.AddWithValue("@10", meter_3.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@12", txt_customer_3.Text)
                    .Parameters.AddWithValue("@13", txt_harga_3.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_4()
        Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_4.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@6", txt_warna_4.Text)
                    .Parameters.AddWithValue("@7", txt_resep_4.Text)
                    .Parameters.AddWithValue("@8", txt_partai_4.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_4.Text)
                    .Parameters.AddWithValue("@10", meter_4.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@12", txt_customer_4.Text)
                    .Parameters.AddWithValue("@13", txt_harga_4.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_5()
        Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_5.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@6", txt_warna_5.Text)
                    .Parameters.AddWithValue("@7", txt_resep_5.Text)
                    .Parameters.AddWithValue("@8", txt_partai_5.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_5.Text)
                    .Parameters.AddWithValue("@10", meter_5.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@12", txt_customer_5.Text)
                    .Parameters.AddWithValue("@13", txt_harga_5.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_6()
        Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_6.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@6", txt_warna_6.Text)
                    .Parameters.AddWithValue("@7", txt_resep_6.Text)
                    .Parameters.AddWithValue("@8", txt_partai_6.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_6.Text)
                    .Parameters.AddWithValue("@10", meter_6.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@12", txt_customer_6.Text)
                    .Parameters.AddWithValue("@13", txt_harga_6.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_7()
        Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_7.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@6", txt_warna_7.Text)
                    .Parameters.AddWithValue("@7", txt_resep_7.Text)
                    .Parameters.AddWithValue("@8", txt_partai_7.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_7.Text)
                    .Parameters.AddWithValue("@10", meter_7.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@12", txt_customer_7.Text)
                    .Parameters.AddWithValue("@13", txt_harga_7.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_8()
        Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_8.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@6", txt_warna_8.Text)
                    .Parameters.AddWithValue("@7", txt_resep_8.Text)
                    .Parameters.AddWithValue("@8", txt_partai_8.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_8.Text)
                    .Parameters.AddWithValue("@10", meter_8.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@12", txt_customer_8.Text)
                    .Parameters.AddWithValue("@13", txt_harga_8.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_9()
        Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_9.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@6", txt_warna_9.Text)
                    .Parameters.AddWithValue("@7", txt_resep_9.Text)
                    .Parameters.AddWithValue("@8", txt_partai_9.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_9.Text)
                    .Parameters.AddWithValue("@10", meter_9.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_9.Text)
                    .Parameters.AddWithValue("@12", txt_customer_9.Text)
                    .Parameters.AddWithValue("@13", txt_harga_9.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwippacking_baris_10()
        Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwippacking (Tanggal,Id_Grey,Id_PO_Packing,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@2", txt_id_po_packing_10.Text)
                    .Parameters.AddWithValue("@3", txt_no_po.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@6", txt_warna_10.Text)
                    .Parameters.AddWithValue("@7", txt_resep_10.Text)
                    .Parameters.AddWithValue("@8", txt_partai_10.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_10.Text)
                    .Parameters.AddWithValue("@10", meter_10.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_10.Text)
                    .Parameters.AddWithValue("@12", txt_customer_10.Text)
                    .Parameters.AddWithValue("@13", txt_harga_10.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub hapus_tbstokwippacking_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwippacking_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwippacking WHERE Id_Po_Packing ='" & txt_id_po_packing_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub update_status_tbsuratjalan_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_status_tbsuratjalan_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", ("Closed"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
   
    Private Sub hapus_update_status_tbsuratjalan_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_1.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_2.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_3.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_4.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_5.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_6.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_7.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_8.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_9.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_status_tbsuratjalan_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbsuratjalanproses SET STATUS=@1 WHERE Id_SJ_Proses='" & txt_id_sj_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_status_sj_proses_10.Text = "Closed" Then
                        .Parameters.AddWithValue("@1", "Closed")
                    Else
                        .Parameters.AddWithValue("@1", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Sub btn_batal_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_1.Click
        Try
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_2.Click
        Try
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_3.Click
        Try
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_4.Click
        Try
            Call hapus_tbpopacking_3()
            Call hapus_update_tbstokexproses_baris_3()
            Call hapus_tbstokwippacking_3()
            Call hapus_update_status_tbsuratjalan_3()
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_5.Click
        Try
            Call hapus_tbpopacking_4()
            Call hapus_update_tbstokexproses_baris_4()
            Call hapus_tbstokwippacking_4()
            Call hapus_update_status_tbsuratjalan_4()
            Call hapus_tbpopacking_3()
            Call hapus_update_tbstokexproses_baris_3()
            Call hapus_tbstokwippacking_3()
            Call hapus_update_status_tbsuratjalan_3()
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_6.Click
        Try
            Call hapus_tbpopacking_5()
            Call hapus_update_tbstokexproses_baris_5()
            Call hapus_tbstokwippacking_5()
            Call hapus_update_status_tbsuratjalan_5()
            Call hapus_tbpopacking_4()
            Call hapus_update_tbstokexproses_baris_4()
            Call hapus_tbstokwippacking_4()
            Call hapus_update_status_tbsuratjalan_4()
            Call hapus_tbpopacking_3()
            Call hapus_update_tbstokexproses_baris_3()
            Call hapus_tbstokwippacking_3()
            Call hapus_update_status_tbsuratjalan_3()
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_7.Click
        Try
            Call hapus_tbpopacking_6()
            Call hapus_update_tbstokexproses_baris_6()
            Call hapus_tbstokwippacking_6()
            Call hapus_update_status_tbsuratjalan_6()
            Call hapus_tbpopacking_5()
            Call hapus_update_tbstokexproses_baris_5()
            Call hapus_tbstokwippacking_5()
            Call hapus_update_status_tbsuratjalan_5()
            Call hapus_tbpopacking_4()
            Call hapus_update_tbstokexproses_baris_4()
            Call hapus_tbstokwippacking_4()
            Call hapus_update_status_tbsuratjalan_4()
            Call hapus_tbpopacking_3()
            Call hapus_update_tbstokexproses_baris_3()
            Call hapus_tbstokwippacking_3()
            Call hapus_update_status_tbsuratjalan_3()
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_8.Click
        Try
            Call hapus_tbpopacking_7()
            Call hapus_update_tbstokexproses_baris_7()
            Call hapus_tbstokwippacking_7()
            Call hapus_update_status_tbsuratjalan_7()
            Call hapus_tbpopacking_6()
            Call hapus_update_tbstokexproses_baris_6()
            Call hapus_tbstokwippacking_6()
            Call hapus_update_status_tbsuratjalan_6()
            Call hapus_tbpopacking_5()
            Call hapus_update_tbstokexproses_baris_5()
            Call hapus_tbstokwippacking_5()
            Call hapus_update_status_tbsuratjalan_5()
            Call hapus_tbpopacking_4()
            Call hapus_update_tbstokexproses_baris_4()
            Call hapus_tbstokwippacking_4()
            Call hapus_update_status_tbsuratjalan_4()
            Call hapus_tbpopacking_3()
            Call hapus_update_tbstokexproses_baris_3()
            Call hapus_tbstokwippacking_3()
            Call hapus_update_status_tbsuratjalan_3()
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_9.Click
        Try
            Call hapus_tbpopacking_8()
            Call hapus_update_tbstokexproses_baris_8()
            Call hapus_tbstokwippacking_8()
            Call hapus_update_status_tbsuratjalan_8()
            Call hapus_tbpopacking_7()
            Call hapus_update_tbstokexproses_baris_7()
            Call hapus_tbstokwippacking_7()
            Call hapus_update_status_tbsuratjalan_7()
            Call hapus_tbpopacking_6()
            Call hapus_update_tbstokexproses_baris_6()
            Call hapus_tbstokwippacking_6()
            Call hapus_update_status_tbsuratjalan_6()
            Call hapus_tbpopacking_5()
            Call hapus_update_tbstokexproses_baris_5()
            Call hapus_tbstokwippacking_5()
            Call hapus_update_status_tbsuratjalan_5()
            Call hapus_tbpopacking_4()
            Call hapus_update_tbstokexproses_baris_4()
            Call hapus_tbstokwippacking_4()
            Call hapus_update_status_tbsuratjalan_4()
            Call hapus_tbpopacking_3()
            Call hapus_update_tbstokexproses_baris_3()
            Call hapus_tbstokwippacking_3()
            Call hapus_update_status_tbsuratjalan_3()
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_10.Click
        Try
            Call hapus_tbpopacking_9()
            Call hapus_update_tbstokexproses_baris_9()
            Call hapus_tbstokwippacking_9()
            Call hapus_update_status_tbsuratjalan_9()
            Call hapus_tbpopacking_8()
            Call hapus_update_tbstokexproses_baris_8()
            Call hapus_tbstokwippacking_8()
            Call hapus_update_status_tbsuratjalan_8()
            Call hapus_tbpopacking_7()
            Call hapus_update_tbstokexproses_baris_7()
            Call hapus_tbstokwippacking_7()
            Call hapus_update_status_tbsuratjalan_7()
            Call hapus_tbpopacking_6()
            Call hapus_update_tbstokexproses_baris_6()
            Call hapus_tbstokwippacking_6()
            Call hapus_update_status_tbsuratjalan_6()
            Call hapus_tbpopacking_5()
            Call hapus_update_tbstokexproses_baris_5()
            Call hapus_tbstokwippacking_5()
            Call hapus_update_status_tbsuratjalan_5()
            Call hapus_tbpopacking_4()
            Call hapus_update_tbstokexproses_baris_4()
            Call hapus_tbstokwippacking_4()
            Call hapus_update_status_tbsuratjalan_4()
            Call hapus_tbpopacking_3()
            Call hapus_update_tbstokexproses_baris_3()
            Call hapus_tbstokwippacking_3()
            Call hapus_update_status_tbsuratjalan_3()
            Call hapus_tbpopacking_2()
            Call hapus_update_tbstokexproses_baris_2()
            Call hapus_tbstokwippacking_2()
            Call hapus_update_status_tbsuratjalan_2()
            Call hapus_tbpopacking_1()
            Call hapus_update_tbstokexproses_baris_1()
            Call hapus_tbstokwippacking_1()
            Call hapus_update_status_tbsuratjalan_1()
            form_po_packing.Show()
            form_po_packing.Focus()
            form_po_packing.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub cb_satuan_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_1.TextChanged
        Dim qty_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1, qa_1, h_1 As Double
        If txt_meter_1.Text = "" Then
        ElseIf Val(txt_meter_1.Text) = 0 Then
        Else
            If cb_satuan_1.Text = "Yard" Then
                qa_1 = Math.Round(Val(txt_qty_asal_1.Text.Replace(",", ".")) * 1.0936, 0)
                q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_1.Text = "Meter" Then
                qa_1 = Math.Round(Val(txt_qty_asal_1.Text.Replace(",", ".")) * 0.9144, 0)
                q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_1.Text = qa_1
            txt_meter_1.Text = q_1
            txt_harga_1.Text = h_1
        End If
    End Sub
    Private Sub cb_satuan_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_2.TextChanged
        Dim qty_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2, qa_2, h_2 As Double
        If txt_meter_2.Text = "" Then
        ElseIf Val(txt_meter_2.Text) = 0 Then
        Else
            If cb_satuan_2.Text = "Yard" Then
                qa_2 = Math.Round(Val(txt_qty_asal_2.Text.Replace(",", ".")) * 1.0936, 0)
                q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_2.Text = "Meter" Then
                qa_2 = Math.Round(Val(txt_qty_asal_2.Text.Replace(",", ".")) * 0.9144, 0)
                q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_2.Text = qa_2
            txt_meter_2.Text = q_2
            txt_harga_2.Text = h_2
        End If
    End Sub
    Private Sub cb_satuan_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_3.TextChanged
        Dim qty_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3, qa_3, h_3 As Double
        If txt_meter_3.Text = "" Then
        ElseIf Val(txt_meter_3.Text) = 0 Then
        Else
            If cb_satuan_3.Text = "Yard" Then
                qa_3 = Math.Round(Val(txt_qty_asal_3.Text.Replace(",", ".")) * 1.0936, 0)
                q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_3.Text = "Meter" Then
                qa_3 = Math.Round(Val(txt_qty_asal_3.Text.Replace(",", ".")) * 0.9144, 0)
                q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_3.Text = qa_3
            txt_meter_3.Text = q_3
            txt_harga_3.Text = h_3
        End If
    End Sub
    Private Sub cb_satuan_4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_4.TextChanged
        Dim qty_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4, qa_4, h_4 As Double
        If txt_meter_4.Text = "" Then
        ElseIf Val(txt_meter_4.Text) = 0 Then
        Else
            If cb_satuan_4.Text = "Yard" Then
                qa_4 = Math.Round(Val(txt_qty_asal_4.Text.Replace(",", ".")) * 1.0936, 0)
                q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_4.Text = "Meter" Then
                qa_4 = Math.Round(Val(txt_qty_asal_4.Text.Replace(",", ".")) * 0.9144, 0)
                q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_4.Text = qa_4
            txt_meter_4.Text = q_4
            txt_harga_4.Text = h_4
        End If
    End Sub
    Private Sub cb_satuan_5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_5.TextChanged
        Dim qty_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5, qa_5, h_5 As Double
        If txt_meter_5.Text = "" Then
        ElseIf Val(txt_meter_5.Text) = 0 Then
        Else
            If cb_satuan_5.Text = "Yard" Then
                qa_5 = Math.Round(Val(txt_qty_asal_5.Text.Replace(",", ".")) * 1.0936, 0)
                q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_5.Text = "Meter" Then
                qa_5 = Math.Round(Val(txt_qty_asal_5.Text.Replace(",", ".")) * 0.9144, 0)
                q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_5.Text = qa_5
            txt_meter_5.Text = q_5
            txt_harga_5.Text = h_5
        End If
    End Sub
    Private Sub cb_satuan_6_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_6.TextChanged
        Dim qty_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6, qa_6, h_6 As Double
        If txt_meter_6.Text = "" Then
        ElseIf Val(txt_meter_6.Text) = 0 Then
        Else
            If cb_satuan_6.Text = "Yard" Then
                qa_6 = Math.Round(Val(txt_qty_asal_6.Text.Replace(",", ".")) * 1.0936, 0)
                q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_6.Text = "Meter" Then
                qa_6 = Math.Round(Val(txt_qty_asal_6.Text.Replace(",", ".")) * 0.9144, 0)
                q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_6.Text = qa_6
            txt_meter_6.Text = q_6
            txt_harga_6.Text = h_6
        End If
    End Sub
    Private Sub cb_satuan_7_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_7.TextChanged
        Dim qty_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7, qa_7, h_7 As Double
        If txt_meter_7.Text = "" Then
        ElseIf Val(txt_meter_7.Text) = 0 Then
        Else
            If cb_satuan_7.Text = "Yard" Then
                qa_7 = Math.Round(Val(txt_qty_asal_7.Text.Replace(",", ".")) * 1.0936, 0)
                q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_7.Text = "Meter" Then
                qa_7 = Math.Round(Val(txt_qty_asal_7.Text.Replace(",", ".")) * 0.9144, 0)
                q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_7.Text = qa_7
            txt_meter_7.Text = q_7
            txt_harga_7.Text = h_7
        End If
    End Sub
    Private Sub cb_satuan_8_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_8.TextChanged
        Dim qty_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8, qa_8, h_8 As Double
        If txt_meter_8.Text = "" Then
        ElseIf Val(txt_meter_8.Text) = 0 Then
        Else
            If cb_satuan_8.Text = "Yard" Then
                qa_8 = Math.Round(Val(txt_qty_asal_8.Text.Replace(",", ".")) * 1.0936, 0)
                q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_8.Text = "Meter" Then
                qa_8 = Math.Round(Val(txt_qty_asal_8.Text.Replace(",", ".")) * 0.9144, 0)
                q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_8.Text = qa_8
            txt_meter_8.Text = q_8
            txt_harga_8.Text = h_8
        End If
    End Sub
    Private Sub cb_satuan_9_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_9.TextChanged
        Dim qty_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_9 As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_9, qa_9, h_9 As Double
        If txt_meter_9.Text = "" Then
        ElseIf Val(txt_meter_9.Text) = 0 Then
        Else
            If cb_satuan_9.Text = "Yard" Then
                qa_9 = Math.Round(Val(txt_qty_asal_9.Text.Replace(",", ".")) * 1.0936, 0)
                q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 1.0936, 0)
                h_9 = Math.Round(Val(harga_9.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_9.Text = "Meter" Then
                qa_9 = Math.Round(Val(txt_qty_asal_9.Text.Replace(",", ".")) * 0.9144, 0)
                q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 0.9144, 0)
                h_9 = Math.Round(Val(harga_9.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_9.Text = qa_9
            txt_meter_9.Text = q_9
            txt_harga_9.Text = h_9
        End If
    End Sub
    Private Sub cb_satuan_10_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_10.TextChanged
        Dim qty_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_10 As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_10, qa_10, h_10 As Double
        If txt_meter_10.Text = "" Then
        ElseIf Val(txt_meter_10.Text) = 0 Then
        Else
            If cb_satuan_10.Text = "Yard" Then
                qa_10 = Math.Round(Val(txt_qty_asal_10.Text.Replace(",", ".")) * 1.0936, 0)
                q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 1.0936, 0)
                h_10 = Math.Round(Val(harga_10.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_10.Text = "Meter" Then
                qa_10 = Math.Round(Val(txt_qty_asal_10.Text.Replace(",", ".")) * 0.9144, 0)
                q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 0.9144, 0)
                h_10 = Math.Round(Val(harga_10.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_10.Text = qa_10
            txt_meter_10.Text = q_10
            txt_harga_10.Text = h_10
        End If
    End Sub

    Private Sub isiidpopacking_1()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_1.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_2()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_2.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_3()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_3.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_4()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_4.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_5()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_5.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_6()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_6.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_7()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_7.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_8()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_8.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_9()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_9.Text = x.ToString
    End Sub
    Private Sub isiidpopacking_10()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Packing FROM tbpopacking WHERE Id_Po_Packing in(select max(Id_Po_Packing) FROM tbpopacking)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_packing_10.Text = x.ToString
    End Sub

    Private Sub txt_id_sj_proses_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_1.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_1.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_2.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_2.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_3.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_3.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_4.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_4.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_5.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_5.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_6.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_6.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_7.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_7.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_8.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_8.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_9.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_9.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_sj_proses_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_sj_proses_10.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Status FROM tbsuratjalanproses WHERE Id_SJ_Proses='" & txt_id_sj_proses_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_status_sj_proses_10.Text = drx.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub
End Class