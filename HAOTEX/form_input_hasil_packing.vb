﻿Imports MySql.Data.MySqlClient

Public Class form_input_hasil_packing

    Private Sub form_input_hasil_packing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ts_edit.Enabled = False
        ts_hitung.Enabled = False
        Panel3.Enabled = False
        Call isidtpawal()
        dtp_hari_ini.Text = Today
        dtp_awal.Text = Today
    End Sub
    Private Sub txt_sj_packing_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing.TextChanged
        If txt_gudang.Text = "" Then
            txt_sj_packing.Text = ""
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_hasil_packing"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = dtp_awal.Value
        tanggal = tanggal.AddDays(15)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        Dim tanggal As DateTime
        tanggal = dtp_awal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            CheckBox1.ForeColor = Color.Red
            CheckBox1.Text = "Tutup"
        Else
            CheckBox1.ForeColor = Color.Black
            CheckBox1.Text = "Belum Tutup"
        End If
    End Sub
    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        If Not e.KeyChar = Chr(13) Then e.Handled = True
    End Sub
    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim tanggal As DateTime
        tanggal = dtp_awal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub

    Private Sub A1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A1.GotFocus, B1.GotFocus, C1.GotFocus, D1.GotFocus, E1.GotFocus
        If M1.Text = "" Then
            M1.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A2.GotFocus, B2.GotFocus, C2.GotFocus, D2.GotFocus, E2.GotFocus
        If M2.Text = "" Then
            M2.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A3.GotFocus, B3.GotFocus, C3.GotFocus, D3.GotFocus, E3.GotFocus
        If M3.Text = "" Then
            M3.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A4.GotFocus, B4.GotFocus, C4.GotFocus, D4.GotFocus, E4.GotFocus
        If M4.Text = "" Then
            M4.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A5.GotFocus, B5.GotFocus, C5.GotFocus, D5.GotFocus, E5.GotFocus
        If M5.Text = "" Then
            M5.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A6.GotFocus, B6.GotFocus, C6.GotFocus, D6.GotFocus, E6.GotFocus
        If M6.Text = "" Then
            M6.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A7.GotFocus, B7.GotFocus, C7.GotFocus, D7.GotFocus, E7.GotFocus
        If M7.Text = "" Then
            M7.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A8.GotFocus, B8.GotFocus, C8.GotFocus, D8.GotFocus, E8.GotFocus
        If M8.Text = "" Then
            M8.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A9.GotFocus, B9.GotFocus, C9.GotFocus, D9.GotFocus, E9.GotFocus
        If M9.Text = "" Then
            M9.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A10.GotFocus, B10.GotFocus, C10.GotFocus, D10.GotFocus, E10.GotFocus
        If M10.Text = "" Then
            M10.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A11_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A11.GotFocus, B11.GotFocus, C11.GotFocus, D11.GotFocus, E11.GotFocus
        If M11.Text = "" Then
            M11.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A12_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A12.GotFocus, B12.GotFocus, C12.GotFocus, D12.GotFocus, E12.GotFocus
        If M12.Text = "" Then
            M12.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A13_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A13.GotFocus, B13.GotFocus, C13.GotFocus, D13.GotFocus, E13.GotFocus
        If M13.Text = "" Then
            M13.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A14_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A14.GotFocus, B14.GotFocus, C14.GotFocus, D14.GotFocus, E14.GotFocus
        If M14.Text = "" Then
            M14.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A15_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A15.GotFocus, B15.GotFocus, C15.GotFocus, D15.GotFocus, E15.GotFocus
        If M15.Text = "" Then
            M15.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A16_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A16.GotFocus, B16.GotFocus, C16.GotFocus, D16.GotFocus, E16.GotFocus
        If M16.Text = "" Then
            M16.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A17_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A17.GotFocus, B17.GotFocus, C17.GotFocus, D17.GotFocus, E17.GotFocus
        If M17.Text = "" Then
            M17.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A18_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A18.GotFocus, B18.GotFocus, C18.GotFocus, D18.GotFocus, E18.GotFocus
        If M18.Text = "" Then
            M18.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A19_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A19.GotFocus, B19.GotFocus, C19.GotFocus, D19.GotFocus, E19.GotFocus
        If M19.Text = "" Then
            M19.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A20_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A20.GotFocus, B20.GotFocus, C20.GotFocus, D20.GotFocus, E20.GotFocus
        If M20.Text = "" Then
            M20.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A21_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A21.GotFocus, B21.GotFocus, C21.GotFocus, D21.GotFocus, E21.GotFocus
        If M21.Text = "" Then
            M21.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A22_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A22.GotFocus, B22.GotFocus, C22.GotFocus, D22.GotFocus, E22.GotFocus
        If M22.Text = "" Then
            M22.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A23_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A23.GotFocus, B23.GotFocus, C23.GotFocus, D23.GotFocus, E23.GotFocus
        If M23.Text = "" Then
            M23.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A24_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A24.GotFocus, B24.GotFocus, C24.GotFocus, D24.GotFocus, E24.GotFocus
        If M24.Text = "" Then
            M24.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub
    Private Sub A25_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles A25.GotFocus, B25.GotFocus, C25.GotFocus, D25.GotFocus, E25.GotFocus
        If M25.Text = "" Then
            M25.Focus()
            MsgBox("METER Belum Diinput")
        End If
    End Sub

    Private Sub A1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A1.TextChanged, B1.TextChanged, C1.TextChanged, D1.TextChanged, E1.TextChanged
        Y1.Text = Val(A1.Text.Replace(",", ".")) + Val(B1.Text.Replace(",", ".")) + Val(C1.Text.Replace(",", ".")) + Val(D1.Text.Replace(",", ".")) + Val(E1.Text.Replace(",", "."))
        'If Not A1.Text = "" Or Not B1.Text = "" Or Not C1.Text = "" Or Not D1.Text = "" Or Not E1.Text = "" Then
        '    M2.ReadOnly = False
        '    K2.ReadOnly = False
        '    A2.ReadOnly = False
        '    B2.ReadOnly = False
        '    C2.ReadOnly = False
        '    D2.ReadOnly = False
        '    E2.ReadOnly = False
        'ElseIf A1.Text = "" And B1.Text = "" And C1.Text = "" And D1.Text = "" And E1.Text = "" Then
        '    K2.ReadOnly = True
        '    A2.ReadOnly = True
        '    B2.ReadOnly = True
        '    C2.ReadOnly = True
        '    D2.ReadOnly = True
        '    E2.ReadOnly = True
        '    M2.ReadOnly = True
        '    K2.Text = ""
        '    A2.Text = ""
        '    B2.Text = ""
        '    C2.Text = ""
        '    D2.Text = ""
        '    E2.Text = ""
        '    M2.Text = ""
        '    Y2.Text = ""
        '    M1.Text = ""
        '    Y1.Text = ""
        '    S1.Text = ""
        '    K1.Text = ""
        '    M1.Focus()
        'End If
    End Sub
    Private Sub A2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A2.TextChanged, B2.TextChanged, C2.TextChanged, D2.TextChanged, E2.TextChanged
        Y2.Text = Val(A2.Text.Replace(",", ".")) + Val(B2.Text.Replace(",", ".")) + Val(C2.Text.Replace(",", ".")) + Val(D2.Text.Replace(",", ".")) + Val(E2.Text.Replace(",", "."))
        'If Not A2.Text = "" Or Not B2.Text = "" Or Not C2.Text = "" Or Not D2.Text = "" Or Not E2.Text = "" Then
        '    M3.ReadOnly = False
        '    K3.ReadOnly = False
        '    A3.ReadOnly = False
        '    B3.ReadOnly = False
        '    C3.ReadOnly = False
        '    D3.ReadOnly = False
        '    E3.ReadOnly = False
        'ElseIf A2.Text = "" And B2.Text = "" And C2.Text = "" And D2.Text = "" And E2.Text = "" Then
        '    K3.ReadOnly = True
        '    A3.ReadOnly = True
        '    B3.ReadOnly = True
        '    C3.ReadOnly = True
        '    D3.ReadOnly = True
        '    E3.ReadOnly = True
        '    M3.ReadOnly = True
        '    K3.Text = ""
        '    A3.Text = ""
        '    B3.Text = ""
        '    C3.Text = ""
        '    D3.Text = ""
        '    E3.Text = ""
        '    M3.Text = ""
        '    Y3.Text = ""
        '    M2.Text = ""
        '    Y2.Text = ""
        '    S2.Text = ""
        '    K2.Text = ""
        '    M2.Focus()
        'End If
    End Sub
    Private Sub A3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A3.TextChanged, B3.TextChanged, C3.TextChanged, D3.TextChanged, E3.TextChanged
        Y3.Text = Val(A3.Text.Replace(",", ".")) + Val(B3.Text.Replace(",", ".")) + Val(C3.Text.Replace(",", ".")) + Val(D3.Text.Replace(",", ".")) + Val(E3.Text.Replace(",", "."))
        'If Not A3.Text = "" Or Not B3.Text = "" Or Not C3.Text = "" Or Not D3.Text = "" Or Not E3.Text = "" Then
        '    M4.ReadOnly = False
        '    K4.ReadOnly = False
        '    A4.ReadOnly = False
        '    B4.ReadOnly = False
        '    C4.ReadOnly = False
        '    D4.ReadOnly = False
        '    E4.ReadOnly = False
        'ElseIf A3.Text = "" And B3.Text = "" And C3.Text = "" And D3.Text = "" And E3.Text = "" Then
        '    K4.ReadOnly = True
        '    A4.ReadOnly = True
        '    B4.ReadOnly = True
        '    C4.ReadOnly = True
        '    D4.ReadOnly = True
        '    E4.ReadOnly = True
        '    M4.ReadOnly = True
        '    K4.Text = ""
        '    A4.Text = ""
        '    B4.Text = ""
        '    C4.Text = ""
        '    D4.Text = ""
        '    E4.Text = ""
        '    M4.Text = ""
        '    Y4.Text = ""
        '    M3.Text = ""
        '    Y3.Text = ""
        '    S3.Text = ""
        '    K3.Text = ""
        '    M3.Focus()
        'End If
    End Sub
    Private Sub A4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A4.TextChanged, B4.TextChanged, C4.TextChanged, D4.TextChanged, E4.TextChanged
        Y4.Text = Val(A4.Text.Replace(",", ".")) + Val(B4.Text.Replace(",", ".")) + Val(C4.Text.Replace(",", ".")) + Val(D4.Text.Replace(",", ".")) + Val(E4.Text.Replace(",", "."))
        'If Not A4.Text = "" Or Not B4.Text = "" Or Not C4.Text = "" Or Not D4.Text = "" Or Not E4.Text = "" Then
        '    M5.ReadOnly = False
        '    K5.ReadOnly = False
        '    A5.ReadOnly = False
        '    B5.ReadOnly = False
        '    C5.ReadOnly = False
        '    D5.ReadOnly = False
        '    E5.ReadOnly = False
        'ElseIf A4.Text = "" And B4.Text = "" And C4.Text = "" And D4.Text = "" And E4.Text = "" Then
        '    K5.ReadOnly = True
        '    A5.ReadOnly = True
        '    B5.ReadOnly = True
        '    C5.ReadOnly = True
        '    D5.ReadOnly = True
        '    E5.ReadOnly = True
        '    M5.ReadOnly = True
        '    K5.Text = ""
        '    A5.Text = ""
        '    B5.Text = ""
        '    C5.Text = ""
        '    D5.Text = ""
        '    E5.Text = ""
        '    M5.Text = ""
        '    Y5.Text = ""
        '    M4.Text = ""
        '    Y4.Text = ""
        '    S4.Text = ""
        '    K4.Text = ""
        '    M4.Focus()
        'End If
    End Sub
    Private Sub A5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A5.TextChanged, B5.TextChanged, C5.TextChanged, D5.TextChanged, E5.TextChanged
        Y5.Text = Val(A5.Text.Replace(",", ".")) + Val(B5.Text.Replace(",", ".")) + Val(C5.Text.Replace(",", ".")) + Val(D5.Text.Replace(",", ".")) + Val(E5.Text.Replace(",", "."))
        'If Not A5.Text = "" Or Not B5.Text = "" Or Not C5.Text = "" Or Not D5.Text = "" Or Not E5.Text = "" Then
        '    M6.ReadOnly = False
        '    K6.ReadOnly = False
        '    A6.ReadOnly = False
        '    B6.ReadOnly = False
        '    C6.ReadOnly = False
        '    D6.ReadOnly = False
        '    E6.ReadOnly = False
        'ElseIf A5.Text = "" And B5.Text = "" And C5.Text = "" And D5.Text = "" And E5.Text = "" Then
        '    K6.ReadOnly = True
        '    A6.ReadOnly = True
        '    B6.ReadOnly = True
        '    C6.ReadOnly = True
        '    D6.ReadOnly = True
        '    E6.ReadOnly = True
        '    M6.ReadOnly = True
        '    K6.Text = ""
        '    A6.Text = ""
        '    B6.Text = ""
        '    C6.Text = ""
        '    D6.Text = ""
        '    E6.Text = ""
        '    M6.Text = ""
        '    Y6.Text = ""
        '    M5.Text = ""
        '    Y5.Text = ""
        '    S5.Text = ""
        '    K5.Text = ""
        '    M5.Focus()
        'End If
    End Sub
    Private Sub A6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A6.TextChanged, B6.TextChanged, C6.TextChanged, D6.TextChanged, E6.TextChanged
        Y6.Text = Val(A6.Text.Replace(",", ".")) + Val(B6.Text.Replace(",", ".")) + Val(C6.Text.Replace(",", ".")) + Val(D6.Text.Replace(",", ".")) + Val(E6.Text.Replace(",", "."))
        'If Not A6.Text = "" Or Not B6.Text = "" Or Not C6.Text = "" Or Not D6.Text = "" Or Not E6.Text = "" Then
        '    M7.ReadOnly = False
        '    K7.ReadOnly = False
        '    A7.ReadOnly = False
        '    B7.ReadOnly = False
        '    C7.ReadOnly = False
        '    D7.ReadOnly = False
        '    E7.ReadOnly = False
        'ElseIf A6.Text = "" And B6.Text = "" And C6.Text = "" And D6.Text = "" And E6.Text = "" Then
        '    K7.ReadOnly = True
        '    A7.ReadOnly = True
        '    B7.ReadOnly = True
        '    C7.ReadOnly = True
        '    D7.ReadOnly = True
        '    E7.ReadOnly = True
        '    M7.ReadOnly = True
        '    K7.Text = ""
        '    A7.Text = ""
        '    B7.Text = ""
        '    C7.Text = ""
        '    D7.Text = ""
        '    E7.Text = ""
        '    M7.Text = ""
        '    Y7.Text = ""
        '    M6.Text = ""
        '    Y6.Text = ""
        '    S6.Text = ""
        '    K6.Text = ""
        '    M6.Focus()
        'End If
    End Sub
    Private Sub A7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A7.TextChanged, B7.TextChanged, C7.TextChanged, D7.TextChanged, E7.TextChanged
        Y7.Text = Val(A7.Text.Replace(",", ".")) + Val(B7.Text.Replace(",", ".")) + Val(C7.Text.Replace(",", ".")) + Val(D7.Text.Replace(",", ".")) + Val(E7.Text.Replace(",", "."))
        'If Not A7.Text = "" Or Not B7.Text = "" Or Not C7.Text = "" Or Not D7.Text = "" Or Not E7.Text = "" Then
        '    M8.ReadOnly = False
        '    K8.ReadOnly = False
        '    A8.ReadOnly = False
        '    B8.ReadOnly = False
        '    C8.ReadOnly = False
        '    D8.ReadOnly = False
        '    E8.ReadOnly = False
        'ElseIf A7.Text = "" And B7.Text = "" And C7.Text = "" And D7.Text = "" And E7.Text = "" Then
        '    K8.ReadOnly = True
        '    A8.ReadOnly = True
        '    B8.ReadOnly = True
        '    C8.ReadOnly = True
        '    D8.ReadOnly = True
        '    E8.ReadOnly = True
        '    M8.ReadOnly = True
        '    K8.Text = ""
        '    A8.Text = ""
        '    B8.Text = ""
        '    C8.Text = ""
        '    D8.Text = ""
        '    E8.Text = ""
        '    M8.Text = ""
        '    Y8.Text = ""
        '    M7.Text = ""
        '    Y7.Text = ""
        '    S7.Text = ""
        '    K7.Text = ""
        '    M7.Focus()
        'End If
    End Sub
    Private Sub A8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A8.TextChanged, B8.TextChanged, C8.TextChanged, D8.TextChanged, E8.TextChanged
        Y8.Text = Val(A8.Text.Replace(",", ".")) + Val(B8.Text.Replace(",", ".")) + Val(C8.Text.Replace(",", ".")) + Val(D8.Text.Replace(",", ".")) + Val(E8.Text.Replace(",", "."))
        'If Not A8.Text = "" Or Not B8.Text = "" Or Not C8.Text = "" Or Not D8.Text = "" Or Not E8.Text = "" Then
        '    M9.ReadOnly = False
        '    K9.ReadOnly = False
        '    A9.ReadOnly = False
        '    B9.ReadOnly = False
        '    C9.ReadOnly = False
        '    D9.ReadOnly = False
        '    E9.ReadOnly = False
        'ElseIf A8.Text = "" And B8.Text = "" And C8.Text = "" And D8.Text = "" And E8.Text = "" Then
        '    K9.ReadOnly = True
        '    A9.ReadOnly = True
        '    B9.ReadOnly = True
        '    C9.ReadOnly = True
        '    D9.ReadOnly = True
        '    E9.ReadOnly = True
        '    M9.ReadOnly = True
        '    K9.Text = ""
        '    A9.Text = ""
        '    B9.Text = ""
        '    C9.Text = ""
        '    D9.Text = ""
        '    E9.Text = ""
        '    M9.Text = ""
        '    Y9.Text = ""
        '    M8.Text = ""
        '    Y8.Text = ""
        '    S8.Text = ""
        '    K8.Text = ""
        '    M8.Focus()
        'End If
    End Sub
    Private Sub A9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A9.TextChanged, B9.TextChanged, C9.TextChanged, D9.TextChanged, E9.TextChanged
        Y9.Text = Val(A9.Text.Replace(",", ".")) + Val(B9.Text.Replace(",", ".")) + Val(C9.Text.Replace(",", ".")) + Val(D9.Text.Replace(",", ".")) + Val(E9.Text.Replace(",", "."))
        'If Not A9.Text = "" Or Not B9.Text = "" Or Not C9.Text = "" Or Not D9.Text = "" Or Not E9.Text = "" Then
        '    M10.ReadOnly = False
        '    K10.ReadOnly = False
        '    A10.ReadOnly = False
        '    B10.ReadOnly = False
        '    C10.ReadOnly = False
        '    D10.ReadOnly = False
        '    E10.ReadOnly = False
        'ElseIf A9.Text = "" And B9.Text = "" And C9.Text = "" And D9.Text = "" And E9.Text = "" Then
        '    K10.ReadOnly = True
        '    A10.ReadOnly = True
        '    B10.ReadOnly = True
        '    C10.ReadOnly = True
        '    D10.ReadOnly = True
        '    E10.ReadOnly = True
        '    M10.ReadOnly = True
        '    K10.Text = ""
        '    A10.Text = ""
        '    B10.Text = ""
        '    C10.Text = ""
        '    D10.Text = ""
        '    E10.Text = ""
        '    M10.Text = ""
        '    Y10.Text = ""
        '    M9.Text = ""
        '    Y9.Text = ""
        '    S9.Text = ""
        '    K9.Text = ""
        '    M9.Focus()
        'End If
    End Sub
    Private Sub A10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A10.TextChanged, B10.TextChanged, C10.TextChanged, D10.TextChanged, E10.TextChanged
        Y10.Text = Val(A10.Text.Replace(",", ".")) + Val(B10.Text.Replace(",", ".")) + Val(C10.Text.Replace(",", ".")) + Val(D10.Text.Replace(",", ".")) + Val(E10.Text.Replace(",", "."))
        'If Not A10.Text = "" Or Not B10.Text = "" Or Not C10.Text = "" Or Not D10.Text = "" Or Not E10.Text = "" Then
        '    M11.ReadOnly = False
        '    K11.ReadOnly = False
        '    A11.ReadOnly = False
        '    B11.ReadOnly = False
        '    C11.ReadOnly = False
        '    D11.ReadOnly = False
        '    E11.ReadOnly = False
        'ElseIf A10.Text = "" And B10.Text = "" And C10.Text = "" And D10.Text = "" And E10.Text = "" Then
        '    K11.ReadOnly = True
        '    A11.ReadOnly = True
        '    B11.ReadOnly = True
        '    C11.ReadOnly = True
        '    D11.ReadOnly = True
        '    E11.ReadOnly = True
        '    M11.ReadOnly = True
        '    K11.Text = ""
        '    A11.Text = ""
        '    B11.Text = ""
        '    C11.Text = ""
        '    D11.Text = ""
        '    E11.Text = ""
        '    M11.Text = ""
        '    Y11.Text = ""
        '    M10.Text = ""
        '    Y10.Text = ""
        '    S10.Text = ""
        '    K10.Text = ""
        '    M10.Focus()
        'End If
    End Sub
    Private Sub A11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A11.TextChanged, B11.TextChanged, C11.TextChanged, D11.TextChanged, E11.TextChanged
        Y11.Text = Val(A11.Text.Replace(",", ".")) + Val(B11.Text.Replace(",", ".")) + Val(C11.Text.Replace(",", ".")) + Val(D11.Text.Replace(",", ".")) + Val(E11.Text.Replace(",", "."))
        'If Not A11.Text = "" Or Not B11.Text = "" Or Not C11.Text = "" Or Not D11.Text = "" Or Not E11.Text = "" Then
        '    M12.ReadOnly = False
        '    K12.ReadOnly = False
        '    A12.ReadOnly = False
        '    B12.ReadOnly = False
        '    C12.ReadOnly = False
        '    D12.ReadOnly = False
        '    E12.ReadOnly = False
        'ElseIf A11.Text = "" And B11.Text = "" And C11.Text = "" And D11.Text = "" And E11.Text = "" Then
        '    K12.ReadOnly = True
        '    A12.ReadOnly = True
        '    B12.ReadOnly = True
        '    C12.ReadOnly = True
        '    D12.ReadOnly = True
        '    E12.ReadOnly = True
        '    M12.ReadOnly = True
        '    K12.Text = ""
        '    A12.Text = ""
        '    B12.Text = ""
        '    C12.Text = ""
        '    D12.Text = ""
        '    E12.Text = ""
        '    M12.Text = ""
        '    Y12.Text = ""
        '    M11.Text = ""
        '    Y11.Text = ""
        '    S11.Text = ""
        '    K11.Text = ""
        '    M11.Focus()
        'End If
    End Sub
    Private Sub A12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A12.TextChanged, B12.TextChanged, C12.TextChanged, D12.TextChanged, E12.TextChanged
        Y12.Text = Val(A12.Text.Replace(",", ".")) + Val(B12.Text.Replace(",", ".")) + Val(C12.Text.Replace(",", ".")) + Val(D12.Text.Replace(",", ".")) + Val(E12.Text.Replace(",", "."))
        'If Not A12.Text = "" Or Not B12.Text = "" Or Not C12.Text = "" Or Not D12.Text = "" Or Not E12.Text = "" Then
        '    M13.ReadOnly = False
        '    K13.ReadOnly = False
        '    A13.ReadOnly = False
        '    B13.ReadOnly = False
        '    C13.ReadOnly = False
        '    D13.ReadOnly = False
        '    E13.ReadOnly = False
        'ElseIf A12.Text = "" And B12.Text = "" And C12.Text = "" And D12.Text = "" And E12.Text = "" Then
        '    K13.ReadOnly = True
        '    A13.ReadOnly = True
        '    B13.ReadOnly = True
        '    C13.ReadOnly = True
        '    D13.ReadOnly = True
        '    E13.ReadOnly = True
        '    M13.ReadOnly = True
        '    K13.Text = ""
        '    A13.Text = ""
        '    B13.Text = ""
        '    C13.Text = ""
        '    D13.Text = ""
        '    E13.Text = ""
        '    M13.Text = ""
        '    Y13.Text = ""
        '    M12.Text = ""
        '    Y12.Text = ""
        '    S12.Text = ""
        '    K12.Text = ""
        '    M12.Focus()
        'End If
    End Sub
    Private Sub A13_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A13.TextChanged, B13.TextChanged, C13.TextChanged, D13.TextChanged, E13.TextChanged
        Y13.Text = Val(A13.Text.Replace(",", ".")) + Val(B13.Text.Replace(",", ".")) + Val(C13.Text.Replace(",", ".")) + Val(D13.Text.Replace(",", ".")) + Val(E13.Text.Replace(",", "."))
        'If Not A13.Text = "" Or Not B13.Text = "" Or Not C13.Text = "" Or Not D13.Text = "" Or Not E13.Text = "" Then
        '    M14.ReadOnly = False
        '    K14.ReadOnly = False
        '    A14.ReadOnly = False
        '    B14.ReadOnly = False
        '    C14.ReadOnly = False
        '    D14.ReadOnly = False
        '    E14.ReadOnly = False
        'ElseIf A13.Text = "" And B13.Text = "" And C13.Text = "" And D13.Text = "" And E13.Text = "" Then
        '    K14.ReadOnly = True
        '    A14.ReadOnly = True
        '    B14.ReadOnly = True
        '    C14.ReadOnly = True
        '    D14.ReadOnly = True
        '    E14.ReadOnly = True
        '    M14.ReadOnly = True
        '    K14.Text = ""
        '    A14.Text = ""
        '    B14.Text = ""
        '    C14.Text = ""
        '    D14.Text = ""
        '    E14.Text = ""
        '    M14.Text = ""
        '    Y14.Text = ""
        '    M13.Text = ""
        '    Y13.Text = ""
        '    S13.Text = ""
        '    K13.Text = ""
        '    M13.Focus()
        'End If
    End Sub
    Private Sub A14_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A14.TextChanged, B14.TextChanged, C14.TextChanged, D14.TextChanged, E14.TextChanged
        Y14.Text = Val(A14.Text.Replace(",", ".")) + Val(B14.Text.Replace(",", ".")) + Val(C14.Text.Replace(",", ".")) + Val(D14.Text.Replace(",", ".")) + Val(E14.Text.Replace(",", "."))
        'If Not A14.Text = "" Or Not B14.Text = "" Or Not C14.Text = "" Or Not D14.Text = "" Or Not E14.Text = "" Then
        '    M15.ReadOnly = False
        '    K15.ReadOnly = False
        '    A15.ReadOnly = False
        '    B15.ReadOnly = False
        '    C15.ReadOnly = False
        '    D15.ReadOnly = False
        '    E15.ReadOnly = False
        'ElseIf A14.Text = "" And B14.Text = "" And C14.Text = "" And D14.Text = "" And E14.Text = "" Then
        '    K15.ReadOnly = True
        '    A15.ReadOnly = True
        '    B15.ReadOnly = True
        '    C15.ReadOnly = True
        '    D15.ReadOnly = True
        '    E15.ReadOnly = True
        '    M15.ReadOnly = True
        '    K15.Text = ""
        '    A15.Text = ""
        '    B15.Text = ""
        '    C15.Text = ""
        '    D15.Text = ""
        '    E15.Text = ""
        '    M15.Text = ""
        '    Y15.Text = ""
        '    M14.Text = ""
        '    Y14.Text = ""
        '    S14.Text = ""
        '    K14.Text = ""
        '    M14.Focus()
        'End If
    End Sub
    Private Sub A15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A15.TextChanged, B15.TextChanged, C15.TextChanged, D15.TextChanged, E15.TextChanged
        Y15.Text = Val(A15.Text.Replace(",", ".")) + Val(B15.Text.Replace(",", ".")) + Val(C15.Text.Replace(",", ".")) + Val(D15.Text.Replace(",", ".")) + Val(E15.Text.Replace(",", "."))
        'If Not A15.Text = "" Or Not B15.Text = "" Or Not C15.Text = "" Or Not D15.Text = "" Or Not E15.Text = "" Then
        '    M16.ReadOnly = False
        '    K16.ReadOnly = False
        '    A16.ReadOnly = False
        '    B16.ReadOnly = False
        '    C16.ReadOnly = False
        '    D16.ReadOnly = False
        '    E16.ReadOnly = False
        'ElseIf A15.Text = "" And B15.Text = "" And C15.Text = "" And D15.Text = "" And E15.Text = "" Then
        '    K16.ReadOnly = True
        '    A16.ReadOnly = True
        '    B16.ReadOnly = True
        '    C16.ReadOnly = True
        '    D16.ReadOnly = True
        '    E16.ReadOnly = True
        '    M16.ReadOnly = True
        '    K16.Text = ""
        '    A16.Text = ""
        '    B16.Text = ""
        '    C16.Text = ""
        '    D16.Text = ""
        '    E16.Text = ""
        '    M16.Text = ""
        '    Y16.Text = ""
        '    M15.Text = ""
        '    Y15.Text = ""
        '    S15.Text = ""
        '    K15.Text = ""
        '    M15.Focus()
        'End If
    End Sub
    Private Sub A16_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A16.TextChanged, B16.TextChanged, C16.TextChanged, D16.TextChanged, E16.TextChanged
        Y16.Text = Val(A16.Text.Replace(",", ".")) + Val(B16.Text.Replace(",", ".")) + Val(C16.Text.Replace(",", ".")) + Val(D16.Text.Replace(",", ".")) + Val(E16.Text.Replace(",", "."))
        'If Not A16.Text = "" Or Not B16.Text = "" Or Not C16.Text = "" Or Not D16.Text = "" Or Not E16.Text = "" Then
        '    M17.ReadOnly = False
        '    K17.ReadOnly = False
        '    A17.ReadOnly = False
        '    B17.ReadOnly = False
        '    C17.ReadOnly = False
        '    D17.ReadOnly = False
        '    E17.ReadOnly = False
        'ElseIf A16.Text = "" And B16.Text = "" And C16.Text = "" And D16.Text = "" And E16.Text = "" Then
        '    K17.ReadOnly = True
        '    A17.ReadOnly = True
        '    B17.ReadOnly = True
        '    C17.ReadOnly = True
        '    D17.ReadOnly = True
        '    E17.ReadOnly = True
        '    M17.ReadOnly = True
        '    K17.Text = ""
        '    A17.Text = ""
        '    B17.Text = ""
        '    C17.Text = ""
        '    D17.Text = ""
        '    E17.Text = ""
        '    M17.Text = ""
        '    Y17.Text = ""
        '    M16.Text = ""
        '    Y16.Text = ""
        '    S16.Text = ""
        '    K16.Text = ""
        '    M16.Focus()
        'End If
    End Sub
    Private Sub A17_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A17.TextChanged, B17.TextChanged, C17.TextChanged, D17.TextChanged, E17.TextChanged
        Y17.Text = Val(A17.Text.Replace(",", ".")) + Val(B17.Text.Replace(",", ".")) + Val(C17.Text.Replace(",", ".")) + Val(D17.Text.Replace(",", ".")) + Val(E17.Text.Replace(",", "."))
        'If Not A17.Text = "" Or Not B17.Text = "" Or Not C17.Text = "" Or Not D17.Text = "" Or Not E17.Text = "" Then
        '    M18.ReadOnly = False
        '    K18.ReadOnly = False
        '    A18.ReadOnly = False
        '    B18.ReadOnly = False
        '    C18.ReadOnly = False
        '    D18.ReadOnly = False
        '    E18.ReadOnly = False
        'ElseIf A17.Text = "" And B17.Text = "" And C17.Text = "" And D17.Text = "" And E17.Text = "" Then
        '    K18.ReadOnly = True
        '    A18.ReadOnly = True
        '    B18.ReadOnly = True
        '    C18.ReadOnly = True
        '    D18.ReadOnly = True
        '    E18.ReadOnly = True
        '    M18.ReadOnly = True
        '    K18.Text = ""
        '    A18.Text = ""
        '    B18.Text = ""
        '    C18.Text = ""
        '    D18.Text = ""
        '    E18.Text = ""
        '    M18.Text = ""
        '    Y18.Text = ""
        '    M17.Text = ""
        '    Y17.Text = ""
        '    S17.Text = ""
        '    K17.Text = ""
        '    M17.Focus()
        'End If
    End Sub
    Private Sub A18_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A18.TextChanged, B18.TextChanged, C18.TextChanged, D18.TextChanged, E18.TextChanged
        Y18.Text = Val(A18.Text.Replace(",", ".")) + Val(B18.Text.Replace(",", ".")) + Val(C18.Text.Replace(",", ".")) + Val(D18.Text.Replace(",", ".")) + Val(E18.Text.Replace(",", "."))
        'If Not A18.Text = "" Or Not B18.Text = "" Or Not C18.Text = "" Or Not D18.Text = "" Or Not E18.Text = "" Then
        '    M19.ReadOnly = False
        '    K19.ReadOnly = False
        '    A19.ReadOnly = False
        '    B19.ReadOnly = False
        '    C19.ReadOnly = False
        '    D19.ReadOnly = False
        '    E19.ReadOnly = False
        'ElseIf A18.Text = "" And B18.Text = "" And C18.Text = "" And D18.Text = "" And E18.Text = "" Then
        '    K19.ReadOnly = True
        '    A19.ReadOnly = True
        '    B19.ReadOnly = True
        '    C19.ReadOnly = True
        '    D19.ReadOnly = True
        '    E19.ReadOnly = True
        '    M19.ReadOnly = True
        '    K19.Text = ""
        '    A19.Text = ""
        '    B19.Text = ""
        '    C19.Text = ""
        '    D19.Text = ""
        '    E19.Text = ""
        '    M19.Text = ""
        '    Y19.Text = ""
        '    M18.Text = ""
        '    Y18.Text = ""
        '    S18.Text = ""
        '    K18.Text = ""
        '    M18.Focus()
        'End If
    End Sub
    Private Sub A19_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A19.TextChanged, B19.TextChanged, C19.TextChanged, D19.TextChanged, E19.TextChanged
        Y19.Text = Val(A19.Text.Replace(",", ".")) + Val(B19.Text.Replace(",", ".")) + Val(C19.Text.Replace(",", ".")) + Val(D19.Text.Replace(",", ".")) + Val(E19.Text.Replace(",", "."))
        'If Not A19.Text = "" Or Not B19.Text = "" Or Not C19.Text = "" Or Not D19.Text = "" Or Not E19.Text = "" Then
        '    M20.ReadOnly = False
        '    K20.ReadOnly = False
        '    A20.ReadOnly = False
        '    B20.ReadOnly = False
        '    C20.ReadOnly = False
        '    D20.ReadOnly = False
        '    E20.ReadOnly = False
        'ElseIf A19.Text = "" And B19.Text = "" And C19.Text = "" And D19.Text = "" And E19.Text = "" Then
        '    K20.ReadOnly = True
        '    A20.ReadOnly = True
        '    B20.ReadOnly = True
        '    C20.ReadOnly = True
        '    D20.ReadOnly = True
        '    E20.ReadOnly = True
        '    M20.ReadOnly = True
        '    K20.Text = ""
        '    A20.Text = ""
        '    B20.Text = ""
        '    C20.Text = ""
        '    D20.Text = ""
        '    E20.Text = ""
        '    M20.Text = ""
        '    Y20.Text = ""
        '    M19.Text = ""
        '    Y19.Text = ""
        '    S19.Text = ""
        '    K19.Text = ""
        '    M19.Focus()
        'End If
    End Sub
    Private Sub A20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A20.TextChanged, B20.TextChanged, C20.TextChanged, D20.TextChanged, E20.TextChanged
        Y20.Text = Val(A20.Text.Replace(",", ".")) + Val(B20.Text.Replace(",", ".")) + Val(C20.Text.Replace(",", ".")) + Val(D20.Text.Replace(",", ".")) + Val(E20.Text.Replace(",", "."))
        'If Not A20.Text = "" Or Not B20.Text = "" Or Not C20.Text = "" Or Not D20.Text = "" Or Not E20.Text = "" Then
        '    M21.ReadOnly = False
        '    K21.ReadOnly = False
        '    A21.ReadOnly = False
        '    B21.ReadOnly = False
        '    C21.ReadOnly = False
        '    D21.ReadOnly = False
        '    E21.ReadOnly = False
        'ElseIf A20.Text = "" And B20.Text = "" And C20.Text = "" And D20.Text = "" And E20.Text = "" Then
        '    K21.ReadOnly = True
        '    A21.ReadOnly = True
        '    B21.ReadOnly = True
        '    C21.ReadOnly = True
        '    D21.ReadOnly = True
        '    E21.ReadOnly = True
        '    M21.ReadOnly = True
        '    K21.Text = ""
        '    A21.Text = ""
        '    B21.Text = ""
        '    C21.Text = ""
        '    D21.Text = ""
        '    E21.Text = ""
        '    M21.Text = ""
        '    Y21.Text = ""
        '    M20.Text = ""
        '    Y20.Text = ""
        '    S20.Text = ""
        '    K20.Text = ""
        '    M20.Focus()
        'End If
    End Sub
    Private Sub A21_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A21.TextChanged, B21.TextChanged, C21.TextChanged, D21.TextChanged, E21.TextChanged
        Y21.Text = Val(A21.Text.Replace(",", ".")) + Val(B21.Text.Replace(",", ".")) + Val(C21.Text.Replace(",", ".")) + Val(D21.Text.Replace(",", ".")) + Val(E21.Text.Replace(",", "."))
        'If Not A21.Text = "" Or Not B21.Text = "" Or Not C21.Text = "" Or Not D21.Text = "" Or Not E21.Text = "" Then
        '    M22.ReadOnly = False
        '    K22.ReadOnly = False
        '    A22.ReadOnly = False
        '    B22.ReadOnly = False
        '    C22.ReadOnly = False
        '    D22.ReadOnly = False
        '    E22.ReadOnly = False
        'ElseIf A21.Text = "" And B21.Text = "" And C21.Text = "" And D21.Text = "" And E21.Text = "" Then
        '    K22.ReadOnly = True
        '    A22.ReadOnly = True
        '    B22.ReadOnly = True
        '    C22.ReadOnly = True
        '    D22.ReadOnly = True
        '    E22.ReadOnly = True
        '    M22.ReadOnly = True
        '    K22.Text = ""
        '    A22.Text = ""
        '    B22.Text = ""
        '    C22.Text = ""
        '    D22.Text = ""
        '    E22.Text = ""
        '    M22.Text = ""
        '    Y22.Text = ""
        '    M21.Text = ""
        '    Y21.Text = ""
        '    S21.Text = ""
        '    K21.Text = ""
        '    M21.Focus()
        'End If
    End Sub
    Private Sub A22_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A22.TextChanged, B22.TextChanged, C22.TextChanged, D22.TextChanged, E22.TextChanged
        Y22.Text = Val(A22.Text.Replace(",", ".")) + Val(B22.Text.Replace(",", ".")) + Val(C22.Text.Replace(",", ".")) + Val(D22.Text.Replace(",", ".")) + Val(E22.Text.Replace(",", "."))
        'If Not A22.Text = "" Or Not B22.Text = "" Or Not C22.Text = "" Or Not D22.Text = "" Or Not E22.Text = "" Then
        '    M23.ReadOnly = False
        '    K23.ReadOnly = False
        '    A23.ReadOnly = False
        '    B23.ReadOnly = False
        '    C23.ReadOnly = False
        '    D23.ReadOnly = False
        '    E23.ReadOnly = False
        'ElseIf A22.Text = "" And B22.Text = "" And C22.Text = "" And D22.Text = "" And E22.Text = "" Then
        '    K23.ReadOnly = True
        '    A23.ReadOnly = True
        '    B23.ReadOnly = True
        '    C23.ReadOnly = True
        '    D23.ReadOnly = True
        '    E23.ReadOnly = True
        '    M23.ReadOnly = True
        '    K23.Text = ""
        '    A23.Text = ""
        '    B23.Text = ""
        '    C23.Text = ""
        '    D23.Text = ""
        '    E23.Text = ""
        '    M23.Text = ""
        '    Y23.Text = ""
        '    M22.Text = ""
        '    Y22.Text = ""
        '    S22.Text = ""
        '    K22.Text = ""
        '    M22.Focus()
        'End If
    End Sub
    Private Sub A23_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A23.TextChanged, B23.TextChanged, C23.TextChanged, D23.TextChanged, E23.TextChanged
        Y23.Text = Val(A23.Text.Replace(",", ".")) + Val(B23.Text.Replace(",", ".")) + Val(C23.Text.Replace(",", ".")) + Val(D23.Text.Replace(",", ".")) + Val(E23.Text.Replace(",", "."))
        'If Not A23.Text = "" Or Not B23.Text = "" Or Not C23.Text = "" Or Not D23.Text = "" Or Not E23.Text = "" Then
        '    M24.ReadOnly = False
        '    K24.ReadOnly = False
        '    A24.ReadOnly = False
        '    B24.ReadOnly = False
        '    C24.ReadOnly = False
        '    D24.ReadOnly = False
        '    E24.ReadOnly = False
        'ElseIf A23.Text = "" And B23.Text = "" And C23.Text = "" And D23.Text = "" And E23.Text = "" Then
        '    K24.ReadOnly = True
        '    A24.ReadOnly = True
        '    B24.ReadOnly = True
        '    C24.ReadOnly = True
        '    D24.ReadOnly = True
        '    E24.ReadOnly = True
        '    M24.ReadOnly = True
        '    K24.Text = ""
        '    A24.Text = ""
        '    B24.Text = ""
        '    C24.Text = ""
        '    D24.Text = ""
        '    E24.Text = ""
        '    M24.Text = ""
        '    Y24.Text = ""
        '    M23.Text = ""
        '    Y23.Text = ""
        '    S23.Text = ""
        '    K23.Text = ""
        '    M23.Focus()
        'End If
    End Sub
    Private Sub A24_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A24.TextChanged, B24.TextChanged, C24.TextChanged, D24.TextChanged, E24.TextChanged
        Y24.Text = Val(A24.Text.Replace(",", ".")) + Val(B24.Text.Replace(",", ".")) + Val(C24.Text.Replace(",", ".")) + Val(D24.Text.Replace(",", ".")) + Val(E24.Text.Replace(",", "."))
        'If Not A24.Text = "" Or Not B24.Text = "" Or Not C24.Text = "" Or Not D24.Text = "" Or Not E24.Text = "" Then
        '    M25.ReadOnly = False
        '    K25.ReadOnly = False
        '    A25.ReadOnly = False
        '    B25.ReadOnly = False
        '    C25.ReadOnly = False
        '    D25.ReadOnly = False
        '    E25.ReadOnly = False
        'ElseIf A24.Text = "" And B24.Text = "" And C24.Text = "" And D24.Text = "" And E24.Text = "" Then
        '    K25.ReadOnly = True
        '    A25.ReadOnly = True
        '    B25.ReadOnly = True
        '    C25.ReadOnly = True
        '    D25.ReadOnly = True
        '    E25.ReadOnly = True
        '    M25.ReadOnly = True
        '    K25.Text = ""
        '    A25.Text = ""
        '    B25.Text = ""
        '    C25.Text = ""
        '    D25.Text = ""
        '    E25.Text = ""
        '    M25.Text = ""
        '    Y25.Text = ""
        '    M24.Text = ""
        '    Y24.Text = ""
        '    S24.Text = ""
        '    K24.Text = ""
        '    M24.Focus()
        'End If
    End Sub
    Private Sub A25_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles A25.TextChanged, B25.TextChanged, C25.TextChanged, D25.TextChanged, E25.TextChanged
        Y25.Text = Val(A25.Text.Replace(",", ".")) + Val(B25.Text.Replace(",", ".")) + Val(C25.Text.Replace(",", ".")) + Val(D25.Text.Replace(",", ".")) + Val(E25.Text.Replace(",", "."))
    End Sub

    Private Sub A1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A1.KeyPress
        If A1.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B1.Focus()
        End If
    End Sub
    Private Sub A2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A2.KeyPress
        If A2.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B2.Focus()
        End If
    End Sub
    Private Sub A3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A3.KeyPress
        If A3.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B3.Focus()
        End If
    End Sub
    Private Sub A4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A4.KeyPress
        If A4.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B4.Focus()
        End If
    End Sub
    Private Sub A5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A5.KeyPress
        If A5.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B5.Focus()
        End If
    End Sub
    Private Sub A6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A6.KeyPress
        If A6.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B6.Focus()
        End If
    End Sub
    Private Sub A7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A7.KeyPress
        If A7.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B7.Focus()
        End If
    End Sub
    Private Sub A8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A8.KeyPress
        If A8.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B8.Focus()
        End If
    End Sub
    Private Sub A9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A9.KeyPress
        If A9.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B9.Focus()
        End If
    End Sub
    Private Sub A10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A10.KeyPress
        If A10.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B10.Focus()
        End If
    End Sub
    Private Sub A11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A11.KeyPress
        If A11.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A11.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B11.Focus()
        End If
    End Sub
    Private Sub A12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A12.KeyPress
        If A12.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A12.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B12.Focus()
        End If
    End Sub
    Private Sub A13_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A13.KeyPress
        If A13.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A13.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B13.Focus()
        End If
    End Sub
    Private Sub A14_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A14.KeyPress
        If A14.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A14.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B14.Focus()
        End If
    End Sub
    Private Sub A15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A15.KeyPress
        If A15.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A15.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B15.Focus()
        End If
    End Sub
    Private Sub A16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A16.KeyPress
        If A16.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A16.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B16.Focus()
        End If
    End Sub
    Private Sub A17_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A17.KeyPress
        If A17.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A17.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B17.Focus()
        End If
    End Sub
    Private Sub A18_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A18.KeyPress
        If A18.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A18.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B1.Focus()
        End If
        If e.KeyChar = Chr(13) Then
            B18.Focus()
        End If
    End Sub
    Private Sub A19_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A19.KeyPress
        If A19.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A19.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B19.Focus()
        End If
    End Sub
    Private Sub A20_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A20.KeyPress
        If A20.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A20.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B20.Focus()
        End If
    End Sub
    Private Sub A21_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A21.KeyPress
        If A21.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A21.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B21.Focus()
        End If
    End Sub
    Private Sub A22_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A22.KeyPress
        If A22.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A22.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B22.Focus()
        End If
    End Sub
    Private Sub A23_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A23.KeyPress
        If A23.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A23.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B23.Focus()
        End If
    End Sub
    Private Sub A24_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A24.KeyPress
        If A24.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A24.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B24.Focus()
        End If
    End Sub
    Private Sub A25_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles A25.KeyPress
        If A25.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf A25.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            B25.Focus()
        End If
    End Sub

    Private Sub B1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B1.KeyPress
        If B1.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C1.Focus()
        End If
    End Sub
    Private Sub B2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B2.KeyPress
        If B2.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C2.Focus()
        End If
    End Sub
    Private Sub B3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B3.KeyPress
        If B3.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C3.Focus()
        End If
    End Sub
    Private Sub B4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B4.KeyPress
        If B4.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C4.Focus()
        End If
    End Sub
    Private Sub B5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B5.KeyPress
        If B5.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C5.Focus()
        End If
    End Sub
    Private Sub B6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B6.KeyPress
        If B6.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C6.Focus()
        End If
    End Sub
    Private Sub B7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B7.KeyPress
        If B7.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C7.Focus()
        End If
    End Sub
    Private Sub B8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B8.KeyPress
        If B8.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C8.Focus()
        End If
    End Sub
    Private Sub B9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B9.KeyPress
        If B9.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C9.Focus()
        End If
    End Sub
    Private Sub B10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B10.KeyPress
        If B10.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C10.Focus()
        End If
    End Sub
    Private Sub B11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B11.KeyPress
        If B11.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B11.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C11.Focus()
        End If
    End Sub
    Private Sub B12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B12.KeyPress
        If B12.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B12.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C12.Focus()
        End If
    End Sub
    Private Sub B13_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B13.KeyPress
        If B13.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B13.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C13.Focus()
        End If
    End Sub
    Private Sub B14_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B14.KeyPress
        If B14.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B14.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C14.Focus()
        End If
    End Sub
    Private Sub B15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B15.KeyPress
        If B15.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B15.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C15.Focus()
        End If
    End Sub
    Private Sub B16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B16.KeyPress
        If B16.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B16.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C16.Focus()
        End If
    End Sub
    Private Sub B17_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B17.KeyPress
        If B17.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B17.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C17.Focus()
        End If
    End Sub
    Private Sub B18_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B18.KeyPress
        If B18.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B18.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C18.Focus()
        End If
    End Sub
    Private Sub B19_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B19.KeyPress
        If B19.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B19.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C19.Focus()
        End If
    End Sub
    Private Sub B20_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B20.KeyPress
        If B20.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B20.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C20.Focus()
        End If
    End Sub
    Private Sub B21_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B21.KeyPress
        If B21.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B21.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C21.Focus()
        End If
    End Sub
    Private Sub B22_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B22.KeyPress
        If B22.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B22.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C22.Focus()
        End If
    End Sub
    Private Sub B23_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B23.KeyPress
        If B23.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B23.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C23.Focus()
        End If
    End Sub
    Private Sub B24_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B24.KeyPress
        If B24.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B24.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C24.Focus()
        End If
    End Sub
    Private Sub B25_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B25.KeyPress
        If B25.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf B25.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            C25.Focus()
        End If
    End Sub

    Private Sub C1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C1.KeyPress
        If C1.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D1.Focus()
        End If
    End Sub
    Private Sub C2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C2.KeyPress
        If C2.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D2.Focus()
        End If
    End Sub
    Private Sub C3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C3.KeyPress
        If C3.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D3.Focus()
        End If
    End Sub
    Private Sub C4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C4.KeyPress
        If C4.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D4.Focus()
        End If
    End Sub
    Private Sub C5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C5.KeyPress
        If C5.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D5.Focus()
        End If
    End Sub
    Private Sub C6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C6.KeyPress
        If C6.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D6.Focus()
        End If
    End Sub
    Private Sub C7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C7.KeyPress
        If C7.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D7.Focus()
        End If
    End Sub
    Private Sub C8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C8.KeyPress
        If C8.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D8.Focus()
        End If
    End Sub
    Private Sub C9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C9.KeyPress
        If C9.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D9.Focus()
        End If
    End Sub
    Private Sub C10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C10.KeyPress
        If C10.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D10.Focus()
        End If
    End Sub
    Private Sub C11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C11.KeyPress
        If C11.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C11.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D11.Focus()
        End If
    End Sub
    Private Sub C12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C12.KeyPress
        If C12.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C12.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D12.Focus()
        End If
    End Sub
    Private Sub C13_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C13.KeyPress
        If C13.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C13.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D13.Focus()
        End If
    End Sub
    Private Sub C14_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C14.KeyPress
        If C14.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C14.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D14.Focus()
        End If
    End Sub
    Private Sub C15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C15.KeyPress
        If C15.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C15.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D15.Focus()
        End If
    End Sub
    Private Sub C16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C16.KeyPress
        If C16.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C16.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D16.Focus()
        End If
    End Sub
    Private Sub C17_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C17.KeyPress
        If C17.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C17.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D17.Focus()
        End If
    End Sub
    Private Sub C18_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C18.KeyPress
        If C18.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C18.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D18.Focus()
        End If
    End Sub
    Private Sub C19_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C19.KeyPress
        If C19.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C19.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D19.Focus()
        End If
    End Sub
    Private Sub C20_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C20.KeyPress
        If C20.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C20.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D20.Focus()
        End If
    End Sub
    Private Sub C21_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C21.KeyPress
        If C21.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C21.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D21.Focus()
        End If
    End Sub
    Private Sub C22_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C22.KeyPress
        If C22.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C22.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D22.Focus()
        End If
    End Sub
    Private Sub C23_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C23.KeyPress
        If C23.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C23.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D23.Focus()
        End If
    End Sub
    Private Sub C24_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C24.KeyPress
        If C24.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C24.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D24.Focus()
        End If
    End Sub
    Private Sub C25_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles C25.KeyPress
        If C25.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf C25.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            D25.Focus()
        End If
    End Sub

    Private Sub D1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D1.KeyPress
        If D1.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E1.Focus()
        End If
    End Sub
    Private Sub D2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D2.KeyPress
        If D2.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E2.Focus()
        End If
    End Sub
    Private Sub D3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D3.KeyPress
        If D3.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E3.Focus()
        End If
    End Sub
    Private Sub D4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D4.KeyPress
        If D4.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E4.Focus()
        End If
    End Sub
    Private Sub D5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D5.KeyPress
        If D5.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E5.Focus()
        End If
    End Sub
    Private Sub D6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D6.KeyPress
        If D6.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E6.Focus()
        End If
    End Sub
    Private Sub D7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D7.KeyPress
        If D7.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E7.Focus()
        End If
    End Sub
    Private Sub D8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D8.KeyPress
        If D8.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E8.Focus()
        End If
    End Sub
    Private Sub D9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D9.KeyPress
        If D9.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E9.Focus()
        End If
    End Sub
    Private Sub D10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D10.KeyPress
        If D10.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E10.Focus()
        End If
    End Sub
    Private Sub D11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D11.KeyPress
        If D11.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D11.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E11.Focus()
        End If
    End Sub
    Private Sub D12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D12.KeyPress
        If D12.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D12.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E12.Focus()
        End If
    End Sub
    Private Sub D13_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D13.KeyPress
        If D13.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D13.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E13.Focus()
        End If
    End Sub
    Private Sub D14_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D14.KeyPress
        If D14.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D14.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E14.Focus()
        End If
    End Sub
    Private Sub D15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D15.KeyPress
        If D15.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D15.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E15.Focus()
        End If
    End Sub
    Private Sub D16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D16.KeyPress
        If D16.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D16.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E16.Focus()
        End If
    End Sub
    Private Sub D17_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D17.KeyPress
        If D17.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D17.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E17.Focus()
        End If
    End Sub
    Private Sub D18_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D18.KeyPress
        If D18.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D18.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E18.Focus()
        End If
    End Sub
    Private Sub D19_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D19.KeyPress
        If D19.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D19.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E19.Focus()
        End If
    End Sub
    Private Sub D20_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D20.KeyPress
        If D20.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D20.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E20.Focus()
        End If
    End Sub
    Private Sub D21_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D21.KeyPress
        If D21.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D21.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E21.Focus()
        End If
    End Sub
    Private Sub D22_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D22.KeyPress
        If D22.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D22.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E22.Focus()
        End If
    End Sub
    Private Sub D23_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D23.KeyPress
        If D23.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D23.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E23.Focus()
        End If
    End Sub
    Private Sub D24_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D24.KeyPress
        If D24.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D24.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E24.Focus()
        End If
    End Sub
    Private Sub D25_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles D25.KeyPress
        If D25.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf D25.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            E25.Focus()
        End If
    End Sub

    Private Sub E1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E1.KeyPress
        If E1.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M2.Focus()
        End If
    End Sub
    Private Sub E2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E2.KeyPress
        If E2.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M3.Focus()
        End If
    End Sub
    Private Sub E3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E3.KeyPress
        If E3.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M4.Focus()
        End If
    End Sub
    Private Sub E4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E4.KeyPress
        If E4.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M5.Focus()
        End If
    End Sub
    Private Sub E5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E5.KeyPress
        If E5.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M6.Focus()
        End If
    End Sub
    Private Sub E6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E6.KeyPress
        If E6.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M7.Focus()
        End If
    End Sub
    Private Sub E7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E7.KeyPress
        If E7.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M8.Focus()
        End If
    End Sub
    Private Sub E8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E8.KeyPress
        If E8.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M9.Focus()
        End If
    End Sub
    Private Sub E9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E9.KeyPress
        If E9.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M10.Focus()
        End If
    End Sub
    Private Sub E10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E10.KeyPress
        If E10.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M11.Focus()
        End If
    End Sub
    Private Sub E11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E11.KeyPress
        If E11.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E11.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M12.Focus()
        End If
    End Sub
    Private Sub E12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E12.KeyPress
        If E12.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E12.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M13.Focus()
        End If
    End Sub
    Private Sub E13_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E13.KeyPress
        If E13.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E13.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M14.Focus()
        End If
    End Sub
    Private Sub E14_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E14.KeyPress
        If E14.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E14.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M15.Focus()
        End If
    End Sub
    Private Sub E15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E15.KeyPress
        If E15.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E15.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M16.Focus()
        End If
    End Sub
    Private Sub E16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E16.KeyPress
        If E16.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E16.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M17.Focus()
        End If
    End Sub
    Private Sub E17_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E17.KeyPress
        If E17.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E17.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M18.Focus()
        End If
    End Sub
    Private Sub E18_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E18.KeyPress
        If E18.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E18.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M19.Focus()
        End If
    End Sub
    Private Sub E19_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E19.KeyPress
        If E19.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E19.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M20.Focus()
        End If
    End Sub
    Private Sub E20_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E20.KeyPress
        If E20.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E20.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M21.Focus()
        End If
    End Sub
    Private Sub E21_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E21.KeyPress
        If E21.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E21.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M22.Focus()
        End If
    End Sub
    Private Sub E22_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E22.KeyPress
        If E22.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E22.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M23.Focus()
        End If
    End Sub
    Private Sub E23_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E23.KeyPress
        If E23.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E23.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M24.Focus()
        End If
    End Sub
    Private Sub E24_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E24.KeyPress
        If E24.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E24.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            M25.Focus()
        End If
    End Sub
    Private Sub E25_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles E25.KeyPress
        If E25.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf E25.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub

    Private Sub M1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M1.KeyPress
        If M1.Text.Contains(",") Or M1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K1.Focus()
        End If
    End Sub
    Private Sub M2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M2.KeyPress
        If M2.Text.Contains(",") Or M2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K2.Focus()
        End If
    End Sub
    Private Sub M3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M3.KeyPress
        If M3.Text.Contains(",") Or M3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K3.Focus()
        End If
    End Sub
    Private Sub M4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M4.KeyPress
        If M4.Text.Contains(",") Or M4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K4.Focus()
        End If
    End Sub
    Private Sub M5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M5.KeyPress
        If M5.Text.Contains(",") Or M5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K5.Focus()
        End If
    End Sub
    Private Sub M6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M6.KeyPress
        If M6.Text.Contains(",") Or M6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K6.Focus()
        End If
    End Sub
    Private Sub M7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M7.KeyPress
        If M7.Text.Contains(",") Or M7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K7.Focus()
        End If
    End Sub
    Private Sub M8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M8.KeyPress
        If M8.Text.Contains(",") Or M8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K8.Focus()
        End If
    End Sub
    Private Sub M9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M9.KeyPress
        If M9.Text.Contains(",") Or M9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K9.Focus()
        End If
    End Sub
    Private Sub M10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M10.KeyPress
        If M10.Text.Contains(",") Or M10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K10.Focus()
        End If
    End Sub
    Private Sub M11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M11.KeyPress
        If M11.Text.Contains(",") Or M11.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K11.Focus()
        End If
    End Sub
    Private Sub M12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M12.KeyPress
        If M12.Text.Contains(",") Or M12.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K12.Focus()
        End If
    End Sub
    Private Sub M13_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M13.KeyPress
        If M13.Text.Contains(",") Or M13.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K13.Focus()
        End If
    End Sub
    Private Sub M14_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M14.KeyPress
        If M14.Text.Contains(",") Or M14.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K14.Focus()
        End If
    End Sub
    Private Sub M15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M15.KeyPress
        If M15.Text.Contains(",") Or M15.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K15.Focus()
        End If
    End Sub
    Private Sub M16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M16.KeyPress
        If M16.Text.Contains(",") Or M16.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K16.Focus()
        End If
    End Sub
    Private Sub M17_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M17.KeyPress
        If M17.Text.Contains(",") Or M17.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K17.Focus()
        End If
    End Sub
    Private Sub M18_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M18.KeyPress
        If M18.Text.Contains(",") Or M18.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K18.Focus()
        End If
    End Sub
    Private Sub M19_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M19.KeyPress
        If M19.Text.Contains(",") Or M19.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K19.Focus()
        End If
    End Sub
    Private Sub M20_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M20.KeyPress
        If M20.Text.Contains(",") Or M20.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K20.Focus()
        End If
    End Sub
    Private Sub M21_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M21.KeyPress
        If M21.Text.Contains(",") Or M21.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K21.Focus()
        End If
    End Sub
    Private Sub M22_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M22.KeyPress
        If M22.Text.Contains(",") Or M22.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K22.Focus()
        End If
    End Sub
    Private Sub M23_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M23.KeyPress
        If M23.Text.Contains(",") Or M23.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K23.Focus()
        End If
    End Sub
    Private Sub M24_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M24.KeyPress
        If M24.Text.Contains(",") Or M24.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K24.Focus()
        End If
    End Sub
    Private Sub M25_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M25.KeyPress
        If M25.Text.Contains(",") Or M25.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
        If e.KeyChar = Chr(13) Then
            K25.Focus()
        End If
    End Sub

    Private Sub K1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K1.KeyPress
        If e.KeyChar = Chr(13) Then
            A1.Focus()
        End If
    End Sub
    Private Sub K2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K2.KeyPress
        If e.KeyChar = Chr(13) Then
            A2.Focus()
        End If
    End Sub
    Private Sub K3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K3.KeyPress
        If e.KeyChar = Chr(13) Then
            A3.Focus()
        End If
    End Sub
    Private Sub K4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K4.KeyPress
        If e.KeyChar = Chr(13) Then
            A4.Focus()
        End If
    End Sub
    Private Sub K5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K5.KeyPress
        If e.KeyChar = Chr(13) Then
            A5.Focus()
        End If
    End Sub
    Private Sub K6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K6.KeyPress
        If e.KeyChar = Chr(13) Then
            A6.Focus()
        End If
    End Sub
    Private Sub K7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K7.KeyPress
        If e.KeyChar = Chr(13) Then
            A7.Focus()
        End If
    End Sub
    Private Sub K8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K8.KeyPress
        If e.KeyChar = Chr(13) Then
            A8.Focus()
        End If
    End Sub
    Private Sub K9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K9.KeyPress
        If e.KeyChar = Chr(13) Then
            A9.Focus()
        End If
    End Sub
    Private Sub K10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K10.KeyPress
        If e.KeyChar = Chr(13) Then
            A10.Focus()
        End If
    End Sub
    Private Sub K11_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K11.KeyPress
        If e.KeyChar = Chr(13) Then
            A11.Focus()
        End If
    End Sub
    Private Sub K12_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K12.KeyPress
        If e.KeyChar = Chr(13) Then
            A12.Focus()
        End If
    End Sub
    Private Sub K13_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K13.KeyPress
        If e.KeyChar = Chr(13) Then
            A13.Focus()
        End If
    End Sub
    Private Sub K14_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K14.KeyPress
        If e.KeyChar = Chr(13) Then
            A14.Focus()
        End If
    End Sub
    Private Sub K15_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K15.KeyPress
        If e.KeyChar = Chr(13) Then
            A15.Focus()
        End If
    End Sub
    Private Sub K16_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K16.KeyPress
        If e.KeyChar = Chr(13) Then
            A16.Focus()
        End If
    End Sub
    Private Sub K17_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K17.KeyPress
        If e.KeyChar = Chr(13) Then
            A17.Focus()
        End If
    End Sub
    Private Sub K18_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K18.KeyPress
        If e.KeyChar = Chr(13) Then
            A18.Focus()
        End If
    End Sub
    Private Sub K19_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K19.KeyPress
        If e.KeyChar = Chr(13) Then
            A19.Focus()
        End If
    End Sub
    Private Sub K20_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K20.KeyPress
        If e.KeyChar = Chr(13) Then
            A20.Focus()
        End If
    End Sub
    Private Sub K21_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K21.KeyPress
        If e.KeyChar = Chr(13) Then
            A21.Focus()
        End If
    End Sub
    Private Sub K22_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K22.KeyPress
        If e.KeyChar = Chr(13) Then
            A22.Focus()
        End If
    End Sub
    Private Sub K23_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K23.KeyPress
        If e.KeyChar = Chr(13) Then
            A23.Focus()
        End If
    End Sub
    Private Sub K24_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K24.KeyPress
        If e.KeyChar = Chr(13) Then
            A24.Focus()
        End If
    End Sub
    Private Sub K25_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles K25.KeyPress
        If e.KeyChar = Chr(13) Then
            A25.Focus()
        End If
    End Sub

    Private Sub Y1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y1.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M1.Text.Replace(",", ".")) - Val(Y1.Text.Replace(",", "."))) / Val(M1.Text.Replace(",", "."))) * 100, 2)
            S1.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M1.Text.Replace(",", ".")) - (Val(Y1.Text.Replace(",", ".")) * 0.9144)) / Val(M1.Text.Replace(",", "."))) * 100, 2)
            S1.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y2.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M2.Text.Replace(",", ".")) - Val(Y2.Text.Replace(",", "."))) / Val(M2.Text.Replace(",", "."))) * 100, 2)
            S2.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M2.Text.Replace(",", ".")) - (Val(Y2.Text.Replace(",", ".")) * 0.9144)) / Val(M2.Text.Replace(",", "."))) * 100, 2)
            S2.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y3.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M3.Text.Replace(",", ".")) - Val(Y3.Text.Replace(",", "."))) / Val(M3.Text.Replace(",", "."))) * 100, 2)
            S3.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M3.Text.Replace(",", ".")) - (Val(Y3.Text.Replace(",", ".")) * 0.9144)) / Val(M3.Text.Replace(",", "."))) * 100, 2)
            S3.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y4.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M4.Text.Replace(",", ".")) - Val(Y4.Text.Replace(",", "."))) / Val(M4.Text.Replace(",", "."))) * 100, 2)
            S4.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M4.Text.Replace(",", ".")) - (Val(Y4.Text.Replace(",", ".")) * 0.9144)) / Val(M4.Text.Replace(",", "."))) * 100, 2)
            S4.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y5.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M5.Text.Replace(",", ".")) - Val(Y5.Text.Replace(",", "."))) / Val(M5.Text.Replace(",", "."))) * 100, 2)
            S5.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M5.Text.Replace(",", ".")) - (Val(Y5.Text.Replace(",", ".")) * 0.9144)) / Val(M5.Text.Replace(",", "."))) * 100, 2)
            S5.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y6.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M6.Text.Replace(",", ".")) - Val(Y6.Text.Replace(",", "."))) / Val(M6.Text.Replace(",", "."))) * 100, 2)
            S6.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M6.Text.Replace(",", ".")) - (Val(Y6.Text.Replace(",", ".")) * 0.9144)) / Val(M6.Text.Replace(",", "."))) * 100, 2)
            S6.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y7.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M7.Text.Replace(",", ".")) - Val(Y7.Text.Replace(",", "."))) / Val(M7.Text.Replace(",", "."))) * 100, 2)
            S7.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M7.Text.Replace(",", ".")) - (Val(Y7.Text.Replace(",", ".")) * 0.9144)) / Val(M7.Text.Replace(",", "."))) * 100, 2)
            S7.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y8.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M8.Text.Replace(",", ".")) - Val(Y8.Text.Replace(",", "."))) / Val(M8.Text.Replace(",", "."))) * 100, 2)
            S8.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M8.Text.Replace(",", ".")) - (Val(Y8.Text.Replace(",", ".")) * 0.9144)) / Val(M8.Text.Replace(",", "."))) * 100, 2)
            S8.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y9.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M9.Text.Replace(",", ".")) - Val(Y9.Text.Replace(",", "."))) / Val(M9.Text.Replace(",", "."))) * 100, 2)
            S9.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M9.Text.Replace(",", ".")) - (Val(Y9.Text.Replace(",", ".")) * 0.9144)) / Val(M9.Text.Replace(",", "."))) * 100, 2)
            S9.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y10.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M10.Text.Replace(",", ".")) - Val(Y10.Text.Replace(",", "."))) / Val(M10.Text.Replace(",", "."))) * 100, 2)
            S10.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M10.Text.Replace(",", ".")) - (Val(Y10.Text.Replace(",", ".")) * 0.9144)) / Val(M10.Text.Replace(",", "."))) * 100, 2)
            S10.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y11_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y11.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M11.Text.Replace(",", ".")) - Val(Y11.Text.Replace(",", "."))) / Val(M11.Text.Replace(",", "."))) * 100, 2)
            S11.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M11.Text.Replace(",", ".")) - (Val(Y11.Text.Replace(",", ".")) * 0.9144)) / Val(M11.Text.Replace(",", "."))) * 100, 2)
            S11.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y12_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y12.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M12.Text.Replace(",", ".")) - Val(Y12.Text.Replace(",", "."))) / Val(M12.Text.Replace(",", "."))) * 100, 2)
            S12.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M12.Text.Replace(",", ".")) - (Val(Y12.Text.Replace(",", ".")) * 0.9144)) / Val(M12.Text.Replace(",", "."))) * 100, 2)
            S12.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y13_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y13.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M13.Text.Replace(",", ".")) - Val(Y13.Text.Replace(",", "."))) / Val(M13.Text.Replace(",", "."))) * 100, 2)
            S13.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M13.Text.Replace(",", ".")) - (Val(Y13.Text.Replace(",", ".")) * 0.9144)) / Val(M13.Text.Replace(",", "."))) * 100, 2)
            S13.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y14_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y14.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M14.Text.Replace(",", ".")) - Val(Y14.Text.Replace(",", "."))) / Val(M14.Text.Replace(",", "."))) * 100, 2)
            S14.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M14.Text.Replace(",", ".")) - (Val(Y14.Text.Replace(",", ".")) * 0.9144)) / Val(M14.Text.Replace(",", "."))) * 100, 2)
            S14.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y15_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y15.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M15.Text.Replace(",", ".")) - Val(Y15.Text.Replace(",", "."))) / Val(M15.Text.Replace(",", "."))) * 100, 2)
            S15.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M15.Text.Replace(",", ".")) - (Val(Y15.Text.Replace(",", ".")) * 0.9144)) / Val(M15.Text.Replace(",", "."))) * 100, 2)
            S15.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y16_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y16.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M16.Text.Replace(",", ".")) - Val(Y16.Text.Replace(",", "."))) / Val(M16.Text.Replace(",", "."))) * 100, 2)
            S16.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M16.Text.Replace(",", ".")) - (Val(Y16.Text.Replace(",", ".")) * 0.9144)) / Val(M16.Text.Replace(",", "."))) * 100, 2)
            S16.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y17_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y17.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M17.Text.Replace(",", ".")) - Val(Y17.Text.Replace(",", "."))) / Val(M17.Text.Replace(",", "."))) * 100, 2)
            S17.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M17.Text.Replace(",", ".")) - (Val(Y17.Text.Replace(",", ".")) * 0.9144)) / Val(M17.Text.Replace(",", "."))) * 100, 2)
            S17.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y18_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y18.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M18.Text.Replace(",", ".")) - Val(Y18.Text.Replace(",", "."))) / Val(M18.Text.Replace(",", "."))) * 100, 2)
            S18.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M18.Text.Replace(",", ".")) - (Val(Y18.Text.Replace(",", ".")) * 0.9144)) / Val(M18.Text.Replace(",", "."))) * 100, 2)
            S18.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y19_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y19.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M19.Text.Replace(",", ".")) - Val(Y19.Text.Replace(",", "."))) / Val(M19.Text.Replace(",", "."))) * 100, 2)
            S19.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M19.Text.Replace(",", ".")) - (Val(Y19.Text.Replace(",", ".")) * 0.9144)) / Val(M19.Text.Replace(",", "."))) * 100, 2)
            S19.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y20_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y20.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M20.Text.Replace(",", ".")) - Val(Y20.Text.Replace(",", "."))) / Val(M20.Text.Replace(",", "."))) * 100, 2)
            S20.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M20.Text.Replace(",", ".")) - (Val(Y20.Text.Replace(",", ".")) * 0.9144)) / Val(M20.Text.Replace(",", "."))) * 100, 2)
            S20.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y21_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y21.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M21.Text.Replace(",", ".")) - Val(Y21.Text.Replace(",", "."))) / Val(M21.Text.Replace(",", "."))) * 100, 2)
            S21.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M21.Text.Replace(",", ".")) - (Val(Y21.Text.Replace(",", ".")) * 0.9144)) / Val(M21.Text.Replace(",", "."))) * 100, 2)
            S21.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y22_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y22.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M22.Text.Replace(",", ".")) - Val(Y22.Text.Replace(",", "."))) / Val(M22.Text.Replace(",", "."))) * 100, 2)
            S22.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M22.Text.Replace(",", ".")) - (Val(Y22.Text.Replace(",", ".")) * 0.9144)) / Val(M22.Text.Replace(",", "."))) * 100, 2)
            S22.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y23_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y23.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M23.Text.Replace(",", ".")) - Val(Y23.Text.Replace(",", "."))) / Val(M23.Text.Replace(",", "."))) * 100, 2)
            S23.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M23.Text.Replace(",", ".")) - (Val(Y23.Text.Replace(",", ".")) * 0.9144)) / Val(M23.Text.Replace(",", "."))) * 100, 2)
            S23.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y24_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y24.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M24.Text.Replace(",", ".")) - Val(Y24.Text.Replace(",", "."))) / Val(M24.Text.Replace(",", "."))) * 100, 2)
            S24.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M24.Text.Replace(",", ".")) - (Val(Y24.Text.Replace(",", ".")) * 0.9144)) / Val(M24.Text.Replace(",", "."))) * 100, 2)
            S24.Text = susut.ToString + " %"
        End If
    End Sub
    Private Sub Y25_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Y25.TextChanged
        Dim susut As Double
        If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M25.Text.Replace(",", ".")) - Val(Y25.Text.Replace(",", "."))) / Val(M25.Text.Replace(",", "."))) * 100, 2)
            S25.Text = susut.ToString + " %"
        ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
            susut = Math.Round(((Val(M25.Text.Replace(",", ".")) - (Val(Y25.Text.Replace(",", ".")) * 0.9144)) / Val(M25.Text.Replace(",", "."))) * 100, 2)
            S25.Text = susut.ToString + " %"
        End If
    End Sub

    Private Sub M1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M1.TextChanged
        A1.Text = ""
        B1.Text = ""
        C1.Text = ""
        D1.Text = ""
        E1.Text = ""
        Y1.Text = ""
        S1.Text = ""
        'If Not Val(M1.Text) = 0 Then
        '    ts_hitung.Enabled = True
        'Else
        '    ts_hitung.Enabled = False
        'End If
    End Sub
    Private Sub M2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M2.TextChanged
        A2.Text = ""
        B2.Text = ""
        C2.Text = ""
        D2.Text = ""
        E2.Text = ""
        Y2.Text = ""
        S2.Text = ""
    End Sub
    Private Sub M3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M3.TextChanged
        A3.Text = ""
        B3.Text = ""
        C3.Text = ""
        D3.Text = ""
        E3.Text = ""
        Y3.Text = ""
        S3.Text = ""
    End Sub
    Private Sub M4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M4.TextChanged
        A4.Text = ""
        B4.Text = ""
        C4.Text = ""
        D4.Text = ""
        E4.Text = ""
        Y4.Text = ""
        S4.Text = ""
    End Sub
    Private Sub M5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M5.TextChanged
        A5.Text = ""
        B5.Text = ""
        C5.Text = ""
        D5.Text = ""
        E5.Text = ""
        Y5.Text = ""
        S5.Text = ""
    End Sub
    Private Sub M6_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M6.TextChanged
        A6.Text = ""
        B6.Text = ""
        C6.Text = ""
        D6.Text = ""
        E6.Text = ""
        Y6.Text = ""
        S6.Text = ""
    End Sub
    Private Sub M7_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M7.TextChanged
        A7.Text = ""
        B7.Text = ""
        C7.Text = ""
        D7.Text = ""
        E7.Text = ""
        Y7.Text = ""
        S7.Text = ""
    End Sub
    Private Sub M8_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M8.TextChanged
        A8.Text = ""
        B8.Text = ""
        C8.Text = ""
        D8.Text = ""
        E8.Text = ""
        Y8.Text = ""
        S8.Text = ""
    End Sub
    Private Sub M9_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M9.TextChanged
        A9.Text = ""
        B9.Text = ""
        C9.Text = ""
        D9.Text = ""
        E9.Text = ""
        Y9.Text = ""
        S9.Text = ""
    End Sub
    Private Sub M10_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M10.TextChanged
        A10.Text = ""
        B10.Text = ""
        C10.Text = ""
        D10.Text = ""
        E10.Text = ""
        Y10.Text = ""
        S10.Text = ""
    End Sub
    Private Sub M11_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M11.TextChanged
        A11.Text = ""
        B11.Text = ""
        C11.Text = ""
        D11.Text = ""
        E11.Text = ""
        Y11.Text = ""
        S11.Text = ""
    End Sub
    Private Sub M12_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M12.TextChanged
        A12.Text = ""
        B12.Text = ""
        C12.Text = ""
        D12.Text = ""
        E12.Text = ""
        Y12.Text = ""
        S12.Text = ""
    End Sub
    Private Sub M13_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M13.TextChanged
        A13.Text = ""
        B13.Text = ""
        C13.Text = ""
        D13.Text = ""
        E13.Text = ""
        Y13.Text = ""
        S13.Text = ""
    End Sub
    Private Sub M14_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M14.TextChanged
        A14.Text = ""
        B14.Text = ""
        C14.Text = ""
        D14.Text = ""
        E14.Text = ""
        Y14.Text = ""
        S14.Text = ""
    End Sub
    Private Sub M15_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M15.TextChanged
        A15.Text = ""
        B15.Text = ""
        C15.Text = ""
        D15.Text = ""
        E15.Text = ""
        Y15.Text = ""
        S15.Text = ""
    End Sub
    Private Sub M16_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M16.TextChanged
        A16.Text = ""
        B16.Text = ""
        C16.Text = ""
        D16.Text = ""
        E16.Text = ""
        Y16.Text = ""
        S16.Text = ""
    End Sub
    Private Sub M17_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M17.TextChanged
        A17.Text = ""
        B17.Text = ""
        C17.Text = ""
        D17.Text = ""
        E17.Text = ""
        Y17.Text = ""
        S17.Text = ""
    End Sub
    Private Sub M18_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M18.TextChanged
        A18.Text = ""
        B18.Text = ""
        C18.Text = ""
        D18.Text = ""
        E18.Text = ""
        Y18.Text = ""
        S18.Text = ""
    End Sub
    Private Sub M19_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M19.TextChanged
        A19.Text = ""
        B19.Text = ""
        C19.Text = ""
        D19.Text = ""
        E19.Text = ""
        Y19.Text = ""
        S19.Text = ""
    End Sub
    Private Sub M20_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M20.TextChanged
        A20.Text = ""
        B20.Text = ""
        C20.Text = ""
        D20.Text = ""
        E20.Text = ""
        Y20.Text = ""
        S20.Text = ""
    End Sub
    Private Sub M21_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M21.TextChanged
        A21.Text = ""
        B21.Text = ""
        C21.Text = ""
        D21.Text = ""
        E21.Text = ""
        Y21.Text = ""
        S21.Text = ""
    End Sub
    Private Sub M22_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M22.TextChanged
        A22.Text = ""
        B22.Text = ""
        C22.Text = ""
        D22.Text = ""
        E22.Text = ""
        Y22.Text = ""
        S22.Text = ""
    End Sub
    Private Sub M23_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M23.TextChanged
        A23.Text = ""
        B23.Text = ""
        C23.Text = ""
        D23.Text = ""
        E23.Text = ""
        Y23.Text = ""
        S23.Text = ""
    End Sub
    Private Sub M24_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M24.TextChanged
        A24.Text = ""
        B24.Text = ""
        C24.Text = ""
        D24.Text = ""
        E24.Text = ""
        Y24.Text = ""
        S24.Text = ""
    End Sub
    Private Sub M25_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles M25.TextChanged
        A25.Text = ""
        B25.Text = ""
        C25.Text = ""
        D25.Text = ""
        E25.Text = ""
        Y25.Text = ""
        S25.Text = ""
    End Sub

    Private Sub txt_no_po_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po.GotFocus, _
         txt_customer.GotFocus, txt_jenis_kain.GotFocus, txt_warna.GotFocus
        If txt_gudang.Text = "" Then
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_hasil_packing"
            form_input_gudang.Focus()
        Else
            form_stok_wip_packing.MdiParent = form_menu_utama
            form_stok_wip_packing.Show()
            form_stok_wip_packing.txt_form.Text = "form_input_hasil_packing"
            form_stok_wip_packing.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_packing.txt_cari_gudang.ReadOnly = True
            form_stok_wip_packing.Focus()
        End If
    End Sub
    Private Sub txt_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang.GotFocus
        If txt_gudang.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_hasil_packing"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gudang.TextChanged
        txt_id_grey.Text = ""
        txt_harga_asal.Text = ""
        txt_resep.Text = ""
        txt_partai.Text = ""
        txt_no_po.Text = ""
        txt_jenis_kain.Text = ""
        txt_warna.Text = ""
        txt_asal_gulung.Text = ""
        txt_asal_meter.Text = ""
        txt_customer.Text = ""
        txt_no_po.ReadOnly = False
        txt_jenis_kain.ReadOnly = False
        txt_warna.ReadOnly = False
        txt_asal_gulung.ReadOnly = False
        txt_meter.ReadOnly = False
        txt_customer.ReadOnly = False
    End Sub
    Private Sub txt_harga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga.Select(txt_harga.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga.Select(txt_harga.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga.Text = String.Empty Then
                        txt_total_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga.Select(txt_total_harga.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga.Text = String.Empty Then
                        txt_total_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga.Select(txt_total_harga.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga.TextChanged
        If txt_harga.Text <> String.Empty Then
            Dim temp As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga.Select(txt_harga.Text.Length, 0)
            ElseIf txt_harga.Text = "-"c Then

            Else
                txt_harga.Text = CDec(temp).ToString("N0")
                txt_harga.Select(txt_harga.Text.Length, 0)
            End If
        End If

        Dim total_harga As Double
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        total_harga = Val(harga.Replace(",", ".")) * Val(meter.Replace(",", "."))
        txt_total_harga.Text = Math.Round(total_harga, 0)
    End Sub
    Private Sub txt_total_harga_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_total_harga.TextChanged
        If txt_total_harga.Text <> String.Empty Then
            Dim temp As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga.Select(txt_total_harga.Text.Length, 0)
            ElseIf txt_total_harga.Text = "-"c Then

            Else
                txt_total_harga.Text = CDec(temp).ToString("N0")
                txt_total_harga.Select(txt_total_harga.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_asal_meter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_meter.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_asal_meter.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_asal_meter.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_asal_meter.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_asal_meter.Text = String.Empty Then
                        txt_asal_meter.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_asal_meter.Select(txt_asal_meter.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_asal_meter.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_asal_meter.Text = String.Empty Then
                        txt_asal_meter.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_asal_meter.Select(txt_asal_meter.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_asal_meter_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_meter.TextChanged
        If txt_asal_meter.Text <> String.Empty Then
            Dim temp As String = txt_asal_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_asal_meter.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_asal_meter.Select(txt_asal_meter.Text.Length, 0)
            ElseIf txt_asal_meter.Text = "-"c Then

            Else
                txt_asal_meter.Text = CDec(temp).ToString("N0")
                txt_asal_meter.Select(txt_asal_meter.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_qty_awal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_awal.TextChanged
        If txt_qty_awal.Text <> String.Empty Then
            Dim temp As String = txt_qty_awal.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_awal.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_awal.Select(txt_qty_awal.Text.Length, 0)
            ElseIf txt_qty_awal.Text = "-"c Then

            Else
                txt_qty_awal.Text = CDec(temp).ToString("N0")
                txt_qty_awal.Select(txt_qty_awal.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub ts_edit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_edit.Click
        Panel3.Enabled = True
        ts_hitung.Enabled = True
        ts_edit.Enabled = False
        txt_gulung.Text = ""
        txt_meter.Text = ""
        txt_susut.Text = ""
        txt_grade_a.Text = ""
        txt_grade_b.Text = ""
        txt_claim_jadi.Text = ""
        txt_claim_celup.Text = ""
        txt_gl_grade_a.Text = ""
        txt_gl_grade_b.Text = ""
        txt_gl_claim_jadi.Text = ""
        txt_gl_claim_celup.Text = ""
        txt_harga.Text = ""
        txt_total_harga.Text = ""
    End Sub
    Private Sub ts_hitung_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hitung.Click
        If Val(M1.Text) = 0 Then
            MsgBox("Hasil Packing Belum Diinput")
            M1.Focus()
        ElseIf Not Val(M1.Text) = 0 And Val(Y1.Text) = 0 Then
            MsgBox("Yard Baris 1 Belum Diinput")
            A1.Focus()
        ElseIf Not Val(M2.Text) = 0 And Val(Y2.Text) = 0 Then
            MsgBox("Yard Baris 2 Belum Diinput")
            A2.Focus()
        ElseIf Not Val(M3.Text) = 0 And Val(Y3.Text) = 0 Then
            MsgBox("Yard Baris 3 Belum Diinput")
            A3.Focus()
        ElseIf Not Val(M4.Text) = 0 And Val(Y4.Text) = 0 Then
            MsgBox("Yard Baris 4 Belum Diinput")
            A4.Focus()
        ElseIf Not Val(M5.Text) = 0 And Val(Y5.Text) = 0 Then
            MsgBox("Yard Baris 5 Belum Diinput")
            A5.Focus()
        ElseIf Not Val(M6.Text) = 0 And Val(Y6.Text) = 0 Then
            MsgBox("Yard Baris 6 Belum Diinput")
            A6.Focus()
        ElseIf Not Val(M7.Text) = 0 And Val(Y7.Text) = 0 Then
            MsgBox("Yard Baris 7 Belum Diinput")
            A7.Focus()
        ElseIf Not Val(M8.Text) = 0 And Val(Y8.Text) = 0 Then
            MsgBox("Yard Baris 8 Belum Diinput")
            A8.Focus()
        ElseIf Not Val(M9.Text) = 0 And Val(Y9.Text) = 0 Then
            MsgBox("Yard Baris 9 Belum Diinput")
            A9.Focus()
        ElseIf Not Val(M10.Text) = 0 And Val(Y10.Text) = 0 Then
            MsgBox("Yard Baris 10 Belum Diinput")
            A10.Focus()
        ElseIf Not Val(M11.Text) = 0 And Val(Y11.Text) = 0 Then
            MsgBox("Yard Baris 11 Belum Diinput")
            A11.Focus()
        ElseIf Not Val(M12.Text) = 0 And Val(Y12.Text) = 0 Then
            MsgBox("Yard Baris 12 Belum Diinput")
            A12.Focus()
        ElseIf Not Val(M13.Text) = 0 And Val(Y13.Text) = 0 Then
            MsgBox("Yard Baris 13 Belum Diinput")
            A13.Focus()
        ElseIf Not Val(M14.Text) = 0 And Val(Y14.Text) = 0 Then
            MsgBox("Yard Baris 14 Belum Diinput")
            A14.Focus()
        ElseIf Not Val(M15.Text) = 0 And Val(Y15.Text) = 0 Then
            MsgBox("Yard Baris 15 Belum Diinput")
            A15.Focus()
        ElseIf Not Val(M16.Text) = 0 And Val(Y16.Text) = 0 Then
            MsgBox("Yard Baris 16 Belum Diinput")
            A16.Focus()
        ElseIf Not Val(M17.Text) = 0 And Val(Y17.Text) = 0 Then
            MsgBox("Yard Baris 17 Belum Diinput")
            A17.Focus()
        ElseIf Not Val(M18.Text) = 0 And Val(Y18.Text) = 0 Then
            MsgBox("Yard Baris 18 Belum Diinput")
            A18.Focus()
        ElseIf Not Val(M19.Text) = 0 And Val(Y19.Text) = 0 Then
            MsgBox("Yard Baris 19 Belum Diinput")
            A19.Focus()
        ElseIf Not Val(M20.Text) = 0 And Val(Y20.Text) = 0 Then
            MsgBox("Yard Baris 20 Belum Diinput")
            A20.Focus()
        ElseIf Not Val(M21.Text) = 0 And Val(Y21.Text) = 0 Then
            MsgBox("Yard Baris 21 Belum Diinput")
            A21.Focus()
        ElseIf Not Val(M22.Text) = 0 And Val(Y22.Text) = 0 Then
            MsgBox("Yard Baris 22 Belum Diinput")
            A22.Focus()
        ElseIf Not Val(M23.Text) = 0 And Val(Y23.Text) = 0 Then
            MsgBox("Yard Baris 23 Belum Diinput")
            A23.Focus()
        ElseIf Not Val(M24.Text) = 0 And Val(Y24.Text) = 0 Then
            MsgBox("Yard Baris 24 Belum Diinput")
            A24.Focus()
        ElseIf Not Val(M25.Text) = 0 And Val(Y25.Text) = 0 Then
            MsgBox("Yard Baris 25 Belum Diinput")
            A25.Focus()
        Else
            Panel3.Enabled = False
            ts_hitung.Enabled = False
            ts_edit.Enabled = True
            txt_asal_meter.Text = Val(M1.Text.Replace(",", ".")) + Val(M2.Text.Replace(",", ".")) + Val(M3.Text.Replace(",", ".")) + Val(M4.Text.Replace(",", ".")) + Val(M5.Text.Replace(",", ".")) + _
                Val(M6.Text.Replace(",", ".")) + Val(M7.Text.Replace(",", ".")) + Val(M8.Text.Replace(",", ".")) + Val(M9.Text.Replace(",", ".")) + Val(M10.Text.Replace(",", ".")) + _
                Val(M11.Text.Replace(",", ".")) + Val(M12.Text.Replace(",", ".")) + Val(M13.Text.Replace(",", ".")) + Val(M14.Text.Replace(",", ".")) + Val(M15.Text.Replace(",", ".")) + _
                Val(M16.Text.Replace(",", ".")) + Val(M17.Text.Replace(",", ".")) + Val(M18.Text.Replace(",", ".")) + Val(M19.Text.Replace(",", ".")) + Val(M20.Text.Replace(",", ".")) + _
                Val(M21.Text.Replace(",", ".")) + Val(M22.Text.Replace(",", ".")) + Val(M23.Text.Replace(",", ".")) + Val(M24.Text.Replace(",", ".")) + Val(M25.Text.Replace(",", "."))
            txt_meter.Text = Val(Y1.Text.Replace(",", ".")) + Val(Y2.Text.Replace(",", ".")) + Val(Y3.Text.Replace(",", ".")) + Val(Y4.Text.Replace(",", ".")) + Val(Y5.Text.Replace(",", ".")) + _
                Val(Y6.Text.Replace(",", ".")) + Val(Y7.Text.Replace(",", ".")) + Val(Y8.Text.Replace(",", ".")) + Val(Y9.Text.Replace(",", ".")) + Val(Y10.Text.Replace(",", ".")) + _
                Val(Y11.Text.Replace(",", ".")) + Val(Y12.Text.Replace(",", ".")) + Val(Y13.Text.Replace(",", ".")) + Val(Y14.Text.Replace(",", ".")) + Val(Y15.Text.Replace(",", ".")) + _
                Val(Y16.Text.Replace(",", ".")) + Val(Y17.Text.Replace(",", ".")) + Val(Y18.Text.Replace(",", ".")) + Val(Y19.Text.Replace(",", ".")) + Val(Y20.Text.Replace(",", ".")) + _
                Val(Y21.Text.Replace(",", ".")) + Val(Y22.Text.Replace(",", ".")) + Val(Y23.Text.Replace(",", ".")) + Val(Y24.Text.Replace(",", ".")) + Val(Y25.Text.Replace(",", "."))

            Dim qty_celup As String = txt_asal_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim qty_packing As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim susut As Double
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut = Math.Round(((Val(qty_celup.Replace(",", ".")) - Val(qty_packing.Replace(",", "."))) / Val(qty_celup.Replace(",", "."))) * 100, 2)
                txt_susut.Text = susut.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut = Math.Round(((Val(qty_celup.Replace(",", ".")) - (Val(qty_packing.Replace(",", ".")) * 0.9144)) / Val(qty_celup.Replace(",", "."))) * 100, 2)
                txt_susut.Text = susut.ToString + " %"
            End If

            Call isi_gl_A1()
            Call isi_gl_B1()
            Call isi_gl_C1()
            Call isi_gl_D1()
            Call isi_gl_E1()
            If Not Val(M2.Text) = 0 Then
                Call isi_gl_A2()
                Call isi_gl_B2()
                Call isi_gl_C2()
                Call isi_gl_D2()
                Call isi_gl_E2()
            End If
            If Not Val(M3.Text) = 0 Then
                Call isi_gl_A3()
                Call isi_gl_B3()
                Call isi_gl_C3()
                Call isi_gl_D3()
                Call isi_gl_E3()
            End If
            If Not Val(M4.Text) = 0 Then
                Call isi_gl_A4()
                Call isi_gl_B4()
                Call isi_gl_C4()
                Call isi_gl_D4()
                Call isi_gl_E4()
            End If
            If Not Val(M5.Text) = 0 Then
                Call isi_gl_A5()
                Call isi_gl_B5()
                Call isi_gl_C5()
                Call isi_gl_D5()
                Call isi_gl_E5()
            End If
            If Not Val(M6.Text) = 0 Then
                Call isi_gl_A6()
                Call isi_gl_B6()
                Call isi_gl_C6()
                Call isi_gl_D6()
                Call isi_gl_E6()
            End If
            If Not Val(M7.Text) = 0 Then
                Call isi_gl_A7()
                Call isi_gl_B7()
                Call isi_gl_C7()
                Call isi_gl_D7()
                Call isi_gl_E7()
            End If
            If Not Val(M8.Text) = 0 Then
                Call isi_gl_A8()
                Call isi_gl_B8()
                Call isi_gl_C8()
                Call isi_gl_D8()
                Call isi_gl_E8()
            End If
            If Not Val(M9.Text) = 0 Then
                Call isi_gl_A9()
                Call isi_gl_B9()
                Call isi_gl_C9()
                Call isi_gl_D9()
                Call isi_gl_E9()
            End If
            If Not Val(M10.Text) = 0 Then
                Call isi_gl_A10()
                Call isi_gl_B10()
                Call isi_gl_C10()
                Call isi_gl_D10()
                Call isi_gl_E10()
            End If
            If Not Val(M11.Text) = 0 Then
                Call isi_gl_A11()
                Call isi_gl_B11()
                Call isi_gl_C11()
                Call isi_gl_D11()
                Call isi_gl_E11()
            End If
            If Not Val(M12.Text) = 0 Then
                Call isi_gl_A12()
                Call isi_gl_B12()
                Call isi_gl_C12()
                Call isi_gl_D12()
                Call isi_gl_E12()
            End If
            If Not Val(M13.Text) = 0 Then
                Call isi_gl_A13()
                Call isi_gl_B13()
                Call isi_gl_C13()
                Call isi_gl_D13()
                Call isi_gl_E13()
            End If
            If Not Val(M14.Text) = 0 Then
                Call isi_gl_A14()
                Call isi_gl_B14()
                Call isi_gl_C14()
                Call isi_gl_D14()
                Call isi_gl_E14()
            End If
            If Not Val(M15.Text) = 0 Then
                Call isi_gl_A15()
                Call isi_gl_B15()
                Call isi_gl_C15()
                Call isi_gl_D15()
                Call isi_gl_E15()
            End If
            If Not Val(M16.Text) = 0 Then
                Call isi_gl_A16()
                Call isi_gl_B16()
                Call isi_gl_C16()
                Call isi_gl_D16()
                Call isi_gl_E16()
            End If
            If Not Val(M17.Text) = 0 Then
                Call isi_gl_A17()
                Call isi_gl_B17()
                Call isi_gl_C17()
                Call isi_gl_D17()
                Call isi_gl_E17()
            End If
            If Not Val(M18.Text) = 0 Then
                Call isi_gl_A18()
                Call isi_gl_B18()
                Call isi_gl_C18()
                Call isi_gl_D18()
                Call isi_gl_E18()
            End If
            If Not Val(M19.Text) = 0 Then
                Call isi_gl_A19()
                Call isi_gl_B19()
                Call isi_gl_C19()
                Call isi_gl_D19()
                Call isi_gl_E19()
            End If
            If Not Val(M20.Text) = 0 Then
                Call isi_gl_A20()
                Call isi_gl_B20()
                Call isi_gl_C20()
                Call isi_gl_D20()
                Call isi_gl_E20()
            End If
            If Not Val(M21.Text) = 0 Then
                Call isi_gl_A21()
                Call isi_gl_B21()
                Call isi_gl_C21()
                Call isi_gl_D21()
                Call isi_gl_E21()
            End If
            If Not Val(M22.Text) = 0 Then
                Call isi_gl_A22()
                Call isi_gl_B22()
                Call isi_gl_C22()
                Call isi_gl_D22()
                Call isi_gl_E22()
            End If
            If Not Val(M23.Text) = 0 Then
                Call isi_gl_A23()
                Call isi_gl_B23()
                Call isi_gl_C23()
                Call isi_gl_D23()
                Call isi_gl_E23()
            End If
            If Not Val(M24.Text) = 0 Then
                Call isi_gl_A24()
                Call isi_gl_B24()
                Call isi_gl_C24()
                Call isi_gl_D24()
                Call isi_gl_E24()
            End If
            If Not Val(M25.Text) = 0 Then
                Call isi_gl_A25()
                Call isi_gl_B25()
                Call isi_gl_C25()
                Call isi_gl_D25()
                Call isi_gl_E25()
            End If

            If Val(txt_grade_a.Text) = 0 Then
                txt_grade_a.Text = "0"
            End If
            If Val(txt_grade_b.Text) = 0 Then
                txt_grade_b.Text = "0"
            End If
            If Val(txt_claim_jadi.Text) = 0 Then
                txt_claim_jadi.Text = "0"
            End If
            If Val(txt_claim_celup.Text) = 0 Then
                txt_claim_celup.Text = "0"
            End If
            If Val(txt_gl_grade_a.Text) = 0 Then
                txt_gl_grade_a.Text = "0"
            End If
            If Val(txt_gl_grade_b.Text) = 0 Then
                txt_gl_grade_b.Text = "0"
            End If
            If Val(txt_gl_claim_jadi.Text) = 0 Then
                txt_gl_claim_jadi.Text = "0"
            End If
            If Val(txt_gl_claim_celup.Text) = 0 Then
                txt_gl_claim_celup.Text = "0"
            End If
            txt_gulung.Text = Val(txt_gl_grade_a.Text) + Val(txt_gl_grade_b.Text) + Val(txt_gl_claim_jadi.Text) + Val(txt_gl_claim_celup.Text)
            txt_harga.Focus()
        End If
    End Sub

    Private Sub A1_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A1.Click
        If A1.BackColor = Color.White Then
            A1.BackColor = Color.LightBlue
            AW1.Text = "L"
        ElseIf A1.BackColor = Color.LightBlue Then
            A1.BackColor = Color.Khaki
            AW1.Text = "K"
        ElseIf A1.BackColor = Color.Khaki Then
            A1.BackColor = Color.Pink
            AW1.Text = "P"
        ElseIf A1.BackColor = Color.Pink Then
            A1.BackColor = Color.White
            AW1.Text = "W"
        End If
    End Sub
    Private Sub A2_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A2.Click
        If A2.BackColor = Color.White Then
            A2.BackColor = Color.LightBlue
            AW2.Text = "L"
        ElseIf A2.BackColor = Color.LightBlue Then
            A2.BackColor = Color.Khaki
            AW2.Text = "K"
        ElseIf A2.BackColor = Color.Khaki Then
            A2.BackColor = Color.Pink
            AW2.Text = "P"
        ElseIf A2.BackColor = Color.Pink Then
            A2.BackColor = Color.White
            AW2.Text = "W"
        End If
    End Sub
    Private Sub A3_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A3.Click
        If A3.BackColor = Color.White Then
            A3.BackColor = Color.LightBlue
            AW3.Text = "L"
        ElseIf A3.BackColor = Color.LightBlue Then
            A3.BackColor = Color.Khaki
            AW3.Text = "K"
        ElseIf A3.BackColor = Color.Khaki Then
            A3.BackColor = Color.Pink
            AW3.Text = "P"
        ElseIf A3.BackColor = Color.Pink Then
            A3.BackColor = Color.White
            AW3.Text = "W"
        End If
    End Sub
    Private Sub A4_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A4.Click
        If A4.BackColor = Color.White Then
            A4.BackColor = Color.LightBlue
            AW4.Text = "L"
        ElseIf A4.BackColor = Color.LightBlue Then
            A4.BackColor = Color.Khaki
            AW4.Text = "K"
        ElseIf A4.BackColor = Color.Khaki Then
            A4.BackColor = Color.Pink
            AW4.Text = "P"
        ElseIf A4.BackColor = Color.Pink Then
            A4.BackColor = Color.White
            AW4.Text = "W"
        End If
    End Sub
    Private Sub A5_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A5.Click
        If A5.BackColor = Color.White Then
            A5.BackColor = Color.LightBlue
            AW5.Text = "L"
        ElseIf A5.BackColor = Color.LightBlue Then
            A5.BackColor = Color.Khaki
            AW5.Text = "K"
        ElseIf A5.BackColor = Color.Khaki Then
            A5.BackColor = Color.Pink
            AW5.Text = "P"
        ElseIf A5.BackColor = Color.Pink Then
            A5.BackColor = Color.White
            AW5.Text = "W"
        End If
    End Sub
    Private Sub A6_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A6.Click
        If A6.BackColor = Color.White Then
            A6.BackColor = Color.LightBlue
            AW6.Text = "L"
        ElseIf A6.BackColor = Color.LightBlue Then
            A6.BackColor = Color.Khaki
            AW6.Text = "K"
        ElseIf A6.BackColor = Color.Khaki Then
            A6.BackColor = Color.Pink
            AW6.Text = "P"
        ElseIf A6.BackColor = Color.Pink Then
            A6.BackColor = Color.White
            AW6.Text = "W"
        End If
    End Sub
    Private Sub A7_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A7.Click
        If A7.BackColor = Color.White Then
            A7.BackColor = Color.LightBlue
            AW7.Text = "L"
        ElseIf A7.BackColor = Color.LightBlue Then
            A7.BackColor = Color.Khaki
            AW7.Text = "K"
        ElseIf A7.BackColor = Color.Khaki Then
            A7.BackColor = Color.Pink
            AW7.Text = "P"
        ElseIf A7.BackColor = Color.Pink Then
            A7.BackColor = Color.White
            AW7.Text = "W"
        End If
    End Sub
    Private Sub A8_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A8.Click
        If A8.BackColor = Color.White Then
            A8.BackColor = Color.LightBlue
            AW8.Text = "L"
        ElseIf A8.BackColor = Color.LightBlue Then
            A8.BackColor = Color.Khaki
            AW8.Text = "K"
        ElseIf A8.BackColor = Color.Khaki Then
            A8.BackColor = Color.Pink
            AW8.Text = "P"
        ElseIf A8.BackColor = Color.Pink Then
            A8.BackColor = Color.White
            AW8.Text = "W"
        End If
    End Sub
    Private Sub A9_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A9.Click
        If A9.BackColor = Color.White Then
            A9.BackColor = Color.LightBlue
            AW9.Text = "L"
        ElseIf A9.BackColor = Color.LightBlue Then
            A9.BackColor = Color.Khaki
            AW9.Text = "K"
        ElseIf A9.BackColor = Color.Khaki Then
            A9.BackColor = Color.Pink
            AW9.Text = "P"
        ElseIf A9.BackColor = Color.Pink Then
            A9.BackColor = Color.White
            AW9.Text = "W"
        End If
    End Sub
    Private Sub A10_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A10.Click
        If A10.BackColor = Color.White Then
            A10.BackColor = Color.LightBlue
            AW10.Text = "L"
        ElseIf A10.BackColor = Color.LightBlue Then
            A10.BackColor = Color.Khaki
            AW10.Text = "K"
        ElseIf A10.BackColor = Color.Khaki Then
            A10.BackColor = Color.Pink
            AW10.Text = "P"
        ElseIf A10.BackColor = Color.Pink Then
            A10.BackColor = Color.White
            AW10.Text = "W"
        End If
    End Sub
    Private Sub A11_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A11.Click
        If A11.BackColor = Color.White Then
            A11.BackColor = Color.LightBlue
            AW11.Text = "L"
        ElseIf A11.BackColor = Color.LightBlue Then
            A11.BackColor = Color.Khaki
            AW11.Text = "K"
        ElseIf A11.BackColor = Color.Khaki Then
            A11.BackColor = Color.Pink
            AW11.Text = "P"
        ElseIf A11.BackColor = Color.Pink Then
            A11.BackColor = Color.White
            AW11.Text = "W"
        End If
    End Sub
    Private Sub A12_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A12.Click
        If A12.BackColor = Color.White Then
            A12.BackColor = Color.LightBlue
            AW12.Text = "L"
        ElseIf A12.BackColor = Color.LightBlue Then
            A12.BackColor = Color.Khaki
            AW12.Text = "K"
        ElseIf A12.BackColor = Color.Khaki Then
            A12.BackColor = Color.Pink
            AW12.Text = "P"
        ElseIf A12.BackColor = Color.Pink Then
            A12.BackColor = Color.White
            AW12.Text = "W"
        End If
    End Sub
    Private Sub A13_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A13.Click
        If A13.BackColor = Color.White Then
            A13.BackColor = Color.LightBlue
            AW13.Text = "L"
        ElseIf A13.BackColor = Color.LightBlue Then
            A13.BackColor = Color.Khaki
            AW13.Text = "K"
        ElseIf A13.BackColor = Color.Khaki Then
            A13.BackColor = Color.Pink
            AW13.Text = "P"
        ElseIf A13.BackColor = Color.Pink Then
            A13.BackColor = Color.White
            AW13.Text = "W"
        End If
    End Sub
    Private Sub A14_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A14.Click
        If A14.BackColor = Color.White Then
            A14.BackColor = Color.LightBlue
            AW14.Text = "L"
        ElseIf A14.BackColor = Color.LightBlue Then
            A14.BackColor = Color.Khaki
            AW14.Text = "K"
        ElseIf A14.BackColor = Color.Khaki Then
            A14.BackColor = Color.Pink
            AW14.Text = "P"
        ElseIf A14.BackColor = Color.Pink Then
            A14.BackColor = Color.White
            AW14.Text = "W"
        End If
    End Sub
    Private Sub A15_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A15.Click
        If A15.BackColor = Color.White Then
            A15.BackColor = Color.LightBlue
            AW15.Text = "L"
        ElseIf A15.BackColor = Color.LightBlue Then
            A15.BackColor = Color.Khaki
            AW15.Text = "K"
        ElseIf A15.BackColor = Color.Khaki Then
            A15.BackColor = Color.Pink
            AW15.Text = "P"
        ElseIf A15.BackColor = Color.Pink Then
            A15.BackColor = Color.White
            AW15.Text = "W"
        End If
    End Sub
    Private Sub A16_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A16.Click
        If A16.BackColor = Color.White Then
            A16.BackColor = Color.LightBlue
            AW16.Text = "L"
        ElseIf A16.BackColor = Color.LightBlue Then
            A16.BackColor = Color.Khaki
            AW16.Text = "K"
        ElseIf A16.BackColor = Color.Khaki Then
            A16.BackColor = Color.Pink
            AW16.Text = "P"
        ElseIf A16.BackColor = Color.Pink Then
            A16.BackColor = Color.White
            AW16.Text = "W"
        End If
    End Sub
    Private Sub A17_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A17.Click
        If A17.BackColor = Color.White Then
            A17.BackColor = Color.LightBlue
            AW17.Text = "L"
        ElseIf A17.BackColor = Color.LightBlue Then
            A17.BackColor = Color.Khaki
            AW17.Text = "K"
        ElseIf A17.BackColor = Color.Khaki Then
            A17.BackColor = Color.Pink
            AW17.Text = "P"
        ElseIf A17.BackColor = Color.Pink Then
            A17.BackColor = Color.White
            AW17.Text = "W"
        End If
    End Sub
    Private Sub A18_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A18.Click
        If A18.BackColor = Color.White Then
            A18.BackColor = Color.LightBlue
            AW18.Text = "L"
        ElseIf A18.BackColor = Color.LightBlue Then
            A18.BackColor = Color.Khaki
            AW18.Text = "K"
        ElseIf A18.BackColor = Color.Khaki Then
            A18.BackColor = Color.Pink
            AW18.Text = "P"
        ElseIf A18.BackColor = Color.Pink Then
            A18.BackColor = Color.White
            AW18.Text = "W"
        End If
    End Sub
    Private Sub A19_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A19.Click
        If A19.BackColor = Color.White Then
            A19.BackColor = Color.LightBlue
            AW19.Text = "L"
        ElseIf A19.BackColor = Color.LightBlue Then
            A19.BackColor = Color.Khaki
            AW19.Text = "K"
        ElseIf A19.BackColor = Color.Khaki Then
            A19.BackColor = Color.Pink
            AW19.Text = "P"
        ElseIf A19.BackColor = Color.Pink Then
            A19.BackColor = Color.White
            AW19.Text = "W"
        End If
    End Sub
    Private Sub A20_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A20.Click
        If A20.BackColor = Color.White Then
            A20.BackColor = Color.LightBlue
            AW20.Text = "L"
        ElseIf A20.BackColor = Color.LightBlue Then
            A20.BackColor = Color.Khaki
            AW20.Text = "K"
        ElseIf A20.BackColor = Color.Khaki Then
            A20.BackColor = Color.Pink
            AW20.Text = "P"
        ElseIf A20.BackColor = Color.Pink Then
            A20.BackColor = Color.White
            AW20.Text = "W"
        End If
    End Sub
    Private Sub A21_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A21.Click
        If A21.BackColor = Color.White Then
            A21.BackColor = Color.LightBlue
            AW21.Text = "L"
        ElseIf A21.BackColor = Color.LightBlue Then
            A21.BackColor = Color.Khaki
            AW21.Text = "K"
        ElseIf A21.BackColor = Color.Khaki Then
            A21.BackColor = Color.Pink
            AW21.Text = "P"
        ElseIf A21.BackColor = Color.Pink Then
            A21.BackColor = Color.White
            AW21.Text = "W"
        End If
    End Sub
    Private Sub A22_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A22.Click
        If A22.BackColor = Color.White Then
            A22.BackColor = Color.LightBlue
            AW22.Text = "L"
        ElseIf A22.BackColor = Color.LightBlue Then
            A22.BackColor = Color.Khaki
            AW22.Text = "K"
        ElseIf A22.BackColor = Color.Khaki Then
            A22.BackColor = Color.Pink
            AW22.Text = "P"
        ElseIf A22.BackColor = Color.Pink Then
            A22.BackColor = Color.White
            AW22.Text = "W"
        End If
    End Sub
    Private Sub A23_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A23.Click
        If A23.BackColor = Color.White Then
            A23.BackColor = Color.LightBlue
            AW23.Text = "L"
        ElseIf A23.BackColor = Color.LightBlue Then
            A23.BackColor = Color.Khaki
            AW23.Text = "K"
        ElseIf A23.BackColor = Color.Khaki Then
            A23.BackColor = Color.Pink
            AW23.Text = "P"
        ElseIf A23.BackColor = Color.Pink Then
            A23.BackColor = Color.White
            AW23.Text = "W"
        End If
    End Sub
    Private Sub A24_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A24.Click
        If A24.BackColor = Color.White Then
            A24.BackColor = Color.LightBlue
            AW24.Text = "L"
        ElseIf A24.BackColor = Color.LightBlue Then
            A24.BackColor = Color.Khaki
            AW24.Text = "K"
        ElseIf A24.BackColor = Color.Khaki Then
            A24.BackColor = Color.Pink
            AW24.Text = "P"
        ElseIf A24.BackColor = Color.Pink Then
            A24.BackColor = Color.White
            AW24.Text = "W"
        End If
    End Sub
    Private Sub A25_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles A25.Click
        If A25.BackColor = Color.White Then
            A25.BackColor = Color.LightBlue
            AW25.Text = "L"
        ElseIf A25.BackColor = Color.LightBlue Then
            A25.BackColor = Color.Khaki
            AW25.Text = "K"
        ElseIf A25.BackColor = Color.Khaki Then
            A25.BackColor = Color.Pink
            AW25.Text = "P"
        ElseIf A25.BackColor = Color.Pink Then
            A25.BackColor = Color.White
            AW25.Text = "W"
        End If
    End Sub

    Private Sub B1_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B1.Click
        If B1.BackColor = Color.White Then
            B1.BackColor = Color.LightBlue
            BW1.Text = "L"
        ElseIf B1.BackColor = Color.LightBlue Then
            B1.BackColor = Color.Khaki
            BW1.Text = "K"
        ElseIf B1.BackColor = Color.Khaki Then
            B1.BackColor = Color.Pink
            BW1.Text = "P"
        ElseIf B1.BackColor = Color.Pink Then
            B1.BackColor = Color.White
            BW1.Text = "W"
        End If
    End Sub
    Private Sub B2_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B2.Click
        If B2.BackColor = Color.White Then
            B2.BackColor = Color.LightBlue
            BW2.Text = "L"
        ElseIf B2.BackColor = Color.LightBlue Then
            B2.BackColor = Color.Khaki
            BW2.Text = "K"
        ElseIf B2.BackColor = Color.Khaki Then
            B2.BackColor = Color.Pink
            BW2.Text = "P"
        ElseIf B2.BackColor = Color.Pink Then
            B2.BackColor = Color.White
            BW2.Text = "W"
        End If
    End Sub
    Private Sub B3_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B3.Click
        If B3.BackColor = Color.White Then
            B3.BackColor = Color.LightBlue
            BW3.Text = "L"
        ElseIf B3.BackColor = Color.LightBlue Then
            B3.BackColor = Color.Khaki
            BW3.Text = "K"
        ElseIf B3.BackColor = Color.Khaki Then
            B3.BackColor = Color.Pink
            BW3.Text = "P"
        ElseIf B3.BackColor = Color.Pink Then
            B3.BackColor = Color.White
            BW3.Text = "W"
        End If
    End Sub
    Private Sub B4_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B4.Click
        If B4.BackColor = Color.White Then
            B4.BackColor = Color.LightBlue
            BW4.Text = "L"
        ElseIf B4.BackColor = Color.LightBlue Then
            B4.BackColor = Color.Khaki
            BW4.Text = "K"
        ElseIf B4.BackColor = Color.Khaki Then
            B4.BackColor = Color.Pink
            BW4.Text = "P"
        ElseIf B4.BackColor = Color.Pink Then
            B4.BackColor = Color.White
            BW4.Text = "W"
        End If
    End Sub
    Private Sub B5_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B5.Click
        If B5.BackColor = Color.White Then
            B5.BackColor = Color.LightBlue
            BW5.Text = "L"
        ElseIf B5.BackColor = Color.LightBlue Then
            B5.BackColor = Color.Khaki
            BW5.Text = "K"
        ElseIf B5.BackColor = Color.Khaki Then
            B5.BackColor = Color.Pink
            BW5.Text = "P"
        ElseIf B5.BackColor = Color.Pink Then
            B5.BackColor = Color.White
            BW5.Text = "W"
        End If
    End Sub
    Private Sub B6_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B6.Click
        If B6.BackColor = Color.White Then
            B6.BackColor = Color.LightBlue
            BW6.Text = "L"
        ElseIf B6.BackColor = Color.LightBlue Then
            B6.BackColor = Color.Khaki
            BW6.Text = "K"
        ElseIf B6.BackColor = Color.Khaki Then
            B6.BackColor = Color.Pink
            BW6.Text = "P"
        ElseIf B6.BackColor = Color.Pink Then
            B6.BackColor = Color.White
            BW6.Text = "W"
        End If
    End Sub
    Private Sub B7_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B7.Click
        If B7.BackColor = Color.White Then
            B7.BackColor = Color.LightBlue
            BW7.Text = "L"
        ElseIf B7.BackColor = Color.LightBlue Then
            B7.BackColor = Color.Khaki
            BW7.Text = "K"
        ElseIf B7.BackColor = Color.Khaki Then
            B7.BackColor = Color.Pink
            BW7.Text = "P"
        ElseIf B7.BackColor = Color.Pink Then
            B7.BackColor = Color.White
            BW7.Text = "W"
        End If
    End Sub
    Private Sub B8_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B8.Click
        If B8.BackColor = Color.White Then
            B8.BackColor = Color.LightBlue
            BW8.Text = "L"
        ElseIf B8.BackColor = Color.LightBlue Then
            B8.BackColor = Color.Khaki
            BW8.Text = "K"
        ElseIf B8.BackColor = Color.Khaki Then
            B8.BackColor = Color.Pink
            BW8.Text = "P"
        ElseIf B8.BackColor = Color.Pink Then
            B8.BackColor = Color.White
            BW8.Text = "W"
        End If
    End Sub
    Private Sub B9_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B9.Click
        If B9.BackColor = Color.White Then
            B9.BackColor = Color.LightBlue
            BW9.Text = "L"
        ElseIf B9.BackColor = Color.LightBlue Then
            B9.BackColor = Color.Khaki
            BW9.Text = "K"
        ElseIf B9.BackColor = Color.Khaki Then
            B9.BackColor = Color.Pink
            BW9.Text = "P"
        ElseIf B9.BackColor = Color.Pink Then
            B9.BackColor = Color.White
            BW9.Text = "W"
        End If
    End Sub
    Private Sub B10_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B10.Click
        If B10.BackColor = Color.White Then
            B10.BackColor = Color.LightBlue
            BW10.Text = "L"
        ElseIf B10.BackColor = Color.LightBlue Then
            B10.BackColor = Color.Khaki
            BW10.Text = "K"
        ElseIf B10.BackColor = Color.Khaki Then
            B10.BackColor = Color.Pink
            BW10.Text = "P"
        ElseIf B10.BackColor = Color.Pink Then
            B10.BackColor = Color.White
            BW10.Text = "W"
        End If
    End Sub
    Private Sub B11_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B11.Click
        If B11.BackColor = Color.White Then
            B11.BackColor = Color.LightBlue
            BW11.Text = "L"
        ElseIf B11.BackColor = Color.LightBlue Then
            B11.BackColor = Color.Khaki
            BW11.Text = "K"
        ElseIf B11.BackColor = Color.Khaki Then
            B11.BackColor = Color.Pink
            BW11.Text = "P"
        ElseIf B11.BackColor = Color.Pink Then
            B11.BackColor = Color.White
            BW11.Text = "W"
        End If
    End Sub
    Private Sub B12_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B12.Click
        If B12.BackColor = Color.White Then
            B12.BackColor = Color.LightBlue
            BW12.Text = "L"
        ElseIf B12.BackColor = Color.LightBlue Then
            B12.BackColor = Color.Khaki
            BW12.Text = "K"
        ElseIf B12.BackColor = Color.Khaki Then
            B12.BackColor = Color.Pink
            BW12.Text = "P"
        ElseIf B12.BackColor = Color.Pink Then
            B12.BackColor = Color.White
            BW12.Text = "W"
        End If
    End Sub
    Private Sub B13_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B13.Click
        If B13.BackColor = Color.White Then
            B13.BackColor = Color.LightBlue
            BW13.Text = "L"
        ElseIf B13.BackColor = Color.LightBlue Then
            B13.BackColor = Color.Khaki
            BW13.Text = "K"
        ElseIf B13.BackColor = Color.Khaki Then
            B13.BackColor = Color.Pink
            BW13.Text = "P"
        ElseIf B13.BackColor = Color.Pink Then
            B13.BackColor = Color.White
            BW13.Text = "W"
        End If
    End Sub
    Private Sub B14_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B14.Click
        If B14.BackColor = Color.White Then
            B14.BackColor = Color.LightBlue
            BW14.Text = "L"
        ElseIf B14.BackColor = Color.LightBlue Then
            B14.BackColor = Color.Khaki
            BW14.Text = "K"
        ElseIf B14.BackColor = Color.Khaki Then
            B14.BackColor = Color.Pink
            BW14.Text = "P"
        ElseIf B14.BackColor = Color.Pink Then
            B14.BackColor = Color.White
            BW14.Text = "W"
        End If
    End Sub
    Private Sub B15_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B15.Click
        If B15.BackColor = Color.White Then
            B15.BackColor = Color.LightBlue
            BW15.Text = "L"
        ElseIf B15.BackColor = Color.LightBlue Then
            B15.BackColor = Color.Khaki
            BW15.Text = "K"
        ElseIf B15.BackColor = Color.Khaki Then
            B15.BackColor = Color.Pink
            BW15.Text = "P"
        ElseIf B15.BackColor = Color.Pink Then
            B15.BackColor = Color.White
            BW15.Text = "W"
        End If
    End Sub
    Private Sub B16_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B16.Click
        If B16.BackColor = Color.White Then
            B16.BackColor = Color.LightBlue
            BW16.Text = "L"
        ElseIf B16.BackColor = Color.LightBlue Then
            B16.BackColor = Color.Khaki
            BW16.Text = "K"
        ElseIf B16.BackColor = Color.Khaki Then
            B16.BackColor = Color.Pink
            BW16.Text = "P"
        ElseIf B16.BackColor = Color.Pink Then
            B16.BackColor = Color.White
            BW16.Text = "W"
        End If
    End Sub
    Private Sub B17_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B17.Click
        If B17.BackColor = Color.White Then
            B17.BackColor = Color.LightBlue
            BW17.Text = "L"
        ElseIf B17.BackColor = Color.LightBlue Then
            B17.BackColor = Color.Khaki
            BW17.Text = "K"
        ElseIf B17.BackColor = Color.Khaki Then
            B17.BackColor = Color.Pink
            BW17.Text = "P"
        ElseIf B17.BackColor = Color.Pink Then
            B17.BackColor = Color.White
            BW17.Text = "W"
        End If
    End Sub
    Private Sub B18_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B18.Click
        If B18.BackColor = Color.White Then
            B18.BackColor = Color.LightBlue
            BW18.Text = "L"
        ElseIf B18.BackColor = Color.LightBlue Then
            B18.BackColor = Color.Khaki
            BW18.Text = "K"
        ElseIf B18.BackColor = Color.Khaki Then
            B18.BackColor = Color.Pink
            BW18.Text = "P"
        ElseIf B18.BackColor = Color.Pink Then
            B18.BackColor = Color.White
            BW18.Text = "W"
        End If
    End Sub
    Private Sub B19_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B19.Click
        If B19.BackColor = Color.White Then
            B19.BackColor = Color.LightBlue
            BW19.Text = "L"
        ElseIf B19.BackColor = Color.LightBlue Then
            B19.BackColor = Color.Khaki
            BW19.Text = "K"
        ElseIf B19.BackColor = Color.Khaki Then
            B19.BackColor = Color.Pink
            BW19.Text = "P"
        ElseIf B19.BackColor = Color.Pink Then
            B19.BackColor = Color.White
            BW19.Text = "W"
        End If
    End Sub
    Private Sub B20_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B20.Click
        If B20.BackColor = Color.White Then
            B20.BackColor = Color.LightBlue
            BW20.Text = "L"
        ElseIf B20.BackColor = Color.LightBlue Then
            B20.BackColor = Color.Khaki
            BW20.Text = "K"
        ElseIf B20.BackColor = Color.Khaki Then
            B20.BackColor = Color.Pink
            BW20.Text = "P"
        ElseIf B20.BackColor = Color.Pink Then
            B20.BackColor = Color.White
            BW20.Text = "W"
        End If
    End Sub
    Private Sub B21_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B21.Click
        If B21.BackColor = Color.White Then
            B21.BackColor = Color.LightBlue
            BW21.Text = "L"
        ElseIf B21.BackColor = Color.LightBlue Then
            B21.BackColor = Color.Khaki
            BW21.Text = "K"
        ElseIf B21.BackColor = Color.Khaki Then
            B21.BackColor = Color.Pink
            BW21.Text = "P"
        ElseIf B21.BackColor = Color.Pink Then
            B21.BackColor = Color.White
            BW21.Text = "W"
        End If
    End Sub
    Private Sub B22_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B22.Click
        If B22.BackColor = Color.White Then
            B22.BackColor = Color.LightBlue
            BW22.Text = "L"
        ElseIf B22.BackColor = Color.LightBlue Then
            B22.BackColor = Color.Khaki
            BW22.Text = "K"
        ElseIf B22.BackColor = Color.Khaki Then
            B22.BackColor = Color.Pink
            BW22.Text = "P"
        ElseIf B22.BackColor = Color.Pink Then
            B22.BackColor = Color.White
            BW22.Text = "W"
        End If
    End Sub
    Private Sub B23_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B23.Click
        If B23.BackColor = Color.White Then
            B23.BackColor = Color.LightBlue
            BW23.Text = "L"
        ElseIf B23.BackColor = Color.LightBlue Then
            B23.BackColor = Color.Khaki
            BW23.Text = "K"
        ElseIf B23.BackColor = Color.Khaki Then
            B23.BackColor = Color.Pink
            BW23.Text = "P"
        ElseIf B23.BackColor = Color.Pink Then
            B23.BackColor = Color.White
            BW23.Text = "W"
        End If
    End Sub
    Private Sub B24_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B24.Click
        If B24.BackColor = Color.White Then
            B24.BackColor = Color.LightBlue
            BW24.Text = "L"
        ElseIf B24.BackColor = Color.LightBlue Then
            B24.BackColor = Color.Khaki
            BW24.Text = "K"
        ElseIf B24.BackColor = Color.Khaki Then
            B24.BackColor = Color.Pink
            BW24.Text = "P"
        ElseIf B24.BackColor = Color.Pink Then
            B24.BackColor = Color.White
            BW24.Text = "W"
        End If
    End Sub
    Private Sub B25_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles B25.Click
        If B25.BackColor = Color.White Then
            B25.BackColor = Color.LightBlue
            BW25.Text = "L"
        ElseIf B25.BackColor = Color.LightBlue Then
            B25.BackColor = Color.Khaki
            BW25.Text = "K"
        ElseIf B25.BackColor = Color.Khaki Then
            B25.BackColor = Color.Pink
            BW25.Text = "P"
        ElseIf B25.BackColor = Color.Pink Then
            B25.BackColor = Color.White
            BW25.Text = "W"
        End If
    End Sub

    Private Sub C1_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C1.Click
        If C1.BackColor = Color.White Then
            C1.BackColor = Color.LightBlue
            CW1.Text = "L"
        ElseIf C1.BackColor = Color.LightBlue Then
            C1.BackColor = Color.Khaki
            CW1.Text = "K"
        ElseIf C1.BackColor = Color.Khaki Then
            C1.BackColor = Color.Pink
            CW1.Text = "P"
        ElseIf C1.BackColor = Color.Pink Then
            C1.BackColor = Color.White
            CW1.Text = "W"
        End If
    End Sub
    Private Sub C2_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C2.Click
        If C2.BackColor = Color.White Then
            C2.BackColor = Color.LightBlue
            CW2.Text = "L"
        ElseIf C2.BackColor = Color.LightBlue Then
            C2.BackColor = Color.Khaki
            CW2.Text = "K"
        ElseIf C2.BackColor = Color.Khaki Then
            C2.BackColor = Color.Pink
            CW2.Text = "P"
        ElseIf C2.BackColor = Color.Pink Then
            C2.BackColor = Color.White
            CW2.Text = "W"
        End If
    End Sub
    Private Sub C3_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C3.Click
        If C3.BackColor = Color.White Then
            C3.BackColor = Color.LightBlue
            CW3.Text = "L"
        ElseIf C3.BackColor = Color.LightBlue Then
            C3.BackColor = Color.Khaki
            CW3.Text = "K"
        ElseIf C3.BackColor = Color.Khaki Then
            C3.BackColor = Color.Pink
            CW3.Text = "P"
        ElseIf C3.BackColor = Color.Pink Then
            C3.BackColor = Color.White
            CW3.Text = "W"
        End If
    End Sub
    Private Sub C4_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C4.Click
        If C4.BackColor = Color.White Then
            C4.BackColor = Color.LightBlue
            CW4.Text = "L"
        ElseIf C4.BackColor = Color.LightBlue Then
            C4.BackColor = Color.Khaki
            CW4.Text = "K"
        ElseIf C4.BackColor = Color.Khaki Then
            C4.BackColor = Color.Pink
            CW4.Text = "P"
        ElseIf C4.BackColor = Color.Pink Then
            C4.BackColor = Color.White
            CW4.Text = "W"
        End If
    End Sub
    Private Sub C5_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C5.Click
        If C5.BackColor = Color.White Then
            C5.BackColor = Color.LightBlue
            CW5.Text = "L"
        ElseIf C5.BackColor = Color.LightBlue Then
            C5.BackColor = Color.Khaki
            CW5.Text = "K"
        ElseIf C5.BackColor = Color.Khaki Then
            C5.BackColor = Color.Pink
            CW5.Text = "P"
        ElseIf C5.BackColor = Color.Pink Then
            C5.BackColor = Color.White
            CW5.Text = "W"
        End If
    End Sub
    Private Sub C6_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C6.Click
        If C6.BackColor = Color.White Then
            C6.BackColor = Color.LightBlue
            CW6.Text = "L"
        ElseIf C6.BackColor = Color.LightBlue Then
            C6.BackColor = Color.Khaki
            CW6.Text = "K"
        ElseIf C6.BackColor = Color.Khaki Then
            C6.BackColor = Color.Pink
            CW6.Text = "P"
        ElseIf C6.BackColor = Color.Pink Then
            C6.BackColor = Color.White
            CW6.Text = "W"
        End If
    End Sub
    Private Sub C7_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C7.Click
        If C7.BackColor = Color.White Then
            C7.BackColor = Color.LightBlue
            CW7.Text = "L"
        ElseIf C7.BackColor = Color.LightBlue Then
            C7.BackColor = Color.Khaki
            CW7.Text = "K"
        ElseIf C7.BackColor = Color.Khaki Then
            C7.BackColor = Color.Pink
            CW7.Text = "P"
        ElseIf C7.BackColor = Color.Pink Then
            C7.BackColor = Color.White
            CW7.Text = "W"
        End If
    End Sub
    Private Sub C8_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C8.Click
        If C8.BackColor = Color.White Then
            C8.BackColor = Color.LightBlue
            CW8.Text = "L"
        ElseIf C8.BackColor = Color.LightBlue Then
            C8.BackColor = Color.Khaki
            CW8.Text = "K"
        ElseIf C8.BackColor = Color.Khaki Then
            C8.BackColor = Color.Pink
            CW8.Text = "P"
        ElseIf C8.BackColor = Color.Pink Then
            C8.BackColor = Color.White
            CW8.Text = "W"
        End If
    End Sub
    Private Sub C9_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C9.Click
        If C9.BackColor = Color.White Then
            C9.BackColor = Color.LightBlue
            CW9.Text = "L"
        ElseIf C9.BackColor = Color.LightBlue Then
            C9.BackColor = Color.Khaki
            CW9.Text = "K"
        ElseIf C9.BackColor = Color.Khaki Then
            C9.BackColor = Color.Pink
            CW9.Text = "P"
        ElseIf C9.BackColor = Color.Pink Then
            C9.BackColor = Color.White
            CW9.Text = "W"
        End If
    End Sub
    Private Sub C10_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C10.Click
        If C10.BackColor = Color.White Then
            C10.BackColor = Color.LightBlue
            CW10.Text = "L"
        ElseIf C10.BackColor = Color.LightBlue Then
            C10.BackColor = Color.Khaki
            CW10.Text = "K"
        ElseIf C10.BackColor = Color.Khaki Then
            C10.BackColor = Color.Pink
            CW10.Text = "P"
        ElseIf C10.BackColor = Color.Pink Then
            C10.BackColor = Color.White
            CW10.Text = "W"
        End If
    End Sub
    Private Sub C11_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C11.Click
        If C11.BackColor = Color.White Then
            C11.BackColor = Color.LightBlue
            CW11.Text = "L"
        ElseIf C11.BackColor = Color.LightBlue Then
            C11.BackColor = Color.Khaki
            CW11.Text = "K"
        ElseIf C11.BackColor = Color.Khaki Then
            C11.BackColor = Color.Pink
            CW11.Text = "P"
        ElseIf C11.BackColor = Color.Pink Then
            C11.BackColor = Color.White
            CW11.Text = "W"
        End If
    End Sub
    Private Sub C12_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C12.Click
        If C12.BackColor = Color.White Then
            C12.BackColor = Color.LightBlue
            CW12.Text = "L"
        ElseIf C12.BackColor = Color.LightBlue Then
            C12.BackColor = Color.Khaki
            CW12.Text = "K"
        ElseIf C12.BackColor = Color.Khaki Then
            C12.BackColor = Color.Pink
            CW12.Text = "P"
        ElseIf C12.BackColor = Color.Pink Then
            C12.BackColor = Color.White
            CW12.Text = "W"
        End If
    End Sub
    Private Sub C13_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C13.Click
        If C13.BackColor = Color.White Then
            C13.BackColor = Color.LightBlue
            CW13.Text = "L"
        ElseIf C13.BackColor = Color.LightBlue Then
            C13.BackColor = Color.Khaki
            CW13.Text = "K"
        ElseIf C13.BackColor = Color.Khaki Then
            C13.BackColor = Color.Pink
            CW13.Text = "P"
        ElseIf C13.BackColor = Color.Pink Then
            C13.BackColor = Color.White
            CW13.Text = "W"
        End If
    End Sub
    Private Sub C14_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C14.Click
        If C14.BackColor = Color.White Then
            C14.BackColor = Color.LightBlue
            CW14.Text = "L"
        ElseIf C14.BackColor = Color.LightBlue Then
            C14.BackColor = Color.Khaki
            CW14.Text = "K"
        ElseIf C14.BackColor = Color.Khaki Then
            C14.BackColor = Color.Pink
            CW14.Text = "P"
        ElseIf C14.BackColor = Color.Pink Then
            C14.BackColor = Color.White
            CW14.Text = "W"
        End If
    End Sub
    Private Sub C15_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C15.Click
        If C15.BackColor = Color.White Then
            C15.BackColor = Color.LightBlue
            CW15.Text = "L"
        ElseIf C15.BackColor = Color.LightBlue Then
            C15.BackColor = Color.Khaki
            CW15.Text = "K"
        ElseIf C15.BackColor = Color.Khaki Then
            C15.BackColor = Color.Pink
            CW15.Text = "P"
        ElseIf C15.BackColor = Color.Pink Then
            C15.BackColor = Color.White
            CW15.Text = "W"
        End If
    End Sub
    Private Sub C16_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C16.Click
        If C16.BackColor = Color.White Then
            C16.BackColor = Color.LightBlue
            CW16.Text = "L"
        ElseIf C16.BackColor = Color.LightBlue Then
            C16.BackColor = Color.Khaki
            CW16.Text = "K"
        ElseIf C16.BackColor = Color.Khaki Then
            C16.BackColor = Color.Pink
            CW16.Text = "P"
        ElseIf C16.BackColor = Color.Pink Then
            C16.BackColor = Color.White
            CW16.Text = "W"
        End If
    End Sub
    Private Sub C17_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C17.Click
        If C17.BackColor = Color.White Then
            C17.BackColor = Color.LightBlue
            CW17.Text = "L"
        ElseIf C17.BackColor = Color.LightBlue Then
            C17.BackColor = Color.Khaki
            CW17.Text = "K"
        ElseIf C17.BackColor = Color.Khaki Then
            C17.BackColor = Color.Pink
            CW17.Text = "P"
        ElseIf C17.BackColor = Color.Pink Then
            C17.BackColor = Color.White
            CW17.Text = "W"
        End If
    End Sub
    Private Sub C18_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C18.Click
        If C18.BackColor = Color.White Then
            C18.BackColor = Color.LightBlue
            CW18.Text = "L"
        ElseIf C18.BackColor = Color.LightBlue Then
            C18.BackColor = Color.Khaki
            CW18.Text = "K"
        ElseIf C18.BackColor = Color.Khaki Then
            C18.BackColor = Color.Pink
            CW18.Text = "P"
        ElseIf C18.BackColor = Color.Pink Then
            C18.BackColor = Color.White
            CW18.Text = "W"
        End If
    End Sub
    Private Sub C19_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C19.Click
        If C19.BackColor = Color.White Then
            C19.BackColor = Color.LightBlue
            CW19.Text = "L"
        ElseIf C19.BackColor = Color.LightBlue Then
            C19.BackColor = Color.Khaki
            CW19.Text = "K"
        ElseIf C19.BackColor = Color.Khaki Then
            C19.BackColor = Color.Pink
            CW19.Text = "P"
        ElseIf C19.BackColor = Color.Pink Then
            C19.BackColor = Color.White
            CW19.Text = "W"
        End If
    End Sub
    Private Sub C20_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C20.Click
        If C20.BackColor = Color.White Then
            C20.BackColor = Color.LightBlue
            CW20.Text = "L"
        ElseIf C20.BackColor = Color.LightBlue Then
            C20.BackColor = Color.Khaki
            CW20.Text = "K"
        ElseIf C20.BackColor = Color.Khaki Then
            C20.BackColor = Color.Pink
            CW20.Text = "P"
        ElseIf C20.BackColor = Color.Pink Then
            C20.BackColor = Color.White
            CW20.Text = "W"
        End If
    End Sub
    Private Sub C21_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C21.Click
        If C21.BackColor = Color.White Then
            C21.BackColor = Color.LightBlue
            CW21.Text = "L"
        ElseIf C21.BackColor = Color.LightBlue Then
            C21.BackColor = Color.Khaki
            CW21.Text = "K"
        ElseIf C21.BackColor = Color.Khaki Then
            C21.BackColor = Color.Pink
            CW21.Text = "P"
        ElseIf C21.BackColor = Color.Pink Then
            C21.BackColor = Color.White
            CW21.Text = "W"
        End If
    End Sub
    Private Sub C22_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C22.Click
        If C22.BackColor = Color.White Then
            C22.BackColor = Color.LightBlue
            CW22.Text = "L"
        ElseIf C22.BackColor = Color.LightBlue Then
            C22.BackColor = Color.Khaki
            CW22.Text = "K"
        ElseIf C22.BackColor = Color.Khaki Then
            C22.BackColor = Color.Pink
            CW22.Text = "P"
        ElseIf C22.BackColor = Color.Pink Then
            C22.BackColor = Color.White
            CW22.Text = "W"
        End If
    End Sub
    Private Sub C23_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C23.Click
        If C23.BackColor = Color.White Then
            C23.BackColor = Color.LightBlue
            CW23.Text = "L"
        ElseIf C23.BackColor = Color.LightBlue Then
            C23.BackColor = Color.Khaki
            CW23.Text = "K"
        ElseIf C23.BackColor = Color.Khaki Then
            C23.BackColor = Color.Pink
            CW23.Text = "P"
        ElseIf C23.BackColor = Color.Pink Then
            C23.BackColor = Color.White
            CW23.Text = "W"
        End If
    End Sub
    Private Sub C24_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C24.Click
        If C24.BackColor = Color.White Then
            C24.BackColor = Color.LightBlue
            CW24.Text = "L"
        ElseIf C24.BackColor = Color.LightBlue Then
            C24.BackColor = Color.Khaki
            CW24.Text = "K"
        ElseIf C24.BackColor = Color.Khaki Then
            C24.BackColor = Color.Pink
            CW24.Text = "P"
        ElseIf C24.BackColor = Color.Pink Then
            C24.BackColor = Color.White
            CW24.Text = "W"
        End If
    End Sub
    Private Sub C25_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles C25.Click
        If C25.BackColor = Color.White Then
            C25.BackColor = Color.LightBlue
            CW25.Text = "L"
        ElseIf C25.BackColor = Color.LightBlue Then
            C25.BackColor = Color.Khaki
            CW25.Text = "K"
        ElseIf C25.BackColor = Color.Khaki Then
            C25.BackColor = Color.Pink
            CW25.Text = "P"
        ElseIf C25.BackColor = Color.Pink Then
            C25.BackColor = Color.White
            CW25.Text = "W"
        End If
    End Sub

    Private Sub D1_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D1.Click
        If D1.BackColor = Color.White Then
            D1.BackColor = Color.LightBlue
            DW1.Text = "L"
        ElseIf D1.BackColor = Color.LightBlue Then
            D1.BackColor = Color.Khaki
            DW1.Text = "K"
        ElseIf D1.BackColor = Color.Khaki Then
            D1.BackColor = Color.Pink
            DW1.Text = "P"
        ElseIf D1.BackColor = Color.Pink Then
            D1.BackColor = Color.White
            DW1.Text = "W"
        End If
    End Sub
    Private Sub D2_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D2.Click
        If D2.BackColor = Color.White Then
            D2.BackColor = Color.LightBlue
            DW2.Text = "L"
        ElseIf D2.BackColor = Color.LightBlue Then
            D2.BackColor = Color.Khaki
            DW2.Text = "K"
        ElseIf D2.BackColor = Color.Khaki Then
            D2.BackColor = Color.Pink
            DW2.Text = "P"
        ElseIf D2.BackColor = Color.Pink Then
            D2.BackColor = Color.White
            DW2.Text = "W"
        End If
    End Sub
    Private Sub D3_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D3.Click
        If D3.BackColor = Color.White Then
            D3.BackColor = Color.LightBlue
            DW3.Text = "L"
        ElseIf D3.BackColor = Color.LightBlue Then
            D3.BackColor = Color.Khaki
            DW3.Text = "K"
        ElseIf D3.BackColor = Color.Khaki Then
            D3.BackColor = Color.Pink
            DW3.Text = "P"
        ElseIf D3.BackColor = Color.Pink Then
            D3.BackColor = Color.White
            DW3.Text = "W"
        End If
    End Sub
    Private Sub D4_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D4.Click
        If D4.BackColor = Color.White Then
            D4.BackColor = Color.LightBlue
            DW4.Text = "L"
        ElseIf D4.BackColor = Color.LightBlue Then
            D4.BackColor = Color.Khaki
            DW4.Text = "K"
        ElseIf D4.BackColor = Color.Khaki Then
            D4.BackColor = Color.Pink
            DW4.Text = "P"
        ElseIf D4.BackColor = Color.Pink Then
            D4.BackColor = Color.White
            DW4.Text = "W"
        End If
    End Sub
    Private Sub D5_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D5.Click
        If D5.BackColor = Color.White Then
            D5.BackColor = Color.LightBlue
            DW5.Text = "L"
        ElseIf D5.BackColor = Color.LightBlue Then
            D5.BackColor = Color.Khaki
            DW5.Text = "K"
        ElseIf D5.BackColor = Color.Khaki Then
            D5.BackColor = Color.Pink
            DW5.Text = "P"
        ElseIf D5.BackColor = Color.Pink Then
            D5.BackColor = Color.White
            DW5.Text = "W"
        End If
    End Sub
    Private Sub D6_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D6.Click
        If D6.BackColor = Color.White Then
            D6.BackColor = Color.LightBlue
            DW6.Text = "L"
        ElseIf D6.BackColor = Color.LightBlue Then
            D6.BackColor = Color.Khaki
            DW6.Text = "K"
        ElseIf D6.BackColor = Color.Khaki Then
            D6.BackColor = Color.Pink
            DW6.Text = "P"
        ElseIf D6.BackColor = Color.Pink Then
            D6.BackColor = Color.White
            DW6.Text = "W"
        End If
    End Sub
    Private Sub D7_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D7.Click
        If D7.BackColor = Color.White Then
            D7.BackColor = Color.LightBlue
            DW7.Text = "L"
        ElseIf D7.BackColor = Color.LightBlue Then
            D7.BackColor = Color.Khaki
            DW7.Text = "K"
        ElseIf D7.BackColor = Color.Khaki Then
            D7.BackColor = Color.Pink
            DW7.Text = "P"
        ElseIf D7.BackColor = Color.Pink Then
            D7.BackColor = Color.White
            DW7.Text = "W"
        End If
    End Sub
    Private Sub D8_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D8.Click
        If D8.BackColor = Color.White Then
            D8.BackColor = Color.LightBlue
            DW8.Text = "L"
        ElseIf D8.BackColor = Color.LightBlue Then
            D8.BackColor = Color.Khaki
            DW8.Text = "K"
        ElseIf D8.BackColor = Color.Khaki Then
            D8.BackColor = Color.Pink
            DW8.Text = "P"
        ElseIf D8.BackColor = Color.Pink Then
            D8.BackColor = Color.White
            DW8.Text = "W"
        End If
    End Sub
    Private Sub D9_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D9.Click
        If D9.BackColor = Color.White Then
            D9.BackColor = Color.LightBlue
            DW9.Text = "L"
        ElseIf D9.BackColor = Color.LightBlue Then
            D9.BackColor = Color.Khaki
            DW9.Text = "K"
        ElseIf D9.BackColor = Color.Khaki Then
            D9.BackColor = Color.Pink
            DW9.Text = "P"
        ElseIf D9.BackColor = Color.Pink Then
            D9.BackColor = Color.White
            DW9.Text = "W"
        End If
    End Sub
    Private Sub D10_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D10.Click
        If D10.BackColor = Color.White Then
            D10.BackColor = Color.LightBlue
            DW10.Text = "L"
        ElseIf D10.BackColor = Color.LightBlue Then
            D10.BackColor = Color.Khaki
            DW10.Text = "K"
        ElseIf D10.BackColor = Color.Khaki Then
            D10.BackColor = Color.Pink
            DW10.Text = "P"
        ElseIf D10.BackColor = Color.Pink Then
            D10.BackColor = Color.White
            DW10.Text = "W"
        End If
    End Sub
    Private Sub D11_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D11.Click
        If D11.BackColor = Color.White Then
            D11.BackColor = Color.LightBlue
            DW11.Text = "L"
        ElseIf D11.BackColor = Color.LightBlue Then
            D11.BackColor = Color.Khaki
            DW11.Text = "K"
        ElseIf D11.BackColor = Color.Khaki Then
            D11.BackColor = Color.Pink
            DW11.Text = "P"
        ElseIf D11.BackColor = Color.Pink Then
            D11.BackColor = Color.White
            DW11.Text = "W"
        End If
    End Sub
    Private Sub D12_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D12.Click
        If D12.BackColor = Color.White Then
            D12.BackColor = Color.LightBlue
            DW12.Text = "L"
        ElseIf D12.BackColor = Color.LightBlue Then
            D12.BackColor = Color.Khaki
            DW12.Text = "K"
        ElseIf D12.BackColor = Color.Khaki Then
            D12.BackColor = Color.Pink
            DW12.Text = "P"
        ElseIf D12.BackColor = Color.Pink Then
            D12.BackColor = Color.White
            DW12.Text = "W"
        End If
    End Sub
    Private Sub D13_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D13.Click
        If D13.BackColor = Color.White Then
            D13.BackColor = Color.LightBlue
            DW13.Text = "L"
        ElseIf D13.BackColor = Color.LightBlue Then
            D13.BackColor = Color.Khaki
            DW13.Text = "K"
        ElseIf D13.BackColor = Color.Khaki Then
            D13.BackColor = Color.Pink
            DW13.Text = "P"
        ElseIf D13.BackColor = Color.Pink Then
            D13.BackColor = Color.White
            DW13.Text = "W"
        End If
    End Sub
    Private Sub D14_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D14.Click
        If D14.BackColor = Color.White Then
            D14.BackColor = Color.LightBlue
            DW14.Text = "L"
        ElseIf D14.BackColor = Color.LightBlue Then
            D14.BackColor = Color.Khaki
            DW14.Text = "K"
        ElseIf D14.BackColor = Color.Khaki Then
            D14.BackColor = Color.Pink
            DW14.Text = "P"
        ElseIf D14.BackColor = Color.Pink Then
            D14.BackColor = Color.White
            DW14.Text = "W"
        End If
    End Sub
    Private Sub D15_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D15.Click
        If D15.BackColor = Color.White Then
            D15.BackColor = Color.LightBlue
            DW15.Text = "L"
        ElseIf D15.BackColor = Color.LightBlue Then
            D15.BackColor = Color.Khaki
            DW15.Text = "K"
        ElseIf D15.BackColor = Color.Khaki Then
            D15.BackColor = Color.Pink
            DW15.Text = "P"
        ElseIf D15.BackColor = Color.Pink Then
            D15.BackColor = Color.White
            DW15.Text = "W"
        End If
    End Sub
    Private Sub D16_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D16.Click
        If D16.BackColor = Color.White Then
            D16.BackColor = Color.LightBlue
            DW16.Text = "L"
        ElseIf D16.BackColor = Color.LightBlue Then
            D16.BackColor = Color.Khaki
            DW16.Text = "K"
        ElseIf D16.BackColor = Color.Khaki Then
            D16.BackColor = Color.Pink
            DW16.Text = "P"
        ElseIf D16.BackColor = Color.Pink Then
            D16.BackColor = Color.White
            DW16.Text = "W"
        End If
    End Sub
    Private Sub D17_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D17.Click
        If D17.BackColor = Color.White Then
            D17.BackColor = Color.LightBlue
            DW17.Text = "L"
        ElseIf D17.BackColor = Color.LightBlue Then
            D17.BackColor = Color.Khaki
            DW17.Text = "K"
        ElseIf D17.BackColor = Color.Khaki Then
            D17.BackColor = Color.Pink
            DW17.Text = "P"
        ElseIf D17.BackColor = Color.Pink Then
            D17.BackColor = Color.White
            DW17.Text = "W"
        End If
    End Sub
    Private Sub D18_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D18.Click
        If D18.BackColor = Color.White Then
            D18.BackColor = Color.LightBlue
            DW18.Text = "L"
        ElseIf D18.BackColor = Color.LightBlue Then
            D18.BackColor = Color.Khaki
            DW18.Text = "K"
        ElseIf D18.BackColor = Color.Khaki Then
            D18.BackColor = Color.Pink
            DW18.Text = "P"
        ElseIf D18.BackColor = Color.Pink Then
            D18.BackColor = Color.White
            DW18.Text = "W"
        End If
    End Sub
    Private Sub D19_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D19.Click
        If D19.BackColor = Color.White Then
            D19.BackColor = Color.LightBlue
            DW19.Text = "L"
        ElseIf D19.BackColor = Color.LightBlue Then
            D19.BackColor = Color.Khaki
            DW19.Text = "K"
        ElseIf D19.BackColor = Color.Khaki Then
            D19.BackColor = Color.Pink
            DW19.Text = "P"
        ElseIf D19.BackColor = Color.Pink Then
            D19.BackColor = Color.White
            DW19.Text = "W"
        End If
    End Sub
    Private Sub D20_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D20.Click
        If D20.BackColor = Color.White Then
            D20.BackColor = Color.LightBlue
            DW20.Text = "L"
        ElseIf D20.BackColor = Color.LightBlue Then
            D20.BackColor = Color.Khaki
            DW20.Text = "K"
        ElseIf D20.BackColor = Color.Khaki Then
            D20.BackColor = Color.Pink
            DW20.Text = "P"
        ElseIf D20.BackColor = Color.Pink Then
            D20.BackColor = Color.White
            DW20.Text = "W"
        End If
    End Sub
    Private Sub D21_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D21.Click
        If D21.BackColor = Color.White Then
            D21.BackColor = Color.LightBlue
            DW21.Text = "L"
        ElseIf D21.BackColor = Color.LightBlue Then
            D21.BackColor = Color.Khaki
            DW21.Text = "K"
        ElseIf D21.BackColor = Color.Khaki Then
            D21.BackColor = Color.Pink
            DW21.Text = "P"
        ElseIf D21.BackColor = Color.Pink Then
            D21.BackColor = Color.White
            DW21.Text = "W"
        End If
    End Sub
    Private Sub D22_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D22.Click
        If D22.BackColor = Color.White Then
            D22.BackColor = Color.LightBlue
            DW22.Text = "L"
        ElseIf D22.BackColor = Color.LightBlue Then
            D22.BackColor = Color.Khaki
            DW22.Text = "K"
        ElseIf D22.BackColor = Color.Khaki Then
            D22.BackColor = Color.Pink
            DW22.Text = "P"
        ElseIf D22.BackColor = Color.Pink Then
            D22.BackColor = Color.White
            DW22.Text = "W"
        End If
    End Sub
    Private Sub D23_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D23.Click
        If D23.BackColor = Color.White Then
            D23.BackColor = Color.LightBlue
            DW23.Text = "L"
        ElseIf D23.BackColor = Color.LightBlue Then
            D23.BackColor = Color.Khaki
            DW23.Text = "K"
        ElseIf D23.BackColor = Color.Khaki Then
            D23.BackColor = Color.Pink
            DW23.Text = "P"
        ElseIf D23.BackColor = Color.Pink Then
            D23.BackColor = Color.White
            DW23.Text = "W"
        End If
    End Sub
    Private Sub D24_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D24.Click
        If D24.BackColor = Color.White Then
            D24.BackColor = Color.LightBlue
            DW24.Text = "L"
        ElseIf D24.BackColor = Color.LightBlue Then
            D24.BackColor = Color.Khaki
            DW24.Text = "K"
        ElseIf D24.BackColor = Color.Khaki Then
            D24.BackColor = Color.Pink
            DW24.Text = "P"
        ElseIf D24.BackColor = Color.Pink Then
            D24.BackColor = Color.White
            DW24.Text = "W"
        End If
    End Sub
    Private Sub D25_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles D25.Click
        If D25.BackColor = Color.White Then
            D25.BackColor = Color.LightBlue
            DW25.Text = "L"
        ElseIf D25.BackColor = Color.LightBlue Then
            D25.BackColor = Color.Khaki
            DW25.Text = "K"
        ElseIf D25.BackColor = Color.Khaki Then
            D25.BackColor = Color.Pink
            DW25.Text = "P"
        ElseIf D25.BackColor = Color.Pink Then
            D25.BackColor = Color.White
            DW25.Text = "W"
        End If
    End Sub

    Private Sub E1_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E1.Click
        If E1.BackColor = Color.White Then
            E1.BackColor = Color.LightBlue
            EW1.Text = "L"
        ElseIf E1.BackColor = Color.LightBlue Then
            E1.BackColor = Color.Khaki
            EW1.Text = "K"
        ElseIf E1.BackColor = Color.Khaki Then
            E1.BackColor = Color.Pink
            EW1.Text = "P"
        ElseIf E1.BackColor = Color.Pink Then
            E1.BackColor = Color.White
            EW1.Text = "W"
        End If
    End Sub
    Private Sub E2_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E2.Click
        If E2.BackColor = Color.White Then
            E2.BackColor = Color.LightBlue
            EW2.Text = "L"
        ElseIf E2.BackColor = Color.LightBlue Then
            E2.BackColor = Color.Khaki
            EW2.Text = "K"
        ElseIf E2.BackColor = Color.Khaki Then
            E2.BackColor = Color.Pink
            EW2.Text = "P"
        ElseIf E2.BackColor = Color.Pink Then
            E2.BackColor = Color.White
            EW2.Text = "W"
        End If
    End Sub
    Private Sub E3_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E3.Click
        If E3.BackColor = Color.White Then
            E3.BackColor = Color.LightBlue
            EW3.Text = "L"
        ElseIf E3.BackColor = Color.LightBlue Then
            E3.BackColor = Color.Khaki
            EW3.Text = "K"
        ElseIf E3.BackColor = Color.Khaki Then
            E3.BackColor = Color.Pink
            EW3.Text = "P"
        ElseIf E3.BackColor = Color.Pink Then
            E3.BackColor = Color.White
            EW3.Text = "W"
        End If
    End Sub
    Private Sub E4_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E4.Click
        If E4.BackColor = Color.White Then
            E4.BackColor = Color.LightBlue
            EW4.Text = "L"
        ElseIf E4.BackColor = Color.LightBlue Then
            E4.BackColor = Color.Khaki
            EW4.Text = "K"
        ElseIf E4.BackColor = Color.Khaki Then
            E4.BackColor = Color.Pink
            EW4.Text = "P"
        ElseIf E4.BackColor = Color.Pink Then
            E4.BackColor = Color.White
            EW4.Text = "W"
        End If
    End Sub
    Private Sub E5_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E5.Click
        If E5.BackColor = Color.White Then
            E5.BackColor = Color.LightBlue
            EW5.Text = "L"
        ElseIf E5.BackColor = Color.LightBlue Then
            E5.BackColor = Color.Khaki
            EW5.Text = "K"
        ElseIf E5.BackColor = Color.Khaki Then
            E5.BackColor = Color.Pink
            EW5.Text = "P"
        ElseIf E5.BackColor = Color.Pink Then
            E5.BackColor = Color.White
            EW5.Text = "W"
        End If
    End Sub
    Private Sub E6_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E6.Click
        If E6.BackColor = Color.White Then
            E6.BackColor = Color.LightBlue
            EW6.Text = "L"
        ElseIf E6.BackColor = Color.LightBlue Then
            E6.BackColor = Color.Khaki
            EW6.Text = "K"
        ElseIf E6.BackColor = Color.Khaki Then
            E6.BackColor = Color.Pink
            EW6.Text = "P"
        ElseIf E6.BackColor = Color.Pink Then
            E6.BackColor = Color.White
            EW6.Text = "W"
        End If
    End Sub
    Private Sub E7_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E7.Click
        If E7.BackColor = Color.White Then
            E7.BackColor = Color.LightBlue
            EW7.Text = "L"
        ElseIf E7.BackColor = Color.LightBlue Then
            E7.BackColor = Color.Khaki
            EW7.Text = "K"
        ElseIf E7.BackColor = Color.Khaki Then
            E7.BackColor = Color.Pink
            EW7.Text = "P"
        ElseIf E7.BackColor = Color.Pink Then
            E7.BackColor = Color.White
            EW7.Text = "W"
        End If
    End Sub
    Private Sub E8_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E8.Click
        If E8.BackColor = Color.White Then
            E8.BackColor = Color.LightBlue
            EW8.Text = "L"
        ElseIf E8.BackColor = Color.LightBlue Then
            E8.BackColor = Color.Khaki
            EW8.Text = "K"
        ElseIf E8.BackColor = Color.Khaki Then
            E8.BackColor = Color.Pink
            EW8.Text = "P"
        ElseIf E8.BackColor = Color.Pink Then
            E8.BackColor = Color.White
            EW8.Text = "W"
        End If
    End Sub
    Private Sub E9_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E9.Click
        If E9.BackColor = Color.White Then
            E9.BackColor = Color.LightBlue
            EW9.Text = "L"
        ElseIf E9.BackColor = Color.LightBlue Then
            E9.BackColor = Color.Khaki
            EW9.Text = "K"
        ElseIf E9.BackColor = Color.Khaki Then
            E9.BackColor = Color.Pink
            EW9.Text = "P"
        ElseIf E9.BackColor = Color.Pink Then
            E9.BackColor = Color.White
            EW9.Text = "W"
        End If
    End Sub
    Private Sub E10_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E10.Click
        If E10.BackColor = Color.White Then
            E10.BackColor = Color.LightBlue
            EW10.Text = "L"
        ElseIf E10.BackColor = Color.LightBlue Then
            E10.BackColor = Color.Khaki
            EW10.Text = "K"
        ElseIf E10.BackColor = Color.Khaki Then
            E10.BackColor = Color.Pink
            EW10.Text = "P"
        ElseIf E10.BackColor = Color.Pink Then
            E10.BackColor = Color.White
            EW10.Text = "W"
        End If
    End Sub
    Private Sub E11_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E11.Click
        If E11.BackColor = Color.White Then
            E11.BackColor = Color.LightBlue
            EW11.Text = "L"
        ElseIf E11.BackColor = Color.LightBlue Then
            E11.BackColor = Color.Khaki
            EW11.Text = "K"
        ElseIf E11.BackColor = Color.Khaki Then
            E11.BackColor = Color.Pink
            EW11.Text = "P"
        ElseIf E11.BackColor = Color.Pink Then
            E11.BackColor = Color.White
            EW11.Text = "W"
        End If
    End Sub
    Private Sub E12_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E12.Click
        If E12.BackColor = Color.White Then
            E12.BackColor = Color.LightBlue
            EW12.Text = "L"
        ElseIf E12.BackColor = Color.LightBlue Then
            E12.BackColor = Color.Khaki
            EW12.Text = "K"
        ElseIf E12.BackColor = Color.Khaki Then
            E12.BackColor = Color.Pink
            EW12.Text = "P"
        ElseIf E12.BackColor = Color.Pink Then
            E12.BackColor = Color.White
            EW12.Text = "W"
        End If
    End Sub
    Private Sub E13_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E13.Click
        If E13.BackColor = Color.White Then
            E13.BackColor = Color.LightBlue
            EW13.Text = "L"
        ElseIf E13.BackColor = Color.LightBlue Then
            E13.BackColor = Color.Khaki
            EW13.Text = "K"
        ElseIf E13.BackColor = Color.Khaki Then
            E13.BackColor = Color.Pink
            EW13.Text = "P"
        ElseIf E13.BackColor = Color.Pink Then
            E13.BackColor = Color.White
            EW13.Text = "W"
        End If
    End Sub
    Private Sub E14_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E14.Click
        If E14.BackColor = Color.White Then
            E14.BackColor = Color.LightBlue
            EW14.Text = "L"
        ElseIf E14.BackColor = Color.LightBlue Then
            E14.BackColor = Color.Khaki
            EW14.Text = "K"
        ElseIf E14.BackColor = Color.Khaki Then
            E14.BackColor = Color.Pink
            EW14.Text = "P"
        ElseIf E14.BackColor = Color.Pink Then
            E14.BackColor = Color.White
            EW14.Text = "W"
        End If
    End Sub
    Private Sub E15_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E15.Click
        If E15.BackColor = Color.White Then
            E15.BackColor = Color.LightBlue
            EW15.Text = "L"
        ElseIf E15.BackColor = Color.LightBlue Then
            E15.BackColor = Color.Khaki
            EW15.Text = "K"
        ElseIf E15.BackColor = Color.Khaki Then
            E15.BackColor = Color.Pink
            EW15.Text = "P"
        ElseIf E15.BackColor = Color.Pink Then
            E15.BackColor = Color.White
            EW15.Text = "W"
        End If
    End Sub
    Private Sub E16_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E16.Click
        If E16.BackColor = Color.White Then
            E16.BackColor = Color.LightBlue
            EW16.Text = "L"
        ElseIf E16.BackColor = Color.LightBlue Then
            E16.BackColor = Color.Khaki
            EW16.Text = "K"
        ElseIf E16.BackColor = Color.Khaki Then
            E16.BackColor = Color.Pink
            EW16.Text = "P"
        ElseIf E16.BackColor = Color.Pink Then
            E16.BackColor = Color.White
            EW16.Text = "W"
        End If
    End Sub
    Private Sub E17_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E17.Click
        If E17.BackColor = Color.White Then
            E17.BackColor = Color.LightBlue
            EW17.Text = "L"
        ElseIf E17.BackColor = Color.LightBlue Then
            E17.BackColor = Color.Khaki
            EW17.Text = "K"
        ElseIf E17.BackColor = Color.Khaki Then
            E17.BackColor = Color.Pink
            EW17.Text = "P"
        ElseIf E17.BackColor = Color.Pink Then
            E17.BackColor = Color.White
            EW17.Text = "W"
        End If
    End Sub
    Private Sub E18_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E18.Click
        If E18.BackColor = Color.White Then
            E18.BackColor = Color.LightBlue
            EW18.Text = "L"
        ElseIf E18.BackColor = Color.LightBlue Then
            E18.BackColor = Color.Khaki
            EW18.Text = "K"
        ElseIf E18.BackColor = Color.Khaki Then
            E18.BackColor = Color.Pink
            EW18.Text = "P"
        ElseIf E18.BackColor = Color.Pink Then
            E18.BackColor = Color.White
            EW18.Text = "W"
        End If
    End Sub
    Private Sub E19_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E19.Click
        If E19.BackColor = Color.White Then
            E19.BackColor = Color.LightBlue
            EW19.Text = "L"
        ElseIf E19.BackColor = Color.LightBlue Then
            E19.BackColor = Color.Khaki
            EW19.Text = "K"
        ElseIf E19.BackColor = Color.Khaki Then
            E19.BackColor = Color.Pink
            EW19.Text = "P"
        ElseIf E19.BackColor = Color.Pink Then
            E19.BackColor = Color.White
            EW19.Text = "W"
        End If
    End Sub
    Private Sub E20_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E20.Click
        If E20.BackColor = Color.White Then
            E20.BackColor = Color.LightBlue
            EW20.Text = "L"
        ElseIf E20.BackColor = Color.LightBlue Then
            E20.BackColor = Color.Khaki
            EW20.Text = "K"
        ElseIf E20.BackColor = Color.Khaki Then
            E20.BackColor = Color.Pink
            EW20.Text = "P"
        ElseIf E20.BackColor = Color.Pink Then
            E20.BackColor = Color.White
            EW20.Text = "W"
        End If
    End Sub
    Private Sub E21_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E21.Click
        If E21.BackColor = Color.White Then
            E21.BackColor = Color.LightBlue
            EW21.Text = "L"
        ElseIf E21.BackColor = Color.LightBlue Then
            E21.BackColor = Color.Khaki
            EW21.Text = "K"
        ElseIf E21.BackColor = Color.Khaki Then
            E21.BackColor = Color.Pink
            EW21.Text = "P"
        ElseIf E21.BackColor = Color.Pink Then
            E21.BackColor = Color.White
            EW21.Text = "W"
        End If
    End Sub
    Private Sub E22_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E22.Click
        If E22.BackColor = Color.White Then
            E22.BackColor = Color.LightBlue
            EW22.Text = "L"
        ElseIf E22.BackColor = Color.LightBlue Then
            E22.BackColor = Color.Khaki
            EW22.Text = "K"
        ElseIf E22.BackColor = Color.Khaki Then
            E22.BackColor = Color.Pink
            EW22.Text = "P"
        ElseIf E22.BackColor = Color.Pink Then
            E22.BackColor = Color.White
            EW22.Text = "W"
        End If
    End Sub
    Private Sub E23_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E23.Click
        If E23.BackColor = Color.White Then
            E23.BackColor = Color.LightBlue
            EW23.Text = "L"
        ElseIf E23.BackColor = Color.LightBlue Then
            E23.BackColor = Color.Khaki
            EW23.Text = "K"
        ElseIf E23.BackColor = Color.Khaki Then
            E23.BackColor = Color.Pink
            EW23.Text = "P"
        ElseIf E23.BackColor = Color.Pink Then
            E23.BackColor = Color.White
            EW23.Text = "W"
        End If
    End Sub
    Private Sub E24_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E24.Click
        If E24.BackColor = Color.White Then
            E24.BackColor = Color.LightBlue
            EW24.Text = "L"
        ElseIf E24.BackColor = Color.LightBlue Then
            E24.BackColor = Color.Khaki
            EW24.Text = "K"
        ElseIf E24.BackColor = Color.Khaki Then
            E24.BackColor = Color.Pink
            EW24.Text = "P"
        ElseIf E24.BackColor = Color.Pink Then
            E24.BackColor = Color.White
            EW24.Text = "W"
        End If
    End Sub
    Private Sub E25_Click(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles E25.Click
        If E25.BackColor = Color.White Then
            E25.BackColor = Color.LightBlue
            EW25.Text = "L"
        ElseIf E25.BackColor = Color.LightBlue Then
            E25.BackColor = Color.Khaki
            EW25.Text = "K"
        ElseIf E25.BackColor = Color.Khaki Then
            E25.BackColor = Color.Pink
            EW25.Text = "P"
        ElseIf E25.BackColor = Color.Pink Then
            E25.BackColor = Color.White
            EW25.Text = "W"
        End If
    End Sub

    Private Sub isi_gl_A1()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW1.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A1.Text.Replace(",", "."))
            If Not Val(A1.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW1.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A1.Text.Replace(",", "."))
            If Not Val(A1.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW1.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A1.Text.Replace(",", "."))
            If Not Val(A1.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW1.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A1.Text.Replace(",", "."))
            If Not Val(A1.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A2()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW2.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A2.Text.Replace(",", "."))
            If Not Val(A2.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW2.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A2.Text.Replace(",", "."))
            If Not Val(A2.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW2.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A2.Text.Replace(",", "."))
            If Not Val(A2.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW2.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A2.Text.Replace(",", "."))
            If Not Val(A2.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A3()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW3.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A3.Text.Replace(",", "."))
            If Not Val(A3.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW3.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A3.Text.Replace(",", "."))
            If Not Val(A3.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW3.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A3.Text.Replace(",", "."))
            If Not Val(A3.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW3.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A3.Text.Replace(",", "."))
            If Not Val(A3.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A4()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW4.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A4.Text.Replace(",", "."))
            If Not Val(A4.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW4.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A4.Text.Replace(",", "."))
            If Not Val(A4.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW4.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A4.Text.Replace(",", "."))
            If Not Val(A4.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW4.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A4.Text.Replace(",", "."))
            If Not Val(A4.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A5()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW5.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A5.Text.Replace(",", "."))
            If Not Val(A5.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW5.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A5.Text.Replace(",", "."))
            If Not Val(A5.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW5.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A5.Text.Replace(",", "."))
            If Not Val(A5.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW5.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A5.Text.Replace(",", "."))
            If Not Val(A5.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A6()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW6.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A6.Text.Replace(",", "."))
            If Not Val(A6.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW6.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A6.Text.Replace(",", "."))
            If Not Val(A6.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW6.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A6.Text.Replace(",", "."))
            If Not Val(A6.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW6.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A6.Text.Replace(",", "."))
            If Not Val(A6.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A7()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW7.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A7.Text.Replace(",", "."))
            If Not Val(A7.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW7.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A7.Text.Replace(",", "."))
            If Not Val(A7.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW7.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A7.Text.Replace(",", "."))
            If Not Val(A7.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW7.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A7.Text.Replace(",", "."))
            If Not Val(A7.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A8()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW8.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A8.Text.Replace(",", "."))
            If Not Val(A8.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW8.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A8.Text.Replace(",", "."))
            If Not Val(A8.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW8.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A8.Text.Replace(",", "."))
            If Not Val(A8.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW8.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A8.Text.Replace(",", "."))
            If Not Val(A8.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A9()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW9.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A9.Text.Replace(",", "."))
            If Not Val(A9.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW9.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A9.Text.Replace(",", "."))
            If Not Val(A9.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW9.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A9.Text.Replace(",", "."))
            If Not Val(A9.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW9.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A9.Text.Replace(",", "."))
            If Not Val(A9.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A10()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW10.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A10.Text.Replace(",", "."))
            If Not Val(A10.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW10.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A10.Text.Replace(",", "."))
            If Not Val(A10.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW10.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A10.Text.Replace(",", "."))
            If Not Val(A10.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW10.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A10.Text.Replace(",", "."))
            If Not Val(A10.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A11()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW11.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A11.Text.Replace(",", "."))
            If Not Val(A11.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW11.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A11.Text.Replace(",", "."))
            If Not Val(A11.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW11.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A11.Text.Replace(",", "."))
            If Not Val(A11.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW11.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A11.Text.Replace(",", "."))
            If Not Val(A11.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A12()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW12.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A12.Text.Replace(",", "."))
            If Not Val(A12.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW12.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A12.Text.Replace(",", "."))
            If Not Val(A12.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW12.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A12.Text.Replace(",", "."))
            If Not Val(A12.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW12.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A12.Text.Replace(",", "."))
            If Not Val(A12.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A13()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW13.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A13.Text.Replace(",", "."))
            If Not Val(A13.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW13.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A13.Text.Replace(",", "."))
            If Not Val(A13.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW13.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A13.Text.Replace(",", "."))
            If Not Val(A13.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW13.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A13.Text.Replace(",", "."))
            If Not Val(A13.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A14()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW14.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A14.Text.Replace(",", "."))
            If Not Val(A14.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW14.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A14.Text.Replace(",", "."))
            If Not Val(A14.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW14.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A14.Text.Replace(",", "."))
            If Not Val(A14.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW14.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A14.Text.Replace(",", "."))
            If Not Val(A14.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A15()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW15.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A15.Text.Replace(",", "."))
            If Not Val(A15.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW15.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A15.Text.Replace(",", "."))
            If Not Val(A15.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW15.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A15.Text.Replace(",", "."))
            If Not Val(A15.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW15.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A15.Text.Replace(",", "."))
            If Not Val(A15.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A16()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW16.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A16.Text.Replace(",", "."))
            If Not Val(A16.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW16.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A16.Text.Replace(",", "."))
            If Not Val(A16.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW16.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A16.Text.Replace(",", "."))
            If Not Val(A16.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW16.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A16.Text.Replace(",", "."))
            If Not Val(A16.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A17()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW17.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A17.Text.Replace(",", "."))
            If Not Val(A17.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW17.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A17.Text.Replace(",", "."))
            If Not Val(A17.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW17.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A17.Text.Replace(",", "."))
            If Not Val(A17.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW17.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A17.Text.Replace(",", "."))
            If Not Val(A17.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A18()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW18.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A18.Text.Replace(",", "."))
            If Not Val(A18.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW18.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A18.Text.Replace(",", "."))
            If Not Val(A18.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW18.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A18.Text.Replace(",", "."))
            If Not Val(A18.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW18.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A18.Text.Replace(",", "."))
            If Not Val(A18.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A19()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW19.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A19.Text.Replace(",", "."))
            If Not Val(A19.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW19.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A19.Text.Replace(",", "."))
            If Not Val(A19.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW19.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A19.Text.Replace(",", "."))
            If Not Val(A19.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW19.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A19.Text.Replace(",", "."))
            If Not Val(A19.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A20()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW20.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A20.Text.Replace(",", "."))
            If Not Val(A20.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW20.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A20.Text.Replace(",", "."))
            If Not Val(A20.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW20.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A20.Text.Replace(",", "."))
            If Not Val(A20.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW20.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A20.Text.Replace(",", "."))
            If Not Val(A20.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A21()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW21.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A21.Text.Replace(",", "."))
            If Not Val(A21.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW21.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A21.Text.Replace(",", "."))
            If Not Val(A21.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW21.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A21.Text.Replace(",", "."))
            If Not Val(A21.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW21.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A21.Text.Replace(",", "."))
            If Not Val(A21.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A22()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW22.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A22.Text.Replace(",", "."))
            If Not Val(A22.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW22.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A22.Text.Replace(",", "."))
            If Not Val(A22.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW22.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A22.Text.Replace(",", "."))
            If Not Val(A22.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW22.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A22.Text.Replace(",", "."))
            If Not Val(A22.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A23()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW23.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A23.Text.Replace(",", "."))
            If Not Val(A23.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW23.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A23.Text.Replace(",", "."))
            If Not Val(A23.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW23.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A23.Text.Replace(",", "."))
            If Not Val(A23.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW23.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A23.Text.Replace(",", "."))
            If Not Val(A23.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A24()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW24.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A24.Text.Replace(",", "."))
            If Not Val(A24.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW24.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A24.Text.Replace(",", "."))
            If Not Val(A24.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW24.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A24.Text.Replace(",", "."))
            If Not Val(A24.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW24.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A24.Text.Replace(",", "."))
            If Not Val(A24.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_A25()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If AW25.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(A25.Text.Replace(",", "."))
            If Not Val(A25.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf AW25.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(A25.Text.Replace(",", "."))
            If Not Val(A25.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf AW25.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(A25.Text.Replace(",", "."))
            If Not Val(A25.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf AW25.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(A25.Text.Replace(",", "."))
            If Not Val(A25.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub

    Private Sub isi_gl_B1()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW1.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B1.Text.Replace(",", "."))
            If Not Val(B1.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW1.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B1.Text.Replace(",", "."))
            If Not Val(B1.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW1.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B1.Text.Replace(",", "."))
            If Not Val(B1.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW1.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B1.Text.Replace(",", "."))
            If Not Val(B1.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B2()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW2.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B2.Text.Replace(",", "."))
            If Not Val(B2.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW2.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B2.Text.Replace(",", "."))
            If Not Val(B2.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW2.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B2.Text.Replace(",", "."))
            If Not Val(B2.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW2.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B2.Text.Replace(",", "."))
            If Not Val(B2.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B3()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW3.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B3.Text.Replace(",", "."))
            If Not Val(B3.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW3.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B3.Text.Replace(",", "."))
            If Not Val(B3.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW3.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B3.Text.Replace(",", "."))
            If Not Val(B3.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW3.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B3.Text.Replace(",", "."))
            If Not Val(B3.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B4()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW4.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B4.Text.Replace(",", "."))
            If Not Val(B4.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW4.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B4.Text.Replace(",", "."))
            If Not Val(B4.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW4.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B4.Text.Replace(",", "."))
            If Not Val(B4.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW4.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B4.Text.Replace(",", "."))
            If Not Val(B4.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B5()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW5.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B5.Text.Replace(",", "."))
            If Not Val(B5.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW5.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B5.Text.Replace(",", "."))
            If Not Val(B5.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW5.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B5.Text.Replace(",", "."))
            If Not Val(B5.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW5.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B5.Text.Replace(",", "."))
            If Not Val(B5.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B6()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW6.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B6.Text.Replace(",", "."))
            If Not Val(B6.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW6.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B6.Text.Replace(",", "."))
            If Not Val(B6.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW6.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B6.Text.Replace(",", "."))
            If Not Val(B6.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW6.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B6.Text.Replace(",", "."))
            If Not Val(B6.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B7()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW7.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B7.Text.Replace(",", "."))
            If Not Val(B7.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW7.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B7.Text.Replace(",", "."))
            If Not Val(B7.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW7.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B7.Text.Replace(",", "."))
            If Not Val(B7.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW7.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B7.Text.Replace(",", "."))
            If Not Val(B7.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B8()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW8.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B8.Text.Replace(",", "."))
            If Not Val(B8.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW8.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B8.Text.Replace(",", "."))
            If Not Val(B8.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW8.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B8.Text.Replace(",", "."))
            If Not Val(B8.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW8.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B8.Text.Replace(",", "."))
            If Not Val(B8.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B9()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW9.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B9.Text.Replace(",", "."))
            If Not Val(B9.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW9.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B9.Text.Replace(",", "."))
            If Not Val(B9.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW9.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B9.Text.Replace(",", "."))
            If Not Val(B9.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW9.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B9.Text.Replace(",", "."))
            If Not Val(B9.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B10()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW10.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B10.Text.Replace(",", "."))
            If Not Val(B10.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW10.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B10.Text.Replace(",", "."))
            If Not Val(B10.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW10.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B10.Text.Replace(",", "."))
            If Not Val(B10.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW10.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B10.Text.Replace(",", "."))
            If Not Val(B10.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B11()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW11.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B11.Text.Replace(",", "."))
            If Not Val(B11.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW11.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B11.Text.Replace(",", "."))
            If Not Val(B11.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW11.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B11.Text.Replace(",", "."))
            If Not Val(B11.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW11.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B11.Text.Replace(",", "."))
            If Not Val(B11.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B12()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW12.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B12.Text.Replace(",", "."))
            If Not Val(B12.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW12.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B12.Text.Replace(",", "."))
            If Not Val(B12.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW12.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B12.Text.Replace(",", "."))
            If Not Val(B12.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW12.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B12.Text.Replace(",", "."))
            If Not Val(B12.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B13()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW13.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B13.Text.Replace(",", "."))
            If Not Val(B13.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW13.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B13.Text.Replace(",", "."))
            If Not Val(B13.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW13.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B13.Text.Replace(",", "."))
            If Not Val(B13.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW13.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B13.Text.Replace(",", "."))
            If Not Val(B13.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B14()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW14.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B14.Text.Replace(",", "."))
            If Not Val(B14.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW14.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B14.Text.Replace(",", "."))
            If Not Val(B14.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW14.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B14.Text.Replace(",", "."))
            If Not Val(B14.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW14.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B14.Text.Replace(",", "."))
            If Not Val(B14.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B15()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW15.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B15.Text.Replace(",", "."))
            If Not Val(B15.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW15.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B15.Text.Replace(",", "."))
            If Not Val(B15.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW15.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B15.Text.Replace(",", "."))
            If Not Val(B15.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW15.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B15.Text.Replace(",", "."))
            If Not Val(B15.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B16()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW16.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B16.Text.Replace(",", "."))
            If Not Val(B16.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW16.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B16.Text.Replace(",", "."))
            If Not Val(B16.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW16.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B16.Text.Replace(",", "."))
            If Not Val(B16.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW16.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B16.Text.Replace(",", "."))
            If Not Val(B16.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B17()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW17.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B17.Text.Replace(",", "."))
            If Not Val(B17.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW17.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B17.Text.Replace(",", "."))
            If Not Val(B17.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW17.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B17.Text.Replace(",", "."))
            If Not Val(B17.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW17.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B17.Text.Replace(",", "."))
            If Not Val(B17.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B18()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW18.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B18.Text.Replace(",", "."))
            If Not Val(B18.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW18.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B18.Text.Replace(",", "."))
            If Not Val(B18.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW18.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B18.Text.Replace(",", "."))
            If Not Val(B18.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW18.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B18.Text.Replace(",", "."))
            If Not Val(B18.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B19()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW19.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B19.Text.Replace(",", "."))
            If Not Val(B19.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW19.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B19.Text.Replace(",", "."))
            If Not Val(B19.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW19.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B19.Text.Replace(",", "."))
            If Not Val(B19.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW19.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B19.Text.Replace(",", "."))
            If Not Val(B19.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B20()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW20.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B20.Text.Replace(",", "."))
            If Not Val(B20.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW20.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B20.Text.Replace(",", "."))
            If Not Val(B20.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW20.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B20.Text.Replace(",", "."))
            If Not Val(B20.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW20.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B20.Text.Replace(",", "."))
            If Not Val(B20.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B21()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW21.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B21.Text.Replace(",", "."))
            If Not Val(B21.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW21.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B21.Text.Replace(",", "."))
            If Not Val(B21.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW21.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B21.Text.Replace(",", "."))
            If Not Val(B21.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW21.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B21.Text.Replace(",", "."))
            If Not Val(B21.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B22()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW22.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B22.Text.Replace(",", "."))
            If Not Val(B22.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW22.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B22.Text.Replace(",", "."))
            If Not Val(B22.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW22.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B22.Text.Replace(",", "."))
            If Not Val(B22.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW22.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B22.Text.Replace(",", "."))
            If Not Val(B22.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B23()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW23.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B23.Text.Replace(",", "."))
            If Not Val(B23.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW23.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B23.Text.Replace(",", "."))
            If Not Val(B23.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW23.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B23.Text.Replace(",", "."))
            If Not Val(B23.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW23.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B23.Text.Replace(",", "."))
            If Not Val(B23.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B24()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW24.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B24.Text.Replace(",", "."))
            If Not Val(B24.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW24.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B24.Text.Replace(",", "."))
            If Not Val(B24.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW24.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B24.Text.Replace(",", "."))
            If Not Val(B24.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW24.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B24.Text.Replace(",", "."))
            If Not Val(B24.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_B25()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If BW25.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(B25.Text.Replace(",", "."))
            If Not Val(B25.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf BW25.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(B25.Text.Replace(",", "."))
            If Not Val(B25.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf BW25.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(B25.Text.Replace(",", "."))
            If Not Val(B25.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf BW25.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(B25.Text.Replace(",", "."))
            If Not Val(B25.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub

    Private Sub isi_gl_C1()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW1.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C1.Text.Replace(",", "."))
            If Not Val(C1.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW1.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C1.Text.Replace(",", "."))
            If Not Val(C1.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW1.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C1.Text.Replace(",", "."))
            If Not Val(C1.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW1.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C1.Text.Replace(",", "."))
            If Not Val(C1.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C2()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW2.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C2.Text.Replace(",", "."))
            If Not Val(C2.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW2.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C2.Text.Replace(",", "."))
            If Not Val(C2.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW2.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C2.Text.Replace(",", "."))
            If Not Val(C2.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW2.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C2.Text.Replace(",", "."))
            If Not Val(C2.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C3()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW3.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C3.Text.Replace(",", "."))
            If Not Val(C3.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW3.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C3.Text.Replace(",", "."))
            If Not Val(C3.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW3.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C3.Text.Replace(",", "."))
            If Not Val(C3.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW3.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C3.Text.Replace(",", "."))
            If Not Val(C3.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C4()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW4.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C4.Text.Replace(",", "."))
            If Not Val(C4.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW4.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C4.Text.Replace(",", "."))
            If Not Val(C4.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW4.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C4.Text.Replace(",", "."))
            If Not Val(C4.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW4.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C4.Text.Replace(",", "."))
            If Not Val(C4.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C5()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW5.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C5.Text.Replace(",", "."))
            If Not Val(C5.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW5.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C5.Text.Replace(",", "."))
            If Not Val(C5.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW5.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C5.Text.Replace(",", "."))
            If Not Val(C5.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW5.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C5.Text.Replace(",", "."))
            If Not Val(C5.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C6()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW6.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C6.Text.Replace(",", "."))
            If Not Val(C6.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW6.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C6.Text.Replace(",", "."))
            If Not Val(C6.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW6.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C6.Text.Replace(",", "."))
            If Not Val(C6.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW6.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C6.Text.Replace(",", "."))
            If Not Val(C6.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C7()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW7.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C7.Text.Replace(",", "."))
            If Not Val(C7.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW7.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C7.Text.Replace(",", "."))
            If Not Val(C7.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW7.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C7.Text.Replace(",", "."))
            If Not Val(C7.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW7.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C7.Text.Replace(",", "."))
            If Not Val(C7.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C8()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW8.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C8.Text.Replace(",", "."))
            If Not Val(C8.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW8.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C8.Text.Replace(",", "."))
            If Not Val(C8.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW8.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C8.Text.Replace(",", "."))
            If Not Val(C8.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW8.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C8.Text.Replace(",", "."))
            If Not Val(C8.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C9()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW9.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C9.Text.Replace(",", "."))
            If Not Val(C9.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW9.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C9.Text.Replace(",", "."))
            If Not Val(C9.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW9.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C9.Text.Replace(",", "."))
            If Not Val(C9.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW9.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C9.Text.Replace(",", "."))
            If Not Val(C9.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C10()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW10.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C10.Text.Replace(",", "."))
            If Not Val(C10.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW10.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C10.Text.Replace(",", "."))
            If Not Val(C10.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW10.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C10.Text.Replace(",", "."))
            If Not Val(C10.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW10.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C10.Text.Replace(",", "."))
            If Not Val(C10.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C11()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW11.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C11.Text.Replace(",", "."))
            If Not Val(C11.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW11.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C11.Text.Replace(",", "."))
            If Not Val(C11.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW11.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C11.Text.Replace(",", "."))
            If Not Val(C11.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW11.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C11.Text.Replace(",", "."))
            If Not Val(C11.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C12()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW12.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C12.Text.Replace(",", "."))
            If Not Val(C12.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW12.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C12.Text.Replace(",", "."))
            If Not Val(C12.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW12.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C12.Text.Replace(",", "."))
            If Not Val(C12.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW12.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C12.Text.Replace(",", "."))
            If Not Val(C12.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C13()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW13.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C13.Text.Replace(",", "."))
            If Not Val(C13.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW13.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C13.Text.Replace(",", "."))
            If Not Val(C13.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW13.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C13.Text.Replace(",", "."))
            If Not Val(C13.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW13.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C13.Text.Replace(",", "."))
            If Not Val(C13.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C14()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW14.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C14.Text.Replace(",", "."))
            If Not Val(C14.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW14.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C14.Text.Replace(",", "."))
            If Not Val(C14.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW14.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C14.Text.Replace(",", "."))
            If Not Val(C14.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW14.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C14.Text.Replace(",", "."))
            If Not Val(C14.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C15()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW15.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C15.Text.Replace(",", "."))
            If Not Val(C15.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW15.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C15.Text.Replace(",", "."))
            If Not Val(C15.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW15.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C15.Text.Replace(",", "."))
            If Not Val(C15.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW15.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C15.Text.Replace(",", "."))
            If Not Val(C15.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C16()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW16.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C16.Text.Replace(",", "."))
            If Not Val(C16.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW16.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C16.Text.Replace(",", "."))
            If Not Val(C16.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW16.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C16.Text.Replace(",", "."))
            If Not Val(C16.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW16.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C16.Text.Replace(",", "."))
            If Not Val(C16.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C17()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW17.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C17.Text.Replace(",", "."))
            If Not Val(C17.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW17.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C17.Text.Replace(",", "."))
            If Not Val(C17.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW17.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C17.Text.Replace(",", "."))
            If Not Val(C17.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW17.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C17.Text.Replace(",", "."))
            If Not Val(C17.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C18()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW18.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C18.Text.Replace(",", "."))
            If Not Val(C18.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW18.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C18.Text.Replace(",", "."))
            If Not Val(C18.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW18.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C18.Text.Replace(",", "."))
            If Not Val(C18.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW18.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C18.Text.Replace(",", "."))
            If Not Val(C18.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C19()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW19.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C19.Text.Replace(",", "."))
            If Not Val(C19.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW19.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C19.Text.Replace(",", "."))
            If Not Val(C19.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW19.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C19.Text.Replace(",", "."))
            If Not Val(C19.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW19.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C19.Text.Replace(",", "."))
            If Not Val(C19.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C20()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW20.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C20.Text.Replace(",", "."))
            If Not Val(C20.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW20.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C20.Text.Replace(",", "."))
            If Not Val(C20.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW20.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C20.Text.Replace(",", "."))
            If Not Val(C20.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW20.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C20.Text.Replace(",", "."))
            If Not Val(C20.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C21()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW21.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C21.Text.Replace(",", "."))
            If Not Val(C21.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW21.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C21.Text.Replace(",", "."))
            If Not Val(C21.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW21.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C21.Text.Replace(",", "."))
            If Not Val(C21.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW21.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C21.Text.Replace(",", "."))
            If Not Val(C21.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C22()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW22.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C22.Text.Replace(",", "."))
            If Not Val(C22.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW22.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C22.Text.Replace(",", "."))
            If Not Val(C22.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW22.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C22.Text.Replace(",", "."))
            If Not Val(C22.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW22.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C22.Text.Replace(",", "."))
            If Not Val(C22.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C23()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW23.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C23.Text.Replace(",", "."))
            If Not Val(C23.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW23.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C23.Text.Replace(",", "."))
            If Not Val(C23.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW23.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C23.Text.Replace(",", "."))
            If Not Val(C23.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW23.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C23.Text.Replace(",", "."))
            If Not Val(C23.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C24()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW24.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C24.Text.Replace(",", "."))
            If Not Val(C24.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW24.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C24.Text.Replace(",", "."))
            If Not Val(C24.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW24.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C24.Text.Replace(",", "."))
            If Not Val(C24.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW24.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C24.Text.Replace(",", "."))
            If Not Val(C24.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_C25()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If CW25.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(C25.Text.Replace(",", "."))
            If Not Val(C25.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf CW25.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(C25.Text.Replace(",", "."))
            If Not Val(C25.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf CW25.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(C25.Text.Replace(",", "."))
            If Not Val(C25.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf CW25.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(C25.Text.Replace(",", "."))
            If Not Val(C25.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub

    Private Sub isi_gl_D1()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW1.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D1.Text.Replace(",", "."))
            If Not Val(D1.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW1.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D1.Text.Replace(",", "."))
            If Not Val(D1.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW1.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D1.Text.Replace(",", "."))
            If Not Val(D1.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW1.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D1.Text.Replace(",", "."))
            If Not Val(D1.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D2()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW2.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D2.Text.Replace(",", "."))
            If Not Val(D2.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW2.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D2.Text.Replace(",", "."))
            If Not Val(D2.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW2.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D2.Text.Replace(",", "."))
            If Not Val(D2.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW2.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D2.Text.Replace(",", "."))
            If Not Val(D2.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D3()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW3.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D3.Text.Replace(",", "."))
            If Not Val(D3.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW3.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D3.Text.Replace(",", "."))
            If Not Val(D3.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW3.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D3.Text.Replace(",", "."))
            If Not Val(D3.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW3.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D3.Text.Replace(",", "."))
            If Not Val(D3.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D4()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW4.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D4.Text.Replace(",", "."))
            If Not Val(D4.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW4.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D4.Text.Replace(",", "."))
            If Not Val(D4.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW4.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D4.Text.Replace(",", "."))
            If Not Val(D4.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW4.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D4.Text.Replace(",", "."))
            If Not Val(D4.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D5()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW5.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D5.Text.Replace(",", "."))
            If Not Val(D5.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW5.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D5.Text.Replace(",", "."))
            If Not Val(D5.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW5.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D5.Text.Replace(",", "."))
            If Not Val(D5.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW5.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D5.Text.Replace(",", "."))
            If Not Val(D5.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D6()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW6.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D6.Text.Replace(",", "."))
            If Not Val(D6.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW6.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D6.Text.Replace(",", "."))
            If Not Val(D6.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW6.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D6.Text.Replace(",", "."))
            If Not Val(D6.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW6.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D6.Text.Replace(",", "."))
            If Not Val(D6.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D7()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW7.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D7.Text.Replace(",", "."))
            If Not Val(D7.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW7.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D7.Text.Replace(",", "."))
            If Not Val(D7.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW7.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D7.Text.Replace(",", "."))
            If Not Val(D7.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW7.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D7.Text.Replace(",", "."))
            If Not Val(D7.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D8()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW8.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D8.Text.Replace(",", "."))
            If Not Val(D8.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW8.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D8.Text.Replace(",", "."))
            If Not Val(D8.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW8.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D8.Text.Replace(",", "."))
            If Not Val(D8.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW8.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D8.Text.Replace(",", "."))
            If Not Val(D8.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D9()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW9.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D9.Text.Replace(",", "."))
            If Not Val(D9.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW9.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D9.Text.Replace(",", "."))
            If Not Val(D9.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW9.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D9.Text.Replace(",", "."))
            If Not Val(D9.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW9.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D9.Text.Replace(",", "."))
            If Not Val(D9.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D10()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW10.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D10.Text.Replace(",", "."))
            If Not Val(D10.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW10.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D10.Text.Replace(",", "."))
            If Not Val(D10.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW10.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D10.Text.Replace(",", "."))
            If Not Val(D10.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW10.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D10.Text.Replace(",", "."))
            If Not Val(D10.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D11()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW11.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D11.Text.Replace(",", "."))
            If Not Val(D11.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW11.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D11.Text.Replace(",", "."))
            If Not Val(D11.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW11.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D11.Text.Replace(",", "."))
            If Not Val(D11.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW11.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D11.Text.Replace(",", "."))
            If Not Val(D11.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D12()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW12.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D12.Text.Replace(",", "."))
            If Not Val(D12.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW12.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D12.Text.Replace(",", "."))
            If Not Val(D12.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW12.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D12.Text.Replace(",", "."))
            If Not Val(D12.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW12.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D12.Text.Replace(",", "."))
            If Not Val(D12.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D13()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW13.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D13.Text.Replace(",", "."))
            If Not Val(D13.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW13.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D13.Text.Replace(",", "."))
            If Not Val(D13.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW13.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D13.Text.Replace(",", "."))
            If Not Val(D13.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW13.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D13.Text.Replace(",", "."))
            If Not Val(D13.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D14()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW14.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D14.Text.Replace(",", "."))
            If Not Val(D14.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW14.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D14.Text.Replace(",", "."))
            If Not Val(D14.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW14.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D14.Text.Replace(",", "."))
            If Not Val(D14.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW14.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D14.Text.Replace(",", "."))
            If Not Val(D14.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D15()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW15.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D15.Text.Replace(",", "."))
            If Not Val(D15.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW15.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D15.Text.Replace(",", "."))
            If Not Val(D15.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW15.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D15.Text.Replace(",", "."))
            If Not Val(D15.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW15.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D15.Text.Replace(",", "."))
            If Not Val(D15.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D16()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW16.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D16.Text.Replace(",", "."))
            If Not Val(D16.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW16.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D16.Text.Replace(",", "."))
            If Not Val(D16.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW16.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D16.Text.Replace(",", "."))
            If Not Val(D16.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW16.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D16.Text.Replace(",", "."))
            If Not Val(D16.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D17()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW17.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D17.Text.Replace(",", "."))
            If Not Val(D17.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW17.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D17.Text.Replace(",", "."))
            If Not Val(D17.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW17.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D17.Text.Replace(",", "."))
            If Not Val(D17.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW17.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D17.Text.Replace(",", "."))
            If Not Val(D17.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D18()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW18.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D18.Text.Replace(",", "."))
            If Not Val(D18.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW18.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D18.Text.Replace(",", "."))
            If Not Val(D18.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW18.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D18.Text.Replace(",", "."))
            If Not Val(D18.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW18.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D18.Text.Replace(",", "."))
            If Not Val(D18.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D19()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW19.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D19.Text.Replace(",", "."))
            If Not Val(D19.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW19.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D19.Text.Replace(",", "."))
            If Not Val(D19.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW19.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D19.Text.Replace(",", "."))
            If Not Val(D19.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW19.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D19.Text.Replace(",", "."))
            If Not Val(D19.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D20()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW20.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D20.Text.Replace(",", "."))
            If Not Val(D20.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW20.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D20.Text.Replace(",", "."))
            If Not Val(D20.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW20.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D20.Text.Replace(",", "."))
            If Not Val(D20.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW20.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D20.Text.Replace(",", "."))
            If Not Val(D20.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D21()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW21.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D21.Text.Replace(",", "."))
            If Not Val(D21.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW21.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D21.Text.Replace(",", "."))
            If Not Val(D21.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW21.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D21.Text.Replace(",", "."))
            If Not Val(D21.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW21.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D21.Text.Replace(",", "."))
            If Not Val(D21.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D22()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW22.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D22.Text.Replace(",", "."))
            If Not Val(D22.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW22.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D22.Text.Replace(",", "."))
            If Not Val(D22.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW22.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D22.Text.Replace(",", "."))
            If Not Val(D22.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW22.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D22.Text.Replace(",", "."))
            If Not Val(D22.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D23()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW23.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D23.Text.Replace(",", "."))
            If Not Val(D23.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW23.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D23.Text.Replace(",", "."))
            If Not Val(D23.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW23.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D23.Text.Replace(",", "."))
            If Not Val(D23.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW23.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D23.Text.Replace(",", "."))
            If Not Val(D23.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D24()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW24.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D24.Text.Replace(",", "."))
            If Not Val(D24.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW24.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D24.Text.Replace(",", "."))
            If Not Val(D24.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW24.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D24.Text.Replace(",", "."))
            If Not Val(D24.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW24.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D24.Text.Replace(",", "."))
            If Not Val(D24.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_D25()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If DW25.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(D25.Text.Replace(",", "."))
            If Not Val(D25.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf DW25.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(D25.Text.Replace(",", "."))
            If Not Val(D25.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf DW25.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(D25.Text.Replace(",", "."))
            If Not Val(D25.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf DW25.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(D25.Text.Replace(",", "."))
            If Not Val(D25.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub

    Private Sub isi_gl_E1()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW1.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E1.Text.Replace(",", "."))
            If Not Val(E1.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW1.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E1.Text.Replace(",", "."))
            If Not Val(E1.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW1.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E1.Text.Replace(",", "."))
            If Not Val(E1.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW1.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E1.Text.Replace(",", "."))
            If Not Val(E1.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E2()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW2.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E2.Text.Replace(",", "."))
            If Not Val(E2.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW2.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E2.Text.Replace(",", "."))
            If Not Val(E2.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW2.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E2.Text.Replace(",", "."))
            If Not Val(E2.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW2.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E2.Text.Replace(",", "."))
            If Not Val(E2.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E3()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW3.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E3.Text.Replace(",", "."))
            If Not Val(E3.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW3.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E3.Text.Replace(",", "."))
            If Not Val(E3.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW3.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E3.Text.Replace(",", "."))
            If Not Val(E3.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW3.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E3.Text.Replace(",", "."))
            If Not Val(E3.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E4()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW4.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E4.Text.Replace(",", "."))
            If Not Val(E4.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW4.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E4.Text.Replace(",", "."))
            If Not Val(E4.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW4.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E4.Text.Replace(",", "."))
            If Not Val(E4.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW4.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E4.Text.Replace(",", "."))
            If Not Val(E4.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E5()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW5.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E5.Text.Replace(",", "."))
            If Not Val(E5.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW5.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E5.Text.Replace(",", "."))
            If Not Val(E5.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW5.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E5.Text.Replace(",", "."))
            If Not Val(E5.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW5.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E5.Text.Replace(",", "."))
            If Not Val(E5.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E6()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW6.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E6.Text.Replace(",", "."))
            If Not Val(E6.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW6.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E6.Text.Replace(",", "."))
            If Not Val(E6.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW6.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E6.Text.Replace(",", "."))
            If Not Val(E6.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW6.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E6.Text.Replace(",", "."))
            If Not Val(E6.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E7()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW7.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E7.Text.Replace(",", "."))
            If Not Val(E7.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW7.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E7.Text.Replace(",", "."))
            If Not Val(E7.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW7.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E7.Text.Replace(",", "."))
            If Not Val(E7.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW7.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E7.Text.Replace(",", "."))
            If Not Val(E7.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E8()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW8.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E8.Text.Replace(",", "."))
            If Not Val(E8.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW8.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E8.Text.Replace(",", "."))
            If Not Val(E8.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW8.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E8.Text.Replace(",", "."))
            If Not Val(E8.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW8.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E8.Text.Replace(",", "."))
            If Not Val(E8.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E9()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW9.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E9.Text.Replace(",", "."))
            If Not Val(E9.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW9.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E9.Text.Replace(",", "."))
            If Not Val(E9.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW9.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E9.Text.Replace(",", "."))
            If Not Val(E9.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW9.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E9.Text.Replace(",", "."))
            If Not Val(E9.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E10()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW10.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E10.Text.Replace(",", "."))
            If Not Val(E10.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW10.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E10.Text.Replace(",", "."))
            If Not Val(E10.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW10.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E10.Text.Replace(",", "."))
            If Not Val(E10.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW10.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E10.Text.Replace(",", "."))
            If Not Val(E10.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E11()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW11.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E11.Text.Replace(",", "."))
            If Not Val(E11.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW11.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E11.Text.Replace(",", "."))
            If Not Val(E11.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW11.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E11.Text.Replace(",", "."))
            If Not Val(E11.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW11.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E11.Text.Replace(",", "."))
            If Not Val(E11.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E12()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW12.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E12.Text.Replace(",", "."))
            If Not Val(E12.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW12.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E12.Text.Replace(",", "."))
            If Not Val(E12.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW12.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E12.Text.Replace(",", "."))
            If Not Val(E12.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW12.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E12.Text.Replace(",", "."))
            If Not Val(E12.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E13()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW13.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E13.Text.Replace(",", "."))
            If Not Val(E13.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW13.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E13.Text.Replace(",", "."))
            If Not Val(E13.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW13.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E13.Text.Replace(",", "."))
            If Not Val(E13.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW13.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E13.Text.Replace(",", "."))
            If Not Val(E13.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E14()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW14.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E14.Text.Replace(",", "."))
            If Not Val(E14.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW14.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E14.Text.Replace(",", "."))
            If Not Val(E14.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW14.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E14.Text.Replace(",", "."))
            If Not Val(E14.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW14.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E14.Text.Replace(",", "."))
            If Not Val(E14.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E15()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW15.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E15.Text.Replace(",", "."))
            If Not Val(E15.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW15.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E15.Text.Replace(",", "."))
            If Not Val(E15.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW15.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E15.Text.Replace(",", "."))
            If Not Val(E15.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW15.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E15.Text.Replace(",", "."))
            If Not Val(E15.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E16()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW16.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E16.Text.Replace(",", "."))
            If Not Val(E16.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW16.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E16.Text.Replace(",", "."))
            If Not Val(E16.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW16.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E16.Text.Replace(",", "."))
            If Not Val(E16.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW16.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E16.Text.Replace(",", "."))
            If Not Val(E16.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E17()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW17.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E17.Text.Replace(",", "."))
            If Not Val(E17.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW17.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E17.Text.Replace(",", "."))
            If Not Val(E17.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW17.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E17.Text.Replace(",", "."))
            If Not Val(E17.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW17.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E17.Text.Replace(",", "."))
            If Not Val(E17.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E18()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW18.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E18.Text.Replace(",", "."))
            If Not Val(E18.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW18.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E18.Text.Replace(",", "."))
            If Not Val(E18.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW18.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E18.Text.Replace(",", "."))
            If Not Val(E18.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW18.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E18.Text.Replace(",", "."))
            If Not Val(E18.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E19()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW19.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E19.Text.Replace(",", "."))
            If Not Val(E19.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW19.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E19.Text.Replace(",", "."))
            If Not Val(E19.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW19.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E19.Text.Replace(",", "."))
            If Not Val(E19.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW19.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E19.Text.Replace(",", "."))
            If Not Val(E19.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E20()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW20.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E20.Text.Replace(",", "."))
            If Not Val(E20.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW20.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E20.Text.Replace(",", "."))
            If Not Val(E20.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW20.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E20.Text.Replace(",", "."))
            If Not Val(E20.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW20.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E20.Text.Replace(",", "."))
            If Not Val(E20.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E21()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW21.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E21.Text.Replace(",", "."))
            If Not Val(E21.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW21.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E21.Text.Replace(",", "."))
            If Not Val(E21.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW21.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E21.Text.Replace(",", "."))
            If Not Val(E21.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW21.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E21.Text.Replace(",", "."))
            If Not Val(E21.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E22()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW22.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E22.Text.Replace(",", "."))
            If Not Val(E22.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW22.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E22.Text.Replace(",", "."))
            If Not Val(E22.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW22.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E22.Text.Replace(",", "."))
            If Not Val(E22.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW22.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E22.Text.Replace(",", "."))
            If Not Val(E22.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E23()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW23.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E23.Text.Replace(",", "."))
            If Not Val(E23.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW23.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E23.Text.Replace(",", "."))
            If Not Val(E23.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW23.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E23.Text.Replace(",", "."))
            If Not Val(E23.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW23.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E23.Text.Replace(",", "."))
            If Not Val(E23.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E24()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW24.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E24.Text.Replace(",", "."))
            If Not Val(E24.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW24.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E24.Text.Replace(",", "."))
            If Not Val(E24.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW24.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E24.Text.Replace(",", "."))
            If Not Val(E24.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW24.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E24.Text.Replace(",", "."))
            If Not Val(E24.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub
    Private Sub isi_gl_E25()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If EW25.Text = "W" Then
            txt_grade_a.Text = Val(grade_a.Replace(",", ".")) + Val(E25.Text.Replace(",", "."))
            If Not Val(E25.Text) = 0 Then
                txt_gl_grade_a.Text = Val(txt_gl_grade_a.Text) + 1
            End If
        ElseIf EW25.Text = "L" Then
            txt_grade_b.Text = Val(grade_b.Replace(",", ".")) + Val(E25.Text.Replace(",", "."))
            If Not Val(E25.Text) = 0 Then
                txt_gl_grade_b.Text = Val(txt_gl_grade_b.Text) + 1
            End If
        ElseIf EW25.Text = "K" Then
            txt_claim_jadi.Text = Val(claim_jadi.Replace(",", ".")) + Val(E25.Text.Replace(",", "."))
            If Not Val(E25.Text) = 0 Then
                txt_gl_claim_jadi.Text = Val(txt_gl_claim_jadi.Text) + 1
            End If
        ElseIf EW25.Text = "P" Then
            txt_claim_celup.Text = Val(claim_celup.Replace(",", ".")) + Val(E25.Text.Replace(",", "."))
            If Not Val(E25.Text) = 0 Then
                txt_gl_claim_celup.Text = Val(txt_gl_claim_celup.Text) + 1
            End If
        End If
    End Sub

    Private Sub txt_meter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter.TextChanged
        If txt_meter.Text <> String.Empty Then
            Dim temp As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter.Select(txt_meter.Text.Length, 0)
            ElseIf txt_meter.Text = "-"c Then

            Else
                txt_meter.Text = CDec(temp).ToString("N0")
                txt_meter.Select(txt_meter.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_grade_a_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_grade_a.TextChanged
        If txt_grade_a.Text <> String.Empty Then
            Dim temp As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_grade_a.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_grade_a.Select(txt_grade_a.Text.Length, 0)
            ElseIf txt_grade_a.Text = "-"c Then

            Else
                txt_grade_a.Text = CDec(temp).ToString("N0")
                txt_grade_a.Select(txt_grade_a.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_grade_b_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_grade_b.TextChanged
        If txt_grade_b.Text <> String.Empty Then
            Dim temp As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_grade_b.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_grade_b.Select(txt_grade_b.Text.Length, 0)
            ElseIf txt_grade_b.Text = "-"c Then

            Else
                txt_grade_b.Text = CDec(temp).ToString("N0")
                txt_grade_b.Select(txt_grade_b.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_claim_jadi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_claim_jadi.TextChanged
        If txt_claim_jadi.Text <> String.Empty Then
            Dim temp As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_claim_jadi.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_claim_jadi.Select(txt_claim_jadi.Text.Length, 0)
            ElseIf txt_claim_jadi.Text = "-"c Then

            Else
                txt_claim_jadi.Text = CDec(temp).ToString("N0")
                txt_claim_jadi.Select(txt_claim_jadi.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_claim_celup_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_claim_celup.TextChanged
        If txt_claim_celup.Text <> String.Empty Then
            Dim temp As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_claim_celup.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_claim_celup.Select(txt_claim_celup.Text.Length, 0)
            ElseIf txt_claim_celup.Text = "-"c Then

            Else
                txt_claim_celup.Text = CDec(temp).ToString("N0")
                txt_claim_celup.Select(txt_claim_celup.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_meter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter.Text = String.Empty Then
                        txt_meter.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter.Select(txt_meter.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter.Text = String.Empty Then
                        txt_meter.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter.Select(txt_meter.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_grade_a_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_grade_a.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_grade_a.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_grade_a.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_grade_a.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_grade_a.Text = String.Empty Then
                        txt_grade_a.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_grade_a.Select(txt_grade_a.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_grade_a.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_grade_a.Text = String.Empty Then
                        txt_grade_a.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_grade_a.Select(txt_grade_a.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_grade_b_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_grade_b.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_grade_b.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_grade_b.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_grade_b.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_grade_b.Text = String.Empty Then
                        txt_grade_b.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_grade_b.Select(txt_grade_b.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_grade_b.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_grade_b.Text = String.Empty Then
                        txt_grade_b.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_grade_b.Select(txt_grade_b.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_claim_celup_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_claim_celup.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_claim_celup.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_claim_celup.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_claim_celup.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_claim_celup.Text = String.Empty Then
                        txt_claim_celup.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_claim_celup.Select(txt_claim_celup.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_claim_celup.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_claim_celup.Text = String.Empty Then
                        txt_claim_celup.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_claim_celup.Select(txt_claim_celup.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_claim_jadi_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_claim_jadi.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_claim_jadi.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_claim_jadi.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_claim_jadi.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_claim_jadi.Text = String.Empty Then
                        txt_claim_jadi.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_claim_jadi.Select(txt_claim_jadi.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_claim_jadi.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_claim_jadi.Text = String.Empty Then
                        txt_claim_jadi.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_claim_jadi.Select(txt_claim_jadi.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_susut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_susut.KeyPress
        If txt_susut.Text.Contains("%") Then
            If Not (e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_susut.Text.Contains(" ") Then
            If Not (e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = "%") Then e.Handled = True
        ElseIf txt_susut.Text.Contains(",") Or txt_susut.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = "%") Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = "," Or e.KeyChar = "%" Or e.KeyChar = " ") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_gulung_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung.KeyPress, txt_gl_grade_a.KeyPress, txt_gl_grade_b.KeyPress _
        , txt_gl_claim_jadi.KeyPress, txt_gl_claim_celup.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub

    Private Sub simpan_tbdetilhutang()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_packing.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty.Replace(",", "."))
                    .Parameters.AddWithValue("@9", txt_satuan_akhir.Text)
                    .Parameters.AddWithValue("@10", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Packing")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwippacking()
        Dim stok As String = txt_qty_awal.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty As String = txt_asal_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwippacking SET Stok=@1,Tambah1=@2 WHERE Id_Po_Packing='" & txt_id_po_packing.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    If txt_satuan_asal.Text = "Yard" And txt_satuan_awal.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(stok.Replace(",", ".")) - Val(qty.Replace(",", ".")), 2))
                    ElseIf txt_satuan_asal.Text = "Meter" And txt_satuan_awal.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(stok.Replace(",", ".")) - Val(qty.Replace(",", ".")), 2))
                    ElseIf txt_satuan_asal.Text = "Meter" And txt_satuan_awal.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(stok.Replace(",", ".")) - (Val(qty.Replace(",", ".")) * 0.9144), 2))
                    ElseIf txt_satuan_asal.Text = "Yard" And txt_satuan_awal.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(stok.Replace(",", ".")) - (Val(qty.Replace(",", ".")) * 1.0936), 2))
                    End If
                    If CheckBox1.Checked = True Then
                        .Parameters.AddWithValue("@2", "Closed")
                    Else
                        .Parameters.AddWithValue("@2", "")
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokgradea()
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_asal, harga_baru As Double
        harga_asal = txt_harga_asal.Text.Replace(",", ".")
        If txt_satuan_asal.Text = "Yard" Then
            harga_baru = harga_asal + harga.Replace(",", ".")
        ElseIf txt_satuan_asal.Text = "Meter" Then
            harga_baru = (harga_asal / 1.0936) + harga.Replace(",", ".")
        End If
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokgradea (Tanggal,Id_Grey,Id_SJ_Packing,Id_Grade_A,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_packing.Text)
                    .Parameters.AddWithValue("@3", txt_id_grade_a.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_sj_packing.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    .Parameters.AddWithValue("@7", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@8", txt_warna.Text)
                    .Parameters.AddWithValue("@9", txt_gl_grade_a.Text)
                    .Parameters.AddWithValue("@10", grade_a.Replace(",", "."))
                    .Parameters.AddWithValue("@11", "Yard")
                    .Parameters.AddWithValue("@12", Math.Round(harga_baru, 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokgradeb()
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_asal, harga_baru As Double
        harga_asal = txt_harga_asal.Text.Replace(",", ".")
        If txt_satuan_asal.Text = "Yard" Then
            harga_baru = harga_asal + harga.Replace(",", ".")
        ElseIf txt_satuan_asal.Text = "Meter" Then
            harga_baru = (harga_asal / 1.0936) + harga.Replace(",", ".")
        End If
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokgradeb (Tanggal,Id_Grey,Id_SJ_Packing,Id_Grade_B,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_packing.Text)
                    .Parameters.AddWithValue("@3", txt_id_grade_b.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_sj_packing.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    .Parameters.AddWithValue("@7", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@8", txt_warna.Text)
                    .Parameters.AddWithValue("@9", txt_gl_grade_b.Text)
                    .Parameters.AddWithValue("@10", grade_b.Replace(",", "."))
                    .Parameters.AddWithValue("@11", "Yard")
                    .Parameters.AddWithValue("@12", Math.Round(harga_baru, 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokclaimjadi()
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_asal, harga_baru As Double
        harga_asal = txt_harga_asal.Text.Replace(",", ".")
        If txt_satuan_asal.Text = "Yard" Then
            harga_baru = harga_asal + harga.Replace(",", ".")
        ElseIf txt_satuan_asal.Text = "Meter" Then
            harga_baru = (harga_asal / 1.0936) + harga.Replace(",", ".")
        End If
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokclaimjadi (Tanggal,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_packing.Text)
                    .Parameters.AddWithValue("@3", txt_id_claim_jadi.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_sj_packing.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    .Parameters.AddWithValue("@7", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@8", txt_warna.Text)
                    .Parameters.AddWithValue("@9", txt_gl_claim_jadi.Text)
                    .Parameters.AddWithValue("@10", claim_jadi.Replace(",", "."))
                    .Parameters.AddWithValue("@11", "Yard")
                    .Parameters.AddWithValue("@12", Math.Round(harga_baru, 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokclaimcelup()
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_asal, harga_baru As Double
        harga_asal = txt_harga_asal.Text.Replace(",", ".")
        If txt_satuan_asal.Text = "Yard" Then
            harga_baru = harga_asal + harga.Replace(",", ".")
        ElseIf txt_satuan_asal.Text = "Meter" Then
            harga_baru = (harga_asal / 1.0936) + harga.Replace(",", ".")
        End If
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokclaimcelup (Tanggal,Id_Grey,Id_SJ_Packing,Id_Claim_Celup,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_packing.Text)
                    .Parameters.AddWithValue("@3", txt_id_claim_jadi.Text)
                    .Parameters.AddWithValue("@4", txt_gudang.Text)
                    .Parameters.AddWithValue("@5", txt_sj_packing.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    .Parameters.AddWithValue("@7", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@8", txt_warna.Text)
                    .Parameters.AddWithValue("@9", txt_gl_claim_celup.Text)
                    .Parameters.AddWithValue("@10", claim_celup.Replace(",", "."))
                    .Parameters.AddWithValue("@11", "Yard")
                    .Parameters.AddWithValue("@12", Math.Round(harga_baru, 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbkeluarbarangjadi()
        Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_a As String = txt_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim grade_b As String = txt_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_jadi As String = txt_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim claim_celup As String = txt_claim_celup.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim QTY As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbkeluarbarangjadi (Tanggal,Jatuh_Tempo,Gudang,SJ_Packing,No_PO,Customer,Jenis_Kain,Warna,Gulung,QTY,Satuan,Susut,Harga,Total_Harga,Grade_A,Keluar_SJ_A,Gl_Grade_A,Grade_B,Keluar_SJ_B,Gl_Grade_B,Claim_Jadi,Keluar_SJ_Jadi,Gl_Claim_Jadi,Claim_Celup,Keluar_SJ_Celup,Gl_Claim_Celup,Keterangan,STATUS,Id_Grey,Id_SJ_Packing,Id_PO_Packing,M1,M2,M3,M4,M5,M6,M7,M8,M9,M10,M11,M12,M13,M14,M15,M16,M17,M18,M19,M20,M21,M22,M23,M24,M25,K1,K2,K3,K4,K5,K6,K7,K8,K9,K10,K11,K12,K13,K14,K15,K16,K17,K18,K19,K20,K21,K22,K23,K24,K25,Y1,Y2,Y3,Y4,Y5,Y6,Y7,Y8,Y9,Y10,Y11,Y12,Y13,Y14,Y15,Y16,Y17,Y18,Y19,Y20,Y21,Y22,Y23,Y24,Y25,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,A15,A16,A17,A18,A19,A20,A21,A22,A23,A24,A25,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,B16,B17,B18,B19,B20,B21,B22,B23,B24,B25,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,C16,C17,C18,C19,C20,C21,C22,C23,C24,C25,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,D15,D16,D17,D18,D19,D20,D21,D22,D23,D24,D25,E1,E2,E3,E4,E5,E6,E7,E8,E9,E10,E11,E12,E13,E14,E15,E16,E17,E18,E19,E20,E21,E22,E23,E24,E25,AW1,AW2,AW3,AW4,AW5,AW6,AW7,AW8,AW9,AW10,AW11,AW12,AW13,AW14,AW15,AW16,AW17,AW18,AW19,AW20,AW21,AW22,AW23,AW24,AW25,BW1,BW2,BW3,BW4,BW5,BW6,BW7,BW8,BW9,BW10,BW11,BW12,BW13,BW14,BW15,BW16,BW17,BW18,BW19,BW20,BW21,BW22,BW23,BW24,BW25,CW1,CW2,CW3,CW4,CW5,CW6,CW7,CW8,CW9,CW10,CW11,CW12,CW13,CW14,CW15,CW16,CW17,CW18,CW19,CW20,CW21,CW22,CW23,CW24,CW25,DW1,DW2,DW3,DW4,DW5,DW6,DW7,DW8,DW9,DW10,DW11,DW12,DW13,DW14,DW15,DW16,DW17,DW18,DW19,DW20,DW21,DW22,DW23,DW24,DW25,EW1,EW2,EW3,EW4,EW5,EW6,EW7,EW8,EW9,EW10,EW11,EW12,EW13,EW14,EW15,EW16,EW17,EW18,EW19,EW20,EW21,EW22,EW23,EW24,EW25,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,S11,S12,S13,S14,S15,S16,S17,S18,S19,S20,S21,S22,S23,S24,S25,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUE (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38,@39,@40,@41,@42,@43,@44,@45,@46,@47,@48,@49,@50,@51,@52,@53,@54,@55,@56,@57,@58,@59,@60,@61,@62,@63,@64,@65,@66,@67,@68,@69,@70,@71,@72,@73,@74,@75,@76,@77,@78,@79,@80,@81,@82,@83,@84,@85,@86,@87,@88,@89,@90,@91,@92,@93,@94,@95,@96,@97,@98,@99,@100,@101,@102,@103,@104,@105,@106,@107,@108,@109,@110,@111,@112,@113,@114,@115,@116,@117,@118,@119,@120,@121,@122,@123,@124,@125,@126,@127,@128,@129,@130,@131,@132,@133,@134,@135,@136,@137,@138,@139,@140,@141,@142,@143,@144,@145,@146,@147,@148,@149,@150,@151,@152,@153,@154,@155,@156,@157,@158,@159,@160,@161,@162,@163,@164,@165,@166,@167,@168,@169,@170,@171,@172,@173,@174,@175,@176,@177,@178,@179,@180,@181,@182,@183,@184,@185,@186,@187,@188,@189,@190,@191,@192,@193,@194,@195,@196,@197,@198,@199,@200,@201,@202,@203,@204,@205,@206,@207,@208,@209,@210,@211,@212,@213,@214,@215,@216,@217,@218,@219,@220,@221,@222,@223,@224,@225,@226,@227,@228,@229,@230,@231,@232,@233,@234,@235,@236,@237,@238,@239,@240,@241,@242,@243,@244,@245,@246,@247,@248,@249,@250,@251,@252,@253,@254,@255,@256,@257,@258,@259,@260,@261,@262,@263,@264,@265,@266,@267,@268,@269,@270,@271,@272,@273,@274,@275,@276,@277,@278,@279,@280,@281,@282,@283,@284,@285,@286,@287,@288,@289,@290,@291,@292,@293,@294,@295,@296,@297,@298,@299,@300,@301,@302,@303,@304,@305,@306,@307,@308,@309,@310,@311,@312,@313,@314,@315,@316,@317,@318,@319,@320,@321,@322,@323,@324,@325,@326,@327,@328,@329,@330,@331,@332,@333,@334,@335,@336,@337,@338,@339,@340,@341,@342,@343,@344,@345,@346,@347,@348,@349,@350,@351,@352,@353,@354,@355,@356,@357,@358,@359,@360,@361,@362,@363,@364,@365,@366,@367,@368,@369,@370,@371,@372,@373,@374,@375,@376,@377,@378,@379,@380,@381,@382,@383,@384,@385,@386)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", dtp_awal.Text)
                    .Parameters.AddWithValue("@2", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@3", txt_gudang.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing.Text)
                    .Parameters.AddWithValue("@5", txt_no_po.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    .Parameters.AddWithValue("@7", txt_jenis_kain.Text)
                    .Parameters.AddWithValue("@8", txt_warna.Text)
                    .Parameters.AddWithValue("@9", txt_gulung.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@10", QTY.Replace(",", "."))
                    .Parameters.AddWithValue("@11", txt_satuan_akhir.Text)
                    .Parameters.AddWithValue("@12", txt_susut.Text)
                    .Parameters.AddWithValue("@13", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@15", grade_a.Replace(",", "."))
                    .Parameters.AddWithValue("@16", 0)
                    .Parameters.AddWithValue("@17", txt_gl_grade_a.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", grade_b.Replace(",", "."))
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", txt_gl_grade_b.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@21", claim_jadi.Replace(",", "."))
                    .Parameters.AddWithValue("@22", 0)
                    .Parameters.AddWithValue("@23", txt_gl_claim_jadi.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@24", claim_celup.Replace(",", "."))
                    .Parameters.AddWithValue("@25", 0)
                    .Parameters.AddWithValue("@26", txt_gl_claim_celup.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@27", txt_keterangan.Text)
                    .Parameters.AddWithValue("@28", "")
                    .Parameters.AddWithValue("@29", txt_id_grey.Text)
                    .Parameters.AddWithValue("@30", txt_sj_packing.Text)
                    .Parameters.AddWithValue("@31", txt_id_po_packing.Text)
                    .Parameters.AddWithValue("@32", (M1.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@33", (M2.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@34", (M3.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@35", (M4.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@36", (M5.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@37", (M6.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@38", (M7.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@39", (M8.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@40", (M9.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@41", (M10.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@42", (M11.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@43", (M12.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@44", (M13.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@45", (M14.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@46", (M15.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@47", (M16.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@48", (M17.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@49", (M18.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@50", (M19.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@51", (M20.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@52", (M21.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@53", (M22.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@54", (M23.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@55", (M24.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@56", (M25.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@57", (K1.Text))
                    .Parameters.AddWithValue("@58", (K2.Text))
                    .Parameters.AddWithValue("@59", (K3.Text))
                    .Parameters.AddWithValue("@60", (K4.Text))
                    .Parameters.AddWithValue("@61", (K5.Text))
                    .Parameters.AddWithValue("@62", (K6.Text))
                    .Parameters.AddWithValue("@63", (K7.Text))
                    .Parameters.AddWithValue("@64", (K8.Text))
                    .Parameters.AddWithValue("@65", (K9.Text))
                    .Parameters.AddWithValue("@66", (K10.Text))
                    .Parameters.AddWithValue("@67", (K11.Text))
                    .Parameters.AddWithValue("@68", (K12.Text))
                    .Parameters.AddWithValue("@69", (K13.Text))
                    .Parameters.AddWithValue("@70", (K14.Text))
                    .Parameters.AddWithValue("@71", (K15.Text))
                    .Parameters.AddWithValue("@72", (K16.Text))
                    .Parameters.AddWithValue("@73", (K17.Text))
                    .Parameters.AddWithValue("@74", (K18.Text))
                    .Parameters.AddWithValue("@75", (K19.Text))
                    .Parameters.AddWithValue("@76", (K20.Text))
                    .Parameters.AddWithValue("@77", (K21.Text))
                    .Parameters.AddWithValue("@78", (K22.Text))
                    .Parameters.AddWithValue("@79", (K23.Text))
                    .Parameters.AddWithValue("@80", (K24.Text))
                    .Parameters.AddWithValue("@81", (K25.Text))
                    .Parameters.AddWithValue("@82", (Y1.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@83", (Y2.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@84", (Y3.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@85", (Y4.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@86", (Y5.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@87", (Y6.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@88", (Y7.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@89", (Y8.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@90", (Y9.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@91", (Y10.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@92", (Y11.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@93", (Y12.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@94", (Y13.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@95", (Y14.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@96", (Y15.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@97", (Y16.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@98", (Y17.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@99", (Y18.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@100", (Y19.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@101", (Y20.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@102", (Y21.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@103", (Y22.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@104", (Y23.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@105", (Y24.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@106", (Y25.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@107", (A1.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@108", (A2.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@109", (A3.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@110", (A4.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@111", (A5.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@112", (A6.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@113", (A7.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@114", (A8.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@115", (A9.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@116", (A10.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@117", (A11.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@118", (A12.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@119", (A13.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@120", (A14.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@121", (A15.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@122", (A16.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@123", (A17.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@124", (A18.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@125", (A19.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@126", (A20.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@127", (A21.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@128", (A22.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@129", (A23.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@130", (A24.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@131", (A25.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@132", (B1.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@133", (B2.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@134", (B3.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@135", (B4.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@136", (B5.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@137", (B6.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@138", (B7.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@139", (B8.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@140", (B9.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@141", (B10.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@142", (B11.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@143", (B12.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@144", (B13.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@145", (B14.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@146", (B15.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@147", (B16.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@148", (B17.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@149", (B18.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@150", (B19.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@151", (B20.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@152", (B21.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@153", (B22.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@154", (B23.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@155", (B24.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@156", (B25.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@157", (C1.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@158", (C2.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@159", (C3.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@160", (C4.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@161", (C5.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@162", (C6.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@163", (C7.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@164", (C8.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@165", (C9.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@166", (C10.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@167", (C11.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@168", (C12.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@169", (C13.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@170", (C14.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@171", (C15.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@172", (C16.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@173", (C17.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@174", (C18.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@175", (C19.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@176", (C20.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@177", (C21.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@178", (C22.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@179", (C23.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@180", (C24.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@181", (C25.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@182", (D1.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@183", (D2.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@184", (D3.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@185", (D4.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@186", (D5.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@187", (D6.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@188", (D7.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@189", (D8.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@190", (D9.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@191", (D10.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@192", (D11.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@193", (D12.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@194", (D13.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@195", (D14.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@196", (D15.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@197", (D16.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@198", (D17.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@199", (D18.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@200", (D19.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@201", (D20.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@202", (D21.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@203", (D22.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@204", (D23.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@205", (D24.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@206", (D25.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@207", (E1.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@208", (E2.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@209", (E3.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@210", (E4.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@211", (E5.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@212", (E6.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@213", (E7.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@214", (E8.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@215", (E9.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@216", (E10.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@217", (E11.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@218", (E12.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@219", (E13.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@220", (E14.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@221", (E15.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@222", (E16.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@223", (E17.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@224", (E18.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@225", (E19.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@226", (E20.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@227", (E21.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@228", (E22.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@229", (E23.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@230", (E24.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@231", (E25.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@232", (AW1.Text))
                    .Parameters.AddWithValue("@233", (AW2.Text))
                    .Parameters.AddWithValue("@234", (AW3.Text))
                    .Parameters.AddWithValue("@235", (AW4.Text))
                    .Parameters.AddWithValue("@236", (AW5.Text))
                    .Parameters.AddWithValue("@237", (AW6.Text))
                    .Parameters.AddWithValue("@238", (AW7.Text))
                    .Parameters.AddWithValue("@239", (AW8.Text))
                    .Parameters.AddWithValue("@240", (AW9.Text))
                    .Parameters.AddWithValue("@241", (AW10.Text))
                    .Parameters.AddWithValue("@242", (AW11.Text))
                    .Parameters.AddWithValue("@243", (AW12.Text))
                    .Parameters.AddWithValue("@244", (AW13.Text))
                    .Parameters.AddWithValue("@245", (AW14.Text))
                    .Parameters.AddWithValue("@246", (AW15.Text))
                    .Parameters.AddWithValue("@247", (AW16.Text))
                    .Parameters.AddWithValue("@248", (AW17.Text))
                    .Parameters.AddWithValue("@249", (AW18.Text))
                    .Parameters.AddWithValue("@250", (AW19.Text))
                    .Parameters.AddWithValue("@251", (AW20.Text))
                    .Parameters.AddWithValue("@252", (AW21.Text))
                    .Parameters.AddWithValue("@253", (AW22.Text))
                    .Parameters.AddWithValue("@254", (AW23.Text))
                    .Parameters.AddWithValue("@255", (AW24.Text))
                    .Parameters.AddWithValue("@256", (AW25.Text))
                    .Parameters.AddWithValue("@257", (BW1.Text))
                    .Parameters.AddWithValue("@258", (BW2.Text))
                    .Parameters.AddWithValue("@259", (BW3.Text))
                    .Parameters.AddWithValue("@260", (BW4.Text))
                    .Parameters.AddWithValue("@261", (BW5.Text))
                    .Parameters.AddWithValue("@262", (BW6.Text))
                    .Parameters.AddWithValue("@263", (BW7.Text))
                    .Parameters.AddWithValue("@264", (BW8.Text))
                    .Parameters.AddWithValue("@265", (BW9.Text))
                    .Parameters.AddWithValue("@266", (BW10.Text))
                    .Parameters.AddWithValue("@267", (BW11.Text))
                    .Parameters.AddWithValue("@268", (BW12.Text))
                    .Parameters.AddWithValue("@269", (BW13.Text))
                    .Parameters.AddWithValue("@270", (BW14.Text))
                    .Parameters.AddWithValue("@271", (BW15.Text))
                    .Parameters.AddWithValue("@272", (BW16.Text))
                    .Parameters.AddWithValue("@273", (BW17.Text))
                    .Parameters.AddWithValue("@274", (BW18.Text))
                    .Parameters.AddWithValue("@275", (BW19.Text))
                    .Parameters.AddWithValue("@276", (BW20.Text))
                    .Parameters.AddWithValue("@277", (BW21.Text))
                    .Parameters.AddWithValue("@278", (BW22.Text))
                    .Parameters.AddWithValue("@279", (BW23.Text))
                    .Parameters.AddWithValue("@280", (BW24.Text))
                    .Parameters.AddWithValue("@281", (BW25.Text))
                    .Parameters.AddWithValue("@282", (CW1.Text))
                    .Parameters.AddWithValue("@283", (CW2.Text))
                    .Parameters.AddWithValue("@284", (CW3.Text))
                    .Parameters.AddWithValue("@285", (CW4.Text))
                    .Parameters.AddWithValue("@286", (CW5.Text))
                    .Parameters.AddWithValue("@287", (CW6.Text))
                    .Parameters.AddWithValue("@288", (CW7.Text))
                    .Parameters.AddWithValue("@289", (CW8.Text))
                    .Parameters.AddWithValue("@290", (CW9.Text))
                    .Parameters.AddWithValue("@291", (CW10.Text))
                    .Parameters.AddWithValue("@292", (CW11.Text))
                    .Parameters.AddWithValue("@293", (CW12.Text))
                    .Parameters.AddWithValue("@294", (CW13.Text))
                    .Parameters.AddWithValue("@295", (CW14.Text))
                    .Parameters.AddWithValue("@296", (CW15.Text))
                    .Parameters.AddWithValue("@297", (CW16.Text))
                    .Parameters.AddWithValue("@298", (CW17.Text))
                    .Parameters.AddWithValue("@299", (CW18.Text))
                    .Parameters.AddWithValue("@300", (CW19.Text))
                    .Parameters.AddWithValue("@301", (CW20.Text))
                    .Parameters.AddWithValue("@302", (CW21.Text))
                    .Parameters.AddWithValue("@303", (CW22.Text))
                    .Parameters.AddWithValue("@304", (CW23.Text))
                    .Parameters.AddWithValue("@305", (CW24.Text))
                    .Parameters.AddWithValue("@306", (CW25.Text))
                    .Parameters.AddWithValue("@307", (DW1.Text))
                    .Parameters.AddWithValue("@308", (DW2.Text))
                    .Parameters.AddWithValue("@309", (DW3.Text))
                    .Parameters.AddWithValue("@310", (DW4.Text))
                    .Parameters.AddWithValue("@311", (DW5.Text))
                    .Parameters.AddWithValue("@312", (DW6.Text))
                    .Parameters.AddWithValue("@313", (DW7.Text))
                    .Parameters.AddWithValue("@314", (DW8.Text))
                    .Parameters.AddWithValue("@315", (DW9.Text))
                    .Parameters.AddWithValue("@316", (DW10.Text))
                    .Parameters.AddWithValue("@317", (DW11.Text))
                    .Parameters.AddWithValue("@318", (DW12.Text))
                    .Parameters.AddWithValue("@319", (DW13.Text))
                    .Parameters.AddWithValue("@320", (DW14.Text))
                    .Parameters.AddWithValue("@321", (DW15.Text))
                    .Parameters.AddWithValue("@322", (DW16.Text))
                    .Parameters.AddWithValue("@323", (DW17.Text))
                    .Parameters.AddWithValue("@324", (DW18.Text))
                    .Parameters.AddWithValue("@325", (DW19.Text))
                    .Parameters.AddWithValue("@326", (DW20.Text))
                    .Parameters.AddWithValue("@327", (DW21.Text))
                    .Parameters.AddWithValue("@328", (DW22.Text))
                    .Parameters.AddWithValue("@329", (DW23.Text))
                    .Parameters.AddWithValue("@330", (DW24.Text))
                    .Parameters.AddWithValue("@331", (DW25.Text))
                    .Parameters.AddWithValue("@332", (EW1.Text))
                    .Parameters.AddWithValue("@333", (EW2.Text))
                    .Parameters.AddWithValue("@334", (EW3.Text))
                    .Parameters.AddWithValue("@335", (EW4.Text))
                    .Parameters.AddWithValue("@336", (EW5.Text))
                    .Parameters.AddWithValue("@337", (EW6.Text))
                    .Parameters.AddWithValue("@338", (EW7.Text))
                    .Parameters.AddWithValue("@339", (EW8.Text))
                    .Parameters.AddWithValue("@340", (EW9.Text))
                    .Parameters.AddWithValue("@341", (EW10.Text))
                    .Parameters.AddWithValue("@342", (EW11.Text))
                    .Parameters.AddWithValue("@343", (EW12.Text))
                    .Parameters.AddWithValue("@344", (EW13.Text))
                    .Parameters.AddWithValue("@345", (EW14.Text))
                    .Parameters.AddWithValue("@346", (EW15.Text))
                    .Parameters.AddWithValue("@347", (EW16.Text))
                    .Parameters.AddWithValue("@348", (EW17.Text))
                    .Parameters.AddWithValue("@349", (EW18.Text))
                    .Parameters.AddWithValue("@350", (EW19.Text))
                    .Parameters.AddWithValue("@351", (EW20.Text))
                    .Parameters.AddWithValue("@352", (EW21.Text))
                    .Parameters.AddWithValue("@353", (EW22.Text))
                    .Parameters.AddWithValue("@354", (EW23.Text))
                    .Parameters.AddWithValue("@355", (EW24.Text))
                    .Parameters.AddWithValue("@356", (EW25.Text))
                    .Parameters.AddWithValue("@357", (S1.Text))
                    .Parameters.AddWithValue("@358", (S2.Text))
                    .Parameters.AddWithValue("@359", (S3.Text))
                    .Parameters.AddWithValue("@360", (S4.Text))
                    .Parameters.AddWithValue("@361", (S5.Text))
                    .Parameters.AddWithValue("@362", (S6.Text))
                    .Parameters.AddWithValue("@363", (S7.Text))
                    .Parameters.AddWithValue("@364", (S8.Text))
                    .Parameters.AddWithValue("@365", (S9.Text))
                    .Parameters.AddWithValue("@366", (S10.Text))
                    .Parameters.AddWithValue("@367", (S11.Text))
                    .Parameters.AddWithValue("@368", (S12.Text))
                    .Parameters.AddWithValue("@369", (S13.Text))
                    .Parameters.AddWithValue("@370", (S14.Text))
                    .Parameters.AddWithValue("@371", (S15.Text))
                    .Parameters.AddWithValue("@372", (S16.Text))
                    .Parameters.AddWithValue("@373", (S17.Text))
                    .Parameters.AddWithValue("@374", (S18.Text))
                    .Parameters.AddWithValue("@375", (S19.Text))
                    .Parameters.AddWithValue("@376", (S20.Text))
                    .Parameters.AddWithValue("@377", (S21.Text))
                    .Parameters.AddWithValue("@378", (S22.Text))
                    .Parameters.AddWithValue("@379", (S23.Text))
                    .Parameters.AddWithValue("@380", (S24.Text))
                    .Parameters.AddWithValue("@381", (S25.Text))
                    .Parameters.AddWithValue("@382", (""))
                    .Parameters.AddWithValue("@383", (""))
                    .Parameters.AddWithValue("@384", (""))
                    .Parameters.AddWithValue("@385", (ComboBox1.Text))
                    .Parameters.AddWithValue("@386", (0))
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub ts_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_simpan.Click
        Try
            Call isiidsjpacking()
            Call isiidgradea()
            Call isiidgradeb()
            Call isiidclaimjadi()
            Call isiidclaimcelup()
            Call isiiddetilhutang()
            Dim asal_meter As String = txt_asal_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim meter As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If ts_hitung.Enabled = True Then
                MsgBox("Hasil Packing Belum Dihitung")
            ElseIf txt_gudang.Text = "" Then
                MsgBox("GUDANG Belum Diinput")
                txt_gudang.Focus()
            ElseIf txt_sj_packing.Text = "" Then
                MsgBox("SURAT JALAN PACKING Belum Diinput")
                txt_sj_packing.Focus()
            ElseIf txt_no_po.Text = "" Then
                MsgBox("No PO Belum Diinput")
                txt_no_po.Focus()
            ElseIf txt_harga.Text = "" Then
                MsgBox("HARGA Belum Diinput")
                txt_harga.Focus()
            Else
                Call simpan_tbdetilhutang()
                If Not Val(txt_grade_a.Text) = 0 Then
                    Call simpan_tbstokgradea()
                End If
                If Not Val(txt_grade_b.Text) = 0 Then
                    Call simpan_tbstokgradeb()
                End If
                If Not Val(txt_claim_jadi.Text) = 0 Then
                    Call simpan_tbstokclaimjadi()
                End If
                If Not Val(txt_claim_celup.Text) = 0 Then
                    Call simpan_tbstokclaimcelup()
                End If
                Call update_tbstokwippacking()
                Call simpan_tbkeluarbarangjadi()
                MsgBox("HASIL PACKING Baru Berhasil Disimpan")
                form_hasil_packing.Focus()
                form_hasil_packing.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub isiidsjpacking()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Packing FROM tbkeluarbarangjadi WHERE Id_SJ_Packing in(select max(Id_SJ_Packing) FROM tbkeluarbarangjadi)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Packing")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_packing.Text = x.ToString
    End Sub
    Private Sub isiidgradea()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grade_A FROM tbstokgradea WHERE Id_Grade_A in(select max(Id_Grade_A) FROM tbstokgradea)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grade_A")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_grade_a.Text = x.ToString
    End Sub
    Private Sub isiidgradeb()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grade_B FROM tbstokgradeb WHERE Id_Grade_B in(select max(Id_Grade_B) FROM tbstokgradeb)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grade_B")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_grade_b.Text = x.ToString
    End Sub
    Private Sub isiidclaimjadi()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Claim_Jadi FROM tbstokclaimjadi WHERE Id_Claim_Jadi in(select max(Id_Claim_Jadi) FROM tbstokclaimjadi)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Claim_Jadi")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_claim_jadi.Text = x.ToString
    End Sub
    Private Sub isiidclaimcelup()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_claim_celup FROM tbstokclaimcelup WHERE Id_claim_celup in(select max(Id_claim_celup) FROM tbstokclaimcelup)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_claim_celup")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_claim_celup.Text = x.ToString
    End Sub
    Private Sub isiiddetilhutang()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang.Text = x.ToString
    End Sub

    Private Sub cb_meter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cb_meter.SelectedIndexChanged
        txt_satuan_awal.Text = cb_meter.Text
        Call rubah_susut_1()
        Call rubah_susut_2()
        Call rubah_susut_3()
        Call rubah_susut_4()
        Call rubah_susut_5()
        Call rubah_susut_6()
        Call rubah_susut_7()
        Call rubah_susut_8()
        Call rubah_susut_9()
        Call rubah_susut_10()
        Call rubah_susut_11()
        Call rubah_susut_12()
        Call rubah_susut_13()
        Call rubah_susut_14()
        Call rubah_susut_15()
        Call rubah_susut_16()
        Call rubah_susut_17()
        Call rubah_susut_18()
        Call rubah_susut_19()
        Call rubah_susut_20()
        Call rubah_susut_21()
        Call rubah_susut_22()
        Call rubah_susut_23()
        Call rubah_susut_24()
        Call rubah_susut_25()
    End Sub
    Private Sub rubah_susut_1()
        Dim susut_1 As Double
        If Not Y1.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_1 = Math.Round(((Val(M1.Text.Replace(",", ".")) - Val(Y1.Text.Replace(",", "."))) / Val(M1.Text.Replace(",", "."))) * 100, 2)
                S1.Text = susut_1.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_1 = Math.Round(((Val(M1.Text.Replace(",", ".")) - (Val(Y1.Text.Replace(",", ".")) * 0.9144)) / Val(M1.Text.Replace(",", "."))) * 100, 2)
                S1.Text = susut_1.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_2()
        Dim susut_2 As Double
        If Not Y2.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_2 = Math.Round(((Val(M2.Text.Replace(",", ".")) - Val(Y2.Text.Replace(",", "."))) / Val(M2.Text.Replace(",", "."))) * 100, 2)
                S2.Text = susut_2.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_2 = Math.Round(((Val(M2.Text.Replace(",", ".")) - (Val(Y2.Text.Replace(",", ".")) * 0.9144)) / Val(M2.Text.Replace(",", "."))) * 100, 2)
                S2.Text = susut_2.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_3()
        Dim susut_3 As Double
        If Not Y3.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_3 = Math.Round(((Val(M3.Text.Replace(",", ".")) - Val(Y3.Text.Replace(",", "."))) / Val(M3.Text.Replace(",", "."))) * 100, 2)
                S3.Text = susut_3.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_3 = Math.Round(((Val(M3.Text.Replace(",", ".")) - (Val(Y3.Text.Replace(",", ".")) * 0.9144)) / Val(M3.Text.Replace(",", "."))) * 100, 2)
                S3.Text = susut_3.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_4()
        Dim susut_4 As Double
        If Not Y4.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_4 = Math.Round(((Val(M4.Text.Replace(",", ".")) - Val(Y4.Text.Replace(",", "."))) / Val(M4.Text.Replace(",", "."))) * 100, 2)
                S4.Text = susut_4.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_4 = Math.Round(((Val(M4.Text.Replace(",", ".")) - (Val(Y4.Text.Replace(",", ".")) * 0.9144)) / Val(M4.Text.Replace(",", "."))) * 100, 2)
                S4.Text = susut_4.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_5()
        Dim susut_5 As Double
        If Not Y5.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_5 = Math.Round(((Val(M5.Text.Replace(",", ".")) - Val(Y5.Text.Replace(",", "."))) / Val(M5.Text.Replace(",", "."))) * 100, 2)
                S5.Text = susut_5.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_5 = Math.Round(((Val(M5.Text.Replace(",", ".")) - (Val(Y5.Text.Replace(",", ".")) * 0.9144)) / Val(M5.Text.Replace(",", "."))) * 100, 2)
                S5.Text = susut_5.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_6()
        Dim susut_6 As Double
        If Not Y6.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_6 = Math.Round(((Val(M6.Text.Replace(",", ".")) - Val(Y6.Text.Replace(",", "."))) / Val(M6.Text.Replace(",", "."))) * 100, 2)
                S6.Text = susut_6.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_6 = Math.Round(((Val(M6.Text.Replace(",", ".")) - (Val(Y6.Text.Replace(",", ".")) * 0.9144)) / Val(M6.Text.Replace(",", "."))) * 100, 2)
                S6.Text = susut_6.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_7()
        Dim susut_7 As Double
        If Not Y7.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_7 = Math.Round(((Val(M7.Text.Replace(",", ".")) - Val(Y7.Text.Replace(",", "."))) / Val(M7.Text.Replace(",", "."))) * 100, 2)
                S7.Text = susut_7.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_7 = Math.Round(((Val(M7.Text.Replace(",", ".")) - (Val(Y7.Text.Replace(",", ".")) * 0.9144)) / Val(M7.Text.Replace(",", "."))) * 100, 2)
                S7.Text = susut_7.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_8()
        Dim susut_8 As Double
        If Not Y8.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_8 = Math.Round(((Val(M8.Text.Replace(",", ".")) - Val(Y8.Text.Replace(",", "."))) / Val(M8.Text.Replace(",", "."))) * 100, 2)
                S8.Text = susut_8.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_8 = Math.Round(((Val(M8.Text.Replace(",", ".")) - (Val(Y8.Text.Replace(",", ".")) * 0.9144)) / Val(M8.Text.Replace(",", "."))) * 100, 2)
                S8.Text = susut_8.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_9()
        Dim susut_9 As Double
        If Not Y9.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_9 = Math.Round(((Val(M9.Text.Replace(",", ".")) - Val(Y9.Text.Replace(",", "."))) / Val(M9.Text.Replace(",", "."))) * 100, 2)
                S9.Text = susut_9.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_9 = Math.Round(((Val(M9.Text.Replace(",", ".")) - (Val(Y9.Text.Replace(",", ".")) * 0.9144)) / Val(M9.Text.Replace(",", "."))) * 100, 2)
                S9.Text = susut_9.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_10()
        Dim susut_10 As Double
        If Not Y10.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_10 = Math.Round(((Val(M10.Text.Replace(",", ".")) - Val(Y10.Text.Replace(",", "."))) / Val(M10.Text.Replace(",", "."))) * 100, 2)
                S10.Text = susut_10.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_10 = Math.Round(((Val(M10.Text.Replace(",", ".")) - (Val(Y10.Text.Replace(",", ".")) * 0.9144)) / Val(M10.Text.Replace(",", "."))) * 100, 2)
                S10.Text = susut_10.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_11()
        Dim susut_11 As Double
        If Not Y11.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_11 = Math.Round(((Val(M11.Text.Replace(",", ".")) - Val(Y11.Text.Replace(",", "."))) / Val(M11.Text.Replace(",", "."))) * 100, 2)
                S11.Text = susut_11.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_11 = Math.Round(((Val(M11.Text.Replace(",", ".")) - (Val(Y11.Text.Replace(",", ".")) * 0.9144)) / Val(M11.Text.Replace(",", "."))) * 100, 2)
                S11.Text = susut_11.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_12()
        Dim susut_12 As Double
        If Not Y12.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_12 = Math.Round(((Val(M12.Text.Replace(",", ".")) - Val(Y12.Text.Replace(",", "."))) / Val(M12.Text.Replace(",", "."))) * 100, 2)
                S12.Text = susut_12.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_12 = Math.Round(((Val(M12.Text.Replace(",", ".")) - (Val(Y12.Text.Replace(",", ".")) * 0.9144)) / Val(M12.Text.Replace(",", "."))) * 100, 2)
                S12.Text = susut_12.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_13()
        Dim susut_13 As Double
        If Not Y13.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_13 = Math.Round(((Val(M13.Text.Replace(",", ".")) - Val(Y13.Text.Replace(",", "."))) / Val(M13.Text.Replace(",", "."))) * 100, 2)
                S13.Text = susut_13.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_13 = Math.Round(((Val(M13.Text.Replace(",", ".")) - (Val(Y13.Text.Replace(",", ".")) * 0.9144)) / Val(M13.Text.Replace(",", "."))) * 100, 2)
                S13.Text = susut_13.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_14()
        Dim susut_14 As Double
        If Not Y14.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_14 = Math.Round(((Val(M14.Text.Replace(",", ".")) - Val(Y14.Text.Replace(",", "."))) / Val(M14.Text.Replace(",", "."))) * 100, 2)
                S14.Text = susut_14.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_14 = Math.Round(((Val(M14.Text.Replace(",", ".")) - (Val(Y14.Text.Replace(",", ".")) * 0.9144)) / Val(M14.Text.Replace(",", "."))) * 100, 2)
                S14.Text = susut_14.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_15()
        Dim susut_15 As Double
        If Not Y15.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_15 = Math.Round(((Val(M15.Text.Replace(",", ".")) - Val(Y15.Text.Replace(",", "."))) / Val(M15.Text.Replace(",", "."))) * 100, 2)
                S15.Text = susut_15.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_15 = Math.Round(((Val(M15.Text.Replace(",", ".")) - (Val(Y15.Text.Replace(",", ".")) * 0.9144)) / Val(M15.Text.Replace(",", "."))) * 100, 2)
                S15.Text = susut_15.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_16()
        Dim susut_16 As Double
        If Not Y16.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_16 = Math.Round(((Val(M16.Text.Replace(",", ".")) - Val(Y16.Text.Replace(",", "."))) / Val(M16.Text.Replace(",", "."))) * 100, 2)
                S16.Text = susut_16.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_16 = Math.Round(((Val(M16.Text.Replace(",", ".")) - (Val(Y16.Text.Replace(",", ".")) * 0.9144)) / Val(M16.Text.Replace(",", "."))) * 100, 2)
                S16.Text = susut_16.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_17()
        Dim susut_17 As Double
        If Not Y17.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_17 = Math.Round(((Val(M17.Text.Replace(",", ".")) - Val(Y17.Text.Replace(",", "."))) / Val(M17.Text.Replace(",", "."))) * 100, 2)
                S17.Text = susut_17.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_17 = Math.Round(((Val(M17.Text.Replace(",", ".")) - (Val(Y17.Text.Replace(",", ".")) * 0.9144)) / Val(M17.Text.Replace(",", "."))) * 100, 2)
                S17.Text = susut_17.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_18()
        Dim susut_18 As Double
        If Not Y18.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_18 = Math.Round(((Val(M18.Text.Replace(",", ".")) - Val(Y18.Text.Replace(",", "."))) / Val(M18.Text.Replace(",", "."))) * 100, 2)
                S18.Text = susut_18.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_18 = Math.Round(((Val(M18.Text.Replace(",", ".")) - (Val(Y18.Text.Replace(",", ".")) * 0.9144)) / Val(M18.Text.Replace(",", "."))) * 100, 2)
                S18.Text = susut_18.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_19()
        Dim susut_19 As Double
        If Not Y19.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_19 = Math.Round(((Val(M19.Text.Replace(",", ".")) - Val(Y19.Text.Replace(",", "."))) / Val(M19.Text.Replace(",", "."))) * 100, 2)
                S19.Text = susut_19.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_19 = Math.Round(((Val(M19.Text.Replace(",", ".")) - (Val(Y19.Text.Replace(",", ".")) * 0.9144)) / Val(M19.Text.Replace(",", "."))) * 100, 2)
                S19.Text = susut_19.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_20()
        Dim susut_20 As Double
        If Not Y20.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_20 = Math.Round(((Val(M20.Text.Replace(",", ".")) - Val(Y20.Text.Replace(",", "."))) / Val(M20.Text.Replace(",", "."))) * 100, 2)
                S20.Text = susut_20.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_20 = Math.Round(((Val(M20.Text.Replace(",", ".")) - (Val(Y20.Text.Replace(",", ".")) * 0.9144)) / Val(M20.Text.Replace(",", "."))) * 100, 2)
                S20.Text = susut_20.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_21()
        Dim susut_21 As Double
        If Not Y21.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_21 = Math.Round(((Val(M21.Text.Replace(",", ".")) - Val(Y21.Text.Replace(",", "."))) / Val(M21.Text.Replace(",", "."))) * 100, 2)
                S21.Text = susut_21.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_21 = Math.Round(((Val(M21.Text.Replace(",", ".")) - (Val(Y21.Text.Replace(",", ".")) * 0.9144)) / Val(M21.Text.Replace(",", "."))) * 100, 2)
                S21.Text = susut_21.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_22()
        Dim susut_22 As Double
        If Not Y22.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_22 = Math.Round(((Val(M22.Text.Replace(",", ".")) - Val(Y22.Text.Replace(",", "."))) / Val(M22.Text.Replace(",", "."))) * 100, 2)
                S22.Text = susut_22.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_22 = Math.Round(((Val(M22.Text.Replace(",", ".")) - (Val(Y22.Text.Replace(",", ".")) * 0.9144)) / Val(M22.Text.Replace(",", "."))) * 100, 2)
                S22.Text = susut_22.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_23()
        Dim susut_23 As Double
        If Not Y23.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_23 = Math.Round(((Val(M23.Text.Replace(",", ".")) - Val(Y23.Text.Replace(",", "."))) / Val(M23.Text.Replace(",", "."))) * 100, 2)
                S23.Text = susut_23.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_23 = Math.Round(((Val(M23.Text.Replace(",", ".")) - (Val(Y23.Text.Replace(",", ".")) * 0.9144)) / Val(M23.Text.Replace(",", "."))) * 100, 2)
                S23.Text = susut_23.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_24()
        Dim susut_24 As Double
        If Not Y24.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_24 = Math.Round(((Val(M24.Text.Replace(",", ".")) - Val(Y24.Text.Replace(",", "."))) / Val(M24.Text.Replace(",", "."))) * 100, 2)
                S24.Text = susut_24.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_24 = Math.Round(((Val(M24.Text.Replace(",", ".")) - (Val(Y24.Text.Replace(",", ".")) * 0.9144)) / Val(M24.Text.Replace(",", "."))) * 100, 2)
                S24.Text = susut_24.ToString + " %"
            End If
        End If
    End Sub
    Private Sub rubah_susut_25()
        Dim susut_25 As Double
        If Not Y25.Text = "" Then
            If txt_satuan_awal.Text = "Yard" And txt_satuan_akhir.Text = "Yard" Then
                susut_25 = Math.Round(((Val(M25.Text.Replace(",", ".")) - Val(Y25.Text.Replace(",", "."))) / Val(M25.Text.Replace(",", "."))) * 100, 2)
                S25.Text = susut_25.ToString + " %"
            ElseIf txt_satuan_awal.Text = "Meter" And txt_satuan_akhir.Text = "Yard" Then
                susut_25 = Math.Round(((Val(M25.Text.Replace(",", ".")) - (Val(Y25.Text.Replace(",", ".")) * 0.9144)) / Val(M25.Text.Replace(",", "."))) * 100, 2)
                S25.Text = susut_25.ToString + " %"
            End If
        End If
    End Sub

End Class