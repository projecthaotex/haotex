﻿Imports MySql.Data.MySqlClient

Public Class form_stok_wip_packing

    Private Sub form_stok_wip_packing_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If txt_form.Text = "form_input_hasil_packing" Then
            form_input_hasil_packing.txt_sj_packing.Focus()
            form_input_hasil_packing.Focus()
        End If
    End Sub
    Private Sub form_stok_wip_packing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "No PO"
        dgv1.Columns(2).HeaderText = "Gudang"
        dgv1.Columns(3).HeaderText = "Jenis Kain"
        dgv1.Columns(4).HeaderText = "Warna"
        dgv1.Columns(5).HeaderText = "Resep"
        dgv1.Columns(6).HeaderText = "Partai"
        dgv1.Columns(7).HeaderText = "Gulung"
        dgv1.Columns(8).HeaderText = "Stok"
        dgv1.Columns(9).HeaderText = "Satuan"
        dgv1.Columns(10).HeaderText = "Customer"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 80
        dgv1.Columns(2).Width = 130
        dgv1.Columns(3).Width = 150
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 100
        dgv1.Columns(6).Width = 100
        dgv1.Columns(7).Width = 80
        dgv1.Columns(8).Width = 100
        dgv1.Columns(9).Width = 70
        dgv1.Columns(10).Width = 130
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(11).Visible = False
        dgv1.Columns(12).Visible = False
        dgv1.Columns(13).Visible = False
        dgv1.Columns(8).DefaultCellStyle.Format = "N"
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Id_Grey,Id_PO_Packing,Harga FROM tbstokwippacking WHERE NOT Tambah1='Closed' ORDER BY Gudang"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokwippacking")
                            dgv1.DataSource = dsx.Tables("tbstokwippacking")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.GotFocus
        If txt_cari_gudang.Text = "< Gudang >" Then
            txt_cari_gudang.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.LostFocus
        If txt_cari_gudang.Text = "" Then
            txt_cari_gudang.Text = "< Gudang >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.TextChanged
        Try
            If txt_cari_gudang.Text = "< Gudang >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_gudang.Text = "< Gudang >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_gudang.Text = "< Gudang >" Then

            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_gudang()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Id_Grey,Id_PO_Packing,Harga FROM tbstokwippacking WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND NOT Tambah1='Closed' ORDER BY Gudang"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbstokwippacking")
                        dgv1.DataSource = dsx.Tables("tbstokwippacking")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
    End Sub
    Private Sub isidgv_jeniskain()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Id_Grey,Id_PO_Packing,Harga FROM tbstokwippacking WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Tambah1='Closed' ORDER BY Gudang"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbstokwippacking")
                        dgv1.DataSource = dsx.Tables("tbstokwippacking")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
    End Sub
    Private Sub isidgv_gudang_jeniskain()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,No_PO,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Customer,Id_Grey,Id_PO_Packing,Harga FROM tbstokwippacking WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Tambah1='Closed' ORDER BY Gudang"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbstokwippacking")
                        dgv1.DataSource = dsx.Tables("tbstokwippacking")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
    End Sub
   
    Private Sub dgv1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv1.MouseDoubleClick
        Try
            If txt_form.Text = "form_input_hasil_packing" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_hasil_packing.MdiParent = form_menu_utama
                    form_input_hasil_packing.Show()
                    form_input_hasil_packing.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_hasil_packing.txt_no_po.Text = .Cells(1).Value.ToString
                        form_input_hasil_packing.txt_jenis_kain.Text = .Cells(3).Value.ToString
                        form_input_hasil_packing.txt_warna.Text = .Cells(4).Value.ToString
                        form_input_hasil_packing.txt_resep.Text = .Cells(5).Value.ToString
                        form_input_hasil_packing.txt_partai.Text = .Cells(6).Value.ToString
                        form_input_hasil_packing.txt_asal_gulung.Text = .Cells(7).Value.ToString
                        form_input_hasil_packing.txt_qty_awal.Text = .Cells(8).Value.ToString
                        form_input_hasil_packing.cb_meter.Text = .Cells(9).Value.ToString
                        form_input_hasil_packing.txt_satuan_asal.Text = .Cells(9).Value.ToString
                        form_input_hasil_packing.txt_customer.Text = .Cells(10).Value.ToString
                        form_input_hasil_packing.txt_id_grey.Text = .Cells(11).Value.ToString
                        form_input_hasil_packing.txt_id_po_packing.Text = .Cells(12).Value.ToString
                        form_input_hasil_packing.txt_harga_asal.Text = .Cells(13).Value.ToString
                        form_input_hasil_packing.ts_hitung.Enabled = True
                        form_input_hasil_packing.Panel3.Enabled = True
                        form_input_hasil_packing.M1.Focus()
                    End With
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class