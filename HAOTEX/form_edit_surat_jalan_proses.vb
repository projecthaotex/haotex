﻿Imports MySql.Data.MySqlClient

Public Class form_edit_surat_jalan_proses

    Private Sub txt_meter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter.Text = String.Empty Then
                        txt_meter.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter.Select(txt_meter.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter.Text = String.Empty Then
                        txt_meter.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter.Select(txt_meter.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_meter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter.TextChanged
        If txt_meter.Text <> String.Empty Then
            Dim temp As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter.Select(txt_meter.Text.Length, 0)
            ElseIf txt_meter.Text = "-"c Then

            Else
                txt_meter.Text = CDec(temp).ToString("N0")
                txt_meter.Select(txt_meter.Text.Length, 0)
            End If
        End If
        txt_kg.Text = ""
        txt_gramasi.Text = ""
    End Sub

    Private Sub txt_kg_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kg.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kg.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kg.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kg.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kg.Text = String.Empty Then
                        txt_kg.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kg.Select(txt_kg.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kg.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kg.Text = String.Empty Then
                        txt_kg.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kg.Select(txt_kg.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_kg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kg.TextChanged
        If txt_kg.Text <> String.Empty Then
            Dim temp As String = txt_kg.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kg.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kg.Select(txt_kg.Text.Length, 0)
            ElseIf txt_kg.Text = "-"c Then

            Else
                txt_kg.Text = CDec(temp).ToString("N0")
                txt_kg.Select(txt_kg.Text.Length, 0)
            End If
        End If
        txt_harga.Text = ""
        txt_total_harga.Text = ""
        Dim kiloan As String = txt_kg.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_gramasi.Text = Math.Round(Val(kiloan.Replace(",", ".")) / Val(meter.Replace(",", ".")), 2)
    End Sub

    Private Sub txt_harga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga.Select(txt_harga.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga.Text = String.Empty Then
                        txt_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga.Select(txt_harga.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_harga_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga.TextChanged
        If txt_harga.Text <> String.Empty Then
            Dim temp As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga.Select(txt_harga.Text.Length, 0)
            ElseIf txt_harga.Text = "-"c Then

            Else
                txt_harga.Text = CDec(temp).ToString("N0")
                txt_harga.Select(txt_harga.Text.Length, 0)
            End If
        End If
        Dim kiloan As String = txt_kg.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga.Text = Math.Round(Val(harga.Replace(",", ".")) * Val(kiloan.Replace(",", ".")), 0)
    End Sub

    Private Sub txt_total_harga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga.Text = String.Empty Then
                        txt_total_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga.Select(txt_total_harga.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga.Text = String.Empty Then
                        txt_total_harga.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga.Select(txt_total_harga.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_total_harga_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga.TextChanged
        If txt_total_harga.Text <> String.Empty Then
            Dim temp As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga.Select(txt_total_harga.Text.Length, 0)
            ElseIf txt_total_harga.Text = "-"c Then

            Else
                txt_total_harga.Text = CDec(temp).ToString("N0")
                txt_total_harga.Select(txt_total_harga.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_sisa_meter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_meter.TextChanged
        If txt_sisa_meter.Text <> String.Empty Then
            Dim temp As String = txt_sisa_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_meter.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_meter.Select(txt_sisa_meter.Text.Length, 0)
            ElseIf txt_sisa_meter.Text = "-"c Then

            Else
                txt_sisa_meter.Text = CDec(temp).ToString("N0")
                txt_sisa_meter.Select(txt_sisa_meter.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Me.Close()
    End Sub

    Private Sub txt_gudang_packing_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_edit_surat_jalan_proses"
        form_input_gudang.Focus()
    End Sub

    Private Sub isi_dtp_jatuh_tempo()
        Dim tanggal As DateTime
        tanggal = dtp_tanggal.Text
        tanggal = tanggal.AddMonths(+1)
        dtp_jatuh_tempo.Text = tanggal
    End Sub

    Private Sub dtp_tanggal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_tanggal.TextChanged
        If dtp_tanggal.Value > dtp_jatuh_tempo.Value Then
            Call isi_dtp_jatuh_tempo()
        End If
    End Sub

    Private Sub dtp_jatuh_tempo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_jatuh_tempo.TextChanged
        If dtp_tanggal.Value > dtp_jatuh_tempo.Value Then
            Call isi_dtp_jatuh_tempo()
        End If
    End Sub

    Private Sub btn_simpan_tutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_tutup.Click
        Try
            Dim meter As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa As String = txt_sisa_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If Val(meter.Replace(",", ".")) > Val(sisa.Replace(",", ".")) Then
                MsgBox("Jumlah Meter Melebihi Stok Yang Ada")
            ElseIf Val(txt_meter.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter.Focus()
            ElseIf Val(txt_kg.Text) = 0 Then
                MsgBox("KG Tidak Boleh Kosong")
                txt_kg.Focus()
            ElseIf txt_partai.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai.Focus()
            ElseIf txt_gulung.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung.Focus()
            ElseIf txt_harga.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga.Focus()
            ElseIf txt_total_harga.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga.Focus()
            ElseIf txt_gudang_packing.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing.Focus()
            Else
                dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                dtp_tanggal_1.CustomFormat = "yyyy/MM/dd"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                dtp_jatuh_tempo_1.CustomFormat = "yyyy/MM/dd"
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim sqly = "UPDATE tbsuratjalanproses SET Partai=@1,Gulung=@2,Meter=@3,Kiloan=@4,Gramasi=@5,Harga=@6,Total_Harga=@7,Keterangan=@8 WHERE Tanggal='" & dtp_tanggal_1.Text & "' AND Jatuh_Tempo='" & dtp_jatuh_tempo_1.Text & "' AND Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_gudang.Text & "' AND SJ_Proses='" & txt_surat_jalan.Text & "' AND Baris_SJ='" & txt_baris_sj.Text & "' AND No_PO='" & txt_no_po.Text & "' AND Baris_PO='" & txt_baris_po.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND Warna='" & txt_warna.Text & "' AND Resep='" & txt_resep.Text & "'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            Dim meter_1 As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            Dim kiloan As String = txt_kg.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            Dim harga As String = txt_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            .Parameters.Clear()
                            .Parameters.AddWithValue("@1", txt_partai.Text)
                            .Parameters.AddWithValue("@2", txt_gulung.Text)
                            .Parameters.AddWithValue("@3", meter_1.Replace(",", "."))
                            .Parameters.AddWithValue("@4", kiloan.Replace(",", "."))
                            .Parameters.AddWithValue("@5", txt_gramasi.Text.Replace(",", "."))
                            .Parameters.AddWithValue("@6", harga.Replace(",", "."))
                            .Parameters.AddWithValue("@7", total_harga.Replace(",", "."))
                            .Parameters.AddWithValue("@8", txt_keterangan.Text)
                            .ExecuteNonQuery()
                        End With
                    End Using
                End Using
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim sqly = "UPDATE tbsuratjalanproses SET Tanggal=@1,Jatuh_Tempo=@2,Gudang_Packing=@3 WHERE Tanggal='" & dtp_tanggal_1.Text & "' AND Jatuh_Tempo='" & dtp_jatuh_tempo_1.Text & "' AND Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_gudang.Text & "' AND SJ_Proses='" & txt_surat_jalan.Text & "'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            .Parameters.Clear()
                            .Parameters.AddWithValue("@1", dtp_tanggal.Text)
                            .Parameters.AddWithValue("@2", dtp_jatuh_tempo.Text)
                            .Parameters.AddWithValue("@3", txt_gudang_packing.Text)
                            .ExecuteNonQuery()
                        End With
                    End Using
                End Using
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim sqly = "UPDATE tbdetilhutang SET Jumlah=@1 WHERE Tanggal='" & dtp_tanggal_1.Text & "' AND Jatuh_Tempo='" & dtp_jatuh_tempo_1.Text & "' AND Id_Grey='" & txt_id_grey.Text & "' AND Nama='" & txt_gudang.Text & "' AND SJ='" & txt_surat_jalan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND Keterangan='Proses' AND Tambah1='" & txt_baris_sj.Text & "'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            .Parameters.Clear()
                            .Parameters.AddWithValue("@1", total_harga.Replace(",", "."))
                            .ExecuteNonQuery()
                        End With
                    End Using
                End Using
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim sqly = "UPDATE tbdetilhutang SET Tanggal=@1,Jatuh_Tempo=@2 WHERE Tanggal='" & dtp_tanggal_1.Text & "' AND Jatuh_Tempo='" & dtp_jatuh_tempo_1.Text & "' AND Id_Grey='" & txt_id_grey.Text & "' AND Nama='" & txt_gudang.Text & "' AND SJ='" & txt_surat_jalan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND Keterangan='Proses'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            .Parameters.Clear()
                            .Parameters.AddWithValue("@1", dtp_tanggal.Text)
                            .Parameters.AddWithValue("@2", dtp_jatuh_tempo.Text)
                            .ExecuteNonQuery()
                        End With
                    End Using
                End Using
                Using cona As New MySqlConnection(sLocalConn)
                    cona.Open()
                    Dim sqla As String = "SELECT Sisa_Hutang FROM tbhutang WHERE Nama='" & txt_gudang.Text & "'"
                    Using cmda As New MySqlCommand(sqla, cona)
                        Using dra As MySqlDataReader = cmda.ExecuteReader
                            dra.Read()
                            If dra.HasRows Then
                                Dim h As Double
                                h = dra.Item(0)
                                Using cony As New MySqlConnection(sLocalConn)
                                    cony.Open()
                                    Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                                    Dim sqly = "UPDATE tbhutang SET Sisa_Hutang=@1 WHERE Nama='" & txt_gudang.Text & "'"
                                    Using cmdy As New MySqlCommand(sqly, cony)
                                        With cmdy
                                            .Parameters.Clear()
                                            If Val(total_harga.Replace(",", ".")) > Val(txt_total_harga_1.Text.Replace(",", ".")) Then
                                                .Parameters.AddWithValue("@1", (h + (Val(total_harga.Replace(",", ".")) - Val(txt_total_harga_1.Text.Replace(",", ".")))))
                                            Else
                                                .Parameters.AddWithValue("@1", (h - (Val(txt_total_harga_1.Text.Replace(",", ".")) - Val(total_harga.Replace(",", ".")))))
                                            End If
                                            .ExecuteNonQuery()
                                        End With
                                    End Using
                                End Using
                            End If
                        End Using
                    End Using
                End Using
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_gudang.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_sj_pembelian.Text & "'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            .Parameters.Clear()
                            Dim meter_1 As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            Dim sisa_1 As String = txt_sisa_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            .Parameters.AddWithValue("@1", (Val(sisa_1.Replace(",", ".")) - Val(meter_1.Replace(",", "."))))
                            .ExecuteNonQuery()
                        End With
                    End Using
                End Using
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sqly = "UPDATE tbstokexproses SET Partai=@1,Gulung=@2,Stok=@3,Harga=@4 WHERE Id_Grey='" & txt_id_grey.Text & "' AND SJ_Proses='" & txt_surat_jalan.Text & "' AND No_Urut='" & txt_baris_sj.Text & "' AND Gudang='" & txt_gudang_awal.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND Warna='" & txt_warna.Text & "' AND Resep='" & txt_resep.Text & "'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            .Parameters.Clear()
                            Dim meter_2 As String = txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            Dim total_harga_2 As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                            .Parameters.AddWithValue("@1", txt_partai.Text)
                            .Parameters.AddWithValue("@2", txt_gulung.Text)
                            .Parameters.AddWithValue("@3", meter_2.Replace(",", "."))
                            .Parameters.AddWithValue("@4", (Math.Round((Val(total_harga_2.Replace(",", ".")) / Val(meter_2.Replace(",", "."))) + Val(txt_harga_wip_proses.Text.Replace(",", ".")), 0)))
                            .ExecuteNonQuery()
                        End With
                    End Using
                End Using
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim total_harga As String = txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sqly = "UPDATE tbstokexproses SET Gudang=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND SJ_Proses='" & txt_surat_jalan.Text & "' AND Gudang='" & txt_gudang_awal.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            .Parameters.Clear()
                            .Parameters.AddWithValue("@1", txt_gudang_packing.Text)
                            .ExecuteNonQuery()
                        End With
                    End Using
                End Using
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_tanggal_1.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo_1.CustomFormat = "dd/MM/yyyy"
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
                MsgBox("Surat Jalan Proses berhasil di UBAH")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class