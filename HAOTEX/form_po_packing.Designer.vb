﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_po_packing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dtp_po = New System.Windows.Forms.DateTimePicker()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_gudang = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtp_akhir = New System.Windows.Forms.DateTimePicker()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_cari_po_packing = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ts_print = New System.Windows.Forms.ToolStripButton()
        Me.ts_perbarui = New System.Windows.Forms.ToolStripButton()
        Me.ts_hapus = New System.Windows.Forms.ToolStripButton()
        Me.ts_ubah = New System.Windows.Forms.ToolStripButton()
        Me.ts_baru = New System.Windows.Forms.ToolStripButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_print_po = New System.Windows.Forms.ToolStripButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txt_keluar_sj = New System.Windows.Forms.TextBox()
        Me.txt_belum_sj = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_total_qty = New System.Windows.Forms.TextBox()
        Me.txt_belum_sj_yard = New System.Windows.Forms.TextBox()
        Me.txt_keluar_sj_yard = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_total_qty_yard = New System.Windows.Forms.TextBox()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(214, 63)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(1079, 466)
        Me.dgv1.TabIndex = 23
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1179, 29)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(104, 20)
        Me.dtp_hari_ini.TabIndex = 27
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 77)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "s/d"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.dtp_po)
        Me.Panel1.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_cari_gudang)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.dtp_akhir)
        Me.Panel1.Controls.Add(Me.dtp_awal)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txt_cari_po_packing)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 63)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 466)
        Me.Panel1.TabIndex = 24
        '
        'dtp_po
        '
        Me.dtp_po.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_po.Location = New System.Drawing.Point(14, 177)
        Me.dtp_po.Name = "dtp_po"
        Me.dtp_po.Size = New System.Drawing.Size(80, 20)
        Me.dtp_po.TabIndex = 12
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(14, 269)
        Me.txt_cari_jenis_kain.MaxLength = 100
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 11
        Me.txt_cari_jenis_kain.Text = "< Jenis Kain >"
        '
        'txt_cari_gudang
        '
        Me.txt_cari_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_gudang.Location = New System.Drawing.Point(14, 223)
        Me.txt_cari_gudang.MaxLength = 100
        Me.txt_cari_gudang.Name = "txt_cari_gudang"
        Me.txt_cari_gudang.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_gudang.TabIndex = 10
        Me.txt_cari_gudang.Text = "< Gudang >"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Dari"
        '
        'dtp_akhir
        '
        Me.dtp_akhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_akhir.Location = New System.Drawing.Point(43, 73)
        Me.dtp_akhir.Name = "dtp_akhir"
        Me.dtp_akhir.Size = New System.Drawing.Size(99, 20)
        Me.dtp_akhir.TabIndex = 5
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(43, 46)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tanggal"
        '
        'txt_cari_po_packing
        '
        Me.txt_cari_po_packing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_po_packing.Location = New System.Drawing.Point(100, 177)
        Me.txt_cari_po_packing.Name = "txt_cari_po_packing"
        Me.txt_cari_po_packing.Size = New System.Drawing.Size(85, 20)
        Me.txt_cari_po_packing.TabIndex = 1
        Me.txt_cari_po_packing.Text = "< No PO >"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 139)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cari :"
        '
        'ts_print
        '
        Me.ts_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print.Name = "ts_print"
        Me.ts_print.Size = New System.Drawing.Size(52, 22)
        Me.ts_print.Text = "Print"
        '
        'ts_perbarui
        '
        Me.ts_perbarui.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.ts_perbarui.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_perbarui.Name = "ts_perbarui"
        Me.ts_perbarui.Size = New System.Drawing.Size(71, 22)
        Me.ts_perbarui.Text = "Perbarui"
        '
        'ts_hapus
        '
        Me.ts_hapus.Enabled = False
        Me.ts_hapus.Image = Global.HAOTEX.My.Resources.Resources.application_remove
        Me.ts_hapus.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_hapus.Name = "ts_hapus"
        Me.ts_hapus.Size = New System.Drawing.Size(61, 22)
        Me.ts_hapus.Text = "Hapus"
        '
        'ts_ubah
        '
        Me.ts_ubah.Enabled = False
        Me.ts_ubah.Image = Global.HAOTEX.My.Resources.Resources.application_edit
        Me.ts_ubah.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_ubah.Name = "ts_ubah"
        Me.ts_ubah.Size = New System.Drawing.Size(55, 22)
        Me.ts_ubah.Text = "Ubah"
        '
        'ts_baru
        '
        Me.ts_baru.Image = Global.HAOTEX.My.Resources.Resources.application_add
        Me.ts_baru.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_baru.Name = "ts_baru"
        Me.ts_baru.Size = New System.Drawing.Size(51, 22)
        Me.ts_baru.Text = "Baru"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label6.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Window
        Me.Label6.Location = New System.Drawing.Point(12, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(1281, 34)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "PO PACKING"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_baru, Me.ts_ubah, Me.ts_hapus, Me.ts_perbarui, Me.ts_print, Me.ts_print_po})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1305, 25)
        Me.ToolStrip1.TabIndex = 22
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_print_po
        '
        Me.ts_print_po.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print_po.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print_po.Name = "ts_print_po"
        Me.ts_print_po.Size = New System.Drawing.Size(71, 22)
        Me.ts_print_po.Text = "Print PO"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.txt_keluar_sj_yard)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.txt_total_qty_yard)
        Me.Panel3.Controls.Add(Me.txt_keluar_sj)
        Me.Panel3.Controls.Add(Me.txt_belum_sj)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.txt_total_qty)
        Me.Panel3.Controls.Add(Me.txt_belum_sj_yard)
        Me.Panel3.Location = New System.Drawing.Point(12, 531)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1281, 66)
        Me.Panel3.TabIndex = 28
        '
        'txt_keluar_sj
        '
        Me.txt_keluar_sj.BackColor = System.Drawing.SystemColors.Window
        Me.txt_keluar_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keluar_sj.Location = New System.Drawing.Point(392, 30)
        Me.txt_keluar_sj.Name = "txt_keluar_sj"
        Me.txt_keluar_sj.ReadOnly = True
        Me.txt_keluar_sj.Size = New System.Drawing.Size(150, 20)
        Me.txt_keluar_sj.TabIndex = 28
        Me.txt_keluar_sj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_belum_sj
        '
        Me.txt_belum_sj.BackColor = System.Drawing.SystemColors.Window
        Me.txt_belum_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_belum_sj.Location = New System.Drawing.Point(234, 30)
        Me.txt_belum_sj.Name = "txt_belum_sj"
        Me.txt_belum_sj.ReadOnly = True
        Me.txt_belum_sj.Size = New System.Drawing.Size(150, 20)
        Me.txt_belum_sj.TabIndex = 27
        Me.txt_belum_sj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(403, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(128, 13)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Sudah Keluar SJ Packing"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(246, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Belum Keluar SJ Packing"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(574, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(98, 13)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "Total QTY ( Meter )"
        '
        'txt_total_qty
        '
        Me.txt_total_qty.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_qty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_qty.Location = New System.Drawing.Point(548, 30)
        Me.txt_total_qty.Name = "txt_total_qty"
        Me.txt_total_qty.ReadOnly = True
        Me.txt_total_qty.Size = New System.Drawing.Size(150, 20)
        Me.txt_total_qty.TabIndex = 22
        Me.txt_total_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_belum_sj_yard
        '
        Me.txt_belum_sj_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_belum_sj_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_belum_sj_yard.Location = New System.Drawing.Point(773, 30)
        Me.txt_belum_sj_yard.Name = "txt_belum_sj_yard"
        Me.txt_belum_sj_yard.ReadOnly = True
        Me.txt_belum_sj_yard.Size = New System.Drawing.Size(150, 20)
        Me.txt_belum_sj_yard.TabIndex = 34
        Me.txt_belum_sj_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keluar_sj_yard
        '
        Me.txt_keluar_sj_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_keluar_sj_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keluar_sj_yard.Location = New System.Drawing.Point(931, 30)
        Me.txt_keluar_sj_yard.Name = "txt_keluar_sj_yard"
        Me.txt_keluar_sj_yard.ReadOnly = True
        Me.txt_keluar_sj_yard.Size = New System.Drawing.Size(150, 20)
        Me.txt_keluar_sj_yard.TabIndex = 33
        Me.txt_keluar_sj_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(942, 14)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(128, 13)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "Sudah Keluar SJ Packing"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(785, 14)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(126, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Belum Keluar SJ Packing"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(1118, 14)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(93, 13)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Total QTY ( Yard )"
        '
        'txt_total_qty_yard
        '
        Me.txt_total_qty_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_qty_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_qty_yard.Location = New System.Drawing.Point(1089, 30)
        Me.txt_total_qty_yard.Name = "txt_total_qty_yard"
        Me.txt_total_qty_yard.ReadOnly = True
        Me.txt_total_qty_yard.Size = New System.Drawing.Size(150, 20)
        Me.txt_total_qty_yard.TabIndex = 29
        Me.txt_total_qty_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'form_po_packing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 609)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_po_packing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtp_akhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_cari_po_packing As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ts_print As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_perbarui As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_hapus As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_ubah As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_baru As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txt_keluar_sj As System.Windows.Forms.TextBox
    Friend WithEvents txt_belum_sj As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_total_qty As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_gudang As System.Windows.Forms.TextBox
    Friend WithEvents ts_print_po As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtp_po As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_belum_sj_yard As System.Windows.Forms.TextBox
    Friend WithEvents txt_keluar_sj_yard As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_total_qty_yard As System.Windows.Forms.TextBox
End Class
