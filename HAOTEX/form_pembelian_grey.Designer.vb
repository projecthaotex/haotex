﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_pembelian_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.txt_cari_penyimpanan = New System.Windows.Forms.TextBox()
        Me.cb_non_kontrak = New System.Windows.Forms.CheckBox()
        Me.cb_kontrak = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtp_akhir = New System.Windows.Forms.DateTimePicker()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_supplier = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btn_cari = New System.Windows.Forms.Button()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_baru = New System.Windows.Forms.ToolStripButton()
        Me.ts_ubah = New System.Windows.Forms.ToolStripButton()
        Me.ts_hapus = New System.Windows.Forms.ToolStripButton()
        Me.ts_perbarui = New System.Windows.Forms.ToolStripButton()
        Me.ts_print = New System.Windows.Forms.ToolStripButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txt_grand_total = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_jumlah_yard = New System.Windows.Forms.TextBox()
        Me.txt_qty_yard = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_jumlah_total_harga = New System.Windows.Forms.TextBox()
        Me.txt_jumlah_qty = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label6.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Window
        Me.Label6.Location = New System.Drawing.Point(11, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(1282, 34)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "PEMBELIAN GREY"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1182, 37)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(99, 20)
        Me.dtp_hari_ini.TabIndex = 19
        '
        'txt_cari_penyimpanan
        '
        Me.txt_cari_penyimpanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_penyimpanan.Location = New System.Drawing.Point(15, 294)
        Me.txt_cari_penyimpanan.Name = "txt_cari_penyimpanan"
        Me.txt_cari_penyimpanan.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_penyimpanan.TabIndex = 10
        '
        'cb_non_kontrak
        '
        Me.cb_non_kontrak.AutoSize = True
        Me.cb_non_kontrak.Checked = True
        Me.cb_non_kontrak.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cb_non_kontrak.Location = New System.Drawing.Point(15, 135)
        Me.cb_non_kontrak.Name = "cb_non_kontrak"
        Me.cb_non_kontrak.Size = New System.Drawing.Size(86, 17)
        Me.cb_non_kontrak.TabIndex = 9
        Me.cb_non_kontrak.Text = "Non Kontrak"
        Me.cb_non_kontrak.UseVisualStyleBackColor = True
        '
        'cb_kontrak
        '
        Me.cb_kontrak.AutoSize = True
        Me.cb_kontrak.Checked = True
        Me.cb_kontrak.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cb_kontrak.Location = New System.Drawing.Point(15, 111)
        Me.cb_kontrak.Name = "cb_kontrak"
        Me.cb_kontrak.Size = New System.Drawing.Size(63, 17)
        Me.cb_kontrak.TabIndex = 8
        Me.cb_kontrak.Text = "Kontrak"
        Me.cb_kontrak.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 61)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "s/d"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Dari"
        '
        'dtp_akhir
        '
        Me.dtp_akhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_akhir.Location = New System.Drawing.Point(45, 57)
        Me.dtp_akhir.Name = "dtp_akhir"
        Me.dtp_akhir.Size = New System.Drawing.Size(99, 20)
        Me.dtp_akhir.TabIndex = 5
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(45, 30)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(15, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tanggal"
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(15, 240)
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 2
        '
        'txt_cari_supplier
        '
        Me.txt_cari_supplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_supplier.Location = New System.Drawing.Point(15, 186)
        Me.txt_cari_supplier.Name = "txt_cari_supplier"
        Me.txt_cari_supplier.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_supplier.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 171)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Supplier"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.btn_cari)
        Me.Panel1.Location = New System.Drawing.Point(11, 61)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 536)
        Me.Panel1.TabIndex = 17
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.txt_cari_penyimpanan)
        Me.Panel3.Controls.Add(Me.cb_non_kontrak)
        Me.Panel3.Controls.Add(Me.cb_kontrak)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.dtp_akhir)
        Me.Panel3.Controls.Add(Me.dtp_awal)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel3.Controls.Add(Me.txt_cari_supplier)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Location = New System.Drawing.Point(-1, 16)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(200, 336)
        Me.Panel3.TabIndex = 15
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 279)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Gudang"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 225)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(55, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Jenis Kain"
        '
        'btn_cari
        '
        Me.btn_cari.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cari.Image = Global.HAOTEX.My.Resources.Resources.search
        Me.btn_cari.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_cari.Location = New System.Drawing.Point(62, 375)
        Me.btn_cari.Name = "btn_cari"
        Me.btn_cari.Size = New System.Drawing.Size(75, 43)
        Me.btn_cari.TabIndex = 14
        Me.btn_cari.Text = "CARI"
        Me.btn_cari.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_cari.UseVisualStyleBackColor = False
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv1.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgv1.GridColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgv1.Location = New System.Drawing.Point(213, 61)
        Me.dgv1.MultiSelect = False
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(1080, 470)
        Me.dgv1.TabIndex = 16
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_baru, Me.ts_ubah, Me.ts_hapus, Me.ts_perbarui, Me.ts_print})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1305, 25)
        Me.ToolStrip1.TabIndex = 15
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_baru
        '
        Me.ts_baru.Image = Global.HAOTEX.My.Resources.Resources.application_add
        Me.ts_baru.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_baru.Name = "ts_baru"
        Me.ts_baru.Size = New System.Drawing.Size(60, 22)
        Me.ts_baru.Text = "Baru   "
        '
        'ts_ubah
        '
        Me.ts_ubah.Image = Global.HAOTEX.My.Resources.Resources.application_edit
        Me.ts_ubah.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_ubah.Name = "ts_ubah"
        Me.ts_ubah.Size = New System.Drawing.Size(64, 22)
        Me.ts_ubah.Text = "Ubah   "
        '
        'ts_hapus
        '
        Me.ts_hapus.Image = Global.HAOTEX.My.Resources.Resources.application_remove
        Me.ts_hapus.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_hapus.Name = "ts_hapus"
        Me.ts_hapus.Size = New System.Drawing.Size(70, 22)
        Me.ts_hapus.Text = "Hapus   "
        '
        'ts_perbarui
        '
        Me.ts_perbarui.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.ts_perbarui.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_perbarui.Name = "ts_perbarui"
        Me.ts_perbarui.Size = New System.Drawing.Size(80, 22)
        Me.ts_perbarui.Text = "Perbarui   "
        '
        'ts_print
        '
        Me.ts_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print.Name = "ts_print"
        Me.ts_print.Size = New System.Drawing.Size(61, 22)
        Me.ts_print.Text = "Print   "
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.txt_grand_total)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.txt_jumlah_yard)
        Me.Panel2.Controls.Add(Me.txt_qty_yard)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.txt_jumlah_total_harga)
        Me.Panel2.Controls.Add(Me.txt_jumlah_qty)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(213, 533)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1080, 64)
        Me.Panel2.TabIndex = 21
        '
        'txt_grand_total
        '
        Me.txt_grand_total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_grand_total.Location = New System.Drawing.Point(857, 29)
        Me.txt_grand_total.Name = "txt_grand_total"
        Me.txt_grand_total.Size = New System.Drawing.Size(200, 20)
        Me.txt_grand_total.TabIndex = 11
        Me.txt_grand_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(903, 13)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(115, 13)
        Me.Label12.TabIndex = 9
        Me.Label12.Text = "Grand Total Pembelian"
        '
        'txt_jumlah_yard
        '
        Me.txt_jumlah_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_yard.Location = New System.Drawing.Point(593, 29)
        Me.txt_jumlah_yard.Name = "txt_jumlah_yard"
        Me.txt_jumlah_yard.Size = New System.Drawing.Size(200, 20)
        Me.txt_jumlah_yard.TabIndex = 7
        Me.txt_jumlah_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_yard
        '
        Me.txt_qty_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_yard.Location = New System.Drawing.Point(437, 29)
        Me.txt_qty_yard.Name = "txt_qty_yard"
        Me.txt_qty_yard.Size = New System.Drawing.Size(150, 20)
        Me.txt_qty_yard.TabIndex = 6
        Me.txt_qty_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(637, 13)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(112, 13)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Total Pembelian (yard)"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(473, 13)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Total Qty (yard)"
        '
        'txt_jumlah_total_harga
        '
        Me.txt_jumlah_total_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_total_harga.Location = New System.Drawing.Point(173, 29)
        Me.txt_jumlah_total_harga.Name = "txt_jumlah_total_harga"
        Me.txt_jumlah_total_harga.Size = New System.Drawing.Size(200, 20)
        Me.txt_jumlah_total_harga.TabIndex = 3
        Me.txt_jumlah_total_harga.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jumlah_qty
        '
        Me.txt_jumlah_qty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_qty.Location = New System.Drawing.Point(17, 29)
        Me.txt_jumlah_qty.Name = "txt_jumlah_qty"
        Me.txt_jumlah_qty.Size = New System.Drawing.Size(150, 20)
        Me.txt_jumlah_qty.TabIndex = 2
        Me.txt_jumlah_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(214, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(118, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Total Pembelian (meter)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(50, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Total Qty (meter)"
        '
        'form_pembelian_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 606)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_pembelian_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents ts_print As System.Windows.Forms.ToolStripButton
    Friend WithEvents txt_cari_penyimpanan As System.Windows.Forms.TextBox
    Friend WithEvents cb_non_kontrak As System.Windows.Forms.CheckBox
    Friend WithEvents cb_kontrak As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtp_akhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents ts_perbarui As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_hapus As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_ubah As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_baru As System.Windows.Forms.ToolStripButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_supplier As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents btn_cari As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_jumlah_total_harga As System.Windows.Forms.TextBox
    Friend WithEvents txt_jumlah_qty As System.Windows.Forms.TextBox
    Friend WithEvents txt_grand_total As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_jumlah_yard As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_yard As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
