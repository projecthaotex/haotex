﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_menu_utama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_menu_utama))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SISTEMToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LOGINToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LOGOUTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenggunaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.KELUARToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GREYToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KONTRAKToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PROGRESSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProsesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.POProsesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SuratJalanProsesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PACKINGToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.POPackingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HasilPackingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StokToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StokProsesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StokProsesAdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StokWIPProsesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StokExProsesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StokWIPPackingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GradeAToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GradeBToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClaimJadiToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClaimCelupToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupplierToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JenisKainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GudangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustomerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DataWarnaDanResepToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PiutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PembayaranHutangToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenjualanToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenjualanGreyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PenjualanBarangJadiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LainlainToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TranGudangsferToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrackingIDGreyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.Status1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Status2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SISTEMToolStripMenuItem, Me.GREYToolStripMenuItem, Me.ProsesToolStripMenuItem, Me.PACKINGToolStripMenuItem, Me.StokToolStripMenuItem, Me.DataToolStripMenuItem, Me.ReportToolStripMenuItem, Me.PenjualanToolStripMenuItem1, Me.LainlainToolStripMenuItem})
        Me.MenuStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1322, 27)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SISTEMToolStripMenuItem
        '
        Me.SISTEMToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LOGINToolStripMenuItem, Me.LOGOUTToolStripMenuItem, Me.PenggunaToolStripMenuItem, Me.ToolStripMenuItem1, Me.KELUARToolStripMenuItem})
        Me.SISTEMToolStripMenuItem.Name = "SISTEMToolStripMenuItem"
        Me.SISTEMToolStripMenuItem.Size = New System.Drawing.Size(67, 23)
        Me.SISTEMToolStripMenuItem.Text = "System"
        '
        'LOGINToolStripMenuItem
        '
        Me.LOGINToolStripMenuItem.Name = "LOGINToolStripMenuItem"
        Me.LOGINToolStripMenuItem.Size = New System.Drawing.Size(142, 24)
        Me.LOGINToolStripMenuItem.Text = "Login"
        '
        'LOGOUTToolStripMenuItem
        '
        Me.LOGOUTToolStripMenuItem.Name = "LOGOUTToolStripMenuItem"
        Me.LOGOUTToolStripMenuItem.Size = New System.Drawing.Size(142, 24)
        Me.LOGOUTToolStripMenuItem.Text = "Logout"
        '
        'PenggunaToolStripMenuItem
        '
        Me.PenggunaToolStripMenuItem.Name = "PenggunaToolStripMenuItem"
        Me.PenggunaToolStripMenuItem.Size = New System.Drawing.Size(142, 24)
        Me.PenggunaToolStripMenuItem.Text = "Pengguna"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(139, 6)
        '
        'KELUARToolStripMenuItem
        '
        Me.KELUARToolStripMenuItem.Name = "KELUARToolStripMenuItem"
        Me.KELUARToolStripMenuItem.Size = New System.Drawing.Size(142, 24)
        Me.KELUARToolStripMenuItem.Text = "Keluar"
        '
        'GREYToolStripMenuItem
        '
        Me.GREYToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.KONTRAKToolStripMenuItem, Me.PROGRESSToolStripMenuItem})
        Me.GREYToolStripMenuItem.Name = "GREYToolStripMenuItem"
        Me.GREYToolStripMenuItem.Size = New System.Drawing.Size(51, 23)
        Me.GREYToolStripMenuItem.Text = "Grey"
        '
        'KONTRAKToolStripMenuItem
        '
        Me.KONTRAKToolStripMenuItem.Name = "KONTRAKToolStripMenuItem"
        Me.KONTRAKToolStripMenuItem.Size = New System.Drawing.Size(180, 24)
        Me.KONTRAKToolStripMenuItem.Text = "Kontrak Grey"
        '
        'PROGRESSToolStripMenuItem
        '
        Me.PROGRESSToolStripMenuItem.Name = "PROGRESSToolStripMenuItem"
        Me.PROGRESSToolStripMenuItem.Size = New System.Drawing.Size(180, 24)
        Me.PROGRESSToolStripMenuItem.Text = "Pembelian Grey"
        '
        'ProsesToolStripMenuItem
        '
        Me.ProsesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.POProsesToolStripMenuItem, Me.SuratJalanProsesToolStripMenuItem})
        Me.ProsesToolStripMenuItem.Name = "ProsesToolStripMenuItem"
        Me.ProsesToolStripMenuItem.Size = New System.Drawing.Size(64, 23)
        Me.ProsesToolStripMenuItem.Text = "Proses"
        '
        'POProsesToolStripMenuItem
        '
        Me.POProsesToolStripMenuItem.Name = "POProsesToolStripMenuItem"
        Me.POProsesToolStripMenuItem.Size = New System.Drawing.Size(195, 24)
        Me.POProsesToolStripMenuItem.Text = "PO Proses"
        '
        'SuratJalanProsesToolStripMenuItem
        '
        Me.SuratJalanProsesToolStripMenuItem.Name = "SuratJalanProsesToolStripMenuItem"
        Me.SuratJalanProsesToolStripMenuItem.Size = New System.Drawing.Size(195, 24)
        Me.SuratJalanProsesToolStripMenuItem.Text = "Surat Jalan Proses"
        '
        'PACKINGToolStripMenuItem
        '
        Me.PACKINGToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.POPackingToolStripMenuItem, Me.HasilPackingToolStripMenuItem})
        Me.PACKINGToolStripMenuItem.Name = "PACKINGToolStripMenuItem"
        Me.PACKINGToolStripMenuItem.Size = New System.Drawing.Size(71, 23)
        Me.PACKINGToolStripMenuItem.Text = "Packing"
        '
        'POPackingToolStripMenuItem
        '
        Me.POPackingToolStripMenuItem.Name = "POPackingToolStripMenuItem"
        Me.POPackingToolStripMenuItem.Size = New System.Drawing.Size(165, 24)
        Me.POPackingToolStripMenuItem.Text = "PO Packing"
        '
        'HasilPackingToolStripMenuItem
        '
        Me.HasilPackingToolStripMenuItem.Name = "HasilPackingToolStripMenuItem"
        Me.HasilPackingToolStripMenuItem.Size = New System.Drawing.Size(165, 24)
        Me.HasilPackingToolStripMenuItem.Text = "Hasil Packing"
        '
        'StokToolStripMenuItem
        '
        Me.StokToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StokProsesToolStripMenuItem, Me.StokProsesAdminToolStripMenuItem, Me.StokWIPProsesToolStripMenuItem, Me.StokExProsesToolStripMenuItem, Me.StokWIPPackingToolStripMenuItem, Me.GradeAToolStripMenuItem1, Me.GradeBToolStripMenuItem1, Me.ClaimJadiToolStripMenuItem1, Me.ClaimCelupToolStripMenuItem1})
        Me.StokToolStripMenuItem.Name = "StokToolStripMenuItem"
        Me.StokToolStripMenuItem.Size = New System.Drawing.Size(48, 23)
        Me.StokToolStripMenuItem.Text = "Stok"
        '
        'StokProsesToolStripMenuItem
        '
        Me.StokProsesToolStripMenuItem.Name = "StokProsesToolStripMenuItem"
        Me.StokProsesToolStripMenuItem.Size = New System.Drawing.Size(189, 24)
        Me.StokProsesToolStripMenuItem.Text = "Stok Proses"
        '
        'StokProsesAdminToolStripMenuItem
        '
        Me.StokProsesAdminToolStripMenuItem.Name = "StokProsesAdminToolStripMenuItem"
        Me.StokProsesAdminToolStripMenuItem.Size = New System.Drawing.Size(189, 24)
        Me.StokProsesAdminToolStripMenuItem.Text = "Stok Proses"
        '
        'StokWIPProsesToolStripMenuItem
        '
        Me.StokWIPProsesToolStripMenuItem.Name = "StokWIPProsesToolStripMenuItem"
        Me.StokWIPProsesToolStripMenuItem.Size = New System.Drawing.Size(189, 24)
        Me.StokWIPProsesToolStripMenuItem.Text = "Stok WIP Proses"
        Me.StokWIPProsesToolStripMenuItem.Visible = False
        '
        'StokExProsesToolStripMenuItem
        '
        Me.StokExProsesToolStripMenuItem.Name = "StokExProsesToolStripMenuItem"
        Me.StokExProsesToolStripMenuItem.Size = New System.Drawing.Size(189, 24)
        Me.StokExProsesToolStripMenuItem.Text = "Stok Ex Proses"
        '
        'StokWIPPackingToolStripMenuItem
        '
        Me.StokWIPPackingToolStripMenuItem.Name = "StokWIPPackingToolStripMenuItem"
        Me.StokWIPPackingToolStripMenuItem.Size = New System.Drawing.Size(189, 24)
        Me.StokWIPPackingToolStripMenuItem.Text = "Stok WIP Packing"
        '
        'GradeAToolStripMenuItem1
        '
        Me.GradeAToolStripMenuItem1.Name = "GradeAToolStripMenuItem1"
        Me.GradeAToolStripMenuItem1.Size = New System.Drawing.Size(189, 24)
        Me.GradeAToolStripMenuItem1.Text = "Stok Grade A"
        '
        'GradeBToolStripMenuItem1
        '
        Me.GradeBToolStripMenuItem1.Name = "GradeBToolStripMenuItem1"
        Me.GradeBToolStripMenuItem1.Size = New System.Drawing.Size(189, 24)
        Me.GradeBToolStripMenuItem1.Text = "Stok Grade B"
        '
        'ClaimJadiToolStripMenuItem1
        '
        Me.ClaimJadiToolStripMenuItem1.Name = "ClaimJadiToolStripMenuItem1"
        Me.ClaimJadiToolStripMenuItem1.Size = New System.Drawing.Size(189, 24)
        Me.ClaimJadiToolStripMenuItem1.Text = "Stok Claim Jadi"
        '
        'ClaimCelupToolStripMenuItem1
        '
        Me.ClaimCelupToolStripMenuItem1.Name = "ClaimCelupToolStripMenuItem1"
        Me.ClaimCelupToolStripMenuItem1.Size = New System.Drawing.Size(189, 24)
        Me.ClaimCelupToolStripMenuItem1.Text = "Stok Claim Celup"
        '
        'DataToolStripMenuItem
        '
        Me.DataToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SupplierToolStripMenuItem, Me.JenisKainToolStripMenuItem, Me.GudangToolStripMenuItem, Me.CustomerToolStripMenuItem, Me.DataWarnaDanResepToolStripMenuItem})
        Me.DataToolStripMenuItem.Name = "DataToolStripMenuItem"
        Me.DataToolStripMenuItem.Size = New System.Drawing.Size(52, 23)
        Me.DataToolStripMenuItem.Text = "Data"
        '
        'SupplierToolStripMenuItem
        '
        Me.SupplierToolStripMenuItem.Name = "SupplierToolStripMenuItem"
        Me.SupplierToolStripMenuItem.Size = New System.Drawing.Size(229, 24)
        Me.SupplierToolStripMenuItem.Text = "Data Supplier"
        '
        'JenisKainToolStripMenuItem
        '
        Me.JenisKainToolStripMenuItem.Name = "JenisKainToolStripMenuItem"
        Me.JenisKainToolStripMenuItem.Size = New System.Drawing.Size(229, 24)
        Me.JenisKainToolStripMenuItem.Text = "Data Jenis Kain"
        '
        'GudangToolStripMenuItem
        '
        Me.GudangToolStripMenuItem.Name = "GudangToolStripMenuItem"
        Me.GudangToolStripMenuItem.Size = New System.Drawing.Size(229, 24)
        Me.GudangToolStripMenuItem.Text = "Data Gudang"
        '
        'CustomerToolStripMenuItem
        '
        Me.CustomerToolStripMenuItem.Name = "CustomerToolStripMenuItem"
        Me.CustomerToolStripMenuItem.Size = New System.Drawing.Size(229, 24)
        Me.CustomerToolStripMenuItem.Text = "Data Customer"
        '
        'DataWarnaDanResepToolStripMenuItem
        '
        Me.DataWarnaDanResepToolStripMenuItem.Name = "DataWarnaDanResepToolStripMenuItem"
        Me.DataWarnaDanResepToolStripMenuItem.Size = New System.Drawing.Size(229, 24)
        Me.DataWarnaDanResepToolStripMenuItem.Text = "Data Warna Dan Resep"
        '
        'ReportToolStripMenuItem
        '
        Me.ReportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HutangToolStripMenuItem, Me.PiutangToolStripMenuItem, Me.PembayaranHutangToolStripMenuItem, Me.ToolStripMenuItem2})
        Me.ReportToolStripMenuItem.Name = "ReportToolStripMenuItem"
        Me.ReportToolStripMenuItem.Size = New System.Drawing.Size(85, 23)
        Me.ReportToolStripMenuItem.Text = "Keuangan"
        '
        'HutangToolStripMenuItem
        '
        Me.HutangToolStripMenuItem.Name = "HutangToolStripMenuItem"
        Me.HutangToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.HutangToolStripMenuItem.Text = "Hutang"
        '
        'PiutangToolStripMenuItem
        '
        Me.PiutangToolStripMenuItem.Name = "PiutangToolStripMenuItem"
        Me.PiutangToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.PiutangToolStripMenuItem.Text = "Piutang"
        '
        'PembayaranHutangToolStripMenuItem
        '
        Me.PembayaranHutangToolStripMenuItem.Name = "PembayaranHutangToolStripMenuItem"
        Me.PembayaranHutangToolStripMenuItem.Size = New System.Drawing.Size(211, 24)
        Me.PembayaranHutangToolStripMenuItem.Text = "Pembayaran Hutang"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(211, 24)
        Me.ToolStripMenuItem2.Text = "Pembayaran Piutang"
        '
        'PenjualanToolStripMenuItem1
        '
        Me.PenjualanToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PenjualanGreyToolStripMenuItem, Me.PenjualanBarangJadiToolStripMenuItem})
        Me.PenjualanToolStripMenuItem1.Name = "PenjualanToolStripMenuItem1"
        Me.PenjualanToolStripMenuItem1.Size = New System.Drawing.Size(85, 23)
        Me.PenjualanToolStripMenuItem1.Text = "Penjualan"
        '
        'PenjualanGreyToolStripMenuItem
        '
        Me.PenjualanGreyToolStripMenuItem.Name = "PenjualanGreyToolStripMenuItem"
        Me.PenjualanGreyToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.PenjualanGreyToolStripMenuItem.Text = "Penjualan Grey"
        '
        'PenjualanBarangJadiToolStripMenuItem
        '
        Me.PenjualanBarangJadiToolStripMenuItem.Name = "PenjualanBarangJadiToolStripMenuItem"
        Me.PenjualanBarangJadiToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.PenjualanBarangJadiToolStripMenuItem.Text = "Penjualan Barang Jadi"
        '
        'LainlainToolStripMenuItem
        '
        Me.LainlainToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TranGudangsferToolStripMenuItem, Me.TrackingIDGreyToolStripMenuItem})
        Me.LainlainToolStripMenuItem.Name = "LainlainToolStripMenuItem"
        Me.LainlainToolStripMenuItem.Size = New System.Drawing.Size(77, 23)
        Me.LainlainToolStripMenuItem.Text = "Lain-lain"
        '
        'TranGudangsferToolStripMenuItem
        '
        Me.TranGudangsferToolStripMenuItem.Name = "TranGudangsferToolStripMenuItem"
        Me.TranGudangsferToolStripMenuItem.Size = New System.Drawing.Size(185, 24)
        Me.TranGudangsferToolStripMenuItem.Text = "Transfer Gudang"
        '
        'TrackingIDGreyToolStripMenuItem
        '
        Me.TrackingIDGreyToolStripMenuItem.Name = "TrackingIDGreyToolStripMenuItem"
        Me.TrackingIDGreyToolStripMenuItem.Size = New System.Drawing.Size(185, 24)
        Me.TrackingIDGreyToolStripMenuItem.Text = "Tracking ID Grey"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Status1, Me.Status2})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 574)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 12, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1322, 22)
        Me.StatusStrip1.TabIndex = 3
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'Status1
        '
        Me.Status1.Name = "Status1"
        Me.Status1.Size = New System.Drawing.Size(45, 17)
        Me.Status1.Text = "Status1"
        '
        'Status2
        '
        Me.Status2.Name = "Status2"
        Me.Status2.Size = New System.Drawing.Size(45, 17)
        Me.Status2.Text = "Status2"
        '
        'form_menu_utama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1322, 596)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "form_menu_utama"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "H A O T E X   v.1.0  Beta Rev 8"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SISTEMToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LOGINToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LOGOUTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenggunaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents KELUARToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GREYToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KONTRAKToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PROGRESSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProsesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents POProsesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SuratJalanProsesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PACKINGToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents POPackingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HasilPackingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SupplierToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents JenisKainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GudangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustomerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PiutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LainlainToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TranGudangsferToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StokToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PembayaranHutangToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrackingIDGreyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents Status1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Status2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents PenjualanToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenjualanGreyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PenjualanBarangJadiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataWarnaDanResepToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StokProsesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StokWIPProsesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StokExProsesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StokWIPPackingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GradeAToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GradeBToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClaimJadiToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClaimCelupToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StokProsesAdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
