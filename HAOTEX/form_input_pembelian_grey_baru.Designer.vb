﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_pembelian_grey_baru
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_input_pembelian_grey_baru))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.dtp_jatuh_tempo = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_surat_jalan = New System.Windows.Forms.TextBox()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_id_grey_1 = New System.Windows.Forms.TextBox()
        Me.rb_non_kontrak_1 = New System.Windows.Forms.RadioButton()
        Me.rb_kontrak_1 = New System.Windows.Forms.RadioButton()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.txt_no_kontrak_1 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_1 = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btn_batal_8 = New System.Windows.Forms.Button()
        Me.btn_batal_7 = New System.Windows.Forms.Button()
        Me.btn_batal_6 = New System.Windows.Forms.Button()
        Me.btn_batal_5 = New System.Windows.Forms.Button()
        Me.btn_batal_4 = New System.Windows.Forms.Button()
        Me.btn_batal_3 = New System.Windows.Forms.Button()
        Me.btn_batal_2 = New System.Windows.Forms.Button()
        Me.btn_batal_1 = New System.Windows.Forms.Button()
        Me.btn_hapus_8 = New System.Windows.Forms.Button()
        Me.btn_selesai_8 = New System.Windows.Forms.Button()
        Me.btn_hapus_7 = New System.Windows.Forms.Button()
        Me.btn_selesai_7 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_7 = New System.Windows.Forms.Button()
        Me.btn_hapus_6 = New System.Windows.Forms.Button()
        Me.btn_selesai_6 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_6 = New System.Windows.Forms.Button()
        Me.btn_hapus_5 = New System.Windows.Forms.Button()
        Me.btn_selesai_5 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_5 = New System.Windows.Forms.Button()
        Me.btn_hapus_4 = New System.Windows.Forms.Button()
        Me.btn_selesai_4 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_4 = New System.Windows.Forms.Button()
        Me.btn_hapus_3 = New System.Windows.Forms.Button()
        Me.btn_selesai_3 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_3 = New System.Windows.Forms.Button()
        Me.btn_hapus_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_2 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_1 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_1 = New System.Windows.Forms.Button()
        Me.Panel_1 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_1 = New System.Windows.Forms.Panel()
        Me.cb_satuan_1 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_1 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_1 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_1 = New System.Windows.Forms.TextBox()
        Me.txt_qty_1 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_1 = New System.Windows.Forms.TextBox()
        Me.txt_harga_1 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_1 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_1 = New System.Windows.Forms.Panel()
        Me.txt_no_urut_1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel_4 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_4 = New System.Windows.Forms.Panel()
        Me.cb_satuan_4 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_4 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_4 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_4 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_4 = New System.Windows.Forms.TextBox()
        Me.txt_no_kontrak_4 = New System.Windows.Forms.TextBox()
        Me.txt_qty_4 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_4 = New System.Windows.Forms.TextBox()
        Me.txt_harga_4 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_4 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_4 = New System.Windows.Forms.Panel()
        Me.rb_kontrak_4 = New System.Windows.Forms.RadioButton()
        Me.rb_non_kontrak_4 = New System.Windows.Forms.RadioButton()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_id_grey_4 = New System.Windows.Forms.TextBox()
        Me.Panel_3 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_3 = New System.Windows.Forms.Panel()
        Me.cb_satuan_3 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_3 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_3 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_3 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_3 = New System.Windows.Forms.TextBox()
        Me.txt_no_kontrak_3 = New System.Windows.Forms.TextBox()
        Me.txt_qty_3 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_3 = New System.Windows.Forms.TextBox()
        Me.txt_harga_3 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_3 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_3 = New System.Windows.Forms.Panel()
        Me.rb_kontrak_3 = New System.Windows.Forms.RadioButton()
        Me.rb_non_kontrak_3 = New System.Windows.Forms.RadioButton()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_id_grey_3 = New System.Windows.Forms.TextBox()
        Me.Panel_2 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_2 = New System.Windows.Forms.Panel()
        Me.cb_satuan_2 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_2 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_2 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_2 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_2 = New System.Windows.Forms.TextBox()
        Me.txt_no_kontrak_2 = New System.Windows.Forms.TextBox()
        Me.txt_qty_2 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_2 = New System.Windows.Forms.TextBox()
        Me.txt_harga_2 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_2 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_2 = New System.Windows.Forms.Panel()
        Me.rb_kontrak_2 = New System.Windows.Forms.RadioButton()
        Me.rb_non_kontrak_2 = New System.Windows.Forms.RadioButton()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_id_grey_2 = New System.Windows.Forms.TextBox()
        Me.Panel_5 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_5 = New System.Windows.Forms.Panel()
        Me.cb_satuan_5 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_5 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_5 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_5 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_5 = New System.Windows.Forms.TextBox()
        Me.txt_no_kontrak_5 = New System.Windows.Forms.TextBox()
        Me.txt_qty_5 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_5 = New System.Windows.Forms.TextBox()
        Me.txt_harga_5 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_5 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_5 = New System.Windows.Forms.Panel()
        Me.rb_kontrak_5 = New System.Windows.Forms.RadioButton()
        Me.rb_non_kontrak_5 = New System.Windows.Forms.RadioButton()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txt_id_grey_5 = New System.Windows.Forms.TextBox()
        Me.Panel_6 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_6 = New System.Windows.Forms.Panel()
        Me.cb_satuan_6 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_6 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_6 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_6 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_6 = New System.Windows.Forms.TextBox()
        Me.txt_no_kontrak_6 = New System.Windows.Forms.TextBox()
        Me.txt_qty_6 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_6 = New System.Windows.Forms.TextBox()
        Me.txt_harga_6 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_6 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_6 = New System.Windows.Forms.Panel()
        Me.rb_kontrak_6 = New System.Windows.Forms.RadioButton()
        Me.rb_non_kontrak_6 = New System.Windows.Forms.RadioButton()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txt_id_grey_6 = New System.Windows.Forms.TextBox()
        Me.Panel_7 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_7 = New System.Windows.Forms.Panel()
        Me.cb_satuan_7 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_7 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_7 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_7 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_7 = New System.Windows.Forms.TextBox()
        Me.txt_no_kontrak_7 = New System.Windows.Forms.TextBox()
        Me.txt_qty_7 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_7 = New System.Windows.Forms.TextBox()
        Me.txt_harga_7 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_7 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_7 = New System.Windows.Forms.Panel()
        Me.rb_kontrak_7 = New System.Windows.Forms.RadioButton()
        Me.rb_non_kontrak_7 = New System.Windows.Forms.RadioButton()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txt_id_grey_7 = New System.Windows.Forms.TextBox()
        Me.Panel_8 = New System.Windows.Forms.Panel()
        Me.Panel_ubah_8 = New System.Windows.Forms.Panel()
        Me.cb_satuan_8 = New System.Windows.Forms.ComboBox()
        Me.txt_total_harga_8 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_8 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_8 = New System.Windows.Forms.TextBox()
        Me.txt_supplier_8 = New System.Windows.Forms.TextBox()
        Me.txt_no_kontrak_8 = New System.Windows.Forms.TextBox()
        Me.txt_qty_8 = New System.Windows.Forms.TextBox()
        Me.txt_sisa_kontrak_8 = New System.Windows.Forms.TextBox()
        Me.txt_harga_8 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_8 = New System.Windows.Forms.TextBox()
        Me.Panel_rb_8 = New System.Windows.Forms.Panel()
        Me.rb_kontrak_8 = New System.Windows.Forms.RadioButton()
        Me.rb_non_kontrak_8 = New System.Windows.Forms.RadioButton()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txt_id_grey_8 = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_id_beli_1 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_5 = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_id_hutang_1 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_hutang_5 = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_asal_supplier_1 = New System.Windows.Forms.TextBox()
        Me.txt_asal_supplier_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_supplier_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_supplier_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_supplier_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_supplier_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_supplier_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_supplier_5 = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel_1.SuspendLayout()
        Me.Panel_ubah_1.SuspendLayout()
        Me.Panel_rb_1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel_4.SuspendLayout()
        Me.Panel_ubah_4.SuspendLayout()
        Me.Panel_rb_4.SuspendLayout()
        Me.Panel_3.SuspendLayout()
        Me.Panel_ubah_3.SuspendLayout()
        Me.Panel_rb_3.SuspendLayout()
        Me.Panel_2.SuspendLayout()
        Me.Panel_ubah_2.SuspendLayout()
        Me.Panel_rb_2.SuspendLayout()
        Me.Panel_5.SuspendLayout()
        Me.Panel_ubah_5.SuspendLayout()
        Me.Panel_rb_5.SuspendLayout()
        Me.Panel_6.SuspendLayout()
        Me.Panel_ubah_6.SuspendLayout()
        Me.Panel_rb_6.SuspendLayout()
        Me.Panel_7.SuspendLayout()
        Me.Panel_ubah_7.SuspendLayout()
        Me.Panel_rb_7.SuspendLayout()
        Me.Panel_8.SuspendLayout()
        Me.Panel_ubah_8.SuspendLayout()
        Me.Panel_rb_8.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(-1, 7)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1300, 120)
        Me.Panel1.TabIndex = 1
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.ComboBox1)
        Me.Panel2.Controls.Add(Me.dtp_jatuh_tempo)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.txt_surat_jalan)
        Me.Panel2.Controls.Add(Me.dtp_awal)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Location = New System.Drawing.Point(500, 29)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(300, 88)
        Me.Panel2.TabIndex = 77
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"15", "30", "45", "60", "75", "90", "105", "120", "135", "150", "165", "180", "195", "210", "225", "240", "255", "270", "285", "300", "315", "330", "345", "360", "375"})
        Me.ComboBox1.Location = New System.Drawing.Point(121, 59)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(59, 21)
        Me.ComboBox1.TabIndex = 76
        Me.ComboBox1.Text = "15"
        '
        'dtp_jatuh_tempo
        '
        Me.dtp_jatuh_tempo.Enabled = False
        Me.dtp_jatuh_tempo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo.Location = New System.Drawing.Point(186, 59)
        Me.dtp_jatuh_tempo.Name = "dtp_jatuh_tempo"
        Me.dtp_jatuh_tempo.Size = New System.Drawing.Size(99, 20)
        Me.dtp_jatuh_tempo.TabIndex = 74
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(15, 61)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(98, 16)
        Me.Label13.TabIndex = 75
        Me.Label13.Text = "Jatuh Tempo"
        '
        'txt_surat_jalan
        '
        Me.txt_surat_jalan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_surat_jalan.Location = New System.Drawing.Point(121, 7)
        Me.txt_surat_jalan.Name = "txt_surat_jalan"
        Me.txt_surat_jalan.Size = New System.Drawing.Size(164, 20)
        Me.txt_surat_jalan.TabIndex = 0
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(121, 33)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tanggal"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(15, 9)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(54, 16)
        Me.Label16.TabIndex = 25
        Me.Label16.Text = "No. SJ"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.Font = New System.Drawing.Font("Cambria", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Window
        Me.Label1.Location = New System.Drawing.Point(500, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(300, 26)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PEMBELIAN GREY"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(68, 91)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 13)
        Me.Label15.TabIndex = 78
        Me.Label15.Text = "ID Grey"
        '
        'txt_id_grey_1
        '
        Me.txt_id_grey_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_1.Location = New System.Drawing.Point(7, 6)
        Me.txt_id_grey_1.Name = "txt_id_grey_1"
        Me.txt_id_grey_1.ReadOnly = True
        Me.txt_id_grey_1.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_1.TabIndex = 77
        Me.txt_id_grey_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'rb_non_kontrak_1
        '
        Me.rb_non_kontrak_1.AutoSize = True
        Me.rb_non_kontrak_1.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_1.Name = "rb_non_kontrak_1"
        Me.rb_non_kontrak_1.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_1.TabIndex = 101
        Me.rb_non_kontrak_1.Text = "Non Kontrak"
        Me.rb_non_kontrak_1.UseVisualStyleBackColor = True
        '
        'rb_kontrak_1
        '
        Me.rb_kontrak_1.AutoSize = True
        Me.rb_kontrak_1.Checked = True
        Me.rb_kontrak_1.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_1.Name = "rb_kontrak_1"
        Me.rb_kontrak_1.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_1.TabIndex = 102
        Me.rb_kontrak_1.TabStop = True
        Me.rb_kontrak_1.Text = "Kontrak"
        Me.rb_kontrak_1.UseVisualStyleBackColor = True
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(775, 557)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(108, 20)
        Me.dtp_hari_ini.TabIndex = 73
        '
        'txt_no_kontrak_1
        '
        Me.txt_no_kontrak_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_1.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_1.Name = "txt_no_kontrak_1"
        Me.txt_no_kontrak_1.ReadOnly = True
        Me.txt_no_kontrak_1.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_1.TabIndex = 104
        Me.txt_no_kontrak_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_1
        '
        Me.txt_supplier_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_1.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_1.Name = "txt_supplier_1"
        Me.txt_supplier_1.ReadOnly = True
        Me.txt_supplier_1.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_1.TabIndex = 106
        Me.txt_supplier_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btn_batal_8)
        Me.Panel4.Controls.Add(Me.btn_batal_7)
        Me.Panel4.Controls.Add(Me.btn_batal_6)
        Me.Panel4.Controls.Add(Me.btn_batal_5)
        Me.Panel4.Controls.Add(Me.btn_batal_4)
        Me.Panel4.Controls.Add(Me.btn_batal_3)
        Me.Panel4.Controls.Add(Me.btn_batal_2)
        Me.Panel4.Controls.Add(Me.btn_batal_1)
        Me.Panel4.Controls.Add(Me.btn_hapus_8)
        Me.Panel4.Controls.Add(Me.btn_selesai_8)
        Me.Panel4.Controls.Add(Me.btn_hapus_7)
        Me.Panel4.Controls.Add(Me.btn_selesai_7)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_7)
        Me.Panel4.Controls.Add(Me.btn_hapus_6)
        Me.Panel4.Controls.Add(Me.btn_selesai_6)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_6)
        Me.Panel4.Controls.Add(Me.btn_hapus_5)
        Me.Panel4.Controls.Add(Me.btn_selesai_5)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_5)
        Me.Panel4.Controls.Add(Me.btn_hapus_4)
        Me.Panel4.Controls.Add(Me.btn_selesai_4)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_4)
        Me.Panel4.Controls.Add(Me.btn_hapus_3)
        Me.Panel4.Controls.Add(Me.btn_selesai_3)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_3)
        Me.Panel4.Controls.Add(Me.btn_hapus_2)
        Me.Panel4.Controls.Add(Me.btn_selesai_2)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_2)
        Me.Panel4.Controls.Add(Me.btn_selesai_1)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_1)
        Me.Panel4.Location = New System.Drawing.Point(403, 211)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(493, 410)
        Me.Panel4.TabIndex = 61
        '
        'btn_batal_8
        '
        Me.btn_batal_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_8.Image = CType(resources.GetObject("btn_batal_8.Image"), System.Drawing.Image)
        Me.btn_batal_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_8.Location = New System.Drawing.Point(335, 364)
        Me.btn_batal_8.Name = "btn_batal_8"
        Me.btn_batal_8.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_8.TabIndex = 39
        Me.btn_batal_8.Text = "Batal"
        Me.btn_batal_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_8.UseVisualStyleBackColor = False
        '
        'btn_batal_7
        '
        Me.btn_batal_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_7.Image = CType(resources.GetObject("btn_batal_7.Image"), System.Drawing.Image)
        Me.btn_batal_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_7.Location = New System.Drawing.Point(401, 314)
        Me.btn_batal_7.Name = "btn_batal_7"
        Me.btn_batal_7.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_7.TabIndex = 38
        Me.btn_batal_7.Text = "Batal"
        Me.btn_batal_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_7.UseVisualStyleBackColor = False
        '
        'btn_batal_6
        '
        Me.btn_batal_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_6.Image = CType(resources.GetObject("btn_batal_6.Image"), System.Drawing.Image)
        Me.btn_batal_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_6.Location = New System.Drawing.Point(401, 266)
        Me.btn_batal_6.Name = "btn_batal_6"
        Me.btn_batal_6.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_6.TabIndex = 37
        Me.btn_batal_6.Text = "Batal"
        Me.btn_batal_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_6.UseVisualStyleBackColor = False
        '
        'btn_batal_5
        '
        Me.btn_batal_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_5.Image = CType(resources.GetObject("btn_batal_5.Image"), System.Drawing.Image)
        Me.btn_batal_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_5.Location = New System.Drawing.Point(401, 217)
        Me.btn_batal_5.Name = "btn_batal_5"
        Me.btn_batal_5.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_5.TabIndex = 36
        Me.btn_batal_5.Text = "Batal"
        Me.btn_batal_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_5.UseVisualStyleBackColor = False
        '
        'btn_batal_4
        '
        Me.btn_batal_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_4.Image = CType(resources.GetObject("btn_batal_4.Image"), System.Drawing.Image)
        Me.btn_batal_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_4.Location = New System.Drawing.Point(401, 169)
        Me.btn_batal_4.Name = "btn_batal_4"
        Me.btn_batal_4.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_4.TabIndex = 35
        Me.btn_batal_4.Text = "Batal"
        Me.btn_batal_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_4.UseVisualStyleBackColor = False
        '
        'btn_batal_3
        '
        Me.btn_batal_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_3.Image = CType(resources.GetObject("btn_batal_3.Image"), System.Drawing.Image)
        Me.btn_batal_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_3.Location = New System.Drawing.Point(401, 119)
        Me.btn_batal_3.Name = "btn_batal_3"
        Me.btn_batal_3.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_3.TabIndex = 34
        Me.btn_batal_3.Text = "Batal"
        Me.btn_batal_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_3.UseVisualStyleBackColor = False
        '
        'btn_batal_2
        '
        Me.btn_batal_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_2.Image = CType(resources.GetObject("btn_batal_2.Image"), System.Drawing.Image)
        Me.btn_batal_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_2.Location = New System.Drawing.Point(401, 70)
        Me.btn_batal_2.Name = "btn_batal_2"
        Me.btn_batal_2.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_2.TabIndex = 33
        Me.btn_batal_2.Text = "Batal"
        Me.btn_batal_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_2.UseVisualStyleBackColor = False
        '
        'btn_batal_1
        '
        Me.btn_batal_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_1.Image = CType(resources.GetObject("btn_batal_1.Image"), System.Drawing.Image)
        Me.btn_batal_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_1.Location = New System.Drawing.Point(338, 21)
        Me.btn_batal_1.Name = "btn_batal_1"
        Me.btn_batal_1.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_1.TabIndex = 32
        Me.btn_batal_1.Text = "Batal"
        Me.btn_batal_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_1.UseVisualStyleBackColor = False
        '
        'btn_hapus_8
        '
        Me.btn_hapus_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_8.Image = CType(resources.GetObject("btn_hapus_8.Image"), System.Drawing.Image)
        Me.btn_hapus_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_8.Location = New System.Drawing.Point(200, 364)
        Me.btn_hapus_8.Name = "btn_hapus_8"
        Me.btn_hapus_8.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_8.TabIndex = 31
        Me.btn_hapus_8.Text = "Hapus Baris"
        Me.btn_hapus_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_8.UseVisualStyleBackColor = False
        '
        'btn_selesai_8
        '
        Me.btn_selesai_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_8.Image = CType(resources.GetObject("btn_selesai_8.Image"), System.Drawing.Image)
        Me.btn_selesai_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_8.Location = New System.Drawing.Point(93, 364)
        Me.btn_selesai_8.Name = "btn_selesai_8"
        Me.btn_selesai_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_8.TabIndex = 30
        Me.btn_selesai_8.Text = "Selesai"
        Me.btn_selesai_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_8.UseVisualStyleBackColor = False
        '
        'btn_hapus_7
        '
        Me.btn_hapus_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_7.Image = CType(resources.GetObject("btn_hapus_7.Image"), System.Drawing.Image)
        Me.btn_hapus_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_7.Location = New System.Drawing.Point(266, 314)
        Me.btn_hapus_7.Name = "btn_hapus_7"
        Me.btn_hapus_7.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_7.TabIndex = 28
        Me.btn_hapus_7.Text = "Hapus Baris"
        Me.btn_hapus_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_7.UseVisualStyleBackColor = False
        '
        'btn_selesai_7
        '
        Me.btn_selesai_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_7.Image = CType(resources.GetObject("btn_selesai_7.Image"), System.Drawing.Image)
        Me.btn_selesai_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_7.Location = New System.Drawing.Point(159, 315)
        Me.btn_selesai_7.Name = "btn_selesai_7"
        Me.btn_selesai_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_7.TabIndex = 27
        Me.btn_selesai_7.Text = "Selesai"
        Me.btn_selesai_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_7.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_7
        '
        Me.btn_tambah_baris_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_7.Image = CType(resources.GetObject("btn_tambah_baris_7.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_7.Location = New System.Drawing.Point(17, 315)
        Me.btn_tambah_baris_7.Name = "btn_tambah_baris_7"
        Me.btn_tambah_baris_7.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_7.TabIndex = 26
        Me.btn_tambah_baris_7.Text = "Tambah Baris"
        Me.btn_tambah_baris_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_7.UseVisualStyleBackColor = False
        '
        'btn_hapus_6
        '
        Me.btn_hapus_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_6.Image = CType(resources.GetObject("btn_hapus_6.Image"), System.Drawing.Image)
        Me.btn_hapus_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_6.Location = New System.Drawing.Point(266, 266)
        Me.btn_hapus_6.Name = "btn_hapus_6"
        Me.btn_hapus_6.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_6.TabIndex = 25
        Me.btn_hapus_6.Text = "Hapus Baris"
        Me.btn_hapus_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_6.UseVisualStyleBackColor = False
        '
        'btn_selesai_6
        '
        Me.btn_selesai_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_6.Image = CType(resources.GetObject("btn_selesai_6.Image"), System.Drawing.Image)
        Me.btn_selesai_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_6.Location = New System.Drawing.Point(159, 266)
        Me.btn_selesai_6.Name = "btn_selesai_6"
        Me.btn_selesai_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_6.TabIndex = 24
        Me.btn_selesai_6.Text = "Selesai"
        Me.btn_selesai_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_6.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_6
        '
        Me.btn_tambah_baris_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_6.Image = CType(resources.GetObject("btn_tambah_baris_6.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_6.Location = New System.Drawing.Point(17, 266)
        Me.btn_tambah_baris_6.Name = "btn_tambah_baris_6"
        Me.btn_tambah_baris_6.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_6.TabIndex = 23
        Me.btn_tambah_baris_6.Text = "Tambah Baris"
        Me.btn_tambah_baris_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_6.UseVisualStyleBackColor = False
        '
        'btn_hapus_5
        '
        Me.btn_hapus_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_5.Image = CType(resources.GetObject("btn_hapus_5.Image"), System.Drawing.Image)
        Me.btn_hapus_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_5.Location = New System.Drawing.Point(266, 217)
        Me.btn_hapus_5.Name = "btn_hapus_5"
        Me.btn_hapus_5.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_5.TabIndex = 22
        Me.btn_hapus_5.Text = "Hapus Baris"
        Me.btn_hapus_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_5.UseVisualStyleBackColor = False
        '
        'btn_selesai_5
        '
        Me.btn_selesai_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_5.Image = CType(resources.GetObject("btn_selesai_5.Image"), System.Drawing.Image)
        Me.btn_selesai_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_5.Location = New System.Drawing.Point(159, 217)
        Me.btn_selesai_5.Name = "btn_selesai_5"
        Me.btn_selesai_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_5.TabIndex = 21
        Me.btn_selesai_5.Text = "Selesai"
        Me.btn_selesai_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_5.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_5
        '
        Me.btn_tambah_baris_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_5.Image = CType(resources.GetObject("btn_tambah_baris_5.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_5.Location = New System.Drawing.Point(17, 217)
        Me.btn_tambah_baris_5.Name = "btn_tambah_baris_5"
        Me.btn_tambah_baris_5.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_5.TabIndex = 20
        Me.btn_tambah_baris_5.Text = "Tambah Baris"
        Me.btn_tambah_baris_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_5.UseVisualStyleBackColor = False
        '
        'btn_hapus_4
        '
        Me.btn_hapus_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_4.Image = CType(resources.GetObject("btn_hapus_4.Image"), System.Drawing.Image)
        Me.btn_hapus_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_4.Location = New System.Drawing.Point(266, 169)
        Me.btn_hapus_4.Name = "btn_hapus_4"
        Me.btn_hapus_4.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_4.TabIndex = 19
        Me.btn_hapus_4.Text = "Hapus Baris"
        Me.btn_hapus_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_4.UseVisualStyleBackColor = False
        '
        'btn_selesai_4
        '
        Me.btn_selesai_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_4.Image = CType(resources.GetObject("btn_selesai_4.Image"), System.Drawing.Image)
        Me.btn_selesai_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_4.Location = New System.Drawing.Point(159, 169)
        Me.btn_selesai_4.Name = "btn_selesai_4"
        Me.btn_selesai_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_4.TabIndex = 18
        Me.btn_selesai_4.Text = "Selesai"
        Me.btn_selesai_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_4.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_4
        '
        Me.btn_tambah_baris_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_4.Image = CType(resources.GetObject("btn_tambah_baris_4.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_4.Location = New System.Drawing.Point(17, 169)
        Me.btn_tambah_baris_4.Name = "btn_tambah_baris_4"
        Me.btn_tambah_baris_4.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_4.TabIndex = 17
        Me.btn_tambah_baris_4.Text = "Tambah Baris"
        Me.btn_tambah_baris_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_4.UseVisualStyleBackColor = False
        '
        'btn_hapus_3
        '
        Me.btn_hapus_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_3.Image = CType(resources.GetObject("btn_hapus_3.Image"), System.Drawing.Image)
        Me.btn_hapus_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_3.Location = New System.Drawing.Point(266, 119)
        Me.btn_hapus_3.Name = "btn_hapus_3"
        Me.btn_hapus_3.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_3.TabIndex = 16
        Me.btn_hapus_3.Text = "Hapus Baris"
        Me.btn_hapus_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_3.UseVisualStyleBackColor = False
        '
        'btn_selesai_3
        '
        Me.btn_selesai_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_3.Image = CType(resources.GetObject("btn_selesai_3.Image"), System.Drawing.Image)
        Me.btn_selesai_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_3.Location = New System.Drawing.Point(159, 119)
        Me.btn_selesai_3.Name = "btn_selesai_3"
        Me.btn_selesai_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_3.TabIndex = 15
        Me.btn_selesai_3.Text = "Selesai"
        Me.btn_selesai_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_3.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_3
        '
        Me.btn_tambah_baris_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_3.Image = CType(resources.GetObject("btn_tambah_baris_3.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_3.Location = New System.Drawing.Point(17, 119)
        Me.btn_tambah_baris_3.Name = "btn_tambah_baris_3"
        Me.btn_tambah_baris_3.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_3.TabIndex = 14
        Me.btn_tambah_baris_3.Text = "Tambah Baris"
        Me.btn_tambah_baris_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_3.UseVisualStyleBackColor = False
        '
        'btn_hapus_2
        '
        Me.btn_hapus_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_2.Image = CType(resources.GetObject("btn_hapus_2.Image"), System.Drawing.Image)
        Me.btn_hapus_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_2.Location = New System.Drawing.Point(266, 70)
        Me.btn_hapus_2.Name = "btn_hapus_2"
        Me.btn_hapus_2.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_2.TabIndex = 13
        Me.btn_hapus_2.Text = "Hapus Baris"
        Me.btn_hapus_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_2
        '
        Me.btn_selesai_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_2.Image = CType(resources.GetObject("btn_selesai_2.Image"), System.Drawing.Image)
        Me.btn_selesai_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_2.Location = New System.Drawing.Point(159, 70)
        Me.btn_selesai_2.Name = "btn_selesai_2"
        Me.btn_selesai_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_2.TabIndex = 12
        Me.btn_selesai_2.Text = "Selesai"
        Me.btn_selesai_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_2.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_2
        '
        Me.btn_tambah_baris_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_2.Image = CType(resources.GetObject("btn_tambah_baris_2.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_2.Location = New System.Drawing.Point(17, 70)
        Me.btn_tambah_baris_2.Name = "btn_tambah_baris_2"
        Me.btn_tambah_baris_2.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_2.TabIndex = 11
        Me.btn_tambah_baris_2.Text = "Tambah Baris"
        Me.btn_tambah_baris_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_1
        '
        Me.btn_selesai_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_1.Image = CType(resources.GetObject("btn_selesai_1.Image"), System.Drawing.Image)
        Me.btn_selesai_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_1.Location = New System.Drawing.Point(231, 21)
        Me.btn_selesai_1.Name = "btn_selesai_1"
        Me.btn_selesai_1.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_1.TabIndex = 4
        Me.btn_selesai_1.Text = "Selesai"
        Me.btn_selesai_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_1.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_1
        '
        Me.btn_tambah_baris_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_1.Image = CType(resources.GetObject("btn_tambah_baris_1.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_1.Location = New System.Drawing.Point(89, 21)
        Me.btn_tambah_baris_1.Name = "btn_tambah_baris_1"
        Me.btn_tambah_baris_1.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_1.TabIndex = 3
        Me.btn_tambah_baris_1.Text = "Tambah Baris"
        Me.btn_tambah_baris_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_1.UseVisualStyleBackColor = False
        '
        'Panel_1
        '
        Me.Panel_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_1.Controls.Add(Me.Panel_ubah_1)
        Me.Panel_1.Controls.Add(Me.Panel_rb_1)
        Me.Panel_1.Controls.Add(Me.txt_no_urut_1)
        Me.Panel_1.Location = New System.Drawing.Point(-1, 160)
        Me.Panel_1.Name = "Panel_1"
        Me.Panel_1.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_1.TabIndex = 64
        '
        'Panel_ubah_1
        '
        Me.Panel_ubah_1.Controls.Add(Me.cb_satuan_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_total_harga_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_keterangan_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_gudang_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_supplier_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_no_kontrak_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_qty_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_sisa_kontrak_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_harga_1)
        Me.Panel_ubah_1.Controls.Add(Me.txt_jenis_kain_1)
        Me.Panel_ubah_1.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_1.Name = "Panel_ubah_1"
        Me.Panel_ubah_1.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_1.TabIndex = 108
        '
        'cb_satuan_1
        '
        Me.cb_satuan_1.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_1.FormattingEnabled = True
        Me.cb_satuan_1.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_1.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_1.Name = "cb_satuan_1"
        Me.cb_satuan_1.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_1.TabIndex = 500
        Me.cb_satuan_1.Text = "Meter"
        '
        'txt_total_harga_1
        '
        Me.txt_total_harga_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_1.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_1.Name = "txt_total_harga_1"
        Me.txt_total_harga_1.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_1.TabIndex = 4
        Me.txt_total_harga_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_1
        '
        Me.txt_keterangan_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_1.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_1.Name = "txt_keterangan_1"
        Me.txt_keterangan_1.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_1.TabIndex = 5
        '
        'txt_gudang_1
        '
        Me.txt_gudang_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_1.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_1.Name = "txt_gudang_1"
        Me.txt_gudang_1.ReadOnly = True
        Me.txt_gudang_1.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_1.TabIndex = 107
        Me.txt_gudang_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_1
        '
        Me.txt_qty_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_1.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_1.Name = "txt_qty_1"
        Me.txt_qty_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_1.TabIndex = 3
        Me.txt_qty_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_1
        '
        Me.txt_sisa_kontrak_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_1.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_1.Name = "txt_sisa_kontrak_1"
        Me.txt_sisa_kontrak_1.ReadOnly = True
        Me.txt_sisa_kontrak_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_1.TabIndex = 2
        Me.txt_sisa_kontrak_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_1
        '
        Me.txt_harga_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_1.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_1.Name = "txt_harga_1"
        Me.txt_harga_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_1.TabIndex = 1
        Me.txt_harga_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_1
        '
        Me.txt_jenis_kain_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_1.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_1.Name = "txt_jenis_kain_1"
        Me.txt_jenis_kain_1.ReadOnly = True
        Me.txt_jenis_kain_1.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_1.TabIndex = 105
        Me.txt_jenis_kain_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_1
        '
        Me.Panel_rb_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_1.Controls.Add(Me.rb_kontrak_1)
        Me.Panel_rb_1.Controls.Add(Me.rb_non_kontrak_1)
        Me.Panel_rb_1.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_1.Name = "Panel_rb_1"
        Me.Panel_rb_1.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_1.TabIndex = 76
        '
        'txt_no_urut_1
        '
        Me.txt_no_urut_1.AutoSize = True
        Me.txt_no_urut_1.Location = New System.Drawing.Point(8, 18)
        Me.txt_no_urut_1.Name = "txt_no_urut_1"
        Me.txt_no_urut_1.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_1.TabIndex = 103
        Me.txt_no_urut_1.Text = "1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Window
        Me.Label3.Location = New System.Drawing.Point(1167, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 13)
        Me.Label3.TabIndex = 72
        Me.Label3.Text = "Keterangan"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Window
        Me.Label4.Location = New System.Drawing.Point(1037, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 71
        Me.Label4.Text = "Gudang"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Window
        Me.Label5.Location = New System.Drawing.Point(880, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(106, 13)
        Me.Label5.TabIndex = 70
        Me.Label5.Text = "Total Harga (Rp.)"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.Window
        Me.Label19.Location = New System.Drawing.Point(744, 7)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(26, 13)
        Me.Label19.TabIndex = 69
        Me.Label19.Text = "Qty"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.SystemColors.Window
        Me.Label21.Location = New System.Drawing.Point(612, 7)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(79, 13)
        Me.Label21.TabIndex = 68
        Me.Label21.Text = "Sisa Kontrak"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.Window
        Me.Label22.Location = New System.Drawing.Point(509, 7)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(73, 13)
        Me.Label22.TabIndex = 67
        Me.Label22.Text = "Harga (Rp.)"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.Window
        Me.Label23.Location = New System.Drawing.Point(407, 7)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(53, 13)
        Me.Label23.TabIndex = 66
        Me.Label23.Text = "Supplier"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.Window
        Me.Label24.Location = New System.Drawing.Point(275, 7)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(65, 13)
        Me.Label24.TabIndex = 65
        Me.Label24.Text = "Jenis Kain"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.Window
        Me.Label25.Location = New System.Drawing.Point(165, 7)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(51, 13)
        Me.Label25.TabIndex = 63
        Me.Label25.Text = "Kontrak"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.Label25)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label24)
        Me.Panel3.Controls.Add(Me.Label19)
        Me.Panel3.Controls.Add(Me.Label23)
        Me.Panel3.Controls.Add(Me.Label21)
        Me.Panel3.Controls.Add(Me.Label22)
        Me.Panel3.Location = New System.Drawing.Point(-1, 132)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1300, 29)
        Me.Panel3.TabIndex = 73
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Window
        Me.Label7.Location = New System.Drawing.Point(815, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 73
        Me.Label7.Text = "Satuan"
        '
        'Panel_4
        '
        Me.Panel_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_4.Controls.Add(Me.Panel_ubah_4)
        Me.Panel_4.Controls.Add(Me.Panel_rb_4)
        Me.Panel_4.Controls.Add(Me.Label10)
        Me.Panel_4.Location = New System.Drawing.Point(-1, 307)
        Me.Panel_4.Name = "Panel_4"
        Me.Panel_4.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_4.TabIndex = 0
        '
        'Panel_ubah_4
        '
        Me.Panel_ubah_4.Controls.Add(Me.cb_satuan_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_total_harga_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_keterangan_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_gudang_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_supplier_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_no_kontrak_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_qty_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_sisa_kontrak_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_harga_4)
        Me.Panel_ubah_4.Controls.Add(Me.txt_jenis_kain_4)
        Me.Panel_ubah_4.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_4.Name = "Panel_ubah_4"
        Me.Panel_ubah_4.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_4.TabIndex = 105
        '
        'cb_satuan_4
        '
        Me.cb_satuan_4.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_4.FormattingEnabled = True
        Me.cb_satuan_4.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_4.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_4.Name = "cb_satuan_4"
        Me.cb_satuan_4.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_4.TabIndex = 500
        Me.cb_satuan_4.Text = "Meter"
        '
        'txt_total_harga_4
        '
        Me.txt_total_harga_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_4.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_4.Name = "txt_total_harga_4"
        Me.txt_total_harga_4.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_4.TabIndex = 9
        Me.txt_total_harga_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_4
        '
        Me.txt_keterangan_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_4.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_4.Name = "txt_keterangan_4"
        Me.txt_keterangan_4.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_4.TabIndex = 8
        '
        'txt_gudang_4
        '
        Me.txt_gudang_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_4.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_4.Name = "txt_gudang_4"
        Me.txt_gudang_4.ReadOnly = True
        Me.txt_gudang_4.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_4.TabIndex = 7
        Me.txt_gudang_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_4
        '
        Me.txt_supplier_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_4.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_4.Name = "txt_supplier_4"
        Me.txt_supplier_4.ReadOnly = True
        Me.txt_supplier_4.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_4.TabIndex = 14
        Me.txt_supplier_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_kontrak_4
        '
        Me.txt_no_kontrak_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_4.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_4.Name = "txt_no_kontrak_4"
        Me.txt_no_kontrak_4.ReadOnly = True
        Me.txt_no_kontrak_4.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_4.TabIndex = 100
        Me.txt_no_kontrak_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_4
        '
        Me.txt_qty_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_4.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_4.Name = "txt_qty_4"
        Me.txt_qty_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_4.TabIndex = 6
        Me.txt_qty_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_4
        '
        Me.txt_sisa_kontrak_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_4.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_4.Name = "txt_sisa_kontrak_4"
        Me.txt_sisa_kontrak_4.ReadOnly = True
        Me.txt_sisa_kontrak_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_4.TabIndex = 5
        Me.txt_sisa_kontrak_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_4
        '
        Me.txt_harga_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_4.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_4.Name = "txt_harga_4"
        Me.txt_harga_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_4.TabIndex = 1
        Me.txt_harga_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_4
        '
        Me.txt_jenis_kain_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_4.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_4.Name = "txt_jenis_kain_4"
        Me.txt_jenis_kain_4.ReadOnly = True
        Me.txt_jenis_kain_4.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_4.TabIndex = 2
        Me.txt_jenis_kain_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_4
        '
        Me.Panel_rb_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_4.Controls.Add(Me.rb_kontrak_4)
        Me.Panel_rb_4.Controls.Add(Me.rb_non_kontrak_4)
        Me.Panel_rb_4.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_4.Name = "Panel_rb_4"
        Me.Panel_rb_4.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_4.TabIndex = 76
        '
        'rb_kontrak_4
        '
        Me.rb_kontrak_4.AutoSize = True
        Me.rb_kontrak_4.Checked = True
        Me.rb_kontrak_4.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_4.Name = "rb_kontrak_4"
        Me.rb_kontrak_4.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_4.TabIndex = 33
        Me.rb_kontrak_4.TabStop = True
        Me.rb_kontrak_4.Text = "Kontrak"
        Me.rb_kontrak_4.UseVisualStyleBackColor = True
        '
        'rb_non_kontrak_4
        '
        Me.rb_non_kontrak_4.AutoSize = True
        Me.rb_non_kontrak_4.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_4.Name = "rb_non_kontrak_4"
        Me.rb_non_kontrak_4.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_4.TabIndex = 34
        Me.rb_non_kontrak_4.Text = "Non Kontrak"
        Me.rb_non_kontrak_4.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 18)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(13, 13)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "4"
        '
        'txt_id_grey_4
        '
        Me.txt_id_grey_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_4.Location = New System.Drawing.Point(7, 66)
        Me.txt_id_grey_4.Name = "txt_id_grey_4"
        Me.txt_id_grey_4.ReadOnly = True
        Me.txt_id_grey_4.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_4.TabIndex = 77
        Me.txt_id_grey_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_3
        '
        Me.Panel_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_3.Controls.Add(Me.Panel_ubah_3)
        Me.Panel_3.Controls.Add(Me.Panel_rb_3)
        Me.Panel_3.Controls.Add(Me.Label14)
        Me.Panel_3.Location = New System.Drawing.Point(-1, 258)
        Me.Panel_3.Name = "Panel_3"
        Me.Panel_3.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_3.TabIndex = 0
        '
        'Panel_ubah_3
        '
        Me.Panel_ubah_3.Controls.Add(Me.cb_satuan_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_total_harga_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_keterangan_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_gudang_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_supplier_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_no_kontrak_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_qty_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_sisa_kontrak_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_harga_3)
        Me.Panel_ubah_3.Controls.Add(Me.txt_jenis_kain_3)
        Me.Panel_ubah_3.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_3.Name = "Panel_ubah_3"
        Me.Panel_ubah_3.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_3.TabIndex = 105
        '
        'cb_satuan_3
        '
        Me.cb_satuan_3.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_3.FormattingEnabled = True
        Me.cb_satuan_3.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_3.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_3.Name = "cb_satuan_3"
        Me.cb_satuan_3.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_3.TabIndex = 500
        Me.cb_satuan_3.Text = "Meter"
        '
        'txt_total_harga_3
        '
        Me.txt_total_harga_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_3.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_3.Name = "txt_total_harga_3"
        Me.txt_total_harga_3.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_3.TabIndex = 9
        Me.txt_total_harga_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_3
        '
        Me.txt_keterangan_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_3.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_3.Name = "txt_keterangan_3"
        Me.txt_keterangan_3.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_3.TabIndex = 8
        '
        'txt_gudang_3
        '
        Me.txt_gudang_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_3.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_3.Name = "txt_gudang_3"
        Me.txt_gudang_3.ReadOnly = True
        Me.txt_gudang_3.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_3.TabIndex = 7
        Me.txt_gudang_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_3
        '
        Me.txt_supplier_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_3.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_3.Name = "txt_supplier_3"
        Me.txt_supplier_3.ReadOnly = True
        Me.txt_supplier_3.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_3.TabIndex = 14
        Me.txt_supplier_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_kontrak_3
        '
        Me.txt_no_kontrak_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_3.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_3.Name = "txt_no_kontrak_3"
        Me.txt_no_kontrak_3.ReadOnly = True
        Me.txt_no_kontrak_3.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_3.TabIndex = 100
        Me.txt_no_kontrak_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_3
        '
        Me.txt_qty_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_3.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_3.Name = "txt_qty_3"
        Me.txt_qty_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_3.TabIndex = 6
        Me.txt_qty_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_3
        '
        Me.txt_sisa_kontrak_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_3.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_3.Name = "txt_sisa_kontrak_3"
        Me.txt_sisa_kontrak_3.ReadOnly = True
        Me.txt_sisa_kontrak_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_3.TabIndex = 5
        Me.txt_sisa_kontrak_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_3
        '
        Me.txt_harga_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_3.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_3.Name = "txt_harga_3"
        Me.txt_harga_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_3.TabIndex = 1
        Me.txt_harga_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_3
        '
        Me.txt_jenis_kain_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_3.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_3.Name = "txt_jenis_kain_3"
        Me.txt_jenis_kain_3.ReadOnly = True
        Me.txt_jenis_kain_3.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_3.TabIndex = 2
        Me.txt_jenis_kain_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_3
        '
        Me.Panel_rb_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_3.Controls.Add(Me.rb_kontrak_3)
        Me.Panel_rb_3.Controls.Add(Me.rb_non_kontrak_3)
        Me.Panel_rb_3.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_3.Name = "Panel_rb_3"
        Me.Panel_rb_3.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_3.TabIndex = 76
        '
        'rb_kontrak_3
        '
        Me.rb_kontrak_3.AutoSize = True
        Me.rb_kontrak_3.Checked = True
        Me.rb_kontrak_3.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_3.Name = "rb_kontrak_3"
        Me.rb_kontrak_3.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_3.TabIndex = 0
        Me.rb_kontrak_3.TabStop = True
        Me.rb_kontrak_3.Text = "Kontrak"
        Me.rb_kontrak_3.UseVisualStyleBackColor = True
        '
        'rb_non_kontrak_3
        '
        Me.rb_non_kontrak_3.AutoSize = True
        Me.rb_non_kontrak_3.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_3.Name = "rb_non_kontrak_3"
        Me.rb_non_kontrak_3.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_3.TabIndex = 34
        Me.rb_non_kontrak_3.Text = "Non Kontrak"
        Me.rb_non_kontrak_3.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 18)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(13, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "3"
        '
        'txt_id_grey_3
        '
        Me.txt_id_grey_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_3.Location = New System.Drawing.Point(7, 46)
        Me.txt_id_grey_3.Name = "txt_id_grey_3"
        Me.txt_id_grey_3.ReadOnly = True
        Me.txt_id_grey_3.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_3.TabIndex = 77
        Me.txt_id_grey_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_2
        '
        Me.Panel_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_2.Controls.Add(Me.Panel_ubah_2)
        Me.Panel_2.Controls.Add(Me.Panel_rb_2)
        Me.Panel_2.Controls.Add(Me.Label20)
        Me.Panel_2.Location = New System.Drawing.Point(-1, 209)
        Me.Panel_2.Name = "Panel_2"
        Me.Panel_2.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_2.TabIndex = 0
        '
        'Panel_ubah_2
        '
        Me.Panel_ubah_2.Controls.Add(Me.cb_satuan_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_total_harga_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_keterangan_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_gudang_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_supplier_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_no_kontrak_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_qty_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_sisa_kontrak_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_harga_2)
        Me.Panel_ubah_2.Controls.Add(Me.txt_jenis_kain_2)
        Me.Panel_ubah_2.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_2.Name = "Panel_ubah_2"
        Me.Panel_ubah_2.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_2.TabIndex = 109
        '
        'cb_satuan_2
        '
        Me.cb_satuan_2.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_2.FormattingEnabled = True
        Me.cb_satuan_2.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_2.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_2.Name = "cb_satuan_2"
        Me.cb_satuan_2.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_2.TabIndex = 500
        Me.cb_satuan_2.Text = "Meter"
        '
        'txt_total_harga_2
        '
        Me.txt_total_harga_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_2.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_2.Name = "txt_total_harga_2"
        Me.txt_total_harga_2.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_2.TabIndex = 3
        Me.txt_total_harga_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_2
        '
        Me.txt_keterangan_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_2.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_2.Name = "txt_keterangan_2"
        Me.txt_keterangan_2.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_2.TabIndex = 8
        '
        'txt_gudang_2
        '
        Me.txt_gudang_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_2.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_2.Name = "txt_gudang_2"
        Me.txt_gudang_2.ReadOnly = True
        Me.txt_gudang_2.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_2.TabIndex = 107
        Me.txt_gudang_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_2
        '
        Me.txt_supplier_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_2.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_2.Name = "txt_supplier_2"
        Me.txt_supplier_2.ReadOnly = True
        Me.txt_supplier_2.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_2.TabIndex = 102
        Me.txt_supplier_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_kontrak_2
        '
        Me.txt_no_kontrak_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_2.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_2.Name = "txt_no_kontrak_2"
        Me.txt_no_kontrak_2.ReadOnly = True
        Me.txt_no_kontrak_2.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_2.TabIndex = 100
        Me.txt_no_kontrak_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_2
        '
        Me.txt_qty_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_2.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_2.Name = "txt_qty_2"
        Me.txt_qty_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_2.TabIndex = 2
        Me.txt_qty_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_2
        '
        Me.txt_sisa_kontrak_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_2.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_2.Name = "txt_sisa_kontrak_2"
        Me.txt_sisa_kontrak_2.ReadOnly = True
        Me.txt_sisa_kontrak_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_2.TabIndex = 1
        Me.txt_sisa_kontrak_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_2
        '
        Me.txt_harga_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_2.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_2.Name = "txt_harga_2"
        Me.txt_harga_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_2.TabIndex = 0
        Me.txt_harga_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_2
        '
        Me.txt_jenis_kain_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_2.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_2.Name = "txt_jenis_kain_2"
        Me.txt_jenis_kain_2.ReadOnly = True
        Me.txt_jenis_kain_2.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_2.TabIndex = 101
        Me.txt_jenis_kain_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_2
        '
        Me.Panel_rb_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_2.Controls.Add(Me.rb_kontrak_2)
        Me.Panel_rb_2.Controls.Add(Me.rb_non_kontrak_2)
        Me.Panel_rb_2.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_2.Name = "Panel_rb_2"
        Me.Panel_rb_2.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_2.TabIndex = 76
        '
        'rb_kontrak_2
        '
        Me.rb_kontrak_2.AutoSize = True
        Me.rb_kontrak_2.Checked = True
        Me.rb_kontrak_2.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_2.Name = "rb_kontrak_2"
        Me.rb_kontrak_2.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_2.TabIndex = 109
        Me.rb_kontrak_2.TabStop = True
        Me.rb_kontrak_2.Text = "Kontrak"
        Me.rb_kontrak_2.UseVisualStyleBackColor = True
        '
        'rb_non_kontrak_2
        '
        Me.rb_non_kontrak_2.AutoSize = True
        Me.rb_non_kontrak_2.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_2.Name = "rb_non_kontrak_2"
        Me.rb_non_kontrak_2.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_2.TabIndex = 110
        Me.rb_non_kontrak_2.Text = "Non Kontrak"
        Me.rb_non_kontrak_2.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(8, 18)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(13, 13)
        Me.Label20.TabIndex = 108
        Me.Label20.Text = "2"
        '
        'txt_id_grey_2
        '
        Me.txt_id_grey_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_2.Location = New System.Drawing.Point(7, 26)
        Me.txt_id_grey_2.Name = "txt_id_grey_2"
        Me.txt_id_grey_2.ReadOnly = True
        Me.txt_id_grey_2.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_2.TabIndex = 77
        Me.txt_id_grey_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_5
        '
        Me.Panel_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_5.Controls.Add(Me.Panel_ubah_5)
        Me.Panel_5.Controls.Add(Me.Panel_rb_5)
        Me.Panel_5.Controls.Add(Me.Label29)
        Me.Panel_5.Location = New System.Drawing.Point(-1, 356)
        Me.Panel_5.Name = "Panel_5"
        Me.Panel_5.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_5.TabIndex = 107
        '
        'Panel_ubah_5
        '
        Me.Panel_ubah_5.Controls.Add(Me.cb_satuan_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_total_harga_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_keterangan_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_gudang_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_supplier_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_no_kontrak_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_qty_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_sisa_kontrak_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_harga_5)
        Me.Panel_ubah_5.Controls.Add(Me.txt_jenis_kain_5)
        Me.Panel_ubah_5.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_5.Name = "Panel_ubah_5"
        Me.Panel_ubah_5.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_5.TabIndex = 105
        '
        'cb_satuan_5
        '
        Me.cb_satuan_5.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_5.FormattingEnabled = True
        Me.cb_satuan_5.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_5.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_5.Name = "cb_satuan_5"
        Me.cb_satuan_5.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_5.TabIndex = 500
        Me.cb_satuan_5.Text = "Meter"
        '
        'txt_total_harga_5
        '
        Me.txt_total_harga_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_5.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_5.Name = "txt_total_harga_5"
        Me.txt_total_harga_5.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_5.TabIndex = 9
        Me.txt_total_harga_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_5
        '
        Me.txt_keterangan_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_5.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_5.Name = "txt_keterangan_5"
        Me.txt_keterangan_5.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_5.TabIndex = 8
        '
        'txt_gudang_5
        '
        Me.txt_gudang_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_5.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_5.Name = "txt_gudang_5"
        Me.txt_gudang_5.ReadOnly = True
        Me.txt_gudang_5.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_5.TabIndex = 7
        Me.txt_gudang_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_5
        '
        Me.txt_supplier_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_5.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_5.Name = "txt_supplier_5"
        Me.txt_supplier_5.ReadOnly = True
        Me.txt_supplier_5.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_5.TabIndex = 14
        Me.txt_supplier_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_kontrak_5
        '
        Me.txt_no_kontrak_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_5.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_5.Name = "txt_no_kontrak_5"
        Me.txt_no_kontrak_5.ReadOnly = True
        Me.txt_no_kontrak_5.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_5.TabIndex = 100
        Me.txt_no_kontrak_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_5
        '
        Me.txt_qty_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_5.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_5.Name = "txt_qty_5"
        Me.txt_qty_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_5.TabIndex = 6
        Me.txt_qty_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_5
        '
        Me.txt_sisa_kontrak_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_5.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_5.Name = "txt_sisa_kontrak_5"
        Me.txt_sisa_kontrak_5.ReadOnly = True
        Me.txt_sisa_kontrak_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_5.TabIndex = 5
        Me.txt_sisa_kontrak_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_5
        '
        Me.txt_harga_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_5.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_5.Name = "txt_harga_5"
        Me.txt_harga_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_5.TabIndex = 1
        Me.txt_harga_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_5
        '
        Me.txt_jenis_kain_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_5.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_5.Name = "txt_jenis_kain_5"
        Me.txt_jenis_kain_5.ReadOnly = True
        Me.txt_jenis_kain_5.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_5.TabIndex = 2
        Me.txt_jenis_kain_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_5
        '
        Me.Panel_rb_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_5.Controls.Add(Me.rb_kontrak_5)
        Me.Panel_rb_5.Controls.Add(Me.rb_non_kontrak_5)
        Me.Panel_rb_5.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_5.Name = "Panel_rb_5"
        Me.Panel_rb_5.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_5.TabIndex = 76
        '
        'rb_kontrak_5
        '
        Me.rb_kontrak_5.AutoSize = True
        Me.rb_kontrak_5.Checked = True
        Me.rb_kontrak_5.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_5.Name = "rb_kontrak_5"
        Me.rb_kontrak_5.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_5.TabIndex = 0
        Me.rb_kontrak_5.TabStop = True
        Me.rb_kontrak_5.Text = "Kontrak"
        Me.rb_kontrak_5.UseVisualStyleBackColor = True
        '
        'rb_non_kontrak_5
        '
        Me.rb_non_kontrak_5.AutoSize = True
        Me.rb_non_kontrak_5.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_5.Name = "rb_non_kontrak_5"
        Me.rb_non_kontrak_5.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_5.TabIndex = 34
        Me.rb_non_kontrak_5.Text = "Non Kontrak"
        Me.rb_non_kontrak_5.UseVisualStyleBackColor = True
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(8, 18)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(13, 13)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "5"
        '
        'txt_id_grey_5
        '
        Me.txt_id_grey_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_5.Location = New System.Drawing.Point(92, 6)
        Me.txt_id_grey_5.Name = "txt_id_grey_5"
        Me.txt_id_grey_5.ReadOnly = True
        Me.txt_id_grey_5.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_5.TabIndex = 77
        Me.txt_id_grey_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_6
        '
        Me.Panel_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_6.Controls.Add(Me.Panel_ubah_6)
        Me.Panel_6.Controls.Add(Me.Panel_rb_6)
        Me.Panel_6.Controls.Add(Me.Label32)
        Me.Panel_6.Location = New System.Drawing.Point(-1, 405)
        Me.Panel_6.Name = "Panel_6"
        Me.Panel_6.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_6.TabIndex = 0
        '
        'Panel_ubah_6
        '
        Me.Panel_ubah_6.Controls.Add(Me.cb_satuan_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_total_harga_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_keterangan_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_gudang_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_supplier_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_no_kontrak_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_qty_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_sisa_kontrak_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_harga_6)
        Me.Panel_ubah_6.Controls.Add(Me.txt_jenis_kain_6)
        Me.Panel_ubah_6.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_6.Name = "Panel_ubah_6"
        Me.Panel_ubah_6.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_6.TabIndex = 105
        '
        'cb_satuan_6
        '
        Me.cb_satuan_6.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_6.FormattingEnabled = True
        Me.cb_satuan_6.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_6.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_6.Name = "cb_satuan_6"
        Me.cb_satuan_6.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_6.TabIndex = 500
        Me.cb_satuan_6.Text = "Meter"
        '
        'txt_total_harga_6
        '
        Me.txt_total_harga_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_6.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_6.Name = "txt_total_harga_6"
        Me.txt_total_harga_6.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_6.TabIndex = 9
        Me.txt_total_harga_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_6
        '
        Me.txt_keterangan_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_6.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_6.Name = "txt_keterangan_6"
        Me.txt_keterangan_6.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_6.TabIndex = 8
        '
        'txt_gudang_6
        '
        Me.txt_gudang_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_6.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_6.Name = "txt_gudang_6"
        Me.txt_gudang_6.ReadOnly = True
        Me.txt_gudang_6.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_6.TabIndex = 7
        Me.txt_gudang_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_6
        '
        Me.txt_supplier_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_6.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_6.Name = "txt_supplier_6"
        Me.txt_supplier_6.ReadOnly = True
        Me.txt_supplier_6.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_6.TabIndex = 14
        Me.txt_supplier_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_kontrak_6
        '
        Me.txt_no_kontrak_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_6.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_6.Name = "txt_no_kontrak_6"
        Me.txt_no_kontrak_6.ReadOnly = True
        Me.txt_no_kontrak_6.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_6.TabIndex = 100
        Me.txt_no_kontrak_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_6
        '
        Me.txt_qty_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_6.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_6.Name = "txt_qty_6"
        Me.txt_qty_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_6.TabIndex = 6
        Me.txt_qty_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_6
        '
        Me.txt_sisa_kontrak_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_6.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_6.Name = "txt_sisa_kontrak_6"
        Me.txt_sisa_kontrak_6.ReadOnly = True
        Me.txt_sisa_kontrak_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_6.TabIndex = 5
        Me.txt_sisa_kontrak_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_6
        '
        Me.txt_harga_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_6.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_6.Name = "txt_harga_6"
        Me.txt_harga_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_6.TabIndex = 1
        Me.txt_harga_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_6
        '
        Me.txt_jenis_kain_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_6.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_6.Name = "txt_jenis_kain_6"
        Me.txt_jenis_kain_6.ReadOnly = True
        Me.txt_jenis_kain_6.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_6.TabIndex = 2
        Me.txt_jenis_kain_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_6
        '
        Me.Panel_rb_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_6.Controls.Add(Me.rb_kontrak_6)
        Me.Panel_rb_6.Controls.Add(Me.rb_non_kontrak_6)
        Me.Panel_rb_6.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_6.Name = "Panel_rb_6"
        Me.Panel_rb_6.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_6.TabIndex = 76
        '
        'rb_kontrak_6
        '
        Me.rb_kontrak_6.AutoSize = True
        Me.rb_kontrak_6.Checked = True
        Me.rb_kontrak_6.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_6.Name = "rb_kontrak_6"
        Me.rb_kontrak_6.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_6.TabIndex = 0
        Me.rb_kontrak_6.TabStop = True
        Me.rb_kontrak_6.Text = "Kontrak"
        Me.rb_kontrak_6.UseVisualStyleBackColor = True
        '
        'rb_non_kontrak_6
        '
        Me.rb_non_kontrak_6.AutoSize = True
        Me.rb_non_kontrak_6.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_6.Name = "rb_non_kontrak_6"
        Me.rb_non_kontrak_6.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_6.TabIndex = 34
        Me.rb_non_kontrak_6.Text = "Non Kontrak"
        Me.rb_non_kontrak_6.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(8, 18)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(13, 13)
        Me.Label32.TabIndex = 1
        Me.Label32.Text = "6"
        '
        'txt_id_grey_6
        '
        Me.txt_id_grey_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_6.Location = New System.Drawing.Point(92, 26)
        Me.txt_id_grey_6.Name = "txt_id_grey_6"
        Me.txt_id_grey_6.ReadOnly = True
        Me.txt_id_grey_6.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_6.TabIndex = 77
        Me.txt_id_grey_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_7
        '
        Me.Panel_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_7.Controls.Add(Me.Panel_ubah_7)
        Me.Panel_7.Controls.Add(Me.Panel_rb_7)
        Me.Panel_7.Controls.Add(Me.Label35)
        Me.Panel_7.Location = New System.Drawing.Point(-1, 454)
        Me.Panel_7.Name = "Panel_7"
        Me.Panel_7.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_7.TabIndex = 0
        '
        'Panel_ubah_7
        '
        Me.Panel_ubah_7.Controls.Add(Me.cb_satuan_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_total_harga_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_keterangan_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_gudang_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_supplier_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_no_kontrak_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_qty_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_sisa_kontrak_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_harga_7)
        Me.Panel_ubah_7.Controls.Add(Me.txt_jenis_kain_7)
        Me.Panel_ubah_7.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_7.Name = "Panel_ubah_7"
        Me.Panel_ubah_7.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_7.TabIndex = 105
        '
        'cb_satuan_7
        '
        Me.cb_satuan_7.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_7.FormattingEnabled = True
        Me.cb_satuan_7.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_7.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_7.Name = "cb_satuan_7"
        Me.cb_satuan_7.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_7.TabIndex = 500
        Me.cb_satuan_7.Text = "Meter"
        '
        'txt_total_harga_7
        '
        Me.txt_total_harga_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_7.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_7.Name = "txt_total_harga_7"
        Me.txt_total_harga_7.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_7.TabIndex = 9
        Me.txt_total_harga_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_7
        '
        Me.txt_keterangan_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_7.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_7.Name = "txt_keterangan_7"
        Me.txt_keterangan_7.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_7.TabIndex = 8
        '
        'txt_gudang_7
        '
        Me.txt_gudang_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_7.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_7.Name = "txt_gudang_7"
        Me.txt_gudang_7.ReadOnly = True
        Me.txt_gudang_7.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_7.TabIndex = 7
        Me.txt_gudang_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_7
        '
        Me.txt_supplier_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_7.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_7.Name = "txt_supplier_7"
        Me.txt_supplier_7.ReadOnly = True
        Me.txt_supplier_7.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_7.TabIndex = 14
        Me.txt_supplier_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_kontrak_7
        '
        Me.txt_no_kontrak_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_7.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_7.Name = "txt_no_kontrak_7"
        Me.txt_no_kontrak_7.ReadOnly = True
        Me.txt_no_kontrak_7.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_7.TabIndex = 100
        Me.txt_no_kontrak_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_7
        '
        Me.txt_qty_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_7.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_7.Name = "txt_qty_7"
        Me.txt_qty_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_7.TabIndex = 6
        Me.txt_qty_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_7
        '
        Me.txt_sisa_kontrak_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_7.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_7.Name = "txt_sisa_kontrak_7"
        Me.txt_sisa_kontrak_7.ReadOnly = True
        Me.txt_sisa_kontrak_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_7.TabIndex = 5
        Me.txt_sisa_kontrak_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_7
        '
        Me.txt_harga_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_7.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_7.Name = "txt_harga_7"
        Me.txt_harga_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_7.TabIndex = 1
        Me.txt_harga_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_7
        '
        Me.txt_jenis_kain_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_7.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_7.Name = "txt_jenis_kain_7"
        Me.txt_jenis_kain_7.ReadOnly = True
        Me.txt_jenis_kain_7.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_7.TabIndex = 2
        Me.txt_jenis_kain_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_7
        '
        Me.Panel_rb_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_7.Controls.Add(Me.rb_kontrak_7)
        Me.Panel_rb_7.Controls.Add(Me.rb_non_kontrak_7)
        Me.Panel_rb_7.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_7.Name = "Panel_rb_7"
        Me.Panel_rb_7.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_7.TabIndex = 76
        '
        'rb_kontrak_7
        '
        Me.rb_kontrak_7.AutoSize = True
        Me.rb_kontrak_7.Checked = True
        Me.rb_kontrak_7.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_7.Name = "rb_kontrak_7"
        Me.rb_kontrak_7.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_7.TabIndex = 0
        Me.rb_kontrak_7.TabStop = True
        Me.rb_kontrak_7.Text = "Kontrak"
        Me.rb_kontrak_7.UseVisualStyleBackColor = True
        '
        'rb_non_kontrak_7
        '
        Me.rb_non_kontrak_7.AutoSize = True
        Me.rb_non_kontrak_7.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_7.Name = "rb_non_kontrak_7"
        Me.rb_non_kontrak_7.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_7.TabIndex = 34
        Me.rb_non_kontrak_7.Text = "Non Kontrak"
        Me.rb_non_kontrak_7.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(8, 18)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(13, 13)
        Me.Label35.TabIndex = 1
        Me.Label35.Text = "7"
        '
        'txt_id_grey_7
        '
        Me.txt_id_grey_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_7.Location = New System.Drawing.Point(92, 46)
        Me.txt_id_grey_7.Name = "txt_id_grey_7"
        Me.txt_id_grey_7.ReadOnly = True
        Me.txt_id_grey_7.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_7.TabIndex = 77
        Me.txt_id_grey_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_8
        '
        Me.Panel_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_8.Controls.Add(Me.Panel_ubah_8)
        Me.Panel_8.Controls.Add(Me.Panel_rb_8)
        Me.Panel_8.Controls.Add(Me.Label38)
        Me.Panel_8.Location = New System.Drawing.Point(-1, 503)
        Me.Panel_8.Name = "Panel_8"
        Me.Panel_8.Size = New System.Drawing.Size(1300, 50)
        Me.Panel_8.TabIndex = 0
        '
        'Panel_ubah_8
        '
        Me.Panel_ubah_8.Controls.Add(Me.cb_satuan_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_total_harga_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_keterangan_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_gudang_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_supplier_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_no_kontrak_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_qty_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_sisa_kontrak_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_harga_8)
        Me.Panel_ubah_8.Controls.Add(Me.txt_jenis_kain_8)
        Me.Panel_ubah_8.Location = New System.Drawing.Point(138, 4)
        Me.Panel_ubah_8.Name = "Panel_ubah_8"
        Me.Panel_ubah_8.Size = New System.Drawing.Size(1150, 40)
        Me.Panel_ubah_8.TabIndex = 105
        '
        'cb_satuan_8
        '
        Me.cb_satuan_8.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_8.FormattingEnabled = True
        Me.cb_satuan_8.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_8.Location = New System.Drawing.Point(676, 10)
        Me.cb_satuan_8.Name = "cb_satuan_8"
        Me.cb_satuan_8.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_8.TabIndex = 500
        Me.cb_satuan_8.Text = "Meter"
        '
        'txt_total_harga_8
        '
        Me.txt_total_harga_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_8.Location = New System.Drawing.Point(732, 10)
        Me.txt_total_harga_8.Name = "txt_total_harga_8"
        Me.txt_total_harga_8.Size = New System.Drawing.Size(129, 20)
        Me.txt_total_harga_8.TabIndex = 9
        Me.txt_total_harga_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_keterangan_8
        '
        Me.txt_keterangan_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_8.Location = New System.Drawing.Point(989, 10)
        Me.txt_keterangan_8.Name = "txt_keterangan_8"
        Me.txt_keterangan_8.Size = New System.Drawing.Size(154, 20)
        Me.txt_keterangan_8.TabIndex = 8
        '
        'txt_gudang_8
        '
        Me.txt_gudang_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_8.Location = New System.Drawing.Point(867, 10)
        Me.txt_gudang_8.Name = "txt_gudang_8"
        Me.txt_gudang_8.ReadOnly = True
        Me.txt_gudang_8.Size = New System.Drawing.Size(116, 20)
        Me.txt_gudang_8.TabIndex = 7
        Me.txt_gudang_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_supplier_8
        '
        Me.txt_supplier_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_supplier_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier_8.Location = New System.Drawing.Point(241, 10)
        Me.txt_supplier_8.Name = "txt_supplier_8"
        Me.txt_supplier_8.ReadOnly = True
        Me.txt_supplier_8.Size = New System.Drawing.Size(111, 20)
        Me.txt_supplier_8.TabIndex = 14
        Me.txt_supplier_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_kontrak_8
        '
        Me.txt_no_kontrak_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_kontrak_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak_8.Location = New System.Drawing.Point(7, 10)
        Me.txt_no_kontrak_8.Name = "txt_no_kontrak_8"
        Me.txt_no_kontrak_8.ReadOnly = True
        Me.txt_no_kontrak_8.Size = New System.Drawing.Size(92, 20)
        Me.txt_no_kontrak_8.TabIndex = 100
        Me.txt_no_kontrak_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_8
        '
        Me.txt_qty_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_8.Location = New System.Drawing.Point(570, 10)
        Me.txt_qty_8.Name = "txt_qty_8"
        Me.txt_qty_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_8.TabIndex = 6
        Me.txt_qty_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_sisa_kontrak_8
        '
        Me.txt_sisa_kontrak_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sisa_kontrak_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak_8.Location = New System.Drawing.Point(464, 10)
        Me.txt_sisa_kontrak_8.Name = "txt_sisa_kontrak_8"
        Me.txt_sisa_kontrak_8.ReadOnly = True
        Me.txt_sisa_kontrak_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_kontrak_8.TabIndex = 5
        Me.txt_sisa_kontrak_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_8
        '
        Me.txt_harga_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_8.Location = New System.Drawing.Point(358, 10)
        Me.txt_harga_8.Name = "txt_harga_8"
        Me.txt_harga_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga_8.TabIndex = 1
        Me.txt_harga_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_8
        '
        Me.txt_jenis_kain_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_8.Location = New System.Drawing.Point(105, 10)
        Me.txt_jenis_kain_8.Name = "txt_jenis_kain_8"
        Me.txt_jenis_kain_8.ReadOnly = True
        Me.txt_jenis_kain_8.Size = New System.Drawing.Size(130, 20)
        Me.txt_jenis_kain_8.TabIndex = 2
        Me.txt_jenis_kain_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel_rb_8
        '
        Me.Panel_rb_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_rb_8.Controls.Add(Me.rb_kontrak_8)
        Me.Panel_rb_8.Controls.Add(Me.rb_non_kontrak_8)
        Me.Panel_rb_8.Location = New System.Drawing.Point(27, -1)
        Me.Panel_rb_8.Name = "Panel_rb_8"
        Me.Panel_rb_8.Size = New System.Drawing.Size(100, 50)
        Me.Panel_rb_8.TabIndex = 76
        '
        'rb_kontrak_8
        '
        Me.rb_kontrak_8.AutoSize = True
        Me.rb_kontrak_8.Checked = True
        Me.rb_kontrak_8.Location = New System.Drawing.Point(7, 5)
        Me.rb_kontrak_8.Name = "rb_kontrak_8"
        Me.rb_kontrak_8.Size = New System.Drawing.Size(62, 17)
        Me.rb_kontrak_8.TabIndex = 33
        Me.rb_kontrak_8.TabStop = True
        Me.rb_kontrak_8.Text = "Kontrak"
        Me.rb_kontrak_8.UseVisualStyleBackColor = True
        '
        'rb_non_kontrak_8
        '
        Me.rb_non_kontrak_8.AutoSize = True
        Me.rb_non_kontrak_8.Location = New System.Drawing.Point(7, 27)
        Me.rb_non_kontrak_8.Name = "rb_non_kontrak_8"
        Me.rb_non_kontrak_8.Size = New System.Drawing.Size(85, 17)
        Me.rb_non_kontrak_8.TabIndex = 34
        Me.rb_non_kontrak_8.Text = "Non Kontrak"
        Me.rb_non_kontrak_8.UseVisualStyleBackColor = True
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(8, 18)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(13, 13)
        Me.Label38.TabIndex = 1
        Me.Label38.Text = "8"
        '
        'txt_id_grey_8
        '
        Me.txt_id_grey_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_grey_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey_8.Location = New System.Drawing.Point(92, 66)
        Me.txt_id_grey_8.Name = "txt_id_grey_8"
        Me.txt_id_grey_8.ReadOnly = True
        Me.txt_id_grey_8.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_grey_8.TabIndex = 77
        Me.txt_id_grey_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label15)
        Me.Panel5.Controls.Add(Me.txt_id_grey_1)
        Me.Panel5.Controls.Add(Me.txt_id_grey_2)
        Me.Panel5.Controls.Add(Me.txt_id_grey_3)
        Me.Panel5.Controls.Add(Me.txt_id_grey_4)
        Me.Panel5.Controls.Add(Me.txt_id_grey_6)
        Me.Panel5.Controls.Add(Me.txt_id_grey_7)
        Me.Panel5.Controls.Add(Me.txt_id_grey_8)
        Me.Panel5.Controls.Add(Me.txt_id_grey_5)
        Me.Panel5.Location = New System.Drawing.Point(33, 2)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(179, 110)
        Me.Panel5.TabIndex = 77
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.txt_id_beli_1)
        Me.Panel7.Controls.Add(Me.txt_id_beli_2)
        Me.Panel7.Controls.Add(Me.txt_id_beli_3)
        Me.Panel7.Controls.Add(Me.txt_id_beli_4)
        Me.Panel7.Controls.Add(Me.txt_id_beli_6)
        Me.Panel7.Controls.Add(Me.txt_id_beli_7)
        Me.Panel7.Controls.Add(Me.txt_id_beli_8)
        Me.Panel7.Controls.Add(Me.txt_id_beli_5)
        Me.Panel7.Location = New System.Drawing.Point(216, 2)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(179, 110)
        Me.Panel7.TabIndex = 108
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(68, 91)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 78
        Me.Label6.Text = "ID Beli"
        '
        'txt_id_beli_1
        '
        Me.txt_id_beli_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_1.Location = New System.Drawing.Point(7, 6)
        Me.txt_id_beli_1.Name = "txt_id_beli_1"
        Me.txt_id_beli_1.ReadOnly = True
        Me.txt_id_beli_1.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_1.TabIndex = 77
        Me.txt_id_beli_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_2
        '
        Me.txt_id_beli_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_2.Location = New System.Drawing.Point(7, 26)
        Me.txt_id_beli_2.Name = "txt_id_beli_2"
        Me.txt_id_beli_2.ReadOnly = True
        Me.txt_id_beli_2.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_2.TabIndex = 77
        Me.txt_id_beli_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_3
        '
        Me.txt_id_beli_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_3.Location = New System.Drawing.Point(7, 46)
        Me.txt_id_beli_3.Name = "txt_id_beli_3"
        Me.txt_id_beli_3.ReadOnly = True
        Me.txt_id_beli_3.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_3.TabIndex = 77
        Me.txt_id_beli_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_4
        '
        Me.txt_id_beli_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_4.Location = New System.Drawing.Point(7, 66)
        Me.txt_id_beli_4.Name = "txt_id_beli_4"
        Me.txt_id_beli_4.ReadOnly = True
        Me.txt_id_beli_4.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_4.TabIndex = 77
        Me.txt_id_beli_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_6
        '
        Me.txt_id_beli_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_6.Location = New System.Drawing.Point(92, 26)
        Me.txt_id_beli_6.Name = "txt_id_beli_6"
        Me.txt_id_beli_6.ReadOnly = True
        Me.txt_id_beli_6.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_6.TabIndex = 77
        Me.txt_id_beli_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_7
        '
        Me.txt_id_beli_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_7.Location = New System.Drawing.Point(92, 46)
        Me.txt_id_beli_7.Name = "txt_id_beli_7"
        Me.txt_id_beli_7.ReadOnly = True
        Me.txt_id_beli_7.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_7.TabIndex = 77
        Me.txt_id_beli_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_8
        '
        Me.txt_id_beli_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_8.Location = New System.Drawing.Point(92, 66)
        Me.txt_id_beli_8.Name = "txt_id_beli_8"
        Me.txt_id_beli_8.ReadOnly = True
        Me.txt_id_beli_8.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_8.TabIndex = 77
        Me.txt_id_beli_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_5
        '
        Me.txt_id_beli_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_5.Location = New System.Drawing.Point(92, 6)
        Me.txt_id_beli_5.Name = "txt_id_beli_5"
        Me.txt_id_beli_5.ReadOnly = True
        Me.txt_id_beli_5.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_5.TabIndex = 77
        Me.txt_id_beli_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label8)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_1)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_2)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_3)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_4)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_6)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_7)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_8)
        Me.Panel6.Controls.Add(Me.txt_id_hutang_5)
        Me.Panel6.Location = New System.Drawing.Point(858, 2)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(179, 110)
        Me.Panel6.TabIndex = 109
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(68, 91)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 13)
        Me.Label8.TabIndex = 78
        Me.Label8.Text = "ID Hutang"
        '
        'txt_id_hutang_1
        '
        Me.txt_id_hutang_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_1.Location = New System.Drawing.Point(7, 6)
        Me.txt_id_hutang_1.Name = "txt_id_hutang_1"
        Me.txt_id_hutang_1.ReadOnly = True
        Me.txt_id_hutang_1.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_1.TabIndex = 77
        Me.txt_id_hutang_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_hutang_2
        '
        Me.txt_id_hutang_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_2.Location = New System.Drawing.Point(7, 26)
        Me.txt_id_hutang_2.Name = "txt_id_hutang_2"
        Me.txt_id_hutang_2.ReadOnly = True
        Me.txt_id_hutang_2.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_2.TabIndex = 77
        Me.txt_id_hutang_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_hutang_3
        '
        Me.txt_id_hutang_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_3.Location = New System.Drawing.Point(7, 46)
        Me.txt_id_hutang_3.Name = "txt_id_hutang_3"
        Me.txt_id_hutang_3.ReadOnly = True
        Me.txt_id_hutang_3.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_3.TabIndex = 77
        Me.txt_id_hutang_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_hutang_4
        '
        Me.txt_id_hutang_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_4.Location = New System.Drawing.Point(7, 66)
        Me.txt_id_hutang_4.Name = "txt_id_hutang_4"
        Me.txt_id_hutang_4.ReadOnly = True
        Me.txt_id_hutang_4.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_4.TabIndex = 77
        Me.txt_id_hutang_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_hutang_6
        '
        Me.txt_id_hutang_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_6.Location = New System.Drawing.Point(92, 26)
        Me.txt_id_hutang_6.Name = "txt_id_hutang_6"
        Me.txt_id_hutang_6.ReadOnly = True
        Me.txt_id_hutang_6.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_6.TabIndex = 77
        Me.txt_id_hutang_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_hutang_7
        '
        Me.txt_id_hutang_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_7.Location = New System.Drawing.Point(92, 46)
        Me.txt_id_hutang_7.Name = "txt_id_hutang_7"
        Me.txt_id_hutang_7.ReadOnly = True
        Me.txt_id_hutang_7.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_7.TabIndex = 77
        Me.txt_id_hutang_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_hutang_8
        '
        Me.txt_id_hutang_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_8.Location = New System.Drawing.Point(92, 66)
        Me.txt_id_hutang_8.Name = "txt_id_hutang_8"
        Me.txt_id_hutang_8.ReadOnly = True
        Me.txt_id_hutang_8.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_8.TabIndex = 77
        Me.txt_id_hutang_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_hutang_5
        '
        Me.txt_id_hutang_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_hutang_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang_5.Location = New System.Drawing.Point(92, 6)
        Me.txt_id_hutang_5.Name = "txt_id_hutang_5"
        Me.txt_id_hutang_5.ReadOnly = True
        Me.txt_id_hutang_5.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_hutang_5.TabIndex = 77
        Me.txt_id_hutang_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label9)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_1)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_2)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_3)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_4)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_6)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_7)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_8)
        Me.Panel8.Controls.Add(Me.txt_asal_supplier_5)
        Me.Panel8.Location = New System.Drawing.Point(1051, 2)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(179, 110)
        Me.Panel8.TabIndex = 110
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(55, 91)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 78
        Me.Label9.Text = "Asal Supplier"
        '
        'txt_asal_supplier_1
        '
        Me.txt_asal_supplier_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_1.Location = New System.Drawing.Point(7, 6)
        Me.txt_asal_supplier_1.Name = "txt_asal_supplier_1"
        Me.txt_asal_supplier_1.ReadOnly = True
        Me.txt_asal_supplier_1.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_1.TabIndex = 77
        Me.txt_asal_supplier_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_supplier_2
        '
        Me.txt_asal_supplier_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_2.Location = New System.Drawing.Point(7, 26)
        Me.txt_asal_supplier_2.Name = "txt_asal_supplier_2"
        Me.txt_asal_supplier_2.ReadOnly = True
        Me.txt_asal_supplier_2.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_2.TabIndex = 77
        Me.txt_asal_supplier_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_supplier_3
        '
        Me.txt_asal_supplier_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_3.Location = New System.Drawing.Point(7, 46)
        Me.txt_asal_supplier_3.Name = "txt_asal_supplier_3"
        Me.txt_asal_supplier_3.ReadOnly = True
        Me.txt_asal_supplier_3.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_3.TabIndex = 77
        Me.txt_asal_supplier_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_supplier_4
        '
        Me.txt_asal_supplier_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_4.Location = New System.Drawing.Point(7, 66)
        Me.txt_asal_supplier_4.Name = "txt_asal_supplier_4"
        Me.txt_asal_supplier_4.ReadOnly = True
        Me.txt_asal_supplier_4.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_4.TabIndex = 77
        Me.txt_asal_supplier_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_supplier_6
        '
        Me.txt_asal_supplier_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_6.Location = New System.Drawing.Point(92, 26)
        Me.txt_asal_supplier_6.Name = "txt_asal_supplier_6"
        Me.txt_asal_supplier_6.ReadOnly = True
        Me.txt_asal_supplier_6.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_6.TabIndex = 77
        Me.txt_asal_supplier_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_supplier_7
        '
        Me.txt_asal_supplier_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_7.Location = New System.Drawing.Point(92, 46)
        Me.txt_asal_supplier_7.Name = "txt_asal_supplier_7"
        Me.txt_asal_supplier_7.ReadOnly = True
        Me.txt_asal_supplier_7.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_7.TabIndex = 77
        Me.txt_asal_supplier_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_supplier_8
        '
        Me.txt_asal_supplier_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_8.Location = New System.Drawing.Point(92, 66)
        Me.txt_asal_supplier_8.Name = "txt_asal_supplier_8"
        Me.txt_asal_supplier_8.ReadOnly = True
        Me.txt_asal_supplier_8.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_8.TabIndex = 77
        Me.txt_asal_supplier_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_supplier_5
        '
        Me.txt_asal_supplier_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_supplier_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier_5.Location = New System.Drawing.Point(92, 6)
        Me.txt_asal_supplier_5.Name = "txt_asal_supplier_5"
        Me.txt_asal_supplier_5.ReadOnly = True
        Me.txt_asal_supplier_5.Size = New System.Drawing.Size(79, 20)
        Me.txt_asal_supplier_5.TabIndex = 77
        Me.txt_asal_supplier_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'form_input_pembelian_grey_baru
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1298, 622)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel_3)
        Me.Controls.Add(Me.Panel_2)
        Me.Controls.Add(Me.Panel_5)
        Me.Controls.Add(Me.Panel_6)
        Me.Controls.Add(Me.Panel_7)
        Me.Controls.Add(Me.Panel_8)
        Me.Controls.Add(Me.Panel_4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel_1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel8)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_pembelian_grey_baru"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM INPUT PEMBELIAN GREY BARU"
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel_1.ResumeLayout(False)
        Me.Panel_1.PerformLayout()
        Me.Panel_ubah_1.ResumeLayout(False)
        Me.Panel_ubah_1.PerformLayout()
        Me.Panel_rb_1.ResumeLayout(False)
        Me.Panel_rb_1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel_4.ResumeLayout(False)
        Me.Panel_4.PerformLayout()
        Me.Panel_ubah_4.ResumeLayout(False)
        Me.Panel_ubah_4.PerformLayout()
        Me.Panel_rb_4.ResumeLayout(False)
        Me.Panel_rb_4.PerformLayout()
        Me.Panel_3.ResumeLayout(False)
        Me.Panel_3.PerformLayout()
        Me.Panel_ubah_3.ResumeLayout(False)
        Me.Panel_ubah_3.PerformLayout()
        Me.Panel_rb_3.ResumeLayout(False)
        Me.Panel_rb_3.PerformLayout()
        Me.Panel_2.ResumeLayout(False)
        Me.Panel_2.PerformLayout()
        Me.Panel_ubah_2.ResumeLayout(False)
        Me.Panel_ubah_2.PerformLayout()
        Me.Panel_rb_2.ResumeLayout(False)
        Me.Panel_rb_2.PerformLayout()
        Me.Panel_5.ResumeLayout(False)
        Me.Panel_5.PerformLayout()
        Me.Panel_ubah_5.ResumeLayout(False)
        Me.Panel_ubah_5.PerformLayout()
        Me.Panel_rb_5.ResumeLayout(False)
        Me.Panel_rb_5.PerformLayout()
        Me.Panel_6.ResumeLayout(False)
        Me.Panel_6.PerformLayout()
        Me.Panel_ubah_6.ResumeLayout(False)
        Me.Panel_ubah_6.PerformLayout()
        Me.Panel_rb_6.ResumeLayout(False)
        Me.Panel_rb_6.PerformLayout()
        Me.Panel_7.ResumeLayout(False)
        Me.Panel_7.PerformLayout()
        Me.Panel_ubah_7.ResumeLayout(False)
        Me.Panel_ubah_7.PerformLayout()
        Me.Panel_rb_7.ResumeLayout(False)
        Me.Panel_rb_7.PerformLayout()
        Me.Panel_8.ResumeLayout(False)
        Me.Panel_8.PerformLayout()
        Me.Panel_ubah_8.ResumeLayout(False)
        Me.Panel_ubah_8.PerformLayout()
        Me.Panel_rb_8.ResumeLayout(False)
        Me.Panel_rb_8.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_id_grey_1 As System.Windows.Forms.TextBox
    Friend WithEvents rb_non_kontrak_1 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_kontrak_1 As System.Windows.Forms.RadioButton
    Friend WithEvents dtp_jatuh_tempo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txt_surat_jalan As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_no_kontrak_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btn_hapus_8 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_8 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_7 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_7 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_7 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_6 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_6 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_6 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_5 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_5 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_5 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_4 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_4 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_4 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_3 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_3 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_3 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_2 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_1 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_1 As System.Windows.Forms.Button
    Friend WithEvents Panel_1 As System.Windows.Forms.Panel
    Friend WithEvents txt_total_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_1 As System.Windows.Forms.Panel
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel_4 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_4 As System.Windows.Forms.Panel
    Friend WithEvents rb_kontrak_4 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_non_kontrak_4 As System.Windows.Forms.RadioButton
    Friend WithEvents txt_id_grey_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_kontrak_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_4 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Panel_3 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_3 As System.Windows.Forms.Panel
    Friend WithEvents rb_kontrak_3 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_non_kontrak_3 As System.Windows.Forms.RadioButton
    Friend WithEvents txt_id_grey_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_kontrak_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_3 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Panel_2 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_2 As System.Windows.Forms.Panel
    Friend WithEvents rb_kontrak_2 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_non_kontrak_2 As System.Windows.Forms.RadioButton
    Friend WithEvents txt_id_grey_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_kontrak_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_2 As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Panel_5 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_5 As System.Windows.Forms.Panel
    Friend WithEvents rb_kontrak_5 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_non_kontrak_5 As System.Windows.Forms.RadioButton
    Friend WithEvents txt_id_grey_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_kontrak_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_5 As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Panel_6 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_6 As System.Windows.Forms.Panel
    Friend WithEvents rb_kontrak_6 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_non_kontrak_6 As System.Windows.Forms.RadioButton
    Friend WithEvents txt_id_grey_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_kontrak_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_6 As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Panel_7 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_7 As System.Windows.Forms.Panel
    Friend WithEvents rb_kontrak_7 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_non_kontrak_7 As System.Windows.Forms.RadioButton
    Friend WithEvents txt_id_grey_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_kontrak_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_7 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Panel_8 As System.Windows.Forms.Panel
    Friend WithEvents Panel_rb_8 As System.Windows.Forms.Panel
    Friend WithEvents rb_kontrak_8 As System.Windows.Forms.RadioButton
    Friend WithEvents rb_non_kontrak_8 As System.Windows.Forms.RadioButton
    Friend WithEvents txt_id_grey_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_kontrak_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_kontrak_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_8 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_id_beli_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_5 As System.Windows.Forms.TextBox
    Friend WithEvents cb_satuan_1 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_4 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_3 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_2 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_5 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_6 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_7 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_8 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel_ubah_1 As System.Windows.Forms.Panel
    Friend WithEvents Panel_ubah_4 As System.Windows.Forms.Panel
    Friend WithEvents Panel_ubah_3 As System.Windows.Forms.Panel
    Friend WithEvents Panel_ubah_2 As System.Windows.Forms.Panel
    Friend WithEvents Panel_ubah_5 As System.Windows.Forms.Panel
    Friend WithEvents Panel_ubah_6 As System.Windows.Forms.Panel
    Friend WithEvents Panel_ubah_7 As System.Windows.Forms.Panel
    Friend WithEvents Panel_ubah_8 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btn_batal_8 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_7 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_6 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_5 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_4 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_3 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_2 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_1 As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_id_hutang_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_hutang_5 As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_supplier_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_supplier_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_supplier_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_supplier_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_supplier_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_supplier_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_supplier_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_supplier_5 As System.Windows.Forms.TextBox
End Class
