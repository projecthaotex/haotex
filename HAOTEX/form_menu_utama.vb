﻿Public Class form_menu_utama

    Private Sub Form_Menu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        form_koneksi_database.MdiParent = Me
        form_koneksi_database.Show()
        form_koneksi_database.Focus()
        MenuStrip1.Visible = False
        StatusStrip1.Visible = False
    End Sub

    Private Sub KELUARToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KELUARToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub KONTRAKToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KONTRAKToolStripMenuItem.Click
        form_kontrak_grey.MdiParent = Me
        form_kontrak_grey.Show()
        form_kontrak_grey.Focus()
    End Sub

    Private Sub SupplierToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SupplierToolStripMenuItem.Click
        form_input_supplier.MdiParent = Me
        form_input_supplier.Show()
        form_input_supplier.Label1.Text = "NAMA SUPPLIER"
        form_input_supplier.BtnOk.Visible = False
        form_input_supplier.Focus()
    End Sub

    Private Sub JenisKainToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JenisKainToolStripMenuItem.Click
        form_input_jenis_kain.MdiParent = Me
        form_input_jenis_kain.Show()
        form_input_jenis_kain.Label1.Text = "NAMA JENIS KAIN"
        form_input_jenis_kain.BtnOk.Visible = False
        form_input_jenis_kain.Focus()
    End Sub

    Private Sub GudangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GudangToolStripMenuItem.Click
        form_input_gudang.MdiParent = Me
        form_input_gudang.Show()
        form_input_gudang.Label1.Text = "NAMA GUDANG"
        form_input_gudang.BtnOk.Visible = False
        form_input_gudang.Focus()
    End Sub

    Private Sub CustomerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CustomerToolStripMenuItem.Click
        form_input_customer.MdiParent = Me
        form_input_customer.Show()
        form_input_customer.Label1.Text = "NAMA CUSTOMER"
        form_input_customer.BtnOk.Visible = False
        form_input_customer.Focus()
    End Sub

    Private Sub PROGRESSToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PROGRESSToolStripMenuItem.Click
        form_pembelian_grey.MdiParent = Me
        form_pembelian_grey.Show()
        form_pembelian_grey.Focus()
    End Sub

    Private Sub HutangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HutangToolStripMenuItem.Click
        form_hutang.MdiParent = Me
        form_hutang.Show()
        form_hutang.Focus()
    End Sub

    Private Sub LOGOUTToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LOGOUTToolStripMenuItem.Click
        Dim frmchild As Form
        For Each frmchild In Me.MdiChildren
            frmchild.Close()
        Next
        form_koneksi_database.MdiParent = Me
        form_koneksi_database.Show()
        form_koneksi_database.Focus()
        LOGINToolStripMenuItem.Enabled = True
        LOGOUTToolStripMenuItem.Enabled = False
        MenuStrip1.Visible = False
    End Sub

    Private Sub PiutangToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PiutangToolStripMenuItem.Click
        form_piutang.MdiParent = Me
        form_piutang.Show()
        form_piutang.Focus()
    End Sub

    Private Sub POProsesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles POProsesToolStripMenuItem.Click
        form_po_proses.MdiParent = Me
        form_po_proses.Show()
        form_po_proses.Focus()
    End Sub

    Private Sub POPackingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles POPackingToolStripMenuItem.Click
        form_po_packing.MdiParent = Me
        form_po_packing.Show()
        form_po_packing.Focus()
    End Sub

    Private Sub HasilPackingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HasilPackingToolStripMenuItem.Click
        form_hasil_packing.MdiParent = Me
        form_hasil_packing.Show()
        form_hasil_packing.Focus()
    End Sub

    Private Sub TrackingIDGreyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackingIDGreyToolStripMenuItem.Click
        form_tracking_id_grey.MdiParent = Me
        form_tracking_id_grey.Show()
        form_tracking_id_grey.Focus()
    End Sub

    Private Sub PenggunaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenggunaToolStripMenuItem.Click
        form_pengguna.MdiParent = Me
        form_pengguna.Show()
        form_pengguna.Focus()
        form_pengguna.txt_username.Focus()
    End Sub

    Private Sub PenjualanGreyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenjualanGreyToolStripMenuItem.Click
        form_penjualan_grey.MdiParent = Me
        form_penjualan_grey.Show()
        form_penjualan_grey.Focus()
    End Sub

    Private Sub PenjualanBarangJadiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PenjualanBarangJadiToolStripMenuItem.Click
        form_surat_jalan_penjualan.MdiParent = Me
        form_surat_jalan_penjualan.Show()
        form_surat_jalan_penjualan.Focus()
    End Sub

    Private Sub DataWarnaDanResepToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataWarnaDanResepToolStripMenuItem.Click
        form_input_warna_resep.MdiParent = Me
        form_input_warna_resep.Show()
        form_input_warna_resep.Label1.Text = "NAMA WARNA DAN RESEP"
        form_input_warna_resep.BtnOk.Visible = False
        form_input_warna_resep.Focus()
    End Sub

    Private Sub StokProsesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StokProsesToolStripMenuItem.Click
        form_stok_proses_grey.MdiParent = Me
        form_stok_proses_grey.Show()
        form_stok_proses_grey.Focus()
    End Sub

    Private Sub StokProsesAdminToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StokProsesAdminToolStripMenuItem.Click
        form_stok_proses_global.MdiParent = Me
        form_stok_proses_global.Show()
        form_stok_proses_global.Focus()
    End Sub

    Private Sub StokWIPProsesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StokWIPProsesToolStripMenuItem.Click
        form_stok_wip_proses.MdiParent = Me
        form_stok_wip_proses.Show()
        form_stok_wip_proses.Focus()
    End Sub

    Private Sub StokExProsesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StokExProsesToolStripMenuItem.Click
        form_stok_ex_proses.MdiParent = Me
        form_stok_ex_proses.Show()
        form_stok_ex_proses.Focus()
    End Sub

    Private Sub StokWIPPackingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StokWIPPackingToolStripMenuItem.Click
        form_stok_wip_packing.MdiParent = Me
        form_stok_wip_packing.Show()
        form_stok_wip_packing.Focus()
    End Sub

    Private Sub SuratJalanProsesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SuratJalanProsesToolStripMenuItem.Click
        form_surat_jalan_proses.MdiParent = Me
        form_surat_jalan_proses.Show()
        form_surat_jalan_proses.Focus()
    End Sub

    Private Sub ClaimCelupToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaimCelupToolStripMenuItem1.Click
        form_stok_claim_celup.MdiParent = Me
        form_stok_claim_celup.Show()
        form_stok_claim_celup.Focus()
    End Sub

    Private Sub ClaimJadiToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClaimJadiToolStripMenuItem1.Click
        form_stok_claim_jadi.MdiParent = Me
        form_stok_claim_jadi.Show()
        form_stok_claim_jadi.Focus()
    End Sub

    Private Sub GradeBToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GradeBToolStripMenuItem1.Click
        form_stok_grade_b.MdiParent = Me
        form_stok_grade_b.Show()
        form_stok_grade_b.Focus()
    End Sub

    Private Sub GradeAToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GradeAToolStripMenuItem1.Click
        form_stok_grade_a.MdiParent = Me
        form_stok_grade_a.Show()
        form_stok_grade_a.Focus()
    End Sub
End Class
