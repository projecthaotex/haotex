﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_hasil_packing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_cari = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtp_akhir = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_cari_customer = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_cari_sj_packing = New System.Windows.Forms.TextBox()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_gudang_packing = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.ts_print = New System.Windows.Forms.ToolStripButton()
        Me.ts_perbarui = New System.Windows.Forms.ToolStripButton()
        Me.ts_hapus = New System.Windows.Forms.ToolStripButton()
        Me.ts_ubah = New System.Windows.Forms.ToolStripButton()
        Me.ts_baru = New System.Windows.Forms.ToolStripButton()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_print_hasil = New System.Windows.Forms.ToolStripButton()
        Me.txt_id_grey = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing = New System.Windows.Forms.TextBox()
        Me.txt_baris_packing = New System.Windows.Forms.TextBox()
        Me.txt_gudang = New System.Windows.Forms.TextBox()
        Me.txt_customer = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_warna = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_sudah_proses = New System.Windows.Forms.TextBox()
        Me.txt_jumlah_claim_celup = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_keluar_sj = New System.Windows.Forms.TextBox()
        Me.txt_belum_sj = New System.Windows.Forms.TextBox()
        Me.txt_jumlah_total = New System.Windows.Forms.TextBox()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(214, 69)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(1079, 459)
        Me.dgv1.TabIndex = 23
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1177, 38)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(104, 20)
        Me.dtp_hari_ini.TabIndex = 27
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_cari)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(12, 69)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 459)
        Me.Panel1.TabIndex = 24
        '
        'btn_cari
        '
        Me.btn_cari.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cari.Image = Global.HAOTEX.My.Resources.Resources.search
        Me.btn_cari.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_cari.Location = New System.Drawing.Point(62, 375)
        Me.btn_cari.Name = "btn_cari"
        Me.btn_cari.Size = New System.Drawing.Size(75, 43)
        Me.btn_cari.TabIndex = 20
        Me.btn_cari.Text = "CARI"
        Me.btn_cari.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_cari.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.dtp_akhir)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.dtp_awal)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txt_cari_customer)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txt_cari_sj_packing)
        Me.Panel2.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel2.Controls.Add(Me.txt_cari_gudang_packing)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Location = New System.Drawing.Point(1, 39)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(196, 319)
        Me.Panel2.TabIndex = 19
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(13, 269)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(51, 13)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Customer"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(13, 226)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(55, 13)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Jenis Kain"
        '
        'dtp_akhir
        '
        Me.dtp_akhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_akhir.Location = New System.Drawing.Point(52, 63)
        Me.dtp_akhir.Name = "dtp_akhir"
        Me.dtp_akhir.Size = New System.Drawing.Size(99, 20)
        Me.dtp_akhir.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(13, 183)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Gudang Packing"
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(52, 36)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 140)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Surat Jalan Packing"
        '
        'txt_cari_customer
        '
        Me.txt_cari_customer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_customer.Location = New System.Drawing.Point(13, 286)
        Me.txt_cari_customer.MaxLength = 100
        Me.txt_cari_customer.Name = "txt_cari_customer"
        Me.txt_cari_customer.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_customer.TabIndex = 13
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 112)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cari :"
        '
        'txt_cari_sj_packing
        '
        Me.txt_cari_sj_packing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_sj_packing.Location = New System.Drawing.Point(13, 157)
        Me.txt_cari_sj_packing.Name = "txt_cari_sj_packing"
        Me.txt_cari_sj_packing.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_sj_packing.TabIndex = 1
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(13, 243)
        Me.txt_cari_jenis_kain.MaxLength = 100
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 12
        '
        'txt_cari_gudang_packing
        '
        Me.txt_cari_gudang_packing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_gudang_packing.Location = New System.Drawing.Point(13, 200)
        Me.txt_cari_gudang_packing.MaxLength = 100
        Me.txt_cari_gudang_packing.Name = "txt_cari_gudang_packing"
        Me.txt_cari_gudang_packing.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_gudang_packing.TabIndex = 11
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(13, 67)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(23, 13)
        Me.Label14.TabIndex = 7
        Me.Label14.Text = "s/d"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(13, 40)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(26, 13)
        Me.Label15.TabIndex = 6
        Me.Label15.Text = "Dari"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(13, 13)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(53, 13)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "Tanggal"
        '
        'ts_print
        '
        Me.ts_print.Enabled = False
        Me.ts_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print.Name = "ts_print"
        Me.ts_print.Size = New System.Drawing.Size(52, 22)
        Me.ts_print.Text = "Print"
        '
        'ts_perbarui
        '
        Me.ts_perbarui.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.ts_perbarui.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_perbarui.Name = "ts_perbarui"
        Me.ts_perbarui.Size = New System.Drawing.Size(71, 22)
        Me.ts_perbarui.Text = "Perbarui"
        '
        'ts_hapus
        '
        Me.ts_hapus.Enabled = False
        Me.ts_hapus.Image = Global.HAOTEX.My.Resources.Resources.application_remove
        Me.ts_hapus.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_hapus.Name = "ts_hapus"
        Me.ts_hapus.Size = New System.Drawing.Size(61, 22)
        Me.ts_hapus.Text = "Hapus"
        '
        'ts_ubah
        '
        Me.ts_ubah.Enabled = False
        Me.ts_ubah.Image = Global.HAOTEX.My.Resources.Resources.application_edit
        Me.ts_ubah.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_ubah.Name = "ts_ubah"
        Me.ts_ubah.Size = New System.Drawing.Size(55, 22)
        Me.ts_ubah.Text = "Ubah"
        '
        'ts_baru
        '
        Me.ts_baru.Image = Global.HAOTEX.My.Resources.Resources.application_add
        Me.ts_baru.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_baru.Name = "ts_baru"
        Me.ts_baru.Size = New System.Drawing.Size(51, 22)
        Me.ts_baru.Text = "Baru"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_baru, Me.ts_ubah, Me.ts_hapus, Me.ts_perbarui, Me.ts_print_hasil, Me.ts_print})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1305, 25)
        Me.ToolStrip1.TabIndex = 22
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_print_hasil
        '
        Me.ts_print_hasil.Enabled = False
        Me.ts_print_hasil.Image = Global.HAOTEX.My.Resources.Resources.application
        Me.ts_print_hasil.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print_hasil.Name = "ts_print_hasil"
        Me.ts_print_hasil.Size = New System.Drawing.Size(157, 22)
        Me.ts_print_hasil.Text = "Tampilkan Hasil Packing"
        '
        'txt_id_grey
        '
        Me.txt_id_grey.Location = New System.Drawing.Point(321, 4)
        Me.txt_id_grey.Name = "txt_id_grey"
        Me.txt_id_grey.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_grey.TabIndex = 28
        '
        'txt_sj_packing
        '
        Me.txt_sj_packing.Location = New System.Drawing.Point(440, 4)
        Me.txt_sj_packing.Name = "txt_sj_packing"
        Me.txt_sj_packing.Size = New System.Drawing.Size(100, 20)
        Me.txt_sj_packing.TabIndex = 29
        '
        'txt_baris_packing
        '
        Me.txt_baris_packing.Location = New System.Drawing.Point(559, 4)
        Me.txt_baris_packing.Name = "txt_baris_packing"
        Me.txt_baris_packing.Size = New System.Drawing.Size(100, 20)
        Me.txt_baris_packing.TabIndex = 30
        '
        'txt_gudang
        '
        Me.txt_gudang.Location = New System.Drawing.Point(678, 4)
        Me.txt_gudang.Name = "txt_gudang"
        Me.txt_gudang.Size = New System.Drawing.Size(100, 20)
        Me.txt_gudang.TabIndex = 31
        '
        'txt_customer
        '
        Me.txt_customer.Location = New System.Drawing.Point(797, 4)
        Me.txt_customer.Name = "txt_customer"
        Me.txt_customer.Size = New System.Drawing.Size(100, 20)
        Me.txt_customer.TabIndex = 32
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.Location = New System.Drawing.Point(916, 4)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.Size = New System.Drawing.Size(100, 20)
        Me.txt_jenis_kain.TabIndex = 33
        '
        'txt_warna
        '
        Me.txt_warna.Location = New System.Drawing.Point(1035, 4)
        Me.txt_warna.Name = "txt_warna"
        Me.txt_warna.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna.TabIndex = 34
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label6.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Window
        Me.Label6.Location = New System.Drawing.Point(12, 33)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(1281, 34)
        Me.Label6.TabIndex = 35
        Me.Label6.Text = "SURAT JALAN PACKING"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txt_sudah_proses)
        Me.Panel3.Controls.Add(Me.txt_jumlah_claim_celup)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.txt_keluar_sj)
        Me.Panel3.Controls.Add(Me.txt_belum_sj)
        Me.Panel3.Controls.Add(Me.txt_jumlah_total)
        Me.Panel3.Location = New System.Drawing.Point(12, 530)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1281, 64)
        Me.Panel3.TabIndex = 36
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1141, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Sudah Proses"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(950, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 13)
        Me.Label4.TabIndex = 24
        Me.Label4.Text = "Jumlah Claim Celup"
        '
        'txt_sudah_proses
        '
        Me.txt_sudah_proses.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sudah_proses.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sudah_proses.Location = New System.Drawing.Point(1102, 29)
        Me.txt_sudah_proses.Name = "txt_sudah_proses"
        Me.txt_sudah_proses.ReadOnly = True
        Me.txt_sudah_proses.Size = New System.Drawing.Size(150, 20)
        Me.txt_sudah_proses.TabIndex = 23
        Me.txt_sudah_proses.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jumlah_claim_celup
        '
        Me.txt_jumlah_claim_celup.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jumlah_claim_celup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_claim_celup.Location = New System.Drawing.Point(924, 29)
        Me.txt_jumlah_claim_celup.Name = "txt_jumlah_claim_celup"
        Me.txt_jumlah_claim_celup.ReadOnly = True
        Me.txt_jumlah_claim_celup.Size = New System.Drawing.Size(150, 20)
        Me.txt_jumlah_claim_celup.TabIndex = 22
        Me.txt_jumlah_claim_celup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(664, 13)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(102, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Keluar SJ Penjualan"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(454, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(101, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "Belum SJ Penjualan"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(260, 13)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Jumlah Total"
        '
        'txt_keluar_sj
        '
        Me.txt_keluar_sj.BackColor = System.Drawing.SystemColors.Window
        Me.txt_keluar_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keluar_sj.Location = New System.Drawing.Point(640, 29)
        Me.txt_keluar_sj.Name = "txt_keluar_sj"
        Me.txt_keluar_sj.ReadOnly = True
        Me.txt_keluar_sj.Size = New System.Drawing.Size(150, 20)
        Me.txt_keluar_sj.TabIndex = 17
        Me.txt_keluar_sj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_belum_sj
        '
        Me.txt_belum_sj.BackColor = System.Drawing.SystemColors.Window
        Me.txt_belum_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_belum_sj.Location = New System.Drawing.Point(429, 29)
        Me.txt_belum_sj.Name = "txt_belum_sj"
        Me.txt_belum_sj.ReadOnly = True
        Me.txt_belum_sj.Size = New System.Drawing.Size(150, 20)
        Me.txt_belum_sj.TabIndex = 16
        Me.txt_belum_sj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jumlah_total
        '
        Me.txt_jumlah_total.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jumlah_total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_total.Location = New System.Drawing.Point(218, 29)
        Me.txt_jumlah_total.Name = "txt_jumlah_total"
        Me.txt_jumlah_total.ReadOnly = True
        Me.txt_jumlah_total.Size = New System.Drawing.Size(150, 20)
        Me.txt_jumlah_total.TabIndex = 15
        Me.txt_jumlah_total.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'form_hasil_packing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 606)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_warna)
        Me.Controls.Add(Me.txt_jenis_kain)
        Me.Controls.Add(Me.txt_customer)
        Me.Controls.Add(Me.txt_gudang)
        Me.Controls.Add(Me.txt_baris_packing)
        Me.Controls.Add(Me.txt_sj_packing)
        Me.Controls.Add(Me.txt_id_grey)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_hasil_packing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dtp_akhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_cari_sj_packing As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ts_print As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_perbarui As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_hapus As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_ubah As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_baru As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents txt_id_grey As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing As System.Windows.Forms.TextBox
    Friend WithEvents txt_baris_packing As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_keluar_sj As System.Windows.Forms.TextBox
    Friend WithEvents txt_belum_sj As System.Windows.Forms.TextBox
    Friend WithEvents txt_jumlah_total As System.Windows.Forms.TextBox
    Friend WithEvents btn_cari As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_cari_customer As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_gudang_packing As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_sudah_proses As System.Windows.Forms.TextBox
    Friend WithEvents txt_jumlah_claim_celup As System.Windows.Forms.TextBox
    Friend WithEvents ts_print_hasil As System.Windows.Forms.ToolStripButton
End Class
