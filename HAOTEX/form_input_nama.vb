﻿Imports MySql.Data.MySqlClient

Public Class form_input_nama

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Me.Close()
    End Sub

    Private Sub simpan_tbjenisgrey()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Grey from tbjenisgrey WHERE Nama_Grey ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("JENIS KAIN Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "INSERT INTO tbjenisgrey (Nama_Grey,Tambah1,Tambah2,Tambah3) VALUES (@1,@2,@3,@4)"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                txt_nama.Text = ""
                                txt_nama.Focus()
                                If form_input_jenis_kain.TxtCari.Text = "" Then
                                    form_input_jenis_kain.btn_perbarui.PerformClick()
                                Else
                                    form_input_jenis_kain.TxtCari.Text = ""
                                End If
                                MsgBox("JENIS KAIN Baru Berhasil Disimpan")
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub update_tbjenisgrey()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Grey from tbjenisgrey WHERE Nama_Grey ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("JENIS KAIN Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbjenisgrey SET Nama_Grey=@1,Tambah1=@2,Tambah2=@3,Tambah3=@4 WHERE Nama_Grey='" & form_input_jenis_kain.TxtCari.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                form_input_jenis_kain.TxtCari.Text = ""
                                MsgBox("JENIS KAIN Berhasil Diubah")
                                form_input_jenis_kain.Focus()
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub simpan_tbsupplier()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Supplier from tbsupplier WHERE Nama_Supplier ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("NAMA SUPPLIER Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "INSERT INTO tbsupplier (Nama_Supplier,Tambah1,Tambah2,Tambah3) VALUES (@1,@2,@3,@4)"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                txt_nama.Text = ""
                                txt_nama.Focus()
                                If form_input_supplier.TxtCari.Text = "" Then
                                    form_input_supplier.btn_perbarui.PerformClick()
                                Else
                                    form_input_supplier.TxtCari.Text = ""
                                End If
                                MsgBox("NAMA SUPPLIER Baru Berhasil Disimpan")
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub update_tbsupplier()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Supplier from tbsupplier WHERE Nama_Supplier ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("NAMA SUPPLIER Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbsupplier SET Nama_Supplier=@1,Tambah1=@2,Tambah2=@3,Tambah3=@4 WHERE Nama_Supplier='" & form_input_supplier.TxtCari.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                form_input_supplier.TxtCari.Text = ""
                                MsgBox("NAMA SUPPLIER Berhasil Diubah")
                                form_input_supplier.Focus()
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub simpan_tbgudang()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Gudang from tbgudang WHERE Nama_Gudang ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("NAMA GUDANG Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "INSERT INTO tbgudang (Nama_Gudang,Tambah1,Tambah2,Tambah3) VALUES (@1,@2,@3,@4)"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                txt_nama.Text = ""
                                txt_nama.Focus()
                                If form_input_gudang.TxtCari.Text = "" Then
                                    form_input_gudang.btn_perbarui.PerformClick()
                                Else
                                    form_input_gudang.TxtCari.Text = ""
                                End If
                                MsgBox("NAMA GUDANG Baru Berhasil Disimpan")
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub update_tbgudang()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Gudang from tbgudang WHERE Nama_Gudang ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("NAMA GUDANG Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbgudang SET Nama_Gudang=@1,Tambah1=@2,Tambah2=@3,Tambah3=@4 WHERE Nama_Gudang='" & form_input_gudang.TxtCari.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                form_input_gudang.TxtCari.Text = ""
                                MsgBox("NAMA GUDANG Berhasil Diubah")
                                form_input_gudang.Focus()
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub simpan_tbcustomer()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Customer from tbcustomer WHERE Nama_Customer ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("NAMA CUSTOMER Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "INSERT INTO tbcustomer (Nama_Customer,Tambah1,Tambah2,Tambah3) VALUES (@1,@2,@3,@4)"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                txt_nama.Text = ""
                                txt_nama.Focus()
                                If form_input_customer.TxtCari.Text = "" Then
                                    form_input_customer.btn_perbarui.PerformClick()
                                Else
                                    form_input_customer.TxtCari.Text = ""
                                End If
                                MsgBox("NAMA CUSTOMER Baru Berhasil Disimpan")
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub update_tbcustomer()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx = "SELECT Nama_Customer from tbcustomer WHERE Nama_Customer ='" & txt_nama.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        MsgBox("NAMA CUSTOMER Sudah Ada")
                        txt_nama.Focus()
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbcustomer SET Nama_Customer=@1,Tambah1=@2,Tambah2=@3,Tambah3=@4 WHERE Nama_Customer='" & form_input_customer.TxtCari.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_nama.Text))
                                    .Parameters.AddWithValue("@2", (""))
                                    .Parameters.AddWithValue("@3", (""))
                                    .Parameters.AddWithValue("@4", (""))
                                    .ExecuteNonQuery()
                                End With
                                form_input_customer.TxtCari.Text = ""
                                MsgBox("NAMA CUSTOMER Berhasil Diubah")
                                form_input_gudang.Focus()
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub btn_simpan_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_baru.Click
        Try
            If Label1.Text = "Input Jenis Kain Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("Jenis Kain Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbjenisgrey()
                End If

            ElseIf Label1.Text = "Input Supplier Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("NAMA SUPPLIER Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbsupplier()
                End If

            ElseIf Label1.Text = "Input Gudang Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("NAMA GUDANG Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbgudang()
                End If

            ElseIf Label1.Text = "Input Customer Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("NAMA CUSTOMER Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbcustomer()
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_simpan_tutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_tutup.Click
        Try
            If Label1.Text = "Input Jenis Kain Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("Jenis Kain Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbjenisgrey()
                    Me.Close()
                End If

            ElseIf Label1.Text = "Ubah Jenis Kain" Then
                If txt_nama.Text = "" Then
                    MsgBox("Jenis Kain Belum Dipilih")
                    txt_nama.Focus()
                Else
                    Call update_tbjenisgrey()
                    Me.Close()
                End If

            ElseIf Label1.Text = "Input Supplier Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("NAMA SUPPLIER Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbsupplier()
                    Me.Close()
                End If

            ElseIf Label1.Text = "Ubah Nama Supplier" Then
                If txt_nama.Text = "" Then
                    MsgBox("Nama Supplier Belum Dipilih")
                    txt_nama.Focus()
                Else
                    Call update_tbsupplier()
                    Me.Close()
                End If

            ElseIf Label1.Text = "Input Gudang Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("NAMA GUDANG Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbgudang()
                    Me.Close()
                End If

            ElseIf Label1.Text = "Ubah Nama Gudang" Then
                If txt_nama.Text = "" Then
                    MsgBox("Nama Gudang Belum Dipilih")
                    txt_nama.Focus()
                Else
                    Call update_tbgudang()
                    Me.Close()
                End If

            ElseIf Label1.Text = "Input Customer Baru" Then
                If txt_nama.Text = "" Then
                    MsgBox("NAMA CUSTOMER Belum Diisi")
                    txt_nama.Focus()
                Else
                    Call simpan_tbcustomer()
                    Me.Close()
                End If

            ElseIf Label1.Text = "Ubah Nama Customer" Then
                If txt_nama.Text = "" Then
                    MsgBox("Nama Customer Belum Dipilih")
                    txt_nama.Focus()
                Else
                    Call update_tbcustomer()
                    Me.Close()
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class