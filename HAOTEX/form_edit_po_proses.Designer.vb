﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_edit_po_proses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_id_grey = New System.Windows.Forms.TextBox()
        Me.txt_sisa_qty = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_resep = New System.Windows.Forms.TextBox()
        Me.dtp_tanggal = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_surat_jalan = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_warna = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cb_satuan = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_qty = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_note = New System.Windows.Forms.TextBox()
        Me.txt_no_po = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_tarik_lebar = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_hand_fill = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_gudang = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_keterangan = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_batal = New System.Windows.Forms.Button()
        Me.btn_simpan_tutup = New System.Windows.Forms.Button()
        Me.dtp_tanggal_1 = New System.Windows.Forms.DateTimePicker()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txt_asal_qty = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_asal_satuan = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_harga = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses = New System.Windows.Forms.TextBox()
        Me.txt_id_beli = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_id_grey
        '
        Me.txt_id_grey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey.Location = New System.Drawing.Point(14, 47)
        Me.txt_id_grey.Name = "txt_id_grey"
        Me.txt_id_grey.ReadOnly = True
        Me.txt_id_grey.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_grey.TabIndex = 1
        '
        'txt_sisa_qty
        '
        Me.txt_sisa_qty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_qty.Location = New System.Drawing.Point(92, 158)
        Me.txt_sisa_qty.Name = "txt_sisa_qty"
        Me.txt_sisa_qty.ReadOnly = True
        Me.txt_sisa_qty.Size = New System.Drawing.Size(110, 20)
        Me.txt_sisa_qty.TabIndex = 76
        Me.txt_sisa_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(13, 249)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(38, 13)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Resep"
        '
        'txt_resep
        '
        Me.txt_resep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep.Location = New System.Drawing.Point(92, 245)
        Me.txt_resep.Name = "txt_resep"
        Me.txt_resep.Size = New System.Drawing.Size(165, 20)
        Me.txt_resep.TabIndex = 40
        '
        'dtp_tanggal
        '
        Me.dtp_tanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal.Location = New System.Drawing.Point(92, 13)
        Me.dtp_tanggal.Name = "dtp_tanggal"
        Me.dtp_tanggal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_tanggal.TabIndex = 16
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 336)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Keterangan"
        '
        'txt_surat_jalan
        '
        Me.txt_surat_jalan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_surat_jalan.Location = New System.Drawing.Point(92, 100)
        Me.txt_surat_jalan.Name = "txt_surat_jalan"
        Me.txt_surat_jalan.ReadOnly = True
        Me.txt_surat_jalan.Size = New System.Drawing.Size(166, 20)
        Me.txt_surat_jalan.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 46)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Gudang"
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain.Location = New System.Drawing.Point(92, 129)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.ReadOnly = True
        Me.txt_jenis_kain.Size = New System.Drawing.Size(166, 20)
        Me.txt_jenis_kain.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(13, 307)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 13)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Hand Fill"
        '
        'txt_warna
        '
        Me.txt_warna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna.Location = New System.Drawing.Point(92, 216)
        Me.txt_warna.Name = "txt_warna"
        Me.txt_warna.Size = New System.Drawing.Size(165, 20)
        Me.txt_warna.TabIndex = 15
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.cb_satuan)
        Me.Panel1.Controls.Add(Me.txt_sisa_qty)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.txt_qty)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.txt_note)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txt_resep)
        Me.Panel1.Controls.Add(Me.dtp_tanggal)
        Me.Panel1.Controls.Add(Me.txt_no_po)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txt_surat_jalan)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txt_jenis_kain)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txt_warna)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txt_tarik_lebar)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txt_hand_fill)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txt_gudang)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_keterangan)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(-3, 34)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(300, 425)
        Me.Panel1.TabIndex = 79
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(13, 162)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(29, 13)
        Me.Label21.TabIndex = 502
        Me.Label21.Text = "Stok"
        '
        'cb_satuan
        '
        Me.cb_satuan.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan.FormattingEnabled = True
        Me.cb_satuan.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan.Location = New System.Drawing.Point(208, 173)
        Me.cb_satuan.Name = "cb_satuan"
        Me.cb_satuan.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan.TabIndex = 501
        Me.cb_satuan.Text = "Meter"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(13, 191)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(29, 13)
        Me.Label11.TabIndex = 104
        Me.Label11.Text = "QTY"
        '
        'txt_qty
        '
        Me.txt_qty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty.Location = New System.Drawing.Point(92, 187)
        Me.txt_qty.Name = "txt_qty"
        Me.txt_qty.Size = New System.Drawing.Size(110, 20)
        Me.txt_qty.TabIndex = 103
        Me.txt_qty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(13, 365)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(30, 13)
        Me.Label14.TabIndex = 102
        Me.Label14.Text = "Note"
        '
        'txt_note
        '
        Me.txt_note.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note.Location = New System.Drawing.Point(92, 361)
        Me.txt_note.Multiline = True
        Me.txt_note.Name = "txt_note"
        Me.txt_note.Size = New System.Drawing.Size(166, 45)
        Me.txt_note.TabIndex = 101
        '
        'txt_no_po
        '
        Me.txt_no_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po.Location = New System.Drawing.Point(92, 71)
        Me.txt_no_po.Name = "txt_no_po"
        Me.txt_no_po.ReadOnly = True
        Me.txt_no_po.Size = New System.Drawing.Size(166, 20)
        Me.txt_no_po.TabIndex = 100
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(13, 278)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(20, 13)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "TL"
        '
        'txt_tarik_lebar
        '
        Me.txt_tarik_lebar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar.Location = New System.Drawing.Point(92, 274)
        Me.txt_tarik_lebar.Name = "txt_tarik_lebar"
        Me.txt_tarik_lebar.Size = New System.Drawing.Size(165, 20)
        Me.txt_tarik_lebar.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(13, 220)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Warna"
        '
        'txt_hand_fill
        '
        Me.txt_hand_fill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill.Location = New System.Drawing.Point(92, 303)
        Me.txt_hand_fill.Name = "txt_hand_fill"
        Me.txt_hand_fill.Size = New System.Drawing.Size(166, 20)
        Me.txt_hand_fill.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(13, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Jenis Kain"
        '
        'txt_gudang
        '
        Me.txt_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang.Location = New System.Drawing.Point(92, 42)
        Me.txt_gudang.Name = "txt_gudang"
        Me.txt_gudang.ReadOnly = True
        Me.txt_gudang.Size = New System.Drawing.Size(166, 20)
        Me.txt_gudang.TabIndex = 19
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Surat Jalan"
        '
        'txt_keterangan
        '
        Me.txt_keterangan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan.Location = New System.Drawing.Point(92, 332)
        Me.txt_keterangan.Name = "txt_keterangan"
        Me.txt_keterangan.Size = New System.Drawing.Size(165, 20)
        Me.txt_keterangan.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "No PO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Tanggal"
        '
        'btn_batal
        '
        Me.btn_batal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Image = Global.HAOTEX.My.Resources.Resources.action_delete
        Me.btn_batal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal.Location = New System.Drawing.Point(189, 475)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(64, 23)
        Me.btn_batal.TabIndex = 81
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal.UseMnemonic = False
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan_tutup
        '
        Me.btn_simpan_tutup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_tutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_tutup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_tutup.Image = Global.HAOTEX.My.Resources.Resources.save1
        Me.btn_simpan_tutup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_tutup.Location = New System.Drawing.Point(41, 475)
        Me.btn_simpan_tutup.Name = "btn_simpan_tutup"
        Me.btn_simpan_tutup.Size = New System.Drawing.Size(127, 23)
        Me.btn_simpan_tutup.TabIndex = 80
        Me.btn_simpan_tutup.Text = "Simpan & Tutup"
        Me.btn_simpan_tutup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_tutup.UseMnemonic = False
        Me.btn_simpan_tutup.UseVisualStyleBackColor = True
        '
        'dtp_tanggal_1
        '
        Me.dtp_tanggal_1.CustomFormat = "dd/MM/yyyy"
        Me.dtp_tanggal_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_1.Location = New System.Drawing.Point(14, 14)
        Me.dtp_tanggal_1.Name = "dtp_tanggal_1"
        Me.dtp_tanggal_1.Size = New System.Drawing.Size(100, 20)
        Me.dtp_tanggal_1.TabIndex = 87
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txt_asal_qty)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.txt_asal_satuan)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.txt_harga)
        Me.Panel2.Controls.Add(Me.txt_id_po_proses)
        Me.Panel2.Controls.Add(Me.txt_id_beli)
        Me.Panel2.Controls.Add(Me.dtp_tanggal_1)
        Me.Panel2.Controls.Add(Me.txt_id_grey)
        Me.Panel2.Location = New System.Drawing.Point(164, 211)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(128, 288)
        Me.Panel2.TabIndex = 88
        '
        'txt_asal_qty
        '
        Me.txt_asal_qty.Location = New System.Drawing.Point(14, 179)
        Me.txt_asal_qty.Name = "txt_asal_qty"
        Me.txt_asal_qty.Size = New System.Drawing.Size(100, 20)
        Me.txt_asal_qty.TabIndex = 104
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(14, 166)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(46, 13)
        Me.Label13.TabIndex = 103
        Me.Label13.Text = "Asal Qty"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(14, 133)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(64, 13)
        Me.Label12.TabIndex = 102
        Me.Label12.Text = "Asal Satuan"
        '
        'txt_asal_satuan
        '
        Me.txt_asal_satuan.Location = New System.Drawing.Point(14, 146)
        Me.txt_asal_satuan.Name = "txt_asal_satuan"
        Me.txt_asal_satuan.Size = New System.Drawing.Size(100, 20)
        Me.txt_asal_satuan.TabIndex = 101
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(14, 199)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(36, 13)
        Me.Label20.TabIndex = 97
        Me.Label20.Text = "Harga"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(14, 100)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(67, 13)
        Me.Label18.TabIndex = 95
        Me.Label18.Text = "Id Po Proses"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(14, 67)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(36, 13)
        Me.Label16.TabIndex = 94
        Me.Label16.Text = "Id Beli"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(14, 34)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(41, 13)
        Me.Label15.TabIndex = 93
        Me.Label15.Text = "Id Grey"
        '
        'txt_harga
        '
        Me.txt_harga.Location = New System.Drawing.Point(14, 212)
        Me.txt_harga.Name = "txt_harga"
        Me.txt_harga.Size = New System.Drawing.Size(100, 20)
        Me.txt_harga.TabIndex = 90
        '
        'txt_id_po_proses
        '
        Me.txt_id_po_proses.Location = New System.Drawing.Point(14, 113)
        Me.txt_id_po_proses.Name = "txt_id_po_proses"
        Me.txt_id_po_proses.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_po_proses.TabIndex = 88
        '
        'txt_id_beli
        '
        Me.txt_id_beli.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli.Location = New System.Drawing.Point(14, 80)
        Me.txt_id_beli.Name = "txt_id_beli"
        Me.txt_id_beli.ReadOnly = True
        Me.txt_id_beli.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_beli.TabIndex = 92
        Me.txt_id_beli.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.CausesValidation = False
        Me.Label1.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Window
        Me.Label1.Location = New System.Drawing.Point(-3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(300, 34)
        Me.Label1.TabIndex = 89
        Me.Label1.Text = "Label1"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'form_edit_po_proses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(294, 511)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btn_batal)
        Me.Controls.Add(Me.btn_simpan_tutup)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_edit_po_proses"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt_id_grey As System.Windows.Forms.TextBox
    Friend WithEvents txt_sisa_qty As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txt_resep As System.Windows.Forms.TextBox
    Friend WithEvents dtp_tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_surat_jalan As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents btn_simpan_tutup As System.Windows.Forms.Button
    Friend WithEvents txt_warna As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_tarik_lebar As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_hand_fill As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_gudang As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_qty As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_note As System.Windows.Forms.TextBox
    Friend WithEvents dtp_tanggal_1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_harga As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli As System.Windows.Forms.TextBox
    Friend WithEvents cb_satuan As System.Windows.Forms.ComboBox
    Friend WithEvents txt_no_po As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_qty As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_satuan As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
