﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_penjualan_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_baru = New System.Windows.Forms.ToolStripButton()
        Me.ts_ubah = New System.Windows.Forms.ToolStripButton()
        Me.ts_hapus = New System.Windows.Forms.ToolStripButton()
        Me.ts_perbarui = New System.Windows.Forms.ToolStripButton()
        Me.ts_print = New System.Windows.Forms.ToolStripButton()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtp_akhir = New System.Windows.Forms.DateTimePicker()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_customer = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(214, 62)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(1079, 532)
        Me.dgv1.TabIndex = 0
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_baru, Me.ts_ubah, Me.ts_hapus, Me.ts_perbarui, Me.ts_print})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1305, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_baru
        '
        Me.ts_baru.Image = Global.HAOTEX.My.Resources.Resources.application_add
        Me.ts_baru.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_baru.Name = "ts_baru"
        Me.ts_baru.Size = New System.Drawing.Size(51, 22)
        Me.ts_baru.Text = "Baru"
        '
        'ts_ubah
        '
        Me.ts_ubah.Image = Global.HAOTEX.My.Resources.Resources.application_edit
        Me.ts_ubah.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_ubah.Name = "ts_ubah"
        Me.ts_ubah.Size = New System.Drawing.Size(55, 22)
        Me.ts_ubah.Text = "Ubah"
        '
        'ts_hapus
        '
        Me.ts_hapus.Image = Global.HAOTEX.My.Resources.Resources.application_remove
        Me.ts_hapus.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_hapus.Name = "ts_hapus"
        Me.ts_hapus.Size = New System.Drawing.Size(61, 22)
        Me.ts_hapus.Text = "Hapus"
        '
        'ts_perbarui
        '
        Me.ts_perbarui.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.ts_perbarui.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_perbarui.Name = "ts_perbarui"
        Me.ts_perbarui.Size = New System.Drawing.Size(71, 22)
        Me.ts_perbarui.Text = "Perbarui"
        '
        'ts_print
        '
        Me.ts_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print.Name = "ts_print"
        Me.ts_print.Size = New System.Drawing.Size(52, 22)
        Me.ts_print.Text = "Print"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(12, 36)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(200, 24)
        Me.Panel2.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Filter"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.dtp_akhir)
        Me.Panel1.Controls.Add(Me.dtp_awal)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_cari_customer)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 62)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 532)
        Me.Panel1.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 77)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "s/d"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Dari"
        '
        'dtp_akhir
        '
        Me.dtp_akhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_akhir.Location = New System.Drawing.Point(43, 73)
        Me.dtp_akhir.Name = "dtp_akhir"
        Me.dtp_akhir.Size = New System.Drawing.Size(99, 20)
        Me.dtp_akhir.TabIndex = 5
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(43, 46)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tanggal"
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(14, 178)
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 2
        Me.txt_cari_jenis_kain.Text = "< Jenis Kain >"
        '
        'txt_cari_customer
        '
        Me.txt_cari_customer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_customer.Location = New System.Drawing.Point(14, 151)
        Me.txt_cari_customer.Name = "txt_cari_customer"
        Me.txt_cari_customer.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_customer.TabIndex = 1
        Me.txt_cari_customer.Text = "< Customer >"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 131)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Cari :"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(214, 36)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(1079, 24)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "PENJUALAN GREY"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1309, 5)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(104, 20)
        Me.dtp_hari_ini.TabIndex = 11
        '
        'form_penjualan_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 606)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.dgv1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_penjualan_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM PENJUALAN GREY"
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_baru As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_ubah As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_hapus As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_perbarui As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_print As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtp_akhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_customer As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
End Class
