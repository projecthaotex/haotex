﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_surat_jalan_penjualan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_input_surat_jalan_penjualan))
        Me.txt_warna_1 = New System.Windows.Forms.TextBox()
        Me.cb_stok_1 = New System.Windows.Forms.ComboBox()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.txt_sj_packing_1 = New System.Windows.Forms.TextBox()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.txt_customer = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_1 = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.dtp_jatuh_tempo = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_sj_penjualan = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_id_claim_jadi_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_claim_jadi_1 = New System.Windows.Forms.TextBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_id_grade_b_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_b_1 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_id_grade_a_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_grade_a_1 = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_id_penjualan_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_penjualan_1 = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txt_id_grey_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_1 = New System.Windows.Forms.TextBox()
        Me.txt_gudang_1 = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.btn_batal_10 = New System.Windows.Forms.Button()
        Me.btn_batal_9 = New System.Windows.Forms.Button()
        Me.btn_batal_8 = New System.Windows.Forms.Button()
        Me.btn_batal_7 = New System.Windows.Forms.Button()
        Me.btn_batal_6 = New System.Windows.Forms.Button()
        Me.btn_batal_5 = New System.Windows.Forms.Button()
        Me.btn_batal_4 = New System.Windows.Forms.Button()
        Me.btn_batal_3 = New System.Windows.Forms.Button()
        Me.btn_batal_2 = New System.Windows.Forms.Button()
        Me.btn_batal_1 = New System.Windows.Forms.Button()
        Me.btn_hapus_10 = New System.Windows.Forms.Button()
        Me.btn_selesai_10 = New System.Windows.Forms.Button()
        Me.btn_hapus_9 = New System.Windows.Forms.Button()
        Me.btn_selesai_9 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_9 = New System.Windows.Forms.Button()
        Me.btn_hapus_8 = New System.Windows.Forms.Button()
        Me.btn_selesai_8 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_8 = New System.Windows.Forms.Button()
        Me.btn_hapus_7 = New System.Windows.Forms.Button()
        Me.btn_selesai_7 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_7 = New System.Windows.Forms.Button()
        Me.btn_hapus_6 = New System.Windows.Forms.Button()
        Me.btn_selesai_6 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_6 = New System.Windows.Forms.Button()
        Me.btn_hapus_5 = New System.Windows.Forms.Button()
        Me.btn_selesai_5 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_5 = New System.Windows.Forms.Button()
        Me.btn_hapus_4 = New System.Windows.Forms.Button()
        Me.btn_selesai_4 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_4 = New System.Windows.Forms.Button()
        Me.btn_hapus_3 = New System.Windows.Forms.Button()
        Me.btn_selesai_3 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_3 = New System.Windows.Forms.Button()
        Me.btn_hapus_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_2 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_1 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_1 = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Panel_1 = New System.Windows.Forms.Panel()
        Me.txt_yards_1 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_1 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_1 = New System.Windows.Forms.TextBox()
        Me.txt_harga_1 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_1 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_1 = New System.Windows.Forms.Label()
        Me.Panel_2 = New System.Windows.Forms.Panel()
        Me.txt_gudang_2 = New System.Windows.Forms.TextBox()
        Me.txt_yards_2 = New System.Windows.Forms.TextBox()
        Me.txt_warna_2 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_2 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_2 = New System.Windows.Forms.TextBox()
        Me.txt_harga_2 = New System.Windows.Forms.TextBox()
        Me.cb_stok_2 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_2 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_2 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_2 = New System.Windows.Forms.TextBox()
        Me.Panel_3 = New System.Windows.Forms.Panel()
        Me.txt_gudang_3 = New System.Windows.Forms.TextBox()
        Me.txt_yards_3 = New System.Windows.Forms.TextBox()
        Me.txt_warna_3 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_3 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_3 = New System.Windows.Forms.TextBox()
        Me.txt_harga_3 = New System.Windows.Forms.TextBox()
        Me.cb_stok_3 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_3 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_3 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_3 = New System.Windows.Forms.TextBox()
        Me.Panel_4 = New System.Windows.Forms.Panel()
        Me.txt_gudang_4 = New System.Windows.Forms.TextBox()
        Me.txt_yards_4 = New System.Windows.Forms.TextBox()
        Me.txt_warna_4 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_4 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_4 = New System.Windows.Forms.TextBox()
        Me.txt_harga_4 = New System.Windows.Forms.TextBox()
        Me.cb_stok_4 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_4 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_4 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_4 = New System.Windows.Forms.TextBox()
        Me.Panel_5 = New System.Windows.Forms.Panel()
        Me.txt_gudang_5 = New System.Windows.Forms.TextBox()
        Me.txt_yards_5 = New System.Windows.Forms.TextBox()
        Me.txt_warna_5 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_5 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_5 = New System.Windows.Forms.TextBox()
        Me.txt_harga_5 = New System.Windows.Forms.TextBox()
        Me.cb_stok_5 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_5 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_5 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_5 = New System.Windows.Forms.TextBox()
        Me.Panel_6 = New System.Windows.Forms.Panel()
        Me.txt_gudang_6 = New System.Windows.Forms.TextBox()
        Me.txt_yards_6 = New System.Windows.Forms.TextBox()
        Me.txt_warna_6 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_6 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_6 = New System.Windows.Forms.TextBox()
        Me.txt_harga_6 = New System.Windows.Forms.TextBox()
        Me.cb_stok_6 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_6 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_6 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_6 = New System.Windows.Forms.TextBox()
        Me.Panel_7 = New System.Windows.Forms.Panel()
        Me.txt_gudang_7 = New System.Windows.Forms.TextBox()
        Me.txt_yards_7 = New System.Windows.Forms.TextBox()
        Me.txt_warna_7 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_7 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_7 = New System.Windows.Forms.TextBox()
        Me.txt_harga_7 = New System.Windows.Forms.TextBox()
        Me.cb_stok_7 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_7 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_7 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_7 = New System.Windows.Forms.TextBox()
        Me.Panel_8 = New System.Windows.Forms.Panel()
        Me.txt_gudang_8 = New System.Windows.Forms.TextBox()
        Me.txt_yards_8 = New System.Windows.Forms.TextBox()
        Me.txt_warna_8 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_8 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_8 = New System.Windows.Forms.TextBox()
        Me.txt_harga_8 = New System.Windows.Forms.TextBox()
        Me.cb_stok_8 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_8 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_8 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_8 = New System.Windows.Forms.TextBox()
        Me.Panel_9 = New System.Windows.Forms.Panel()
        Me.txt_gudang_9 = New System.Windows.Forms.TextBox()
        Me.txt_yards_9 = New System.Windows.Forms.TextBox()
        Me.txt_warna_9 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_9 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_9 = New System.Windows.Forms.TextBox()
        Me.txt_harga_9 = New System.Windows.Forms.TextBox()
        Me.cb_stok_9 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_9 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_9 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_9 = New System.Windows.Forms.TextBox()
        Me.Panel_10 = New System.Windows.Forms.Panel()
        Me.txt_gudang_10 = New System.Windows.Forms.TextBox()
        Me.txt_yards_10 = New System.Windows.Forms.TextBox()
        Me.txt_warna_10 = New System.Windows.Forms.TextBox()
        Me.txt_total_harga_10 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_10 = New System.Windows.Forms.TextBox()
        Me.txt_harga_10 = New System.Windows.Forms.TextBox()
        Me.cb_stok_10 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_10 = New System.Windows.Forms.TextBox()
        Me.txt_sj_packing_10 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain_10 = New System.Windows.Forms.TextBox()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_id_piutang_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_piutang_1 = New System.Windows.Forms.TextBox()
        Me.Panel3.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel_1.SuspendLayout()
        Me.Panel_2.SuspendLayout()
        Me.Panel_3.SuspendLayout()
        Me.Panel_4.SuspendLayout()
        Me.Panel_5.SuspendLayout()
        Me.Panel_6.SuspendLayout()
        Me.Panel_7.SuspendLayout()
        Me.Panel_8.SuspendLayout()
        Me.Panel_9.SuspendLayout()
        Me.Panel_10.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_warna_1
        '
        Me.txt_warna_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_1.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_1.Name = "txt_warna_1"
        Me.txt_warna_1.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_1.TabIndex = 45
        '
        'cb_stok_1
        '
        Me.cb_stok_1.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_1.FormattingEnabled = True
        Me.cb_stok_1.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_1.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_1.Name = "cb_stok_1"
        Me.cb_stok_1.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_1.TabIndex = 43
        Me.cb_stok_1.Text = "-- Pilih Stok --"
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(149, 5)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(97, 20)
        Me.dtp_awal.TabIndex = 3
        '
        'txt_sj_packing_1
        '
        Me.txt_sj_packing_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_1.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_1.Name = "txt_sj_packing_1"
        Me.txt_sj_packing_1.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_1.TabIndex = 1
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1216, 18)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(108, 20)
        Me.dtp_hari_ini.TabIndex = 87
        '
        'txt_customer
        '
        Me.txt_customer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer.Location = New System.Drawing.Point(149, 32)
        Me.txt_customer.Name = "txt_customer"
        Me.txt_customer.Size = New System.Drawing.Size(151, 20)
        Me.txt_customer.TabIndex = 80
        '
        'txt_jenis_kain_1
        '
        Me.txt_jenis_kain_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_1.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_1.Name = "txt_jenis_kain_1"
        Me.txt_jenis_kain_1.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_1.TabIndex = 19
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.Control
        Me.Panel3.Controls.Add(Me.Panel10)
        Me.Panel3.Controls.Add(Me.Label22)
        Me.Panel3.Location = New System.Drawing.Point(-3, 9)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1342, 153)
        Me.Panel3.TabIndex = 89
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel10.Controls.Add(Me.ComboBox1)
        Me.Panel10.Controls.Add(Me.dtp_jatuh_tempo)
        Me.Panel10.Controls.Add(Me.Label12)
        Me.Panel10.Controls.Add(Me.txt_sj_penjualan)
        Me.Panel10.Controls.Add(Me.Label19)
        Me.Panel10.Controls.Add(Me.Label20)
        Me.Panel10.Controls.Add(Me.Label21)
        Me.Panel10.Controls.Add(Me.dtp_awal)
        Me.Panel10.Controls.Add(Me.txt_customer)
        Me.Panel10.Location = New System.Drawing.Point(521, 27)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(352, 116)
        Me.Panel10.TabIndex = 81
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"15", "30", "45", "60", "75", "90", "105", "120", "135", "150", "165", "180", "195", "210", "225", "240", "255", "270", "285", "300", "315", "330", "345", "360", "375"})
        Me.ComboBox1.Location = New System.Drawing.Point(149, 86)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(59, 21)
        Me.ComboBox1.TabIndex = 83
        Me.ComboBox1.Text = "15"
        '
        'dtp_jatuh_tempo
        '
        Me.dtp_jatuh_tempo.Enabled = False
        Me.dtp_jatuh_tempo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo.Location = New System.Drawing.Point(214, 86)
        Me.dtp_jatuh_tempo.Name = "dtp_jatuh_tempo"
        Me.dtp_jatuh_tempo.Size = New System.Drawing.Size(99, 20)
        Me.dtp_jatuh_tempo.TabIndex = 81
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(37, 88)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(98, 16)
        Me.Label12.TabIndex = 82
        Me.Label12.Text = "Jatuh Tempo"
        '
        'txt_sj_penjualan
        '
        Me.txt_sj_penjualan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_penjualan.Location = New System.Drawing.Point(149, 59)
        Me.txt_sj_penjualan.Name = "txt_sj_penjualan"
        Me.txt_sj_penjualan.Size = New System.Drawing.Size(151, 20)
        Me.txt_sj_penjualan.TabIndex = 0
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(37, 7)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(66, 16)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Tanggal"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(37, 34)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(73, 16)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Customer"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(37, 61)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(99, 16)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "SJ Penjualan"
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label22.Font = New System.Drawing.Font("Cambria", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.Window
        Me.Label22.Location = New System.Drawing.Point(521, 1)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(352, 26)
        Me.Label22.TabIndex = 80
        Me.Label22.Text = "SURAT JALAN PENJUALAN"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label4)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_10)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_9)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_8)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_7)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_6)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_5)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_4)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_3)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_2)
        Me.Panel8.Controls.Add(Me.txt_id_claim_jadi_1)
        Me.Panel8.Location = New System.Drawing.Point(167, 31)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(76, 134)
        Me.Panel8.TabIndex = 118
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 114)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(66, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Id Claim Jadi"
        '
        'txt_id_claim_jadi_10
        '
        Me.txt_id_claim_jadi_10.Location = New System.Drawing.Point(38, 91)
        Me.txt_id_claim_jadi_10.Name = "txt_id_claim_jadi_10"
        Me.txt_id_claim_jadi_10.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_10.TabIndex = 9
        '
        'txt_id_claim_jadi_9
        '
        Me.txt_id_claim_jadi_9.Location = New System.Drawing.Point(38, 71)
        Me.txt_id_claim_jadi_9.Name = "txt_id_claim_jadi_9"
        Me.txt_id_claim_jadi_9.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_9.TabIndex = 8
        '
        'txt_id_claim_jadi_8
        '
        Me.txt_id_claim_jadi_8.Location = New System.Drawing.Point(38, 51)
        Me.txt_id_claim_jadi_8.Name = "txt_id_claim_jadi_8"
        Me.txt_id_claim_jadi_8.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_8.TabIndex = 7
        '
        'txt_id_claim_jadi_7
        '
        Me.txt_id_claim_jadi_7.Location = New System.Drawing.Point(38, 31)
        Me.txt_id_claim_jadi_7.Name = "txt_id_claim_jadi_7"
        Me.txt_id_claim_jadi_7.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_7.TabIndex = 6
        '
        'txt_id_claim_jadi_6
        '
        Me.txt_id_claim_jadi_6.Location = New System.Drawing.Point(38, 11)
        Me.txt_id_claim_jadi_6.Name = "txt_id_claim_jadi_6"
        Me.txt_id_claim_jadi_6.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_6.TabIndex = 5
        '
        'txt_id_claim_jadi_5
        '
        Me.txt_id_claim_jadi_5.Location = New System.Drawing.Point(8, 91)
        Me.txt_id_claim_jadi_5.Name = "txt_id_claim_jadi_5"
        Me.txt_id_claim_jadi_5.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_5.TabIndex = 4
        '
        'txt_id_claim_jadi_4
        '
        Me.txt_id_claim_jadi_4.Location = New System.Drawing.Point(8, 71)
        Me.txt_id_claim_jadi_4.Name = "txt_id_claim_jadi_4"
        Me.txt_id_claim_jadi_4.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_4.TabIndex = 3
        '
        'txt_id_claim_jadi_3
        '
        Me.txt_id_claim_jadi_3.Location = New System.Drawing.Point(8, 51)
        Me.txt_id_claim_jadi_3.Name = "txt_id_claim_jadi_3"
        Me.txt_id_claim_jadi_3.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_3.TabIndex = 2
        '
        'txt_id_claim_jadi_2
        '
        Me.txt_id_claim_jadi_2.Location = New System.Drawing.Point(8, 31)
        Me.txt_id_claim_jadi_2.Name = "txt_id_claim_jadi_2"
        Me.txt_id_claim_jadi_2.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_2.TabIndex = 1
        '
        'txt_id_claim_jadi_1
        '
        Me.txt_id_claim_jadi_1.Location = New System.Drawing.Point(8, 11)
        Me.txt_id_claim_jadi_1.Name = "txt_id_claim_jadi_1"
        Me.txt_id_claim_jadi_1.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_claim_jadi_1.TabIndex = 0
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Label3)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_10)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_9)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_8)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_7)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_6)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_5)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_4)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_3)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_2)
        Me.Panel7.Controls.Add(Me.txt_id_grade_b_1)
        Me.Panel7.Location = New System.Drawing.Point(91, 31)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(76, 134)
        Me.Panel7.TabIndex = 114
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 114)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Id Grade B"
        '
        'txt_id_grade_b_10
        '
        Me.txt_id_grade_b_10.Location = New System.Drawing.Point(38, 91)
        Me.txt_id_grade_b_10.Name = "txt_id_grade_b_10"
        Me.txt_id_grade_b_10.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_10.TabIndex = 9
        '
        'txt_id_grade_b_9
        '
        Me.txt_id_grade_b_9.Location = New System.Drawing.Point(38, 71)
        Me.txt_id_grade_b_9.Name = "txt_id_grade_b_9"
        Me.txt_id_grade_b_9.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_9.TabIndex = 8
        '
        'txt_id_grade_b_8
        '
        Me.txt_id_grade_b_8.Location = New System.Drawing.Point(38, 51)
        Me.txt_id_grade_b_8.Name = "txt_id_grade_b_8"
        Me.txt_id_grade_b_8.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_8.TabIndex = 7
        '
        'txt_id_grade_b_7
        '
        Me.txt_id_grade_b_7.Location = New System.Drawing.Point(38, 31)
        Me.txt_id_grade_b_7.Name = "txt_id_grade_b_7"
        Me.txt_id_grade_b_7.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_7.TabIndex = 6
        '
        'txt_id_grade_b_6
        '
        Me.txt_id_grade_b_6.Location = New System.Drawing.Point(38, 11)
        Me.txt_id_grade_b_6.Name = "txt_id_grade_b_6"
        Me.txt_id_grade_b_6.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_6.TabIndex = 5
        '
        'txt_id_grade_b_5
        '
        Me.txt_id_grade_b_5.Location = New System.Drawing.Point(8, 91)
        Me.txt_id_grade_b_5.Name = "txt_id_grade_b_5"
        Me.txt_id_grade_b_5.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_5.TabIndex = 4
        '
        'txt_id_grade_b_4
        '
        Me.txt_id_grade_b_4.Location = New System.Drawing.Point(8, 71)
        Me.txt_id_grade_b_4.Name = "txt_id_grade_b_4"
        Me.txt_id_grade_b_4.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_4.TabIndex = 3
        '
        'txt_id_grade_b_3
        '
        Me.txt_id_grade_b_3.Location = New System.Drawing.Point(8, 51)
        Me.txt_id_grade_b_3.Name = "txt_id_grade_b_3"
        Me.txt_id_grade_b_3.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_3.TabIndex = 2
        '
        'txt_id_grade_b_2
        '
        Me.txt_id_grade_b_2.Location = New System.Drawing.Point(8, 31)
        Me.txt_id_grade_b_2.Name = "txt_id_grade_b_2"
        Me.txt_id_grade_b_2.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_2.TabIndex = 1
        '
        'txt_id_grade_b_1
        '
        Me.txt_id_grade_b_1.Location = New System.Drawing.Point(8, 11)
        Me.txt_id_grade_b_1.Name = "txt_id_grade_b_1"
        Me.txt_id_grade_b_1.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_b_1.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_10)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_9)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_8)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_7)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_6)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_5)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_4)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_3)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_2)
        Me.Panel1.Controls.Add(Me.txt_id_grade_a_1)
        Me.Panel1.Location = New System.Drawing.Point(15, 31)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(76, 134)
        Me.Panel1.TabIndex = 117
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Id Grade A"
        '
        'txt_id_grade_a_10
        '
        Me.txt_id_grade_a_10.Location = New System.Drawing.Point(38, 91)
        Me.txt_id_grade_a_10.Name = "txt_id_grade_a_10"
        Me.txt_id_grade_a_10.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_10.TabIndex = 9
        '
        'txt_id_grade_a_9
        '
        Me.txt_id_grade_a_9.Location = New System.Drawing.Point(38, 71)
        Me.txt_id_grade_a_9.Name = "txt_id_grade_a_9"
        Me.txt_id_grade_a_9.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_9.TabIndex = 8
        '
        'txt_id_grade_a_8
        '
        Me.txt_id_grade_a_8.Location = New System.Drawing.Point(38, 51)
        Me.txt_id_grade_a_8.Name = "txt_id_grade_a_8"
        Me.txt_id_grade_a_8.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_8.TabIndex = 7
        '
        'txt_id_grade_a_7
        '
        Me.txt_id_grade_a_7.Location = New System.Drawing.Point(38, 31)
        Me.txt_id_grade_a_7.Name = "txt_id_grade_a_7"
        Me.txt_id_grade_a_7.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_7.TabIndex = 6
        '
        'txt_id_grade_a_6
        '
        Me.txt_id_grade_a_6.Location = New System.Drawing.Point(38, 11)
        Me.txt_id_grade_a_6.Name = "txt_id_grade_a_6"
        Me.txt_id_grade_a_6.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_6.TabIndex = 5
        '
        'txt_id_grade_a_5
        '
        Me.txt_id_grade_a_5.Location = New System.Drawing.Point(8, 91)
        Me.txt_id_grade_a_5.Name = "txt_id_grade_a_5"
        Me.txt_id_grade_a_5.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_5.TabIndex = 4
        '
        'txt_id_grade_a_4
        '
        Me.txt_id_grade_a_4.Location = New System.Drawing.Point(8, 71)
        Me.txt_id_grade_a_4.Name = "txt_id_grade_a_4"
        Me.txt_id_grade_a_4.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_4.TabIndex = 3
        '
        'txt_id_grade_a_3
        '
        Me.txt_id_grade_a_3.Location = New System.Drawing.Point(8, 51)
        Me.txt_id_grade_a_3.Name = "txt_id_grade_a_3"
        Me.txt_id_grade_a_3.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_3.TabIndex = 2
        '
        'txt_id_grade_a_2
        '
        Me.txt_id_grade_a_2.Location = New System.Drawing.Point(8, 31)
        Me.txt_id_grade_a_2.Name = "txt_id_grade_a_2"
        Me.txt_id_grade_a_2.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_2.TabIndex = 1
        '
        'txt_id_grade_a_1
        '
        Me.txt_id_grade_a_1.Location = New System.Drawing.Point(8, 11)
        Me.txt_id_grade_a_1.Name = "txt_id_grade_a_1"
        Me.txt_id_grade_a_1.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grade_a_1.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_10)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_9)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_8)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_7)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_6)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_5)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_4)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_3)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_2)
        Me.Panel5.Controls.Add(Me.txt_id_penjualan_1)
        Me.Panel5.Location = New System.Drawing.Point(243, 31)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(116, 134)
        Me.Panel5.TabIndex = 116
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Id Penjualan"
        '
        'txt_id_penjualan_10
        '
        Me.txt_id_penjualan_10.Location = New System.Drawing.Point(58, 90)
        Me.txt_id_penjualan_10.Name = "txt_id_penjualan_10"
        Me.txt_id_penjualan_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_10.TabIndex = 9
        '
        'txt_id_penjualan_9
        '
        Me.txt_id_penjualan_9.Location = New System.Drawing.Point(58, 70)
        Me.txt_id_penjualan_9.Name = "txt_id_penjualan_9"
        Me.txt_id_penjualan_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_9.TabIndex = 8
        '
        'txt_id_penjualan_8
        '
        Me.txt_id_penjualan_8.Location = New System.Drawing.Point(58, 50)
        Me.txt_id_penjualan_8.Name = "txt_id_penjualan_8"
        Me.txt_id_penjualan_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_8.TabIndex = 7
        '
        'txt_id_penjualan_7
        '
        Me.txt_id_penjualan_7.Location = New System.Drawing.Point(58, 30)
        Me.txt_id_penjualan_7.Name = "txt_id_penjualan_7"
        Me.txt_id_penjualan_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_7.TabIndex = 6
        '
        'txt_id_penjualan_6
        '
        Me.txt_id_penjualan_6.Location = New System.Drawing.Point(58, 10)
        Me.txt_id_penjualan_6.Name = "txt_id_penjualan_6"
        Me.txt_id_penjualan_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_6.TabIndex = 5
        '
        'txt_id_penjualan_5
        '
        Me.txt_id_penjualan_5.Location = New System.Drawing.Point(8, 90)
        Me.txt_id_penjualan_5.Name = "txt_id_penjualan_5"
        Me.txt_id_penjualan_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_5.TabIndex = 4
        '
        'txt_id_penjualan_4
        '
        Me.txt_id_penjualan_4.Location = New System.Drawing.Point(8, 70)
        Me.txt_id_penjualan_4.Name = "txt_id_penjualan_4"
        Me.txt_id_penjualan_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_4.TabIndex = 3
        '
        'txt_id_penjualan_3
        '
        Me.txt_id_penjualan_3.Location = New System.Drawing.Point(8, 50)
        Me.txt_id_penjualan_3.Name = "txt_id_penjualan_3"
        Me.txt_id_penjualan_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_3.TabIndex = 2
        '
        'txt_id_penjualan_2
        '
        Me.txt_id_penjualan_2.Location = New System.Drawing.Point(8, 30)
        Me.txt_id_penjualan_2.Name = "txt_id_penjualan_2"
        Me.txt_id_penjualan_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_2.TabIndex = 1
        '
        'txt_id_penjualan_1
        '
        Me.txt_id_penjualan_1.Location = New System.Drawing.Point(8, 10)
        Me.txt_id_penjualan_1.Name = "txt_id_penjualan_1"
        Me.txt_id_penjualan_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_penjualan_1.TabIndex = 0
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label23)
        Me.Panel6.Controls.Add(Me.txt_id_grey_10)
        Me.Panel6.Controls.Add(Me.txt_id_grey_9)
        Me.Panel6.Controls.Add(Me.txt_id_grey_8)
        Me.Panel6.Controls.Add(Me.txt_id_grey_7)
        Me.Panel6.Controls.Add(Me.txt_id_grey_6)
        Me.Panel6.Controls.Add(Me.txt_id_grey_5)
        Me.Panel6.Controls.Add(Me.txt_id_grey_4)
        Me.Panel6.Controls.Add(Me.txt_id_grey_3)
        Me.Panel6.Controls.Add(Me.txt_id_grey_2)
        Me.Panel6.Controls.Add(Me.txt_id_grey_1)
        Me.Panel6.Location = New System.Drawing.Point(359, 31)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(76, 134)
        Me.Panel6.TabIndex = 113
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(18, 114)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(41, 13)
        Me.Label23.TabIndex = 10
        Me.Label23.Text = "Id Grey"
        '
        'txt_id_grey_10
        '
        Me.txt_id_grey_10.Location = New System.Drawing.Point(38, 91)
        Me.txt_id_grey_10.Name = "txt_id_grey_10"
        Me.txt_id_grey_10.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_10.TabIndex = 9
        '
        'txt_id_grey_9
        '
        Me.txt_id_grey_9.Location = New System.Drawing.Point(38, 71)
        Me.txt_id_grey_9.Name = "txt_id_grey_9"
        Me.txt_id_grey_9.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_9.TabIndex = 8
        '
        'txt_id_grey_8
        '
        Me.txt_id_grey_8.Location = New System.Drawing.Point(38, 51)
        Me.txt_id_grey_8.Name = "txt_id_grey_8"
        Me.txt_id_grey_8.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_8.TabIndex = 7
        '
        'txt_id_grey_7
        '
        Me.txt_id_grey_7.Location = New System.Drawing.Point(38, 31)
        Me.txt_id_grey_7.Name = "txt_id_grey_7"
        Me.txt_id_grey_7.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_7.TabIndex = 6
        '
        'txt_id_grey_6
        '
        Me.txt_id_grey_6.Location = New System.Drawing.Point(38, 11)
        Me.txt_id_grey_6.Name = "txt_id_grey_6"
        Me.txt_id_grey_6.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_6.TabIndex = 5
        '
        'txt_id_grey_5
        '
        Me.txt_id_grey_5.Location = New System.Drawing.Point(8, 91)
        Me.txt_id_grey_5.Name = "txt_id_grey_5"
        Me.txt_id_grey_5.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_5.TabIndex = 4
        '
        'txt_id_grey_4
        '
        Me.txt_id_grey_4.Location = New System.Drawing.Point(8, 71)
        Me.txt_id_grey_4.Name = "txt_id_grey_4"
        Me.txt_id_grey_4.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_4.TabIndex = 3
        '
        'txt_id_grey_3
        '
        Me.txt_id_grey_3.Location = New System.Drawing.Point(8, 51)
        Me.txt_id_grey_3.Name = "txt_id_grey_3"
        Me.txt_id_grey_3.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_3.TabIndex = 2
        '
        'txt_id_grey_2
        '
        Me.txt_id_grey_2.Location = New System.Drawing.Point(8, 31)
        Me.txt_id_grey_2.Name = "txt_id_grey_2"
        Me.txt_id_grey_2.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_2.TabIndex = 1
        '
        'txt_id_grey_1
        '
        Me.txt_id_grey_1.Location = New System.Drawing.Point(8, 11)
        Me.txt_id_grey_1.Name = "txt_id_grey_1"
        Me.txt_id_grey_1.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_1.TabIndex = 0
        '
        'txt_gudang_1
        '
        Me.txt_gudang_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_1.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_1.Name = "txt_gudang_1"
        Me.txt_gudang_1.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_1.TabIndex = 4
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btn_batal_10)
        Me.Panel4.Controls.Add(Me.btn_batal_9)
        Me.Panel4.Controls.Add(Me.btn_batal_8)
        Me.Panel4.Controls.Add(Me.btn_batal_7)
        Me.Panel4.Controls.Add(Me.btn_batal_6)
        Me.Panel4.Controls.Add(Me.btn_batal_5)
        Me.Panel4.Controls.Add(Me.btn_batal_4)
        Me.Panel4.Controls.Add(Me.btn_batal_3)
        Me.Panel4.Controls.Add(Me.btn_batal_2)
        Me.Panel4.Controls.Add(Me.btn_batal_1)
        Me.Panel4.Controls.Add(Me.btn_hapus_10)
        Me.Panel4.Controls.Add(Me.btn_selesai_10)
        Me.Panel4.Controls.Add(Me.btn_hapus_9)
        Me.Panel4.Controls.Add(Me.btn_selesai_9)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_9)
        Me.Panel4.Controls.Add(Me.btn_hapus_8)
        Me.Panel4.Controls.Add(Me.btn_selesai_8)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_8)
        Me.Panel4.Controls.Add(Me.btn_hapus_7)
        Me.Panel4.Controls.Add(Me.btn_selesai_7)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_7)
        Me.Panel4.Controls.Add(Me.btn_hapus_6)
        Me.Panel4.Controls.Add(Me.btn_selesai_6)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_6)
        Me.Panel4.Controls.Add(Me.btn_hapus_5)
        Me.Panel4.Controls.Add(Me.btn_selesai_5)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_5)
        Me.Panel4.Controls.Add(Me.btn_hapus_4)
        Me.Panel4.Controls.Add(Me.btn_selesai_4)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_4)
        Me.Panel4.Controls.Add(Me.btn_hapus_3)
        Me.Panel4.Controls.Add(Me.btn_selesai_3)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_3)
        Me.Panel4.Controls.Add(Me.btn_hapus_2)
        Me.Panel4.Controls.Add(Me.btn_selesai_2)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_2)
        Me.Panel4.Controls.Add(Me.btn_selesai_1)
        Me.Panel4.Controls.Add(Me.btn_tambah_baris_1)
        Me.Panel4.Location = New System.Drawing.Point(434, 226)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(469, 375)
        Me.Panel4.TabIndex = 90
        '
        'btn_batal_10
        '
        Me.btn_batal_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_10.Image = CType(resources.GetObject("btn_batal_10.Image"), System.Drawing.Image)
        Me.btn_batal_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_10.Location = New System.Drawing.Point(310, 329)
        Me.btn_batal_10.Name = "btn_batal_10"
        Me.btn_batal_10.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_10.TabIndex = 68
        Me.btn_batal_10.Text = "Batal"
        Me.btn_batal_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_10.UseVisualStyleBackColor = False
        '
        'btn_batal_9
        '
        Me.btn_batal_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_9.Image = CType(resources.GetObject("btn_batal_9.Image"), System.Drawing.Image)
        Me.btn_batal_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_9.Location = New System.Drawing.Point(381, 293)
        Me.btn_batal_9.Name = "btn_batal_9"
        Me.btn_batal_9.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_9.TabIndex = 67
        Me.btn_batal_9.Text = "Batal"
        Me.btn_batal_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_9.UseVisualStyleBackColor = False
        '
        'btn_batal_8
        '
        Me.btn_batal_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_8.Image = CType(resources.GetObject("btn_batal_8.Image"), System.Drawing.Image)
        Me.btn_batal_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_8.Location = New System.Drawing.Point(381, 257)
        Me.btn_batal_8.Name = "btn_batal_8"
        Me.btn_batal_8.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_8.TabIndex = 66
        Me.btn_batal_8.Text = "Batal"
        Me.btn_batal_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_8.UseVisualStyleBackColor = False
        '
        'btn_batal_7
        '
        Me.btn_batal_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_7.Image = CType(resources.GetObject("btn_batal_7.Image"), System.Drawing.Image)
        Me.btn_batal_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_7.Location = New System.Drawing.Point(381, 223)
        Me.btn_batal_7.Name = "btn_batal_7"
        Me.btn_batal_7.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_7.TabIndex = 65
        Me.btn_batal_7.Text = "Batal"
        Me.btn_batal_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_7.UseVisualStyleBackColor = False
        '
        'btn_batal_6
        '
        Me.btn_batal_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_6.Image = CType(resources.GetObject("btn_batal_6.Image"), System.Drawing.Image)
        Me.btn_batal_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_6.Location = New System.Drawing.Point(381, 188)
        Me.btn_batal_6.Name = "btn_batal_6"
        Me.btn_batal_6.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_6.TabIndex = 64
        Me.btn_batal_6.Text = "Batal"
        Me.btn_batal_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_6.UseVisualStyleBackColor = False
        '
        'btn_batal_5
        '
        Me.btn_batal_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_5.Image = CType(resources.GetObject("btn_batal_5.Image"), System.Drawing.Image)
        Me.btn_batal_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_5.Location = New System.Drawing.Point(381, 154)
        Me.btn_batal_5.Name = "btn_batal_5"
        Me.btn_batal_5.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_5.TabIndex = 63
        Me.btn_batal_5.Text = "Batal"
        Me.btn_batal_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_5.UseVisualStyleBackColor = False
        '
        'btn_batal_4
        '
        Me.btn_batal_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_4.Image = CType(resources.GetObject("btn_batal_4.Image"), System.Drawing.Image)
        Me.btn_batal_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_4.Location = New System.Drawing.Point(381, 119)
        Me.btn_batal_4.Name = "btn_batal_4"
        Me.btn_batal_4.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_4.TabIndex = 62
        Me.btn_batal_4.Text = "Batal"
        Me.btn_batal_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_4.UseVisualStyleBackColor = False
        '
        'btn_batal_3
        '
        Me.btn_batal_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_3.Image = CType(resources.GetObject("btn_batal_3.Image"), System.Drawing.Image)
        Me.btn_batal_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_3.Location = New System.Drawing.Point(381, 83)
        Me.btn_batal_3.Name = "btn_batal_3"
        Me.btn_batal_3.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_3.TabIndex = 61
        Me.btn_batal_3.Text = "Batal"
        Me.btn_batal_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_3.UseVisualStyleBackColor = False
        '
        'btn_batal_2
        '
        Me.btn_batal_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_2.Image = CType(resources.GetObject("btn_batal_2.Image"), System.Drawing.Image)
        Me.btn_batal_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_2.Location = New System.Drawing.Point(381, 47)
        Me.btn_batal_2.Name = "btn_batal_2"
        Me.btn_batal_2.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_2.TabIndex = 60
        Me.btn_batal_2.Text = "Batal"
        Me.btn_batal_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_2.UseVisualStyleBackColor = False
        '
        'btn_batal_1
        '
        Me.btn_batal_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_1.Image = CType(resources.GetObject("btn_batal_1.Image"), System.Drawing.Image)
        Me.btn_batal_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_1.Location = New System.Drawing.Point(327, 11)
        Me.btn_batal_1.Name = "btn_batal_1"
        Me.btn_batal_1.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_1.TabIndex = 59
        Me.btn_batal_1.Text = "Batal"
        Me.btn_batal_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_1.UseVisualStyleBackColor = False
        '
        'btn_hapus_10
        '
        Me.btn_hapus_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_10.Image = CType(resources.GetObject("btn_hapus_10.Image"), System.Drawing.Image)
        Me.btn_hapus_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_10.Location = New System.Drawing.Point(202, 329)
        Me.btn_hapus_10.Name = "btn_hapus_10"
        Me.btn_hapus_10.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_10.TabIndex = 37
        Me.btn_hapus_10.Text = "Hapus"
        Me.btn_hapus_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_10.UseVisualStyleBackColor = False
        '
        'btn_selesai_10
        '
        Me.btn_selesai_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_10.Image = CType(resources.GetObject("btn_selesai_10.Image"), System.Drawing.Image)
        Me.btn_selesai_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_10.Location = New System.Drawing.Point(94, 329)
        Me.btn_selesai_10.Name = "btn_selesai_10"
        Me.btn_selesai_10.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_10.TabIndex = 36
        Me.btn_selesai_10.Text = "Selesai"
        Me.btn_selesai_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_10.UseVisualStyleBackColor = False
        '
        'btn_hapus_9
        '
        Me.btn_hapus_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_9.Image = CType(resources.GetObject("btn_hapus_9.Image"), System.Drawing.Image)
        Me.btn_hapus_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_9.Location = New System.Drawing.Point(273, 293)
        Me.btn_hapus_9.Name = "btn_hapus_9"
        Me.btn_hapus_9.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_9.TabIndex = 34
        Me.btn_hapus_9.Text = "Hapus"
        Me.btn_hapus_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_9.UseVisualStyleBackColor = False
        '
        'btn_selesai_9
        '
        Me.btn_selesai_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_9.Image = CType(resources.GetObject("btn_selesai_9.Image"), System.Drawing.Image)
        Me.btn_selesai_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_9.Location = New System.Drawing.Point(165, 293)
        Me.btn_selesai_9.Name = "btn_selesai_9"
        Me.btn_selesai_9.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_9.TabIndex = 33
        Me.btn_selesai_9.Text = "Selesai"
        Me.btn_selesai_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_9.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_9
        '
        Me.btn_tambah_baris_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_9.Image = CType(resources.GetObject("btn_tambah_baris_9.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_9.Location = New System.Drawing.Point(22, 293)
        Me.btn_tambah_baris_9.Name = "btn_tambah_baris_9"
        Me.btn_tambah_baris_9.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_9.TabIndex = 32
        Me.btn_tambah_baris_9.Text = "Tambah Baris"
        Me.btn_tambah_baris_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_9.UseVisualStyleBackColor = False
        '
        'btn_hapus_8
        '
        Me.btn_hapus_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_8.Image = CType(resources.GetObject("btn_hapus_8.Image"), System.Drawing.Image)
        Me.btn_hapus_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_8.Location = New System.Drawing.Point(273, 257)
        Me.btn_hapus_8.Name = "btn_hapus_8"
        Me.btn_hapus_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_8.TabIndex = 31
        Me.btn_hapus_8.Text = "Hapus"
        Me.btn_hapus_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_8.UseVisualStyleBackColor = False
        '
        'btn_selesai_8
        '
        Me.btn_selesai_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_8.Image = CType(resources.GetObject("btn_selesai_8.Image"), System.Drawing.Image)
        Me.btn_selesai_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_8.Location = New System.Drawing.Point(165, 257)
        Me.btn_selesai_8.Name = "btn_selesai_8"
        Me.btn_selesai_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_8.TabIndex = 30
        Me.btn_selesai_8.Text = "Selesai"
        Me.btn_selesai_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_8.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_8
        '
        Me.btn_tambah_baris_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_8.Image = CType(resources.GetObject("btn_tambah_baris_8.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_8.Location = New System.Drawing.Point(22, 257)
        Me.btn_tambah_baris_8.Name = "btn_tambah_baris_8"
        Me.btn_tambah_baris_8.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_8.TabIndex = 29
        Me.btn_tambah_baris_8.Text = "Tambah Baris"
        Me.btn_tambah_baris_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_8.UseVisualStyleBackColor = False
        '
        'btn_hapus_7
        '
        Me.btn_hapus_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_7.Image = CType(resources.GetObject("btn_hapus_7.Image"), System.Drawing.Image)
        Me.btn_hapus_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_7.Location = New System.Drawing.Point(273, 223)
        Me.btn_hapus_7.Name = "btn_hapus_7"
        Me.btn_hapus_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_7.TabIndex = 28
        Me.btn_hapus_7.Text = "Hapus"
        Me.btn_hapus_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_7.UseVisualStyleBackColor = False
        '
        'btn_selesai_7
        '
        Me.btn_selesai_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_7.Image = CType(resources.GetObject("btn_selesai_7.Image"), System.Drawing.Image)
        Me.btn_selesai_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_7.Location = New System.Drawing.Point(165, 223)
        Me.btn_selesai_7.Name = "btn_selesai_7"
        Me.btn_selesai_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_7.TabIndex = 27
        Me.btn_selesai_7.Text = "Selesai"
        Me.btn_selesai_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_7.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_7
        '
        Me.btn_tambah_baris_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_7.Image = CType(resources.GetObject("btn_tambah_baris_7.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_7.Location = New System.Drawing.Point(22, 223)
        Me.btn_tambah_baris_7.Name = "btn_tambah_baris_7"
        Me.btn_tambah_baris_7.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_7.TabIndex = 26
        Me.btn_tambah_baris_7.Text = "Tambah Baris"
        Me.btn_tambah_baris_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_7.UseVisualStyleBackColor = False
        '
        'btn_hapus_6
        '
        Me.btn_hapus_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_6.Image = CType(resources.GetObject("btn_hapus_6.Image"), System.Drawing.Image)
        Me.btn_hapus_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_6.Location = New System.Drawing.Point(273, 188)
        Me.btn_hapus_6.Name = "btn_hapus_6"
        Me.btn_hapus_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_6.TabIndex = 25
        Me.btn_hapus_6.Text = "Hapus"
        Me.btn_hapus_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_6.UseVisualStyleBackColor = False
        '
        'btn_selesai_6
        '
        Me.btn_selesai_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_6.Image = CType(resources.GetObject("btn_selesai_6.Image"), System.Drawing.Image)
        Me.btn_selesai_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_6.Location = New System.Drawing.Point(165, 188)
        Me.btn_selesai_6.Name = "btn_selesai_6"
        Me.btn_selesai_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_6.TabIndex = 24
        Me.btn_selesai_6.Text = "Selesai"
        Me.btn_selesai_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_6.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_6
        '
        Me.btn_tambah_baris_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_6.Image = CType(resources.GetObject("btn_tambah_baris_6.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_6.Location = New System.Drawing.Point(22, 188)
        Me.btn_tambah_baris_6.Name = "btn_tambah_baris_6"
        Me.btn_tambah_baris_6.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_6.TabIndex = 23
        Me.btn_tambah_baris_6.Text = "Tambah Baris"
        Me.btn_tambah_baris_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_6.UseVisualStyleBackColor = False
        '
        'btn_hapus_5
        '
        Me.btn_hapus_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_5.Image = CType(resources.GetObject("btn_hapus_5.Image"), System.Drawing.Image)
        Me.btn_hapus_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_5.Location = New System.Drawing.Point(273, 154)
        Me.btn_hapus_5.Name = "btn_hapus_5"
        Me.btn_hapus_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_5.TabIndex = 22
        Me.btn_hapus_5.Text = "Hapus"
        Me.btn_hapus_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_5.UseVisualStyleBackColor = False
        '
        'btn_selesai_5
        '
        Me.btn_selesai_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_5.Image = CType(resources.GetObject("btn_selesai_5.Image"), System.Drawing.Image)
        Me.btn_selesai_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_5.Location = New System.Drawing.Point(165, 154)
        Me.btn_selesai_5.Name = "btn_selesai_5"
        Me.btn_selesai_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_5.TabIndex = 21
        Me.btn_selesai_5.Text = "Selesai"
        Me.btn_selesai_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_5.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_5
        '
        Me.btn_tambah_baris_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_5.Image = CType(resources.GetObject("btn_tambah_baris_5.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_5.Location = New System.Drawing.Point(22, 154)
        Me.btn_tambah_baris_5.Name = "btn_tambah_baris_5"
        Me.btn_tambah_baris_5.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_5.TabIndex = 20
        Me.btn_tambah_baris_5.Text = "Tambah Baris"
        Me.btn_tambah_baris_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_5.UseVisualStyleBackColor = False
        '
        'btn_hapus_4
        '
        Me.btn_hapus_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_4.Image = CType(resources.GetObject("btn_hapus_4.Image"), System.Drawing.Image)
        Me.btn_hapus_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_4.Location = New System.Drawing.Point(273, 119)
        Me.btn_hapus_4.Name = "btn_hapus_4"
        Me.btn_hapus_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_4.TabIndex = 19
        Me.btn_hapus_4.Text = "Hapus"
        Me.btn_hapus_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_4.UseVisualStyleBackColor = False
        '
        'btn_selesai_4
        '
        Me.btn_selesai_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_4.Image = CType(resources.GetObject("btn_selesai_4.Image"), System.Drawing.Image)
        Me.btn_selesai_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_4.Location = New System.Drawing.Point(165, 119)
        Me.btn_selesai_4.Name = "btn_selesai_4"
        Me.btn_selesai_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_4.TabIndex = 18
        Me.btn_selesai_4.Text = "Selesai"
        Me.btn_selesai_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_4.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_4
        '
        Me.btn_tambah_baris_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_4.Image = CType(resources.GetObject("btn_tambah_baris_4.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_4.Location = New System.Drawing.Point(22, 119)
        Me.btn_tambah_baris_4.Name = "btn_tambah_baris_4"
        Me.btn_tambah_baris_4.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_4.TabIndex = 17
        Me.btn_tambah_baris_4.Text = "Tambah Baris"
        Me.btn_tambah_baris_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_4.UseVisualStyleBackColor = False
        '
        'btn_hapus_3
        '
        Me.btn_hapus_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_3.Image = CType(resources.GetObject("btn_hapus_3.Image"), System.Drawing.Image)
        Me.btn_hapus_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_3.Location = New System.Drawing.Point(273, 83)
        Me.btn_hapus_3.Name = "btn_hapus_3"
        Me.btn_hapus_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_3.TabIndex = 16
        Me.btn_hapus_3.Text = "Hapus"
        Me.btn_hapus_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_3.UseVisualStyleBackColor = False
        '
        'btn_selesai_3
        '
        Me.btn_selesai_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_3.Image = CType(resources.GetObject("btn_selesai_3.Image"), System.Drawing.Image)
        Me.btn_selesai_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_3.Location = New System.Drawing.Point(165, 83)
        Me.btn_selesai_3.Name = "btn_selesai_3"
        Me.btn_selesai_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_3.TabIndex = 15
        Me.btn_selesai_3.Text = "Selesai"
        Me.btn_selesai_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_3.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_3
        '
        Me.btn_tambah_baris_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_3.Image = CType(resources.GetObject("btn_tambah_baris_3.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_3.Location = New System.Drawing.Point(22, 83)
        Me.btn_tambah_baris_3.Name = "btn_tambah_baris_3"
        Me.btn_tambah_baris_3.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_3.TabIndex = 14
        Me.btn_tambah_baris_3.Text = "Tambah Baris"
        Me.btn_tambah_baris_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_3.UseVisualStyleBackColor = False
        '
        'btn_hapus_2
        '
        Me.btn_hapus_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_2.Image = CType(resources.GetObject("btn_hapus_2.Image"), System.Drawing.Image)
        Me.btn_hapus_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_2.Location = New System.Drawing.Point(273, 47)
        Me.btn_hapus_2.Name = "btn_hapus_2"
        Me.btn_hapus_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_2.TabIndex = 13
        Me.btn_hapus_2.Text = "Hapus"
        Me.btn_hapus_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_2
        '
        Me.btn_selesai_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_2.Image = CType(resources.GetObject("btn_selesai_2.Image"), System.Drawing.Image)
        Me.btn_selesai_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_2.Location = New System.Drawing.Point(165, 47)
        Me.btn_selesai_2.Name = "btn_selesai_2"
        Me.btn_selesai_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_2.TabIndex = 12
        Me.btn_selesai_2.Text = "Selesai"
        Me.btn_selesai_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_2.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_2
        '
        Me.btn_tambah_baris_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_2.Image = CType(resources.GetObject("btn_tambah_baris_2.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_2.Location = New System.Drawing.Point(22, 47)
        Me.btn_tambah_baris_2.Name = "btn_tambah_baris_2"
        Me.btn_tambah_baris_2.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_2.TabIndex = 11
        Me.btn_tambah_baris_2.Text = "Tambah Baris"
        Me.btn_tambah_baris_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_1
        '
        Me.btn_selesai_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_1.Image = CType(resources.GetObject("btn_selesai_1.Image"), System.Drawing.Image)
        Me.btn_selesai_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_1.Location = New System.Drawing.Point(219, 11)
        Me.btn_selesai_1.Name = "btn_selesai_1"
        Me.btn_selesai_1.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_1.TabIndex = 4
        Me.btn_selesai_1.Text = "Selesai"
        Me.btn_selesai_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_1.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_1
        '
        Me.btn_tambah_baris_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_1.Image = CType(resources.GetObject("btn_tambah_baris_1.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_1.Location = New System.Drawing.Point(76, 11)
        Me.btn_tambah_baris_1.Name = "btn_tambah_baris_1"
        Me.btn_tambah_baris_1.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_1.TabIndex = 3
        Me.btn_tambah_baris_1.Text = "Tambah Baris"
        Me.btn_tambah_baris_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_1.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label26)
        Me.Panel2.Controls.Add(Me.Label25)
        Me.Panel2.Controls.Add(Me.Label27)
        Me.Panel2.Controls.Add(Me.Label28)
        Me.Panel2.Controls.Add(Me.Label29)
        Me.Panel2.Controls.Add(Me.Label30)
        Me.Panel2.Controls.Add(Me.Label32)
        Me.Panel2.Controls.Add(Me.Label33)
        Me.Panel2.Controls.Add(Me.Label34)
        Me.Panel2.Controls.Add(Me.Label35)
        Me.Panel2.Controls.Add(Me.Label36)
        Me.Panel2.Location = New System.Drawing.Point(-3, 161)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1342, 30)
        Me.Panel2.TabIndex = 91
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.Window
        Me.Label26.Location = New System.Drawing.Point(761, 7)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(39, 13)
        Me.Label26.TabIndex = 16
        Me.Label26.Text = "Yards"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.Window
        Me.Label25.Location = New System.Drawing.Point(1177, 8)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(72, 13)
        Me.Label25.TabIndex = 13
        Me.Label25.Text = "Keterangan"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.Window
        Me.Label27.Location = New System.Drawing.Point(999, 7)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(74, 13)
        Me.Label27.TabIndex = 12
        Me.Label27.Text = "Total Harga"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.Window
        Me.Label28.Location = New System.Drawing.Point(875, 8)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(41, 13)
        Me.Label28.TabIndex = 10
        Me.Label28.Text = "Harga"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.SystemColors.Window
        Me.Label29.Location = New System.Drawing.Point(670, 7)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(47, 13)
        Me.Label29.TabIndex = 8
        Me.Label29.Text = "Gulung"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.SystemColors.Window
        Me.Label30.Location = New System.Drawing.Point(574, 8)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(44, 13)
        Me.Label30.TabIndex = 7
        Me.Label30.Text = "Warna"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.Window
        Me.Label32.Location = New System.Drawing.Point(434, 7)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(65, 13)
        Me.Label32.TabIndex = 5
        Me.Label32.Text = "Jenis Kain"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.SystemColors.Window
        Me.Label33.Location = New System.Drawing.Point(298, 7)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(51, 13)
        Me.Label33.TabIndex = 4
        Me.Label33.Text = "Gudang"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.Window
        Me.Label34.Location = New System.Drawing.Point(159, 7)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(71, 13)
        Me.Label34.TabIndex = 3
        Me.Label34.Text = "SJ Packing"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.Window
        Me.Label35.Location = New System.Drawing.Point(66, 7)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(40, 13)
        Me.Label35.TabIndex = 1
        Me.Label35.Text = "STOK"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.Window
        Me.Label36.Location = New System.Drawing.Point(8, 7)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(27, 13)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "No."
        '
        'Panel_1
        '
        Me.Panel_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_1.Controls.Add(Me.txt_gudang_1)
        Me.Panel_1.Controls.Add(Me.txt_yards_1)
        Me.Panel_1.Controls.Add(Me.txt_warna_1)
        Me.Panel_1.Controls.Add(Me.txt_total_harga_1)
        Me.Panel_1.Controls.Add(Me.txt_keterangan_1)
        Me.Panel_1.Controls.Add(Me.txt_harga_1)
        Me.Panel_1.Controls.Add(Me.cb_stok_1)
        Me.Panel_1.Controls.Add(Me.txt_gulung_1)
        Me.Panel_1.Controls.Add(Me.txt_sj_packing_1)
        Me.Panel_1.Controls.Add(Me.txt_no_urut_1)
        Me.Panel_1.Controls.Add(Me.txt_jenis_kain_1)
        Me.Panel_1.Location = New System.Drawing.Point(-3, 191)
        Me.Panel_1.Name = "Panel_1"
        Me.Panel_1.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_1.TabIndex = 92
        '
        'txt_yards_1
        '
        Me.txt_yards_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_1.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_1.Name = "txt_yards_1"
        Me.txt_yards_1.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_1.TabIndex = 90
        Me.txt_yards_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_total_harga_1
        '
        Me.txt_total_harga_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_1.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_1.Name = "txt_total_harga_1"
        Me.txt_total_harga_1.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_1.TabIndex = 8
        Me.txt_total_harga_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_1
        '
        Me.txt_keterangan_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_1.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_1.Name = "txt_keterangan_1"
        Me.txt_keterangan_1.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_1.TabIndex = 7
        '
        'txt_harga_1
        '
        Me.txt_harga_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_1.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_1.Name = "txt_harga_1"
        Me.txt_harga_1.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_1.TabIndex = 89
        Me.txt_harga_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_1
        '
        Me.txt_gulung_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_1.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_1.Name = "txt_gulung_1"
        Me.txt_gulung_1.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_1.TabIndex = 6
        Me.txt_gulung_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_no_urut_1
        '
        Me.txt_no_urut_1.AutoSize = True
        Me.txt_no_urut_1.Location = New System.Drawing.Point(14, 11)
        Me.txt_no_urut_1.Name = "txt_no_urut_1"
        Me.txt_no_urut_1.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_1.TabIndex = 0
        Me.txt_no_urut_1.Text = "1"
        '
        'Panel_2
        '
        Me.Panel_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_2.Controls.Add(Me.txt_gudang_2)
        Me.Panel_2.Controls.Add(Me.txt_yards_2)
        Me.Panel_2.Controls.Add(Me.txt_warna_2)
        Me.Panel_2.Controls.Add(Me.txt_total_harga_2)
        Me.Panel_2.Controls.Add(Me.txt_keterangan_2)
        Me.Panel_2.Controls.Add(Me.txt_harga_2)
        Me.Panel_2.Controls.Add(Me.cb_stok_2)
        Me.Panel_2.Controls.Add(Me.txt_gulung_2)
        Me.Panel_2.Controls.Add(Me.txt_sj_packing_2)
        Me.Panel_2.Controls.Add(Me.Label5)
        Me.Panel_2.Controls.Add(Me.txt_jenis_kain_2)
        Me.Panel_2.Location = New System.Drawing.Point(-3, 226)
        Me.Panel_2.Name = "Panel_2"
        Me.Panel_2.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_2.TabIndex = 93
        '
        'txt_gudang_2
        '
        Me.txt_gudang_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_2.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_2.Name = "txt_gudang_2"
        Me.txt_gudang_2.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_2.TabIndex = 4
        '
        'txt_yards_2
        '
        Me.txt_yards_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_2.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_2.Name = "txt_yards_2"
        Me.txt_yards_2.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_2.TabIndex = 90
        Me.txt_yards_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_2
        '
        Me.txt_warna_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_2.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_2.Name = "txt_warna_2"
        Me.txt_warna_2.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_2.TabIndex = 45
        '
        'txt_total_harga_2
        '
        Me.txt_total_harga_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_2.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_2.Name = "txt_total_harga_2"
        Me.txt_total_harga_2.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_2.TabIndex = 8
        Me.txt_total_harga_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_2
        '
        Me.txt_keterangan_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_2.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_2.Name = "txt_keterangan_2"
        Me.txt_keterangan_2.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_2.TabIndex = 7
        '
        'txt_harga_2
        '
        Me.txt_harga_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_2.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_2.Name = "txt_harga_2"
        Me.txt_harga_2.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_2.TabIndex = 89
        Me.txt_harga_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_2
        '
        Me.cb_stok_2.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_2.FormattingEnabled = True
        Me.cb_stok_2.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_2.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_2.Name = "cb_stok_2"
        Me.cb_stok_2.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_2.TabIndex = 43
        Me.cb_stok_2.Text = "-- Pilih Stok --"
        '
        'txt_gulung_2
        '
        Me.txt_gulung_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_2.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_2.Name = "txt_gulung_2"
        Me.txt_gulung_2.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_2.TabIndex = 6
        Me.txt_gulung_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_2
        '
        Me.txt_sj_packing_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_2.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_2.Name = "txt_sj_packing_2"
        Me.txt_sj_packing_2.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_2.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "2"
        '
        'txt_jenis_kain_2
        '
        Me.txt_jenis_kain_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_2.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_2.Name = "txt_jenis_kain_2"
        Me.txt_jenis_kain_2.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_2.TabIndex = 19
        '
        'Panel_3
        '
        Me.Panel_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_3.Controls.Add(Me.txt_gudang_3)
        Me.Panel_3.Controls.Add(Me.txt_yards_3)
        Me.Panel_3.Controls.Add(Me.txt_warna_3)
        Me.Panel_3.Controls.Add(Me.txt_total_harga_3)
        Me.Panel_3.Controls.Add(Me.txt_keterangan_3)
        Me.Panel_3.Controls.Add(Me.txt_harga_3)
        Me.Panel_3.Controls.Add(Me.cb_stok_3)
        Me.Panel_3.Controls.Add(Me.txt_gulung_3)
        Me.Panel_3.Controls.Add(Me.txt_sj_packing_3)
        Me.Panel_3.Controls.Add(Me.Label6)
        Me.Panel_3.Controls.Add(Me.txt_jenis_kain_3)
        Me.Panel_3.Location = New System.Drawing.Point(-3, 261)
        Me.Panel_3.Name = "Panel_3"
        Me.Panel_3.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_3.TabIndex = 94
        '
        'txt_gudang_3
        '
        Me.txt_gudang_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_3.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_3.Name = "txt_gudang_3"
        Me.txt_gudang_3.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_3.TabIndex = 4
        '
        'txt_yards_3
        '
        Me.txt_yards_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_3.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_3.Name = "txt_yards_3"
        Me.txt_yards_3.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_3.TabIndex = 90
        Me.txt_yards_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_3
        '
        Me.txt_warna_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_3.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_3.Name = "txt_warna_3"
        Me.txt_warna_3.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_3.TabIndex = 45
        '
        'txt_total_harga_3
        '
        Me.txt_total_harga_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_3.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_3.Name = "txt_total_harga_3"
        Me.txt_total_harga_3.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_3.TabIndex = 8
        Me.txt_total_harga_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_3
        '
        Me.txt_keterangan_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_3.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_3.Name = "txt_keterangan_3"
        Me.txt_keterangan_3.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_3.TabIndex = 7
        '
        'txt_harga_3
        '
        Me.txt_harga_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_3.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_3.Name = "txt_harga_3"
        Me.txt_harga_3.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_3.TabIndex = 89
        Me.txt_harga_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_3
        '
        Me.cb_stok_3.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_3.FormattingEnabled = True
        Me.cb_stok_3.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_3.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_3.Name = "cb_stok_3"
        Me.cb_stok_3.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_3.TabIndex = 43
        Me.cb_stok_3.Text = "-- Pilih Stok --"
        '
        'txt_gulung_3
        '
        Me.txt_gulung_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_3.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_3.Name = "txt_gulung_3"
        Me.txt_gulung_3.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_3.TabIndex = 6
        Me.txt_gulung_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_3
        '
        Me.txt_sj_packing_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_3.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_3.Name = "txt_sj_packing_3"
        Me.txt_sj_packing_3.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_3.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 11)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(13, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "3"
        '
        'txt_jenis_kain_3
        '
        Me.txt_jenis_kain_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_3.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_3.Name = "txt_jenis_kain_3"
        Me.txt_jenis_kain_3.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_3.TabIndex = 19
        '
        'Panel_4
        '
        Me.Panel_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_4.Controls.Add(Me.txt_gudang_4)
        Me.Panel_4.Controls.Add(Me.txt_yards_4)
        Me.Panel_4.Controls.Add(Me.txt_warna_4)
        Me.Panel_4.Controls.Add(Me.txt_total_harga_4)
        Me.Panel_4.Controls.Add(Me.txt_keterangan_4)
        Me.Panel_4.Controls.Add(Me.txt_harga_4)
        Me.Panel_4.Controls.Add(Me.cb_stok_4)
        Me.Panel_4.Controls.Add(Me.txt_gulung_4)
        Me.Panel_4.Controls.Add(Me.txt_sj_packing_4)
        Me.Panel_4.Controls.Add(Me.Label7)
        Me.Panel_4.Controls.Add(Me.txt_jenis_kain_4)
        Me.Panel_4.Location = New System.Drawing.Point(-3, 296)
        Me.Panel_4.Name = "Panel_4"
        Me.Panel_4.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_4.TabIndex = 95
        '
        'txt_gudang_4
        '
        Me.txt_gudang_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_4.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_4.Name = "txt_gudang_4"
        Me.txt_gudang_4.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_4.TabIndex = 4
        '
        'txt_yards_4
        '
        Me.txt_yards_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_4.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_4.Name = "txt_yards_4"
        Me.txt_yards_4.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_4.TabIndex = 90
        Me.txt_yards_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_4
        '
        Me.txt_warna_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_4.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_4.Name = "txt_warna_4"
        Me.txt_warna_4.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_4.TabIndex = 45
        '
        'txt_total_harga_4
        '
        Me.txt_total_harga_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_4.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_4.Name = "txt_total_harga_4"
        Me.txt_total_harga_4.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_4.TabIndex = 8
        Me.txt_total_harga_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_4
        '
        Me.txt_keterangan_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_4.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_4.Name = "txt_keterangan_4"
        Me.txt_keterangan_4.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_4.TabIndex = 7
        '
        'txt_harga_4
        '
        Me.txt_harga_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_4.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_4.Name = "txt_harga_4"
        Me.txt_harga_4.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_4.TabIndex = 89
        Me.txt_harga_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_4
        '
        Me.cb_stok_4.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_4.FormattingEnabled = True
        Me.cb_stok_4.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_4.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_4.Name = "cb_stok_4"
        Me.cb_stok_4.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_4.TabIndex = 43
        Me.cb_stok_4.Text = "-- Pilih Stok --"
        '
        'txt_gulung_4
        '
        Me.txt_gulung_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_4.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_4.Name = "txt_gulung_4"
        Me.txt_gulung_4.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_4.TabIndex = 6
        Me.txt_gulung_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_4
        '
        Me.txt_sj_packing_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_4.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_4.Name = "txt_sj_packing_4"
        Me.txt_sj_packing_4.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_4.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(14, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(13, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "4"
        '
        'txt_jenis_kain_4
        '
        Me.txt_jenis_kain_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_4.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_4.Name = "txt_jenis_kain_4"
        Me.txt_jenis_kain_4.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_4.TabIndex = 19
        '
        'Panel_5
        '
        Me.Panel_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_5.Controls.Add(Me.txt_gudang_5)
        Me.Panel_5.Controls.Add(Me.txt_yards_5)
        Me.Panel_5.Controls.Add(Me.txt_warna_5)
        Me.Panel_5.Controls.Add(Me.txt_total_harga_5)
        Me.Panel_5.Controls.Add(Me.txt_keterangan_5)
        Me.Panel_5.Controls.Add(Me.txt_harga_5)
        Me.Panel_5.Controls.Add(Me.cb_stok_5)
        Me.Panel_5.Controls.Add(Me.txt_gulung_5)
        Me.Panel_5.Controls.Add(Me.txt_sj_packing_5)
        Me.Panel_5.Controls.Add(Me.Label8)
        Me.Panel_5.Controls.Add(Me.txt_jenis_kain_5)
        Me.Panel_5.Location = New System.Drawing.Point(-3, 331)
        Me.Panel_5.Name = "Panel_5"
        Me.Panel_5.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_5.TabIndex = 96
        '
        'txt_gudang_5
        '
        Me.txt_gudang_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_5.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_5.Name = "txt_gudang_5"
        Me.txt_gudang_5.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_5.TabIndex = 4
        '
        'txt_yards_5
        '
        Me.txt_yards_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_5.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_5.Name = "txt_yards_5"
        Me.txt_yards_5.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_5.TabIndex = 90
        Me.txt_yards_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_5
        '
        Me.txt_warna_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_5.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_5.Name = "txt_warna_5"
        Me.txt_warna_5.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_5.TabIndex = 45
        '
        'txt_total_harga_5
        '
        Me.txt_total_harga_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_5.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_5.Name = "txt_total_harga_5"
        Me.txt_total_harga_5.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_5.TabIndex = 8
        Me.txt_total_harga_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_5
        '
        Me.txt_keterangan_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_5.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_5.Name = "txt_keterangan_5"
        Me.txt_keterangan_5.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_5.TabIndex = 7
        '
        'txt_harga_5
        '
        Me.txt_harga_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_5.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_5.Name = "txt_harga_5"
        Me.txt_harga_5.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_5.TabIndex = 89
        Me.txt_harga_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_5
        '
        Me.cb_stok_5.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_5.FormattingEnabled = True
        Me.cb_stok_5.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_5.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_5.Name = "cb_stok_5"
        Me.cb_stok_5.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_5.TabIndex = 43
        Me.cb_stok_5.Text = "-- Pilih Stok --"
        '
        'txt_gulung_5
        '
        Me.txt_gulung_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_5.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_5.Name = "txt_gulung_5"
        Me.txt_gulung_5.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_5.TabIndex = 6
        Me.txt_gulung_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_5
        '
        Me.txt_sj_packing_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_5.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_5.Name = "txt_sj_packing_5"
        Me.txt_sj_packing_5.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_5.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(13, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "5"
        '
        'txt_jenis_kain_5
        '
        Me.txt_jenis_kain_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_5.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_5.Name = "txt_jenis_kain_5"
        Me.txt_jenis_kain_5.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_5.TabIndex = 19
        '
        'Panel_6
        '
        Me.Panel_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_6.Controls.Add(Me.txt_gudang_6)
        Me.Panel_6.Controls.Add(Me.txt_yards_6)
        Me.Panel_6.Controls.Add(Me.txt_warna_6)
        Me.Panel_6.Controls.Add(Me.txt_total_harga_6)
        Me.Panel_6.Controls.Add(Me.txt_keterangan_6)
        Me.Panel_6.Controls.Add(Me.txt_harga_6)
        Me.Panel_6.Controls.Add(Me.cb_stok_6)
        Me.Panel_6.Controls.Add(Me.txt_gulung_6)
        Me.Panel_6.Controls.Add(Me.txt_sj_packing_6)
        Me.Panel_6.Controls.Add(Me.Label9)
        Me.Panel_6.Controls.Add(Me.txt_jenis_kain_6)
        Me.Panel_6.Location = New System.Drawing.Point(-3, 366)
        Me.Panel_6.Name = "Panel_6"
        Me.Panel_6.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_6.TabIndex = 97
        '
        'txt_gudang_6
        '
        Me.txt_gudang_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_6.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_6.Name = "txt_gudang_6"
        Me.txt_gudang_6.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_6.TabIndex = 4
        '
        'txt_yards_6
        '
        Me.txt_yards_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_6.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_6.Name = "txt_yards_6"
        Me.txt_yards_6.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_6.TabIndex = 90
        Me.txt_yards_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_6
        '
        Me.txt_warna_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_6.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_6.Name = "txt_warna_6"
        Me.txt_warna_6.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_6.TabIndex = 45
        '
        'txt_total_harga_6
        '
        Me.txt_total_harga_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_6.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_6.Name = "txt_total_harga_6"
        Me.txt_total_harga_6.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_6.TabIndex = 8
        Me.txt_total_harga_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_6
        '
        Me.txt_keterangan_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_6.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_6.Name = "txt_keterangan_6"
        Me.txt_keterangan_6.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_6.TabIndex = 7
        '
        'txt_harga_6
        '
        Me.txt_harga_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_6.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_6.Name = "txt_harga_6"
        Me.txt_harga_6.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_6.TabIndex = 89
        Me.txt_harga_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_6
        '
        Me.cb_stok_6.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_6.FormattingEnabled = True
        Me.cb_stok_6.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_6.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_6.Name = "cb_stok_6"
        Me.cb_stok_6.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_6.TabIndex = 43
        Me.cb_stok_6.Text = "-- Pilih Stok --"
        '
        'txt_gulung_6
        '
        Me.txt_gulung_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_6.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_6.Name = "txt_gulung_6"
        Me.txt_gulung_6.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_6.TabIndex = 6
        Me.txt_gulung_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_6
        '
        Me.txt_sj_packing_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_6.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_6.Name = "txt_sj_packing_6"
        Me.txt_sj_packing_6.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_6.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(13, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "6"
        '
        'txt_jenis_kain_6
        '
        Me.txt_jenis_kain_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_6.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_6.Name = "txt_jenis_kain_6"
        Me.txt_jenis_kain_6.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_6.TabIndex = 19
        '
        'Panel_7
        '
        Me.Panel_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_7.Controls.Add(Me.txt_gudang_7)
        Me.Panel_7.Controls.Add(Me.txt_yards_7)
        Me.Panel_7.Controls.Add(Me.txt_warna_7)
        Me.Panel_7.Controls.Add(Me.txt_total_harga_7)
        Me.Panel_7.Controls.Add(Me.txt_keterangan_7)
        Me.Panel_7.Controls.Add(Me.txt_harga_7)
        Me.Panel_7.Controls.Add(Me.cb_stok_7)
        Me.Panel_7.Controls.Add(Me.txt_gulung_7)
        Me.Panel_7.Controls.Add(Me.txt_sj_packing_7)
        Me.Panel_7.Controls.Add(Me.Label10)
        Me.Panel_7.Controls.Add(Me.txt_jenis_kain_7)
        Me.Panel_7.Location = New System.Drawing.Point(-3, 401)
        Me.Panel_7.Name = "Panel_7"
        Me.Panel_7.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_7.TabIndex = 98
        '
        'txt_gudang_7
        '
        Me.txt_gudang_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_7.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_7.Name = "txt_gudang_7"
        Me.txt_gudang_7.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_7.TabIndex = 4
        '
        'txt_yards_7
        '
        Me.txt_yards_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_7.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_7.Name = "txt_yards_7"
        Me.txt_yards_7.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_7.TabIndex = 90
        Me.txt_yards_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_7
        '
        Me.txt_warna_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_7.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_7.Name = "txt_warna_7"
        Me.txt_warna_7.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_7.TabIndex = 45
        '
        'txt_total_harga_7
        '
        Me.txt_total_harga_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_7.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_7.Name = "txt_total_harga_7"
        Me.txt_total_harga_7.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_7.TabIndex = 8
        Me.txt_total_harga_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_7
        '
        Me.txt_keterangan_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_7.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_7.Name = "txt_keterangan_7"
        Me.txt_keterangan_7.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_7.TabIndex = 7
        '
        'txt_harga_7
        '
        Me.txt_harga_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_7.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_7.Name = "txt_harga_7"
        Me.txt_harga_7.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_7.TabIndex = 89
        Me.txt_harga_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_7
        '
        Me.cb_stok_7.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_7.FormattingEnabled = True
        Me.cb_stok_7.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_7.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_7.Name = "cb_stok_7"
        Me.cb_stok_7.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_7.TabIndex = 43
        Me.cb_stok_7.Text = "-- Pilih Stok --"
        '
        'txt_gulung_7
        '
        Me.txt_gulung_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_7.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_7.Name = "txt_gulung_7"
        Me.txt_gulung_7.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_7.TabIndex = 6
        Me.txt_gulung_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_7
        '
        Me.txt_sj_packing_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_7.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_7.Name = "txt_sj_packing_7"
        Me.txt_sj_packing_7.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_7.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 11)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(13, 13)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "7"
        '
        'txt_jenis_kain_7
        '
        Me.txt_jenis_kain_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_7.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_7.Name = "txt_jenis_kain_7"
        Me.txt_jenis_kain_7.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_7.TabIndex = 19
        '
        'Panel_8
        '
        Me.Panel_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_8.Controls.Add(Me.txt_gudang_8)
        Me.Panel_8.Controls.Add(Me.txt_yards_8)
        Me.Panel_8.Controls.Add(Me.txt_warna_8)
        Me.Panel_8.Controls.Add(Me.txt_total_harga_8)
        Me.Panel_8.Controls.Add(Me.txt_keterangan_8)
        Me.Panel_8.Controls.Add(Me.txt_harga_8)
        Me.Panel_8.Controls.Add(Me.cb_stok_8)
        Me.Panel_8.Controls.Add(Me.txt_gulung_8)
        Me.Panel_8.Controls.Add(Me.txt_sj_packing_8)
        Me.Panel_8.Controls.Add(Me.Label11)
        Me.Panel_8.Controls.Add(Me.txt_jenis_kain_8)
        Me.Panel_8.Location = New System.Drawing.Point(-3, 436)
        Me.Panel_8.Name = "Panel_8"
        Me.Panel_8.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_8.TabIndex = 99
        '
        'txt_gudang_8
        '
        Me.txt_gudang_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_8.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_8.Name = "txt_gudang_8"
        Me.txt_gudang_8.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_8.TabIndex = 4
        '
        'txt_yards_8
        '
        Me.txt_yards_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_8.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_8.Name = "txt_yards_8"
        Me.txt_yards_8.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_8.TabIndex = 90
        Me.txt_yards_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_8
        '
        Me.txt_warna_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_8.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_8.Name = "txt_warna_8"
        Me.txt_warna_8.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_8.TabIndex = 45
        '
        'txt_total_harga_8
        '
        Me.txt_total_harga_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_8.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_8.Name = "txt_total_harga_8"
        Me.txt_total_harga_8.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_8.TabIndex = 8
        Me.txt_total_harga_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_8
        '
        Me.txt_keterangan_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_8.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_8.Name = "txt_keterangan_8"
        Me.txt_keterangan_8.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_8.TabIndex = 7
        '
        'txt_harga_8
        '
        Me.txt_harga_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_8.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_8.Name = "txt_harga_8"
        Me.txt_harga_8.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_8.TabIndex = 89
        Me.txt_harga_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_8
        '
        Me.cb_stok_8.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_8.FormattingEnabled = True
        Me.cb_stok_8.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_8.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_8.Name = "cb_stok_8"
        Me.cb_stok_8.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_8.TabIndex = 43
        Me.cb_stok_8.Text = "-- Pilih Stok --"
        '
        'txt_gulung_8
        '
        Me.txt_gulung_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_8.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_8.Name = "txt_gulung_8"
        Me.txt_gulung_8.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_8.TabIndex = 6
        Me.txt_gulung_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_8
        '
        Me.txt_sj_packing_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_8.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_8.Name = "txt_sj_packing_8"
        Me.txt_sj_packing_8.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_8.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(14, 11)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(13, 13)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "8"
        '
        'txt_jenis_kain_8
        '
        Me.txt_jenis_kain_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_8.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_8.Name = "txt_jenis_kain_8"
        Me.txt_jenis_kain_8.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_8.TabIndex = 19
        '
        'Panel_9
        '
        Me.Panel_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_9.Controls.Add(Me.txt_gudang_9)
        Me.Panel_9.Controls.Add(Me.txt_yards_9)
        Me.Panel_9.Controls.Add(Me.txt_warna_9)
        Me.Panel_9.Controls.Add(Me.txt_total_harga_9)
        Me.Panel_9.Controls.Add(Me.txt_keterangan_9)
        Me.Panel_9.Controls.Add(Me.txt_harga_9)
        Me.Panel_9.Controls.Add(Me.cb_stok_9)
        Me.Panel_9.Controls.Add(Me.txt_gulung_9)
        Me.Panel_9.Controls.Add(Me.txt_sj_packing_9)
        Me.Panel_9.Controls.Add(Me.Label13)
        Me.Panel_9.Controls.Add(Me.txt_jenis_kain_9)
        Me.Panel_9.Location = New System.Drawing.Point(-3, 471)
        Me.Panel_9.Name = "Panel_9"
        Me.Panel_9.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_9.TabIndex = 101
        '
        'txt_gudang_9
        '
        Me.txt_gudang_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_9.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_9.Name = "txt_gudang_9"
        Me.txt_gudang_9.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_9.TabIndex = 4
        '
        'txt_yards_9
        '
        Me.txt_yards_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_9.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_9.Name = "txt_yards_9"
        Me.txt_yards_9.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_9.TabIndex = 90
        Me.txt_yards_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_9
        '
        Me.txt_warna_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_9.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_9.Name = "txt_warna_9"
        Me.txt_warna_9.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_9.TabIndex = 45
        '
        'txt_total_harga_9
        '
        Me.txt_total_harga_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_9.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_9.Name = "txt_total_harga_9"
        Me.txt_total_harga_9.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_9.TabIndex = 8
        Me.txt_total_harga_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_9
        '
        Me.txt_keterangan_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_9.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_9.Name = "txt_keterangan_9"
        Me.txt_keterangan_9.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_9.TabIndex = 7
        '
        'txt_harga_9
        '
        Me.txt_harga_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_9.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_9.Name = "txt_harga_9"
        Me.txt_harga_9.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_9.TabIndex = 89
        Me.txt_harga_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_9
        '
        Me.cb_stok_9.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_9.FormattingEnabled = True
        Me.cb_stok_9.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_9.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_9.Name = "cb_stok_9"
        Me.cb_stok_9.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_9.TabIndex = 43
        Me.cb_stok_9.Text = "-- Pilih Stok --"
        '
        'txt_gulung_9
        '
        Me.txt_gulung_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_9.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_9.Name = "txt_gulung_9"
        Me.txt_gulung_9.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_9.TabIndex = 6
        Me.txt_gulung_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_9
        '
        Me.txt_sj_packing_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_9.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_9.Name = "txt_sj_packing_9"
        Me.txt_sj_packing_9.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_9.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(14, 11)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(13, 13)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "9"
        '
        'txt_jenis_kain_9
        '
        Me.txt_jenis_kain_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_9.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_9.Name = "txt_jenis_kain_9"
        Me.txt_jenis_kain_9.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_9.TabIndex = 19
        '
        'Panel_10
        '
        Me.Panel_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_10.Controls.Add(Me.txt_gudang_10)
        Me.Panel_10.Controls.Add(Me.txt_yards_10)
        Me.Panel_10.Controls.Add(Me.txt_warna_10)
        Me.Panel_10.Controls.Add(Me.txt_total_harga_10)
        Me.Panel_10.Controls.Add(Me.txt_keterangan_10)
        Me.Panel_10.Controls.Add(Me.txt_harga_10)
        Me.Panel_10.Controls.Add(Me.cb_stok_10)
        Me.Panel_10.Controls.Add(Me.txt_gulung_10)
        Me.Panel_10.Controls.Add(Me.txt_sj_packing_10)
        Me.Panel_10.Controls.Add(Me.Label14)
        Me.Panel_10.Controls.Add(Me.txt_jenis_kain_10)
        Me.Panel_10.Location = New System.Drawing.Point(-3, 506)
        Me.Panel_10.Name = "Panel_10"
        Me.Panel_10.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_10.TabIndex = 102
        '
        'txt_gudang_10
        '
        Me.txt_gudang_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang_10.Location = New System.Drawing.Point(254, 7)
        Me.txt_gudang_10.Name = "txt_gudang_10"
        Me.txt_gudang_10.Size = New System.Drawing.Size(138, 20)
        Me.txt_gudang_10.TabIndex = 4
        '
        'txt_yards_10
        '
        Me.txt_yards_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards_10.Location = New System.Drawing.Point(734, 7)
        Me.txt_yards_10.Name = "txt_yards_10"
        Me.txt_yards_10.Size = New System.Drawing.Size(92, 20)
        Me.txt_yards_10.TabIndex = 90
        Me.txt_yards_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna_10
        '
        Me.txt_warna_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_10.Location = New System.Drawing.Point(540, 7)
        Me.txt_warna_10.Name = "txt_warna_10"
        Me.txt_warna_10.Size = New System.Drawing.Size(112, 20)
        Me.txt_warna_10.TabIndex = 45
        '
        'txt_total_harga_10
        '
        Me.txt_total_harga_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga_10.Location = New System.Drawing.Point(965, 7)
        Me.txt_total_harga_10.Name = "txt_total_harga_10"
        Me.txt_total_harga_10.Size = New System.Drawing.Size(142, 20)
        Me.txt_total_harga_10.TabIndex = 8
        Me.txt_total_harga_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_keterangan_10
        '
        Me.txt_keterangan_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_10.Location = New System.Drawing.Point(1118, 7)
        Me.txt_keterangan_10.Name = "txt_keterangan_10"
        Me.txt_keterangan_10.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_10.TabIndex = 7
        '
        'txt_harga_10
        '
        Me.txt_harga_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga_10.Location = New System.Drawing.Point(837, 7)
        Me.txt_harga_10.Name = "txt_harga_10"
        Me.txt_harga_10.Size = New System.Drawing.Size(117, 20)
        Me.txt_harga_10.TabIndex = 89
        Me.txt_harga_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cb_stok_10
        '
        Me.cb_stok_10.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok_10.FormattingEnabled = True
        Me.cb_stok_10.Items.AddRange(New Object() {"Grade A", "Grade B", "Claim Jadi"})
        Me.cb_stok_10.Location = New System.Drawing.Point(38, 6)
        Me.cb_stok_10.Name = "cb_stok_10"
        Me.cb_stok_10.Size = New System.Drawing.Size(97, 21)
        Me.cb_stok_10.TabIndex = 43
        Me.cb_stok_10.Text = "-- Pilih Stok --"
        '
        'txt_gulung_10
        '
        Me.txt_gulung_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_10.Location = New System.Drawing.Point(663, 7)
        Me.txt_gulung_10.Name = "txt_gulung_10"
        Me.txt_gulung_10.Size = New System.Drawing.Size(60, 20)
        Me.txt_gulung_10.TabIndex = 6
        Me.txt_gulung_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_sj_packing_10
        '
        Me.txt_sj_packing_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing_10.Location = New System.Drawing.Point(146, 7)
        Me.txt_sj_packing_10.Name = "txt_sj_packing_10"
        Me.txt_sj_packing_10.Size = New System.Drawing.Size(97, 20)
        Me.txt_sj_packing_10.TabIndex = 1
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(8, 11)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(19, 13)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "10"
        '
        'txt_jenis_kain_10
        '
        Me.txt_jenis_kain_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_10.Location = New System.Drawing.Point(403, 7)
        Me.txt_jenis_kain_10.Name = "txt_jenis_kain_10"
        Me.txt_jenis_kain_10.Size = New System.Drawing.Size(126, 20)
        Me.txt_jenis_kain_10.TabIndex = 19
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Label15)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_10)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_9)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_8)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_7)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_6)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_5)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_4)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_3)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_2)
        Me.Panel9.Controls.Add(Me.txt_id_piutang_1)
        Me.Panel9.Location = New System.Drawing.Point(435, 31)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(76, 134)
        Me.Panel9.TabIndex = 119
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(11, 114)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(55, 13)
        Me.Label15.TabIndex = 10
        Me.Label15.Text = "Id Piutang"
        '
        'txt_id_piutang_10
        '
        Me.txt_id_piutang_10.Location = New System.Drawing.Point(38, 91)
        Me.txt_id_piutang_10.Name = "txt_id_piutang_10"
        Me.txt_id_piutang_10.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_10.TabIndex = 9
        '
        'txt_id_piutang_9
        '
        Me.txt_id_piutang_9.Location = New System.Drawing.Point(38, 71)
        Me.txt_id_piutang_9.Name = "txt_id_piutang_9"
        Me.txt_id_piutang_9.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_9.TabIndex = 8
        '
        'txt_id_piutang_8
        '
        Me.txt_id_piutang_8.Location = New System.Drawing.Point(38, 51)
        Me.txt_id_piutang_8.Name = "txt_id_piutang_8"
        Me.txt_id_piutang_8.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_8.TabIndex = 7
        '
        'txt_id_piutang_7
        '
        Me.txt_id_piutang_7.Location = New System.Drawing.Point(38, 31)
        Me.txt_id_piutang_7.Name = "txt_id_piutang_7"
        Me.txt_id_piutang_7.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_7.TabIndex = 6
        '
        'txt_id_piutang_6
        '
        Me.txt_id_piutang_6.Location = New System.Drawing.Point(38, 11)
        Me.txt_id_piutang_6.Name = "txt_id_piutang_6"
        Me.txt_id_piutang_6.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_6.TabIndex = 5
        '
        'txt_id_piutang_5
        '
        Me.txt_id_piutang_5.Location = New System.Drawing.Point(8, 91)
        Me.txt_id_piutang_5.Name = "txt_id_piutang_5"
        Me.txt_id_piutang_5.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_5.TabIndex = 4
        '
        'txt_id_piutang_4
        '
        Me.txt_id_piutang_4.Location = New System.Drawing.Point(8, 71)
        Me.txt_id_piutang_4.Name = "txt_id_piutang_4"
        Me.txt_id_piutang_4.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_4.TabIndex = 3
        '
        'txt_id_piutang_3
        '
        Me.txt_id_piutang_3.Location = New System.Drawing.Point(8, 51)
        Me.txt_id_piutang_3.Name = "txt_id_piutang_3"
        Me.txt_id_piutang_3.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_3.TabIndex = 2
        '
        'txt_id_piutang_2
        '
        Me.txt_id_piutang_2.Location = New System.Drawing.Point(8, 31)
        Me.txt_id_piutang_2.Name = "txt_id_piutang_2"
        Me.txt_id_piutang_2.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_2.TabIndex = 1
        '
        'txt_id_piutang_1
        '
        Me.txt_id_piutang_1.Location = New System.Drawing.Point(8, 11)
        Me.txt_id_piutang_1.Name = "txt_id_piutang_1"
        Me.txt_id_piutang_1.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_piutang_1.TabIndex = 0
        '
        'form_input_surat_jalan_penjualan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1336, 618)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel_10)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel_9)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel_8)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel_7)
        Me.Controls.Add(Me.Panel_6)
        Me.Controls.Add(Me.Panel_5)
        Me.Controls.Add(Me.Panel_4)
        Me.Controls.Add(Me.Panel_3)
        Me.Controls.Add(Me.Panel_2)
        Me.Controls.Add(Me.Panel_1)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.Panel3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_surat_jalan_penjualan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM INPUT SURAT JALAN PENJUALAN BARU"
        Me.Panel3.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel_1.ResumeLayout(False)
        Me.Panel_1.PerformLayout()
        Me.Panel_2.ResumeLayout(False)
        Me.Panel_2.PerformLayout()
        Me.Panel_3.ResumeLayout(False)
        Me.Panel_3.PerformLayout()
        Me.Panel_4.ResumeLayout(False)
        Me.Panel_4.PerformLayout()
        Me.Panel_5.ResumeLayout(False)
        Me.Panel_5.PerformLayout()
        Me.Panel_6.ResumeLayout(False)
        Me.Panel_6.PerformLayout()
        Me.Panel_7.ResumeLayout(False)
        Me.Panel_7.PerformLayout()
        Me.Panel_8.ResumeLayout(False)
        Me.Panel_8.PerformLayout()
        Me.Panel_9.ResumeLayout(False)
        Me.Panel_9.PerformLayout()
        Me.Panel_10.ResumeLayout(False)
        Me.Panel_10.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt_warna_1 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_1 As System.Windows.Forms.ComboBox
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_sj_packing_1 As System.Windows.Forms.TextBox
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_jenis_kain_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_penjualan As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btn_batal_10 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_9 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_8 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_7 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_6 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_5 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_4 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_3 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_2 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_1 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_10 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_10 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_9 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_9 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_9 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_8 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_8 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_8 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_7 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_7 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_7 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_6 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_6 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_6 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_5 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_5 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_5 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_4 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_4 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_4 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_3 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_3 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_3 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_2 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_1 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_1 As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txt_id_grey_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Panel_1 As System.Windows.Forms.Panel
    Friend WithEvents txt_total_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_1 As System.Windows.Forms.Label
    Friend WithEvents txt_yards_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txt_id_penjualan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_penjualan_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_id_claim_jadi_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_claim_jadi_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_id_grade_b_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_b_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_id_grade_a_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grade_a_1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel_2 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_2 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_3 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_3 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_3 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_3 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_4 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_4 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_4 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_4 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_5 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_5 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_5 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_5 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_6 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_6 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_6 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_6 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_7 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_7 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_7 As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_7 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_8 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_8 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_8 As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_8 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_9 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_9 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_9 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_9 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_9 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_10 As System.Windows.Forms.Panel
    Friend WithEvents txt_gudang_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_10 As System.Windows.Forms.TextBox
    Friend WithEvents cb_stok_10 As System.Windows.Forms.ComboBox
    Friend WithEvents txt_gulung_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_sj_packing_10 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txt_jenis_kain_10 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents dtp_jatuh_tempo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_id_piutang_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_piutang_1 As System.Windows.Forms.TextBox
End Class
