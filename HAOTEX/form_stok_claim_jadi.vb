﻿Imports MySql.Data.MySqlClient

Public Class form_stok_claim_jadi

    Private Sub form_stok_claim_jadi_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If txt_form.Text = "form_input_surat_jalan_penjualan_1" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_1.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_2" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_2.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_3" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_3.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_4" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_4.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_5" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_5.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_6" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_6.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_7" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_7.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_8" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_8.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_9" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_9.Focus()
        ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_10" Then
            form_input_surat_jalan_penjualan.Focus()
            form_input_surat_jalan_penjualan.txt_yards_10.Focus()
        End If
    End Sub
    Private Sub form_stok_claim_jadi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Gudang"
        dgv1.Columns(2).HeaderText = "SJ Packing"
        dgv1.Columns(3).HeaderText = "Customer"
        dgv1.Columns(4).HeaderText = "Jenis Kain"
        dgv1.Columns(5).HeaderText = "Warna"
        dgv1.Columns(6).HeaderText = "Gulung"
        dgv1.Columns(7).HeaderText = "Stok"
        dgv1.Columns(8).HeaderText = "Satuan"
        dgv1.Columns(0).Width = 70
        dgv1.Columns(1).Width = 120
        dgv1.Columns(2).Width = 100
        dgv1.Columns(3).Width = 70
        dgv1.Columns(4).Width = 130
        dgv1.Columns(5).Width = 150
        dgv1.Columns(6).Width = 100
        dgv1.Columns(7).Width = 70
        dgv1.Columns(8).Width = 80
        dgv1.Columns(9).Width = 100
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Format = "N"
        dgv1.Columns(9).DefaultCellStyle.Format = "C"
        dgv1.Columns(9).Visible = False
        dgv1.Columns(10).Visible = False
        dgv1.Columns(11).Visible = False
        dgv1.Columns(12).Visible = False
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Harga FROM tbstokclaimjadi WHERE NOT Stok=0 ORDER BY Customer"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokclaimjadi")
                            dgv1.DataSource = dsx.Tables("tbstokclaimjadi")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_customer.GotFocus
        If txt_cari_customer.Text = "< Customer >" Then
            txt_cari_customer.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_customer.LostFocus
        If txt_cari_customer.Text = "" Then
            txt_cari_customer.Text = "< Customer >"
        End If
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_customer.TextChanged
        Try
            If txt_cari_customer.Text = "< Customer >" Then

            ElseIf txt_cari_customer.Text = "" Then
                If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                    Call isidgv()
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Harga FROM tbstokclaimjadi WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_Kain"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimjadi")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimjadi")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If
            Else
                If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Harga FROM tbstokclaimjadi WHERE Customer like '%" & txt_cari_customer.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimjadi")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimjadi")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Harga FROM tbstokclaimjadi WHERE Customer like '%" & txt_cari_customer.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimjadi")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimjadi")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_jenis_kain.Text = "" Then
                If txt_cari_customer.Text = "< Customer >" Then
                    Call isidgv()
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Harga FROM tbstokclaimjadi WHERE Customer like '%" & txt_cari_customer.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimjadi")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimjadi")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If
            Else
                If txt_cari_customer.Text = "< Customer >" Then
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Harga FROM tbstokclaimjadi WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_Kain"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimjadi")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimjadi")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Stok,Satuan,Id_Grey,Id_SJ_Packing,Id_Claim_Jadi,Harga FROM tbstokclaimjadi WHERE Customer like '%" & txt_cari_customer.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Customer"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using dax As New MySqlDataAdapter
                                dax.SelectCommand = cmdx
                                Using dsx As New DataSet
                                    dax.Fill(dsx, "tbstokclaimjadi")
                                    dgv1.DataSource = dsx.Tables("tbstokclaimjadi")
                                    Call headertabel()
                                End Using
                            End Using
                        End Using
                    End Using
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgv1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv1.MouseDoubleClick
        Try
            If dgv1.RowCount = 0 Then
                MsgBox("Tidak Terdapat Data")
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_1" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_1.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_1.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_1.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_1.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_1.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_1.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_1.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_1.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_1.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_1.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_1.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_1.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_2" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_2.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_2.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_2.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_2.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_2.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_2.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_2.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_2.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_2.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_2.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_2.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_2.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_3" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_3.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_3.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_3.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_3.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_3.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_3.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_3.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_3.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_3.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_3.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_3.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_3.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_4" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_4.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_4.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_4.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_4.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_4.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_4.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_4.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_4.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_4.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_4.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_4.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_4.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_5" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_5.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_5.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_5.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_5.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_5.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_5.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_5.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_5.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_5.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_5.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_5.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_5.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_6" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_6.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_6.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_6.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_6.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_6.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_6.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_6.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_6.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_6.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_6.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_6.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_6.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_7" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_7.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_7.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_7.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_7.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_7.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_7.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_7.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_7.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_7.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_7.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_7.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_7.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_8" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_8.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_8.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_8.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_8.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_8.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_8.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_8.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_8.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_8.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_8.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_8.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_8.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_9" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_9.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_9.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_9.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_9.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_9.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_9.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_9.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_9.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_9.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_9.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_9.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_9.Focus()
                End With
                Me.Close()
            ElseIf txt_form.Text = "form_input_surat_jalan_penjualan_10" Then
                form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
                form_input_surat_jalan_penjualan.Show()
                form_input_surat_jalan_penjualan.Focus()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_surat_jalan_penjualan.txt_gudang_10.Text = .Cells(1).Value.ToString
                    form_input_surat_jalan_penjualan.txt_sj_packing_10.Text = .Cells(2).Value.ToString
                    form_input_surat_jalan_penjualan.txt_jenis_kain_10.Text = .Cells(4).Value.ToString
                    form_input_surat_jalan_penjualan.txt_warna_10.Text = .Cells(5).Value.ToString
                    form_input_surat_jalan_penjualan.txt_gulung_10.Text = .Cells(6).Value.ToString
                    form_input_surat_jalan_penjualan.txt_yards_10.Text = .Cells(7).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grey_10.Text = .Cells(9).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_claim_jadi_10.Text = .Cells(11).Value.ToString
                    form_input_surat_jalan_penjualan.txt_id_grade_b_10.Text = ""
                    form_input_surat_jalan_penjualan.txt_id_grade_a_10.Text = ""
                    form_input_surat_jalan_penjualan.txt_harga_10.Text = Math.Round(.Cells(12).Value + (0.05 * .Cells(12).Value), 0)
                    form_input_surat_jalan_penjualan.txt_yards_10.Focus()
                End With
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class