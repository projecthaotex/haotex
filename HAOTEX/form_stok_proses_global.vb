﻿Imports MySql.Data.MySqlClient

Public Class form_stok_proses_global
    Private Sub form_stok_proses_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertabel_1()
        dgv1.Columns(0).HeaderText = "Jenis Kain"
        dgv1.Columns(1).HeaderText = "STOK"
        dgv1.Columns(0).Width = 140
        dgv1.Columns(1).Width = 120
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(1).DefaultCellStyle.Format = "N"
    End Sub
    Private Sub headertabel_2()
        dgv2.Columns(0).HeaderText = "Jenis Kain"
        dgv2.Columns(1).HeaderText = "STOK"
        dgv2.Columns(0).Width = 140
        dgv2.Columns(1).Width = 120
        dgv2.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv2.Columns(1).DefaultCellStyle.Format = "N"
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Jenis_Kain, SUM(stok) AS STOK FROM tbstokprosesgrey WHERE satuan='Meter' GROUP BY jenis_kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokprosesgrey")
                            dgv1.DataSource = dsx.Tables("tbstokprosesgrey")
                            Call headertabel_1()
                        End Using
                    End Using
                End Using
                Dim sqly As String = "SELECT Jenis_Kain, SUM(stok) AS STOK FROM tbstokprosesgrey WHERE satuan='Yard' GROUP BY jenis_kain"
                Using cmdx As New MySqlCommand(sqly, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokprosesgrey")
                            dgv2.DataSource = dsx.Tables("tbstokprosesgrey")
                            Call headertabel_2()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub hitungjumlah()
        Dim totalmeter, totalyard, totalstok As Double
        totalmeter = 0
        totalyard = 0
        totalstok = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            totalmeter = Math.Round(totalmeter + Val(dgv1.Rows(i).Cells(1).Value), 0)
        Next
        For i As Integer = 0 To dgv2.Rows.Count - 1
            totalyard = Math.Round(totalyard + Val(dgv2.Rows(i).Cells(1).Value), 0)
        Next
        If cb_satuan.Text = "Meter" Then
            totalstok = Math.Round(totalmeter + (totalyard * 0.9144), 0)
        ElseIf cb_satuan.Text = "Yard" Then
            totalstok = Math.Round((totalmeter * 1.0936) + totalyard, 0)
        End If
        txt_total_meter.Text = totalmeter
        txt_total_yard.Text = totalyard
        txt_total_stok.Text = totalstok
    End Sub
    Private Sub txt_total_meter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_meter.TextChanged
        If txt_total_meter.Text <> String.Empty Then
            Dim temp As String = txt_total_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_meter.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_meter.Select(txt_total_meter.Text.Length, 0)
            ElseIf txt_total_meter.Text = "-"c Then

            Else
                txt_total_meter.Text = CDec(temp).ToString("N0")
                txt_total_meter.Select(txt_total_meter.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_yard.TextChanged
        If txt_total_yard.Text <> String.Empty Then
            Dim temp As String = txt_total_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            ElseIf txt_total_yard.Text = "-"c Then

            Else
                txt_total_yard.Text = CDec(temp).ToString("N0")
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_stok_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_stok.TextChanged
        If txt_total_stok.Text <> String.Empty Then
            Dim temp As String = txt_total_stok.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_stok.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_stok.Select(txt_total_stok.Text.Length, 0)
            ElseIf txt_total_stok.Text = "-"c Then

            Else
                txt_total_stok.Text = CDec(temp).ToString("N0")
                txt_total_stok.Select(txt_total_stok.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub cb_satuan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan.TextChanged
        Call hitungjumlah()
    End Sub
    Private Sub btn_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_perbarui.Click
        Call isidgv()
        dgv1.Focus()
    End Sub

    Private Sub dgv1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv1.MouseDoubleClick
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data")
        Else
            form_stok_proses_grey.MdiParent = form_menu_utama
            form_stok_proses_grey.Show()
            form_stok_proses_grey.btn_perbarui.Visible = True
            form_stok_proses_grey.txt_cari_gudang.Text = "< Gudang >"
            form_stok_proses_grey.txt_cari_gudang.ReadOnly = False
            form_stok_proses_grey.Focus()
            form_stok_proses_grey.txt_form.Text = ""
            form_stok_proses_grey.dgv1.Focus()
            Dim i As Integer
            i = Me.dgv1.CurrentRow.Index
            With dgv1.Rows.Item(i)
                form_stok_proses_grey.txt_cari_jenis_kain.Text = .Cells(0).Value.ToString
            End With
            Me.Close()
        End If
    End Sub
    Private Sub dgv2_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv2.MouseDoubleClick
        If dgv2.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data")
        Else
            form_stok_proses_grey.MdiParent = form_menu_utama
            form_stok_proses_grey.Show()
            form_stok_proses_grey.btn_perbarui.Visible = True
            form_stok_proses_grey.txt_cari_gudang.Text = "< Gudang >"
            form_stok_proses_grey.txt_cari_gudang.ReadOnly = False
            form_stok_proses_grey.Focus()
            form_stok_proses_grey.txt_form.Text = ""
            form_stok_proses_grey.dgv1.Focus()
            Dim i As Integer
            i = Me.dgv2.CurrentRow.Index
            With dgv2.Rows.Item(i)
                form_stok_proses_grey.txt_cari_jenis_kain.Text = .Cells(0).Value.ToString
            End With
            Me.Close()
        End If
    End Sub
End Class