﻿Imports MySql.Data.MySqlClient

Public Class form_po_packing

    Private Sub form_po_packing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call awal()
        dtp_hari_ini.Text = Today
    End Sub
    Private Sub awal()
        Call isidtpawal()
        Call isidgv()
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(-3)
        dtp_awal.Text = tanggal
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Gudang"
        dgv1.Columns(2).HeaderText = "No PO"
        dgv1.Columns(3).HeaderText = "Tgl SJ Proses"
        dgv1.Columns(4).HeaderText = "SJ Proses"
        dgv1.Columns(5).HeaderText = "Jenis Kain"
        dgv1.Columns(6).HeaderText = "Warna"
        dgv1.Columns(7).HeaderText = "Resep"
        dgv1.Columns(8).HeaderText = "Partai"
        dgv1.Columns(9).HeaderText = "Gulung"
        dgv1.Columns(10).HeaderText = "QTY"
        dgv1.Columns(11).HeaderText = "QTY Keluar"
        dgv1.Columns(12).HeaderText = "Satuan"
        dgv1.Columns(13).HeaderText = "Customer"
        dgv1.Columns(14).HeaderText = "Aksesoris"
        dgv1.Columns(15).HeaderText = "Keterangan"
        dgv1.Columns(16).HeaderText = "ID Grey"
        dgv1.Columns(17).HeaderText = "ID SJ Proses"
        dgv1.Columns(18).HeaderText = "ID PO Packing"
        dgv1.Columns(19).HeaderText = "Status"
        dgv1.Columns(20).HeaderText = "Harga"
        dgv1.RowHeadersWidth = 50
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 140
        dgv1.Columns(2).Width = 100
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 140
        dgv1.Columns(6).Width = 100
        dgv1.Columns(7).Width = 100
        dgv1.Columns(8).Width = 100
        dgv1.Columns(9).Width = 80
        dgv1.Columns(10).Width = 100
        dgv1.Columns(11).Width = 100
        dgv1.Columns(12).Width = 80
        dgv1.Columns(13).Width = 140
        dgv1.Columns(14).Width = 150
        dgv1.Columns(15).Width = 150
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(10).DefaultCellStyle.Format = "N"
        dgv1.Columns(11).DefaultCellStyle.Format = "N"
        dgv1.Columns(3).Visible = False
        dgv1.Columns(16).Visible = False
        dgv1.Columns(17).Visible = False
        dgv1.Columns(18).Visible = False
        dgv1.Columns(19).Visible = False
        dgv1.Columns(20).Visible = False
    End Sub
    Private Sub dgv1_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgv1.CellFormatting
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(19).Value = "Closed" Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Red
            ElseIf dgv1.Rows(i).Cells(19).Value = "Sebagian" Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Blue
            End If
        Next
        dgv1.Rows(e.RowIndex).HeaderCell.Value = CStr(e.RowIndex + 1)
    End Sub
    Private Sub hitungjumlah()
        Dim jumlahtotal, belumsj, keluarsj, jumlahtotalyard, belumsjyard, keluarsjyard As Double
        jumlahtotal = 0
        belumsj = 0
        keluarsj = 0
        jumlahtotalyard = 0
        belumsjyard = 0
        keluarsjyard = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(12).Value = "Meter" Then
                jumlahtotal = jumlahtotal + Val(dgv1.Rows(i).Cells(10).Value)
                keluarsj = keluarsj + Val(dgv1.Rows(i).Cells(11).Value)
                belumsj = belumsj + (Val(dgv1.Rows(i).Cells(10).Value) - Val(dgv1.Rows(i).Cells(11).Value))
            ElseIf dgv1.Rows(i).Cells(12).Value = "Yard" Then
                jumlahtotalyard = jumlahtotalyard + Val(dgv1.Rows(i).Cells(10).Value)
                keluarsjyard = keluarsjyard + Val(dgv1.Rows(i).Cells(11).Value)
                belumsjyard = belumsjyard + (Val(dgv1.Rows(i).Cells(10).Value) - Val(dgv1.Rows(i).Cells(11).Value))
            End If
        Next
        txt_total_qty.Text = jumlahtotal
        txt_belum_sj.Text = belumsj
        txt_keluar_sj.Text = keluarsj
        txt_total_qty_yard.Text = jumlahtotalyard
        txt_belum_sj_yard.Text = belumsjyard
        txt_keluar_sj_yard.Text = keluarsjyard
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Meter_Keluar,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Harga FROM tbpopacking WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpopacking")
                            dgv1.DataSource = dsx.Tables("tbpopacking")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_awal.Text = dtp_akhir.Text
        End If
        txt_cari_po_packing.Text = "< No PO >"
        txt_cari_gudang.Text = "< Gudang >"
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Call isidgv()
    End Sub
    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
        txt_cari_po_packing.Text = "< No PO >"
        txt_cari_gudang.Text = "< Gudang >"
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Call isidgv()
    End Sub
    Private Sub isidgv_po_packing()
        Try
            dtp_po.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Meter_Keluar,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Harga FROM tbpopacking WHERE No_PO LIKE '" & txt_cari_po_packing.Text & "' AND Tanggal='" & dtp_po.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpopacking")
                            dgv1.DataSource = dsx.Tables("tbpopacking")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
            dtp_po.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_po_packing.MdiParent = form_menu_utama
        form_input_po_packing.Show()
        form_input_po_packing.Focus()
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        txt_cari_po_packing.Text = "< No PO >"
        txt_cari_gudang.Text = "< Gudang >"
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Call awal()
        dgv1.Focus()
    End Sub
    Private Sub txt_cari_po_packing_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_po_packing.GotFocus
        If txt_cari_po_packing.Text = "< No PO >" Then
            txt_cari_po_packing.Text = ""
            txt_cari_gudang.Text = "< Gudang >"
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub txt_cari_po_packing_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_po_packing.LostFocus
        If txt_cari_po_packing.Text = "" Then
            txt_cari_po_packing.Text = "< No PO >"
        End If
    End Sub
    Private Sub txt_cari_po_packing_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_po_packing.TextChanged
        If txt_cari_po_packing.Text = "< No PO >" Then

        ElseIf txt_cari_po_packing.Text = "" Then
            Call isidgv()
        Else
            Call isidgv_po_packing()
        End If
    End Sub
    Private Sub dtp_po_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_po.TextChanged
        If txt_cari_po_packing.Text = "" Or txt_cari_po_packing.Text = "< No PO >" Then
        Else
            Call isidgv_po_packing()
        End If
    End Sub

    Private Sub reportpopacking()
        dgv1.Columns(0).DefaultCellStyle.Format = "D"
        dgv1.Columns(3).DefaultCellStyle.Format = "dd/MM"
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
        End With
        tbheaderfooter.Rows.Add(dgv1.Rows(0).Cells(0).FormattedValue, dgv1.Rows(0).Cells(1).Value, dgv1.Rows(0).Cells(2).Value)

        Dim dtreportpoproses As New DataTable
        With dtreportpoproses
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            dtreportpoproses.Rows.Add(row.Index + 1, row.Cells(3).FormattedValue, row.Cells(4).Value, _
                                         row.Cells(5).Value, row.Cells(6).Value, row.Cells(8).Value, row.Cells(9).Value, _
                                         row.Cells(10).FormattedValue, row.Cells(12).Value, _
                                         row.Cells(13).Value, row.Cells(14).Value, row.Cells(15).Value)
        Next
        form_report_po_packing.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_po_packing.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportpoproses
        form_report_po_packing.ShowDialog()
        form_report_po_packing.Dispose()
        dgv1.Columns(0).DefaultCellStyle.Format = Nothing
    End Sub
    Private Sub ts_print_po_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print_po.Click
        If txt_cari_po_packing.Text = "< No PO >" Or txt_cari_po_packing.Text = "" Then
            MsgBox("No PO Packing Belum di INPUT")
            txt_cari_po_packing.Focus()
        ElseIf dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportpopacking()
        End If
    End Sub
    Private Sub reportpopackingall()
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
        End With
        tbheaderfooter.Rows.Add(dtp_awal.Text, dtp_akhir.Text, dtp_hari_ini.Text, txt_total_qty.Text, _
                                txt_belum_sj.Text, txt_keluar_sj.Text, txt_total_qty_yard.Text, _
                                txt_belum_sj_yard.Text, txt_keluar_sj_yard.Text)
        Dim dtreportpoproses As New DataTable
        With dtreportpoproses
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
            .Columns.Add("DataColumn13")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            dtreportpoproses.Rows.Add(row.Cells(0).FormattedValue, row.Cells(1).Value, row.Cells(2).Value, _
                                      row.Cells(4).Value, row.Cells(5).Value, row.Cells(6).Value, row.Cells(7).Value, _
                                      row.Cells(8).Value, row.Cells(9).Value, row.Cells(10).FormattedValue, row.Cells(11).FormattedValue, _
                                      row.Cells(12).Value, row.Cells(13).Value)
        Next
        form_report_po_packing_all.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_po_packing_all.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportpoproses
        form_report_po_packing_all.ShowDialog()
        form_report_po_packing_all.Dispose()
    End Sub
    Private Sub ts_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportpopackingall()
        End If
    End Sub

    Private Sub txt_total_qty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_qty.TextChanged
        If txt_total_qty.Text <> String.Empty Then
            Dim temp As String = txt_total_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_qty.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_qty.Select(txt_total_qty.Text.Length, 0)
            ElseIf txt_total_qty.Text = "-"c Then

            Else
                txt_total_qty.Text = CDec(temp).ToString("N0")
                txt_total_qty.Select(txt_total_qty.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_belum_sj_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_belum_sj.TextChanged
        If txt_belum_sj.Text <> String.Empty Then
            Dim temp As String = txt_belum_sj.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_belum_sj.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_belum_sj.Select(txt_belum_sj.Text.Length, 0)
            ElseIf txt_belum_sj.Text = "-"c Then

            Else
                txt_belum_sj.Text = CDec(temp).ToString("N0")
                txt_belum_sj.Select(txt_belum_sj.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_keluar_sj_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_keluar_sj.TextChanged
        If txt_keluar_sj.Text <> String.Empty Then
            Dim temp As String = txt_keluar_sj.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_keluar_sj.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_keluar_sj.Select(txt_keluar_sj.Text.Length, 0)
            ElseIf txt_keluar_sj.Text = "-"c Then

            Else
                txt_keluar_sj.Text = CDec(temp).ToString("N0")
                txt_keluar_sj.Select(txt_keluar_sj.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_qty_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_qty_yard.TextChanged
        If txt_total_qty_yard.Text <> String.Empty Then
            Dim temp As String = txt_total_qty_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_qty_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_qty_yard.Select(txt_total_qty_yard.Text.Length, 0)
            ElseIf txt_total_qty_yard.Text = "-"c Then

            Else
                txt_total_qty_yard.Text = CDec(temp).ToString("N0")
                txt_total_qty_yard.Select(txt_total_qty_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_belum_sj_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_belum_sj_yard.TextChanged
        If txt_belum_sj_yard.Text <> String.Empty Then
            Dim temp As String = txt_belum_sj_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_belum_sj_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_belum_sj_yard.Select(txt_belum_sj_yard.Text.Length, 0)
            ElseIf txt_belum_sj_yard.Text = "-"c Then

            Else
                txt_belum_sj_yard.Text = CDec(temp).ToString("N0")
                txt_belum_sj_yard.Select(txt_belum_sj_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_keluar_sj_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_keluar_sj_yard.TextChanged
        If txt_keluar_sj_yard.Text <> String.Empty Then
            Dim temp As String = txt_keluar_sj_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_keluar_sj_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_keluar_sj_yard.Select(txt_keluar_sj_yard.Text.Length, 0)
            ElseIf txt_keluar_sj_yard.Text = "-"c Then

            Else
                txt_keluar_sj_yard.Text = CDec(temp).ToString("N0")
                txt_keluar_sj_yard.Select(txt_keluar_sj_yard.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub isidgv_gudang()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Meter_Keluar,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Harga FROM tbpopacking WHERE Gudang LIKE '%" & txt_cari_gudang.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY No_PO"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpopacking")
                            dgv1.DataSource = dsx.Tables("tbpopacking")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_jenis_kain()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Meter_Keluar,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Harga FROM tbpopacking WHERE Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY No_PO"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpopacking")
                            dgv1.DataSource = dsx.Tables("tbpopacking")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_gudang_jenis_kain()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Tanggal_SJ,Asal_SJ,Jenis_Kain,Warna,Resep,Partai,Gulung,Meter,Meter_Keluar,Satuan,Customer,Aksesoris,Keterangan,Id_Grey,Id_SJ_Proses,Id_PO_Packing,STATUS,Harga FROM tbpopacking WHERE Gudang LIKE '%" & txt_cari_gudang.Text & "%' AND Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY No_PO"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpopacking")
                            dgv1.DataSource = dsx.Tables("tbpopacking")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.GotFocus
        If txt_cari_gudang.Text = "< Gudang >" Then
            txt_cari_po_packing.Text = "< No PO >"
            txt_cari_gudang.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.LostFocus
        If txt_cari_gudang.Text = "" Then
            txt_cari_gudang.Text = "< Gudang >"
        End If
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.TextChanged
        Try
            If txt_cari_gudang.Text = "< Gudang >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_gudang.Text = "< Gudang >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jenis_kain()
            ElseIf txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jenis_kain()
            ElseIf Not txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_gudang_jenis_kain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_po_packing.Text = "< No PO >"
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_gudang.Text = "< Gudang >" Then

            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv_jenis_kain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_gudang_jenis_kain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class