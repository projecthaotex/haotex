﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_nama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_frm = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_simpan_baru = New System.Windows.Forms.Button()
        Me.btn_batal = New System.Windows.Forms.Button()
        Me.btn_simpan_tutup = New System.Windows.Forms.Button()
        Me.txt_nama = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_frm
        '
        Me.txt_frm.Location = New System.Drawing.Point(390, 20)
        Me.txt_frm.Name = "txt_frm"
        Me.txt_frm.Size = New System.Drawing.Size(106, 20)
        Me.txt_frm.TabIndex = 48
        Me.txt_frm.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Label2"
        '
        'btn_simpan_baru
        '
        Me.btn_simpan_baru.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_baru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_baru.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_baru.Image = Global.HAOTEX.My.Resources.Resources.action_add
        Me.btn_simpan_baru.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_baru.Location = New System.Drawing.Point(18, 140)
        Me.btn_simpan_baru.Name = "btn_simpan_baru"
        Me.btn_simpan_baru.Size = New System.Drawing.Size(120, 23)
        Me.btn_simpan_baru.TabIndex = 46
        Me.btn_simpan_baru.Text = "Simpan & Baru"
        Me.btn_simpan_baru.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_baru.UseMnemonic = False
        Me.btn_simpan_baru.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Image = Global.HAOTEX.My.Resources.Resources.action_delete
        Me.btn_batal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal.Location = New System.Drawing.Point(307, 140)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(64, 23)
        Me.btn_batal.TabIndex = 45
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal.UseMnemonic = False
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan_tutup
        '
        Me.btn_simpan_tutup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_tutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_tutup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_tutup.Image = Global.HAOTEX.My.Resources.Resources.save1
        Me.btn_simpan_tutup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_tutup.Location = New System.Drawing.Point(159, 140)
        Me.btn_simpan_tutup.Name = "btn_simpan_tutup"
        Me.btn_simpan_tutup.Size = New System.Drawing.Size(127, 23)
        Me.btn_simpan_tutup.TabIndex = 44
        Me.btn_simpan_tutup.Text = "Simpan & Tutup"
        Me.btn_simpan_tutup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_tutup.UseMnemonic = False
        Me.btn_simpan_tutup.UseVisualStyleBackColor = True
        '
        'txt_nama
        '
        Me.txt_nama.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_nama.Location = New System.Drawing.Point(113, 21)
        Me.txt_nama.Name = "txt_nama"
        Me.txt_nama.Size = New System.Drawing.Size(259, 20)
        Me.txt_nama.TabIndex = 43
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(25, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 16)
        Me.Label1.TabIndex = 42
        Me.Label1.Text = "Label1"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Window
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txt_nama)
        Me.Panel1.Location = New System.Drawing.Point(-3, 47)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 65)
        Me.Panel1.TabIndex = 49
        '
        'form_input_nama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(388, 182)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txt_frm)
        Me.Controls.Add(Me.btn_simpan_baru)
        Me.Controls.Add(Me.btn_batal)
        Me.Controls.Add(Me.btn_simpan_tutup)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "form_input_nama"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_frm As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btn_simpan_baru As System.Windows.Forms.Button
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents btn_simpan_tutup As System.Windows.Forms.Button
    Friend WithEvents txt_nama As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
