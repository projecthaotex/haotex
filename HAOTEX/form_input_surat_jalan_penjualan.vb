﻿Imports MySql.Data.MySqlClient

Public Class form_input_surat_jalan_penjualan

    Private Sub form_input_surat_jalan_penjualan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel_2.Visible = False
        Panel_3.Visible = False
        Panel_4.Visible = False
        Panel_5.Visible = False
        Panel_6.Visible = False
        Panel_7.Visible = False
        Panel_8.Visible = False
        Panel_9.Visible = False
        Panel_10.Visible = False
        btn_hapus_2.Visible = False
        btn_hapus_3.Visible = False
        btn_hapus_4.Visible = False
        btn_hapus_5.Visible = False
        btn_hapus_6.Visible = False
        btn_hapus_7.Visible = False
        btn_hapus_8.Visible = False
        btn_hapus_9.Visible = False
        btn_hapus_10.Visible = False
        btn_tambah_baris_2.Visible = False
        btn_tambah_baris_3.Visible = False
        btn_tambah_baris_4.Visible = False
        btn_tambah_baris_5.Visible = False
        btn_tambah_baris_6.Visible = False
        btn_tambah_baris_7.Visible = False
        btn_tambah_baris_8.Visible = False
        btn_tambah_baris_9.Visible = False
        btn_selesai_2.Visible = False
        btn_selesai_3.Visible = False
        btn_selesai_4.Visible = False
        btn_selesai_5.Visible = False
        btn_selesai_6.Visible = False
        btn_selesai_7.Visible = False
        btn_selesai_8.Visible = False
        btn_selesai_9.Visible = False
        btn_selesai_10.Visible = False
        btn_batal_2.Visible = False
        btn_batal_3.Visible = False
        btn_batal_4.Visible = False
        btn_batal_5.Visible = False
        btn_batal_6.Visible = False
        btn_batal_7.Visible = False
        btn_batal_8.Visible = False
        btn_batal_9.Visible = False
        btn_batal_10.Visible = False
    End Sub

    Private Sub txt_customer_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer.GotFocus
        If txt_customer.ReadOnly = True Then
        Else
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        End If
    End Sub

    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        If Not e.KeyChar = Chr(13) Then e.Handled = True
    End Sub
    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim tanggal As DateTime
        tanggal = dtp_awal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        Dim tanggal As DateTime
        tanggal = dtp_awal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub

    Private Sub isiidpenjualan_1()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_1.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_1.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_2()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_2.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_2.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_3()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_3.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_3.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_4()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_4.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_4.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_5()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_5.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_5.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_6()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_6.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_6.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_7()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_7.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_7.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_8()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_8.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_8.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_9()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_9.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_9.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpenjualan_10()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Penjualan FROM tbsuratjalanpenjualan WHERE Id_Penjualan in(select max(Id_Penjualan) FROM tbsuratjalanpenjualan)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_penjualan_10.Text = Val(drx.Item("Id_Penjualan")) + 1
                    Else
                        txt_id_penjualan_10.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub isiidpiutang_1()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_1.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_1.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_2()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_2.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_2.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_3()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_3.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_3.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_4()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_4.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_4.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_5()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_5.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_5.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_6()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_6.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_6.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_7()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_7.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_7.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_8()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_8.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_8.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_9()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_9.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_9.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub isiidpiutang_10()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Piutang FROM tbdetilpiutang WHERE Id_Piutang in(select max(Id_Piutang) FROM tbdetilpiutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_piutang_10.Text = Val(drx.Item("Id_Piutang")) + 1
                    Else
                        txt_id_piutang_10.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub cb_stok_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_1.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_2.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_3.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_4.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_5.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_6.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_7.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_8.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_9.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub cb_stok_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok_10.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub

    Private Sub txt_sj_packing_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_1.GotFocus, txt_gudang_1.GotFocus, _
        txt_jenis_kain_1.GotFocus, txt_warna_1.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_1.Text = "-- Pilih Stok --" Then
            cb_stok_1.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_1.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_1"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_1.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_1"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_1.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_1"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_2.GotFocus, txt_gudang_2.GotFocus, _
        txt_jenis_kain_2.GotFocus, txt_warna_2.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_2.Text = "-- Pilih Stok --" Then
            cb_stok_2.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_2.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_2"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_2.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_2"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_2.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_2"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_3.GotFocus, txt_gudang_3.GotFocus, _
        txt_jenis_kain_3.GotFocus, txt_warna_3.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_3.Text = "-- Pilih Stok --" Then
            cb_stok_3.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_3.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_3"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_3.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_3"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_3.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_3"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_4.GotFocus, txt_gudang_4.GotFocus, _
        txt_jenis_kain_4.GotFocus, txt_warna_4.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_4.Text = "-- Pilih Stok --" Then
            cb_stok_4.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_4.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_4"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_4.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_4"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_4.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_4"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_5.GotFocus, txt_gudang_5.GotFocus, _
        txt_jenis_kain_5.GotFocus, txt_warna_5.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_5.Text = "-- Pilih Stok --" Then
            cb_stok_5.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_5.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_5"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_5.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_5"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_5.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_5"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_6.GotFocus, txt_gudang_6.GotFocus, _
        txt_jenis_kain_6.GotFocus, txt_warna_6.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_6.Text = "-- Pilih Stok --" Then
            cb_stok_6.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_6.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_6"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_6.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_6"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_6.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_6"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_7.GotFocus, txt_gudang_7.GotFocus, _
        txt_jenis_kain_7.GotFocus, txt_warna_7.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_7.Text = "-- Pilih Stok --" Then
            cb_stok_7.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_7.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_7"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_7.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_7"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_7.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_7"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_8.GotFocus, txt_gudang_8.GotFocus, _
        txt_jenis_kain_8.GotFocus, txt_warna_8.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_8.Text = "-- Pilih Stok --" Then
            cb_stok_8.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_8.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_8"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_8.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_8"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_8.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_8"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_9.GotFocus, txt_gudang_9.GotFocus, _
        txt_jenis_kain_9.GotFocus, txt_warna_9.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_9.Text = "-- Pilih Stok --" Then
            cb_stok_9.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_9.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_9"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_9.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_9"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_9.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_9"
            form_stok_claim_jadi.Focus()
        End If
    End Sub
    Private Sub txt_sj_packing_10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_sj_packing_10.GotFocus, txt_gudang_10.GotFocus, _
        txt_jenis_kain_10.GotFocus, txt_warna_10.GotFocus
        If txt_customer.Text = "" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_surat_jalan_penjualan"
            form_input_customer.Focus()
        ElseIf cb_stok_10.Text = "-- Pilih Stok --" Then
            cb_stok_10.Focus()
            MsgBox("Stok Belum Dipilih")
        ElseIf cb_stok_10.Text = "Grade A" Then
            form_stok_grade_a.MdiParent = form_menu_utama
            form_stok_grade_a.Show()
            form_stok_grade_a.txt_form.Text = "form_input_surat_jalan_penjualan_10"
            form_stok_grade_a.Focus()
        ElseIf cb_stok_10.Text = "Grade B" Then
            form_stok_grade_b.MdiParent = form_menu_utama
            form_stok_grade_b.Show()
            form_stok_grade_b.txt_form.Text = "form_input_surat_jalan_penjualan_10"
            form_stok_grade_b.Focus()
        ElseIf cb_stok_10.Text = "Claim Jadi" Then
            form_stok_claim_jadi.MdiParent = form_menu_utama
            form_stok_claim_jadi.Show()
            form_stok_claim_jadi.txt_form.Text = "form_input_surat_jalan_penjualan_10"
            form_stok_claim_jadi.Focus()
        End If
    End Sub

    Private Sub Bersihkanlah_1()
        txt_sj_packing_1.Text = ""
        txt_gudang_1.Text = ""
        txt_jenis_kain_1.Text = ""
        txt_warna_1.Text = ""
        txt_gulung_1.Text = ""
        txt_yards_1.Text = ""
        txt_harga_1.Text = ""
        txt_total_harga_1.Text = ""
        txt_keterangan_1.Text = ""
        txt_id_grade_a_1.Text = ""
        txt_id_grade_b_1.Text = ""
        txt_id_claim_jadi_1.Text = ""
        txt_id_grey_1.Text = ""
    End Sub
    Private Sub Bersihkanlah_2()
        txt_sj_packing_2.Text = ""
        txt_gudang_2.Text = ""
        txt_jenis_kain_2.Text = ""
        txt_warna_2.Text = ""
        txt_gulung_2.Text = ""
        txt_yards_2.Text = ""
        txt_harga_2.Text = ""
        txt_total_harga_2.Text = ""
        txt_keterangan_2.Text = ""
        txt_id_grade_a_2.Text = ""
        txt_id_grade_b_2.Text = ""
        txt_id_claim_jadi_2.Text = ""
        txt_id_grey_2.Text = ""
    End Sub
    Private Sub Bersihkanlah_3()
        txt_sj_packing_3.Text = ""
        txt_gudang_3.Text = ""
        txt_jenis_kain_3.Text = ""
        txt_warna_3.Text = ""
        txt_gulung_3.Text = ""
        txt_yards_3.Text = ""
        txt_harga_3.Text = ""
        txt_total_harga_3.Text = ""
        txt_keterangan_3.Text = ""
        txt_id_grade_a_3.Text = ""
        txt_id_grade_b_3.Text = ""
        txt_id_claim_jadi_3.Text = ""
        txt_id_grey_3.Text = ""
    End Sub
    Private Sub Bersihkanlah_4()
        txt_sj_packing_4.Text = ""
        txt_gudang_4.Text = ""
        txt_jenis_kain_4.Text = ""
        txt_warna_4.Text = ""
        txt_gulung_4.Text = ""
        txt_yards_4.Text = ""
        txt_harga_4.Text = ""
        txt_total_harga_4.Text = ""
        txt_keterangan_4.Text = ""
        txt_id_grade_a_4.Text = ""
        txt_id_grade_b_4.Text = ""
        txt_id_claim_jadi_4.Text = ""
        txt_id_grey_4.Text = ""
    End Sub
    Private Sub Bersihkanlah_5()
        txt_sj_packing_5.Text = ""
        txt_gudang_5.Text = ""
        txt_jenis_kain_5.Text = ""
        txt_warna_5.Text = ""
        txt_gulung_5.Text = ""
        txt_yards_5.Text = ""
        txt_harga_5.Text = ""
        txt_total_harga_5.Text = ""
        txt_keterangan_5.Text = ""
        txt_id_grade_a_5.Text = ""
        txt_id_grade_b_5.Text = ""
        txt_id_claim_jadi_5.Text = ""
        txt_id_grey_5.Text = ""
    End Sub
    Private Sub Bersihkanlah_6()
        txt_sj_packing_6.Text = ""
        txt_gudang_6.Text = ""
        txt_jenis_kain_6.Text = ""
        txt_warna_6.Text = ""
        txt_gulung_6.Text = ""
        txt_yards_6.Text = ""
        txt_harga_6.Text = ""
        txt_total_harga_6.Text = ""
        txt_keterangan_6.Text = ""
        txt_id_grade_a_6.Text = ""
        txt_id_grade_b_6.Text = ""
        txt_id_claim_jadi_6.Text = ""
        txt_id_grey_6.Text = ""
    End Sub
    Private Sub Bersihkanlah_7()
        txt_sj_packing_7.Text = ""
        txt_gudang_7.Text = ""
        txt_jenis_kain_7.Text = ""
        txt_warna_7.Text = ""
        txt_gulung_7.Text = ""
        txt_yards_7.Text = ""
        txt_harga_7.Text = ""
        txt_total_harga_7.Text = ""
        txt_keterangan_7.Text = ""
        txt_id_grade_a_7.Text = ""
        txt_id_grade_b_7.Text = ""
        txt_id_claim_jadi_7.Text = ""
        txt_id_grey_7.Text = ""
    End Sub
    Private Sub Bersihkanlah_8()
        txt_sj_packing_8.Text = ""
        txt_gudang_8.Text = ""
        txt_jenis_kain_8.Text = ""
        txt_warna_8.Text = ""
        txt_gulung_8.Text = ""
        txt_yards_8.Text = ""
        txt_harga_8.Text = ""
        txt_total_harga_8.Text = ""
        txt_keterangan_8.Text = ""
        txt_id_grade_a_8.Text = ""
        txt_id_grade_b_8.Text = ""
        txt_id_claim_jadi_8.Text = ""
        txt_id_grey_8.Text = ""
    End Sub
    Private Sub Bersihkanlah_9()
        txt_sj_packing_9.Text = ""
        txt_gudang_9.Text = ""
        txt_jenis_kain_9.Text = ""
        txt_warna_9.Text = ""
        txt_gulung_9.Text = ""
        txt_yards_9.Text = ""
        txt_harga_9.Text = ""
        txt_total_harga_9.Text = ""
        txt_keterangan_9.Text = ""
        txt_id_grade_a_9.Text = ""
        txt_id_grade_b_9.Text = ""
        txt_id_claim_jadi_9.Text = ""
        txt_id_grey_9.Text = ""
    End Sub
    Private Sub Bersihkanlah_10()
        txt_sj_packing_10.Text = ""
        txt_gudang_10.Text = ""
        txt_jenis_kain_10.Text = ""
        txt_warna_10.Text = ""
        txt_gulung_10.Text = ""
        txt_yards_10.Text = ""
        txt_harga_10.Text = ""
        txt_total_harga_10.Text = ""
        txt_keterangan_10.Text = ""
        txt_id_grade_a_10.Text = ""
        txt_id_grade_b_10.Text = ""
        txt_id_claim_jadi_10.Text = ""
        txt_id_grey_10.Text = ""
    End Sub

    Private Sub cb_stok_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_1.TextChanged
        Call Bersihkanlah_1()
    End Sub
    Private Sub cb_stok_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_2.TextChanged
        Call Bersihkanlah_2()
    End Sub
    Private Sub cb_stok_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_3.TextChanged
        Call Bersihkanlah_3()
    End Sub
    Private Sub cb_stok_4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_4.TextChanged
        Call Bersihkanlah_4()
    End Sub
    Private Sub cb_stok_5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_5.TextChanged
        Call Bersihkanlah_5()
    End Sub
    Private Sub cb_stok_6_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_6.TextChanged
        Call Bersihkanlah_6()
    End Sub
    Private Sub cb_stok_7_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_7.TextChanged
        Call Bersihkanlah_7()
    End Sub
    Private Sub cb_stok_8_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_8.TextChanged
        Call Bersihkanlah_8()
    End Sub
    Private Sub cb_stok_9_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_9.TextChanged
        Call Bersihkanlah_9()
    End Sub
    Private Sub cb_stok_10_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok_10.TextChanged
        Call Bersihkanlah_10()
    End Sub

    Private Sub simpan_tbsuratjalanpenjualan_1()
        Dim total_harga As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_1.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_1.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_1.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@7", txt_warna_1.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_1.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_1.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_1.Text)
                    If cb_stok_1.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_1.Text)
                    ElseIf cb_stok_1.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_1.Text)
                    ElseIf cb_stok_1.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_1.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_2()
        Dim total_harga As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_2.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_2.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_2.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@7", txt_warna_2.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_2.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_2.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_2.Text)
                    If cb_stok_2.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_2.Text)
                    ElseIf cb_stok_2.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_2.Text)
                    ElseIf cb_stok_2.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_2.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_3()
        Dim total_harga As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_3.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_3.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_3.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@7", txt_warna_3.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_3.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_3.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_3.Text)
                    If cb_stok_3.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_3.Text)
                    ElseIf cb_stok_3.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_3.Text)
                    ElseIf cb_stok_3.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_3.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_4()
        Dim total_harga As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_4.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_4.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_4.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@7", txt_warna_4.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_4.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_4.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_4.Text)
                    If cb_stok_4.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_4.Text)
                    ElseIf cb_stok_4.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_4.Text)
                    ElseIf cb_stok_4.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_4.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_5()
        Dim total_harga As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_5.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_5.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_5.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@7", txt_warna_5.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_5.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_5.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_5.Text)
                    If cb_stok_5.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_5.Text)
                    ElseIf cb_stok_5.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_5.Text)
                    ElseIf cb_stok_5.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_5.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_6()
        Dim total_harga As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_6.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_6.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_6.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@7", txt_warna_6.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_6.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_6.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_6.Text)
                    If cb_stok_6.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_6.Text)
                    ElseIf cb_stok_6.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_6.Text)
                    ElseIf cb_stok_6.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_6.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_7()
        Dim total_harga As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_7.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_7.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_7.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@7", txt_warna_7.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_7.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_7.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_7.Text)
                    If cb_stok_7.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_7.Text)
                    ElseIf cb_stok_7.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_7.Text)
                    ElseIf cb_stok_7.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_7.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_8()
        Dim total_harga As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_8.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_8.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_8.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@7", txt_warna_8.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_8.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_8.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_8.Text)
                    If cb_stok_8.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_8.Text)
                    ElseIf cb_stok_8.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_8.Text)
                    ElseIf cb_stok_8.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_8.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_9()
        Dim total_harga As String = txt_total_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_9.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_9.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_9.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@7", txt_warna_9.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_9.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_9.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_9.Text)
                    If cb_stok_9.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_9.Text)
                    ElseIf cb_stok_9.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_9.Text)
                    ElseIf cb_stok_9.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_9.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanpenjualan_10()
        Dim total_harga As String = txt_total_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanpenjualan (Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang_10.Text)
                    .Parameters.AddWithValue("@2", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@3", cb_stok_10.Text)
                    .Parameters.AddWithValue("@4", txt_sj_packing_10.Text)
                    .Parameters.AddWithValue("@5", txt_customer.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@7", txt_warna_10.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_10.Text)
                    .Parameters.AddWithValue("@9", yards.Replace(",", "."))
                    .Parameters.AddWithValue("@10", harga.Replace(",", "."))
                    .Parameters.AddWithValue("@11", total_harga.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_keterangan_10.Text)
                    .Parameters.AddWithValue("@13", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@14", txt_id_penjualan_10.Text)
                    If cb_stok_10.Text = "Grade A" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_a_10.Text)
                    ElseIf cb_stok_10.Text = "Grade B" Then
                        .Parameters.AddWithValue("@15", txt_id_grade_b_10.Text)
                    ElseIf cb_stok_10.Text = "Claim Jadi" Then
                        .Parameters.AddWithValue("@15", txt_id_claim_jadi_10.Text)
                    End If
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", 0)
                    .Parameters.AddWithValue("@20", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub

    Private Sub simpan_tbdetilpiutang_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_1.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_1.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_1 As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_1 As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_1.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_1.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_1.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_2.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_2.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_2 As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_2 As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_2.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_2.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_2.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_3.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_3.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_3 As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_3 As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_3.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_3.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_3.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_4.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_4.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_4 As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_4 As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_4.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_4.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_4.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_5.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_5.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_5 As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_5 As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_5.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_5.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_5.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_6.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_6.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_6 As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_6 As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_6.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_6.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_6.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_7.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_7.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_7 As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_7 As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_7.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_7.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_7.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_8.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_8.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_8 As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_8 As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_8.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_8.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_8.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_9.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_9.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_9 As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_9 As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_9 As String = txt_total_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_9.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_9.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_9.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilpiutang_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilpiutang (Tanggal,Id_Grey,Id_Asal,Id_Piutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@2", txt_id_penjualan_10.Text)
                    .Parameters.AddWithValue("@3", txt_id_piutang_10.Text)
                    .Parameters.AddWithValue("@4", txt_sj_penjualan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@6", txt_customer.Text)
                    Dim harga_10 As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_10 As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_10 As String = txt_total_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_10.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_10.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Yard")
                    .Parameters.AddWithValue("@10", total_harga_10.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Penjualan")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub simpan_update_tbstokgradea_1()
        Dim Gulung_1 As String = txt_gulung_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_1 As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_1 As Double
                        Dim y_1 As Double
                        g_1 = drx.Item(0)
                        y_1 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_1 - Val(Gulung_1.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_1 - Val(yards_1.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_2()
        Dim Gulung_2 As String = txt_gulung_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_2 As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_2 As Double
                        Dim y_2 As Double
                        g_2 = drx.Item(0)
                        y_2 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_2 - Val(Gulung_2.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_2 - Val(yards_2.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_3()
        Dim Gulung_3 As String = txt_gulung_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_3 As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_3 As Double
                        Dim y_3 As Double
                        g_3 = drx.Item(0)
                        y_3 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_3 - Val(Gulung_3.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_3 - Val(yards_3.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_4()
        Dim Gulung_4 As String = txt_gulung_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_4 As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_4 As Double
                        Dim y_4 As Double
                        g_4 = drx.Item(0)
                        y_4 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_4 - Val(Gulung_4.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_4 - Val(yards_4.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_5()
        Dim Gulung_5 As String = txt_gulung_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_5 As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_5 As Double
                        Dim y_5 As Double
                        g_5 = drx.Item(0)
                        y_5 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_5 - Val(Gulung_5.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_5 - Val(yards_5.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_6()
        Dim Gulung_6 As String = txt_gulung_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_6 As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_6 As Double
                        Dim y_6 As Double
                        g_6 = drx.Item(0)
                        y_6 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_6 - Val(Gulung_6.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_6 - Val(yards_6.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_7()
        Dim Gulung_7 As String = txt_gulung_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_7 As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_7 As Double
                        Dim y_7 As Double
                        g_7 = drx.Item(0)
                        y_7 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_7 - Val(Gulung_7.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_7 - Val(yards_7.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_8()
        Dim Gulung_8 As String = txt_gulung_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_8 As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_8 As Double
                        Dim y_8 As Double
                        g_8 = drx.Item(0)
                        y_8 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_8 - Val(Gulung_8.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_8 - Val(yards_8.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_9()
        Dim Gulung_9 As String = txt_gulung_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_9 As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_9 As Double
                        Dim y_9 As Double
                        g_9 = drx.Item(0)
                        y_9 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_9.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_9 - Val(Gulung_9.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_9 - Val(yards_9.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradea_10()
        Dim Gulung_10 As String = txt_gulung_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_10 As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradea WHERE Id_Grade_A='" & txt_id_grade_a_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_10 As Double
                        Dim y_10 As Double
                        g_10 = drx.Item(0)
                        y_10 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradea SET Gulung=@0,Stok=@1 WHERE Id_Grade_A='" & txt_id_grade_a_10.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_10 - Val(Gulung_10.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_10 - Val(yards_10.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub simpan_update_tbstokgradeb_1()
        Dim Gulung_1 As String = txt_gulung_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_1 As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_1 As Double
                        Dim y_1 As Double
                        g_1 = drx.Item(0)
                        y_1 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_1 - Val(Gulung_1.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_1 - Val(yards_1.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_2()
        Dim Gulung_2 As String = txt_gulung_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_2 As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_2 As Double
                        Dim y_2 As Double
                        g_2 = drx.Item(0)
                        y_2 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_2 - Val(Gulung_2.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_2 - Val(yards_2.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_3()
        Dim Gulung_3 As String = txt_gulung_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_3 As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_3 As Double
                        Dim y_3 As Double
                        g_3 = drx.Item(0)
                        y_3 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_3 - Val(Gulung_3.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_3 - Val(yards_3.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_4()
        Dim Gulung_4 As String = txt_gulung_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_4 As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_4 As Double
                        Dim y_4 As Double
                        g_4 = drx.Item(0)
                        y_4 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_4 - Val(Gulung_4.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_4 - Val(yards_4.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_5()
        Dim Gulung_5 As String = txt_gulung_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_5 As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_5 As Double
                        Dim y_5 As Double
                        g_5 = drx.Item(0)
                        y_5 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_5 - Val(Gulung_5.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_5 - Val(yards_5.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_6()
        Dim Gulung_6 As String = txt_gulung_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_6 As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_6 As Double
                        Dim y_6 As Double
                        g_6 = drx.Item(0)
                        y_6 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_6 - Val(Gulung_6.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_6 - Val(yards_6.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_7()
        Dim Gulung_7 As String = txt_gulung_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_7 As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_7 As Double
                        Dim y_7 As Double
                        g_7 = drx.Item(0)
                        y_7 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_7 - Val(Gulung_7.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_7 - Val(yards_7.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_8()
        Dim Gulung_8 As String = txt_gulung_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_8 As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_8 As Double
                        Dim y_8 As Double
                        g_8 = drx.Item(0)
                        y_8 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_8 - Val(Gulung_8.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_8 - Val(yards_8.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_9()
        Dim Gulung_9 As String = txt_gulung_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_9 As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_9 As Double
                        Dim y_9 As Double
                        g_9 = drx.Item(0)
                        y_9 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_9.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_9 - Val(Gulung_9.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_9 - Val(yards_9.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokgradeb_10()
        Dim Gulung_10 As String = txt_gulung_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_10 As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokgradeb WHERE Id_Grade_B='" & txt_id_grade_b_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_10 As Double
                        Dim y_10 As Double
                        g_10 = drx.Item(0)
                        y_10 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokgradeb SET Gulung=@0,Stok=@1 WHERE Id_Grade_B='" & txt_id_grade_b_10.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_10 - Val(Gulung_10.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_10 - Val(yards_10.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub simpan_update_tbstokclaimjadi_1()
        Dim Gulung_1 As String = txt_gulung_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_1 As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_1 As Double
                        Dim y_1 As Double
                        g_1 = drx.Item(0)
                        y_1 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_1 - Val(Gulung_1.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_1 - Val(yards_1.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_2()
        Dim Gulung_2 As String = txt_gulung_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_2 As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_2 As Double
                        Dim y_2 As Double
                        g_2 = drx.Item(0)
                        y_2 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_2 - Val(Gulung_2.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_2 - Val(yards_2.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_3()
        Dim Gulung_3 As String = txt_gulung_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_3 As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_3 As Double
                        Dim y_3 As Double
                        g_3 = drx.Item(0)
                        y_3 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_3 - Val(Gulung_3.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_3 - Val(yards_3.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_4()
        Dim Gulung_4 As String = txt_gulung_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_4 As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_4 As Double
                        Dim y_4 As Double
                        g_4 = drx.Item(0)
                        y_4 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_4 - Val(Gulung_4.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_4 - Val(yards_4.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_5()
        Dim Gulung_5 As String = txt_gulung_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_5 As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_5 As Double
                        Dim y_5 As Double
                        g_5 = drx.Item(0)
                        y_5 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_5 - Val(Gulung_5.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_5 - Val(yards_5.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_6()
        Dim Gulung_6 As String = txt_gulung_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_6 As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_6 As Double
                        Dim y_6 As Double
                        g_6 = drx.Item(0)
                        y_6 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_6 - Val(Gulung_6.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_6 - Val(yards_6.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_7()
        Dim Gulung_7 As String = txt_gulung_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_7 As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_7 As Double
                        Dim y_7 As Double
                        g_7 = drx.Item(0)
                        y_7 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_7 - Val(Gulung_7.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_7 - Val(yards_7.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_8()
        Dim Gulung_8 As String = txt_gulung_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_8 As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_8 As Double
                        Dim y_8 As Double
                        g_8 = drx.Item(0)
                        y_8 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_8 - Val(Gulung_8.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_8 - Val(yards_8.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_9()
        Dim Gulung_9 As String = txt_gulung_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_9 As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_9 As Double
                        Dim y_9 As Double
                        g_9 = drx.Item(0)
                        y_9 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_9.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_9 - Val(Gulung_9.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_9 - Val(yards_9.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub simpan_update_tbstokclaimjadi_10()
        Dim Gulung_10 As String = txt_gulung_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_10 As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT gulung,stok FROM tbstokclaimjadi WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim g_10 As Double
                        Dim y_10 As Double
                        g_10 = drx.Item(0)
                        y_10 = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokclaimjadi SET Gulung=@0,Stok=@1 WHERE Id_Claim_Jadi='" & txt_id_claim_jadi_10.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@0", Math.Round(g_10 - Val(Gulung_10.Replace(",", ".")), 2))
                                    .Parameters.AddWithValue("@1", Math.Round(y_10 - Val(yards_10.Replace(",", ".")), 2))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub txt_yards_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_1.Text = String.Empty Then
                        txt_yards_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_1.Select(txt_yards_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_1.Text = String.Empty Then
                        txt_yards_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_1.Select(txt_yards_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_2.Text = String.Empty Then
                        txt_yards_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_2.Select(txt_yards_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_2.Text = String.Empty Then
                        txt_yards_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_2.Select(txt_yards_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_3.Text = String.Empty Then
                        txt_yards_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_3.Select(txt_yards_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_3.Text = String.Empty Then
                        txt_yards_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_3.Select(txt_yards_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_4.Text = String.Empty Then
                        txt_yards_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_4.Select(txt_yards_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_4.Text = String.Empty Then
                        txt_yards_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_4.Select(txt_yards_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_5.Text = String.Empty Then
                        txt_yards_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_5.Select(txt_yards_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_5.Text = String.Empty Then
                        txt_yards_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_5.Select(txt_yards_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_6.Text = String.Empty Then
                        txt_yards_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_6.Select(txt_yards_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_6.Text = String.Empty Then
                        txt_yards_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_6.Select(txt_yards_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_7.Text = String.Empty Then
                        txt_yards_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_7.Select(txt_yards_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_7.Text = String.Empty Then
                        txt_yards_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_7.Select(txt_yards_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_8.Text = String.Empty Then
                        txt_yards_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_8.Select(txt_yards_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_8.Text = String.Empty Then
                        txt_yards_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_8.Select(txt_yards_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_9.Text = String.Empty Then
                        txt_yards_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_9.Select(txt_yards_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_9.Text = String.Empty Then
                        txt_yards_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_9.Select(txt_yards_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_yards_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_yards_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_yards_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_yards_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_yards_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_10.Text = String.Empty Then
                        txt_yards_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_10.Select(txt_yards_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_yards_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_yards_10.Text = String.Empty Then
                        txt_yards_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_yards_10.Select(txt_yards_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_harga_1_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_1.Text = String.Empty Then
                        txt_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_1.Select(txt_harga_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_1.Text = String.Empty Then
                        txt_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_1.Select(txt_harga_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_2_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_2.Text = String.Empty Then
                        txt_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_2.Select(txt_harga_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_2.Text = String.Empty Then
                        txt_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_2.Select(txt_harga_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_3_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_3.Text = String.Empty Then
                        txt_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_3.Select(txt_harga_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_3.Text = String.Empty Then
                        txt_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_3.Select(txt_harga_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_4_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_4.Text = String.Empty Then
                        txt_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_4.Select(txt_harga_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_4.Text = String.Empty Then
                        txt_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_4.Select(txt_harga_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_5_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_5.Text = String.Empty Then
                        txt_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_5.Select(txt_harga_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_5.Text = String.Empty Then
                        txt_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_5.Select(txt_harga_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_6_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_6.Text = String.Empty Then
                        txt_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_6.Select(txt_harga_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_6.Text = String.Empty Then
                        txt_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_6.Select(txt_harga_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_7_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_7.Text = String.Empty Then
                        txt_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_7.Select(txt_harga_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_7.Text = String.Empty Then
                        txt_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_7.Select(txt_harga_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_8_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_8.Text = String.Empty Then
                        txt_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_8.Select(txt_harga_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_8.Text = String.Empty Then
                        txt_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_8.Select(txt_harga_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_9_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_9.Text = String.Empty Then
                        txt_harga_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_9.Select(txt_harga_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_9.Text = String.Empty Then
                        txt_harga_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_9.Select(txt_harga_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_10_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_10.Text = String.Empty Then
                        txt_harga_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_10.Select(txt_harga_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_10.Text = String.Empty Then
                        txt_harga_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_10.Select(txt_harga_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_total_harga_1_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        txt_total_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        txt_total_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_2_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        txt_total_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        txt_total_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_3_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        txt_total_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        txt_total_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_4_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        txt_total_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        txt_total_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_5_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        txt_total_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        txt_total_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_6_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        txt_total_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        txt_total_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_7_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        txt_total_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        txt_total_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_8_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        txt_total_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        txt_total_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_9_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_9.Text = String.Empty Then
                        txt_total_harga_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_9.Text = String.Empty Then
                        txt_total_harga_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_10_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_10.Text = String.Empty Then
                        txt_total_harga_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_10.Text = String.Empty Then
                        txt_total_harga_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_harga_1_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_1.TextChanged
        If txt_harga_1.Text <> String.Empty Then
            Dim temp As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_1.Select(txt_harga_1.Text.Length, 0)
            ElseIf txt_harga_1.Text = "-"c Then

            Else
                txt_harga_1.Text = CDec(temp).ToString("N0")
                txt_harga_1.Select(txt_harga_1.Text.Length, 0)
            End If
        End If
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_1 As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_1.Text = Math.Round(Val(harga_1.Replace(",", ".")) * Val(yards_1.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_2_TextChanged_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_2.TextChanged
        If txt_harga_2.Text <> String.Empty Then
            Dim temp As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_2.Select(txt_harga_2.Text.Length, 0)
            ElseIf txt_harga_2.Text = "-"c Then

            Else
                txt_harga_2.Text = CDec(temp).ToString("N0")
                txt_harga_2.Select(txt_harga_2.Text.Length, 0)
            End If
        End If
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_2 As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_2.Text = Math.Round(Val(harga_2.Replace(",", ".")) * Val(yards_2.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_3_TextChanged_3(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_3.TextChanged
        If txt_harga_3.Text <> String.Empty Then
            Dim temp As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_3.Select(txt_harga_3.Text.Length, 0)
            ElseIf txt_harga_3.Text = "-"c Then

            Else
                txt_harga_3.Text = CDec(temp).ToString("N0")
                txt_harga_3.Select(txt_harga_3.Text.Length, 0)
            End If
        End If
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_3 As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_3.Text = Math.Round(Val(harga_3.Replace(",", ".")) * Val(yards_3.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_4_TextChanged_4(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_4.TextChanged
        If txt_harga_4.Text <> String.Empty Then
            Dim temp As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_4.Select(txt_harga_4.Text.Length, 0)
            ElseIf txt_harga_4.Text = "-"c Then

            Else
                txt_harga_4.Text = CDec(temp).ToString("N0")
                txt_harga_4.Select(txt_harga_4.Text.Length, 0)
            End If
        End If
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_4 As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_4.Text = Math.Round(Val(harga_4.Replace(",", ".")) * Val(yards_4.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_5_TextChanged_5(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_5.TextChanged
        If txt_harga_5.Text <> String.Empty Then
            Dim temp As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_5.Select(txt_harga_5.Text.Length, 0)
            ElseIf txt_harga_5.Text = "-"c Then

            Else
                txt_harga_5.Text = CDec(temp).ToString("N0")
                txt_harga_5.Select(txt_harga_5.Text.Length, 0)
            End If
        End If
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_5 As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_5.Text = Math.Round(Val(harga_5.Replace(",", ".")) * Val(yards_5.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_6_TextChanged_6(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_6.TextChanged
        If txt_harga_6.Text <> String.Empty Then
            Dim temp As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_6.Select(txt_harga_6.Text.Length, 0)
            ElseIf txt_harga_6.Text = "-"c Then

            Else
                txt_harga_6.Text = CDec(temp).ToString("N0")
                txt_harga_6.Select(txt_harga_6.Text.Length, 0)
            End If
        End If
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_6 As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_6.Text = Math.Round(Val(harga_6.Replace(",", ".")) * Val(yards_6.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_7_TextChanged_7(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_7.TextChanged
        If txt_harga_7.Text <> String.Empty Then
            Dim temp As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_7.Select(txt_harga_7.Text.Length, 0)
            ElseIf txt_harga_7.Text = "-"c Then

            Else
                txt_harga_7.Text = CDec(temp).ToString("N0")
                txt_harga_7.Select(txt_harga_7.Text.Length, 0)
            End If
        End If
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_7 As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_7.Text = Math.Round(Val(harga_7.Replace(",", ".")) * Val(yards_7.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_8_TextChanged_8(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_8.TextChanged
        If txt_harga_8.Text <> String.Empty Then
            Dim temp As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_8.Select(txt_harga_8.Text.Length, 0)
            ElseIf txt_harga_8.Text = "-"c Then

            Else
                txt_harga_8.Text = CDec(temp).ToString("N0")
                txt_harga_8.Select(txt_harga_8.Text.Length, 0)
            End If
        End If
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_8 As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_8.Text = Math.Round(Val(harga_8.Replace(",", ".")) * Val(yards_8.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_9_TextChanged_9(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_9.TextChanged
        If txt_harga_9.Text <> String.Empty Then
            Dim temp As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_9.Select(txt_harga_9.Text.Length, 0)
            ElseIf txt_harga_9.Text = "-"c Then

            Else
                txt_harga_9.Text = CDec(temp).ToString("N0")
                txt_harga_9.Select(txt_harga_9.Text.Length, 0)
            End If
        End If
        Dim harga_9 As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_9 As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_9.Text = Math.Round(Val(harga_9.Replace(",", ".")) * Val(yards_9.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_10_TextChanged_10(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_10.TextChanged
        If txt_harga_10.Text <> String.Empty Then
            Dim temp As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_10.Select(txt_harga_10.Text.Length, 0)
            ElseIf txt_harga_10.Text = "-"c Then

            Else
                txt_harga_10.Text = CDec(temp).ToString("N0")
                txt_harga_10.Select(txt_harga_10.Text.Length, 0)
            End If
        End If
        Dim harga_10 As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_10 As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_10.Text = Math.Round(Val(harga_10.Replace(",", ".")) * Val(yards_10.Replace(",", ".")), 0)
    End Sub

    Private Sub txt_yards_1_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_1.TextChanged
        If txt_yards_1.Text <> String.Empty Then
            Dim temp As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_1.Select(txt_yards_1.Text.Length, 0)
            ElseIf txt_yards_1.Text = "-"c Then

            Else
                txt_yards_1.Text = CDec(temp).ToString("N0")
                txt_yards_1.Select(txt_yards_1.Text.Length, 0)
            End If
        End If
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_1 As String = txt_yards_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_1.Text = Math.Round(Val(harga_1.Replace(",", ".")) * Val(yards_1.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_2_TextChanged_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_2.TextChanged
        If txt_yards_2.Text <> String.Empty Then
            Dim temp As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_2.Select(txt_yards_2.Text.Length, 0)
            ElseIf txt_yards_2.Text = "-"c Then

            Else
                txt_yards_2.Text = CDec(temp).ToString("N0")
                txt_yards_2.Select(txt_yards_2.Text.Length, 0)
            End If
        End If
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_2 As String = txt_yards_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_2.Text = Math.Round(Val(harga_2.Replace(",", ".")) * Val(yards_2.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_3_TextChanged_3(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_3.TextChanged
        If txt_yards_3.Text <> String.Empty Then
            Dim temp As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_3.Select(txt_yards_3.Text.Length, 0)
            ElseIf txt_yards_3.Text = "-"c Then

            Else
                txt_yards_3.Text = CDec(temp).ToString("N0")
                txt_yards_3.Select(txt_yards_3.Text.Length, 0)
            End If
        End If
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_3 As String = txt_yards_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_3.Text = Math.Round(Val(harga_3.Replace(",", ".")) * Val(yards_3.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_4_TextChanged_4(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_4.TextChanged
        If txt_yards_4.Text <> String.Empty Then
            Dim temp As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_4.Select(txt_yards_4.Text.Length, 0)
            ElseIf txt_yards_4.Text = "-"c Then

            Else
                txt_yards_4.Text = CDec(temp).ToString("N0")
                txt_yards_4.Select(txt_yards_4.Text.Length, 0)
            End If
        End If
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_4 As String = txt_yards_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_4.Text = Math.Round(Val(harga_4.Replace(",", ".")) * Val(yards_4.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_5_TextChanged_5(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_5.TextChanged
        If txt_yards_5.Text <> String.Empty Then
            Dim temp As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_5.Select(txt_yards_5.Text.Length, 0)
            ElseIf txt_yards_5.Text = "-"c Then

            Else
                txt_yards_5.Text = CDec(temp).ToString("N0")
                txt_yards_5.Select(txt_yards_5.Text.Length, 0)
            End If
        End If
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_5 As String = txt_yards_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_5.Text = Math.Round(Val(harga_5.Replace(",", ".")) * Val(yards_5.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_6_TextChanged_6(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_6.TextChanged
        If txt_yards_6.Text <> String.Empty Then
            Dim temp As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_6.Select(txt_yards_6.Text.Length, 0)
            ElseIf txt_yards_6.Text = "-"c Then

            Else
                txt_yards_6.Text = CDec(temp).ToString("N0")
                txt_yards_6.Select(txt_yards_6.Text.Length, 0)
            End If
        End If
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_6 As String = txt_yards_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_6.Text = Math.Round(Val(harga_6.Replace(",", ".")) * Val(yards_6.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_7_TextChanged_7(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_7.TextChanged
        If txt_yards_7.Text <> String.Empty Then
            Dim temp As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_7.Select(txt_yards_7.Text.Length, 0)
            ElseIf txt_yards_7.Text = "-"c Then

            Else
                txt_yards_7.Text = CDec(temp).ToString("N0")
                txt_yards_7.Select(txt_yards_7.Text.Length, 0)
            End If
        End If
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_7 As String = txt_yards_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_7.Text = Math.Round(Val(harga_7.Replace(",", ".")) * Val(yards_7.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_8_TextChanged_8(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_8.TextChanged
        If txt_yards_8.Text <> String.Empty Then
            Dim temp As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_8.Select(txt_yards_8.Text.Length, 0)
            ElseIf txt_yards_8.Text = "-"c Then

            Else
                txt_yards_8.Text = CDec(temp).ToString("N0")
                txt_yards_8.Select(txt_yards_8.Text.Length, 0)
            End If
        End If
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_8 As String = txt_yards_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_8.Text = Math.Round(Val(harga_8.Replace(",", ".")) * Val(yards_8.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_9_TextChanged_9(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_9.TextChanged
        If txt_yards_9.Text <> String.Empty Then
            Dim temp As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_9.Select(txt_yards_9.Text.Length, 0)
            ElseIf txt_yards_9.Text = "-"c Then

            Else
                txt_yards_9.Text = CDec(temp).ToString("N0")
                txt_yards_9.Select(txt_yards_9.Text.Length, 0)
            End If
        End If
        Dim harga_9 As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_9 As String = txt_yards_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_9.Text = Math.Round(Val(harga_9.Replace(",", ".")) * Val(yards_9.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_yards_10_TextChanged_10(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_yards_10.TextChanged
        If txt_yards_10.Text <> String.Empty Then
            Dim temp As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_yards_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_yards_10.Select(txt_yards_10.Text.Length, 0)
            ElseIf txt_yards_10.Text = "-"c Then

            Else
                txt_yards_10.Text = CDec(temp).ToString("N0")
                txt_yards_10.Select(txt_yards_10.Text.Length, 0)
            End If
        End If
        Dim harga_10 As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim yards_10 As String = txt_yards_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_10.Text = Math.Round(Val(harga_10.Replace(",", ".")) * Val(yards_10.Replace(",", ".")), 0)
    End Sub

    Private Sub txt_total_harga_1_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_1.TextChanged
        If txt_total_harga_1.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
            ElseIf txt_total_harga_1.Text = "-"c Then

            Else
                txt_total_harga_1.Text = CDec(temp).ToString("N0")
                txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_2_TextChanged_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_2.TextChanged
        If txt_total_harga_2.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
            ElseIf txt_total_harga_2.Text = "-"c Then

            Else
                txt_total_harga_2.Text = CDec(temp).ToString("N0")
                txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_3_TextChanged_3(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_3.TextChanged
        If txt_total_harga_3.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
            ElseIf txt_total_harga_3.Text = "-"c Then

            Else
                txt_total_harga_3.Text = CDec(temp).ToString("N0")
                txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_4_TextChanged_4(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_4.TextChanged
        If txt_total_harga_4.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
            ElseIf txt_total_harga_4.Text = "-"c Then

            Else
                txt_total_harga_4.Text = CDec(temp).ToString("N0")
                txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_5_TextChanged_5(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_5.TextChanged
        If txt_total_harga_5.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
            ElseIf txt_total_harga_5.Text = "-"c Then

            Else
                txt_total_harga_5.Text = CDec(temp).ToString("N0")
                txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_6_TextChanged_6(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_6.TextChanged
        If txt_total_harga_6.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
            ElseIf txt_total_harga_6.Text = "-"c Then

            Else
                txt_total_harga_6.Text = CDec(temp).ToString("N0")
                txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_7_TextChanged_7(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_7.TextChanged
        If txt_total_harga_7.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
            ElseIf txt_total_harga_7.Text = "-"c Then

            Else
                txt_total_harga_7.Text = CDec(temp).ToString("N0")
                txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_8_TextChanged_8(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_8.TextChanged
        If txt_total_harga_8.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
            ElseIf txt_total_harga_8.Text = "-"c Then

            Else
                txt_total_harga_8.Text = CDec(temp).ToString("N0")
                txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_9_TextChanged_9(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_9.TextChanged
        If txt_total_harga_9.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
            ElseIf txt_total_harga_9.Text = "-"c Then

            Else
                txt_total_harga_9.Text = CDec(temp).ToString("N0")
                txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_10_TextChanged_10(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_10.TextChanged
        If txt_total_harga_10.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
            ElseIf txt_total_harga_10.Text = "-"c Then

            Else
                txt_total_harga_10.Text = CDec(temp).ToString("N0")
                txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub btn_tambah_baris_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_1.Click
        Try
            If txt_customer.Text = "" Then
                MsgBox("Input Customer Terlebih Dahulu")
                txt_customer.Focus()
            ElseIf txt_sj_penjualan.Text = "" Then
                MsgBox("Input Surat Jalan Penjualan Terlebih Dahulu")
                txt_sj_penjualan.Focus()
            ElseIf cb_stok_1.Text = "-- Pilih Stok --" Then
                MsgBox("Stok Baris 1 Belum Dipilih")
                cb_stok_1.Focus()
            ElseIf txt_sj_packing_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
                txt_sj_packing_1.Focus()
            ElseIf txt_gulung_1.Text = "" Then
                MsgBox("Gulung Tidak Boleh Kosong")
                txt_gulung_1.Focus()
            ElseIf Val(txt_yards_1.Text) = 0 Then
                MsgBox("Yards Tidak Boleh Kosong")
                txt_yards_1.Focus()
            ElseIf txt_harga_1.Text = "" Then
                MsgBox("Harga Tidak Boleh Kosong")
                txt_harga_1.Focus()
            Else
                Panel10.Enabled = False
                Panel_1.Enabled = False
                btn_tambah_baris_1.Visible = False
                btn_selesai_1.Visible = False
                btn_batal_1.Visible = False
                Panel_2.Visible = True
                btn_hapus_2.Visible = True
                btn_tambah_baris_2.Visible = True
                btn_selesai_2.Visible = True
                btn_batal_2.Visible = True
                Call isiidpenjualan_1()
                Call isiidpiutang_1()
                Call simpan_tbsuratjalanpenjualan_1()
                Call simpan_tbdetilpiutang_1()
                If cb_stok_1.Text = "Grade A" Then
                    Call simpan_update_tbstokgradea_1()
                ElseIf cb_stok_1.Text = "Grade B" Then
                    Call simpan_update_tbstokgradeb_1()
                ElseIf cb_stok_1.Text = "Claim Jadi" Then
                    Call simpan_update_tbstokclaimjadi_1()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_2.Click
        Try
           If cb_stok_2.Text = "-- Pilih Stok --" Then
                MsgBox("Stok Baris 2 Belum Dipilih")
                cb_stok_2.Focus()
            ElseIf txt_sj_packing_2.Text = "" Then
                MsgBox("Baris 2 Belum Diisi Data")
                txt_sj_packing_2.Focus()
            ElseIf txt_gulung_2.Text = "" Then
                MsgBox("Gulung Tidak Boleh Kosong")
                txt_gulung_2.Focus()
            ElseIf Val(txt_yards_2.Text) = 0 Then
                MsgBox("Yards Tidak Boleh Kosong")
                txt_yards_2.Focus()
            ElseIf txt_harga_2.Text = "" Then
                MsgBox("Harga Tidak Boleh Kosong")
                txt_harga_2.Focus()
            Else
                Panel_2.Enabled = False
                btn_tambah_baris_2.Visible = False
                btn_selesai_2.Visible = False
                btn_batal_2.Visible = False
                Panel_3.Visible = True
                btn_hapus_3.Visible = True
                btn_tambah_baris_3.Visible = True
                btn_selesai_3.Visible = True
                btn_batal_3.Visible = True
                Call isiidpenjualan_2()
                Call isiidpiutang_2()
                Call simpan_tbsuratjalanpenjualan_2()
                Call simpan_tbdetilpiutang_2()
                If cb_stok_2.Text = "Grade A" Then
                    Call simpan_update_tbstokgradea_2()
                ElseIf cb_stok_2.Text = "Grade B" Then
                    Call simpan_update_tbstokgradeb_2()
                ElseIf cb_stok_2.Text = "Claim Jadi" Then
                    Call simpan_update_tbstokclaimjadi_2()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_selesai_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_1.Click
        Try
            If txt_customer.Text = "" Then
                MsgBox("Input Customer Terlebih Dahulu")
                txt_customer.Focus()
            ElseIf txt_sj_penjualan.Text = "" Then
                MsgBox("Input Surat Jalan Penjualan Terlebih Dahulu")
                txt_sj_penjualan.Focus()
            ElseIf cb_stok_1.Text = "-- Pilih Stok --" Then
                MsgBox("Stok Belum Dipilih")
                cb_stok_1.Focus()
            ElseIf txt_sj_packing_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
                txt_sj_packing_1.Focus()
            ElseIf txt_gulung_1.Text = "" Then
                MsgBox("Gulung Tidak Boleh Kosong")
                txt_gulung_1.Focus()
            ElseIf Val(txt_yards_1.Text) = 0 Then
                MsgBox("Yards Tidak Boleh Kosong")
                txt_yards_1.Focus()
            ElseIf txt_harga_1.Text = "" Then
                MsgBox("Harga Tidak Boleh Kosong")
                txt_harga_1.Focus()
            Else
                Call isiidpenjualan_1()
                Call isiidpiutang_1()
                Call simpan_tbsuratjalanpenjualan_1()
                Call simpan_tbdetilpiutang_1()
                If cb_stok_1.Text = "Grade A" Then
                    Call simpan_update_tbstokgradea_1()
                ElseIf cb_stok_1.Text = "Grade B" Then
                    Call simpan_update_tbstokgradeb_1()
                ElseIf cb_stok_1.Text = "Claim Jadi" Then
                    Call simpan_update_tbstokclaimjadi_1()
                End If
                MsgBox("SURAT JALAN PENJUALAN Baru Berhasil Disimpan")
                form_surat_jalan_penjualan.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_2.Click
        'Try
        If cb_stok_2.Text = "Grade A" Or cb_stok_2.Text = "Grade B" Or cb_stok_2.Text = "Claim Jadi" Then
            If txt_sj_packing_2.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
                txt_sj_packing_2.Focus()
            ElseIf txt_gulung_2.Text = "" Then
                MsgBox("Gulung Tidak Boleh Kosong")
                txt_gulung_2.Focus()
            ElseIf Val(txt_yards_2.Text) = 0 Then
                MsgBox("Yards Tidak Boleh Kosong")
                txt_yards_2.Focus()
            ElseIf txt_harga_2.Text = "" Then
                MsgBox("Harga Tidak Boleh Kosong")
                txt_harga_2.Focus()
            Else
                Call isiidpenjualan_2()
                Call isiidpiutang_2()
                Call simpan_tbsuratjalanpenjualan_2()
                Call simpan_tbdetilpiutang_2()
                If cb_stok_2.Text = "Grade A" Then
                    Call simpan_update_tbstokgradea_2()
                ElseIf cb_stok_2.Text = "Grade B" Then
                    Call simpan_update_tbstokgradeb_2()
                ElseIf cb_stok_2.Text = "Claim Jadi" Then
                    Call simpan_update_tbstokclaimjadi_2()
                End If
                MsgBox("SURAT JALAN PENJUALAN Baru Berhasil Disimpan")
                form_surat_jalan_penjualan.ts_perbarui.PerformClick()
                Me.Close()
            End If
        ElseIf cb_stok_2.Text = "-- Pilih Stok --" Then
            Call isiidpenjualan_2()
            Call isiidpiutang_2()
            Call simpan_tbsuratjalanpenjualan_2()
            Call simpan_tbdetilpiutang_2()
            If cb_stok_2.Text = "Grade A" Then
                Call simpan_update_tbstokgradea_2()
            ElseIf cb_stok_2.Text = "Grade B" Then
                Call simpan_update_tbstokgradeb_2()
            ElseIf cb_stok_2.Text = "Claim Jadi" Then
                Call simpan_update_tbstokclaimjadi_2()
            End If
            MsgBox("SURAT JALAN PENJUALAN Baru Berhasil Disimpan")
            form_surat_jalan_penjualan.ts_perbarui.PerformClick()
            Me.Close()
        End If
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
    End Sub
   
    
    Private Sub btn_batal_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_1.Click
        Try
            form_surat_jalan_penjualan.Show()
            form_surat_jalan_penjualan.Focus()
            form_surat_jalan_penjualan.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_hapus_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_2.Click

    End Sub

End Class