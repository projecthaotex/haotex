﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_warna_resep
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_baru = New System.Windows.Forms.ToolStripButton()
        Me.ts_ubah = New System.Windows.Forms.ToolStripButton()
        Me.ts_hapus = New System.Windows.Forms.ToolStripButton()
        Me.ts_perbarui = New System.Windows.Forms.ToolStripButton()
        Me.ts_keluar = New System.Windows.Forms.ToolStripButton()
        Me.txt_cari_warna = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtForm = New System.Windows.Forms.TextBox()
        Me.Dgv1 = New System.Windows.Forms.DataGridView()
        Me.txt_cari_resep = New System.Windows.Forms.TextBox()
        Me.BtnOk = New System.Windows.Forms.Button()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.Dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_baru, Me.ts_ubah, Me.ts_hapus, Me.ts_perbarui, Me.ts_keluar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(398, 25)
        Me.ToolStrip1.TabIndex = 32
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_baru
        '
        Me.ts_baru.Image = Global.HAOTEX.My.Resources.Resources.application_add
        Me.ts_baru.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_baru.Name = "ts_baru"
        Me.ts_baru.Size = New System.Drawing.Size(51, 22)
        Me.ts_baru.Text = "Baru"
        '
        'ts_ubah
        '
        Me.ts_ubah.Image = Global.HAOTEX.My.Resources.Resources.application_edit
        Me.ts_ubah.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_ubah.Name = "ts_ubah"
        Me.ts_ubah.Size = New System.Drawing.Size(55, 22)
        Me.ts_ubah.Text = "Ubah"
        '
        'ts_hapus
        '
        Me.ts_hapus.Image = Global.HAOTEX.My.Resources.Resources.application_remove
        Me.ts_hapus.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_hapus.Name = "ts_hapus"
        Me.ts_hapus.Size = New System.Drawing.Size(61, 22)
        Me.ts_hapus.Text = "Hapus"
        '
        'ts_perbarui
        '
        Me.ts_perbarui.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.ts_perbarui.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_perbarui.Name = "ts_perbarui"
        Me.ts_perbarui.Size = New System.Drawing.Size(71, 22)
        Me.ts_perbarui.Text = "Perbarui"
        '
        'ts_keluar
        '
        Me.ts_keluar.Image = Global.HAOTEX.My.Resources.Resources.action_delete
        Me.ts_keluar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_keluar.Name = "ts_keluar"
        Me.ts_keluar.Size = New System.Drawing.Size(60, 22)
        Me.ts_keluar.Text = "Keluar"
        '
        'txt_cari_warna
        '
        Me.txt_cari_warna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_warna.Location = New System.Drawing.Point(110, 113)
        Me.txt_cari_warna.Name = "txt_cari_warna"
        Me.txt_cari_warna.Size = New System.Drawing.Size(154, 20)
        Me.txt_cari_warna.TabIndex = 29
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(89, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(217, 20)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "INPUT WARNA DAN RESEP"
        '
        'TxtForm
        '
        Me.TxtForm.Location = New System.Drawing.Point(224, 193)
        Me.TxtForm.Name = "TxtForm"
        Me.TxtForm.Size = New System.Drawing.Size(111, 20)
        Me.TxtForm.TabIndex = 31
        '
        'Dgv1
        '
        Me.Dgv1.AllowUserToAddRows = False
        Me.Dgv1.AllowUserToDeleteRows = False
        Me.Dgv1.AllowUserToResizeColumns = False
        Me.Dgv1.AllowUserToResizeRows = False
        Me.Dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.Dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.Dgv1.Location = New System.Drawing.Point(22, 183)
        Me.Dgv1.MultiSelect = False
        Me.Dgv1.Name = "Dgv1"
        Me.Dgv1.ReadOnly = True
        Me.Dgv1.RowHeadersVisible = False
        Me.Dgv1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.Dgv1.Size = New System.Drawing.Size(355, 434)
        Me.Dgv1.TabIndex = 27
        '
        'txt_cari_resep
        '
        Me.txt_cari_resep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_resep.Location = New System.Drawing.Point(110, 141)
        Me.txt_cari_resep.Name = "txt_cari_resep"
        Me.txt_cari_resep.Size = New System.Drawing.Size(154, 20)
        Me.txt_cari_resep.TabIndex = 34
        '
        'BtnOk
        '
        Me.BtnOk.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnOk.Image = Global.HAOTEX.My.Resources.Resources.action_check
        Me.BtnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnOk.Location = New System.Drawing.Point(296, 110)
        Me.BtnOk.Name = "BtnOk"
        Me.BtnOk.Size = New System.Drawing.Size(62, 27)
        Me.BtnOk.TabIndex = 30
        Me.BtnOk.Text = "OK"
        Me.BtnOk.UseVisualStyleBackColor = True
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(110, 85)
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(154, 20)
        Me.txt_cari_jenis_kain.TabIndex = 35
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(36, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Jenis Kain"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(36, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Warna"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(36, 145)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 38
        Me.Label4.Text = "Resep"
        '
        'form_input_warna_resep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(398, 638)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Controls.Add(Me.txt_cari_resep)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.BtnOk)
        Me.Controls.Add(Me.txt_cari_warna)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Dgv1)
        Me.Controls.Add(Me.TxtForm)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "form_input_warna_resep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.Dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_baru As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_ubah As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_hapus As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_keluar As System.Windows.Forms.ToolStripButton
    Friend WithEvents BtnOk As System.Windows.Forms.Button
    Friend WithEvents txt_cari_warna As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtForm As System.Windows.Forms.TextBox
    Friend WithEvents Dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents txt_cari_resep As System.Windows.Forms.TextBox
    Friend WithEvents ts_perbarui As System.Windows.Forms.ToolStripButton
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
