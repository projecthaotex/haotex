﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_no_kontrak
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.TxtForm = New System.Windows.Forms.TextBox()
        Me.dtp_akhir = New System.Windows.Forms.DateTimePicker()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_supplier = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(12, 59)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(1120, 380)
        Me.dgv1.TabIndex = 0
        '
        'TxtForm
        '
        Me.TxtForm.Location = New System.Drawing.Point(749, 12)
        Me.TxtForm.Name = "TxtForm"
        Me.TxtForm.Size = New System.Drawing.Size(100, 20)
        Me.TxtForm.TabIndex = 1
        '
        'dtp_akhir
        '
        Me.dtp_akhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_akhir.Location = New System.Drawing.Point(314, 10)
        Me.dtp_akhir.Name = "dtp_akhir"
        Me.dtp_akhir.Size = New System.Drawing.Size(99, 20)
        Me.dtp_akhir.TabIndex = 7
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(174, 10)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 6
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(774, 10)
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 9
        Me.txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Me.txt_cari_jenis_kain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_cari_supplier
        '
        Me.txt_cari_supplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_supplier.Location = New System.Drawing.Point(584, 10)
        Me.txt_cari_supplier.Name = "txt_cari_supplier"
        Me.txt_cari_supplier.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_supplier.TabIndex = 8
        Me.txt_cari_supplier.Text = "< Supplier >"
        Me.txt_cari_supplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_cari_supplier)
        Me.Panel1.Controls.Add(Me.dtp_akhir)
        Me.Panel1.Controls.Add(Me.dtp_awal)
        Me.Panel1.Location = New System.Drawing.Point(12, 11)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1120, 42)
        Me.Panel1.TabIndex = 12
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(282, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "s/d"
        '
        'form_input_no_kontrak
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1144, 448)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TxtForm)
        Me.Controls.Add(Me.dgv1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_no_kontrak"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM INPUT NO KONTRAK"
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents TxtForm As System.Windows.Forms.TextBox
    Friend WithEvents dtp_akhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_supplier As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
