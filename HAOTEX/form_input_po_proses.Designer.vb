﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_po_proses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_input_po_proses))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_no_po = New System.Windows.Forms.TextBox()
        Me.txt_gudang = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel_1 = New System.Windows.Forms.Panel()
        Me.cb_satuan_1 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_1 = New System.Windows.Forms.TextBox()
        Me.txt_note_1 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_1 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_1 = New System.Windows.Forms.TextBox()
        Me.txt_resep_1 = New System.Windows.Forms.TextBox()
        Me.txt_warna_1 = New System.Windows.Forms.TextBox()
        Me.txt_qty_1 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_1 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_1 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_1 = New System.Windows.Forms.Label()
        Me.btn_tambah_baris_1 = New System.Windows.Forms.Button()
        Me.btn_selesai_1 = New System.Windows.Forms.Button()
        Me.Panel_2 = New System.Windows.Forms.Panel()
        Me.cb_satuan_2 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_2 = New System.Windows.Forms.TextBox()
        Me.txt_note_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_2 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_2 = New System.Windows.Forms.TextBox()
        Me.txt_resep_2 = New System.Windows.Forms.TextBox()
        Me.txt_warna_2 = New System.Windows.Forms.TextBox()
        Me.txt_qty_2 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_2 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_2 = New System.Windows.Forms.Label()
        Me.btn_hapus_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_2 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_2 = New System.Windows.Forms.Button()
        Me.btn_hapus_4 = New System.Windows.Forms.Button()
        Me.btn_selesai_4 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_4 = New System.Windows.Forms.Button()
        Me.btn_hapus_3 = New System.Windows.Forms.Button()
        Me.btn_selesai_3 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_3 = New System.Windows.Forms.Button()
        Me.btn_hapus_8 = New System.Windows.Forms.Button()
        Me.btn_selesai_8 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_8 = New System.Windows.Forms.Button()
        Me.btn_hapus_7 = New System.Windows.Forms.Button()
        Me.btn_selesai_7 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_7 = New System.Windows.Forms.Button()
        Me.btn_hapus_6 = New System.Windows.Forms.Button()
        Me.btn_selesai_6 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_6 = New System.Windows.Forms.Button()
        Me.btn_hapus_5 = New System.Windows.Forms.Button()
        Me.btn_selesai_5 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_5 = New System.Windows.Forms.Button()
        Me.btn_hapus_10 = New System.Windows.Forms.Button()
        Me.btn_selesai_10 = New System.Windows.Forms.Button()
        Me.btn_hapus_9 = New System.Windows.Forms.Button()
        Me.btn_selesai_9 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_9 = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btn_batal_10 = New System.Windows.Forms.Button()
        Me.btn_batal_9 = New System.Windows.Forms.Button()
        Me.btn_batal_8 = New System.Windows.Forms.Button()
        Me.btn_batal_7 = New System.Windows.Forms.Button()
        Me.btn_batal_6 = New System.Windows.Forms.Button()
        Me.btn_batal_5 = New System.Windows.Forms.Button()
        Me.btn_batal_4 = New System.Windows.Forms.Button()
        Me.btn_batal_3 = New System.Windows.Forms.Button()
        Me.btn_batal_2 = New System.Windows.Forms.Button()
        Me.btn_batal_1 = New System.Windows.Forms.Button()
        Me.Panel_3 = New System.Windows.Forms.Panel()
        Me.cb_satuan_3 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_3 = New System.Windows.Forms.TextBox()
        Me.txt_note_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_3 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_3 = New System.Windows.Forms.TextBox()
        Me.txt_resep_3 = New System.Windows.Forms.TextBox()
        Me.txt_warna_3 = New System.Windows.Forms.TextBox()
        Me.txt_qty_3 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_3 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_3 = New System.Windows.Forms.Label()
        Me.Panel_4 = New System.Windows.Forms.Panel()
        Me.cb_satuan_4 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_4 = New System.Windows.Forms.TextBox()
        Me.txt_note_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_4 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_4 = New System.Windows.Forms.TextBox()
        Me.txt_resep_4 = New System.Windows.Forms.TextBox()
        Me.txt_warna_4 = New System.Windows.Forms.TextBox()
        Me.txt_qty_4 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_4 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_4 = New System.Windows.Forms.Label()
        Me.Panel_5 = New System.Windows.Forms.Panel()
        Me.cb_satuan_5 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_5 = New System.Windows.Forms.TextBox()
        Me.txt_note_5 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_5 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_5 = New System.Windows.Forms.TextBox()
        Me.txt_resep_5 = New System.Windows.Forms.TextBox()
        Me.txt_warna_5 = New System.Windows.Forms.TextBox()
        Me.txt_qty_5 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_5 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_5 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_5 = New System.Windows.Forms.Label()
        Me.Panel_10 = New System.Windows.Forms.Panel()
        Me.cb_satuan_10 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_10 = New System.Windows.Forms.TextBox()
        Me.txt_note_10 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_10 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_10 = New System.Windows.Forms.TextBox()
        Me.txt_resep_10 = New System.Windows.Forms.TextBox()
        Me.txt_warna_10 = New System.Windows.Forms.TextBox()
        Me.txt_qty_10 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_10 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_10 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_10 = New System.Windows.Forms.Label()
        Me.Panel_9 = New System.Windows.Forms.Panel()
        Me.cb_satuan_9 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_9 = New System.Windows.Forms.TextBox()
        Me.txt_note_9 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_9 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_9 = New System.Windows.Forms.TextBox()
        Me.txt_resep_9 = New System.Windows.Forms.TextBox()
        Me.txt_warna_9 = New System.Windows.Forms.TextBox()
        Me.txt_qty_9 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_9 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_9 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_9 = New System.Windows.Forms.Label()
        Me.Panel_8 = New System.Windows.Forms.Panel()
        Me.cb_satuan_8 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_8 = New System.Windows.Forms.TextBox()
        Me.txt_note_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_8 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_8 = New System.Windows.Forms.TextBox()
        Me.txt_resep_8 = New System.Windows.Forms.TextBox()
        Me.txt_warna_8 = New System.Windows.Forms.TextBox()
        Me.txt_qty_8 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_8 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_8 = New System.Windows.Forms.Label()
        Me.Panel_7 = New System.Windows.Forms.Panel()
        Me.cb_satuan_7 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_7 = New System.Windows.Forms.TextBox()
        Me.txt_note_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_7 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_7 = New System.Windows.Forms.TextBox()
        Me.txt_resep_7 = New System.Windows.Forms.TextBox()
        Me.txt_warna_7 = New System.Windows.Forms.TextBox()
        Me.txt_qty_7 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_7 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_7 = New System.Windows.Forms.Label()
        Me.Panel_6 = New System.Windows.Forms.Panel()
        Me.cb_satuan_6 = New System.Windows.Forms.ComboBox()
        Me.txt_hand_fill_6 = New System.Windows.Forms.TextBox()
        Me.txt_note_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_keterangan_6 = New System.Windows.Forms.TextBox()
        Me.txt_tarik_lebar_6 = New System.Windows.Forms.TextBox()
        Me.txt_resep_6 = New System.Windows.Forms.TextBox()
        Me.txt_warna_6 = New System.Windows.Forms.TextBox()
        Me.txt_qty_6 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_6 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_6 = New System.Windows.Forms.Label()
        Me.txt_note_po = New System.Windows.Forms.RichTextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_qty_asal_10 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_9 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_8 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_7 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_6 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_5 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_4 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_3 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_2 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_1 = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_id_grey_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_1 = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.txt_id_beli_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_9 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_id_beli_1 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_beli_5 = New System.Windows.Forms.TextBox()
        Me.txt_harga_1 = New System.Windows.Forms.TextBox()
        Me.txt_harga_2 = New System.Windows.Forms.TextBox()
        Me.txt_harga_3 = New System.Windows.Forms.TextBox()
        Me.txt_harga_4 = New System.Windows.Forms.TextBox()
        Me.txt_harga_5 = New System.Windows.Forms.TextBox()
        Me.txt_harga_6 = New System.Windows.Forms.TextBox()
        Me.txt_harga_7 = New System.Windows.Forms.TextBox()
        Me.txt_harga_8 = New System.Windows.Forms.TextBox()
        Me.txt_harga_9 = New System.Windows.Forms.TextBox()
        Me.txt_harga_10 = New System.Windows.Forms.TextBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txt_id_po_proses_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_proses_1 = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel_1.SuspendLayout()
        Me.Panel_2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel_3.SuspendLayout()
        Me.Panel_4.SuspendLayout()
        Me.Panel_5.SuspendLayout()
        Me.Panel_10.SuspendLayout()
        Me.Panel_9.SuspendLayout()
        Me.Panel_8.SuspendLayout()
        Me.Panel_7.SuspendLayout()
        Me.Panel_6.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.Controls.Add(Me.Panel10)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(-3, 6)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1300, 120)
        Me.Panel1.TabIndex = 0
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel10.Controls.Add(Me.dtp_awal)
        Me.Panel10.Controls.Add(Me.Label2)
        Me.Panel10.Controls.Add(Me.Label3)
        Me.Panel10.Controls.Add(Me.Label4)
        Me.Panel10.Controls.Add(Me.txt_no_po)
        Me.Panel10.Controls.Add(Me.txt_gudang)
        Me.Panel10.Location = New System.Drawing.Point(500, 29)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(300, 88)
        Me.Panel10.TabIndex = 79
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(119, 6)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(29, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tanggal"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(29, 62)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Gudang"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(29, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "No. PO"
        '
        'txt_no_po
        '
        Me.txt_no_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po.Location = New System.Drawing.Point(119, 33)
        Me.txt_no_po.Name = "txt_no_po"
        Me.txt_no_po.Size = New System.Drawing.Size(151, 20)
        Me.txt_no_po.TabIndex = 0
        '
        'txt_gudang
        '
        Me.txt_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang.Location = New System.Drawing.Point(119, 60)
        Me.txt_gudang.Name = "txt_gudang"
        Me.txt_gudang.Size = New System.Drawing.Size(151, 20)
        Me.txt_gudang.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.Font = New System.Drawing.Font("Cambria", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Window
        Me.Label1.Location = New System.Drawing.Point(500, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(300, 26)
        Me.Label1.TabIndex = 78
        Me.Label1.Text = "PO PROSES"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1179, 12)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(108, 20)
        Me.dtp_hari_ini.TabIndex = 73
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label17)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Location = New System.Drawing.Point(-3, 131)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1300, 30)
        Me.Panel2.TabIndex = 1
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.Window
        Me.Label17.Location = New System.Drawing.Point(395, 8)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(47, 13)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "Satuan"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.Window
        Me.Label14.Location = New System.Drawing.Point(1136, 8)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(34, 13)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Note"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Window
        Me.Label13.Location = New System.Drawing.Point(918, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 13)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Keterangan"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.Window
        Me.Label12.Location = New System.Drawing.Point(793, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(65, 13)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "Hand Feel"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.Window
        Me.Label11.Location = New System.Drawing.Point(706, 8)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(22, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "TL"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Window
        Me.Label10.Location = New System.Drawing.Point(588, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Resep"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Window
        Me.Label9.Location = New System.Drawing.Point(479, 8)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(44, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Warna"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Window
        Me.Label8.Location = New System.Drawing.Point(324, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(32, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "QTY"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Window
        Me.Label7.Location = New System.Drawing.Point(180, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Jenis Kain"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Window
        Me.Label6.Location = New System.Drawing.Point(74, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(21, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "SJ"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Window
        Me.Label5.Location = New System.Drawing.Point(11, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(27, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "No."
        '
        'Panel_1
        '
        Me.Panel_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_1.Controls.Add(Me.cb_satuan_1)
        Me.Panel_1.Controls.Add(Me.txt_hand_fill_1)
        Me.Panel_1.Controls.Add(Me.txt_note_1)
        Me.Panel_1.Controls.Add(Me.txt_asal_keterangan_1)
        Me.Panel_1.Controls.Add(Me.txt_tarik_lebar_1)
        Me.Panel_1.Controls.Add(Me.txt_resep_1)
        Me.Panel_1.Controls.Add(Me.txt_warna_1)
        Me.Panel_1.Controls.Add(Me.txt_qty_1)
        Me.Panel_1.Controls.Add(Me.txt_jenis_kain_1)
        Me.Panel_1.Controls.Add(Me.txt_asal_sj_1)
        Me.Panel_1.Controls.Add(Me.txt_no_urut_1)
        Me.Panel_1.Location = New System.Drawing.Point(-3, 160)
        Me.Panel_1.Name = "Panel_1"
        Me.Panel_1.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_1.TabIndex = 2
        '
        'cb_satuan_1
        '
        Me.cb_satuan_1.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_1.FormattingEnabled = True
        Me.cb_satuan_1.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_1.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_1.Name = "cb_satuan_1"
        Me.cb_satuan_1.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_1.TabIndex = 501
        Me.cb_satuan_1.Text = "Meter"
        '
        'txt_hand_fill_1
        '
        Me.txt_hand_fill_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_1.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_1.Name = "txt_hand_fill_1"
        Me.txt_hand_fill_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_1.TabIndex = 9
        '
        'txt_note_1
        '
        Me.txt_note_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_1.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_1.MaxLength = 200
        Me.txt_note_1.Multiline = True
        Me.txt_note_1.Name = "txt_note_1"
        Me.txt_note_1.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_1.TabIndex = 8
        '
        'txt_asal_keterangan_1
        '
        Me.txt_asal_keterangan_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_1.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_1.Name = "txt_asal_keterangan_1"
        Me.txt_asal_keterangan_1.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_1.TabIndex = 7
        '
        'txt_tarik_lebar_1
        '
        Me.txt_tarik_lebar_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_1.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_1.Name = "txt_tarik_lebar_1"
        Me.txt_tarik_lebar_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_1.TabIndex = 6
        Me.txt_tarik_lebar_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_1
        '
        Me.txt_resep_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_1.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_1.Name = "txt_resep_1"
        Me.txt_resep_1.ReadOnly = True
        Me.txt_resep_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_1.TabIndex = 5
        Me.txt_resep_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_1
        '
        Me.txt_warna_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_1.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_1.Name = "txt_warna_1"
        Me.txt_warna_1.ReadOnly = True
        Me.txt_warna_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_1.TabIndex = 4
        Me.txt_warna_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_1
        '
        Me.txt_qty_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_1.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_1.Name = "txt_qty_1"
        Me.txt_qty_1.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_1.TabIndex = 3
        Me.txt_qty_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_1
        '
        Me.txt_jenis_kain_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_1.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_1.Name = "txt_jenis_kain_1"
        Me.txt_jenis_kain_1.ReadOnly = True
        Me.txt_jenis_kain_1.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_1.TabIndex = 2
        Me.txt_jenis_kain_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_1
        '
        Me.txt_asal_sj_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_1.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_1.Name = "txt_asal_sj_1"
        Me.txt_asal_sj_1.ReadOnly = True
        Me.txt_asal_sj_1.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_1.TabIndex = 1
        Me.txt_asal_sj_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_1
        '
        Me.txt_no_urut_1.AutoSize = True
        Me.txt_no_urut_1.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_1.Name = "txt_no_urut_1"
        Me.txt_no_urut_1.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_1.TabIndex = 0
        Me.txt_no_urut_1.Text = "1"
        '
        'btn_tambah_baris_1
        '
        Me.btn_tambah_baris_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_1.Image = CType(resources.GetObject("btn_tambah_baris_1.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_1.Location = New System.Drawing.Point(106, 11)
        Me.btn_tambah_baris_1.Name = "btn_tambah_baris_1"
        Me.btn_tambah_baris_1.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_1.TabIndex = 3
        Me.btn_tambah_baris_1.Text = "Tambah Baris"
        Me.btn_tambah_baris_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_1.UseVisualStyleBackColor = False
        '
        'btn_selesai_1
        '
        Me.btn_selesai_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_1.Image = CType(resources.GetObject("btn_selesai_1.Image"), System.Drawing.Image)
        Me.btn_selesai_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_1.Location = New System.Drawing.Point(249, 11)
        Me.btn_selesai_1.Name = "btn_selesai_1"
        Me.btn_selesai_1.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_1.TabIndex = 4
        Me.btn_selesai_1.Text = "Selesai"
        Me.btn_selesai_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_1.UseVisualStyleBackColor = False
        '
        'Panel_2
        '
        Me.Panel_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_2.Controls.Add(Me.cb_satuan_2)
        Me.Panel_2.Controls.Add(Me.txt_hand_fill_2)
        Me.Panel_2.Controls.Add(Me.txt_note_2)
        Me.Panel_2.Controls.Add(Me.txt_asal_keterangan_2)
        Me.Panel_2.Controls.Add(Me.txt_tarik_lebar_2)
        Me.Panel_2.Controls.Add(Me.txt_resep_2)
        Me.Panel_2.Controls.Add(Me.txt_warna_2)
        Me.Panel_2.Controls.Add(Me.txt_qty_2)
        Me.Panel_2.Controls.Add(Me.txt_jenis_kain_2)
        Me.Panel_2.Controls.Add(Me.txt_asal_sj_2)
        Me.Panel_2.Controls.Add(Me.txt_no_urut_2)
        Me.Panel_2.Location = New System.Drawing.Point(-3, 195)
        Me.Panel_2.Name = "Panel_2"
        Me.Panel_2.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_2.TabIndex = 10
        '
        'cb_satuan_2
        '
        Me.cb_satuan_2.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_2.FormattingEnabled = True
        Me.cb_satuan_2.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_2.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_2.Name = "cb_satuan_2"
        Me.cb_satuan_2.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_2.TabIndex = 501
        Me.cb_satuan_2.Text = "Meter"
        '
        'txt_hand_fill_2
        '
        Me.txt_hand_fill_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_2.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_2.Name = "txt_hand_fill_2"
        Me.txt_hand_fill_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_2.TabIndex = 9
        '
        'txt_note_2
        '
        Me.txt_note_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_2.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_2.MaxLength = 200
        Me.txt_note_2.Multiline = True
        Me.txt_note_2.Name = "txt_note_2"
        Me.txt_note_2.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_2.TabIndex = 8
        '
        'txt_asal_keterangan_2
        '
        Me.txt_asal_keterangan_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_2.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_2.Name = "txt_asal_keterangan_2"
        Me.txt_asal_keterangan_2.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_2.TabIndex = 7
        '
        'txt_tarik_lebar_2
        '
        Me.txt_tarik_lebar_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_2.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_2.Name = "txt_tarik_lebar_2"
        Me.txt_tarik_lebar_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_2.TabIndex = 6
        Me.txt_tarik_lebar_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_2
        '
        Me.txt_resep_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_2.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_2.Name = "txt_resep_2"
        Me.txt_resep_2.ReadOnly = True
        Me.txt_resep_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_2.TabIndex = 5
        Me.txt_resep_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_2
        '
        Me.txt_warna_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_2.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_2.Name = "txt_warna_2"
        Me.txt_warna_2.ReadOnly = True
        Me.txt_warna_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_2.TabIndex = 4
        Me.txt_warna_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_2
        '
        Me.txt_qty_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_2.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_2.Name = "txt_qty_2"
        Me.txt_qty_2.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_2.TabIndex = 3
        Me.txt_qty_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_2
        '
        Me.txt_jenis_kain_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_2.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_2.Name = "txt_jenis_kain_2"
        Me.txt_jenis_kain_2.ReadOnly = True
        Me.txt_jenis_kain_2.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_2.TabIndex = 2
        Me.txt_jenis_kain_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_2
        '
        Me.txt_asal_sj_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_2.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_2.Name = "txt_asal_sj_2"
        Me.txt_asal_sj_2.ReadOnly = True
        Me.txt_asal_sj_2.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_2.TabIndex = 1
        Me.txt_asal_sj_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_2
        '
        Me.txt_no_urut_2.AutoSize = True
        Me.txt_no_urut_2.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_2.Name = "txt_no_urut_2"
        Me.txt_no_urut_2.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_2.TabIndex = 0
        Me.txt_no_urut_2.Text = "2"
        '
        'btn_hapus_2
        '
        Me.btn_hapus_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_2.Image = CType(resources.GetObject("btn_hapus_2.Image"), System.Drawing.Image)
        Me.btn_hapus_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_2.Location = New System.Drawing.Point(289, 47)
        Me.btn_hapus_2.Name = "btn_hapus_2"
        Me.btn_hapus_2.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_2.TabIndex = 13
        Me.btn_hapus_2.Text = "Hapus Baris"
        Me.btn_hapus_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_2
        '
        Me.btn_selesai_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_2.Image = CType(resources.GetObject("btn_selesai_2.Image"), System.Drawing.Image)
        Me.btn_selesai_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_2.Location = New System.Drawing.Point(181, 47)
        Me.btn_selesai_2.Name = "btn_selesai_2"
        Me.btn_selesai_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_2.TabIndex = 12
        Me.btn_selesai_2.Text = "Selesai"
        Me.btn_selesai_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_2.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_2
        '
        Me.btn_tambah_baris_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_2.Image = CType(resources.GetObject("btn_tambah_baris_2.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_2.Location = New System.Drawing.Point(38, 47)
        Me.btn_tambah_baris_2.Name = "btn_tambah_baris_2"
        Me.btn_tambah_baris_2.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_2.TabIndex = 11
        Me.btn_tambah_baris_2.Text = "Tambah Baris"
        Me.btn_tambah_baris_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_2.UseVisualStyleBackColor = False
        '
        'btn_hapus_4
        '
        Me.btn_hapus_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_4.Image = CType(resources.GetObject("btn_hapus_4.Image"), System.Drawing.Image)
        Me.btn_hapus_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_4.Location = New System.Drawing.Point(289, 119)
        Me.btn_hapus_4.Name = "btn_hapus_4"
        Me.btn_hapus_4.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_4.TabIndex = 19
        Me.btn_hapus_4.Text = "Hapus Baris"
        Me.btn_hapus_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_4.UseVisualStyleBackColor = False
        '
        'btn_selesai_4
        '
        Me.btn_selesai_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_4.Image = CType(resources.GetObject("btn_selesai_4.Image"), System.Drawing.Image)
        Me.btn_selesai_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_4.Location = New System.Drawing.Point(181, 119)
        Me.btn_selesai_4.Name = "btn_selesai_4"
        Me.btn_selesai_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_4.TabIndex = 18
        Me.btn_selesai_4.Text = "Selesai"
        Me.btn_selesai_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_4.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_4
        '
        Me.btn_tambah_baris_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_4.Image = CType(resources.GetObject("btn_tambah_baris_4.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_4.Location = New System.Drawing.Point(38, 119)
        Me.btn_tambah_baris_4.Name = "btn_tambah_baris_4"
        Me.btn_tambah_baris_4.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_4.TabIndex = 17
        Me.btn_tambah_baris_4.Text = "Tambah Baris"
        Me.btn_tambah_baris_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_4.UseVisualStyleBackColor = False
        '
        'btn_hapus_3
        '
        Me.btn_hapus_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_3.Image = CType(resources.GetObject("btn_hapus_3.Image"), System.Drawing.Image)
        Me.btn_hapus_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_3.Location = New System.Drawing.Point(289, 83)
        Me.btn_hapus_3.Name = "btn_hapus_3"
        Me.btn_hapus_3.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_3.TabIndex = 16
        Me.btn_hapus_3.Text = "Hapus Baris"
        Me.btn_hapus_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_3.UseVisualStyleBackColor = False
        '
        'btn_selesai_3
        '
        Me.btn_selesai_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_3.Image = CType(resources.GetObject("btn_selesai_3.Image"), System.Drawing.Image)
        Me.btn_selesai_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_3.Location = New System.Drawing.Point(181, 83)
        Me.btn_selesai_3.Name = "btn_selesai_3"
        Me.btn_selesai_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_3.TabIndex = 15
        Me.btn_selesai_3.Text = "Selesai"
        Me.btn_selesai_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_3.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_3
        '
        Me.btn_tambah_baris_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_3.Image = CType(resources.GetObject("btn_tambah_baris_3.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_3.Location = New System.Drawing.Point(38, 83)
        Me.btn_tambah_baris_3.Name = "btn_tambah_baris_3"
        Me.btn_tambah_baris_3.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_3.TabIndex = 14
        Me.btn_tambah_baris_3.Text = "Tambah Baris"
        Me.btn_tambah_baris_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_3.UseVisualStyleBackColor = False
        '
        'btn_hapus_8
        '
        Me.btn_hapus_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_8.Image = CType(resources.GetObject("btn_hapus_8.Image"), System.Drawing.Image)
        Me.btn_hapus_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_8.Location = New System.Drawing.Point(289, 257)
        Me.btn_hapus_8.Name = "btn_hapus_8"
        Me.btn_hapus_8.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_8.TabIndex = 31
        Me.btn_hapus_8.Text = "Hapus Baris"
        Me.btn_hapus_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_8.UseVisualStyleBackColor = False
        '
        'btn_selesai_8
        '
        Me.btn_selesai_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_8.Image = CType(resources.GetObject("btn_selesai_8.Image"), System.Drawing.Image)
        Me.btn_selesai_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_8.Location = New System.Drawing.Point(181, 257)
        Me.btn_selesai_8.Name = "btn_selesai_8"
        Me.btn_selesai_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_8.TabIndex = 30
        Me.btn_selesai_8.Text = "Selesai"
        Me.btn_selesai_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_8.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_8
        '
        Me.btn_tambah_baris_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_8.Image = CType(resources.GetObject("btn_tambah_baris_8.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_8.Location = New System.Drawing.Point(38, 257)
        Me.btn_tambah_baris_8.Name = "btn_tambah_baris_8"
        Me.btn_tambah_baris_8.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_8.TabIndex = 29
        Me.btn_tambah_baris_8.Text = "Tambah Baris"
        Me.btn_tambah_baris_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_8.UseVisualStyleBackColor = False
        '
        'btn_hapus_7
        '
        Me.btn_hapus_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_7.Image = CType(resources.GetObject("btn_hapus_7.Image"), System.Drawing.Image)
        Me.btn_hapus_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_7.Location = New System.Drawing.Point(289, 223)
        Me.btn_hapus_7.Name = "btn_hapus_7"
        Me.btn_hapus_7.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_7.TabIndex = 28
        Me.btn_hapus_7.Text = "Hapus Baris"
        Me.btn_hapus_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_7.UseVisualStyleBackColor = False
        '
        'btn_selesai_7
        '
        Me.btn_selesai_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_7.Image = CType(resources.GetObject("btn_selesai_7.Image"), System.Drawing.Image)
        Me.btn_selesai_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_7.Location = New System.Drawing.Point(181, 223)
        Me.btn_selesai_7.Name = "btn_selesai_7"
        Me.btn_selesai_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_7.TabIndex = 27
        Me.btn_selesai_7.Text = "Selesai"
        Me.btn_selesai_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_7.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_7
        '
        Me.btn_tambah_baris_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_7.Image = CType(resources.GetObject("btn_tambah_baris_7.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_7.Location = New System.Drawing.Point(38, 223)
        Me.btn_tambah_baris_7.Name = "btn_tambah_baris_7"
        Me.btn_tambah_baris_7.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_7.TabIndex = 26
        Me.btn_tambah_baris_7.Text = "Tambah Baris"
        Me.btn_tambah_baris_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_7.UseVisualStyleBackColor = False
        '
        'btn_hapus_6
        '
        Me.btn_hapus_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_6.Image = CType(resources.GetObject("btn_hapus_6.Image"), System.Drawing.Image)
        Me.btn_hapus_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_6.Location = New System.Drawing.Point(289, 188)
        Me.btn_hapus_6.Name = "btn_hapus_6"
        Me.btn_hapus_6.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_6.TabIndex = 25
        Me.btn_hapus_6.Text = "Hapus Baris"
        Me.btn_hapus_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_6.UseVisualStyleBackColor = False
        '
        'btn_selesai_6
        '
        Me.btn_selesai_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_6.Image = CType(resources.GetObject("btn_selesai_6.Image"), System.Drawing.Image)
        Me.btn_selesai_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_6.Location = New System.Drawing.Point(181, 188)
        Me.btn_selesai_6.Name = "btn_selesai_6"
        Me.btn_selesai_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_6.TabIndex = 24
        Me.btn_selesai_6.Text = "Selesai"
        Me.btn_selesai_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_6.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_6
        '
        Me.btn_tambah_baris_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_6.Image = CType(resources.GetObject("btn_tambah_baris_6.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_6.Location = New System.Drawing.Point(38, 188)
        Me.btn_tambah_baris_6.Name = "btn_tambah_baris_6"
        Me.btn_tambah_baris_6.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_6.TabIndex = 23
        Me.btn_tambah_baris_6.Text = "Tambah Baris"
        Me.btn_tambah_baris_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_6.UseVisualStyleBackColor = False
        '
        'btn_hapus_5
        '
        Me.btn_hapus_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_5.Image = CType(resources.GetObject("btn_hapus_5.Image"), System.Drawing.Image)
        Me.btn_hapus_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_5.Location = New System.Drawing.Point(289, 154)
        Me.btn_hapus_5.Name = "btn_hapus_5"
        Me.btn_hapus_5.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_5.TabIndex = 22
        Me.btn_hapus_5.Text = "Hapus Baris"
        Me.btn_hapus_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_5.UseVisualStyleBackColor = False
        '
        'btn_selesai_5
        '
        Me.btn_selesai_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_5.Image = CType(resources.GetObject("btn_selesai_5.Image"), System.Drawing.Image)
        Me.btn_selesai_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_5.Location = New System.Drawing.Point(181, 154)
        Me.btn_selesai_5.Name = "btn_selesai_5"
        Me.btn_selesai_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_5.TabIndex = 21
        Me.btn_selesai_5.Text = "Selesai"
        Me.btn_selesai_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_5.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_5
        '
        Me.btn_tambah_baris_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_5.Image = CType(resources.GetObject("btn_tambah_baris_5.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_5.Location = New System.Drawing.Point(38, 154)
        Me.btn_tambah_baris_5.Name = "btn_tambah_baris_5"
        Me.btn_tambah_baris_5.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_5.TabIndex = 20
        Me.btn_tambah_baris_5.Text = "Tambah Baris"
        Me.btn_tambah_baris_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_5.UseVisualStyleBackColor = False
        '
        'btn_hapus_10
        '
        Me.btn_hapus_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_10.Image = CType(resources.GetObject("btn_hapus_10.Image"), System.Drawing.Image)
        Me.btn_hapus_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_10.Location = New System.Drawing.Point(218, 329)
        Me.btn_hapus_10.Name = "btn_hapus_10"
        Me.btn_hapus_10.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_10.TabIndex = 37
        Me.btn_hapus_10.Text = "Hapus Baris"
        Me.btn_hapus_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_10.UseVisualStyleBackColor = False
        '
        'btn_selesai_10
        '
        Me.btn_selesai_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_10.Image = CType(resources.GetObject("btn_selesai_10.Image"), System.Drawing.Image)
        Me.btn_selesai_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_10.Location = New System.Drawing.Point(110, 329)
        Me.btn_selesai_10.Name = "btn_selesai_10"
        Me.btn_selesai_10.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_10.TabIndex = 36
        Me.btn_selesai_10.Text = "Selesai"
        Me.btn_selesai_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_10.UseVisualStyleBackColor = False
        '
        'btn_hapus_9
        '
        Me.btn_hapus_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_9.Image = CType(resources.GetObject("btn_hapus_9.Image"), System.Drawing.Image)
        Me.btn_hapus_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_9.Location = New System.Drawing.Point(289, 293)
        Me.btn_hapus_9.Name = "btn_hapus_9"
        Me.btn_hapus_9.Size = New System.Drawing.Size(103, 23)
        Me.btn_hapus_9.TabIndex = 34
        Me.btn_hapus_9.Text = "Hapus Baris"
        Me.btn_hapus_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_9.UseVisualStyleBackColor = False
        '
        'btn_selesai_9
        '
        Me.btn_selesai_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_9.Image = CType(resources.GetObject("btn_selesai_9.Image"), System.Drawing.Image)
        Me.btn_selesai_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_9.Location = New System.Drawing.Point(181, 293)
        Me.btn_selesai_9.Name = "btn_selesai_9"
        Me.btn_selesai_9.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_9.TabIndex = 33
        Me.btn_selesai_9.Text = "Selesai"
        Me.btn_selesai_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_9.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_9
        '
        Me.btn_tambah_baris_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_9.Image = CType(resources.GetObject("btn_tambah_baris_9.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_9.Location = New System.Drawing.Point(38, 293)
        Me.btn_tambah_baris_9.Name = "btn_tambah_baris_9"
        Me.btn_tambah_baris_9.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_9.TabIndex = 32
        Me.btn_tambah_baris_9.Text = "Tambah Baris"
        Me.btn_tambah_baris_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_9.UseVisualStyleBackColor = False
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btn_batal_10)
        Me.Panel3.Controls.Add(Me.btn_batal_9)
        Me.Panel3.Controls.Add(Me.btn_batal_8)
        Me.Panel3.Controls.Add(Me.btn_batal_7)
        Me.Panel3.Controls.Add(Me.btn_batal_6)
        Me.Panel3.Controls.Add(Me.btn_batal_5)
        Me.Panel3.Controls.Add(Me.btn_batal_4)
        Me.Panel3.Controls.Add(Me.btn_batal_3)
        Me.Panel3.Controls.Add(Me.btn_batal_2)
        Me.Panel3.Controls.Add(Me.btn_batal_1)
        Me.Panel3.Controls.Add(Me.btn_hapus_10)
        Me.Panel3.Controls.Add(Me.btn_selesai_10)
        Me.Panel3.Controls.Add(Me.btn_hapus_9)
        Me.Panel3.Controls.Add(Me.btn_selesai_9)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_9)
        Me.Panel3.Controls.Add(Me.btn_hapus_8)
        Me.Panel3.Controls.Add(Me.btn_selesai_8)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_8)
        Me.Panel3.Controls.Add(Me.btn_hapus_7)
        Me.Panel3.Controls.Add(Me.btn_selesai_7)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_7)
        Me.Panel3.Controls.Add(Me.btn_hapus_6)
        Me.Panel3.Controls.Add(Me.btn_selesai_6)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_6)
        Me.Panel3.Controls.Add(Me.btn_hapus_5)
        Me.Panel3.Controls.Add(Me.btn_selesai_5)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_5)
        Me.Panel3.Controls.Add(Me.btn_hapus_4)
        Me.Panel3.Controls.Add(Me.btn_selesai_4)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_4)
        Me.Panel3.Controls.Add(Me.btn_hapus_3)
        Me.Panel3.Controls.Add(Me.btn_selesai_3)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_3)
        Me.Panel3.Controls.Add(Me.btn_hapus_2)
        Me.Panel3.Controls.Add(Me.btn_selesai_2)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_2)
        Me.Panel3.Controls.Add(Me.btn_selesai_1)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_1)
        Me.Panel3.Location = New System.Drawing.Point(407, 217)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(529, 389)
        Me.Panel3.TabIndex = 38
        '
        'btn_batal_10
        '
        Me.btn_batal_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_10.Image = CType(resources.GetObject("btn_batal_10.Image"), System.Drawing.Image)
        Me.btn_batal_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_10.Location = New System.Drawing.Point(354, 329)
        Me.btn_batal_10.Name = "btn_batal_10"
        Me.btn_batal_10.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_10.TabIndex = 48
        Me.btn_batal_10.Text = "Batal"
        Me.btn_batal_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_10.UseVisualStyleBackColor = False
        '
        'btn_batal_9
        '
        Me.btn_batal_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_9.Image = CType(resources.GetObject("btn_batal_9.Image"), System.Drawing.Image)
        Me.btn_batal_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_9.Location = New System.Drawing.Point(425, 293)
        Me.btn_batal_9.Name = "btn_batal_9"
        Me.btn_batal_9.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_9.TabIndex = 47
        Me.btn_batal_9.Text = "Batal"
        Me.btn_batal_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_9.UseVisualStyleBackColor = False
        '
        'btn_batal_8
        '
        Me.btn_batal_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_8.Image = CType(resources.GetObject("btn_batal_8.Image"), System.Drawing.Image)
        Me.btn_batal_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_8.Location = New System.Drawing.Point(425, 257)
        Me.btn_batal_8.Name = "btn_batal_8"
        Me.btn_batal_8.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_8.TabIndex = 46
        Me.btn_batal_8.Text = "Batal"
        Me.btn_batal_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_8.UseVisualStyleBackColor = False
        '
        'btn_batal_7
        '
        Me.btn_batal_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_7.Image = CType(resources.GetObject("btn_batal_7.Image"), System.Drawing.Image)
        Me.btn_batal_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_7.Location = New System.Drawing.Point(425, 223)
        Me.btn_batal_7.Name = "btn_batal_7"
        Me.btn_batal_7.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_7.TabIndex = 45
        Me.btn_batal_7.Text = "Batal"
        Me.btn_batal_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_7.UseVisualStyleBackColor = False
        '
        'btn_batal_6
        '
        Me.btn_batal_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_6.Image = CType(resources.GetObject("btn_batal_6.Image"), System.Drawing.Image)
        Me.btn_batal_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_6.Location = New System.Drawing.Point(425, 188)
        Me.btn_batal_6.Name = "btn_batal_6"
        Me.btn_batal_6.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_6.TabIndex = 44
        Me.btn_batal_6.Text = "Batal"
        Me.btn_batal_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_6.UseVisualStyleBackColor = False
        '
        'btn_batal_5
        '
        Me.btn_batal_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_5.Image = CType(resources.GetObject("btn_batal_5.Image"), System.Drawing.Image)
        Me.btn_batal_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_5.Location = New System.Drawing.Point(425, 154)
        Me.btn_batal_5.Name = "btn_batal_5"
        Me.btn_batal_5.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_5.TabIndex = 43
        Me.btn_batal_5.Text = "Batal"
        Me.btn_batal_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_5.UseVisualStyleBackColor = False
        '
        'btn_batal_4
        '
        Me.btn_batal_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_4.Image = CType(resources.GetObject("btn_batal_4.Image"), System.Drawing.Image)
        Me.btn_batal_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_4.Location = New System.Drawing.Point(425, 119)
        Me.btn_batal_4.Name = "btn_batal_4"
        Me.btn_batal_4.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_4.TabIndex = 42
        Me.btn_batal_4.Text = "Batal"
        Me.btn_batal_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_4.UseVisualStyleBackColor = False
        '
        'btn_batal_3
        '
        Me.btn_batal_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_3.Image = CType(resources.GetObject("btn_batal_3.Image"), System.Drawing.Image)
        Me.btn_batal_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_3.Location = New System.Drawing.Point(425, 83)
        Me.btn_batal_3.Name = "btn_batal_3"
        Me.btn_batal_3.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_3.TabIndex = 41
        Me.btn_batal_3.Text = "Batal"
        Me.btn_batal_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_3.UseVisualStyleBackColor = False
        '
        'btn_batal_2
        '
        Me.btn_batal_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_2.Image = CType(resources.GetObject("btn_batal_2.Image"), System.Drawing.Image)
        Me.btn_batal_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_2.Location = New System.Drawing.Point(425, 47)
        Me.btn_batal_2.Name = "btn_batal_2"
        Me.btn_batal_2.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_2.TabIndex = 40
        Me.btn_batal_2.Text = "Batal"
        Me.btn_batal_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_2.UseVisualStyleBackColor = False
        '
        'btn_batal_1
        '
        Me.btn_batal_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_1.Image = CType(resources.GetObject("btn_batal_1.Image"), System.Drawing.Image)
        Me.btn_batal_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_1.Location = New System.Drawing.Point(357, 11)
        Me.btn_batal_1.Name = "btn_batal_1"
        Me.btn_batal_1.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_1.TabIndex = 39
        Me.btn_batal_1.Text = "Batal"
        Me.btn_batal_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_1.UseVisualStyleBackColor = False
        '
        'Panel_3
        '
        Me.Panel_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_3.Controls.Add(Me.cb_satuan_3)
        Me.Panel_3.Controls.Add(Me.txt_hand_fill_3)
        Me.Panel_3.Controls.Add(Me.txt_note_3)
        Me.Panel_3.Controls.Add(Me.txt_asal_keterangan_3)
        Me.Panel_3.Controls.Add(Me.txt_tarik_lebar_3)
        Me.Panel_3.Controls.Add(Me.txt_resep_3)
        Me.Panel_3.Controls.Add(Me.txt_warna_3)
        Me.Panel_3.Controls.Add(Me.txt_qty_3)
        Me.Panel_3.Controls.Add(Me.txt_jenis_kain_3)
        Me.Panel_3.Controls.Add(Me.txt_asal_sj_3)
        Me.Panel_3.Controls.Add(Me.txt_no_urut_3)
        Me.Panel_3.Location = New System.Drawing.Point(-3, 230)
        Me.Panel_3.Name = "Panel_3"
        Me.Panel_3.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_3.TabIndex = 11
        '
        'cb_satuan_3
        '
        Me.cb_satuan_3.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_3.FormattingEnabled = True
        Me.cb_satuan_3.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_3.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_3.Name = "cb_satuan_3"
        Me.cb_satuan_3.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_3.TabIndex = 501
        Me.cb_satuan_3.Text = "Meter"
        '
        'txt_hand_fill_3
        '
        Me.txt_hand_fill_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_3.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_3.Name = "txt_hand_fill_3"
        Me.txt_hand_fill_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_3.TabIndex = 9
        '
        'txt_note_3
        '
        Me.txt_note_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_3.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_3.MaxLength = 200
        Me.txt_note_3.Multiline = True
        Me.txt_note_3.Name = "txt_note_3"
        Me.txt_note_3.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_3.TabIndex = 8
        '
        'txt_asal_keterangan_3
        '
        Me.txt_asal_keterangan_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_3.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_3.Name = "txt_asal_keterangan_3"
        Me.txt_asal_keterangan_3.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_3.TabIndex = 7
        '
        'txt_tarik_lebar_3
        '
        Me.txt_tarik_lebar_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_3.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_3.Name = "txt_tarik_lebar_3"
        Me.txt_tarik_lebar_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_3.TabIndex = 6
        Me.txt_tarik_lebar_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_3
        '
        Me.txt_resep_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_3.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_3.Name = "txt_resep_3"
        Me.txt_resep_3.ReadOnly = True
        Me.txt_resep_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_3.TabIndex = 5
        Me.txt_resep_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_3
        '
        Me.txt_warna_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_3.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_3.Name = "txt_warna_3"
        Me.txt_warna_3.ReadOnly = True
        Me.txt_warna_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_3.TabIndex = 4
        Me.txt_warna_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_3
        '
        Me.txt_qty_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_3.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_3.Name = "txt_qty_3"
        Me.txt_qty_3.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_3.TabIndex = 3
        Me.txt_qty_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_3
        '
        Me.txt_jenis_kain_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_3.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_3.Name = "txt_jenis_kain_3"
        Me.txt_jenis_kain_3.ReadOnly = True
        Me.txt_jenis_kain_3.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_3.TabIndex = 2
        Me.txt_jenis_kain_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_3
        '
        Me.txt_asal_sj_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_3.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_3.Name = "txt_asal_sj_3"
        Me.txt_asal_sj_3.ReadOnly = True
        Me.txt_asal_sj_3.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_3.TabIndex = 1
        Me.txt_asal_sj_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_3
        '
        Me.txt_no_urut_3.AutoSize = True
        Me.txt_no_urut_3.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_3.Name = "txt_no_urut_3"
        Me.txt_no_urut_3.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_3.TabIndex = 0
        Me.txt_no_urut_3.Text = "3"
        '
        'Panel_4
        '
        Me.Panel_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_4.Controls.Add(Me.cb_satuan_4)
        Me.Panel_4.Controls.Add(Me.txt_hand_fill_4)
        Me.Panel_4.Controls.Add(Me.txt_note_4)
        Me.Panel_4.Controls.Add(Me.txt_asal_keterangan_4)
        Me.Panel_4.Controls.Add(Me.txt_tarik_lebar_4)
        Me.Panel_4.Controls.Add(Me.txt_resep_4)
        Me.Panel_4.Controls.Add(Me.txt_warna_4)
        Me.Panel_4.Controls.Add(Me.txt_qty_4)
        Me.Panel_4.Controls.Add(Me.txt_jenis_kain_4)
        Me.Panel_4.Controls.Add(Me.txt_asal_sj_4)
        Me.Panel_4.Controls.Add(Me.txt_no_urut_4)
        Me.Panel_4.Location = New System.Drawing.Point(-3, 265)
        Me.Panel_4.Name = "Panel_4"
        Me.Panel_4.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_4.TabIndex = 11
        '
        'cb_satuan_4
        '
        Me.cb_satuan_4.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_4.FormattingEnabled = True
        Me.cb_satuan_4.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_4.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_4.Name = "cb_satuan_4"
        Me.cb_satuan_4.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_4.TabIndex = 501
        Me.cb_satuan_4.Text = "Meter"
        '
        'txt_hand_fill_4
        '
        Me.txt_hand_fill_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_4.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_4.Name = "txt_hand_fill_4"
        Me.txt_hand_fill_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_4.TabIndex = 9
        '
        'txt_note_4
        '
        Me.txt_note_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_4.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_4.MaxLength = 200
        Me.txt_note_4.Multiline = True
        Me.txt_note_4.Name = "txt_note_4"
        Me.txt_note_4.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_4.TabIndex = 8
        '
        'txt_asal_keterangan_4
        '
        Me.txt_asal_keterangan_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_4.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_4.Name = "txt_asal_keterangan_4"
        Me.txt_asal_keterangan_4.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_4.TabIndex = 7
        '
        'txt_tarik_lebar_4
        '
        Me.txt_tarik_lebar_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_4.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_4.Name = "txt_tarik_lebar_4"
        Me.txt_tarik_lebar_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_4.TabIndex = 6
        Me.txt_tarik_lebar_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_4
        '
        Me.txt_resep_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_4.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_4.Name = "txt_resep_4"
        Me.txt_resep_4.ReadOnly = True
        Me.txt_resep_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_4.TabIndex = 5
        Me.txt_resep_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_4
        '
        Me.txt_warna_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_4.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_4.Name = "txt_warna_4"
        Me.txt_warna_4.ReadOnly = True
        Me.txt_warna_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_4.TabIndex = 4
        Me.txt_warna_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_4
        '
        Me.txt_qty_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_4.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_4.Name = "txt_qty_4"
        Me.txt_qty_4.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_4.TabIndex = 3
        Me.txt_qty_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_4
        '
        Me.txt_jenis_kain_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_4.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_4.Name = "txt_jenis_kain_4"
        Me.txt_jenis_kain_4.ReadOnly = True
        Me.txt_jenis_kain_4.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_4.TabIndex = 2
        Me.txt_jenis_kain_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_4
        '
        Me.txt_asal_sj_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_4.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_4.Name = "txt_asal_sj_4"
        Me.txt_asal_sj_4.ReadOnly = True
        Me.txt_asal_sj_4.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_4.TabIndex = 1
        Me.txt_asal_sj_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_4
        '
        Me.txt_no_urut_4.AutoSize = True
        Me.txt_no_urut_4.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_4.Name = "txt_no_urut_4"
        Me.txt_no_urut_4.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_4.TabIndex = 0
        Me.txt_no_urut_4.Text = "4"
        '
        'Panel_5
        '
        Me.Panel_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_5.Controls.Add(Me.cb_satuan_5)
        Me.Panel_5.Controls.Add(Me.txt_hand_fill_5)
        Me.Panel_5.Controls.Add(Me.txt_note_5)
        Me.Panel_5.Controls.Add(Me.txt_asal_keterangan_5)
        Me.Panel_5.Controls.Add(Me.txt_tarik_lebar_5)
        Me.Panel_5.Controls.Add(Me.txt_resep_5)
        Me.Panel_5.Controls.Add(Me.txt_warna_5)
        Me.Panel_5.Controls.Add(Me.txt_qty_5)
        Me.Panel_5.Controls.Add(Me.txt_jenis_kain_5)
        Me.Panel_5.Controls.Add(Me.txt_asal_sj_5)
        Me.Panel_5.Controls.Add(Me.txt_no_urut_5)
        Me.Panel_5.Location = New System.Drawing.Point(-3, 300)
        Me.Panel_5.Name = "Panel_5"
        Me.Panel_5.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_5.TabIndex = 11
        '
        'cb_satuan_5
        '
        Me.cb_satuan_5.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_5.FormattingEnabled = True
        Me.cb_satuan_5.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_5.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_5.Name = "cb_satuan_5"
        Me.cb_satuan_5.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_5.TabIndex = 501
        Me.cb_satuan_5.Text = "Meter"
        '
        'txt_hand_fill_5
        '
        Me.txt_hand_fill_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_5.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_5.Name = "txt_hand_fill_5"
        Me.txt_hand_fill_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_5.TabIndex = 9
        '
        'txt_note_5
        '
        Me.txt_note_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_5.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_5.MaxLength = 200
        Me.txt_note_5.Multiline = True
        Me.txt_note_5.Name = "txt_note_5"
        Me.txt_note_5.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_5.TabIndex = 8
        '
        'txt_asal_keterangan_5
        '
        Me.txt_asal_keterangan_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_5.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_5.Name = "txt_asal_keterangan_5"
        Me.txt_asal_keterangan_5.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_5.TabIndex = 7
        '
        'txt_tarik_lebar_5
        '
        Me.txt_tarik_lebar_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_5.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_5.Name = "txt_tarik_lebar_5"
        Me.txt_tarik_lebar_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_5.TabIndex = 6
        Me.txt_tarik_lebar_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_5
        '
        Me.txt_resep_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_5.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_5.Name = "txt_resep_5"
        Me.txt_resep_5.ReadOnly = True
        Me.txt_resep_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_5.TabIndex = 5
        Me.txt_resep_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_5
        '
        Me.txt_warna_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_5.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_5.Name = "txt_warna_5"
        Me.txt_warna_5.ReadOnly = True
        Me.txt_warna_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_5.TabIndex = 4
        Me.txt_warna_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_5
        '
        Me.txt_qty_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_5.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_5.Name = "txt_qty_5"
        Me.txt_qty_5.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_5.TabIndex = 3
        Me.txt_qty_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_5
        '
        Me.txt_jenis_kain_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_5.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_5.Name = "txt_jenis_kain_5"
        Me.txt_jenis_kain_5.ReadOnly = True
        Me.txt_jenis_kain_5.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_5.TabIndex = 2
        Me.txt_jenis_kain_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_5
        '
        Me.txt_asal_sj_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_5.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_5.Name = "txt_asal_sj_5"
        Me.txt_asal_sj_5.ReadOnly = True
        Me.txt_asal_sj_5.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_5.TabIndex = 1
        Me.txt_asal_sj_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_5
        '
        Me.txt_no_urut_5.AutoSize = True
        Me.txt_no_urut_5.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_5.Name = "txt_no_urut_5"
        Me.txt_no_urut_5.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_5.TabIndex = 0
        Me.txt_no_urut_5.Text = "5"
        '
        'Panel_10
        '
        Me.Panel_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_10.Controls.Add(Me.cb_satuan_10)
        Me.Panel_10.Controls.Add(Me.txt_hand_fill_10)
        Me.Panel_10.Controls.Add(Me.txt_note_10)
        Me.Panel_10.Controls.Add(Me.txt_asal_keterangan_10)
        Me.Panel_10.Controls.Add(Me.txt_tarik_lebar_10)
        Me.Panel_10.Controls.Add(Me.txt_resep_10)
        Me.Panel_10.Controls.Add(Me.txt_warna_10)
        Me.Panel_10.Controls.Add(Me.txt_qty_10)
        Me.Panel_10.Controls.Add(Me.txt_jenis_kain_10)
        Me.Panel_10.Controls.Add(Me.txt_asal_sj_10)
        Me.Panel_10.Controls.Add(Me.txt_no_urut_10)
        Me.Panel_10.Location = New System.Drawing.Point(-3, 475)
        Me.Panel_10.Name = "Panel_10"
        Me.Panel_10.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_10.TabIndex = 11
        '
        'cb_satuan_10
        '
        Me.cb_satuan_10.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_10.FormattingEnabled = True
        Me.cb_satuan_10.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_10.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_10.Name = "cb_satuan_10"
        Me.cb_satuan_10.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_10.TabIndex = 501
        Me.cb_satuan_10.Text = "Meter"
        '
        'txt_hand_fill_10
        '
        Me.txt_hand_fill_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_10.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_10.Name = "txt_hand_fill_10"
        Me.txt_hand_fill_10.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_10.TabIndex = 9
        '
        'txt_note_10
        '
        Me.txt_note_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_10.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_10.MaxLength = 200
        Me.txt_note_10.Multiline = True
        Me.txt_note_10.Name = "txt_note_10"
        Me.txt_note_10.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_10.TabIndex = 8
        '
        'txt_asal_keterangan_10
        '
        Me.txt_asal_keterangan_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_10.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_10.Name = "txt_asal_keterangan_10"
        Me.txt_asal_keterangan_10.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_10.TabIndex = 7
        '
        'txt_tarik_lebar_10
        '
        Me.txt_tarik_lebar_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_10.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_10.Name = "txt_tarik_lebar_10"
        Me.txt_tarik_lebar_10.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_10.TabIndex = 6
        Me.txt_tarik_lebar_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_10
        '
        Me.txt_resep_10.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_10.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_10.Name = "txt_resep_10"
        Me.txt_resep_10.ReadOnly = True
        Me.txt_resep_10.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_10.TabIndex = 5
        Me.txt_resep_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_10
        '
        Me.txt_warna_10.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_10.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_10.Name = "txt_warna_10"
        Me.txt_warna_10.ReadOnly = True
        Me.txt_warna_10.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_10.TabIndex = 4
        Me.txt_warna_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_10
        '
        Me.txt_qty_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_10.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_10.Name = "txt_qty_10"
        Me.txt_qty_10.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_10.TabIndex = 3
        Me.txt_qty_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_10
        '
        Me.txt_jenis_kain_10.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_10.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_10.Name = "txt_jenis_kain_10"
        Me.txt_jenis_kain_10.ReadOnly = True
        Me.txt_jenis_kain_10.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_10.TabIndex = 2
        Me.txt_jenis_kain_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_10
        '
        Me.txt_asal_sj_10.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_10.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_10.Name = "txt_asal_sj_10"
        Me.txt_asal_sj_10.ReadOnly = True
        Me.txt_asal_sj_10.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_10.TabIndex = 1
        Me.txt_asal_sj_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_10
        '
        Me.txt_no_urut_10.AutoSize = True
        Me.txt_no_urut_10.Location = New System.Drawing.Point(15, 11)
        Me.txt_no_urut_10.Name = "txt_no_urut_10"
        Me.txt_no_urut_10.Size = New System.Drawing.Size(19, 13)
        Me.txt_no_urut_10.TabIndex = 0
        Me.txt_no_urut_10.Text = "10"
        '
        'Panel_9
        '
        Me.Panel_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_9.Controls.Add(Me.cb_satuan_9)
        Me.Panel_9.Controls.Add(Me.txt_hand_fill_9)
        Me.Panel_9.Controls.Add(Me.txt_note_9)
        Me.Panel_9.Controls.Add(Me.txt_asal_keterangan_9)
        Me.Panel_9.Controls.Add(Me.txt_tarik_lebar_9)
        Me.Panel_9.Controls.Add(Me.txt_resep_9)
        Me.Panel_9.Controls.Add(Me.txt_warna_9)
        Me.Panel_9.Controls.Add(Me.txt_qty_9)
        Me.Panel_9.Controls.Add(Me.txt_jenis_kain_9)
        Me.Panel_9.Controls.Add(Me.txt_asal_sj_9)
        Me.Panel_9.Controls.Add(Me.txt_no_urut_9)
        Me.Panel_9.Location = New System.Drawing.Point(-3, 440)
        Me.Panel_9.Name = "Panel_9"
        Me.Panel_9.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_9.TabIndex = 11
        '
        'cb_satuan_9
        '
        Me.cb_satuan_9.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_9.FormattingEnabled = True
        Me.cb_satuan_9.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_9.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_9.Name = "cb_satuan_9"
        Me.cb_satuan_9.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_9.TabIndex = 501
        Me.cb_satuan_9.Text = "Meter"
        '
        'txt_hand_fill_9
        '
        Me.txt_hand_fill_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_9.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_9.Name = "txt_hand_fill_9"
        Me.txt_hand_fill_9.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_9.TabIndex = 9
        '
        'txt_note_9
        '
        Me.txt_note_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_9.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_9.MaxLength = 200
        Me.txt_note_9.Multiline = True
        Me.txt_note_9.Name = "txt_note_9"
        Me.txt_note_9.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_9.TabIndex = 8
        '
        'txt_asal_keterangan_9
        '
        Me.txt_asal_keterangan_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_9.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_9.Name = "txt_asal_keterangan_9"
        Me.txt_asal_keterangan_9.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_9.TabIndex = 7
        '
        'txt_tarik_lebar_9
        '
        Me.txt_tarik_lebar_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_9.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_9.Name = "txt_tarik_lebar_9"
        Me.txt_tarik_lebar_9.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_9.TabIndex = 6
        Me.txt_tarik_lebar_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_9
        '
        Me.txt_resep_9.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_9.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_9.Name = "txt_resep_9"
        Me.txt_resep_9.ReadOnly = True
        Me.txt_resep_9.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_9.TabIndex = 5
        Me.txt_resep_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_9
        '
        Me.txt_warna_9.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_9.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_9.Name = "txt_warna_9"
        Me.txt_warna_9.ReadOnly = True
        Me.txt_warna_9.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_9.TabIndex = 4
        Me.txt_warna_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_9
        '
        Me.txt_qty_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_9.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_9.Name = "txt_qty_9"
        Me.txt_qty_9.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_9.TabIndex = 3
        Me.txt_qty_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_9
        '
        Me.txt_jenis_kain_9.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_9.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_9.Name = "txt_jenis_kain_9"
        Me.txt_jenis_kain_9.ReadOnly = True
        Me.txt_jenis_kain_9.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_9.TabIndex = 2
        Me.txt_jenis_kain_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_9
        '
        Me.txt_asal_sj_9.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_9.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_9.Name = "txt_asal_sj_9"
        Me.txt_asal_sj_9.ReadOnly = True
        Me.txt_asal_sj_9.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_9.TabIndex = 1
        Me.txt_asal_sj_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_9
        '
        Me.txt_no_urut_9.AutoSize = True
        Me.txt_no_urut_9.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_9.Name = "txt_no_urut_9"
        Me.txt_no_urut_9.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_9.TabIndex = 0
        Me.txt_no_urut_9.Text = "9"
        '
        'Panel_8
        '
        Me.Panel_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_8.Controls.Add(Me.cb_satuan_8)
        Me.Panel_8.Controls.Add(Me.txt_hand_fill_8)
        Me.Panel_8.Controls.Add(Me.txt_note_8)
        Me.Panel_8.Controls.Add(Me.txt_asal_keterangan_8)
        Me.Panel_8.Controls.Add(Me.txt_tarik_lebar_8)
        Me.Panel_8.Controls.Add(Me.txt_resep_8)
        Me.Panel_8.Controls.Add(Me.txt_warna_8)
        Me.Panel_8.Controls.Add(Me.txt_qty_8)
        Me.Panel_8.Controls.Add(Me.txt_jenis_kain_8)
        Me.Panel_8.Controls.Add(Me.txt_asal_sj_8)
        Me.Panel_8.Controls.Add(Me.txt_no_urut_8)
        Me.Panel_8.Location = New System.Drawing.Point(-3, 405)
        Me.Panel_8.Name = "Panel_8"
        Me.Panel_8.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_8.TabIndex = 11
        '
        'cb_satuan_8
        '
        Me.cb_satuan_8.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_8.FormattingEnabled = True
        Me.cb_satuan_8.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_8.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_8.Name = "cb_satuan_8"
        Me.cb_satuan_8.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_8.TabIndex = 501
        Me.cb_satuan_8.Text = "Meter"
        '
        'txt_hand_fill_8
        '
        Me.txt_hand_fill_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_8.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_8.Name = "txt_hand_fill_8"
        Me.txt_hand_fill_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_8.TabIndex = 9
        '
        'txt_note_8
        '
        Me.txt_note_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_8.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_8.MaxLength = 200
        Me.txt_note_8.Multiline = True
        Me.txt_note_8.Name = "txt_note_8"
        Me.txt_note_8.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_8.TabIndex = 8
        '
        'txt_asal_keterangan_8
        '
        Me.txt_asal_keterangan_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_8.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_8.Name = "txt_asal_keterangan_8"
        Me.txt_asal_keterangan_8.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_8.TabIndex = 7
        '
        'txt_tarik_lebar_8
        '
        Me.txt_tarik_lebar_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_8.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_8.Name = "txt_tarik_lebar_8"
        Me.txt_tarik_lebar_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_8.TabIndex = 6
        Me.txt_tarik_lebar_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_8
        '
        Me.txt_resep_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_8.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_8.Name = "txt_resep_8"
        Me.txt_resep_8.ReadOnly = True
        Me.txt_resep_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_8.TabIndex = 5
        Me.txt_resep_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_8
        '
        Me.txt_warna_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_8.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_8.Name = "txt_warna_8"
        Me.txt_warna_8.ReadOnly = True
        Me.txt_warna_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_8.TabIndex = 4
        Me.txt_warna_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_8
        '
        Me.txt_qty_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_8.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_8.Name = "txt_qty_8"
        Me.txt_qty_8.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_8.TabIndex = 3
        Me.txt_qty_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_8
        '
        Me.txt_jenis_kain_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_8.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_8.Name = "txt_jenis_kain_8"
        Me.txt_jenis_kain_8.ReadOnly = True
        Me.txt_jenis_kain_8.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_8.TabIndex = 2
        Me.txt_jenis_kain_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_8
        '
        Me.txt_asal_sj_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_8.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_8.Name = "txt_asal_sj_8"
        Me.txt_asal_sj_8.ReadOnly = True
        Me.txt_asal_sj_8.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_8.TabIndex = 1
        Me.txt_asal_sj_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_8
        '
        Me.txt_no_urut_8.AutoSize = True
        Me.txt_no_urut_8.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_8.Name = "txt_no_urut_8"
        Me.txt_no_urut_8.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_8.TabIndex = 0
        Me.txt_no_urut_8.Text = "8"
        '
        'Panel_7
        '
        Me.Panel_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_7.Controls.Add(Me.cb_satuan_7)
        Me.Panel_7.Controls.Add(Me.txt_hand_fill_7)
        Me.Panel_7.Controls.Add(Me.txt_note_7)
        Me.Panel_7.Controls.Add(Me.txt_asal_keterangan_7)
        Me.Panel_7.Controls.Add(Me.txt_tarik_lebar_7)
        Me.Panel_7.Controls.Add(Me.txt_resep_7)
        Me.Panel_7.Controls.Add(Me.txt_warna_7)
        Me.Panel_7.Controls.Add(Me.txt_qty_7)
        Me.Panel_7.Controls.Add(Me.txt_jenis_kain_7)
        Me.Panel_7.Controls.Add(Me.txt_asal_sj_7)
        Me.Panel_7.Controls.Add(Me.txt_no_urut_7)
        Me.Panel_7.Location = New System.Drawing.Point(-3, 370)
        Me.Panel_7.Name = "Panel_7"
        Me.Panel_7.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_7.TabIndex = 11
        '
        'cb_satuan_7
        '
        Me.cb_satuan_7.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_7.FormattingEnabled = True
        Me.cb_satuan_7.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_7.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_7.Name = "cb_satuan_7"
        Me.cb_satuan_7.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_7.TabIndex = 501
        Me.cb_satuan_7.Text = "Meter"
        '
        'txt_hand_fill_7
        '
        Me.txt_hand_fill_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_7.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_7.Name = "txt_hand_fill_7"
        Me.txt_hand_fill_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_7.TabIndex = 9
        '
        'txt_note_7
        '
        Me.txt_note_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_7.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_7.MaxLength = 200
        Me.txt_note_7.Multiline = True
        Me.txt_note_7.Name = "txt_note_7"
        Me.txt_note_7.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_7.TabIndex = 8
        '
        'txt_asal_keterangan_7
        '
        Me.txt_asal_keterangan_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_7.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_7.Name = "txt_asal_keterangan_7"
        Me.txt_asal_keterangan_7.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_7.TabIndex = 7
        '
        'txt_tarik_lebar_7
        '
        Me.txt_tarik_lebar_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_7.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_7.Name = "txt_tarik_lebar_7"
        Me.txt_tarik_lebar_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_7.TabIndex = 6
        Me.txt_tarik_lebar_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_7
        '
        Me.txt_resep_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_7.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_7.Name = "txt_resep_7"
        Me.txt_resep_7.ReadOnly = True
        Me.txt_resep_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_7.TabIndex = 5
        Me.txt_resep_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_7
        '
        Me.txt_warna_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_7.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_7.Name = "txt_warna_7"
        Me.txt_warna_7.ReadOnly = True
        Me.txt_warna_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_7.TabIndex = 4
        Me.txt_warna_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_7
        '
        Me.txt_qty_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_7.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_7.Name = "txt_qty_7"
        Me.txt_qty_7.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_7.TabIndex = 3
        Me.txt_qty_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_7
        '
        Me.txt_jenis_kain_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_7.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_7.Name = "txt_jenis_kain_7"
        Me.txt_jenis_kain_7.ReadOnly = True
        Me.txt_jenis_kain_7.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_7.TabIndex = 2
        Me.txt_jenis_kain_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_7
        '
        Me.txt_asal_sj_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_7.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_7.Name = "txt_asal_sj_7"
        Me.txt_asal_sj_7.ReadOnly = True
        Me.txt_asal_sj_7.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_7.TabIndex = 1
        Me.txt_asal_sj_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_7
        '
        Me.txt_no_urut_7.AutoSize = True
        Me.txt_no_urut_7.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_7.Name = "txt_no_urut_7"
        Me.txt_no_urut_7.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_7.TabIndex = 0
        Me.txt_no_urut_7.Text = "7"
        '
        'Panel_6
        '
        Me.Panel_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_6.Controls.Add(Me.cb_satuan_6)
        Me.Panel_6.Controls.Add(Me.txt_hand_fill_6)
        Me.Panel_6.Controls.Add(Me.txt_note_6)
        Me.Panel_6.Controls.Add(Me.txt_asal_keterangan_6)
        Me.Panel_6.Controls.Add(Me.txt_tarik_lebar_6)
        Me.Panel_6.Controls.Add(Me.txt_resep_6)
        Me.Panel_6.Controls.Add(Me.txt_warna_6)
        Me.Panel_6.Controls.Add(Me.txt_qty_6)
        Me.Panel_6.Controls.Add(Me.txt_jenis_kain_6)
        Me.Panel_6.Controls.Add(Me.txt_asal_sj_6)
        Me.Panel_6.Controls.Add(Me.txt_no_urut_6)
        Me.Panel_6.Location = New System.Drawing.Point(-3, 335)
        Me.Panel_6.Name = "Panel_6"
        Me.Panel_6.Size = New System.Drawing.Size(1300, 36)
        Me.Panel_6.TabIndex = 11
        '
        'cb_satuan_6
        '
        Me.cb_satuan_6.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_6.FormattingEnabled = True
        Me.cb_satuan_6.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_6.Location = New System.Drawing.Point(393, 6)
        Me.cb_satuan_6.Name = "cb_satuan_6"
        Me.cb_satuan_6.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_6.TabIndex = 501
        Me.cb_satuan_6.Text = "Meter"
        '
        'txt_hand_fill_6
        '
        Me.txt_hand_fill_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_hand_fill_6.Location = New System.Drawing.Point(775, 7)
        Me.txt_hand_fill_6.Name = "txt_hand_fill_6"
        Me.txt_hand_fill_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_hand_fill_6.TabIndex = 9
        '
        'txt_note_6
        '
        Me.txt_note_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_6.Location = New System.Drawing.Point(1034, 7)
        Me.txt_note_6.MaxLength = 200
        Me.txt_note_6.Multiline = True
        Me.txt_note_6.Name = "txt_note_6"
        Me.txt_note_6.Size = New System.Drawing.Size(239, 20)
        Me.txt_note_6.TabIndex = 8
        '
        'txt_asal_keterangan_6
        '
        Me.txt_asal_keterangan_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_keterangan_6.Location = New System.Drawing.Point(883, 7)
        Me.txt_asal_keterangan_6.Name = "txt_asal_keterangan_6"
        Me.txt_asal_keterangan_6.Size = New System.Drawing.Size(143, 20)
        Me.txt_asal_keterangan_6.TabIndex = 7
        '
        'txt_tarik_lebar_6
        '
        Me.txt_tarik_lebar_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_tarik_lebar_6.Location = New System.Drawing.Point(667, 7)
        Me.txt_tarik_lebar_6.Name = "txt_tarik_lebar_6"
        Me.txt_tarik_lebar_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_tarik_lebar_6.TabIndex = 6
        Me.txt_tarik_lebar_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_resep_6
        '
        Me.txt_resep_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_resep_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_6.Location = New System.Drawing.Point(559, 7)
        Me.txt_resep_6.Name = "txt_resep_6"
        Me.txt_resep_6.ReadOnly = True
        Me.txt_resep_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_6.TabIndex = 5
        Me.txt_resep_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_warna_6
        '
        Me.txt_warna_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_6.Location = New System.Drawing.Point(451, 7)
        Me.txt_warna_6.Name = "txt_warna_6"
        Me.txt_warna_6.ReadOnly = True
        Me.txt_warna_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_6.TabIndex = 4
        Me.txt_warna_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_qty_6
        '
        Me.txt_qty_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_6.Location = New System.Drawing.Point(295, 7)
        Me.txt_qty_6.Name = "txt_qty_6"
        Me.txt_qty_6.Size = New System.Drawing.Size(90, 20)
        Me.txt_qty_6.TabIndex = 3
        Me.txt_qty_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jenis_kain_6
        '
        Me.txt_jenis_kain_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_6.Location = New System.Drawing.Point(137, 7)
        Me.txt_jenis_kain_6.Name = "txt_jenis_kain_6"
        Me.txt_jenis_kain_6.ReadOnly = True
        Me.txt_jenis_kain_6.Size = New System.Drawing.Size(150, 20)
        Me.txt_jenis_kain_6.TabIndex = 2
        Me.txt_jenis_kain_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_asal_sj_6
        '
        Me.txt_asal_sj_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_asal_sj_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_6.Location = New System.Drawing.Point(39, 7)
        Me.txt_asal_sj_6.Name = "txt_asal_sj_6"
        Me.txt_asal_sj_6.ReadOnly = True
        Me.txt_asal_sj_6.Size = New System.Drawing.Size(90, 20)
        Me.txt_asal_sj_6.TabIndex = 1
        Me.txt_asal_sj_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_no_urut_6
        '
        Me.txt_no_urut_6.AutoSize = True
        Me.txt_no_urut_6.Location = New System.Drawing.Point(18, 11)
        Me.txt_no_urut_6.Name = "txt_no_urut_6"
        Me.txt_no_urut_6.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_6.TabIndex = 0
        Me.txt_no_urut_6.Text = "6"
        '
        'txt_note_po
        '
        Me.txt_note_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_note_po.Location = New System.Drawing.Point(63, 6)
        Me.txt_note_po.MaxLength = 300
        Me.txt_note_po.Name = "txt_note_po"
        Me.txt_note_po.Size = New System.Drawing.Size(333, 58)
        Me.txt_note_po.TabIndex = 100
        Me.txt_note_po.Text = ""
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(1, 29)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(55, 13)
        Me.Label15.TabIndex = 40
        Me.Label15.Text = "Note PO"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.txt_note_po)
        Me.Panel4.Location = New System.Drawing.Point(-1, 539)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(403, 72)
        Me.Panel4.TabIndex = 41
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label18)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_10)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_9)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_8)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_7)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_6)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_5)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_4)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_3)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_2)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_1)
        Me.Panel5.Location = New System.Drawing.Point(4, 1)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(163, 134)
        Me.Panel5.TabIndex = 42
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(54, 114)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(55, 13)
        Me.Label18.TabIndex = 10
        Me.Label18.Text = "QTY Awal"
        '
        'txt_qty_asal_10
        '
        Me.txt_qty_asal_10.Location = New System.Drawing.Point(84, 87)
        Me.txt_qty_asal_10.Name = "txt_qty_asal_10"
        Me.txt_qty_asal_10.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_10.TabIndex = 9
        '
        'txt_qty_asal_9
        '
        Me.txt_qty_asal_9.Location = New System.Drawing.Point(84, 67)
        Me.txt_qty_asal_9.Name = "txt_qty_asal_9"
        Me.txt_qty_asal_9.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_9.TabIndex = 8
        '
        'txt_qty_asal_8
        '
        Me.txt_qty_asal_8.Location = New System.Drawing.Point(84, 47)
        Me.txt_qty_asal_8.Name = "txt_qty_asal_8"
        Me.txt_qty_asal_8.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_8.TabIndex = 7
        '
        'txt_qty_asal_7
        '
        Me.txt_qty_asal_7.Location = New System.Drawing.Point(84, 27)
        Me.txt_qty_asal_7.Name = "txt_qty_asal_7"
        Me.txt_qty_asal_7.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_7.TabIndex = 6
        '
        'txt_qty_asal_6
        '
        Me.txt_qty_asal_6.Location = New System.Drawing.Point(84, 7)
        Me.txt_qty_asal_6.Name = "txt_qty_asal_6"
        Me.txt_qty_asal_6.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_6.TabIndex = 5
        '
        'txt_qty_asal_5
        '
        Me.txt_qty_asal_5.Location = New System.Drawing.Point(8, 87)
        Me.txt_qty_asal_5.Name = "txt_qty_asal_5"
        Me.txt_qty_asal_5.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_5.TabIndex = 4
        '
        'txt_qty_asal_4
        '
        Me.txt_qty_asal_4.Location = New System.Drawing.Point(8, 67)
        Me.txt_qty_asal_4.Name = "txt_qty_asal_4"
        Me.txt_qty_asal_4.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_4.TabIndex = 3
        '
        'txt_qty_asal_3
        '
        Me.txt_qty_asal_3.Location = New System.Drawing.Point(8, 47)
        Me.txt_qty_asal_3.Name = "txt_qty_asal_3"
        Me.txt_qty_asal_3.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_3.TabIndex = 2
        '
        'txt_qty_asal_2
        '
        Me.txt_qty_asal_2.Location = New System.Drawing.Point(8, 27)
        Me.txt_qty_asal_2.Name = "txt_qty_asal_2"
        Me.txt_qty_asal_2.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_2.TabIndex = 1
        '
        'txt_qty_asal_1
        '
        Me.txt_qty_asal_1.Location = New System.Drawing.Point(8, 7)
        Me.txt_qty_asal_1.Name = "txt_qty_asal_1"
        Me.txt_qty_asal_1.Size = New System.Drawing.Size(70, 20)
        Me.txt_qty_asal_1.TabIndex = 0
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label19)
        Me.Panel6.Controls.Add(Me.txt_id_grey_10)
        Me.Panel6.Controls.Add(Me.txt_id_grey_9)
        Me.Panel6.Controls.Add(Me.txt_id_grey_8)
        Me.Panel6.Controls.Add(Me.txt_id_grey_7)
        Me.Panel6.Controls.Add(Me.txt_id_grey_6)
        Me.Panel6.Controls.Add(Me.txt_id_grey_5)
        Me.Panel6.Controls.Add(Me.txt_id_grey_4)
        Me.Panel6.Controls.Add(Me.txt_id_grey_3)
        Me.Panel6.Controls.Add(Me.txt_id_grey_2)
        Me.Panel6.Controls.Add(Me.txt_id_grey_1)
        Me.Panel6.Location = New System.Drawing.Point(171, 2)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(163, 134)
        Me.Panel6.TabIndex = 43
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(60, 108)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(43, 13)
        Me.Label19.TabIndex = 10
        Me.Label19.Text = "ID Grey"
        '
        'txt_id_grey_10
        '
        Me.txt_id_grey_10.Location = New System.Drawing.Point(84, 89)
        Me.txt_id_grey_10.Name = "txt_id_grey_10"
        Me.txt_id_grey_10.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_10.TabIndex = 9
        '
        'txt_id_grey_9
        '
        Me.txt_id_grey_9.Location = New System.Drawing.Point(84, 69)
        Me.txt_id_grey_9.Name = "txt_id_grey_9"
        Me.txt_id_grey_9.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_9.TabIndex = 8
        '
        'txt_id_grey_8
        '
        Me.txt_id_grey_8.Location = New System.Drawing.Point(84, 49)
        Me.txt_id_grey_8.Name = "txt_id_grey_8"
        Me.txt_id_grey_8.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_8.TabIndex = 7
        '
        'txt_id_grey_7
        '
        Me.txt_id_grey_7.Location = New System.Drawing.Point(84, 29)
        Me.txt_id_grey_7.Name = "txt_id_grey_7"
        Me.txt_id_grey_7.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_7.TabIndex = 6
        '
        'txt_id_grey_6
        '
        Me.txt_id_grey_6.Location = New System.Drawing.Point(84, 9)
        Me.txt_id_grey_6.Name = "txt_id_grey_6"
        Me.txt_id_grey_6.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_6.TabIndex = 5
        '
        'txt_id_grey_5
        '
        Me.txt_id_grey_5.Location = New System.Drawing.Point(8, 89)
        Me.txt_id_grey_5.Name = "txt_id_grey_5"
        Me.txt_id_grey_5.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_5.TabIndex = 4
        '
        'txt_id_grey_4
        '
        Me.txt_id_grey_4.Location = New System.Drawing.Point(8, 69)
        Me.txt_id_grey_4.Name = "txt_id_grey_4"
        Me.txt_id_grey_4.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_4.TabIndex = 3
        '
        'txt_id_grey_3
        '
        Me.txt_id_grey_3.Location = New System.Drawing.Point(8, 49)
        Me.txt_id_grey_3.Name = "txt_id_grey_3"
        Me.txt_id_grey_3.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_3.TabIndex = 2
        '
        'txt_id_grey_2
        '
        Me.txt_id_grey_2.Location = New System.Drawing.Point(8, 29)
        Me.txt_id_grey_2.Name = "txt_id_grey_2"
        Me.txt_id_grey_2.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_2.TabIndex = 1
        '
        'txt_id_grey_1
        '
        Me.txt_id_grey_1.Location = New System.Drawing.Point(8, 9)
        Me.txt_id_grey_1.Name = "txt_id_grey_1"
        Me.txt_id_grey_1.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_grey_1.TabIndex = 0
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.txt_id_beli_10)
        Me.Panel8.Controls.Add(Me.txt_id_beli_9)
        Me.Panel8.Controls.Add(Me.Label16)
        Me.Panel8.Controls.Add(Me.txt_id_beli_1)
        Me.Panel8.Controls.Add(Me.txt_id_beli_2)
        Me.Panel8.Controls.Add(Me.txt_id_beli_3)
        Me.Panel8.Controls.Add(Me.txt_id_beli_4)
        Me.Panel8.Controls.Add(Me.txt_id_beli_6)
        Me.Panel8.Controls.Add(Me.txt_id_beli_7)
        Me.Panel8.Controls.Add(Me.txt_id_beli_8)
        Me.Panel8.Controls.Add(Me.txt_id_beli_5)
        Me.Panel8.Location = New System.Drawing.Point(959, 3)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(179, 129)
        Me.Panel8.TabIndex = 109
        '
        'txt_id_beli_10
        '
        Me.txt_id_beli_10.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_10.Location = New System.Drawing.Point(92, 87)
        Me.txt_id_beli_10.Name = "txt_id_beli_10"
        Me.txt_id_beli_10.ReadOnly = True
        Me.txt_id_beli_10.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_10.TabIndex = 80
        Me.txt_id_beli_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_9
        '
        Me.txt_id_beli_9.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_9.Location = New System.Drawing.Point(92, 67)
        Me.txt_id_beli_9.Name = "txt_id_beli_9"
        Me.txt_id_beli_9.ReadOnly = True
        Me.txt_id_beli_9.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_9.TabIndex = 79
        Me.txt_id_beli_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(70, 112)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(38, 13)
        Me.Label16.TabIndex = 78
        Me.Label16.Text = "ID Beli"
        '
        'txt_id_beli_1
        '
        Me.txt_id_beli_1.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_1.Location = New System.Drawing.Point(7, 6)
        Me.txt_id_beli_1.Name = "txt_id_beli_1"
        Me.txt_id_beli_1.ReadOnly = True
        Me.txt_id_beli_1.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_1.TabIndex = 77
        Me.txt_id_beli_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_2
        '
        Me.txt_id_beli_2.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_2.Location = New System.Drawing.Point(7, 26)
        Me.txt_id_beli_2.Name = "txt_id_beli_2"
        Me.txt_id_beli_2.ReadOnly = True
        Me.txt_id_beli_2.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_2.TabIndex = 77
        Me.txt_id_beli_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_3
        '
        Me.txt_id_beli_3.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_3.Location = New System.Drawing.Point(7, 46)
        Me.txt_id_beli_3.Name = "txt_id_beli_3"
        Me.txt_id_beli_3.ReadOnly = True
        Me.txt_id_beli_3.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_3.TabIndex = 77
        Me.txt_id_beli_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_4
        '
        Me.txt_id_beli_4.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_4.Location = New System.Drawing.Point(7, 66)
        Me.txt_id_beli_4.Name = "txt_id_beli_4"
        Me.txt_id_beli_4.ReadOnly = True
        Me.txt_id_beli_4.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_4.TabIndex = 77
        Me.txt_id_beli_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_6
        '
        Me.txt_id_beli_6.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_6.Location = New System.Drawing.Point(92, 7)
        Me.txt_id_beli_6.Name = "txt_id_beli_6"
        Me.txt_id_beli_6.ReadOnly = True
        Me.txt_id_beli_6.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_6.TabIndex = 77
        Me.txt_id_beli_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_7
        '
        Me.txt_id_beli_7.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_7.Location = New System.Drawing.Point(92, 27)
        Me.txt_id_beli_7.Name = "txt_id_beli_7"
        Me.txt_id_beli_7.ReadOnly = True
        Me.txt_id_beli_7.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_7.TabIndex = 77
        Me.txt_id_beli_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_8
        '
        Me.txt_id_beli_8.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_8.Location = New System.Drawing.Point(92, 47)
        Me.txt_id_beli_8.Name = "txt_id_beli_8"
        Me.txt_id_beli_8.ReadOnly = True
        Me.txt_id_beli_8.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_8.TabIndex = 77
        Me.txt_id_beli_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_id_beli_5
        '
        Me.txt_id_beli_5.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli_5.Location = New System.Drawing.Point(7, 86)
        Me.txt_id_beli_5.Name = "txt_id_beli_5"
        Me.txt_id_beli_5.ReadOnly = True
        Me.txt_id_beli_5.Size = New System.Drawing.Size(79, 20)
        Me.txt_id_beli_5.TabIndex = 77
        Me.txt_id_beli_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_harga_1
        '
        Me.txt_harga_1.Location = New System.Drawing.Point(8, 9)
        Me.txt_harga_1.Name = "txt_harga_1"
        Me.txt_harga_1.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_1.TabIndex = 0
        '
        'txt_harga_2
        '
        Me.txt_harga_2.Location = New System.Drawing.Point(8, 29)
        Me.txt_harga_2.Name = "txt_harga_2"
        Me.txt_harga_2.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_2.TabIndex = 1
        '
        'txt_harga_3
        '
        Me.txt_harga_3.Location = New System.Drawing.Point(8, 49)
        Me.txt_harga_3.Name = "txt_harga_3"
        Me.txt_harga_3.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_3.TabIndex = 2
        '
        'txt_harga_4
        '
        Me.txt_harga_4.Location = New System.Drawing.Point(8, 69)
        Me.txt_harga_4.Name = "txt_harga_4"
        Me.txt_harga_4.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_4.TabIndex = 3
        '
        'txt_harga_5
        '
        Me.txt_harga_5.Location = New System.Drawing.Point(8, 89)
        Me.txt_harga_5.Name = "txt_harga_5"
        Me.txt_harga_5.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_5.TabIndex = 4
        '
        'txt_harga_6
        '
        Me.txt_harga_6.Location = New System.Drawing.Point(84, 9)
        Me.txt_harga_6.Name = "txt_harga_6"
        Me.txt_harga_6.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_6.TabIndex = 5
        '
        'txt_harga_7
        '
        Me.txt_harga_7.Location = New System.Drawing.Point(84, 29)
        Me.txt_harga_7.Name = "txt_harga_7"
        Me.txt_harga_7.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_7.TabIndex = 6
        '
        'txt_harga_8
        '
        Me.txt_harga_8.Location = New System.Drawing.Point(84, 49)
        Me.txt_harga_8.Name = "txt_harga_8"
        Me.txt_harga_8.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_8.TabIndex = 7
        '
        'txt_harga_9
        '
        Me.txt_harga_9.Location = New System.Drawing.Point(84, 69)
        Me.txt_harga_9.Name = "txt_harga_9"
        Me.txt_harga_9.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_9.TabIndex = 8
        '
        'txt_harga_10
        '
        Me.txt_harga_10.Location = New System.Drawing.Point(84, 89)
        Me.txt_harga_10.Name = "txt_harga_10"
        Me.txt_harga_10.Size = New System.Drawing.Size(70, 20)
        Me.txt_harga_10.TabIndex = 9
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Label20)
        Me.Panel7.Controls.Add(Me.txt_harga_10)
        Me.Panel7.Controls.Add(Me.txt_harga_9)
        Me.Panel7.Controls.Add(Me.txt_harga_8)
        Me.Panel7.Controls.Add(Me.txt_harga_7)
        Me.Panel7.Controls.Add(Me.txt_harga_6)
        Me.Panel7.Controls.Add(Me.txt_harga_5)
        Me.Panel7.Controls.Add(Me.txt_harga_4)
        Me.Panel7.Controls.Add(Me.txt_harga_3)
        Me.Panel7.Controls.Add(Me.txt_harga_2)
        Me.Panel7.Controls.Add(Me.txt_harga_1)
        Me.Panel7.Location = New System.Drawing.Point(790, 3)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(163, 134)
        Me.Panel7.TabIndex = 44
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(63, 107)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(36, 13)
        Me.Label20.TabIndex = 10
        Me.Label20.Text = "Harga"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Label21)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_10)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_9)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_8)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_7)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_6)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_5)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_4)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_3)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_2)
        Me.Panel9.Controls.Add(Me.txt_id_po_proses_1)
        Me.Panel9.Location = New System.Drawing.Point(340, 2)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(163, 134)
        Me.Panel9.TabIndex = 110
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(46, 113)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(71, 13)
        Me.Label21.TabIndex = 10
        Me.Label21.Text = "ID PO Proses"
        '
        'txt_id_po_proses_10
        '
        Me.txt_id_po_proses_10.Location = New System.Drawing.Point(84, 89)
        Me.txt_id_po_proses_10.Name = "txt_id_po_proses_10"
        Me.txt_id_po_proses_10.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_10.TabIndex = 9
        '
        'txt_id_po_proses_9
        '
        Me.txt_id_po_proses_9.Location = New System.Drawing.Point(84, 69)
        Me.txt_id_po_proses_9.Name = "txt_id_po_proses_9"
        Me.txt_id_po_proses_9.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_9.TabIndex = 8
        '
        'txt_id_po_proses_8
        '
        Me.txt_id_po_proses_8.Location = New System.Drawing.Point(84, 49)
        Me.txt_id_po_proses_8.Name = "txt_id_po_proses_8"
        Me.txt_id_po_proses_8.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_8.TabIndex = 7
        '
        'txt_id_po_proses_7
        '
        Me.txt_id_po_proses_7.Location = New System.Drawing.Point(84, 29)
        Me.txt_id_po_proses_7.Name = "txt_id_po_proses_7"
        Me.txt_id_po_proses_7.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_7.TabIndex = 6
        '
        'txt_id_po_proses_6
        '
        Me.txt_id_po_proses_6.Location = New System.Drawing.Point(84, 9)
        Me.txt_id_po_proses_6.Name = "txt_id_po_proses_6"
        Me.txt_id_po_proses_6.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_6.TabIndex = 5
        '
        'txt_id_po_proses_5
        '
        Me.txt_id_po_proses_5.Location = New System.Drawing.Point(8, 89)
        Me.txt_id_po_proses_5.Name = "txt_id_po_proses_5"
        Me.txt_id_po_proses_5.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_5.TabIndex = 4
        '
        'txt_id_po_proses_4
        '
        Me.txt_id_po_proses_4.Location = New System.Drawing.Point(8, 69)
        Me.txt_id_po_proses_4.Name = "txt_id_po_proses_4"
        Me.txt_id_po_proses_4.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_4.TabIndex = 3
        '
        'txt_id_po_proses_3
        '
        Me.txt_id_po_proses_3.Location = New System.Drawing.Point(8, 49)
        Me.txt_id_po_proses_3.Name = "txt_id_po_proses_3"
        Me.txt_id_po_proses_3.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_3.TabIndex = 2
        '
        'txt_id_po_proses_2
        '
        Me.txt_id_po_proses_2.Location = New System.Drawing.Point(8, 29)
        Me.txt_id_po_proses_2.Name = "txt_id_po_proses_2"
        Me.txt_id_po_proses_2.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_2.TabIndex = 1
        '
        'txt_id_po_proses_1
        '
        Me.txt_id_po_proses_1.Location = New System.Drawing.Point(8, 9)
        Me.txt_id_po_proses_1.Name = "txt_id_po_proses_1"
        Me.txt_id_po_proses_1.Size = New System.Drawing.Size(70, 20)
        Me.txt_id_po_proses_1.TabIndex = 0
        '
        'form_input_po_proses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1294, 622)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel_10)
        Me.Controls.Add(Me.Panel_9)
        Me.Controls.Add(Me.Panel_8)
        Me.Controls.Add(Me.Panel_7)
        Me.Controls.Add(Me.Panel_6)
        Me.Controls.Add(Me.Panel_5)
        Me.Controls.Add(Me.Panel_4)
        Me.Controls.Add(Me.Panel_3)
        Me.Controls.Add(Me.Panel_1)
        Me.Controls.Add(Me.Panel_2)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel6)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_po_proses"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM INPUT PO PROSES BARU"
        Me.Panel1.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel_1.ResumeLayout(False)
        Me.Panel_1.PerformLayout()
        Me.Panel_2.ResumeLayout(False)
        Me.Panel_2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel_3.ResumeLayout(False)
        Me.Panel_3.PerformLayout()
        Me.Panel_4.ResumeLayout(False)
        Me.Panel_4.PerformLayout()
        Me.Panel_5.ResumeLayout(False)
        Me.Panel_5.PerformLayout()
        Me.Panel_10.ResumeLayout(False)
        Me.Panel_10.PerformLayout()
        Me.Panel_9.ResumeLayout(False)
        Me.Panel_9.PerformLayout()
        Me.Panel_8.ResumeLayout(False)
        Me.Panel_8.PerformLayout()
        Me.Panel_7.ResumeLayout(False)
        Me.Panel_7.PerformLayout()
        Me.Panel_6.ResumeLayout(False)
        Me.Panel_6.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_no_po As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Panel_1 As System.Windows.Forms.Panel
    Friend WithEvents txt_note_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_1 As System.Windows.Forms.Label
    Friend WithEvents txt_hand_fill_1 As System.Windows.Forms.TextBox
    Friend WithEvents btn_tambah_baris_1 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_1 As System.Windows.Forms.Button
    Friend WithEvents Panel_2 As System.Windows.Forms.Panel
    Friend WithEvents txt_hand_fill_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_2 As System.Windows.Forms.Label
    Friend WithEvents btn_hapus_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_2 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_2 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_4 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_4 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_4 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_3 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_3 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_3 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_8 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_8 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_8 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_7 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_7 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_7 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_6 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_6 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_6 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_5 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_5 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_5 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_10 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_10 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_9 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_9 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_9 As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel_3 As System.Windows.Forms.Panel
    Friend WithEvents txt_hand_fill_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_3 As System.Windows.Forms.Label
    Friend WithEvents Panel_4 As System.Windows.Forms.Panel
    Friend WithEvents txt_hand_fill_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_4 As System.Windows.Forms.Label
    Friend WithEvents Panel_5 As System.Windows.Forms.Panel
    Friend WithEvents Panel_10 As System.Windows.Forms.Panel
    Friend WithEvents Panel_9 As System.Windows.Forms.Panel
    Friend WithEvents Panel_8 As System.Windows.Forms.Panel
    Friend WithEvents txt_hand_fill_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_8 As System.Windows.Forms.Label
    Friend WithEvents txt_hand_fill_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_9 As System.Windows.Forms.Label
    Friend WithEvents txt_hand_fill_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_10 As System.Windows.Forms.Label
    Friend WithEvents txt_hand_fill_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_5 As System.Windows.Forms.Label
    Friend WithEvents Panel_7 As System.Windows.Forms.Panel
    Friend WithEvents txt_hand_fill_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_7 As System.Windows.Forms.Label
    Friend WithEvents Panel_6 As System.Windows.Forms.Panel
    Friend WithEvents txt_hand_fill_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_note_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_keterangan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_tarik_lebar_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_6 As System.Windows.Forms.Label
    Friend WithEvents txt_note_po As System.Windows.Forms.RichTextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txt_qty_asal_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents txt_id_grey_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_1 As System.Windows.Forms.TextBox
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents btn_batal_7 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_6 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_5 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_4 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_3 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_2 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_1 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_10 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_9 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_8 As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents txt_id_beli_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_9 As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_id_beli_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_beli_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_10 As System.Windows.Forms.TextBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txt_id_po_proses_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_proses_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cb_satuan_1 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_2 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_3 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_4 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_5 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_10 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_9 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_8 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_7 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_6 As System.Windows.Forms.ComboBox
End Class
