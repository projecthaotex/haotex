﻿Imports MySql.Data.MySqlClient

Public Class form_input_pembelian_grey_baru

    Private Sub form_input_pembelian_grey_baru_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel_2.Visible = False
        Panel_3.Visible = False
        Panel_4.Visible = False
        Panel_5.Visible = False
        Panel_6.Visible = False
        Panel_7.Visible = False
        Panel_8.Visible = False
        btn_hapus_2.Visible = False
        btn_hapus_3.Visible = False
        btn_hapus_4.Visible = False
        btn_hapus_5.Visible = False
        btn_hapus_6.Visible = False
        btn_hapus_7.Visible = False
        btn_hapus_8.Visible = False
        btn_batal_2.Visible = False
        btn_batal_3.Visible = False
        btn_batal_4.Visible = False
        btn_batal_5.Visible = False
        btn_batal_6.Visible = False
        btn_batal_7.Visible = False
        btn_batal_8.Visible = False
        btn_tambah_baris_2.Visible = False
        btn_tambah_baris_3.Visible = False
        btn_tambah_baris_4.Visible = False
        btn_tambah_baris_5.Visible = False
        btn_tambah_baris_6.Visible = False
        btn_tambah_baris_7.Visible = False
        btn_selesai_2.Visible = False
        btn_selesai_3.Visible = False
        btn_selesai_4.Visible = False
        btn_selesai_5.Visible = False
        btn_selesai_6.Visible = False
        btn_selesai_7.Visible = False
        btn_selesai_8.Visible = False
    End Sub
    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        If Not e.KeyChar = Chr(13) Then e.Handled = True
    End Sub
    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim tanggal As DateTime
        tanggal = dtp_awal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub dtp_awal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_awal.ValueChanged
        Dim tanggal As DateTime
        tanggal = dtp_awal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub

    Private Sub isikodegreybaru_1()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_1.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_1.Text = y.ToString
        Else
            txt_id_grey_1.Text = x.ToString
        End If
    End Sub
    Private Sub isikodegreybaru_2()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_2.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_2.Text = y.ToString
        Else
            txt_id_grey_2.Text = x.ToString
        End If
    End Sub
    Private Sub isikodegreybaru_3()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_3.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_3.Text = y.ToString
        Else
            txt_id_grey_3.Text = x.ToString
        End If
    End Sub
    Private Sub isikodegreybaru_4()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_4.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_4.Text = y.ToString
        Else
            txt_id_grey_4.Text = x.ToString
        End If
    End Sub
    Private Sub isikodegreybaru_5()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_5.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_5.Text = y.ToString
        Else
            txt_id_grey_5.Text = x.ToString
        End If
    End Sub
    Private Sub isikodegreybaru_6()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_6.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_6.Text = y.ToString
        Else
            txt_id_grey_6.Text = x.ToString
        End If
    End Sub
    Private Sub isikodegreybaru_7()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_7.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_7.Text = y.ToString
        Else
            txt_id_grey_7.Text = x.ToString
        End If
    End Sub
    Private Sub isikodegreybaru_8()
        Dim x As Integer
        Dim y As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbkontrakgrey WHERE Id_Grey in(select max(Id_Grey) from tbkontrakgrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Grey")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey FROM tbpembeliangrey WHERE Id_Grey in(select max(Id_Grey) from tbpembeliangrey) AND No_Kontrak='-'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        y = Val(drx.Item("Id_Grey")) + 1
                    Else
                        y = 1
                    End If
                End Using
            End Using
        End Using
        If x > y Then
            txt_id_grey_8.Text = x.ToString
        ElseIf y > x Then
            txt_id_grey_8.Text = y.ToString
        Else
            txt_id_grey_8.Text = x.ToString
        End If
    End Sub

    Private Sub isikodebeli_1()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_1.Text = x.ToString
    End Sub
    Private Sub isikodebeli_2()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_2.Text = x.ToString
    End Sub
    Private Sub isikodebeli_3()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_3.Text = x.ToString
    End Sub
    Private Sub isikodebeli_4()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_4.Text = x.ToString
    End Sub
    Private Sub isikodebeli_5()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_5.Text = x.ToString
    End Sub
    Private Sub isikodebeli_6()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_6.Text = x.ToString
    End Sub
    Private Sub isikodebeli_7()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_7.Text = x.ToString
    End Sub
    Private Sub isikodebeli_8()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Beli FROM tbpembeliangrey WHERE Id_Beli in(select max(Id_Beli) FROM tbpembeliangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Beli")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_beli_8.Text = x.ToString
    End Sub

    Private Sub isikodehutang_1()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_1.Text = x.ToString
    End Sub
    Private Sub isikodehutang_2()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_2.Text = x.ToString
    End Sub
    Private Sub isikodehutang_3()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_3.Text = x.ToString
    End Sub
    Private Sub isikodehutang_4()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_4.Text = x.ToString
    End Sub
    Private Sub isikodehutang_5()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_5.Text = x.ToString
    End Sub
    Private Sub isikodehutang_6()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_6.Text = x.ToString
    End Sub
    Private Sub isikodehutang_7()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_7.Text = x.ToString
    End Sub
    Private Sub isikodehutang_8()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_8.Text = x.ToString
    End Sub

    Private Sub txt_jenis_kain_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_1.GotFocus
        If rb_non_kontrak_1.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_1"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_1.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_1"
        End If
    End Sub
    Private Sub txt_jenis_kain_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_2.GotFocus
        If rb_non_kontrak_2.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_2"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_2.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_2"
        End If
    End Sub
    Private Sub txt_jenis_kain_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_3.GotFocus
        If rb_non_kontrak_3.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_3"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_3.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_3"
        End If
    End Sub
    Private Sub txt_jenis_kain_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_4.GotFocus
        If rb_non_kontrak_4.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_4"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_4.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_4"
        End If
    End Sub
    Private Sub txt_jenis_kain_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_5.GotFocus
        If rb_non_kontrak_5.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_5"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_5.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_5"
        End If
    End Sub
    Private Sub txt_jenis_kain_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_6.GotFocus
        If rb_non_kontrak_6.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_6"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_6.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_6"
        End If
    End Sub
    Private Sub txt_jenis_kain_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_7.GotFocus
        If rb_non_kontrak_7.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_7"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_7.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_7"
        End If
    End Sub
    Private Sub txt_jenis_kain_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain_8.GotFocus
        If rb_non_kontrak_8.Checked = True Then
            form_input_jenis_kain.MdiParent = form_menu_utama
            form_input_jenis_kain.Show()
            form_input_jenis_kain.TxtForm.Text = "form_input_pembelian_grey_baru_8"
            form_input_jenis_kain.Focus()
        ElseIf rb_kontrak_8.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_8"
        End If
    End Sub
    Private Sub txt_supplier_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_1.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_1"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_supplier_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_2.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_2"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_supplier_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_3.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_3"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_supplier_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_4.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_4"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_supplier_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_5.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_5"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_supplier_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_6.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_6"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_supplier_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_7.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_7"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_supplier_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_supplier_8.GotFocus
        form_input_supplier.MdiParent = form_menu_utama
        form_input_supplier.Show()
        form_input_supplier.TxtForm.Text = "form_input_pembelian_grey_baru_8"
        form_input_supplier.Focus()
    End Sub
    Private Sub txt_gudang_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_1.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_1"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_gudang_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_2.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_2"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_gudang_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_3.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_3"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_gudang_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_4.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_4"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_gudang_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_5.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_5"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_gudang_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_6.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_6"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_gudang_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_7.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_7"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_gudang_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_8.GotFocus
        form_input_gudang.MdiParent = form_menu_utama
        form_input_gudang.Show()
        form_input_gudang.TxtForm.Text = "form_input_pembelian_grey_baru_8"
        form_input_gudang.Focus()
    End Sub
    Private Sub txt_no_kontrak_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_1.GotFocus
        If rb_kontrak_1.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_1"
        End If
    End Sub
    Private Sub txt_no_kontrak_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_2.GotFocus
        If rb_kontrak_2.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_2"
        End If
    End Sub
    Private Sub txt_no_kontrak_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_3.GotFocus
        If rb_kontrak_3.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_3"
        End If
    End Sub
    Private Sub txt_no_kontrak_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_4.GotFocus
        If rb_kontrak_4.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_4"
        End If
    End Sub
    Private Sub txt_no_kontrak_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_5.GotFocus
        If rb_kontrak_5.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_5"
        End If
    End Sub
    Private Sub txt_no_kontrak_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_6.GotFocus
        If rb_kontrak_6.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_6"
        End If
    End Sub
    Private Sub txt_no_kontrak_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_7.GotFocus
        If rb_kontrak_7.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_7"
        End If
    End Sub
    Private Sub txt_no_kontrak_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_kontrak_8.GotFocus
        If rb_kontrak_8.Checked = True Then
            form_input_no_kontrak.MdiParent = form_menu_utama
            form_input_no_kontrak.Show()
            form_input_no_kontrak.Focus()
            form_input_no_kontrak.TxtForm.Text = "form_input_pembelian_grey_baru_8"
        End If
    End Sub

    Private Sub txt_harga_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_1.Text = String.Empty Then
                        txt_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_1.Select(txt_harga_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_1.Text = String.Empty Then
                        txt_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_1.Select(txt_harga_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_1.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_1.Focus()
        End If
    End Sub
    Private Sub txt_harga_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_2.Text = String.Empty Then
                        txt_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_2.Select(txt_harga_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_2.Text = String.Empty Then
                        txt_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_2.Select(txt_harga_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_2.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_2.Focus()
        End If
    End Sub
    Private Sub txt_harga_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_3.Text = String.Empty Then
                        txt_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_3.Select(txt_harga_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_3.Text = String.Empty Then
                        txt_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_3.Select(txt_harga_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_3.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_3.Focus()
        End If
    End Sub
    Private Sub txt_harga_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_4.Text = String.Empty Then
                        txt_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_4.Select(txt_harga_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_4.Text = String.Empty Then
                        txt_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_4.Select(txt_harga_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_4.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_4.Focus()
        End If
    End Sub
    Private Sub txt_harga_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_5.Text = String.Empty Then
                        txt_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_5.Select(txt_harga_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_5.Text = String.Empty Then
                        txt_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_5.Select(txt_harga_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_5.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_5.Focus()
        End If
    End Sub
    Private Sub txt_harga_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_6.Text = String.Empty Then
                        txt_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_6.Select(txt_harga_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_6.Text = String.Empty Then
                        txt_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_6.Select(txt_harga_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_6.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_6.Focus()
        End If
    End Sub
    Private Sub txt_harga_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_7.Text = String.Empty Then
                        txt_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_7.Select(txt_harga_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_7.Text = String.Empty Then
                        txt_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_7.Select(txt_harga_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_7.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_7.Focus()
        End If
    End Sub
    Private Sub txt_harga_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_8.Text = String.Empty Then
                        txt_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_8.Select(txt_harga_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_8.Text = String.Empty Then
                        txt_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_8.Select(txt_harga_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If rb_non_kontrak_8.Checked = True Then
            If e.KeyChar = Chr(13) Then txt_qty_8.Focus()
        End If
    End Sub
    Private Sub txt_qty_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_1.Text = String.Empty Then
                        txt_qty_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_1.Select(txt_qty_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_1.Text = String.Empty Then
                        txt_qty_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_1.Select(txt_qty_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_1.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_1.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_1.Focus()
        End If
    End Sub
    Private Sub txt_qty_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_2.Text = String.Empty Then
                        txt_qty_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_2.Select(txt_qty_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_2.Text = String.Empty Then
                        txt_qty_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_2.Select(txt_qty_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_2.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_2.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_2.Focus()
        End If
    End Sub
    Private Sub txt_qty_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_3.Text = String.Empty Then
                        txt_qty_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_3.Select(txt_qty_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_3.Text = String.Empty Then
                        txt_qty_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_3.Select(txt_qty_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_3.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_3.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_3.Focus()
        End If
    End Sub
    Private Sub txt_qty_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_4.Text = String.Empty Then
                        txt_qty_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_4.Select(txt_qty_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_4.Text = String.Empty Then
                        txt_qty_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_4.Select(txt_qty_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_4.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_4.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_4.Focus()
        End If
    End Sub
    Private Sub txt_qty_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_5.Text = String.Empty Then
                        txt_qty_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_5.Select(txt_qty_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_5.Text = String.Empty Then
                        txt_qty_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_5.Select(txt_qty_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_5.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_5.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_5.Focus()
        End If
    End Sub
    Private Sub txt_qty_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_6.Text = String.Empty Then
                        txt_qty_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_6.Select(txt_qty_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_6.Text = String.Empty Then
                        txt_qty_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_6.Select(txt_qty_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_6.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_6.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_6.Focus()
        End If
    End Sub
    Private Sub txt_qty_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_7.Text = String.Empty Then
                        txt_qty_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_7.Select(txt_qty_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_7.Text = String.Empty Then
                        txt_qty_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_7.Select(txt_qty_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_7.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_7.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_7.Focus()
        End If
    End Sub
    Private Sub txt_qty_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_8.Text = String.Empty Then
                        txt_qty_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_8.Select(txt_qty_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_8.Text = String.Empty Then
                        txt_qty_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_8.Select(txt_qty_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_8.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_8.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_8.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        txt_total_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        txt_total_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_1.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_1.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_1.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        txt_total_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        txt_total_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_2.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_2.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_2.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        txt_total_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        txt_total_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_3.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_3.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_3.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        txt_total_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        txt_total_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_4.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_4.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_4.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        txt_total_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        txt_total_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_5.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_5.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_5.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        txt_total_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        txt_total_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_6.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_6.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_6.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        txt_total_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        txt_total_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_7.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_7.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_7.Focus()
        End If
    End Sub
    Private Sub txt_total_harga_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        txt_total_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        txt_total_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
        If txt_gudang_8.Text = "" Then
            If e.KeyChar = Chr(13) Then txt_gudang_8.Focus()
        Else
            If e.KeyChar = Chr(13) Then txt_keterangan_8.Focus()
        End If
    End Sub

    Private Sub txt_harga_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_1.TextChanged
        If txt_harga_1.Text <> String.Empty Then
            Dim temp As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_1.Select(txt_harga_1.Text.Length, 0)
            ElseIf txt_harga_1.Text = "-"c Then

            Else
                txt_harga_1.Text = CDec(temp).ToString("N0")
                txt_harga_1.Select(txt_harga_1.Text.Length, 0)
            End If
        End If
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_1.Text = Val(harga_1.Replace(",", ".")) * Val(qty_1.Replace(",", "."))
    End Sub
    Private Sub txt_harga_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_2.TextChanged
        If txt_harga_2.Text <> String.Empty Then
            Dim temp As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_2.Select(txt_harga_2.Text.Length, 0)
            ElseIf txt_harga_2.Text = "-"c Then

            Else
                txt_harga_2.Text = CDec(temp).ToString("N0")
                txt_harga_2.Select(txt_harga_2.Text.Length, 0)
            End If
        End If
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_2.Text = Val(harga_2.Replace(",", ".")) * Val(qty_2.Replace(",", "."))
    End Sub
    Private Sub txt_harga_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_3.TextChanged
        If txt_harga_3.Text <> String.Empty Then
            Dim temp As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_3.Select(txt_harga_3.Text.Length, 0)
            ElseIf txt_harga_3.Text = "-"c Then

            Else
                txt_harga_3.Text = CDec(temp).ToString("N0")
                txt_harga_3.Select(txt_harga_3.Text.Length, 0)
            End If
        End If
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_3.Text = Val(harga_3.Replace(",", ".")) * Val(qty_3.Replace(",", "."))
    End Sub
    Private Sub txt_harga_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_4.TextChanged
        If txt_harga_4.Text <> String.Empty Then
            Dim temp As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_4.Select(txt_harga_4.Text.Length, 0)
            ElseIf txt_harga_4.Text = "-"c Then

            Else
                txt_harga_4.Text = CDec(temp).ToString("N0")
                txt_harga_4.Select(txt_harga_4.Text.Length, 0)
            End If
        End If
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_4.Text = Val(harga_4.Replace(",", ".")) * Val(qty_4.Replace(",", "."))
    End Sub
    Private Sub txt_harga_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_5.TextChanged
        If txt_harga_5.Text <> String.Empty Then
            Dim temp As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_5.Select(txt_harga_5.Text.Length, 0)
            ElseIf txt_harga_5.Text = "-"c Then

            Else
                txt_harga_5.Text = CDec(temp).ToString("N0")
                txt_harga_5.Select(txt_harga_5.Text.Length, 0)
            End If
        End If
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_5.Text = Val(harga_5.Replace(",", ".")) * Val(qty_5.Replace(",", "."))
    End Sub
    Private Sub txt_harga_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_6.TextChanged
        If txt_harga_6.Text <> String.Empty Then
            Dim temp As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_6.Select(txt_harga_6.Text.Length, 0)
            ElseIf txt_harga_6.Text = "-"c Then

            Else
                txt_harga_6.Text = CDec(temp).ToString("N0")
                txt_harga_6.Select(txt_harga_6.Text.Length, 0)
            End If
        End If
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_6.Text = Val(harga_6.Replace(",", ".")) * Val(qty_6.Replace(",", "."))
    End Sub
    Private Sub txt_harga_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_7.TextChanged
        If txt_harga_7.Text <> String.Empty Then
            Dim temp As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_7.Select(txt_harga_7.Text.Length, 0)
            ElseIf txt_harga_7.Text = "-"c Then

            Else
                txt_harga_7.Text = CDec(temp).ToString("N0")
                txt_harga_7.Select(txt_harga_7.Text.Length, 0)
            End If
        End If
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_7.Text = Val(harga_7.Replace(",", ".")) * Val(qty_7.Replace(",", "."))
    End Sub
    Private Sub txt_harga_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_8.TextChanged
        If txt_harga_8.Text <> String.Empty Then
            Dim temp As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_8.Select(txt_harga_8.Text.Length, 0)
            ElseIf txt_harga_8.Text = "-"c Then

            Else
                txt_harga_8.Text = CDec(temp).ToString("N0")
                txt_harga_8.Select(txt_harga_8.Text.Length, 0)
            End If
        End If
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_8.Text = Val(harga_8.Replace(",", ".")) * Val(qty_8.Replace(",", "."))
    End Sub
    Private Sub txt_sisa_kontrak_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_1.TextChanged
        If txt_sisa_kontrak_1.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_1.Select(txt_sisa_kontrak_1.Text.Length, 0)
            ElseIf txt_sisa_kontrak_1.Text = "-"c Then

            Else
                txt_sisa_kontrak_1.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_1.Select(txt_sisa_kontrak_1.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_2.TextChanged
        If txt_sisa_kontrak_2.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_2.Select(txt_sisa_kontrak_2.Text.Length, 0)
            ElseIf txt_sisa_kontrak_2.Text = "-"c Then

            Else
                txt_sisa_kontrak_2.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_2.Select(txt_sisa_kontrak_2.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_3.TextChanged
        If txt_sisa_kontrak_3.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_3.Select(txt_sisa_kontrak_3.Text.Length, 0)
            ElseIf txt_sisa_kontrak_3.Text = "-"c Then

            Else
                txt_sisa_kontrak_3.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_3.Select(txt_sisa_kontrak_3.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_4.TextChanged
        If txt_sisa_kontrak_4.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_4.Select(txt_sisa_kontrak_4.Text.Length, 0)
            ElseIf txt_sisa_kontrak_4.Text = "-"c Then

            Else
                txt_sisa_kontrak_4.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_4.Select(txt_sisa_kontrak_4.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_5.TextChanged
        If txt_sisa_kontrak_5.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_5.Select(txt_sisa_kontrak_5.Text.Length, 0)
            ElseIf txt_sisa_kontrak_5.Text = "-"c Then

            Else
                txt_sisa_kontrak_5.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_5.Select(txt_sisa_kontrak_5.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_6.TextChanged
        If txt_sisa_kontrak_6.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_6.Select(txt_sisa_kontrak_6.Text.Length, 0)
            ElseIf txt_sisa_kontrak_6.Text = "-"c Then

            Else
                txt_sisa_kontrak_6.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_6.Select(txt_sisa_kontrak_6.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_7.TextChanged
        If txt_sisa_kontrak_7.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_7.Select(txt_sisa_kontrak_7.Text.Length, 0)
            ElseIf txt_sisa_kontrak_7.Text = "-"c Then

            Else
                txt_sisa_kontrak_7.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_7.Select(txt_sisa_kontrak_7.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_kontrak_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_kontrak_8.TextChanged
        If txt_sisa_kontrak_8.Text <> String.Empty Then
            Dim temp As String = txt_sisa_kontrak_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_kontrak_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_kontrak_8.Select(txt_sisa_kontrak_8.Text.Length, 0)
            ElseIf txt_sisa_kontrak_8.Text = "-"c Then

            Else
                txt_sisa_kontrak_8.Text = CDec(temp).ToString("N0")
                txt_sisa_kontrak_8.Select(txt_sisa_kontrak_8.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_qty_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_1.TextChanged
        If txt_qty_1.Text <> String.Empty Then
            Dim temp As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_1.Select(txt_qty_1.Text.Length, 0)
            ElseIf txt_qty_1.Text = "-"c Then

            Else
                txt_qty_1.Text = CDec(temp).ToString("N0")
                txt_qty_1.Select(txt_qty_1.Text.Length, 0)
            End If
        End If
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_1.Text = Val(harga_1.Replace(",", ".")) * Val(qty_1.Replace(",", "."))
    End Sub
    Private Sub txt_qty_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_2.TextChanged
        If txt_qty_2.Text <> String.Empty Then
            Dim temp As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_2.Select(txt_qty_2.Text.Length, 0)
            ElseIf txt_qty_2.Text = "-"c Then

            Else
                txt_qty_2.Text = CDec(temp).ToString("N0")
                txt_qty_2.Select(txt_qty_2.Text.Length, 0)
            End If
        End If
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_2.Text = Val(harga_2.Replace(",", ".")) * Val(qty_2.Replace(",", "."))
    End Sub
    Private Sub txt_qty_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_3.TextChanged
        If txt_qty_3.Text <> String.Empty Then
            Dim temp As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_3.Select(txt_qty_3.Text.Length, 0)
            ElseIf txt_qty_3.Text = "-"c Then

            Else
                txt_qty_3.Text = CDec(temp).ToString("N0")
                txt_qty_3.Select(txt_qty_3.Text.Length, 0)
            End If
        End If
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_3.Text = Val(harga_3.Replace(",", ".")) * Val(qty_3.Replace(",", "."))
    End Sub
    Private Sub txt_qty_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_4.TextChanged
        If txt_qty_4.Text <> String.Empty Then
            Dim temp As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_4.Select(txt_qty_4.Text.Length, 0)
            ElseIf txt_qty_4.Text = "-"c Then

            Else
                txt_qty_4.Text = CDec(temp).ToString("N0")
                txt_qty_4.Select(txt_qty_4.Text.Length, 0)
            End If
        End If
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_4.Text = Val(harga_4.Replace(",", ".")) * Val(qty_4.Replace(",", "."))
    End Sub
    Private Sub txt_qty_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_5.TextChanged
        If txt_qty_5.Text <> String.Empty Then
            Dim temp As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_5.Select(txt_qty_5.Text.Length, 0)
            ElseIf txt_qty_5.Text = "-"c Then

            Else
                txt_qty_5.Text = CDec(temp).ToString("N0")
                txt_qty_5.Select(txt_qty_5.Text.Length, 0)
            End If
        End If
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_5.Text = Val(harga_5.Replace(",", ".")) * Val(qty_5.Replace(",", "."))
    End Sub
    Private Sub txt_qty_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_6.TextChanged
        If txt_qty_6.Text <> String.Empty Then
            Dim temp As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_6.Select(txt_qty_6.Text.Length, 0)
            ElseIf txt_qty_6.Text = "-"c Then

            Else
                txt_qty_6.Text = CDec(temp).ToString("N0")
                txt_qty_6.Select(txt_qty_6.Text.Length, 0)
            End If
        End If
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_6.Text = Val(harga_6.Replace(",", ".")) * Val(qty_6.Replace(",", "."))
    End Sub
    Private Sub txt_qty_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_7.TextChanged
        If txt_qty_7.Text <> String.Empty Then
            Dim temp As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_7.Select(txt_qty_7.Text.Length, 0)
            ElseIf txt_qty_7.Text = "-"c Then

            Else
                txt_qty_7.Text = CDec(temp).ToString("N0")
                txt_qty_7.Select(txt_qty_7.Text.Length, 0)
            End If
        End If
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_7.Text = Val(harga_7.Replace(",", ".")) * Val(qty_7.Replace(",", "."))
    End Sub
    Private Sub txt_qty_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_8.TextChanged
        If txt_qty_8.Text <> String.Empty Then
            Dim temp As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_8.Select(txt_qty_8.Text.Length, 0)
            ElseIf txt_qty_8.Text = "-"c Then

            Else
                txt_qty_8.Text = CDec(temp).ToString("N0")
                txt_qty_8.Select(txt_qty_8.Text.Length, 0)
            End If
        End If
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_8.Text = Val(harga_8.Replace(",", ".")) * Val(qty_8.Replace(",", "."))
    End Sub
    Private Sub txt_total_harga_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_1.TextChanged
        If txt_total_harga_1.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
            ElseIf txt_total_harga_1.Text = "-"c Then

            Else
                txt_total_harga_1.Text = CDec(temp).ToString("N0")
                txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_2.TextChanged
        If txt_total_harga_2.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
            ElseIf txt_total_harga_2.Text = "-"c Then

            Else
                txt_total_harga_2.Text = CDec(temp).ToString("N0")
                txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_3.TextChanged
        If txt_total_harga_3.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
            ElseIf txt_total_harga_3.Text = "-"c Then

            Else
                txt_total_harga_3.Text = CDec(temp).ToString("N0")
                txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_4.TextChanged
        If txt_total_harga_4.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
            ElseIf txt_total_harga_4.Text = "-"c Then

            Else
                txt_total_harga_4.Text = CDec(temp).ToString("N0")
                txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_5.TextChanged
        If txt_total_harga_5.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
            ElseIf txt_total_harga_5.Text = "-"c Then

            Else
                txt_total_harga_5.Text = CDec(temp).ToString("N0")
                txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_6.TextChanged
        If txt_total_harga_6.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
            ElseIf txt_total_harga_6.Text = "-"c Then

            Else
                txt_total_harga_6.Text = CDec(temp).ToString("N0")
                txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_7.TextChanged
        If txt_total_harga_7.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
            ElseIf txt_total_harga_7.Text = "-"c Then

            Else
                txt_total_harga_7.Text = CDec(temp).ToString("N0")
                txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_8.TextChanged
        If txt_total_harga_8.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
            ElseIf txt_total_harga_8.Text = "-"c Then

            Else
                txt_total_harga_8.Text = CDec(temp).ToString("N0")
                txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub awal_rb_kontrak_1()
        txt_id_grey_1.Text = ""
        txt_no_kontrak_1.Text = ""
        txt_jenis_kain_1.Text = ""
        txt_jenis_kain_1.ReadOnly = True
        txt_supplier_1.Text = ""
        txt_supplier_1.ReadOnly = True
        txt_harga_1.Text = ""
        txt_qty_1.Text = ""
        txt_total_harga_1.Text = ""
        txt_sisa_kontrak_1.Text = ""
        txt_sisa_kontrak_1.Visible = True
        txt_gudang_1.Text = ""
        txt_keterangan_1.Text = ""
    End Sub
    Private Sub awal_rb_kontrak_2()
        txt_id_grey_2.Text = ""
        txt_no_kontrak_2.Text = ""
        txt_jenis_kain_2.Text = ""
        txt_jenis_kain_2.ReadOnly = True
        txt_supplier_2.Text = ""
        txt_supplier_2.ReadOnly = True
        txt_harga_2.Text = ""
        txt_qty_2.Text = ""
        txt_total_harga_2.Text = ""
        txt_sisa_kontrak_2.Text = ""
        txt_sisa_kontrak_2.Visible = True
        txt_keterangan_2.Text = ""
    End Sub
    Private Sub awal_rb_kontrak_3()
        txt_id_grey_3.Text = ""
        txt_no_kontrak_3.Text = ""
        txt_jenis_kain_3.Text = ""
        txt_jenis_kain_3.ReadOnly = True
        txt_supplier_3.Text = ""
        txt_supplier_3.ReadOnly = True
        txt_harga_3.Text = ""
        txt_qty_3.Text = ""
        txt_total_harga_3.Text = ""
        txt_sisa_kontrak_3.Text = ""
        txt_sisa_kontrak_3.Visible = True
        txt_keterangan_3.Text = ""
    End Sub
    Private Sub awal_rb_kontrak_4()
        txt_id_grey_4.Text = ""
        txt_no_kontrak_4.Text = ""
        txt_jenis_kain_4.Text = ""
        txt_jenis_kain_4.ReadOnly = True
        txt_supplier_4.Text = ""
        txt_supplier_4.ReadOnly = True
        txt_harga_4.Text = ""
        txt_qty_4.Text = ""
        txt_total_harga_4.Text = ""
        txt_sisa_kontrak_4.Text = ""
        txt_sisa_kontrak_4.Visible = True
        txt_keterangan_4.Text = ""
    End Sub
    Private Sub awal_rb_kontrak_5()
        txt_id_grey_5.Text = ""
        txt_no_kontrak_5.Text = ""
        txt_jenis_kain_5.Text = ""
        txt_jenis_kain_5.ReadOnly = True
        txt_supplier_5.Text = ""
        txt_supplier_5.ReadOnly = True
        txt_harga_5.Text = ""
        txt_qty_5.Text = ""
        txt_total_harga_5.Text = ""
        txt_sisa_kontrak_5.Text = ""
        txt_sisa_kontrak_5.Visible = True
        txt_keterangan_5.Text = ""
    End Sub
    Private Sub awal_rb_kontrak_6()
        txt_id_grey_6.Text = ""
        txt_no_kontrak_6.Text = ""
        txt_jenis_kain_6.Text = ""
        txt_jenis_kain_6.ReadOnly = True
        txt_supplier_6.Text = ""
        txt_supplier_6.ReadOnly = True
        txt_harga_6.Text = ""
        txt_qty_6.Text = ""
        txt_total_harga_6.Text = ""
        txt_sisa_kontrak_6.Text = ""
        txt_sisa_kontrak_6.Visible = True
        txt_keterangan_6.Text = ""
    End Sub
    Private Sub awal_rb_kontrak_7()
        txt_id_grey_7.Text = ""
        txt_no_kontrak_7.Text = ""
        txt_jenis_kain_7.Text = ""
        txt_jenis_kain_7.ReadOnly = True
        txt_supplier_7.Text = ""
        txt_supplier_7.ReadOnly = True
        txt_harga_7.Text = ""
        txt_qty_7.Text = ""
        txt_total_harga_7.Text = ""
        txt_sisa_kontrak_7.Text = ""
        txt_sisa_kontrak_7.Visible = True
        txt_keterangan_7.Text = ""
    End Sub
    Private Sub awal_rb_kontrak_8()
        txt_id_grey_8.Text = ""
        txt_no_kontrak_8.Text = ""
        txt_jenis_kain_8.Text = ""
        txt_jenis_kain_8.ReadOnly = True
        txt_supplier_8.Text = ""
        txt_supplier_8.ReadOnly = True
        txt_harga_8.Text = ""
        txt_qty_8.Text = ""
        txt_total_harga_8.Text = ""
        txt_sisa_kontrak_8.Text = ""
        txt_sisa_kontrak_8.Visible = True
        txt_keterangan_8.Text = ""
    End Sub

    Private Sub awal_rb_non_kontrak_1()
        Call isikodegreybaru_1()
        txt_no_kontrak_1.Text = "-"
        txt_jenis_kain_1.Text = ""
        txt_jenis_kain_1.ReadOnly = True
        txt_supplier_1.Text = ""
        txt_supplier_1.ReadOnly = True
        txt_harga_1.Text = ""
        txt_qty_1.Text = ""
        txt_total_harga_1.Text = ""
        txt_sisa_kontrak_1.Text = ""
        txt_sisa_kontrak_1.Visible = False
        txt_gudang_1.Text = ""
        txt_keterangan_1.Text = ""
    End Sub
    Private Sub awal_rb_non_kontrak_2()
        Call isikodegreybaru_2()
        txt_no_kontrak_2.Text = "-"
        txt_jenis_kain_2.Text = ""
        txt_jenis_kain_2.ReadOnly = True
        txt_supplier_2.Text = ""
        txt_supplier_2.ReadOnly = True
        txt_harga_2.Text = ""
        txt_qty_2.Text = ""
        txt_total_harga_2.Text = ""
        txt_sisa_kontrak_2.Text = ""
        txt_sisa_kontrak_2.Visible = False
        txt_keterangan_2.Text = ""
    End Sub
    Private Sub awal_rb_non_kontrak_3()
        Call isikodegreybaru_3()
        txt_no_kontrak_3.Text = "-"
        txt_jenis_kain_3.Text = ""
        txt_jenis_kain_3.ReadOnly = True
        txt_supplier_3.Text = ""
        txt_supplier_3.ReadOnly = True
        txt_harga_3.Text = ""
        txt_qty_3.Text = ""
        txt_total_harga_3.Text = ""
        txt_sisa_kontrak_3.Text = ""
        txt_sisa_kontrak_3.Visible = False
        txt_keterangan_3.Text = ""
    End Sub
    Private Sub awal_rb_non_kontrak_4()
        Call isikodegreybaru_4()
        txt_no_kontrak_4.Text = "-"
        txt_jenis_kain_4.Text = ""
        txt_jenis_kain_4.ReadOnly = True
        txt_supplier_4.Text = ""
        txt_supplier_4.ReadOnly = True
        txt_harga_4.Text = ""
        txt_qty_4.Text = ""
        txt_total_harga_4.Text = ""
        txt_sisa_kontrak_4.Text = ""
        txt_sisa_kontrak_4.Visible = False
        txt_keterangan_4.Text = ""
    End Sub
    Private Sub awal_rb_non_kontrak_5()
        Call isikodegreybaru_5()
        txt_no_kontrak_5.Text = "-"
        txt_jenis_kain_5.Text = ""
        txt_jenis_kain_5.ReadOnly = True
        txt_supplier_5.Text = ""
        txt_supplier_5.ReadOnly = True
        txt_harga_5.Text = ""
        txt_qty_5.Text = ""
        txt_total_harga_5.Text = ""
        txt_sisa_kontrak_5.Text = ""
        txt_sisa_kontrak_5.Visible = False
        txt_keterangan_5.Text = ""
    End Sub
    Private Sub awal_rb_non_kontrak_6()
        Call isikodegreybaru_6()
        txt_no_kontrak_6.Text = "-"
        txt_jenis_kain_6.Text = ""
        txt_jenis_kain_6.ReadOnly = True
        txt_supplier_6.Text = ""
        txt_supplier_6.ReadOnly = True
        txt_harga_6.Text = ""
        txt_qty_6.Text = ""
        txt_total_harga_6.Text = ""
        txt_sisa_kontrak_6.Text = ""
        txt_sisa_kontrak_6.Visible = False
        txt_keterangan_6.Text = ""
    End Sub
    Private Sub awal_rb_non_kontrak_7()
        Call isikodegreybaru_7()
        txt_no_kontrak_7.Text = "-"
        txt_jenis_kain_7.Text = ""
        txt_jenis_kain_7.ReadOnly = True
        txt_supplier_7.Text = ""
        txt_supplier_7.ReadOnly = True
        txt_harga_7.Text = ""
        txt_qty_7.Text = ""
        txt_total_harga_7.Text = ""
        txt_sisa_kontrak_7.Text = ""
        txt_sisa_kontrak_7.Visible = False
        txt_keterangan_7.Text = ""
    End Sub
    Private Sub awal_rb_non_kontrak_8()
        Call isikodegreybaru_8()
        txt_no_kontrak_8.Text = "-"
        txt_jenis_kain_8.Text = ""
        txt_jenis_kain_8.ReadOnly = True
        txt_supplier_8.Text = ""
        txt_supplier_8.ReadOnly = True
        txt_harga_8.Text = ""
        txt_qty_8.Text = ""
        txt_total_harga_8.Text = ""
        txt_sisa_kontrak_8.Text = ""
        txt_sisa_kontrak_8.Visible = False
        txt_keterangan_8.Text = ""
    End Sub

    Private Sub rb_kontrak_1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_1.CheckedChanged
        If rb_non_kontrak_1.Checked = True Then
            Call awal_rb_non_kontrak_1()
        End If
    End Sub
    Private Sub rb_kontrak_2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_2.CheckedChanged
        If rb_non_kontrak_2.Checked = True Then
            Call awal_rb_non_kontrak_2()
        End If
    End Sub
    Private Sub rb_kontrak_3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_3.CheckedChanged
        If rb_non_kontrak_3.Checked = True Then
            Call awal_rb_non_kontrak_3()
        End If
    End Sub
    Private Sub rb_kontrak_4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_4.CheckedChanged
        If rb_non_kontrak_4.Checked = True Then
            Call awal_rb_non_kontrak_4()
        End If
    End Sub
    Private Sub rb_kontrak_5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_5.CheckedChanged
        If rb_non_kontrak_5.Checked = True Then
            Call awal_rb_non_kontrak_5()
        End If
    End Sub
    Private Sub rb_kontrak_6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_6.CheckedChanged
        If rb_non_kontrak_6.Checked = True Then
            Call awal_rb_non_kontrak_6()
        End If
    End Sub
    Private Sub rb_kontrak_7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_7.CheckedChanged
        If rb_non_kontrak_7.Checked = True Then
            Call awal_rb_non_kontrak_7()
        End If
    End Sub
    Private Sub rb_kontrak_8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_kontrak_8.CheckedChanged
        If rb_non_kontrak_8.Checked = True Then
            Call awal_rb_non_kontrak_8()
        End If
    End Sub

    Private Sub rb_non_kontrak_1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_1.CheckedChanged
        If rb_kontrak_1.Checked = True Then
            Call awal_rb_kontrak_1()
        End If
    End Sub
    Private Sub rb_non_kontrak_2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_2.CheckedChanged
        If rb_kontrak_2.Checked = True Then
            Call awal_rb_kontrak_2()
        End If
    End Sub
    Private Sub rb_non_kontrak_3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_3.CheckedChanged
        If rb_kontrak_3.Checked = True Then
            Call awal_rb_kontrak_3()
        End If
    End Sub
    Private Sub rb_non_kontrak_4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_4.CheckedChanged
        If rb_kontrak_4.Checked = True Then
            Call awal_rb_kontrak_4()
        End If
    End Sub
    Private Sub rb_non_kontrak_5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_5.CheckedChanged
        If rb_kontrak_5.Checked = True Then
            Call awal_rb_kontrak_5()
        End If
    End Sub
    Private Sub rb_non_kontrak_6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_6.CheckedChanged
        If rb_kontrak_6.Checked = True Then
            Call awal_rb_kontrak_6()
        End If
    End Sub
    Private Sub rb_non_kontrak_7_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_7.CheckedChanged
        If rb_kontrak_7.Checked = True Then
            Call awal_rb_kontrak_7()
        End If
    End Sub
    Private Sub rb_non_kontrak_8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rb_non_kontrak_8.CheckedChanged
        If rb_kontrak_8.Checked = True Then
            Call awal_rb_kontrak_8()
        End If
    End Sub

    Private Sub cb_satuan_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_1.TextChanged
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_1 As String = txt_sisa_kontrak_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1, s_1, h_1 As Double
        If txt_no_kontrak_1.Text = "" Or txt_no_kontrak_1.Text = "-" Then
        Else
            If rb_kontrak_1.Checked = True Then
                If cb_satuan_1.Text = "Yard" Then
                    s_1 = Math.Round(Val(sisa_kontrak_1.Replace(",", ".")) * 1.0936, 0)
                    q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                    h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_1.Text = "Meter" Then
                    s_1 = Math.Round(Val(sisa_kontrak_1.Replace(",", ".")) * 0.9144, 0)
                    q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                    h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_1.Text = h_1
                txt_sisa_kontrak_1.Text = s_1
                txt_qty_1.Text = q_1
                txt_harga_1.Focus()
            End If
        End If
    End Sub
    Private Sub cb_satuan_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_2.TextChanged
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_2 As String = txt_sisa_kontrak_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2, s_2, h_2 As Double
        If txt_no_kontrak_2.Text = "" Or txt_no_kontrak_2.Text = "-" Then
        Else
            If rb_kontrak_2.Checked = True Then
                If cb_satuan_2.Text = "Yard" Then
                    s_2 = Math.Round(Val(sisa_kontrak_2.Replace(",", ".")) * 1.0936, 0)
                    q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                    h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_2.Text = "Meter" Then
                    s_2 = Math.Round(Val(sisa_kontrak_2.Replace(",", ".")) * 0.9144, 0)
                    q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                    h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_2.Text = h_2
                txt_sisa_kontrak_2.Text = s_2
                txt_qty_2.Text = q_2
                txt_harga_2.Focus()
            End If
        End If
    End Sub
    Private Sub cb_satuan_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_3.TextChanged
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_3 As String = txt_sisa_kontrak_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3, s_3, h_3 As Double
        If txt_no_kontrak_3.Text = "" Or txt_no_kontrak_3.Text = "-" Then
        Else
            If rb_kontrak_3.Checked = True Then
                If cb_satuan_3.Text = "Yard" Then
                    s_3 = Math.Round(Val(sisa_kontrak_3.Replace(",", ".")) * 1.0936, 0)
                    q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                    h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_3.Text = "Meter" Then
                    s_3 = Math.Round(Val(sisa_kontrak_3.Replace(",", ".")) * 0.9144, 0)
                    q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                    h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_3.Text = h_3
                txt_sisa_kontrak_3.Text = s_3
                txt_qty_3.Text = q_3
                txt_harga_3.Focus()
            End If
        End If
    End Sub
    Private Sub cb_satuan_4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_4.TextChanged
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_4 As String = txt_sisa_kontrak_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4, s_4, h_4 As Double
        If txt_no_kontrak_4.Text = "" Or txt_no_kontrak_4.Text = "-" Then
        Else
            If rb_kontrak_4.Checked = True Then
                If cb_satuan_4.Text = "Yard" Then
                    s_4 = Math.Round(Val(sisa_kontrak_4.Replace(",", ".")) * 1.0936, 0)
                    q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                    h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_4.Text = "Meter" Then
                    s_4 = Math.Round(Val(sisa_kontrak_4.Replace(",", ".")) * 0.9144, 0)
                    q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                    h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_4.Text = h_4
                txt_sisa_kontrak_4.Text = s_4
                txt_qty_4.Text = q_4
                txt_harga_4.Focus()
            End If
        End If
    End Sub
    Private Sub cb_satuan_5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_5.TextChanged
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_5 As String = txt_sisa_kontrak_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5, s_5, h_5 As Double
        If txt_no_kontrak_5.Text = "" Or txt_no_kontrak_5.Text = "-" Then
        Else
            If rb_kontrak_5.Checked = True Then
                If cb_satuan_5.Text = "Yard" Then
                    s_5 = Math.Round(Val(sisa_kontrak_5.Replace(",", ".")) * 1.0936, 0)
                    q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                    h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_5.Text = "Meter" Then
                    s_5 = Math.Round(Val(sisa_kontrak_5.Replace(",", ".")) * 0.9144, 0)
                    q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                    h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_5.Text = h_5
                txt_sisa_kontrak_5.Text = s_5
                txt_qty_5.Text = q_5
                txt_harga_5.Focus()
            End If
        End If
    End Sub
    Private Sub cb_satuan_6_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_6.TextChanged
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_6 As String = txt_sisa_kontrak_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6, s_6, h_6 As Double
        If txt_no_kontrak_6.Text = "" Or txt_no_kontrak_6.Text = "-" Then
        Else
            If rb_kontrak_6.Checked = True Then
                If cb_satuan_6.Text = "Yard" Then
                    s_6 = Math.Round(Val(sisa_kontrak_6.Replace(",", ".")) * 1.0936, 0)
                    q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                    h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_6.Text = "Meter" Then
                    s_6 = Math.Round(Val(sisa_kontrak_6.Replace(",", ".")) * 0.9144, 0)
                    q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                    h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_6.Text = h_6
                txt_sisa_kontrak_6.Text = s_6
                txt_qty_6.Text = q_6
                txt_harga_6.Focus()
            End If
        End If
    End Sub
    Private Sub cb_satuan_7_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_7.TextChanged
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_7 As String = txt_sisa_kontrak_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7, s_7, h_7 As Double
        If txt_no_kontrak_7.Text = "" Or txt_no_kontrak_7.Text = "-" Then
        Else
            If rb_kontrak_7.Checked = True Then
                If cb_satuan_7.Text = "Yard" Then
                    s_7 = Math.Round(Val(sisa_kontrak_7.Replace(",", ".")) * 1.0936, 0)
                    q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                    h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_7.Text = "Meter" Then
                    s_7 = Math.Round(Val(sisa_kontrak_7.Replace(",", ".")) * 0.9144, 0)
                    q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                    h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_7.Text = h_7
                txt_sisa_kontrak_7.Text = s_7
                txt_qty_7.Text = q_7
                txt_harga_7.Focus()
            End If
        End If
    End Sub
    Private Sub cb_satuan_8_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_8.TextChanged
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_kontrak_8 As String = txt_sisa_kontrak_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8, s_8, h_8 As Double
        If txt_no_kontrak_8.Text = "" Or txt_no_kontrak_8.Text = "-" Then
        Else
            If rb_kontrak_8.Checked = True Then
                If cb_satuan_8.Text = "Yard" Then
                    s_8 = Math.Round(Val(sisa_kontrak_8.Replace(",", ".")) * 1.0936, 0)
                    q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                    h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 1.0936, 0)
                ElseIf cb_satuan_8.Text = "Meter" Then
                    s_8 = Math.Round(Val(sisa_kontrak_8.Replace(",", ".")) * 0.9144, 0)
                    q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                    h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 0.9144, 0)
                End If
                txt_harga_8.Text = h_8
                txt_sisa_kontrak_8.Text = s_8
                txt_qty_8.Text = q_8
                txt_harga_8.Focus()
            End If
        End If
    End Sub

    Private Sub btn_hapus_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim g As String = txt_gudang_1.Text
        If rb_non_kontrak_1.Checked = True Then
            rb_kontrak_1.Checked = True
            txt_gudang_1.Text = g
        Else
            Call awal_rb_kontrak_1()
            txt_gudang_1.Text = g
        End If
    End Sub
    Private Sub btn_hapus_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_2.Click
        Dim g As String = txt_gudang_2.Text
        If rb_non_kontrak_2.Checked = True Then
            rb_kontrak_2.Checked = True
            txt_gudang_2.Text = g
        Else
            Call awal_rb_kontrak_2()
            txt_gudang_2.Text = g
        End If
    End Sub
    Private Sub btn_hapus_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_3.Click
        Dim g As String = txt_gudang_3.Text
        If rb_non_kontrak_3.Checked = True Then
            rb_kontrak_3.Checked = True
            txt_gudang_3.Text = g
        Else
            Call awal_rb_kontrak_3()
            txt_gudang_3.Text = g
        End If
    End Sub
    Private Sub btn_hapus_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_4.Click
        Dim g As String = txt_gudang_4.Text
        If rb_non_kontrak_4.Checked = True Then
            rb_kontrak_4.Checked = True
            txt_gudang_4.Text = g
        Else
            Call awal_rb_kontrak_4()
            txt_gudang_4.Text = g
        End If
    End Sub
    Private Sub btn_hapus_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_5.Click
        Dim g As String = txt_gudang_5.Text
        If rb_non_kontrak_5.Checked = True Then
            rb_kontrak_5.Checked = True
            txt_gudang_5.Text = g
        Else
            Call awal_rb_kontrak_5()
            txt_gudang_5.Text = g
        End If
    End Sub
    Private Sub btn_hapus_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_6.Click
        Dim g As String = txt_gudang_6.Text
        If rb_non_kontrak_6.Checked = True Then
            rb_kontrak_6.Checked = True
            txt_gudang_6.Text = g
        Else
            Call awal_rb_kontrak_6()
            txt_gudang_6.Text = g
        End If
    End Sub
    Private Sub btn_hapus_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_7.Click
        Dim g As String = txt_gudang_7.Text
        If rb_non_kontrak_7.Checked = True Then
            rb_kontrak_7.Checked = True
            txt_gudang_7.Text = g
        Else
            Call awal_rb_kontrak_7()
            txt_gudang_7.Text = g
        End If
    End Sub
    Private Sub btn_hapus_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_8.Click
        Dim g As String = txt_gudang_8.Text
        If rb_non_kontrak_8.Checked = True Then
            rb_kontrak_8.Checked = True
            txt_gudang_8.Text = g
        Else
            Call awal_rb_kontrak_8()
            txt_gudang_8.Text = g
        End If
    End Sub

    Private Sub btn_tambah_baris_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_1.Click
        Try
            Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_1 As String = txt_sisa_kontrak_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_surat_jalan.Text = "" Then
                MsgBox("No SURAT JALAN Belum DIINPUT")
                txt_surat_jalan.Focus()
            ElseIf txt_id_grey_1.Text = "" Then
                MsgBox("BARIS 1 Belum Diinput Data")
                txt_jenis_kain_1.Focus()
            ElseIf txt_jenis_kain_1.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_1.Focus()
            ElseIf txt_supplier_1.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_1.Focus()
            ElseIf txt_harga_1.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_1.Focus()
            ElseIf txt_qty_1.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_1.Focus()
            ElseIf Val(qty_1.Replace(",", ".")) > Val(sisa_kontrak_1.Replace(",", ".")) And txt_sisa_kontrak_1.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_1.Focus()
            ElseIf txt_gudang_1.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_1.Focus()
            Else
                dtp_awal.Enabled = False
                ComboBox1.Enabled = False
                txt_surat_jalan.Enabled = False
                btn_tambah_baris_1.Visible = False
                btn_selesai_1.Visible = False
                Panel_rb_1.Enabled = False
                Panel_ubah_1.Enabled = False
                Panel_2.Visible = True
                btn_hapus_2.Visible = True
                btn_batal_2.Visible = True
                btn_tambah_baris_2.Visible = True
                btn_selesai_2.Visible = True
                txt_gudang_2.Text = txt_gudang_1.Text
                Call simpan_tbpembeliangrey_1()
                Call simpan_tbstokprosesgrey_1()
                Call simpan_tbdetilhutang_1()
                If rb_kontrak_1.Checked = True Then
                    Call update_tbkontrak_grey_1()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_2.Click
        Try
            Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_2 As String = txt_sisa_kontrak_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_2.Text = "" Then
                MsgBox("BARIS 2 Belum Diinput Data")
                txt_jenis_kain_2.Focus()
            ElseIf txt_jenis_kain_2.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_2.Focus()
            ElseIf txt_supplier_2.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_2.Focus()
            ElseIf txt_harga_2.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_2.Focus()
            ElseIf txt_qty_2.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_2.Focus()
            ElseIf Val(qty_2.Replace(",", ".")) > Val(sisa_kontrak_2.Replace(",", ".")) And txt_sisa_kontrak_2.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_2.Focus()
            ElseIf txt_gudang_2.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_2.Focus()
            Else
                dtp_awal.Enabled = False
                ComboBox1.Enabled = False
                txt_surat_jalan.Enabled = False
                btn_tambah_baris_2.Visible = False
                btn_selesai_2.Visible = False
                Panel_rb_2.Enabled = False
                Panel_ubah_2.Enabled = False
                Panel_3.Visible = True
                btn_hapus_3.Visible = True
                btn_batal_3.Visible = True
                btn_tambah_baris_3.Visible = True
                btn_selesai_3.Visible = True
                txt_gudang_3.Text = txt_gudang_2.Text
                Call simpan_tbpembeliangrey_2()
                Call simpan_tbstokprosesgrey_2()
                Call simpan_tbdetilhutang_2()
                If rb_kontrak_2.Checked = True Then
                    Call update_tbkontrak_grey_2()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_3.Click
        Try
            Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_3 As String = txt_sisa_kontrak_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_3.Text = "" Then
                MsgBox("BARIS 3 Belum Diinput Data")
                txt_jenis_kain_3.Focus()
            ElseIf txt_jenis_kain_3.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_3.Focus()
            ElseIf txt_supplier_3.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_3.Focus()
            ElseIf txt_harga_3.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_3.Focus()
            ElseIf txt_qty_3.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_3.Focus()
            ElseIf Val(qty_3.Replace(",", ".")) > Val(sisa_kontrak_3.Replace(",", ".")) And txt_sisa_kontrak_3.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_3.Focus()
            ElseIf txt_gudang_3.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_3.Focus()
            Else
                dtp_awal.Enabled = False
                ComboBox1.Enabled = False
                txt_surat_jalan.Enabled = False
                btn_tambah_baris_3.Visible = False
                btn_selesai_3.Visible = False
                Panel_rb_3.Enabled = False
                Panel_ubah_3.Enabled = False
                Panel_4.Visible = True
                btn_hapus_4.Visible = True
                btn_batal_4.Visible = True
                btn_tambah_baris_4.Visible = True
                btn_selesai_4.Visible = True
                txt_gudang_4.Text = txt_gudang_3.Text
                Call simpan_tbpembeliangrey_3()
                Call simpan_tbstokprosesgrey_3()
                Call simpan_tbdetilhutang_3()
                If rb_kontrak_3.Checked = True Then
                    Call update_tbkontrak_grey_3()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_4.Click
        Try
            Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_4 As String = txt_sisa_kontrak_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_4.Text = "" Then
                MsgBox("BARIS 4 Belum Diinput Data")
                txt_jenis_kain_4.Focus()
            ElseIf txt_jenis_kain_4.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_4.Focus()
            ElseIf txt_supplier_4.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_4.Focus()
            ElseIf txt_harga_4.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_4.Focus()
            ElseIf txt_qty_4.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_4.Focus()
            ElseIf Val(qty_4.Replace(",", ".")) > Val(sisa_kontrak_4.Replace(",", ".")) And txt_sisa_kontrak_4.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_4.Focus()
            ElseIf txt_gudang_4.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_4.Focus()
            Else
                dtp_awal.Enabled = False
                ComboBox1.Enabled = False
                txt_surat_jalan.Enabled = False
                btn_tambah_baris_4.Visible = False
                btn_selesai_4.Visible = False
                Panel_rb_4.Enabled = False
                Panel_ubah_4.Enabled = False
                Panel_5.Visible = True
                btn_hapus_5.Visible = True
                btn_batal_5.Visible = True
                btn_tambah_baris_5.Visible = True
                btn_selesai_5.Visible = True
                txt_gudang_5.Text = txt_gudang_4.Text
                Call simpan_tbpembeliangrey_4()
                Call simpan_tbstokprosesgrey_4()
                Call simpan_tbdetilhutang_4()
                If rb_kontrak_4.Checked = True Then
                    Call update_tbkontrak_grey_4()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_5.Click
        Try
            Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_5 As String = txt_sisa_kontrak_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_5.Text = "" Then
                MsgBox("BARIS 5 Belum Diinput Data")
                txt_jenis_kain_5.Focus()
            ElseIf txt_jenis_kain_5.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_5.Focus()
            ElseIf txt_supplier_5.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_5.Focus()
            ElseIf txt_harga_5.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_5.Focus()
            ElseIf txt_qty_5.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_5.Focus()
            ElseIf Val(qty_5.Replace(",", ".")) > Val(sisa_kontrak_5.Replace(",", ".")) And txt_sisa_kontrak_5.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_5.Focus()
            ElseIf txt_gudang_5.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_5.Focus()
            Else
                dtp_awal.Enabled = False
                ComboBox1.Enabled = False
                txt_surat_jalan.Enabled = False
                btn_tambah_baris_5.Visible = False
                btn_selesai_5.Visible = False
                Panel_rb_5.Enabled = False
                Panel_ubah_5.Enabled = False
                Panel_6.Visible = True
                btn_hapus_6.Visible = True
                btn_batal_6.Visible = True
                btn_tambah_baris_6.Visible = True
                btn_selesai_6.Visible = True
                txt_gudang_6.Text = txt_gudang_5.Text
                Call simpan_tbpembeliangrey_5()
                Call simpan_tbstokprosesgrey_5()
                Call simpan_tbdetilhutang_5()
                If rb_kontrak_5.Checked = True Then
                    Call update_tbkontrak_grey_5()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_6.Click
        Try
            Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_6 As String = txt_sisa_kontrak_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_6.Text = "" Then
                MsgBox("BARIS 6 Belum Diinput Data")
                txt_jenis_kain_6.Focus()
            ElseIf txt_jenis_kain_6.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_6.Focus()
            ElseIf txt_supplier_6.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_6.Focus()
            ElseIf txt_harga_6.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_6.Focus()
            ElseIf txt_qty_6.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_6.Focus()
            ElseIf Val(qty_6.Replace(",", ".")) > Val(sisa_kontrak_6.Replace(",", ".")) And txt_sisa_kontrak_6.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_6.Focus()
            ElseIf txt_gudang_6.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_6.Focus()
            Else
                dtp_awal.Enabled = False
                ComboBox1.Enabled = False
                txt_surat_jalan.Enabled = False
                btn_tambah_baris_6.Visible = False
                btn_selesai_6.Visible = False
                Panel_rb_6.Enabled = False
                Panel_ubah_6.Enabled = False
                Panel_7.Visible = True
                btn_hapus_7.Visible = True
                btn_batal_7.Visible = True
                btn_tambah_baris_7.Visible = True
                btn_selesai_7.Visible = True
                txt_gudang_7.Text = txt_gudang_6.Text
                Call simpan_tbpembeliangrey_6()
                Call simpan_tbstokprosesgrey_6()
                Call simpan_tbdetilhutang_6()
                If rb_kontrak_6.Checked = True Then
                    Call update_tbkontrak_grey_6()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_7.Click
        Try
            Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_7 As String = txt_sisa_kontrak_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_7.Text = "" Then
                MsgBox("BARIS 7 Belum Diinput Data")
                txt_jenis_kain_7.Focus()
            ElseIf txt_jenis_kain_7.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_7.Focus()
            ElseIf txt_supplier_7.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_7.Focus()
            ElseIf txt_harga_7.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_7.Focus()
            ElseIf txt_qty_7.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_7.Focus()
            ElseIf Val(qty_7.Replace(",", ".")) > Val(sisa_kontrak_7.Replace(",", ".")) And txt_sisa_kontrak_7.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_7.Focus()
            ElseIf txt_gudang_7.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_7.Focus()
            Else
                dtp_awal.Enabled = False
                ComboBox1.Enabled = False
                txt_surat_jalan.Enabled = False
                btn_tambah_baris_7.Visible = False
                btn_selesai_7.Visible = False
                Panel_rb_7.Enabled = False
                Panel_ubah_7.Enabled = False
                Panel_8.Visible = True
                btn_hapus_8.Visible = True
                btn_batal_8.Visible = True
                btn_selesai_8.Visible = True
                txt_gudang_8.Text = txt_gudang_7.Text
                Call simpan_tbpembeliangrey_7()
                Call simpan_tbstokprosesgrey_7()
                Call simpan_tbdetilhutang_7()
                If rb_kontrak_7.Checked = True Then
                    Call update_tbkontrak_grey_7()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_selesai_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_1.Click
        Try
            Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_1 As String = txt_sisa_kontrak_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_surat_jalan.Text = "" Then
                MsgBox("No SURAT JALAN Belum DIINPUT")
                txt_surat_jalan.Focus()
            ElseIf txt_id_grey_1.Text = "" Then
                MsgBox("BARIS 1 Belum Diinput Data")
                txt_jenis_kain_1.Focus()
            ElseIf txt_jenis_kain_1.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
                txt_jenis_kain_1.Focus()
            ElseIf txt_supplier_1.Text = "" Then
                MsgBox("SUPPLIER Belum Dipilih")
                txt_supplier_1.Focus()
            ElseIf txt_harga_1.Text = "" Then
                MsgBox("HARGA Barang Belum Diinput")
                txt_harga_1.Focus()
            ElseIf txt_qty_1.Text = "" Then
                MsgBox("QUANTITY Barang Belum Diinput")
                txt_qty_1.Focus()
            ElseIf Val(qty_1.Replace(",", ".")) > Val(sisa_kontrak_1.Replace(",", ".")) And txt_sisa_kontrak_1.Visible = True Then
                MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                txt_qty_1.Focus()
            ElseIf txt_gudang_1.Text = "" Then
                MsgBox("GUDANG Belum Dipilih")
                txt_gudang_1.Focus()
            Else
                Call simpan_tbpembeliangrey_1()
                Call simpan_tbstokprosesgrey_1()
                Call simpan_tbdetilhutang_1()
                If rb_kontrak_1.Checked = True Then
                    Call update_tbkontrak_grey_1()
                End If
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Show()
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_2.Click
        Try
            Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_2 As String = txt_sisa_kontrak_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_2.Text = "" Then
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_jenis_kain_2.Text = "" Then
                    MsgBox("JENIS KAIN Belum Dipilih")
                    txt_jenis_kain_2.Focus()
                ElseIf txt_supplier_2.Text = "" Then
                    MsgBox("SUPPLIER Belum Dipilih")
                    txt_supplier_2.Focus()
                ElseIf txt_harga_2.Text = "" Then
                    MsgBox("HARGA Barang Belum Diinput")
                    txt_harga_2.Focus()
                ElseIf txt_qty_2.Text = "" Then
                    MsgBox("QUANTITY Barang Belum Diinput")
                    txt_qty_2.Focus()
                ElseIf Val(qty_2.Replace(",", ".")) > Val(sisa_kontrak_2.Replace(",", ".")) And txt_sisa_kontrak_2.Visible = True Then
                    MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                    txt_qty_2.Focus()
                ElseIf txt_gudang_2.Text = "" Then
                    MsgBox("GUDANG Belum Dipilih")
                    txt_gudang_2.Focus()
                Else
                    Call simpan_tbpembeliangrey_2()
                    Call simpan_tbstokprosesgrey_2()
                    Call simpan_tbdetilhutang_2()
                    If rb_kontrak_2.Checked = True Then
                        Call update_tbkontrak_grey_2()
                    End If
                    MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_3.Click
        Try
            Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_3 As String = txt_sisa_kontrak_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_3.Text = "" Then
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_jenis_kain_3.Text = "" Then
                    MsgBox("JENIS KAIN Belum Dipilih")
                    txt_jenis_kain_3.Focus()
                ElseIf txt_supplier_3.Text = "" Then
                    MsgBox("SUPPLIER Belum Dipilih")
                    txt_supplier_3.Focus()
                ElseIf txt_harga_3.Text = "" Then
                    MsgBox("HARGA Barang Belum Diinput")
                    txt_harga_3.Focus()
                ElseIf txt_qty_3.Text = "" Then
                    MsgBox("QUANTITY Barang Belum Diinput")
                    txt_qty_3.Focus()
                ElseIf Val(qty_3.Replace(",", ".")) > Val(sisa_kontrak_3.Replace(",", ".")) And txt_sisa_kontrak_3.Visible = True Then
                    MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                    txt_qty_3.Focus()
                ElseIf txt_gudang_3.Text = "" Then
                    MsgBox("GUDANG Belum Dipilih")
                    txt_gudang_3.Focus()
                Else
                    Call simpan_tbpembeliangrey_3()
                    Call simpan_tbstokprosesgrey_3()
                    Call simpan_tbdetilhutang_3()
                    If rb_kontrak_3.Checked = True Then
                        Call update_tbkontrak_grey_3()
                    End If
                    MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_4.Click
        Try
            Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_4 As String = txt_sisa_kontrak_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_4.Text = "" Then
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_jenis_kain_4.Text = "" Then
                    MsgBox("JENIS KAIN Belum Dipilih")
                    txt_jenis_kain_4.Focus()
                ElseIf txt_supplier_4.Text = "" Then
                    MsgBox("SUPPLIER Belum Dipilih")
                    txt_supplier_4.Focus()
                ElseIf txt_harga_4.Text = "" Then
                    MsgBox("HARGA Barang Belum Diinput")
                    txt_harga_4.Focus()
                ElseIf txt_qty_4.Text = "" Then
                    MsgBox("QUANTITY Barang Belum Diinput")
                    txt_qty_4.Focus()
                ElseIf Val(qty_4.Replace(",", ".")) > Val(sisa_kontrak_4.Replace(",", ".")) And txt_sisa_kontrak_4.Visible = True Then
                    MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                    txt_qty_4.Focus()
                ElseIf txt_gudang_4.Text = "" Then
                    MsgBox("GUDANG Belum Dipilih")
                    txt_gudang_4.Focus()
                Else
                    Call simpan_tbpembeliangrey_4()
                    Call simpan_tbstokprosesgrey_4()
                    Call simpan_tbdetilhutang_4()
                    If rb_kontrak_4.Checked = True Then
                        Call update_tbkontrak_grey_4()
                    End If
                    MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_5.Click
        Try
            Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_5 As String = txt_sisa_kontrak_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_5.Text = "" Then
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_jenis_kain_5.Text = "" Then
                    MsgBox("JENIS KAIN Belum Dipilih")
                    txt_jenis_kain_5.Focus()
                ElseIf txt_supplier_5.Text = "" Then
                    MsgBox("SUPPLIER Belum Dipilih")
                    txt_supplier_5.Focus()
                ElseIf txt_harga_5.Text = "" Then
                    MsgBox("HARGA Barang Belum Diinput")
                    txt_harga_5.Focus()
                ElseIf txt_qty_5.Text = "" Then
                    MsgBox("QUANTITY Barang Belum Diinput")
                    txt_qty_5.Focus()
                ElseIf Val(qty_5.Replace(",", ".")) > Val(sisa_kontrak_5.Replace(",", ".")) And txt_sisa_kontrak_5.Visible = True Then
                    MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                    txt_qty_5.Focus()
                ElseIf txt_gudang_5.Text = "" Then
                    MsgBox("GUDANG Belum Dipilih")
                    txt_gudang_5.Focus()
                Else
                    Call simpan_tbpembeliangrey_5()
                    Call simpan_tbstokprosesgrey_5()
                    Call simpan_tbdetilhutang_5()
                    If rb_kontrak_5.Checked = True Then
                        Call update_tbkontrak_grey_5()
                    End If
                    MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_6.Click
        Try
            Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_6 As String = txt_sisa_kontrak_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_6.Text = "" Then
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_jenis_kain_6.Text = "" Then
                    MsgBox("JENIS KAIN Belum Dipilih")
                    txt_jenis_kain_6.Focus()
                ElseIf txt_supplier_6.Text = "" Then
                    MsgBox("SUPPLIER Belum Dipilih")
                    txt_supplier_6.Focus()
                ElseIf txt_harga_6.Text = "" Then
                    MsgBox("HARGA Barang Belum Diinput")
                    txt_harga_6.Focus()
                ElseIf txt_qty_6.Text = "" Then
                    MsgBox("QUANTITY Barang Belum Diinput")
                    txt_qty_6.Focus()
                ElseIf Val(qty_6.Replace(",", ".")) > Val(sisa_kontrak_6.Replace(",", ".")) And txt_sisa_kontrak_6.Visible = True Then
                    MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                    txt_qty_6.Focus()
                ElseIf txt_gudang_6.Text = "" Then
                    MsgBox("GUDANG Belum Dipilih")
                    txt_gudang_6.Focus()
                Else
                    Call simpan_tbpembeliangrey_6()
                    Call simpan_tbstokprosesgrey_6()
                    Call simpan_tbdetilhutang_6()
                    If rb_kontrak_6.Checked = True Then
                        Call update_tbkontrak_grey_6()
                    End If
                    MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_7_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_7.Click
        Try
            Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_7 As String = txt_sisa_kontrak_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_7.Text = "" Then
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_jenis_kain_7.Text = "" Then
                    MsgBox("JENIS KAIN Belum Dipilih")
                    txt_jenis_kain_7.Focus()
                ElseIf txt_supplier_7.Text = "" Then
                    MsgBox("SUPPLIER Belum Dipilih")
                    txt_supplier_7.Focus()
                ElseIf txt_harga_7.Text = "" Then
                    MsgBox("HARGA Barang Belum Diinput")
                    txt_harga_7.Focus()
                ElseIf txt_qty_7.Text = "" Then
                    MsgBox("QUANTITY Barang Belum Diinput")
                    txt_qty_7.Focus()
                ElseIf Val(qty_7.Replace(",", ".")) > Val(sisa_kontrak_7.Replace(",", ".")) And txt_sisa_kontrak_7.Visible = True Then
                    MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                    txt_qty_7.Focus()
                ElseIf txt_gudang_7.Text = "" Then
                    MsgBox("GUDANG Belum Dipilih")
                    txt_gudang_7.Focus()
                Else
                    Call simpan_tbpembeliangrey_7()
                    Call simpan_tbstokprosesgrey_7()
                    Call simpan_tbdetilhutang_7()
                    If rb_kontrak_7.Checked = True Then
                        Call update_tbkontrak_grey_7()
                    End If
                    MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_selesai_8.Click
        Try
            Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            Dim sisa_kontrak_8 As String = txt_sisa_kontrak_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If txt_id_grey_8.Text = "" Then
                MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_jenis_kain_8.Text = "" Then
                    MsgBox("JENIS KAIN Belum Dipilih")
                    txt_jenis_kain_8.Focus()
                ElseIf txt_supplier_8.Text = "" Then
                    MsgBox("SUPPLIER Belum Dipilih")
                    txt_supplier_8.Focus()
                ElseIf txt_harga_8.Text = "" Then
                    MsgBox("HARGA Barang Belum Diinput")
                    txt_harga_8.Focus()
                ElseIf txt_qty_8.Text = "" Then
                    MsgBox("QUANTITY Barang Belum Diinput")
                    txt_qty_8.Focus()
                ElseIf Val(qty_8.Replace(",", ".")) > Val(sisa_kontrak_8.Replace(",", ".")) And txt_sisa_kontrak_8.Visible = True Then
                    MsgBox("QUANTITY Barang Tidak Boleh Melebihi Sisa Kontrak")
                    txt_qty_8.Focus()
                ElseIf txt_gudang_8.Text = "" Then
                    MsgBox("GUDANG Belum Dipilih")
                    txt_gudang_8.Focus()
                Else
                    Call simpan_tbpembeliangrey_8()
                    Call simpan_tbstokprosesgrey_8()
                    Call simpan_tbdetilhutang_8()
                    If rb_kontrak_8.Checked = True Then
                        Call update_tbkontrak_grey_8()
                    End If
                    MsgBox("SURAT JALAN Pembelian Baru Berhasil Disimpan")
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub simpan_tbpembeliangrey_1()
        Call isikodebeli_1()
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_1 As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_1.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_1.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_1.Text)
                    .Parameters.AddWithValue("@8", harga_1.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_1.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_1.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_1.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_1.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpembeliangrey_2()
        Call isikodebeli_2()
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_2 As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_2.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_2.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_2.Text)
                    .Parameters.AddWithValue("@8", harga_2.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_2.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_2.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_2.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_2.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpembeliangrey_3()
        Call isikodebeli_3()
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_3 As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_3.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_3.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_3.Text)
                    .Parameters.AddWithValue("@8", harga_3.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_3.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_3.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_3.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_3.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpembeliangrey_4()
        Call isikodebeli_4()
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_4 As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_4.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_4.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_4.Text)
                    .Parameters.AddWithValue("@8", harga_4.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_4.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_4.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_4.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_4.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpembeliangrey_5()
        Call isikodebeli_5()
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_5 As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_5.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_5.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_5.Text)
                    .Parameters.AddWithValue("@8", harga_5.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_5.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_5.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_5.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_5.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpembeliangrey_6()
        Call isikodebeli_6()
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_6 As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_6.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_6.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_6.Text)
                    .Parameters.AddWithValue("@8", harga_6.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_6.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_6.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_6.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_6.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpembeliangrey_7()
        Call isikodebeli_7()
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_7 As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_7.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_7.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_7.Text)
                    .Parameters.AddWithValue("@8", harga_7.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_7.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_7.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_7.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_7.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpembeliangrey_8()
        Call isikodebeli_8()
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_8 As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpembeliangrey (Id_Beli,Id_Grey,Tanggal,Jatuh_Tempo,No_Kontrak,SJ,Jenis_Kain,Supplier,Harga,QTY,Jumlah,Satuan,Lama_Jt,Gudang,Keterangan,Asal_Supplier,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_beli_8.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@4", txt_no_kontrak_8.Text)
                    .Parameters.AddWithValue("@5", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@7", txt_supplier_8.Text)
                    .Parameters.AddWithValue("@8", harga_8.Replace(",", "."))
                    .Parameters.AddWithValue("@9", qty_8.Replace(",", "."))
                    .Parameters.AddWithValue("@10", jumlah_8.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@12", ComboBox1.Text)
                    .Parameters.AddWithValue("@13", txt_gudang_8.Text)
                    .Parameters.AddWithValue("@14", txt_keterangan_8.Text)
                    .Parameters.AddWithValue("@15", txt_asal_supplier_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "0")
                    .Parameters.AddWithValue("@20", "0")
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub

    Private Sub update_tbkontrak_grey_1()
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_1.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_1.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_1.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_1.Text = "Yard" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_1, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_1, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_1.Text = "Meter" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_1, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_1, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_tbkontrak_grey_2()
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_2.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_2.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_2.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_2.Text = "Yard" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_2, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_2, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_2.Text = "Meter" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_2, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_2, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_tbkontrak_grey_3()
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_3.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_3.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_3.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_3.Text = "Yard" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_3, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_3, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_3.Text = "Meter" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_3, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_3, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_tbkontrak_grey_4()
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_4.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_4.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_4.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_4.Text = "Yard" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_4, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_4, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_4.Text = "Meter" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_4, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_4, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_tbkontrak_grey_5()
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_5.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_5.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_5.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_5.Text = "Yard" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_5, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_5, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_5.Text = "Meter" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_5, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_5, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_tbkontrak_grey_6()
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_6.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_6.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_6.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_6.Text = "Yard" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_6, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_6, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_6.Text = "Meter" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_6, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_6, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_tbkontrak_grey_7()
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_7.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_7.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_7.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_7.Text = "Yard" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_7, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_7, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_7.Text = "Meter" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_7, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_7, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_tbkontrak_grey_8()
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_8.Text Then
                                        .Parameters.AddWithValue("@1", d + Math.Round(Val(qty_8.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(Val(qty_8.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_8.Text = "Yard" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_8, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_8, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_8.Text = "Meter" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d + Math.Round(q_8, 2))
                                        .Parameters.AddWithValue("@2", m - (d + Math.Round(q_8, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub

    Private Sub simpan_tbstokprosesgrey_1()
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_1 As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_1.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_1.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_1.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_1.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_1.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_1.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey_2()
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_2 As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_2.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_2.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_2.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_2.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_2.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_2.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey_3()
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_3 As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_3.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_3.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_3.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_3.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_3.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_3.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey_4()
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_4 As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_4.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_4.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_4.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_4.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_4.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_4.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey_5()
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_5 As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_5.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_5.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_5.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_5.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_5.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_5.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey_6()
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_6 As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_6.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_6.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_6.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_6.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_6.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_6.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey_7()
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_7 As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_7.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_7.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_7.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_7.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_7.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_7.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokprosesgrey_8()
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_8 As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokprosesgrey (Id_Grey,Id_Beli,Tanggal,SJ,Supplier,Gudang,Jenis_Kain,Stok,Satuan,Harga,Total_Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@1", txt_id_beli_8.Text)
                    .Parameters.AddWithValue("@2", dtp_awal.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@4", txt_supplier_8.Text)
                    .Parameters.AddWithValue("@5", txt_gudang_8.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@7", Math.Round(Val(qty_8.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@8", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@9", Math.Round(Val(harga_8.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@10", Math.Round(Val(jumlah_8.Replace(",", ".")), 2))
                    .Parameters.AddWithValue("@11", "")
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub

    Private Sub simpan_tbdetilhutang_1()
        Call isikodehutang_1()
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_1 As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_1.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_1.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_1.Text)
                    .Parameters.AddWithValue("@7", harga_1.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_1.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@10", jumlah_1.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_2()
        Call isikodehutang_2()
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_2 As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_2.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_2.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_2.Text)
                    .Parameters.AddWithValue("@7", harga_2.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_2.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@10", jumlah_2.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_3()
        Call isikodehutang_3()
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_3 As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_3.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_3.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_3.Text)
                    .Parameters.AddWithValue("@7", harga_3.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_3.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@10", jumlah_3.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_4()
        Call isikodehutang_4()
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_4 As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_4.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_4.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_4.Text)
                    .Parameters.AddWithValue("@7", harga_4.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_4.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@10", jumlah_4.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_5()
        Call isikodehutang_5()
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_5 As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_5.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_5.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_5.Text)
                    .Parameters.AddWithValue("@7", harga_5.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_5.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@10", jumlah_5.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_6()
        Call isikodehutang_6()
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_6 As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_6.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_6.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_6.Text)
                    .Parameters.AddWithValue("@7", harga_6.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_6.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@10", jumlah_6.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_7()
        Call isikodehutang_7()
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_7 As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_7.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_7.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_7.Text)
                    .Parameters.AddWithValue("@7", harga_7.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_7.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@10", jumlah_7.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_8()
        Call isikodehutang_8()
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim jumlah_8 As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_8.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_8.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@6", txt_supplier_8.Text)
                    .Parameters.AddWithValue("@7", harga_8.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_8.Replace(",", "."))
                    .Parameters.AddWithValue("@9", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@10", jumlah_8.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Pembelian Grey")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub hapus_tbpembeliangrey_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpembeliangrey_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & txt_id_beli_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_tbstokproses_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & txt_id_beli_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_tbdetil_hutang_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & txt_id_hutang_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_update_tbkontrak_grey_1()
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_1.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_1.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_1.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_1.Text = "Yard" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_1, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_1, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_1.Text = "Meter" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_1, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_1, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_tbkontrak_grey_2()
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_2.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_2.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_2.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_2.Text = "Yard" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_2, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_2, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_2.Text = "Meter" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_2, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_2, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_tbkontrak_grey_3()
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_3.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_3.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_3.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_3.Text = "Yard" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_3, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_3, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_3.Text = "Meter" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_3, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_3, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_tbkontrak_grey_4()
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_4.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_4.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_4.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_4.Text = "Yard" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_4, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_4, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_4.Text = "Meter" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_4, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_4, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_tbkontrak_grey_5()
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_5.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_5.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_5.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_5.Text = "Yard" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_5, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_5, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_5.Text = "Meter" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_5, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_5, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_tbkontrak_grey_6()
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_6.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_6.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_6.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_6.Text = "Yard" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_6, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_6, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_6.Text = "Meter" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_6, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_6, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_tbkontrak_grey_7()
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_7.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_7.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_7.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_7.Text = "Yard" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_7, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_7, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_7.Text = "Meter" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_7, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_7, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_tbkontrak_grey_8()
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & txt_id_grey_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = cb_satuan_8.Text Then
                                        .Parameters.AddWithValue("@1", d - Math.Round(Val(qty_8.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(Val(qty_8.Replace(",", ".")), 2)))
                                    ElseIf drx(2) = "Meter" And cb_satuan_8.Text = "Yard" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_8, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_8, 2)))
                                    ElseIf drx(2) = "Yard" And cb_satuan_8.Text = "Meter" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", d - Math.Round(q_8, 2))
                                        .Parameters.AddWithValue("@2", m - (d - Math.Round(q_8, 2)))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        form_kontrak_grey.ts_perbarui.PerformClick()
    End Sub

    Private Sub btn_batal_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_1.Click
        Try
            form_pembelian_grey.Show()
            form_pembelian_grey.Focus()
            form_pembelian_grey.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_2.Click
        Try
            If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpembeliangrey_1()
                Call hapus_tbstokproses_1()
                Call hapus_tbdetil_hutang_1()
                Call hapus_update_tbkontrak_grey_1()
                form_pembelian_grey.Show()
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_3.Click
        Try
            If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpembeliangrey_1()
                Call hapus_tbstokproses_1()
                Call hapus_tbdetil_hutang_1()
                Call hapus_update_tbkontrak_grey_1()
                Call hapus_tbpembeliangrey_2()
                Call hapus_tbstokproses_2()
                Call hapus_tbdetil_hutang_2()
                Call hapus_update_tbkontrak_grey_2()
                form_pembelian_grey.Show()
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_4.Click
        Try
            If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpembeliangrey_1()
                Call hapus_tbstokproses_1()
                Call hapus_tbdetil_hutang_1()
                Call hapus_update_tbkontrak_grey_1()
                Call hapus_tbpembeliangrey_2()
                Call hapus_tbstokproses_2()
                Call hapus_tbdetil_hutang_2()
                Call hapus_update_tbkontrak_grey_2()
                Call hapus_tbpembeliangrey_3()
                Call hapus_tbstokproses_3()
                Call hapus_tbdetil_hutang_3()
                Call hapus_update_tbkontrak_grey_3()
                form_pembelian_grey.Show()
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_5.Click
        Try
            If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpembeliangrey_1()
                Call hapus_tbstokproses_1()
                Call hapus_tbdetil_hutang_1()
                Call hapus_update_tbkontrak_grey_1()
                Call hapus_tbpembeliangrey_2()
                Call hapus_tbstokproses_2()
                Call hapus_tbdetil_hutang_2()
                Call hapus_update_tbkontrak_grey_2()
                Call hapus_tbpembeliangrey_3()
                Call hapus_tbstokproses_3()
                Call hapus_tbdetil_hutang_3()
                Call hapus_update_tbkontrak_grey_3()
                Call hapus_tbpembeliangrey_4()
                Call hapus_tbstokproses_4()
                Call hapus_tbdetil_hutang_4()
                Call hapus_update_tbkontrak_grey_4()
                form_pembelian_grey.Show()
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_6.Click
        Try
            If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                    Call hapus_tbpembeliangrey_1()
                    Call hapus_tbstokproses_1()
                    Call hapus_tbdetil_hutang_1()
                    Call hapus_update_tbkontrak_grey_1()
                    Call hapus_tbpembeliangrey_2()
                    Call hapus_tbstokproses_2()
                    Call hapus_tbdetil_hutang_2()
                    Call hapus_update_tbkontrak_grey_2()
                    Call hapus_tbpembeliangrey_3()
                    Call hapus_tbstokproses_3()
                    Call hapus_tbdetil_hutang_3()
                    Call hapus_update_tbkontrak_grey_3()
                    Call hapus_tbpembeliangrey_4()
                    Call hapus_tbstokproses_4()
                    Call hapus_tbdetil_hutang_4()
                    Call hapus_update_tbkontrak_grey_4()
                    Call hapus_tbpembeliangrey_5()
                    Call hapus_tbstokproses_5()
                    Call hapus_tbdetil_hutang_5()
                    Call hapus_update_tbkontrak_grey_5()
                    form_pembelian_grey.Show()
                    form_pembelian_grey.Focus()
                    form_pembelian_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_7.Click
        Try
            If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpembeliangrey_1()
                Call hapus_tbstokproses_1()
                Call hapus_tbdetil_hutang_1()
                Call hapus_update_tbkontrak_grey_1()
                Call hapus_tbpembeliangrey_2()
                Call hapus_tbstokproses_2()
                Call hapus_tbdetil_hutang_2()
                Call hapus_update_tbkontrak_grey_2()
                Call hapus_tbpembeliangrey_3()
                Call hapus_tbstokproses_3()
                Call hapus_tbdetil_hutang_3()
                Call hapus_update_tbkontrak_grey_3()
                Call hapus_tbpembeliangrey_4()
                Call hapus_tbstokproses_4()
                Call hapus_tbdetil_hutang_4()
                Call hapus_update_tbkontrak_grey_4()
                Call hapus_tbpembeliangrey_5()
                Call hapus_tbstokproses_5()
                Call hapus_tbdetil_hutang_5()
                Call hapus_update_tbkontrak_grey_5()
                Call hapus_tbpembeliangrey_6()
                Call hapus_tbstokproses_6()
                Call hapus_tbdetil_hutang_6()
                Call hapus_update_tbkontrak_grey_6()
                form_pembelian_grey.Show()
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_8.Click
        Try
            If MsgBox("Yakin PEMBELIAN GREY Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpembeliangrey_1()
                Call hapus_tbstokproses_1()
                Call hapus_tbdetil_hutang_1()
                Call hapus_update_tbkontrak_grey_1()
                Call hapus_tbpembeliangrey_2()
                Call hapus_tbstokproses_2()
                Call hapus_tbdetil_hutang_2()
                Call hapus_update_tbkontrak_grey_2()
                Call hapus_tbpembeliangrey_3()
                Call hapus_tbstokproses_3()
                Call hapus_tbdetil_hutang_3()
                Call hapus_update_tbkontrak_grey_3()
                Call hapus_tbpembeliangrey_4()
                Call hapus_tbstokproses_4()
                Call hapus_tbdetil_hutang_4()
                Call hapus_update_tbkontrak_grey_4()
                Call hapus_tbpembeliangrey_5()
                Call hapus_tbstokproses_5()
                Call hapus_tbdetil_hutang_5()
                Call hapus_update_tbkontrak_grey_5()
                Call hapus_tbpembeliangrey_6()
                Call hapus_tbstokproses_6()
                Call hapus_tbdetil_hutang_6()
                Call hapus_update_tbkontrak_grey_6()
                Call hapus_tbpembeliangrey_7()
                Call hapus_tbstokproses_7()
                Call hapus_tbdetil_hutang_7()
                Call hapus_update_tbkontrak_grey_7()
                form_pembelian_grey.Show()
                form_pembelian_grey.Focus()
                form_pembelian_grey.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class