﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_pembelian_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_id_grey = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cb_satuan = New System.Windows.Forms.ComboBox()
        Me.dtp_jatuh_tempo = New System.Windows.Forms.DateTimePicker()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_sisa_kontrak = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtp_tanggal = New System.Windows.Forms.DateTimePicker()
        Me.txt_no_kontrak = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_surat_jalan = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_supplier = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_harga = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_qty = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_jumlah = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_penyimpanan = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_keterangan = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btn_batal = New System.Windows.Forms.Button()
        Me.btn_simpan_tutup = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txt_asal_supplier = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txt_satuan_awal = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txt_satuan = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txt_sisa_awal = New System.Windows.Forms.TextBox()
        Me.txt_dikirim_awal = New System.Windows.Forms.TextBox()
        Me.txt_total_kontrak = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txt_konversi = New System.Windows.Forms.TextBox()
        Me.txt_qty_awal = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txt_id_hutang = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_id_beli = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(105, 294)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(24, 13)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "Rp."
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(12, 17)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 13)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "ID Grey"
        '
        'txt_id_grey
        '
        Me.txt_id_grey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey.Location = New System.Drawing.Point(12, 30)
        Me.txt_id_grey.Name = "txt_id_grey"
        Me.txt_id_grey.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_grey.TabIndex = 1
        Me.txt_id_grey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.Controls.Add(Me.cb_satuan)
        Me.Panel1.Controls.Add(Me.dtp_jatuh_tempo)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txt_sisa_kontrak)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.dtp_tanggal)
        Me.Panel1.Controls.Add(Me.txt_no_kontrak)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txt_surat_jalan)
        Me.Panel1.Controls.Add(Me.txt_jenis_kain)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txt_supplier)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.txt_harga)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txt_qty)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txt_jumlah)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txt_penyimpanan)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_keterangan)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(-1, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(300, 390)
        Me.Panel1.TabIndex = 59
        '
        'cb_satuan
        '
        Me.cb_satuan.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan.FormattingEnabled = True
        Me.cb_satuan.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan.Location = New System.Drawing.Point(220, 260)
        Me.cb_satuan.Name = "cb_satuan"
        Me.cb_satuan.Size = New System.Drawing.Size(49, 21)
        Me.cb_satuan.TabIndex = 103
        Me.cb_satuan.Text = "Meter"
        '
        'dtp_jatuh_tempo
        '
        Me.dtp_jatuh_tempo.Enabled = False
        Me.dtp_jatuh_tempo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo.Location = New System.Drawing.Point(170, 48)
        Me.dtp_jatuh_tempo.Name = "dtp_jatuh_tempo"
        Me.dtp_jatuh_tempo.Size = New System.Drawing.Size(99, 20)
        Me.dtp_jatuh_tempo.TabIndex = 102
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"15", "30", "45", "60", "75", "90", "105", "120", "135", "150", "165", "180", "195", "210", "225", "240", "255", "270", "285", "300", "315", "330", "345", "360", "375"})
        Me.ComboBox1.Location = New System.Drawing.Point(105, 47)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(59, 21)
        Me.ComboBox1.TabIndex = 101
        Me.ComboBox1.Text = "15"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(25, 234)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(67, 13)
        Me.Label17.TabIndex = 41
        Me.Label17.Text = "Sisa Kontrak"
        '
        'txt_sisa_kontrak
        '
        Me.txt_sisa_kontrak.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_kontrak.Location = New System.Drawing.Point(105, 230)
        Me.txt_sisa_kontrak.Name = "txt_sisa_kontrak"
        Me.txt_sisa_kontrak.ReadOnly = True
        Me.txt_sisa_kontrak.Size = New System.Drawing.Size(164, 20)
        Me.txt_sisa_kontrak.TabIndex = 40
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(105, 204)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(24, 13)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Rp."
        '
        'dtp_tanggal
        '
        Me.dtp_tanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal.Location = New System.Drawing.Point(105, 20)
        Me.dtp_tanggal.Name = "dtp_tanggal"
        Me.dtp_tanggal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_tanggal.TabIndex = 16
        '
        'txt_no_kontrak
        '
        Me.txt_no_kontrak.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak.Location = New System.Drawing.Point(105, 80)
        Me.txt_no_kontrak.Name = "txt_no_kontrak"
        Me.txt_no_kontrak.ReadOnly = True
        Me.txt_no_kontrak.Size = New System.Drawing.Size(164, 20)
        Me.txt_no_kontrak.TabIndex = 100
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(25, 354)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Keterangan"
        '
        'txt_surat_jalan
        '
        Me.txt_surat_jalan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_surat_jalan.Location = New System.Drawing.Point(105, 110)
        Me.txt_surat_jalan.Name = "txt_surat_jalan"
        Me.txt_surat_jalan.ReadOnly = True
        Me.txt_surat_jalan.Size = New System.Drawing.Size(164, 20)
        Me.txt_surat_jalan.TabIndex = 0
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain.Location = New System.Drawing.Point(105, 140)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.ReadOnly = True
        Me.txt_jenis_kain.Size = New System.Drawing.Size(164, 20)
        Me.txt_jenis_kain.TabIndex = 13
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(25, 324)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Gudang"
        '
        'txt_supplier
        '
        Me.txt_supplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier.Location = New System.Drawing.Point(105, 170)
        Me.txt_supplier.Name = "txt_supplier"
        Me.txt_supplier.Size = New System.Drawing.Size(164, 20)
        Me.txt_supplier.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(25, 294)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 13)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Total Harga"
        '
        'txt_harga
        '
        Me.txt_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga.Location = New System.Drawing.Point(130, 200)
        Me.txt_harga.Name = "txt_harga"
        Me.txt_harga.Size = New System.Drawing.Size(139, 20)
        Me.txt_harga.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(25, 264)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 13)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "Qty"
        '
        'txt_qty
        '
        Me.txt_qty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty.Location = New System.Drawing.Point(105, 260)
        Me.txt_qty.Name = "txt_qty"
        Me.txt_qty.Size = New System.Drawing.Size(109, 20)
        Me.txt_qty.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(25, 204)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Harga"
        '
        'txt_jumlah
        '
        Me.txt_jumlah.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah.Location = New System.Drawing.Point(129, 290)
        Me.txt_jumlah.Name = "txt_jumlah"
        Me.txt_jumlah.Size = New System.Drawing.Size(140, 20)
        Me.txt_jumlah.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(25, 174)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Supplier"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(25, 114)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 13)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Surat Jalan"
        '
        'txt_penyimpanan
        '
        Me.txt_penyimpanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_penyimpanan.Location = New System.Drawing.Point(105, 320)
        Me.txt_penyimpanan.Name = "txt_penyimpanan"
        Me.txt_penyimpanan.Size = New System.Drawing.Size(164, 20)
        Me.txt_penyimpanan.TabIndex = 19
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 144)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Jenis Kain"
        '
        'txt_keterangan
        '
        Me.txt_keterangan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan.Location = New System.Drawing.Point(105, 350)
        Me.txt_keterangan.Name = "txt_keterangan"
        Me.txt_keterangan.Size = New System.Drawing.Size(164, 20)
        Me.txt_keterangan.TabIndex = 20
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(25, 54)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 13)
        Me.Label13.TabIndex = 23
        Me.Label13.Text = "Jatuh Tempo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(25, 84)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "No Kontrak"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(25, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Tanggal"
        '
        'btn_batal
        '
        Me.btn_batal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Image = Global.HAOTEX.My.Resources.Resources.action_delete
        Me.btn_batal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal.Location = New System.Drawing.Point(189, 437)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(64, 23)
        Me.btn_batal.TabIndex = 61
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal.UseMnemonic = False
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan_tutup
        '
        Me.btn_simpan_tutup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_tutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_tutup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_tutup.Image = Global.HAOTEX.My.Resources.Resources.save1
        Me.btn_simpan_tutup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_tutup.Location = New System.Drawing.Point(41, 437)
        Me.btn_simpan_tutup.Name = "btn_simpan_tutup"
        Me.btn_simpan_tutup.Size = New System.Drawing.Size(127, 23)
        Me.btn_simpan_tutup.TabIndex = 60
        Me.btn_simpan_tutup.Text = "Simpan & Tutup"
        Me.btn_simpan_tutup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_tutup.UseMnemonic = False
        Me.btn_simpan_tutup.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.txt_asal_supplier)
        Me.Panel3.Controls.Add(Me.Label26)
        Me.Panel3.Controls.Add(Me.txt_satuan_awal)
        Me.Panel3.Controls.Add(Me.Label25)
        Me.Panel3.Controls.Add(Me.txt_satuan)
        Me.Panel3.Controls.Add(Me.Label24)
        Me.Panel3.Controls.Add(Me.Label23)
        Me.Panel3.Controls.Add(Me.Label22)
        Me.Panel3.Controls.Add(Me.Label21)
        Me.Panel3.Controls.Add(Me.txt_sisa_awal)
        Me.Panel3.Controls.Add(Me.txt_dikirim_awal)
        Me.Panel3.Controls.Add(Me.txt_total_kontrak)
        Me.Panel3.Controls.Add(Me.Label20)
        Me.Panel3.Controls.Add(Me.txt_konversi)
        Me.Panel3.Controls.Add(Me.txt_qty_awal)
        Me.Panel3.Controls.Add(Me.Label19)
        Me.Panel3.Controls.Add(Me.txt_id_hutang)
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.txt_id_beli)
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Controls.Add(Me.txt_id_grey)
        Me.Panel3.Location = New System.Drawing.Point(167, 12)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(124, 421)
        Me.Panel3.TabIndex = 75
        '
        'txt_asal_supplier
        '
        Me.txt_asal_supplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_supplier.Location = New System.Drawing.Point(12, 360)
        Me.txt_asal_supplier.Name = "txt_asal_supplier"
        Me.txt_asal_supplier.Size = New System.Drawing.Size(100, 20)
        Me.txt_asal_supplier.TabIndex = 97
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(12, 347)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(68, 13)
        Me.Label26.TabIndex = 96
        Me.Label26.Text = "Asal Supplier"
        '
        'txt_satuan_awal
        '
        Me.txt_satuan_awal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_satuan_awal.Location = New System.Drawing.Point(12, 195)
        Me.txt_satuan_awal.Name = "txt_satuan_awal"
        Me.txt_satuan_awal.Size = New System.Drawing.Size(100, 20)
        Me.txt_satuan_awal.TabIndex = 95
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(12, 182)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(86, 13)
        Me.Label25.TabIndex = 94
        Me.Label25.Text = "Satuan Qty Awal"
        '
        'txt_satuan
        '
        Me.txt_satuan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_satuan.Location = New System.Drawing.Point(12, 327)
        Me.txt_satuan.Name = "txt_satuan"
        Me.txt_satuan.Size = New System.Drawing.Size(100, 20)
        Me.txt_satuan.TabIndex = 93
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(12, 314)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(41, 13)
        Me.Label24.TabIndex = 92
        Me.Label24.Text = "Satuan"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(12, 281)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(53, 13)
        Me.Label23.TabIndex = 91
        Me.Label23.Text = "Sisa Awal"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(12, 248)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(64, 13)
        Me.Label22.TabIndex = 90
        Me.Label22.Text = "Dikirim Awal"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(12, 215)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(71, 13)
        Me.Label21.TabIndex = 89
        Me.Label21.Text = "Total Kontrak"
        '
        'txt_sisa_awal
        '
        Me.txt_sisa_awal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sisa_awal.Location = New System.Drawing.Point(12, 294)
        Me.txt_sisa_awal.Name = "txt_sisa_awal"
        Me.txt_sisa_awal.Size = New System.Drawing.Size(100, 20)
        Me.txt_sisa_awal.TabIndex = 88
        '
        'txt_dikirim_awal
        '
        Me.txt_dikirim_awal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_dikirim_awal.Location = New System.Drawing.Point(12, 261)
        Me.txt_dikirim_awal.Name = "txt_dikirim_awal"
        Me.txt_dikirim_awal.Size = New System.Drawing.Size(100, 20)
        Me.txt_dikirim_awal.TabIndex = 87
        '
        'txt_total_kontrak
        '
        Me.txt_total_kontrak.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_kontrak.Location = New System.Drawing.Point(12, 228)
        Me.txt_total_kontrak.Name = "txt_total_kontrak"
        Me.txt_total_kontrak.Size = New System.Drawing.Size(100, 20)
        Me.txt_total_kontrak.TabIndex = 86
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(12, 116)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(81, 13)
        Me.Label20.TabIndex = 85
        Me.Label20.Text = "Status Konversi"
        '
        'txt_konversi
        '
        Me.txt_konversi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_konversi.Location = New System.Drawing.Point(12, 129)
        Me.txt_konversi.Name = "txt_konversi"
        Me.txt_konversi.Size = New System.Drawing.Size(100, 20)
        Me.txt_konversi.TabIndex = 84
        '
        'txt_qty_awal
        '
        Me.txt_qty_awal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty_awal.Location = New System.Drawing.Point(12, 162)
        Me.txt_qty_awal.Name = "txt_qty_awal"
        Me.txt_qty_awal.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_awal.TabIndex = 83
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(12, 149)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(55, 13)
        Me.Label19.TabIndex = 82
        Me.Label19.Text = "QTY Awal"
        '
        'txt_id_hutang
        '
        Me.txt_id_hutang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_hutang.Location = New System.Drawing.Point(12, 63)
        Me.txt_id_hutang.Name = "txt_id_hutang"
        Me.txt_id_hutang.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_hutang.TabIndex = 81
        Me.txt_id_hutang.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(12, 50)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(56, 13)
        Me.Label18.TabIndex = 80
        Me.Label18.Text = "ID Hutang"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(12, 83)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(38, 13)
        Me.Label16.TabIndex = 79
        Me.Label16.Text = "ID Beli"
        '
        'txt_id_beli
        '
        Me.txt_id_beli.BackColor = System.Drawing.SystemColors.Window
        Me.txt_id_beli.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_beli.Location = New System.Drawing.Point(12, 96)
        Me.txt_id_beli.Name = "txt_id_beli"
        Me.txt_id_beli.ReadOnly = True
        Me.txt_id_beli.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_beli.TabIndex = 78
        Me.txt_id_beli.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.CausesValidation = False
        Me.Label1.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Window
        Me.Label1.Location = New System.Drawing.Point(-3, -1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(300, 34)
        Me.Label1.TabIndex = 76
        Me.Label1.Text = "Label1"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'form_input_pembelian_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(294, 472)
        Me.Controls.Add(Me.btn_batal)
        Me.Controls.Add(Me.btn_simpan_tutup)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_pembelian_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_id_grey As System.Windows.Forms.TextBox
    Friend WithEvents btn_simpan_tutup As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtp_tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_no_kontrak As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_surat_jalan As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_supplier As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_harga As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_qty As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_jumlah As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_penyimpanan As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_sisa_kontrak As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dtp_jatuh_tempo As System.Windows.Forms.DateTimePicker
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txt_id_beli As System.Windows.Forms.TextBox
    Friend WithEvents cb_satuan As System.Windows.Forms.ComboBox
    Friend WithEvents txt_id_hutang As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_qty_awal As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txt_konversi As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txt_sisa_awal As System.Windows.Forms.TextBox
    Friend WithEvents txt_dikirim_awal As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_kontrak As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txt_satuan As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txt_satuan_awal As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_supplier As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
End Class
