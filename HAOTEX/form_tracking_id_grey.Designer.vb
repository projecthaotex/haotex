﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_tracking_id_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_id_grey = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgv_kontrak = New System.Windows.Forms.DataGridView()
        Me.dgv_pembelian = New System.Windows.Forms.DataGridView()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgv_po_proses = New System.Windows.Forms.DataGridView()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgv_sj_proses = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgv_po_packing = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dgv_sj_packing = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dgv_sj_penjualan = New System.Windows.Forms.DataGridView()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dgv_penjualan_grey = New System.Windows.Forms.DataGridView()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btn_print = New System.Windows.Forms.Button()
        Me.btn_tracking = New System.Windows.Forms.Button()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        CType(Me.dgv_kontrak, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_pembelian, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.dgv_po_proses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_sj_proses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_po_packing, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_sj_packing, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_sj_penjualan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_penjualan_grey, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_id_grey
        '
        Me.txt_id_grey.Location = New System.Drawing.Point(87, 8)
        Me.txt_id_grey.Name = "txt_id_grey"
        Me.txt_id_grey.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_grey.TabIndex = 0
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.Location = New System.Drawing.Point(87, 36)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.ReadOnly = True
        Me.txt_jenis_kain.Size = New System.Drawing.Size(181, 20)
        Me.txt_jenis_kain.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "ID Grey"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Jenis Kain"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(604, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Kontrak Grey"
        '
        'dgv_kontrak
        '
        Me.dgv_kontrak.AllowUserToAddRows = False
        Me.dgv_kontrak.AllowUserToDeleteRows = False
        Me.dgv_kontrak.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_kontrak.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_kontrak.Location = New System.Drawing.Point(604, 28)
        Me.dgv_kontrak.Name = "dgv_kontrak"
        Me.dgv_kontrak.ReadOnly = True
        Me.dgv_kontrak.Size = New System.Drawing.Size(570, 50)
        Me.dgv_kontrak.TabIndex = 6
        '
        'dgv_pembelian
        '
        Me.dgv_pembelian.AllowUserToAddRows = False
        Me.dgv_pembelian.AllowUserToDeleteRows = False
        Me.dgv_pembelian.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_pembelian.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_pembelian.Location = New System.Drawing.Point(18, 102)
        Me.dgv_pembelian.Name = "dgv_pembelian"
        Me.dgv_pembelian.ReadOnly = True
        Me.dgv_pembelian.Size = New System.Drawing.Size(570, 70)
        Me.dgv_pembelian.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(18, 86)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Pembelian Grey"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_print)
        Me.Panel1.Controls.Add(Me.btn_tracking)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.txt_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_id_grey)
        Me.Panel1.Location = New System.Drawing.Point(18, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(570, 66)
        Me.Panel1.TabIndex = 0
        '
        'dgv_po_proses
        '
        Me.dgv_po_proses.AllowUserToAddRows = False
        Me.dgv_po_proses.AllowUserToDeleteRows = False
        Me.dgv_po_proses.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_po_proses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_po_proses.Location = New System.Drawing.Point(18, 194)
        Me.dgv_po_proses.Name = "dgv_po_proses"
        Me.dgv_po_proses.ReadOnly = True
        Me.dgv_po_proses.Size = New System.Drawing.Size(1156, 70)
        Me.dgv_po_proses.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 178)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "PO Proses"
        '
        'dgv_sj_proses
        '
        Me.dgv_sj_proses.AllowUserToAddRows = False
        Me.dgv_sj_proses.AllowUserToDeleteRows = False
        Me.dgv_sj_proses.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_sj_proses.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_sj_proses.Location = New System.Drawing.Point(18, 286)
        Me.dgv_sj_proses.Name = "dgv_sj_proses"
        Me.dgv_sj_proses.ReadOnly = True
        Me.dgv_sj_proses.Size = New System.Drawing.Size(1156, 70)
        Me.dgv_sj_proses.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(18, 270)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Surat Jalan Proses"
        '
        'dgv_po_packing
        '
        Me.dgv_po_packing.AllowUserToAddRows = False
        Me.dgv_po_packing.AllowUserToDeleteRows = False
        Me.dgv_po_packing.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_po_packing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_po_packing.Location = New System.Drawing.Point(18, 379)
        Me.dgv_po_packing.Name = "dgv_po_packing"
        Me.dgv_po_packing.ReadOnly = True
        Me.dgv_po_packing.Size = New System.Drawing.Size(1156, 70)
        Me.dgv_po_packing.TabIndex = 14
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(18, 363)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "PO Packing"
        '
        'dgv_sj_packing
        '
        Me.dgv_sj_packing.AllowUserToAddRows = False
        Me.dgv_sj_packing.AllowUserToDeleteRows = False
        Me.dgv_sj_packing.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_sj_packing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_sj_packing.Location = New System.Drawing.Point(18, 472)
        Me.dgv_sj_packing.Name = "dgv_sj_packing"
        Me.dgv_sj_packing.ReadOnly = True
        Me.dgv_sj_packing.Size = New System.Drawing.Size(1156, 70)
        Me.dgv_sj_packing.TabIndex = 16
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(18, 456)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(102, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Surat Jalan Packing"
        '
        'dgv_sj_penjualan
        '
        Me.dgv_sj_penjualan.AllowUserToAddRows = False
        Me.dgv_sj_penjualan.AllowUserToDeleteRows = False
        Me.dgv_sj_penjualan.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_sj_penjualan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_sj_penjualan.Location = New System.Drawing.Point(18, 565)
        Me.dgv_sj_penjualan.Name = "dgv_sj_penjualan"
        Me.dgv_sj_penjualan.ReadOnly = True
        Me.dgv_sj_penjualan.Size = New System.Drawing.Size(1156, 70)
        Me.dgv_sj_penjualan.TabIndex = 18
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(18, 549)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(110, 13)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Surat Jalan Penjualan"
        '
        'dgv_penjualan_grey
        '
        Me.dgv_penjualan_grey.AllowUserToAddRows = False
        Me.dgv_penjualan_grey.AllowUserToDeleteRows = False
        Me.dgv_penjualan_grey.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv_penjualan_grey.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_penjualan_grey.Location = New System.Drawing.Point(604, 102)
        Me.dgv_penjualan_grey.Name = "dgv_penjualan_grey"
        Me.dgv_penjualan_grey.ReadOnly = True
        Me.dgv_penjualan_grey.Size = New System.Drawing.Size(570, 70)
        Me.dgv_penjualan_grey.TabIndex = 20
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(604, 86)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "Penjualan Grey"
        '
        'btn_print
        '
        Me.btn_print.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.btn_print.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_print.Location = New System.Drawing.Point(456, 4)
        Me.btn_print.Name = "btn_print"
        Me.btn_print.Size = New System.Drawing.Size(83, 56)
        Me.btn_print.TabIndex = 6
        Me.btn_print.Text = "PRINT"
        Me.btn_print.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_print.UseVisualStyleBackColor = True
        '
        'btn_tracking
        '
        Me.btn_tracking.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tracking.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.btn_tracking.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_tracking.Location = New System.Drawing.Point(330, 4)
        Me.btn_tracking.Name = "btn_tracking"
        Me.btn_tracking.Size = New System.Drawing.Size(83, 56)
        Me.btn_tracking.TabIndex = 5
        Me.btn_tracking.Text = "TRACKING"
        Me.btn_tracking.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_tracking.UseVisualStyleBackColor = True
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(224, 22)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(104, 20)
        Me.dtp_hari_ini.TabIndex = 41
        '
        'form_tracking_id_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1192, 646)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.dgv_penjualan_grey)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.dgv_sj_penjualan)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.dgv_sj_packing)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.dgv_po_packing)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.dgv_sj_proses)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dgv_po_proses)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dgv_pembelian)
        Me.Controls.Add(Me.dgv_kontrak)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_tracking_id_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM TRACKING ID GREY"
        CType(Me.dgv_kontrak, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_pembelian, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgv_po_proses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_sj_proses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_po_packing, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_sj_packing, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_sj_penjualan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_penjualan_grey, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txt_id_grey As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btn_tracking As System.Windows.Forms.Button
    Friend WithEvents dgv_kontrak As System.Windows.Forms.DataGridView
    Friend WithEvents dgv_pembelian As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgv_po_proses As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgv_sj_proses As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dgv_po_packing As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dgv_sj_packing As System.Windows.Forms.DataGridView
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btn_print As System.Windows.Forms.Button
    Friend WithEvents dgv_sj_penjualan As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dgv_penjualan_grey As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
End Class
