﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_print_hasil_packing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_print = New System.Windows.Forms.ToolStripButton()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.txt_gulung = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.dtp_jatuh_tempo = New System.Windows.Forms.DateTimePicker()
        Me.txt_gl_claim_jadi = New System.Windows.Forms.TextBox()
        Me.txt_gl_claim_celup = New System.Windows.Forms.TextBox()
        Me.txt_gl_grade_b = New System.Windows.Forms.TextBox()
        Me.txt_gl_grade_a = New System.Windows.Forms.TextBox()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txt_claim_celup = New System.Windows.Forms.TextBox()
        Me.txt_grade_a = New System.Windows.Forms.TextBox()
        Me.txt_claim_jadi = New System.Windows.Forms.TextBox()
        Me.txt_grade_b = New System.Windows.Forms.TextBox()
        Me.txt_susut = New System.Windows.Forms.TextBox()
        Me.txt_yards = New System.Windows.Forms.TextBox()
        Me.txt_meter = New System.Windows.Forms.TextBox()
        Me.txt_warna = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_harga = New System.Windows.Forms.TextBox()
        Me.txt_customer = New System.Windows.Forms.TextBox()
        Me.txt_baris_po = New System.Windows.Forms.TextBox()
        Me.txt_total_harga = New System.Windows.Forms.TextBox()
        Me.txt_no_po = New System.Windows.Forms.TextBox()
        Me.txt_baris_sj = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.txt_sj_packing = New System.Windows.Forms.TextBox()
        Me.txt_gudang = New System.Windows.Forms.TextBox()
        Me.txt_keterangan = New System.Windows.Forms.RichTextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.S25 = New System.Windows.Forms.TextBox()
        Me.S21 = New System.Windows.Forms.TextBox()
        Me.S23 = New System.Windows.Forms.TextBox()
        Me.S24 = New System.Windows.Forms.TextBox()
        Me.S22 = New System.Windows.Forms.TextBox()
        Me.S20 = New System.Windows.Forms.TextBox()
        Me.S16 = New System.Windows.Forms.TextBox()
        Me.S8 = New System.Windows.Forms.TextBox()
        Me.S12 = New System.Windows.Forms.TextBox()
        Me.S4 = New System.Windows.Forms.TextBox()
        Me.S18 = New System.Windows.Forms.TextBox()
        Me.S14 = New System.Windows.Forms.TextBox()
        Me.S6 = New System.Windows.Forms.TextBox()
        Me.S10 = New System.Windows.Forms.TextBox()
        Me.S2 = New System.Windows.Forms.TextBox()
        Me.S19 = New System.Windows.Forms.TextBox()
        Me.S15 = New System.Windows.Forms.TextBox()
        Me.S7 = New System.Windows.Forms.TextBox()
        Me.S11 = New System.Windows.Forms.TextBox()
        Me.S3 = New System.Windows.Forms.TextBox()
        Me.S17 = New System.Windows.Forms.TextBox()
        Me.S13 = New System.Windows.Forms.TextBox()
        Me.S5 = New System.Windows.Forms.TextBox()
        Me.S9 = New System.Windows.Forms.TextBox()
        Me.S1 = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.E25 = New System.Windows.Forms.TextBox()
        Me.D25 = New System.Windows.Forms.TextBox()
        Me.C25 = New System.Windows.Forms.TextBox()
        Me.B25 = New System.Windows.Forms.TextBox()
        Me.A25 = New System.Windows.Forms.TextBox()
        Me.K25 = New System.Windows.Forms.TextBox()
        Me.Y25 = New System.Windows.Forms.TextBox()
        Me.M25 = New System.Windows.Forms.TextBox()
        Me.E21 = New System.Windows.Forms.TextBox()
        Me.D21 = New System.Windows.Forms.TextBox()
        Me.C21 = New System.Windows.Forms.TextBox()
        Me.B21 = New System.Windows.Forms.TextBox()
        Me.A21 = New System.Windows.Forms.TextBox()
        Me.K21 = New System.Windows.Forms.TextBox()
        Me.Y21 = New System.Windows.Forms.TextBox()
        Me.M21 = New System.Windows.Forms.TextBox()
        Me.E23 = New System.Windows.Forms.TextBox()
        Me.D23 = New System.Windows.Forms.TextBox()
        Me.C23 = New System.Windows.Forms.TextBox()
        Me.B23 = New System.Windows.Forms.TextBox()
        Me.A23 = New System.Windows.Forms.TextBox()
        Me.K23 = New System.Windows.Forms.TextBox()
        Me.Y23 = New System.Windows.Forms.TextBox()
        Me.M23 = New System.Windows.Forms.TextBox()
        Me.E24 = New System.Windows.Forms.TextBox()
        Me.D24 = New System.Windows.Forms.TextBox()
        Me.C24 = New System.Windows.Forms.TextBox()
        Me.B24 = New System.Windows.Forms.TextBox()
        Me.A24 = New System.Windows.Forms.TextBox()
        Me.K24 = New System.Windows.Forms.TextBox()
        Me.Y24 = New System.Windows.Forms.TextBox()
        Me.M24 = New System.Windows.Forms.TextBox()
        Me.E22 = New System.Windows.Forms.TextBox()
        Me.D22 = New System.Windows.Forms.TextBox()
        Me.C22 = New System.Windows.Forms.TextBox()
        Me.B22 = New System.Windows.Forms.TextBox()
        Me.A22 = New System.Windows.Forms.TextBox()
        Me.K22 = New System.Windows.Forms.TextBox()
        Me.Y22 = New System.Windows.Forms.TextBox()
        Me.M22 = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.E20 = New System.Windows.Forms.TextBox()
        Me.D20 = New System.Windows.Forms.TextBox()
        Me.C20 = New System.Windows.Forms.TextBox()
        Me.B20 = New System.Windows.Forms.TextBox()
        Me.A20 = New System.Windows.Forms.TextBox()
        Me.K20 = New System.Windows.Forms.TextBox()
        Me.Y20 = New System.Windows.Forms.TextBox()
        Me.M20 = New System.Windows.Forms.TextBox()
        Me.E16 = New System.Windows.Forms.TextBox()
        Me.D16 = New System.Windows.Forms.TextBox()
        Me.C16 = New System.Windows.Forms.TextBox()
        Me.B16 = New System.Windows.Forms.TextBox()
        Me.A16 = New System.Windows.Forms.TextBox()
        Me.K16 = New System.Windows.Forms.TextBox()
        Me.Y16 = New System.Windows.Forms.TextBox()
        Me.M16 = New System.Windows.Forms.TextBox()
        Me.E8 = New System.Windows.Forms.TextBox()
        Me.D8 = New System.Windows.Forms.TextBox()
        Me.C8 = New System.Windows.Forms.TextBox()
        Me.B8 = New System.Windows.Forms.TextBox()
        Me.A8 = New System.Windows.Forms.TextBox()
        Me.K8 = New System.Windows.Forms.TextBox()
        Me.Y8 = New System.Windows.Forms.TextBox()
        Me.M8 = New System.Windows.Forms.TextBox()
        Me.E12 = New System.Windows.Forms.TextBox()
        Me.D12 = New System.Windows.Forms.TextBox()
        Me.C12 = New System.Windows.Forms.TextBox()
        Me.B12 = New System.Windows.Forms.TextBox()
        Me.A12 = New System.Windows.Forms.TextBox()
        Me.K12 = New System.Windows.Forms.TextBox()
        Me.Y12 = New System.Windows.Forms.TextBox()
        Me.M12 = New System.Windows.Forms.TextBox()
        Me.E4 = New System.Windows.Forms.TextBox()
        Me.D4 = New System.Windows.Forms.TextBox()
        Me.C4 = New System.Windows.Forms.TextBox()
        Me.B4 = New System.Windows.Forms.TextBox()
        Me.A4 = New System.Windows.Forms.TextBox()
        Me.K4 = New System.Windows.Forms.TextBox()
        Me.Y4 = New System.Windows.Forms.TextBox()
        Me.M4 = New System.Windows.Forms.TextBox()
        Me.E18 = New System.Windows.Forms.TextBox()
        Me.D18 = New System.Windows.Forms.TextBox()
        Me.C18 = New System.Windows.Forms.TextBox()
        Me.B18 = New System.Windows.Forms.TextBox()
        Me.A18 = New System.Windows.Forms.TextBox()
        Me.K18 = New System.Windows.Forms.TextBox()
        Me.Y18 = New System.Windows.Forms.TextBox()
        Me.M18 = New System.Windows.Forms.TextBox()
        Me.E14 = New System.Windows.Forms.TextBox()
        Me.D14 = New System.Windows.Forms.TextBox()
        Me.C14 = New System.Windows.Forms.TextBox()
        Me.B14 = New System.Windows.Forms.TextBox()
        Me.A14 = New System.Windows.Forms.TextBox()
        Me.K14 = New System.Windows.Forms.TextBox()
        Me.Y14 = New System.Windows.Forms.TextBox()
        Me.M14 = New System.Windows.Forms.TextBox()
        Me.E6 = New System.Windows.Forms.TextBox()
        Me.D6 = New System.Windows.Forms.TextBox()
        Me.C6 = New System.Windows.Forms.TextBox()
        Me.B6 = New System.Windows.Forms.TextBox()
        Me.A6 = New System.Windows.Forms.TextBox()
        Me.K6 = New System.Windows.Forms.TextBox()
        Me.Y6 = New System.Windows.Forms.TextBox()
        Me.M6 = New System.Windows.Forms.TextBox()
        Me.E10 = New System.Windows.Forms.TextBox()
        Me.D10 = New System.Windows.Forms.TextBox()
        Me.C10 = New System.Windows.Forms.TextBox()
        Me.B10 = New System.Windows.Forms.TextBox()
        Me.A10 = New System.Windows.Forms.TextBox()
        Me.K10 = New System.Windows.Forms.TextBox()
        Me.Y10 = New System.Windows.Forms.TextBox()
        Me.M10 = New System.Windows.Forms.TextBox()
        Me.E2 = New System.Windows.Forms.TextBox()
        Me.D2 = New System.Windows.Forms.TextBox()
        Me.C2 = New System.Windows.Forms.TextBox()
        Me.B2 = New System.Windows.Forms.TextBox()
        Me.A2 = New System.Windows.Forms.TextBox()
        Me.K2 = New System.Windows.Forms.TextBox()
        Me.Y2 = New System.Windows.Forms.TextBox()
        Me.M2 = New System.Windows.Forms.TextBox()
        Me.E19 = New System.Windows.Forms.TextBox()
        Me.D19 = New System.Windows.Forms.TextBox()
        Me.C19 = New System.Windows.Forms.TextBox()
        Me.B19 = New System.Windows.Forms.TextBox()
        Me.A19 = New System.Windows.Forms.TextBox()
        Me.K19 = New System.Windows.Forms.TextBox()
        Me.Y19 = New System.Windows.Forms.TextBox()
        Me.M19 = New System.Windows.Forms.TextBox()
        Me.E15 = New System.Windows.Forms.TextBox()
        Me.D15 = New System.Windows.Forms.TextBox()
        Me.C15 = New System.Windows.Forms.TextBox()
        Me.B15 = New System.Windows.Forms.TextBox()
        Me.A15 = New System.Windows.Forms.TextBox()
        Me.K15 = New System.Windows.Forms.TextBox()
        Me.Y15 = New System.Windows.Forms.TextBox()
        Me.M15 = New System.Windows.Forms.TextBox()
        Me.E7 = New System.Windows.Forms.TextBox()
        Me.D7 = New System.Windows.Forms.TextBox()
        Me.C7 = New System.Windows.Forms.TextBox()
        Me.B7 = New System.Windows.Forms.TextBox()
        Me.A7 = New System.Windows.Forms.TextBox()
        Me.K7 = New System.Windows.Forms.TextBox()
        Me.Y7 = New System.Windows.Forms.TextBox()
        Me.M7 = New System.Windows.Forms.TextBox()
        Me.E11 = New System.Windows.Forms.TextBox()
        Me.D11 = New System.Windows.Forms.TextBox()
        Me.C11 = New System.Windows.Forms.TextBox()
        Me.B11 = New System.Windows.Forms.TextBox()
        Me.A11 = New System.Windows.Forms.TextBox()
        Me.K11 = New System.Windows.Forms.TextBox()
        Me.Y11 = New System.Windows.Forms.TextBox()
        Me.M11 = New System.Windows.Forms.TextBox()
        Me.E3 = New System.Windows.Forms.TextBox()
        Me.D3 = New System.Windows.Forms.TextBox()
        Me.C3 = New System.Windows.Forms.TextBox()
        Me.B3 = New System.Windows.Forms.TextBox()
        Me.A3 = New System.Windows.Forms.TextBox()
        Me.K3 = New System.Windows.Forms.TextBox()
        Me.Y3 = New System.Windows.Forms.TextBox()
        Me.M3 = New System.Windows.Forms.TextBox()
        Me.E17 = New System.Windows.Forms.TextBox()
        Me.D17 = New System.Windows.Forms.TextBox()
        Me.C17 = New System.Windows.Forms.TextBox()
        Me.B17 = New System.Windows.Forms.TextBox()
        Me.A17 = New System.Windows.Forms.TextBox()
        Me.K17 = New System.Windows.Forms.TextBox()
        Me.Y17 = New System.Windows.Forms.TextBox()
        Me.M17 = New System.Windows.Forms.TextBox()
        Me.E13 = New System.Windows.Forms.TextBox()
        Me.D13 = New System.Windows.Forms.TextBox()
        Me.C13 = New System.Windows.Forms.TextBox()
        Me.B13 = New System.Windows.Forms.TextBox()
        Me.A13 = New System.Windows.Forms.TextBox()
        Me.K13 = New System.Windows.Forms.TextBox()
        Me.Y13 = New System.Windows.Forms.TextBox()
        Me.M13 = New System.Windows.Forms.TextBox()
        Me.E5 = New System.Windows.Forms.TextBox()
        Me.D5 = New System.Windows.Forms.TextBox()
        Me.C5 = New System.Windows.Forms.TextBox()
        Me.B5 = New System.Windows.Forms.TextBox()
        Me.A5 = New System.Windows.Forms.TextBox()
        Me.K5 = New System.Windows.Forms.TextBox()
        Me.Y5 = New System.Windows.Forms.TextBox()
        Me.M5 = New System.Windows.Forms.TextBox()
        Me.E9 = New System.Windows.Forms.TextBox()
        Me.D9 = New System.Windows.Forms.TextBox()
        Me.C9 = New System.Windows.Forms.TextBox()
        Me.B9 = New System.Windows.Forms.TextBox()
        Me.A9 = New System.Windows.Forms.TextBox()
        Me.K9 = New System.Windows.Forms.TextBox()
        Me.Y9 = New System.Windows.Forms.TextBox()
        Me.M9 = New System.Windows.Forms.TextBox()
        Me.E1 = New System.Windows.Forms.TextBox()
        Me.D1 = New System.Windows.Forms.TextBox()
        Me.C1 = New System.Windows.Forms.TextBox()
        Me.B1 = New System.Windows.Forms.TextBox()
        Me.A1 = New System.Windows.Forms.TextBox()
        Me.K1 = New System.Windows.Forms.TextBox()
        Me.Y1 = New System.Windows.Forms.TextBox()
        Me.M1 = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.EW25 = New System.Windows.Forms.TextBox()
        Me.DW25 = New System.Windows.Forms.TextBox()
        Me.CW25 = New System.Windows.Forms.TextBox()
        Me.BW25 = New System.Windows.Forms.TextBox()
        Me.AW25 = New System.Windows.Forms.TextBox()
        Me.EW21 = New System.Windows.Forms.TextBox()
        Me.DW21 = New System.Windows.Forms.TextBox()
        Me.CW21 = New System.Windows.Forms.TextBox()
        Me.BW21 = New System.Windows.Forms.TextBox()
        Me.AW21 = New System.Windows.Forms.TextBox()
        Me.EW23 = New System.Windows.Forms.TextBox()
        Me.DW23 = New System.Windows.Forms.TextBox()
        Me.CW23 = New System.Windows.Forms.TextBox()
        Me.BW23 = New System.Windows.Forms.TextBox()
        Me.AW23 = New System.Windows.Forms.TextBox()
        Me.EW24 = New System.Windows.Forms.TextBox()
        Me.DW24 = New System.Windows.Forms.TextBox()
        Me.CW24 = New System.Windows.Forms.TextBox()
        Me.BW24 = New System.Windows.Forms.TextBox()
        Me.AW24 = New System.Windows.Forms.TextBox()
        Me.EW22 = New System.Windows.Forms.TextBox()
        Me.DW22 = New System.Windows.Forms.TextBox()
        Me.CW22 = New System.Windows.Forms.TextBox()
        Me.BW22 = New System.Windows.Forms.TextBox()
        Me.AW22 = New System.Windows.Forms.TextBox()
        Me.EW20 = New System.Windows.Forms.TextBox()
        Me.DW20 = New System.Windows.Forms.TextBox()
        Me.CW20 = New System.Windows.Forms.TextBox()
        Me.BW20 = New System.Windows.Forms.TextBox()
        Me.AW20 = New System.Windows.Forms.TextBox()
        Me.EW16 = New System.Windows.Forms.TextBox()
        Me.DW16 = New System.Windows.Forms.TextBox()
        Me.CW16 = New System.Windows.Forms.TextBox()
        Me.BW16 = New System.Windows.Forms.TextBox()
        Me.AW16 = New System.Windows.Forms.TextBox()
        Me.EW8 = New System.Windows.Forms.TextBox()
        Me.DW8 = New System.Windows.Forms.TextBox()
        Me.CW8 = New System.Windows.Forms.TextBox()
        Me.BW8 = New System.Windows.Forms.TextBox()
        Me.AW8 = New System.Windows.Forms.TextBox()
        Me.EW12 = New System.Windows.Forms.TextBox()
        Me.DW12 = New System.Windows.Forms.TextBox()
        Me.CW12 = New System.Windows.Forms.TextBox()
        Me.BW12 = New System.Windows.Forms.TextBox()
        Me.AW12 = New System.Windows.Forms.TextBox()
        Me.EW4 = New System.Windows.Forms.TextBox()
        Me.DW4 = New System.Windows.Forms.TextBox()
        Me.CW4 = New System.Windows.Forms.TextBox()
        Me.BW4 = New System.Windows.Forms.TextBox()
        Me.AW4 = New System.Windows.Forms.TextBox()
        Me.EW18 = New System.Windows.Forms.TextBox()
        Me.DW18 = New System.Windows.Forms.TextBox()
        Me.CW18 = New System.Windows.Forms.TextBox()
        Me.BW18 = New System.Windows.Forms.TextBox()
        Me.AW18 = New System.Windows.Forms.TextBox()
        Me.EW14 = New System.Windows.Forms.TextBox()
        Me.DW14 = New System.Windows.Forms.TextBox()
        Me.CW14 = New System.Windows.Forms.TextBox()
        Me.BW14 = New System.Windows.Forms.TextBox()
        Me.AW14 = New System.Windows.Forms.TextBox()
        Me.EW6 = New System.Windows.Forms.TextBox()
        Me.DW6 = New System.Windows.Forms.TextBox()
        Me.CW6 = New System.Windows.Forms.TextBox()
        Me.BW6 = New System.Windows.Forms.TextBox()
        Me.AW6 = New System.Windows.Forms.TextBox()
        Me.EW10 = New System.Windows.Forms.TextBox()
        Me.DW10 = New System.Windows.Forms.TextBox()
        Me.CW10 = New System.Windows.Forms.TextBox()
        Me.BW10 = New System.Windows.Forms.TextBox()
        Me.AW10 = New System.Windows.Forms.TextBox()
        Me.EW2 = New System.Windows.Forms.TextBox()
        Me.DW2 = New System.Windows.Forms.TextBox()
        Me.CW2 = New System.Windows.Forms.TextBox()
        Me.BW2 = New System.Windows.Forms.TextBox()
        Me.AW2 = New System.Windows.Forms.TextBox()
        Me.EW19 = New System.Windows.Forms.TextBox()
        Me.DW19 = New System.Windows.Forms.TextBox()
        Me.CW19 = New System.Windows.Forms.TextBox()
        Me.BW19 = New System.Windows.Forms.TextBox()
        Me.AW19 = New System.Windows.Forms.TextBox()
        Me.EW15 = New System.Windows.Forms.TextBox()
        Me.DW15 = New System.Windows.Forms.TextBox()
        Me.CW15 = New System.Windows.Forms.TextBox()
        Me.BW15 = New System.Windows.Forms.TextBox()
        Me.AW15 = New System.Windows.Forms.TextBox()
        Me.EW7 = New System.Windows.Forms.TextBox()
        Me.DW7 = New System.Windows.Forms.TextBox()
        Me.CW7 = New System.Windows.Forms.TextBox()
        Me.BW7 = New System.Windows.Forms.TextBox()
        Me.AW7 = New System.Windows.Forms.TextBox()
        Me.EW11 = New System.Windows.Forms.TextBox()
        Me.DW11 = New System.Windows.Forms.TextBox()
        Me.CW11 = New System.Windows.Forms.TextBox()
        Me.BW11 = New System.Windows.Forms.TextBox()
        Me.AW11 = New System.Windows.Forms.TextBox()
        Me.EW3 = New System.Windows.Forms.TextBox()
        Me.DW3 = New System.Windows.Forms.TextBox()
        Me.CW3 = New System.Windows.Forms.TextBox()
        Me.BW3 = New System.Windows.Forms.TextBox()
        Me.AW3 = New System.Windows.Forms.TextBox()
        Me.EW17 = New System.Windows.Forms.TextBox()
        Me.DW17 = New System.Windows.Forms.TextBox()
        Me.CW17 = New System.Windows.Forms.TextBox()
        Me.BW17 = New System.Windows.Forms.TextBox()
        Me.AW17 = New System.Windows.Forms.TextBox()
        Me.EW13 = New System.Windows.Forms.TextBox()
        Me.DW13 = New System.Windows.Forms.TextBox()
        Me.CW13 = New System.Windows.Forms.TextBox()
        Me.BW13 = New System.Windows.Forms.TextBox()
        Me.AW13 = New System.Windows.Forms.TextBox()
        Me.EW5 = New System.Windows.Forms.TextBox()
        Me.DW5 = New System.Windows.Forms.TextBox()
        Me.CW5 = New System.Windows.Forms.TextBox()
        Me.BW5 = New System.Windows.Forms.TextBox()
        Me.AW5 = New System.Windows.Forms.TextBox()
        Me.EW9 = New System.Windows.Forms.TextBox()
        Me.DW9 = New System.Windows.Forms.TextBox()
        Me.CW9 = New System.Windows.Forms.TextBox()
        Me.BW9 = New System.Windows.Forms.TextBox()
        Me.AW9 = New System.Windows.Forms.TextBox()
        Me.EW1 = New System.Windows.Forms.TextBox()
        Me.DW1 = New System.Windows.Forms.TextBox()
        Me.CW1 = New System.Windows.Forms.TextBox()
        Me.BW1 = New System.Windows.Forms.TextBox()
        Me.AW1 = New System.Windows.Forms.TextBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_print})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1041, 25)
        Me.ToolStrip1.TabIndex = 85
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_print
        '
        Me.ts_print.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ts_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print.Name = "ts_print"
        Me.ts_print.Size = New System.Drawing.Size(62, 22)
        Me.ts_print.Text = "PRINT"
        '
        'Label56
        '
        Me.Label56.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label56.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.SystemColors.Window
        Me.Label56.Location = New System.Drawing.Point(5, 29)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(330, 34)
        Me.Label56.TabIndex = 88
        Me.Label56.Text = "HASIL PACKING"
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Me.Label45)
        Me.Panel1.Controls.Add(Me.Label62)
        Me.Panel1.Controls.Add(Me.txt_gulung)
        Me.Panel1.Controls.Add(Me.Label63)
        Me.Panel1.Controls.Add(Me.dtp_jatuh_tempo)
        Me.Panel1.Controls.Add(Me.txt_gl_claim_jadi)
        Me.Panel1.Controls.Add(Me.txt_gl_claim_celup)
        Me.Panel1.Controls.Add(Me.txt_gl_grade_b)
        Me.Panel1.Controls.Add(Me.txt_gl_grade_a)
        Me.Panel1.Controls.Add(Me.Label55)
        Me.Panel1.Controls.Add(Me.Label54)
        Me.Panel1.Controls.Add(Me.Label53)
        Me.Panel1.Controls.Add(Me.Label52)
        Me.Panel1.Controls.Add(Me.Label51)
        Me.Panel1.Controls.Add(Me.Label50)
        Me.Panel1.Controls.Add(Me.Label49)
        Me.Panel1.Controls.Add(Me.Label48)
        Me.Panel1.Controls.Add(Me.Label47)
        Me.Panel1.Controls.Add(Me.Label44)
        Me.Panel1.Controls.Add(Me.Label43)
        Me.Panel1.Controls.Add(Me.Label46)
        Me.Panel1.Controls.Add(Me.Label42)
        Me.Panel1.Controls.Add(Me.Label41)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.Label35)
        Me.Panel1.Controls.Add(Me.Label34)
        Me.Panel1.Controls.Add(Me.Label33)
        Me.Panel1.Controls.Add(Me.Label38)
        Me.Panel1.Controls.Add(Me.Label32)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.Label37)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.Label29)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.txt_claim_celup)
        Me.Panel1.Controls.Add(Me.txt_grade_a)
        Me.Panel1.Controls.Add(Me.txt_claim_jadi)
        Me.Panel1.Controls.Add(Me.txt_grade_b)
        Me.Panel1.Controls.Add(Me.txt_susut)
        Me.Panel1.Controls.Add(Me.txt_yards)
        Me.Panel1.Controls.Add(Me.txt_meter)
        Me.Panel1.Controls.Add(Me.txt_warna)
        Me.Panel1.Controls.Add(Me.txt_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_harga)
        Me.Panel1.Controls.Add(Me.txt_customer)
        Me.Panel1.Controls.Add(Me.txt_baris_po)
        Me.Panel1.Controls.Add(Me.txt_total_harga)
        Me.Panel1.Controls.Add(Me.txt_no_po)
        Me.Panel1.Controls.Add(Me.txt_baris_sj)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.Label24)
        Me.Panel1.Controls.Add(Me.dtp_awal)
        Me.Panel1.Controls.Add(Me.txt_sj_packing)
        Me.Panel1.Controls.Add(Me.txt_gudang)
        Me.Panel1.Controls.Add(Me.txt_keterangan)
        Me.Panel1.Location = New System.Drawing.Point(5, 64)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(330, 573)
        Me.Panel1.TabIndex = 86
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(104, 543)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(0, 20)
        Me.Label45.TabIndex = 69
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(16, 547)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(37, 13)
        Me.Label62.TabIndex = 67
        Me.Label62.Text = "Status"
        '
        'txt_gulung
        '
        Me.txt_gulung.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gulung.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung.Location = New System.Drawing.Point(104, 252)
        Me.txt_gulung.Name = "txt_gulung"
        Me.txt_gulung.ReadOnly = True
        Me.txt_gulung.Size = New System.Drawing.Size(90, 20)
        Me.txt_gulung.TabIndex = 61
        Me.txt_gulung.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(16, 40)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(69, 13)
        Me.Label63.TabIndex = 59
        Me.Label63.Text = "Jatuh Tempo"
        '
        'dtp_jatuh_tempo
        '
        Me.dtp_jatuh_tempo.Enabled = False
        Me.dtp_jatuh_tempo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo.Location = New System.Drawing.Point(214, 36)
        Me.dtp_jatuh_tempo.Name = "dtp_jatuh_tempo"
        Me.dtp_jatuh_tempo.Size = New System.Drawing.Size(99, 20)
        Me.dtp_jatuh_tempo.TabIndex = 58
        '
        'txt_gl_claim_jadi
        '
        Me.txt_gl_claim_jadi.BackColor = System.Drawing.Color.Khaki
        Me.txt_gl_claim_jadi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gl_claim_jadi.Location = New System.Drawing.Point(223, 397)
        Me.txt_gl_claim_jadi.MaxLength = 6
        Me.txt_gl_claim_jadi.Name = "txt_gl_claim_jadi"
        Me.txt_gl_claim_jadi.ReadOnly = True
        Me.txt_gl_claim_jadi.Size = New System.Drawing.Size(67, 20)
        Me.txt_gl_claim_jadi.TabIndex = 57
        Me.txt_gl_claim_jadi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gl_claim_celup
        '
        Me.txt_gl_claim_celup.BackColor = System.Drawing.Color.Pink
        Me.txt_gl_claim_celup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gl_claim_celup.Location = New System.Drawing.Point(223, 421)
        Me.txt_gl_claim_celup.MaxLength = 6
        Me.txt_gl_claim_celup.Name = "txt_gl_claim_celup"
        Me.txt_gl_claim_celup.ReadOnly = True
        Me.txt_gl_claim_celup.Size = New System.Drawing.Size(67, 20)
        Me.txt_gl_claim_celup.TabIndex = 56
        Me.txt_gl_claim_celup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gl_grade_b
        '
        Me.txt_gl_grade_b.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txt_gl_grade_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gl_grade_b.Location = New System.Drawing.Point(223, 373)
        Me.txt_gl_grade_b.MaxLength = 6
        Me.txt_gl_grade_b.Name = "txt_gl_grade_b"
        Me.txt_gl_grade_b.ReadOnly = True
        Me.txt_gl_grade_b.Size = New System.Drawing.Size(67, 20)
        Me.txt_gl_grade_b.TabIndex = 55
        Me.txt_gl_grade_b.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gl_grade_a
        '
        Me.txt_gl_grade_a.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gl_grade_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gl_grade_a.Location = New System.Drawing.Point(223, 349)
        Me.txt_gl_grade_a.MaxLength = 6
        Me.txt_gl_grade_a.Name = "txt_gl_grade_a"
        Me.txt_gl_grade_a.ReadOnly = True
        Me.txt_gl_grade_a.Size = New System.Drawing.Size(67, 20)
        Me.txt_gl_grade_a.TabIndex = 54
        Me.txt_gl_grade_a.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Location = New System.Drawing.Point(298, 401)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(15, 13)
        Me.Label55.TabIndex = 53
        Me.Label55.Text = "gl"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Location = New System.Drawing.Point(298, 425)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(15, 13)
        Me.Label54.TabIndex = 52
        Me.Label54.Text = "gl"
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Location = New System.Drawing.Point(298, 377)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(15, 13)
        Me.Label53.TabIndex = 51
        Me.Label53.Text = "gl"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Location = New System.Drawing.Point(298, 353)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(15, 13)
        Me.Label52.TabIndex = 50
        Me.Label52.Text = "gl"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(204, 401)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(10, 13)
        Me.Label51.TabIndex = 49
        Me.Label51.Text = "-"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(204, 425)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(10, 13)
        Me.Label50.TabIndex = 48
        Me.Label50.Text = "-"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(204, 377)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(10, 13)
        Me.Label49.TabIndex = 47
        Me.Label49.Text = "-"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Location = New System.Drawing.Point(204, 353)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(10, 13)
        Me.Label48.TabIndex = 46
        Me.Label48.Text = "-"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(104, 474)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(24, 13)
        Me.Label47.TabIndex = 45
        Me.Label47.Text = "Rp."
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(16, 496)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(62, 13)
        Me.Label44.TabIndex = 42
        Me.Label44.Text = "Keterangan"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(16, 280)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(34, 13)
        Me.Label43.TabIndex = 41
        Me.Label43.Text = "Meter"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(104, 450)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(24, 13)
        Me.Label46.TabIndex = 44
        Me.Label46.Text = "Rp."
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(16, 425)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(62, 13)
        Me.Label42.TabIndex = 40
        Me.Label42.Text = "Claim Celup"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(16, 401)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(54, 13)
        Me.Label41.TabIndex = 39
        Me.Label41.Text = "Claim Jadi"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(16, 377)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(46, 13)
        Me.Label40.TabIndex = 38
        Me.Label40.Text = "Grade B"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(16, 353)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(46, 13)
        Me.Label39.TabIndex = 37
        Me.Label39.Text = "Grade A"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(16, 328)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(34, 13)
        Me.Label36.TabIndex = 34
        Me.Label36.Text = "Susut"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(16, 304)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(34, 13)
        Me.Label35.TabIndex = 33
        Me.Label35.Text = "Yards"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(16, 256)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(41, 13)
        Me.Label34.TabIndex = 32
        Me.Label34.Text = "Gulung"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(16, 232)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(39, 13)
        Me.Label33.TabIndex = 31
        Me.Label33.Text = "Warna"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(16, 474)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(63, 13)
        Me.Label38.TabIndex = 36
        Me.Label38.Text = "Total Harga"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(16, 208)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(55, 13)
        Me.Label32.TabIndex = 30
        Me.Label32.Text = "Jenis Kain"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(16, 184)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(51, 13)
        Me.Label31.TabIndex = 29
        Me.Label31.Text = "Customer"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(16, 450)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(36, 13)
        Me.Label37.TabIndex = 35
        Me.Label37.Text = "Harga"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(16, 160)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(48, 13)
        Me.Label30.TabIndex = 28
        Me.Label30.Text = "Baris PO"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(16, 136)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(39, 13)
        Me.Label29.TabIndex = 27
        Me.Label29.Text = "No PO"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(16, 112)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(45, 13)
        Me.Label28.TabIndex = 26
        Me.Label28.Text = "Baris SJ"
        '
        'txt_claim_celup
        '
        Me.txt_claim_celup.BackColor = System.Drawing.Color.Pink
        Me.txt_claim_celup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_claim_celup.Location = New System.Drawing.Point(104, 421)
        Me.txt_claim_celup.Name = "txt_claim_celup"
        Me.txt_claim_celup.ReadOnly = True
        Me.txt_claim_celup.Size = New System.Drawing.Size(90, 20)
        Me.txt_claim_celup.TabIndex = 25
        Me.txt_claim_celup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_grade_a
        '
        Me.txt_grade_a.BackColor = System.Drawing.SystemColors.Window
        Me.txt_grade_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_grade_a.Location = New System.Drawing.Point(104, 349)
        Me.txt_grade_a.Name = "txt_grade_a"
        Me.txt_grade_a.ReadOnly = True
        Me.txt_grade_a.Size = New System.Drawing.Size(90, 20)
        Me.txt_grade_a.TabIndex = 24
        Me.txt_grade_a.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_claim_jadi
        '
        Me.txt_claim_jadi.BackColor = System.Drawing.Color.Khaki
        Me.txt_claim_jadi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_claim_jadi.Location = New System.Drawing.Point(104, 397)
        Me.txt_claim_jadi.Name = "txt_claim_jadi"
        Me.txt_claim_jadi.ReadOnly = True
        Me.txt_claim_jadi.Size = New System.Drawing.Size(90, 20)
        Me.txt_claim_jadi.TabIndex = 21
        Me.txt_claim_jadi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_grade_b
        '
        Me.txt_grade_b.BackColor = System.Drawing.Color.LightSkyBlue
        Me.txt_grade_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_grade_b.Location = New System.Drawing.Point(104, 373)
        Me.txt_grade_b.Name = "txt_grade_b"
        Me.txt_grade_b.ReadOnly = True
        Me.txt_grade_b.Size = New System.Drawing.Size(90, 20)
        Me.txt_grade_b.TabIndex = 20
        Me.txt_grade_b.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_susut
        '
        Me.txt_susut.BackColor = System.Drawing.SystemColors.Window
        Me.txt_susut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_susut.Location = New System.Drawing.Point(104, 324)
        Me.txt_susut.Name = "txt_susut"
        Me.txt_susut.ReadOnly = True
        Me.txt_susut.Size = New System.Drawing.Size(90, 20)
        Me.txt_susut.TabIndex = 18
        Me.txt_susut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_yards
        '
        Me.txt_yards.BackColor = System.Drawing.SystemColors.Window
        Me.txt_yards.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_yards.Location = New System.Drawing.Point(104, 300)
        Me.txt_yards.Name = "txt_yards"
        Me.txt_yards.ReadOnly = True
        Me.txt_yards.Size = New System.Drawing.Size(90, 20)
        Me.txt_yards.TabIndex = 17
        Me.txt_yards.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_meter
        '
        Me.txt_meter.BackColor = System.Drawing.SystemColors.Window
        Me.txt_meter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter.Location = New System.Drawing.Point(104, 276)
        Me.txt_meter.Name = "txt_meter"
        Me.txt_meter.ReadOnly = True
        Me.txt_meter.Size = New System.Drawing.Size(90, 20)
        Me.txt_meter.TabIndex = 16
        Me.txt_meter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_warna
        '
        Me.txt_warna.BackColor = System.Drawing.SystemColors.Window
        Me.txt_warna.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna.Location = New System.Drawing.Point(104, 228)
        Me.txt_warna.Name = "txt_warna"
        Me.txt_warna.ReadOnly = True
        Me.txt_warna.Size = New System.Drawing.Size(209, 20)
        Me.txt_warna.TabIndex = 14
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain.Location = New System.Drawing.Point(104, 204)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.ReadOnly = True
        Me.txt_jenis_kain.Size = New System.Drawing.Size(209, 20)
        Me.txt_jenis_kain.TabIndex = 13
        '
        'txt_harga
        '
        Me.txt_harga.BackColor = System.Drawing.SystemColors.Window
        Me.txt_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga.Location = New System.Drawing.Point(129, 446)
        Me.txt_harga.Name = "txt_harga"
        Me.txt_harga.ReadOnly = True
        Me.txt_harga.Size = New System.Drawing.Size(184, 20)
        Me.txt_harga.TabIndex = 23
        '
        'txt_customer
        '
        Me.txt_customer.BackColor = System.Drawing.SystemColors.Window
        Me.txt_customer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer.Location = New System.Drawing.Point(104, 180)
        Me.txt_customer.Name = "txt_customer"
        Me.txt_customer.ReadOnly = True
        Me.txt_customer.Size = New System.Drawing.Size(209, 20)
        Me.txt_customer.TabIndex = 12
        '
        'txt_baris_po
        '
        Me.txt_baris_po.BackColor = System.Drawing.SystemColors.Window
        Me.txt_baris_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_baris_po.Location = New System.Drawing.Point(104, 156)
        Me.txt_baris_po.Name = "txt_baris_po"
        Me.txt_baris_po.ReadOnly = True
        Me.txt_baris_po.Size = New System.Drawing.Size(209, 20)
        Me.txt_baris_po.TabIndex = 11
        '
        'txt_total_harga
        '
        Me.txt_total_harga.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_harga.Location = New System.Drawing.Point(129, 470)
        Me.txt_total_harga.Name = "txt_total_harga"
        Me.txt_total_harga.ReadOnly = True
        Me.txt_total_harga.Size = New System.Drawing.Size(184, 20)
        Me.txt_total_harga.TabIndex = 19
        '
        'txt_no_po
        '
        Me.txt_no_po.BackColor = System.Drawing.SystemColors.Window
        Me.txt_no_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po.Location = New System.Drawing.Point(104, 132)
        Me.txt_no_po.Name = "txt_no_po"
        Me.txt_no_po.ReadOnly = True
        Me.txt_no_po.Size = New System.Drawing.Size(209, 20)
        Me.txt_no_po.TabIndex = 10
        '
        'txt_baris_sj
        '
        Me.txt_baris_sj.BackColor = System.Drawing.SystemColors.Window
        Me.txt_baris_sj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_baris_sj.Location = New System.Drawing.Point(104, 108)
        Me.txt_baris_sj.Name = "txt_baris_sj"
        Me.txt_baris_sj.ReadOnly = True
        Me.txt_baris_sj.Size = New System.Drawing.Size(209, 20)
        Me.txt_baris_sj.TabIndex = 9
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(16, 88)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(60, 13)
        Me.Label26.TabIndex = 8
        Me.Label26.Text = "Surat Jalan"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(16, 64)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(45, 13)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Gudang"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(16, 16)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(46, 13)
        Me.Label24.TabIndex = 6
        Me.Label24.Text = "Tanggal"
        '
        'dtp_awal
        '
        Me.dtp_awal.Enabled = False
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(104, 12)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 5
        '
        'txt_sj_packing
        '
        Me.txt_sj_packing.BackColor = System.Drawing.SystemColors.Window
        Me.txt_sj_packing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_sj_packing.Location = New System.Drawing.Point(104, 84)
        Me.txt_sj_packing.Name = "txt_sj_packing"
        Me.txt_sj_packing.ReadOnly = True
        Me.txt_sj_packing.Size = New System.Drawing.Size(209, 20)
        Me.txt_sj_packing.TabIndex = 0
        '
        'txt_gudang
        '
        Me.txt_gudang.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang.Location = New System.Drawing.Point(104, 60)
        Me.txt_gudang.Name = "txt_gudang"
        Me.txt_gudang.ReadOnly = True
        Me.txt_gudang.Size = New System.Drawing.Size(209, 20)
        Me.txt_gudang.TabIndex = 4
        '
        'txt_keterangan
        '
        Me.txt_keterangan.BackColor = System.Drawing.SystemColors.Window
        Me.txt_keterangan.Location = New System.Drawing.Point(104, 496)
        Me.txt_keterangan.Name = "txt_keterangan"
        Me.txt_keterangan.ReadOnly = True
        Me.txt_keterangan.Size = New System.Drawing.Size(209, 41)
        Me.txt_keterangan.TabIndex = 60
        Me.txt_keterangan.Text = ""
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label61)
        Me.Panel3.Controls.Add(Me.S25)
        Me.Panel3.Controls.Add(Me.S21)
        Me.Panel3.Controls.Add(Me.S23)
        Me.Panel3.Controls.Add(Me.S24)
        Me.Panel3.Controls.Add(Me.S22)
        Me.Panel3.Controls.Add(Me.S20)
        Me.Panel3.Controls.Add(Me.S16)
        Me.Panel3.Controls.Add(Me.S8)
        Me.Panel3.Controls.Add(Me.S12)
        Me.Panel3.Controls.Add(Me.S4)
        Me.Panel3.Controls.Add(Me.S18)
        Me.Panel3.Controls.Add(Me.S14)
        Me.Panel3.Controls.Add(Me.S6)
        Me.Panel3.Controls.Add(Me.S10)
        Me.Panel3.Controls.Add(Me.S2)
        Me.Panel3.Controls.Add(Me.S19)
        Me.Panel3.Controls.Add(Me.S15)
        Me.Panel3.Controls.Add(Me.S7)
        Me.Panel3.Controls.Add(Me.S11)
        Me.Panel3.Controls.Add(Me.S3)
        Me.Panel3.Controls.Add(Me.S17)
        Me.Panel3.Controls.Add(Me.S13)
        Me.Panel3.Controls.Add(Me.S5)
        Me.Panel3.Controls.Add(Me.S9)
        Me.Panel3.Controls.Add(Me.S1)
        Me.Panel3.Controls.Add(Me.Label27)
        Me.Panel3.Controls.Add(Me.Label57)
        Me.Panel3.Controls.Add(Me.Label58)
        Me.Panel3.Controls.Add(Me.Label59)
        Me.Panel3.Controls.Add(Me.Label60)
        Me.Panel3.Controls.Add(Me.E25)
        Me.Panel3.Controls.Add(Me.D25)
        Me.Panel3.Controls.Add(Me.C25)
        Me.Panel3.Controls.Add(Me.B25)
        Me.Panel3.Controls.Add(Me.A25)
        Me.Panel3.Controls.Add(Me.K25)
        Me.Panel3.Controls.Add(Me.Y25)
        Me.Panel3.Controls.Add(Me.M25)
        Me.Panel3.Controls.Add(Me.E21)
        Me.Panel3.Controls.Add(Me.D21)
        Me.Panel3.Controls.Add(Me.C21)
        Me.Panel3.Controls.Add(Me.B21)
        Me.Panel3.Controls.Add(Me.A21)
        Me.Panel3.Controls.Add(Me.K21)
        Me.Panel3.Controls.Add(Me.Y21)
        Me.Panel3.Controls.Add(Me.M21)
        Me.Panel3.Controls.Add(Me.E23)
        Me.Panel3.Controls.Add(Me.D23)
        Me.Panel3.Controls.Add(Me.C23)
        Me.Panel3.Controls.Add(Me.B23)
        Me.Panel3.Controls.Add(Me.A23)
        Me.Panel3.Controls.Add(Me.K23)
        Me.Panel3.Controls.Add(Me.Y23)
        Me.Panel3.Controls.Add(Me.M23)
        Me.Panel3.Controls.Add(Me.E24)
        Me.Panel3.Controls.Add(Me.D24)
        Me.Panel3.Controls.Add(Me.C24)
        Me.Panel3.Controls.Add(Me.B24)
        Me.Panel3.Controls.Add(Me.A24)
        Me.Panel3.Controls.Add(Me.K24)
        Me.Panel3.Controls.Add(Me.Y24)
        Me.Panel3.Controls.Add(Me.M24)
        Me.Panel3.Controls.Add(Me.E22)
        Me.Panel3.Controls.Add(Me.D22)
        Me.Panel3.Controls.Add(Me.C22)
        Me.Panel3.Controls.Add(Me.B22)
        Me.Panel3.Controls.Add(Me.A22)
        Me.Panel3.Controls.Add(Me.K22)
        Me.Panel3.Controls.Add(Me.Y22)
        Me.Panel3.Controls.Add(Me.M22)
        Me.Panel3.Controls.Add(Me.Label22)
        Me.Panel3.Controls.Add(Me.Label23)
        Me.Panel3.Controls.Add(Me.Label21)
        Me.Panel3.Controls.Add(Me.Label20)
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label19)
        Me.Panel3.Controls.Add(Me.Label14)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label17)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.E20)
        Me.Panel3.Controls.Add(Me.D20)
        Me.Panel3.Controls.Add(Me.C20)
        Me.Panel3.Controls.Add(Me.B20)
        Me.Panel3.Controls.Add(Me.A20)
        Me.Panel3.Controls.Add(Me.K20)
        Me.Panel3.Controls.Add(Me.Y20)
        Me.Panel3.Controls.Add(Me.M20)
        Me.Panel3.Controls.Add(Me.E16)
        Me.Panel3.Controls.Add(Me.D16)
        Me.Panel3.Controls.Add(Me.C16)
        Me.Panel3.Controls.Add(Me.B16)
        Me.Panel3.Controls.Add(Me.A16)
        Me.Panel3.Controls.Add(Me.K16)
        Me.Panel3.Controls.Add(Me.Y16)
        Me.Panel3.Controls.Add(Me.M16)
        Me.Panel3.Controls.Add(Me.E8)
        Me.Panel3.Controls.Add(Me.D8)
        Me.Panel3.Controls.Add(Me.C8)
        Me.Panel3.Controls.Add(Me.B8)
        Me.Panel3.Controls.Add(Me.A8)
        Me.Panel3.Controls.Add(Me.K8)
        Me.Panel3.Controls.Add(Me.Y8)
        Me.Panel3.Controls.Add(Me.M8)
        Me.Panel3.Controls.Add(Me.E12)
        Me.Panel3.Controls.Add(Me.D12)
        Me.Panel3.Controls.Add(Me.C12)
        Me.Panel3.Controls.Add(Me.B12)
        Me.Panel3.Controls.Add(Me.A12)
        Me.Panel3.Controls.Add(Me.K12)
        Me.Panel3.Controls.Add(Me.Y12)
        Me.Panel3.Controls.Add(Me.M12)
        Me.Panel3.Controls.Add(Me.E4)
        Me.Panel3.Controls.Add(Me.D4)
        Me.Panel3.Controls.Add(Me.C4)
        Me.Panel3.Controls.Add(Me.B4)
        Me.Panel3.Controls.Add(Me.A4)
        Me.Panel3.Controls.Add(Me.K4)
        Me.Panel3.Controls.Add(Me.Y4)
        Me.Panel3.Controls.Add(Me.M4)
        Me.Panel3.Controls.Add(Me.E18)
        Me.Panel3.Controls.Add(Me.D18)
        Me.Panel3.Controls.Add(Me.C18)
        Me.Panel3.Controls.Add(Me.B18)
        Me.Panel3.Controls.Add(Me.A18)
        Me.Panel3.Controls.Add(Me.K18)
        Me.Panel3.Controls.Add(Me.Y18)
        Me.Panel3.Controls.Add(Me.M18)
        Me.Panel3.Controls.Add(Me.E14)
        Me.Panel3.Controls.Add(Me.D14)
        Me.Panel3.Controls.Add(Me.C14)
        Me.Panel3.Controls.Add(Me.B14)
        Me.Panel3.Controls.Add(Me.A14)
        Me.Panel3.Controls.Add(Me.K14)
        Me.Panel3.Controls.Add(Me.Y14)
        Me.Panel3.Controls.Add(Me.M14)
        Me.Panel3.Controls.Add(Me.E6)
        Me.Panel3.Controls.Add(Me.D6)
        Me.Panel3.Controls.Add(Me.C6)
        Me.Panel3.Controls.Add(Me.B6)
        Me.Panel3.Controls.Add(Me.A6)
        Me.Panel3.Controls.Add(Me.K6)
        Me.Panel3.Controls.Add(Me.Y6)
        Me.Panel3.Controls.Add(Me.M6)
        Me.Panel3.Controls.Add(Me.E10)
        Me.Panel3.Controls.Add(Me.D10)
        Me.Panel3.Controls.Add(Me.C10)
        Me.Panel3.Controls.Add(Me.B10)
        Me.Panel3.Controls.Add(Me.A10)
        Me.Panel3.Controls.Add(Me.K10)
        Me.Panel3.Controls.Add(Me.Y10)
        Me.Panel3.Controls.Add(Me.M10)
        Me.Panel3.Controls.Add(Me.E2)
        Me.Panel3.Controls.Add(Me.D2)
        Me.Panel3.Controls.Add(Me.C2)
        Me.Panel3.Controls.Add(Me.B2)
        Me.Panel3.Controls.Add(Me.A2)
        Me.Panel3.Controls.Add(Me.K2)
        Me.Panel3.Controls.Add(Me.Y2)
        Me.Panel3.Controls.Add(Me.M2)
        Me.Panel3.Controls.Add(Me.E19)
        Me.Panel3.Controls.Add(Me.D19)
        Me.Panel3.Controls.Add(Me.C19)
        Me.Panel3.Controls.Add(Me.B19)
        Me.Panel3.Controls.Add(Me.A19)
        Me.Panel3.Controls.Add(Me.K19)
        Me.Panel3.Controls.Add(Me.Y19)
        Me.Panel3.Controls.Add(Me.M19)
        Me.Panel3.Controls.Add(Me.E15)
        Me.Panel3.Controls.Add(Me.D15)
        Me.Panel3.Controls.Add(Me.C15)
        Me.Panel3.Controls.Add(Me.B15)
        Me.Panel3.Controls.Add(Me.A15)
        Me.Panel3.Controls.Add(Me.K15)
        Me.Panel3.Controls.Add(Me.Y15)
        Me.Panel3.Controls.Add(Me.M15)
        Me.Panel3.Controls.Add(Me.E7)
        Me.Panel3.Controls.Add(Me.D7)
        Me.Panel3.Controls.Add(Me.C7)
        Me.Panel3.Controls.Add(Me.B7)
        Me.Panel3.Controls.Add(Me.A7)
        Me.Panel3.Controls.Add(Me.K7)
        Me.Panel3.Controls.Add(Me.Y7)
        Me.Panel3.Controls.Add(Me.M7)
        Me.Panel3.Controls.Add(Me.E11)
        Me.Panel3.Controls.Add(Me.D11)
        Me.Panel3.Controls.Add(Me.C11)
        Me.Panel3.Controls.Add(Me.B11)
        Me.Panel3.Controls.Add(Me.A11)
        Me.Panel3.Controls.Add(Me.K11)
        Me.Panel3.Controls.Add(Me.Y11)
        Me.Panel3.Controls.Add(Me.M11)
        Me.Panel3.Controls.Add(Me.E3)
        Me.Panel3.Controls.Add(Me.D3)
        Me.Panel3.Controls.Add(Me.C3)
        Me.Panel3.Controls.Add(Me.B3)
        Me.Panel3.Controls.Add(Me.A3)
        Me.Panel3.Controls.Add(Me.K3)
        Me.Panel3.Controls.Add(Me.Y3)
        Me.Panel3.Controls.Add(Me.M3)
        Me.Panel3.Controls.Add(Me.E17)
        Me.Panel3.Controls.Add(Me.D17)
        Me.Panel3.Controls.Add(Me.C17)
        Me.Panel3.Controls.Add(Me.B17)
        Me.Panel3.Controls.Add(Me.A17)
        Me.Panel3.Controls.Add(Me.K17)
        Me.Panel3.Controls.Add(Me.Y17)
        Me.Panel3.Controls.Add(Me.M17)
        Me.Panel3.Controls.Add(Me.E13)
        Me.Panel3.Controls.Add(Me.D13)
        Me.Panel3.Controls.Add(Me.C13)
        Me.Panel3.Controls.Add(Me.B13)
        Me.Panel3.Controls.Add(Me.A13)
        Me.Panel3.Controls.Add(Me.K13)
        Me.Panel3.Controls.Add(Me.Y13)
        Me.Panel3.Controls.Add(Me.M13)
        Me.Panel3.Controls.Add(Me.E5)
        Me.Panel3.Controls.Add(Me.D5)
        Me.Panel3.Controls.Add(Me.C5)
        Me.Panel3.Controls.Add(Me.B5)
        Me.Panel3.Controls.Add(Me.A5)
        Me.Panel3.Controls.Add(Me.K5)
        Me.Panel3.Controls.Add(Me.Y5)
        Me.Panel3.Controls.Add(Me.M5)
        Me.Panel3.Controls.Add(Me.E9)
        Me.Panel3.Controls.Add(Me.D9)
        Me.Panel3.Controls.Add(Me.C9)
        Me.Panel3.Controls.Add(Me.B9)
        Me.Panel3.Controls.Add(Me.A9)
        Me.Panel3.Controls.Add(Me.K9)
        Me.Panel3.Controls.Add(Me.Y9)
        Me.Panel3.Controls.Add(Me.M9)
        Me.Panel3.Controls.Add(Me.E1)
        Me.Panel3.Controls.Add(Me.D1)
        Me.Panel3.Controls.Add(Me.C1)
        Me.Panel3.Controls.Add(Me.B1)
        Me.Panel3.Controls.Add(Me.A1)
        Me.Panel3.Controls.Add(Me.K1)
        Me.Panel3.Controls.Add(Me.Y1)
        Me.Panel3.Controls.Add(Me.M1)
        Me.Panel3.Location = New System.Drawing.Point(336, 29)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(699, 608)
        Me.Panel3.TabIndex = 87
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(628, 7)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(44, 13)
        Me.Label61.TabIndex = 109
        Me.Label61.Text = "SUSUT"
        '
        'S25
        '
        Me.S25.BackColor = System.Drawing.SystemColors.Window
        Me.S25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S25.Location = New System.Drawing.Point(620, 579)
        Me.S25.Name = "S25"
        Me.S25.ReadOnly = True
        Me.S25.Size = New System.Drawing.Size(61, 20)
        Me.S25.TabIndex = 68
        Me.S25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S21
        '
        Me.S21.BackColor = System.Drawing.SystemColors.Window
        Me.S21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S21.Location = New System.Drawing.Point(620, 487)
        Me.S21.Name = "S21"
        Me.S21.ReadOnly = True
        Me.S21.Size = New System.Drawing.Size(61, 20)
        Me.S21.TabIndex = 69
        Me.S21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S23
        '
        Me.S23.BackColor = System.Drawing.SystemColors.Window
        Me.S23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S23.Location = New System.Drawing.Point(620, 533)
        Me.S23.Name = "S23"
        Me.S23.ReadOnly = True
        Me.S23.Size = New System.Drawing.Size(61, 20)
        Me.S23.TabIndex = 70
        Me.S23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S24
        '
        Me.S24.BackColor = System.Drawing.SystemColors.Window
        Me.S24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S24.Location = New System.Drawing.Point(620, 556)
        Me.S24.Name = "S24"
        Me.S24.ReadOnly = True
        Me.S24.Size = New System.Drawing.Size(61, 20)
        Me.S24.TabIndex = 71
        Me.S24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S22
        '
        Me.S22.BackColor = System.Drawing.SystemColors.Window
        Me.S22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S22.Location = New System.Drawing.Point(620, 510)
        Me.S22.Name = "S22"
        Me.S22.ReadOnly = True
        Me.S22.Size = New System.Drawing.Size(61, 20)
        Me.S22.TabIndex = 72
        Me.S22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S20
        '
        Me.S20.BackColor = System.Drawing.SystemColors.Window
        Me.S20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S20.Location = New System.Drawing.Point(620, 464)
        Me.S20.Name = "S20"
        Me.S20.ReadOnly = True
        Me.S20.Size = New System.Drawing.Size(61, 20)
        Me.S20.TabIndex = 48
        Me.S20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S16
        '
        Me.S16.BackColor = System.Drawing.SystemColors.Window
        Me.S16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S16.Location = New System.Drawing.Point(620, 372)
        Me.S16.Name = "S16"
        Me.S16.ReadOnly = True
        Me.S16.Size = New System.Drawing.Size(61, 20)
        Me.S16.TabIndex = 65
        Me.S16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S8
        '
        Me.S8.BackColor = System.Drawing.SystemColors.Window
        Me.S8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S8.Location = New System.Drawing.Point(620, 188)
        Me.S8.Name = "S8"
        Me.S8.ReadOnly = True
        Me.S8.Size = New System.Drawing.Size(61, 20)
        Me.S8.TabIndex = 64
        Me.S8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S12
        '
        Me.S12.BackColor = System.Drawing.SystemColors.Window
        Me.S12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S12.Location = New System.Drawing.Point(620, 280)
        Me.S12.Name = "S12"
        Me.S12.ReadOnly = True
        Me.S12.Size = New System.Drawing.Size(61, 20)
        Me.S12.TabIndex = 63
        Me.S12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S4
        '
        Me.S4.BackColor = System.Drawing.SystemColors.Window
        Me.S4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S4.Location = New System.Drawing.Point(620, 96)
        Me.S4.Name = "S4"
        Me.S4.ReadOnly = True
        Me.S4.Size = New System.Drawing.Size(61, 20)
        Me.S4.TabIndex = 62
        Me.S4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S18
        '
        Me.S18.BackColor = System.Drawing.SystemColors.Window
        Me.S18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S18.Location = New System.Drawing.Point(620, 418)
        Me.S18.Name = "S18"
        Me.S18.ReadOnly = True
        Me.S18.Size = New System.Drawing.Size(61, 20)
        Me.S18.TabIndex = 61
        Me.S18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S14
        '
        Me.S14.BackColor = System.Drawing.SystemColors.Window
        Me.S14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S14.Location = New System.Drawing.Point(620, 326)
        Me.S14.Name = "S14"
        Me.S14.ReadOnly = True
        Me.S14.Size = New System.Drawing.Size(61, 20)
        Me.S14.TabIndex = 60
        Me.S14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S6
        '
        Me.S6.BackColor = System.Drawing.SystemColors.Window
        Me.S6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S6.Location = New System.Drawing.Point(620, 142)
        Me.S6.Name = "S6"
        Me.S6.ReadOnly = True
        Me.S6.Size = New System.Drawing.Size(61, 20)
        Me.S6.TabIndex = 59
        Me.S6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S10
        '
        Me.S10.BackColor = System.Drawing.SystemColors.Window
        Me.S10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S10.Location = New System.Drawing.Point(620, 234)
        Me.S10.Name = "S10"
        Me.S10.ReadOnly = True
        Me.S10.Size = New System.Drawing.Size(61, 20)
        Me.S10.TabIndex = 58
        Me.S10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S2
        '
        Me.S2.BackColor = System.Drawing.SystemColors.Window
        Me.S2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S2.Location = New System.Drawing.Point(620, 50)
        Me.S2.Name = "S2"
        Me.S2.ReadOnly = True
        Me.S2.Size = New System.Drawing.Size(61, 20)
        Me.S2.TabIndex = 57
        Me.S2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S19
        '
        Me.S19.BackColor = System.Drawing.SystemColors.Window
        Me.S19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S19.Location = New System.Drawing.Point(620, 441)
        Me.S19.Name = "S19"
        Me.S19.ReadOnly = True
        Me.S19.Size = New System.Drawing.Size(61, 20)
        Me.S19.TabIndex = 56
        Me.S19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S15
        '
        Me.S15.BackColor = System.Drawing.SystemColors.Window
        Me.S15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S15.Location = New System.Drawing.Point(620, 349)
        Me.S15.Name = "S15"
        Me.S15.ReadOnly = True
        Me.S15.Size = New System.Drawing.Size(61, 20)
        Me.S15.TabIndex = 55
        Me.S15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S7
        '
        Me.S7.BackColor = System.Drawing.SystemColors.Window
        Me.S7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S7.Location = New System.Drawing.Point(620, 165)
        Me.S7.Name = "S7"
        Me.S7.ReadOnly = True
        Me.S7.Size = New System.Drawing.Size(61, 20)
        Me.S7.TabIndex = 54
        Me.S7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S11
        '
        Me.S11.BackColor = System.Drawing.SystemColors.Window
        Me.S11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S11.Location = New System.Drawing.Point(620, 257)
        Me.S11.Name = "S11"
        Me.S11.ReadOnly = True
        Me.S11.Size = New System.Drawing.Size(61, 20)
        Me.S11.TabIndex = 53
        Me.S11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S3
        '
        Me.S3.BackColor = System.Drawing.SystemColors.Window
        Me.S3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S3.Location = New System.Drawing.Point(620, 73)
        Me.S3.Name = "S3"
        Me.S3.ReadOnly = True
        Me.S3.Size = New System.Drawing.Size(61, 20)
        Me.S3.TabIndex = 52
        Me.S3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S17
        '
        Me.S17.BackColor = System.Drawing.SystemColors.Window
        Me.S17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S17.Location = New System.Drawing.Point(620, 395)
        Me.S17.Name = "S17"
        Me.S17.ReadOnly = True
        Me.S17.Size = New System.Drawing.Size(61, 20)
        Me.S17.TabIndex = 51
        Me.S17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S13
        '
        Me.S13.BackColor = System.Drawing.SystemColors.Window
        Me.S13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S13.Location = New System.Drawing.Point(620, 303)
        Me.S13.Name = "S13"
        Me.S13.ReadOnly = True
        Me.S13.Size = New System.Drawing.Size(61, 20)
        Me.S13.TabIndex = 50
        Me.S13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S5
        '
        Me.S5.BackColor = System.Drawing.SystemColors.Window
        Me.S5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S5.Location = New System.Drawing.Point(620, 119)
        Me.S5.Name = "S5"
        Me.S5.ReadOnly = True
        Me.S5.Size = New System.Drawing.Size(61, 20)
        Me.S5.TabIndex = 49
        Me.S5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S9
        '
        Me.S9.BackColor = System.Drawing.SystemColors.Window
        Me.S9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S9.Location = New System.Drawing.Point(620, 211)
        Me.S9.Name = "S9"
        Me.S9.ReadOnly = True
        Me.S9.Size = New System.Drawing.Size(61, 20)
        Me.S9.TabIndex = 66
        Me.S9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'S1
        '
        Me.S1.BackColor = System.Drawing.SystemColors.Window
        Me.S1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.S1.Location = New System.Drawing.Point(620, 27)
        Me.S1.Name = "S1"
        Me.S1.ReadOnly = True
        Me.S1.Size = New System.Drawing.Size(61, 20)
        Me.S1.TabIndex = 67
        Me.S1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(16, 583)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(22, 13)
        Me.Label27.TabIndex = 47
        Me.Label27.Text = "25."
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(16, 560)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(22, 13)
        Me.Label57.TabIndex = 46
        Me.Label57.Text = "24."
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(16, 537)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(22, 13)
        Me.Label58.TabIndex = 45
        Me.Label58.Text = "23."
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(16, 514)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(22, 13)
        Me.Label59.TabIndex = 44
        Me.Label59.Text = "22."
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(16, 491)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(22, 13)
        Me.Label60.TabIndex = 43
        Me.Label60.Text = "21."
        '
        'E25
        '
        Me.E25.BackColor = System.Drawing.SystemColors.Window
        Me.E25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E25.Location = New System.Drawing.Point(499, 579)
        Me.E25.MaxLength = 6
        Me.E25.Name = "E25"
        Me.E25.ReadOnly = True
        Me.E25.Size = New System.Drawing.Size(35, 20)
        Me.E25.TabIndex = 27
        Me.E25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D25
        '
        Me.D25.BackColor = System.Drawing.SystemColors.Window
        Me.D25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D25.Location = New System.Drawing.Point(458, 579)
        Me.D25.MaxLength = 6
        Me.D25.Name = "D25"
        Me.D25.ReadOnly = True
        Me.D25.Size = New System.Drawing.Size(35, 20)
        Me.D25.TabIndex = 28
        Me.D25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C25
        '
        Me.C25.BackColor = System.Drawing.SystemColors.Window
        Me.C25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C25.Location = New System.Drawing.Point(417, 579)
        Me.C25.MaxLength = 6
        Me.C25.Name = "C25"
        Me.C25.ReadOnly = True
        Me.C25.Size = New System.Drawing.Size(35, 20)
        Me.C25.TabIndex = 29
        Me.C25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B25
        '
        Me.B25.BackColor = System.Drawing.SystemColors.Window
        Me.B25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B25.Location = New System.Drawing.Point(376, 579)
        Me.B25.MaxLength = 6
        Me.B25.Name = "B25"
        Me.B25.ReadOnly = True
        Me.B25.Size = New System.Drawing.Size(35, 20)
        Me.B25.TabIndex = 30
        Me.B25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A25
        '
        Me.A25.BackColor = System.Drawing.SystemColors.Window
        Me.A25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A25.Location = New System.Drawing.Point(335, 579)
        Me.A25.MaxLength = 6
        Me.A25.Name = "A25"
        Me.A25.ReadOnly = True
        Me.A25.Size = New System.Drawing.Size(35, 20)
        Me.A25.TabIndex = 31
        Me.A25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K25
        '
        Me.K25.BackColor = System.Drawing.SystemColors.Window
        Me.K25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K25.Location = New System.Drawing.Point(133, 579)
        Me.K25.MaxLength = 50
        Me.K25.Name = "K25"
        Me.K25.ReadOnly = True
        Me.K25.Size = New System.Drawing.Size(196, 20)
        Me.K25.TabIndex = 34
        '
        'Y25
        '
        Me.Y25.BackColor = System.Drawing.SystemColors.Window
        Me.Y25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y25.Location = New System.Drawing.Point(553, 579)
        Me.Y25.Name = "Y25"
        Me.Y25.ReadOnly = True
        Me.Y25.Size = New System.Drawing.Size(61, 20)
        Me.Y25.TabIndex = 33
        Me.Y25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M25
        '
        Me.M25.BackColor = System.Drawing.SystemColors.Window
        Me.M25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M25.Location = New System.Drawing.Point(50, 579)
        Me.M25.MaxLength = 7
        Me.M25.Name = "M25"
        Me.M25.ReadOnly = True
        Me.M25.Size = New System.Drawing.Size(61, 20)
        Me.M25.TabIndex = 42
        Me.M25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E21
        '
        Me.E21.BackColor = System.Drawing.SystemColors.Window
        Me.E21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E21.Location = New System.Drawing.Point(499, 487)
        Me.E21.MaxLength = 6
        Me.E21.Name = "E21"
        Me.E21.ReadOnly = True
        Me.E21.Size = New System.Drawing.Size(35, 20)
        Me.E21.TabIndex = 35
        Me.E21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D21
        '
        Me.D21.BackColor = System.Drawing.SystemColors.Window
        Me.D21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D21.Location = New System.Drawing.Point(458, 487)
        Me.D21.MaxLength = 6
        Me.D21.Name = "D21"
        Me.D21.ReadOnly = True
        Me.D21.Size = New System.Drawing.Size(35, 20)
        Me.D21.TabIndex = 36
        Me.D21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C21
        '
        Me.C21.BackColor = System.Drawing.SystemColors.Window
        Me.C21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C21.Location = New System.Drawing.Point(417, 487)
        Me.C21.MaxLength = 6
        Me.C21.Name = "C21"
        Me.C21.ReadOnly = True
        Me.C21.Size = New System.Drawing.Size(35, 20)
        Me.C21.TabIndex = 37
        Me.C21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B21
        '
        Me.B21.BackColor = System.Drawing.SystemColors.Window
        Me.B21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B21.Location = New System.Drawing.Point(376, 487)
        Me.B21.MaxLength = 6
        Me.B21.Name = "B21"
        Me.B21.ReadOnly = True
        Me.B21.Size = New System.Drawing.Size(35, 20)
        Me.B21.TabIndex = 38
        Me.B21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A21
        '
        Me.A21.BackColor = System.Drawing.SystemColors.Window
        Me.A21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A21.Location = New System.Drawing.Point(335, 487)
        Me.A21.MaxLength = 6
        Me.A21.Name = "A21"
        Me.A21.ReadOnly = True
        Me.A21.Size = New System.Drawing.Size(35, 20)
        Me.A21.TabIndex = 39
        Me.A21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K21
        '
        Me.K21.BackColor = System.Drawing.SystemColors.Window
        Me.K21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K21.Location = New System.Drawing.Point(133, 487)
        Me.K21.MaxLength = 50
        Me.K21.Name = "K21"
        Me.K21.ReadOnly = True
        Me.K21.Size = New System.Drawing.Size(196, 20)
        Me.K21.TabIndex = 40
        '
        'Y21
        '
        Me.Y21.BackColor = System.Drawing.SystemColors.Window
        Me.Y21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y21.Location = New System.Drawing.Point(553, 487)
        Me.Y21.Name = "Y21"
        Me.Y21.ReadOnly = True
        Me.Y21.Size = New System.Drawing.Size(61, 20)
        Me.Y21.TabIndex = 41
        Me.Y21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M21
        '
        Me.M21.BackColor = System.Drawing.SystemColors.Window
        Me.M21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M21.Location = New System.Drawing.Point(50, 487)
        Me.M21.MaxLength = 7
        Me.M21.Name = "M21"
        Me.M21.ReadOnly = True
        Me.M21.Size = New System.Drawing.Size(61, 20)
        Me.M21.TabIndex = 26
        Me.M21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E23
        '
        Me.E23.BackColor = System.Drawing.SystemColors.Window
        Me.E23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E23.Location = New System.Drawing.Point(499, 533)
        Me.E23.MaxLength = 6
        Me.E23.Name = "E23"
        Me.E23.ReadOnly = True
        Me.E23.Size = New System.Drawing.Size(35, 20)
        Me.E23.TabIndex = 32
        Me.E23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D23
        '
        Me.D23.BackColor = System.Drawing.SystemColors.Window
        Me.D23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D23.Location = New System.Drawing.Point(458, 533)
        Me.D23.MaxLength = 6
        Me.D23.Name = "D23"
        Me.D23.ReadOnly = True
        Me.D23.Size = New System.Drawing.Size(35, 20)
        Me.D23.TabIndex = 3
        Me.D23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C23
        '
        Me.C23.BackColor = System.Drawing.SystemColors.Window
        Me.C23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C23.Location = New System.Drawing.Point(417, 533)
        Me.C23.MaxLength = 6
        Me.C23.Name = "C23"
        Me.C23.ReadOnly = True
        Me.C23.Size = New System.Drawing.Size(35, 20)
        Me.C23.TabIndex = 23
        Me.C23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B23
        '
        Me.B23.BackColor = System.Drawing.SystemColors.Window
        Me.B23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B23.Location = New System.Drawing.Point(376, 533)
        Me.B23.MaxLength = 6
        Me.B23.Name = "B23"
        Me.B23.ReadOnly = True
        Me.B23.Size = New System.Drawing.Size(35, 20)
        Me.B23.TabIndex = 4
        Me.B23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A23
        '
        Me.A23.BackColor = System.Drawing.SystemColors.Window
        Me.A23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A23.Location = New System.Drawing.Point(335, 533)
        Me.A23.MaxLength = 6
        Me.A23.Name = "A23"
        Me.A23.ReadOnly = True
        Me.A23.Size = New System.Drawing.Size(35, 20)
        Me.A23.TabIndex = 5
        Me.A23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K23
        '
        Me.K23.BackColor = System.Drawing.SystemColors.Window
        Me.K23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K23.Location = New System.Drawing.Point(133, 533)
        Me.K23.MaxLength = 50
        Me.K23.Name = "K23"
        Me.K23.ReadOnly = True
        Me.K23.Size = New System.Drawing.Size(196, 20)
        Me.K23.TabIndex = 6
        '
        'Y23
        '
        Me.Y23.BackColor = System.Drawing.SystemColors.Window
        Me.Y23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y23.Location = New System.Drawing.Point(553, 533)
        Me.Y23.Name = "Y23"
        Me.Y23.ReadOnly = True
        Me.Y23.Size = New System.Drawing.Size(61, 20)
        Me.Y23.TabIndex = 7
        Me.Y23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M23
        '
        Me.M23.BackColor = System.Drawing.SystemColors.Window
        Me.M23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M23.Location = New System.Drawing.Point(50, 533)
        Me.M23.MaxLength = 7
        Me.M23.Name = "M23"
        Me.M23.ReadOnly = True
        Me.M23.Size = New System.Drawing.Size(61, 20)
        Me.M23.TabIndex = 8
        Me.M23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E24
        '
        Me.E24.BackColor = System.Drawing.SystemColors.Window
        Me.E24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E24.Location = New System.Drawing.Point(499, 556)
        Me.E24.MaxLength = 6
        Me.E24.Name = "E24"
        Me.E24.ReadOnly = True
        Me.E24.Size = New System.Drawing.Size(35, 20)
        Me.E24.TabIndex = 9
        Me.E24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D24
        '
        Me.D24.BackColor = System.Drawing.SystemColors.Window
        Me.D24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D24.Location = New System.Drawing.Point(458, 556)
        Me.D24.MaxLength = 6
        Me.D24.Name = "D24"
        Me.D24.ReadOnly = True
        Me.D24.Size = New System.Drawing.Size(35, 20)
        Me.D24.TabIndex = 10
        Me.D24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C24
        '
        Me.C24.BackColor = System.Drawing.SystemColors.Window
        Me.C24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C24.Location = New System.Drawing.Point(417, 556)
        Me.C24.MaxLength = 6
        Me.C24.Name = "C24"
        Me.C24.ReadOnly = True
        Me.C24.Size = New System.Drawing.Size(35, 20)
        Me.C24.TabIndex = 11
        Me.C24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B24
        '
        Me.B24.BackColor = System.Drawing.SystemColors.Window
        Me.B24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B24.Location = New System.Drawing.Point(376, 556)
        Me.B24.MaxLength = 6
        Me.B24.Name = "B24"
        Me.B24.ReadOnly = True
        Me.B24.Size = New System.Drawing.Size(35, 20)
        Me.B24.TabIndex = 12
        Me.B24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A24
        '
        Me.A24.BackColor = System.Drawing.SystemColors.Window
        Me.A24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A24.Location = New System.Drawing.Point(335, 556)
        Me.A24.MaxLength = 6
        Me.A24.Name = "A24"
        Me.A24.ReadOnly = True
        Me.A24.Size = New System.Drawing.Size(35, 20)
        Me.A24.TabIndex = 13
        Me.A24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K24
        '
        Me.K24.BackColor = System.Drawing.SystemColors.Window
        Me.K24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K24.Location = New System.Drawing.Point(133, 556)
        Me.K24.MaxLength = 50
        Me.K24.Name = "K24"
        Me.K24.ReadOnly = True
        Me.K24.Size = New System.Drawing.Size(196, 20)
        Me.K24.TabIndex = 14
        '
        'Y24
        '
        Me.Y24.BackColor = System.Drawing.SystemColors.Window
        Me.Y24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y24.Location = New System.Drawing.Point(553, 556)
        Me.Y24.Name = "Y24"
        Me.Y24.ReadOnly = True
        Me.Y24.Size = New System.Drawing.Size(61, 20)
        Me.Y24.TabIndex = 15
        Me.Y24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M24
        '
        Me.M24.BackColor = System.Drawing.SystemColors.Window
        Me.M24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M24.Location = New System.Drawing.Point(50, 556)
        Me.M24.MaxLength = 7
        Me.M24.Name = "M24"
        Me.M24.ReadOnly = True
        Me.M24.Size = New System.Drawing.Size(61, 20)
        Me.M24.TabIndex = 16
        Me.M24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E22
        '
        Me.E22.BackColor = System.Drawing.SystemColors.Window
        Me.E22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E22.Location = New System.Drawing.Point(499, 510)
        Me.E22.MaxLength = 6
        Me.E22.Name = "E22"
        Me.E22.ReadOnly = True
        Me.E22.Size = New System.Drawing.Size(35, 20)
        Me.E22.TabIndex = 17
        Me.E22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D22
        '
        Me.D22.BackColor = System.Drawing.SystemColors.Window
        Me.D22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D22.Location = New System.Drawing.Point(458, 510)
        Me.D22.MaxLength = 6
        Me.D22.Name = "D22"
        Me.D22.ReadOnly = True
        Me.D22.Size = New System.Drawing.Size(35, 20)
        Me.D22.TabIndex = 18
        Me.D22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C22
        '
        Me.C22.BackColor = System.Drawing.SystemColors.Window
        Me.C22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C22.Location = New System.Drawing.Point(417, 510)
        Me.C22.MaxLength = 6
        Me.C22.Name = "C22"
        Me.C22.ReadOnly = True
        Me.C22.Size = New System.Drawing.Size(35, 20)
        Me.C22.TabIndex = 19
        Me.C22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B22
        '
        Me.B22.BackColor = System.Drawing.SystemColors.Window
        Me.B22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B22.Location = New System.Drawing.Point(376, 510)
        Me.B22.MaxLength = 6
        Me.B22.Name = "B22"
        Me.B22.ReadOnly = True
        Me.B22.Size = New System.Drawing.Size(35, 20)
        Me.B22.TabIndex = 20
        Me.B22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A22
        '
        Me.A22.BackColor = System.Drawing.SystemColors.Window
        Me.A22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A22.Location = New System.Drawing.Point(335, 510)
        Me.A22.MaxLength = 6
        Me.A22.Name = "A22"
        Me.A22.ReadOnly = True
        Me.A22.Size = New System.Drawing.Size(35, 20)
        Me.A22.TabIndex = 21
        Me.A22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K22
        '
        Me.K22.BackColor = System.Drawing.SystemColors.Window
        Me.K22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K22.Location = New System.Drawing.Point(133, 510)
        Me.K22.MaxLength = 50
        Me.K22.Name = "K22"
        Me.K22.ReadOnly = True
        Me.K22.Size = New System.Drawing.Size(196, 20)
        Me.K22.TabIndex = 22
        '
        'Y22
        '
        Me.Y22.BackColor = System.Drawing.SystemColors.Window
        Me.Y22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y22.Location = New System.Drawing.Point(553, 510)
        Me.Y22.Name = "Y22"
        Me.Y22.ReadOnly = True
        Me.Y22.Size = New System.Drawing.Size(61, 20)
        Me.Y22.TabIndex = 24
        Me.Y22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M22
        '
        Me.M22.BackColor = System.Drawing.SystemColors.Window
        Me.M22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M22.Location = New System.Drawing.Point(50, 510)
        Me.M22.MaxLength = 7
        Me.M22.Name = "M22"
        Me.M22.ReadOnly = True
        Me.M22.Size = New System.Drawing.Size(61, 20)
        Me.M22.TabIndex = 25
        Me.M22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(181, 7)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(305, 13)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "KETERANGAN HASIL PACKING"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(565, 7)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(37, 13)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "YARD"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(58, 7)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(45, 13)
        Me.Label21.TabIndex = 2
        Me.Label21.Text = "METER"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(16, 468)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(22, 13)
        Me.Label20.TabIndex = 1
        Me.Label20.Text = "20."
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(16, 353)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(22, 13)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "15."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(16, 238)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(22, 13)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "10."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(22, 123)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(16, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "5."
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(16, 445)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(22, 13)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "19."
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(16, 330)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(22, 13)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "14."
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(22, 215)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(16, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "9."
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 100)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(16, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "4."
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(16, 422)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(22, 13)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "18."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(16, 307)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(22, 13)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "13."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(22, 192)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(16, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "8."
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(22, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "3."
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(16, 399)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(22, 13)
        Me.Label17.TabIndex = 1
        Me.Label17.Text = "17."
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(16, 284)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(22, 13)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "12."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(22, 169)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(16, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "7."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 54)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "2."
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(16, 376)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(22, 13)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "16."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(16, 261)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(22, 13)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "11."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(22, 146)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(16, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "6."
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(16, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "1."
        '
        'E20
        '
        Me.E20.BackColor = System.Drawing.SystemColors.Window
        Me.E20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E20.Location = New System.Drawing.Point(499, 464)
        Me.E20.MaxLength = 6
        Me.E20.Name = "E20"
        Me.E20.ReadOnly = True
        Me.E20.Size = New System.Drawing.Size(35, 20)
        Me.E20.TabIndex = 0
        Me.E20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D20
        '
        Me.D20.BackColor = System.Drawing.SystemColors.Window
        Me.D20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D20.Location = New System.Drawing.Point(458, 464)
        Me.D20.MaxLength = 6
        Me.D20.Name = "D20"
        Me.D20.ReadOnly = True
        Me.D20.Size = New System.Drawing.Size(35, 20)
        Me.D20.TabIndex = 0
        Me.D20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C20
        '
        Me.C20.BackColor = System.Drawing.SystemColors.Window
        Me.C20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C20.Location = New System.Drawing.Point(417, 464)
        Me.C20.MaxLength = 6
        Me.C20.Name = "C20"
        Me.C20.ReadOnly = True
        Me.C20.Size = New System.Drawing.Size(35, 20)
        Me.C20.TabIndex = 0
        Me.C20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B20
        '
        Me.B20.BackColor = System.Drawing.SystemColors.Window
        Me.B20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B20.Location = New System.Drawing.Point(376, 464)
        Me.B20.MaxLength = 6
        Me.B20.Name = "B20"
        Me.B20.ReadOnly = True
        Me.B20.Size = New System.Drawing.Size(35, 20)
        Me.B20.TabIndex = 0
        Me.B20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A20
        '
        Me.A20.BackColor = System.Drawing.SystemColors.Window
        Me.A20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A20.Location = New System.Drawing.Point(335, 464)
        Me.A20.MaxLength = 6
        Me.A20.Name = "A20"
        Me.A20.ReadOnly = True
        Me.A20.Size = New System.Drawing.Size(35, 20)
        Me.A20.TabIndex = 0
        Me.A20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K20
        '
        Me.K20.BackColor = System.Drawing.SystemColors.Window
        Me.K20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K20.Location = New System.Drawing.Point(133, 464)
        Me.K20.MaxLength = 50
        Me.K20.Name = "K20"
        Me.K20.ReadOnly = True
        Me.K20.Size = New System.Drawing.Size(196, 20)
        Me.K20.TabIndex = 0
        '
        'Y20
        '
        Me.Y20.BackColor = System.Drawing.SystemColors.Window
        Me.Y20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y20.Location = New System.Drawing.Point(553, 464)
        Me.Y20.Name = "Y20"
        Me.Y20.ReadOnly = True
        Me.Y20.Size = New System.Drawing.Size(61, 20)
        Me.Y20.TabIndex = 0
        Me.Y20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M20
        '
        Me.M20.BackColor = System.Drawing.SystemColors.Window
        Me.M20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M20.Location = New System.Drawing.Point(50, 464)
        Me.M20.MaxLength = 7
        Me.M20.Name = "M20"
        Me.M20.ReadOnly = True
        Me.M20.Size = New System.Drawing.Size(61, 20)
        Me.M20.TabIndex = 0
        Me.M20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E16
        '
        Me.E16.BackColor = System.Drawing.SystemColors.Window
        Me.E16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E16.Location = New System.Drawing.Point(499, 372)
        Me.E16.MaxLength = 6
        Me.E16.Name = "E16"
        Me.E16.ReadOnly = True
        Me.E16.Size = New System.Drawing.Size(35, 20)
        Me.E16.TabIndex = 0
        Me.E16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D16
        '
        Me.D16.BackColor = System.Drawing.SystemColors.Window
        Me.D16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D16.Location = New System.Drawing.Point(458, 372)
        Me.D16.MaxLength = 6
        Me.D16.Name = "D16"
        Me.D16.ReadOnly = True
        Me.D16.Size = New System.Drawing.Size(35, 20)
        Me.D16.TabIndex = 0
        Me.D16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C16
        '
        Me.C16.BackColor = System.Drawing.SystemColors.Window
        Me.C16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C16.Location = New System.Drawing.Point(417, 372)
        Me.C16.MaxLength = 6
        Me.C16.Name = "C16"
        Me.C16.ReadOnly = True
        Me.C16.Size = New System.Drawing.Size(35, 20)
        Me.C16.TabIndex = 0
        Me.C16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B16
        '
        Me.B16.BackColor = System.Drawing.SystemColors.Window
        Me.B16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B16.Location = New System.Drawing.Point(376, 372)
        Me.B16.MaxLength = 6
        Me.B16.Name = "B16"
        Me.B16.ReadOnly = True
        Me.B16.Size = New System.Drawing.Size(35, 20)
        Me.B16.TabIndex = 0
        Me.B16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A16
        '
        Me.A16.BackColor = System.Drawing.SystemColors.Window
        Me.A16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A16.Location = New System.Drawing.Point(335, 372)
        Me.A16.MaxLength = 6
        Me.A16.Name = "A16"
        Me.A16.ReadOnly = True
        Me.A16.Size = New System.Drawing.Size(35, 20)
        Me.A16.TabIndex = 0
        Me.A16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K16
        '
        Me.K16.BackColor = System.Drawing.SystemColors.Window
        Me.K16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K16.Location = New System.Drawing.Point(133, 372)
        Me.K16.MaxLength = 50
        Me.K16.Name = "K16"
        Me.K16.ReadOnly = True
        Me.K16.Size = New System.Drawing.Size(196, 20)
        Me.K16.TabIndex = 0
        '
        'Y16
        '
        Me.Y16.BackColor = System.Drawing.SystemColors.Window
        Me.Y16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y16.Location = New System.Drawing.Point(553, 372)
        Me.Y16.Name = "Y16"
        Me.Y16.ReadOnly = True
        Me.Y16.Size = New System.Drawing.Size(61, 20)
        Me.Y16.TabIndex = 0
        Me.Y16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M16
        '
        Me.M16.BackColor = System.Drawing.SystemColors.Window
        Me.M16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M16.Location = New System.Drawing.Point(50, 372)
        Me.M16.MaxLength = 7
        Me.M16.Name = "M16"
        Me.M16.ReadOnly = True
        Me.M16.Size = New System.Drawing.Size(61, 20)
        Me.M16.TabIndex = 0
        Me.M16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E8
        '
        Me.E8.BackColor = System.Drawing.SystemColors.Window
        Me.E8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E8.Location = New System.Drawing.Point(499, 188)
        Me.E8.MaxLength = 6
        Me.E8.Name = "E8"
        Me.E8.ReadOnly = True
        Me.E8.Size = New System.Drawing.Size(35, 20)
        Me.E8.TabIndex = 0
        Me.E8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D8
        '
        Me.D8.BackColor = System.Drawing.SystemColors.Window
        Me.D8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D8.Location = New System.Drawing.Point(458, 188)
        Me.D8.MaxLength = 6
        Me.D8.Name = "D8"
        Me.D8.ReadOnly = True
        Me.D8.Size = New System.Drawing.Size(35, 20)
        Me.D8.TabIndex = 0
        Me.D8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C8
        '
        Me.C8.BackColor = System.Drawing.SystemColors.Window
        Me.C8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C8.Location = New System.Drawing.Point(417, 188)
        Me.C8.MaxLength = 6
        Me.C8.Name = "C8"
        Me.C8.ReadOnly = True
        Me.C8.Size = New System.Drawing.Size(35, 20)
        Me.C8.TabIndex = 0
        Me.C8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B8
        '
        Me.B8.BackColor = System.Drawing.SystemColors.Window
        Me.B8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B8.Location = New System.Drawing.Point(376, 188)
        Me.B8.MaxLength = 6
        Me.B8.Name = "B8"
        Me.B8.ReadOnly = True
        Me.B8.Size = New System.Drawing.Size(35, 20)
        Me.B8.TabIndex = 0
        Me.B8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A8
        '
        Me.A8.BackColor = System.Drawing.SystemColors.Window
        Me.A8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A8.Location = New System.Drawing.Point(335, 188)
        Me.A8.MaxLength = 6
        Me.A8.Name = "A8"
        Me.A8.ReadOnly = True
        Me.A8.Size = New System.Drawing.Size(35, 20)
        Me.A8.TabIndex = 0
        Me.A8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K8
        '
        Me.K8.BackColor = System.Drawing.SystemColors.Window
        Me.K8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K8.Location = New System.Drawing.Point(133, 188)
        Me.K8.MaxLength = 50
        Me.K8.Name = "K8"
        Me.K8.ReadOnly = True
        Me.K8.Size = New System.Drawing.Size(196, 20)
        Me.K8.TabIndex = 0
        '
        'Y8
        '
        Me.Y8.BackColor = System.Drawing.SystemColors.Window
        Me.Y8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y8.Location = New System.Drawing.Point(553, 188)
        Me.Y8.Name = "Y8"
        Me.Y8.ReadOnly = True
        Me.Y8.Size = New System.Drawing.Size(61, 20)
        Me.Y8.TabIndex = 0
        Me.Y8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M8
        '
        Me.M8.BackColor = System.Drawing.SystemColors.Window
        Me.M8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M8.Location = New System.Drawing.Point(50, 188)
        Me.M8.MaxLength = 7
        Me.M8.Name = "M8"
        Me.M8.ReadOnly = True
        Me.M8.Size = New System.Drawing.Size(61, 20)
        Me.M8.TabIndex = 0
        Me.M8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E12
        '
        Me.E12.BackColor = System.Drawing.SystemColors.Window
        Me.E12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E12.Location = New System.Drawing.Point(499, 280)
        Me.E12.MaxLength = 6
        Me.E12.Name = "E12"
        Me.E12.ReadOnly = True
        Me.E12.Size = New System.Drawing.Size(35, 20)
        Me.E12.TabIndex = 0
        Me.E12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D12
        '
        Me.D12.BackColor = System.Drawing.SystemColors.Window
        Me.D12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D12.Location = New System.Drawing.Point(458, 280)
        Me.D12.MaxLength = 6
        Me.D12.Name = "D12"
        Me.D12.ReadOnly = True
        Me.D12.Size = New System.Drawing.Size(35, 20)
        Me.D12.TabIndex = 0
        Me.D12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C12
        '
        Me.C12.BackColor = System.Drawing.SystemColors.Window
        Me.C12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C12.Location = New System.Drawing.Point(417, 280)
        Me.C12.MaxLength = 6
        Me.C12.Name = "C12"
        Me.C12.ReadOnly = True
        Me.C12.Size = New System.Drawing.Size(35, 20)
        Me.C12.TabIndex = 0
        Me.C12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B12
        '
        Me.B12.BackColor = System.Drawing.SystemColors.Window
        Me.B12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B12.Location = New System.Drawing.Point(376, 280)
        Me.B12.MaxLength = 6
        Me.B12.Name = "B12"
        Me.B12.ReadOnly = True
        Me.B12.Size = New System.Drawing.Size(35, 20)
        Me.B12.TabIndex = 0
        Me.B12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A12
        '
        Me.A12.BackColor = System.Drawing.SystemColors.Window
        Me.A12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A12.Location = New System.Drawing.Point(335, 280)
        Me.A12.MaxLength = 6
        Me.A12.Name = "A12"
        Me.A12.ReadOnly = True
        Me.A12.Size = New System.Drawing.Size(35, 20)
        Me.A12.TabIndex = 0
        Me.A12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K12
        '
        Me.K12.BackColor = System.Drawing.SystemColors.Window
        Me.K12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K12.Location = New System.Drawing.Point(133, 280)
        Me.K12.MaxLength = 50
        Me.K12.Name = "K12"
        Me.K12.ReadOnly = True
        Me.K12.Size = New System.Drawing.Size(196, 20)
        Me.K12.TabIndex = 0
        '
        'Y12
        '
        Me.Y12.BackColor = System.Drawing.SystemColors.Window
        Me.Y12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y12.Location = New System.Drawing.Point(553, 280)
        Me.Y12.Name = "Y12"
        Me.Y12.ReadOnly = True
        Me.Y12.Size = New System.Drawing.Size(61, 20)
        Me.Y12.TabIndex = 0
        Me.Y12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M12
        '
        Me.M12.BackColor = System.Drawing.SystemColors.Window
        Me.M12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M12.Location = New System.Drawing.Point(50, 280)
        Me.M12.MaxLength = 7
        Me.M12.Name = "M12"
        Me.M12.ReadOnly = True
        Me.M12.Size = New System.Drawing.Size(61, 20)
        Me.M12.TabIndex = 0
        Me.M12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E4
        '
        Me.E4.BackColor = System.Drawing.SystemColors.Window
        Me.E4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E4.Location = New System.Drawing.Point(499, 96)
        Me.E4.MaxLength = 6
        Me.E4.Name = "E4"
        Me.E4.ReadOnly = True
        Me.E4.Size = New System.Drawing.Size(35, 20)
        Me.E4.TabIndex = 0
        Me.E4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D4
        '
        Me.D4.BackColor = System.Drawing.SystemColors.Window
        Me.D4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D4.Location = New System.Drawing.Point(458, 96)
        Me.D4.MaxLength = 6
        Me.D4.Name = "D4"
        Me.D4.ReadOnly = True
        Me.D4.Size = New System.Drawing.Size(35, 20)
        Me.D4.TabIndex = 0
        Me.D4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C4
        '
        Me.C4.BackColor = System.Drawing.SystemColors.Window
        Me.C4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C4.Location = New System.Drawing.Point(417, 96)
        Me.C4.MaxLength = 6
        Me.C4.Name = "C4"
        Me.C4.ReadOnly = True
        Me.C4.Size = New System.Drawing.Size(35, 20)
        Me.C4.TabIndex = 0
        Me.C4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B4
        '
        Me.B4.BackColor = System.Drawing.SystemColors.Window
        Me.B4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B4.Location = New System.Drawing.Point(376, 96)
        Me.B4.MaxLength = 6
        Me.B4.Name = "B4"
        Me.B4.ReadOnly = True
        Me.B4.Size = New System.Drawing.Size(35, 20)
        Me.B4.TabIndex = 0
        Me.B4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A4
        '
        Me.A4.BackColor = System.Drawing.SystemColors.Window
        Me.A4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A4.Location = New System.Drawing.Point(335, 96)
        Me.A4.MaxLength = 6
        Me.A4.Name = "A4"
        Me.A4.ReadOnly = True
        Me.A4.Size = New System.Drawing.Size(35, 20)
        Me.A4.TabIndex = 0
        Me.A4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K4
        '
        Me.K4.BackColor = System.Drawing.SystemColors.Window
        Me.K4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K4.Location = New System.Drawing.Point(133, 96)
        Me.K4.MaxLength = 50
        Me.K4.Name = "K4"
        Me.K4.ReadOnly = True
        Me.K4.Size = New System.Drawing.Size(196, 20)
        Me.K4.TabIndex = 0
        '
        'Y4
        '
        Me.Y4.BackColor = System.Drawing.SystemColors.Window
        Me.Y4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y4.Location = New System.Drawing.Point(553, 96)
        Me.Y4.Name = "Y4"
        Me.Y4.ReadOnly = True
        Me.Y4.Size = New System.Drawing.Size(61, 20)
        Me.Y4.TabIndex = 0
        Me.Y4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M4
        '
        Me.M4.BackColor = System.Drawing.SystemColors.Window
        Me.M4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M4.Location = New System.Drawing.Point(50, 96)
        Me.M4.MaxLength = 7
        Me.M4.Name = "M4"
        Me.M4.ReadOnly = True
        Me.M4.Size = New System.Drawing.Size(61, 20)
        Me.M4.TabIndex = 0
        Me.M4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E18
        '
        Me.E18.BackColor = System.Drawing.SystemColors.Window
        Me.E18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E18.Location = New System.Drawing.Point(499, 418)
        Me.E18.MaxLength = 6
        Me.E18.Name = "E18"
        Me.E18.ReadOnly = True
        Me.E18.Size = New System.Drawing.Size(35, 20)
        Me.E18.TabIndex = 0
        Me.E18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D18
        '
        Me.D18.BackColor = System.Drawing.SystemColors.Window
        Me.D18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D18.Location = New System.Drawing.Point(458, 418)
        Me.D18.MaxLength = 6
        Me.D18.Name = "D18"
        Me.D18.ReadOnly = True
        Me.D18.Size = New System.Drawing.Size(35, 20)
        Me.D18.TabIndex = 0
        Me.D18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C18
        '
        Me.C18.BackColor = System.Drawing.SystemColors.Window
        Me.C18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C18.Location = New System.Drawing.Point(417, 418)
        Me.C18.MaxLength = 6
        Me.C18.Name = "C18"
        Me.C18.ReadOnly = True
        Me.C18.Size = New System.Drawing.Size(35, 20)
        Me.C18.TabIndex = 0
        Me.C18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B18
        '
        Me.B18.BackColor = System.Drawing.SystemColors.Window
        Me.B18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B18.Location = New System.Drawing.Point(376, 418)
        Me.B18.MaxLength = 6
        Me.B18.Name = "B18"
        Me.B18.ReadOnly = True
        Me.B18.Size = New System.Drawing.Size(35, 20)
        Me.B18.TabIndex = 0
        Me.B18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A18
        '
        Me.A18.BackColor = System.Drawing.SystemColors.Window
        Me.A18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A18.Location = New System.Drawing.Point(335, 418)
        Me.A18.MaxLength = 6
        Me.A18.Name = "A18"
        Me.A18.ReadOnly = True
        Me.A18.Size = New System.Drawing.Size(35, 20)
        Me.A18.TabIndex = 0
        Me.A18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K18
        '
        Me.K18.BackColor = System.Drawing.SystemColors.Window
        Me.K18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K18.Location = New System.Drawing.Point(133, 418)
        Me.K18.MaxLength = 50
        Me.K18.Name = "K18"
        Me.K18.ReadOnly = True
        Me.K18.Size = New System.Drawing.Size(196, 20)
        Me.K18.TabIndex = 0
        '
        'Y18
        '
        Me.Y18.BackColor = System.Drawing.SystemColors.Window
        Me.Y18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y18.Location = New System.Drawing.Point(553, 418)
        Me.Y18.Name = "Y18"
        Me.Y18.ReadOnly = True
        Me.Y18.Size = New System.Drawing.Size(61, 20)
        Me.Y18.TabIndex = 0
        Me.Y18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M18
        '
        Me.M18.BackColor = System.Drawing.SystemColors.Window
        Me.M18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M18.Location = New System.Drawing.Point(50, 418)
        Me.M18.MaxLength = 7
        Me.M18.Name = "M18"
        Me.M18.ReadOnly = True
        Me.M18.Size = New System.Drawing.Size(61, 20)
        Me.M18.TabIndex = 0
        Me.M18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E14
        '
        Me.E14.BackColor = System.Drawing.SystemColors.Window
        Me.E14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E14.Location = New System.Drawing.Point(499, 326)
        Me.E14.MaxLength = 6
        Me.E14.Name = "E14"
        Me.E14.ReadOnly = True
        Me.E14.Size = New System.Drawing.Size(35, 20)
        Me.E14.TabIndex = 0
        Me.E14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D14
        '
        Me.D14.BackColor = System.Drawing.SystemColors.Window
        Me.D14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D14.Location = New System.Drawing.Point(458, 326)
        Me.D14.MaxLength = 6
        Me.D14.Name = "D14"
        Me.D14.ReadOnly = True
        Me.D14.Size = New System.Drawing.Size(35, 20)
        Me.D14.TabIndex = 0
        Me.D14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C14
        '
        Me.C14.BackColor = System.Drawing.SystemColors.Window
        Me.C14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C14.Location = New System.Drawing.Point(417, 326)
        Me.C14.MaxLength = 6
        Me.C14.Name = "C14"
        Me.C14.ReadOnly = True
        Me.C14.Size = New System.Drawing.Size(35, 20)
        Me.C14.TabIndex = 0
        Me.C14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B14
        '
        Me.B14.BackColor = System.Drawing.SystemColors.Window
        Me.B14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B14.Location = New System.Drawing.Point(376, 326)
        Me.B14.MaxLength = 6
        Me.B14.Name = "B14"
        Me.B14.ReadOnly = True
        Me.B14.Size = New System.Drawing.Size(35, 20)
        Me.B14.TabIndex = 0
        Me.B14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A14
        '
        Me.A14.BackColor = System.Drawing.SystemColors.Window
        Me.A14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A14.Location = New System.Drawing.Point(335, 326)
        Me.A14.MaxLength = 6
        Me.A14.Name = "A14"
        Me.A14.ReadOnly = True
        Me.A14.Size = New System.Drawing.Size(35, 20)
        Me.A14.TabIndex = 0
        Me.A14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K14
        '
        Me.K14.BackColor = System.Drawing.SystemColors.Window
        Me.K14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K14.Location = New System.Drawing.Point(133, 326)
        Me.K14.MaxLength = 50
        Me.K14.Name = "K14"
        Me.K14.ReadOnly = True
        Me.K14.Size = New System.Drawing.Size(196, 20)
        Me.K14.TabIndex = 0
        '
        'Y14
        '
        Me.Y14.BackColor = System.Drawing.SystemColors.Window
        Me.Y14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y14.Location = New System.Drawing.Point(553, 326)
        Me.Y14.Name = "Y14"
        Me.Y14.ReadOnly = True
        Me.Y14.Size = New System.Drawing.Size(61, 20)
        Me.Y14.TabIndex = 0
        Me.Y14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M14
        '
        Me.M14.BackColor = System.Drawing.SystemColors.Window
        Me.M14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M14.Location = New System.Drawing.Point(50, 326)
        Me.M14.MaxLength = 7
        Me.M14.Name = "M14"
        Me.M14.ReadOnly = True
        Me.M14.Size = New System.Drawing.Size(61, 20)
        Me.M14.TabIndex = 0
        Me.M14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E6
        '
        Me.E6.BackColor = System.Drawing.SystemColors.Window
        Me.E6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E6.Location = New System.Drawing.Point(499, 142)
        Me.E6.MaxLength = 6
        Me.E6.Name = "E6"
        Me.E6.ReadOnly = True
        Me.E6.Size = New System.Drawing.Size(35, 20)
        Me.E6.TabIndex = 0
        Me.E6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D6
        '
        Me.D6.BackColor = System.Drawing.SystemColors.Window
        Me.D6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D6.Location = New System.Drawing.Point(458, 142)
        Me.D6.MaxLength = 6
        Me.D6.Name = "D6"
        Me.D6.ReadOnly = True
        Me.D6.Size = New System.Drawing.Size(35, 20)
        Me.D6.TabIndex = 0
        Me.D6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C6
        '
        Me.C6.BackColor = System.Drawing.SystemColors.Window
        Me.C6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C6.Location = New System.Drawing.Point(417, 142)
        Me.C6.MaxLength = 6
        Me.C6.Name = "C6"
        Me.C6.ReadOnly = True
        Me.C6.Size = New System.Drawing.Size(35, 20)
        Me.C6.TabIndex = 0
        Me.C6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B6
        '
        Me.B6.BackColor = System.Drawing.SystemColors.Window
        Me.B6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B6.Location = New System.Drawing.Point(376, 142)
        Me.B6.MaxLength = 6
        Me.B6.Name = "B6"
        Me.B6.ReadOnly = True
        Me.B6.Size = New System.Drawing.Size(35, 20)
        Me.B6.TabIndex = 0
        Me.B6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A6
        '
        Me.A6.BackColor = System.Drawing.SystemColors.Window
        Me.A6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A6.Location = New System.Drawing.Point(335, 142)
        Me.A6.MaxLength = 6
        Me.A6.Name = "A6"
        Me.A6.ReadOnly = True
        Me.A6.Size = New System.Drawing.Size(35, 20)
        Me.A6.TabIndex = 0
        Me.A6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K6
        '
        Me.K6.BackColor = System.Drawing.SystemColors.Window
        Me.K6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K6.Location = New System.Drawing.Point(133, 142)
        Me.K6.MaxLength = 50
        Me.K6.Name = "K6"
        Me.K6.ReadOnly = True
        Me.K6.Size = New System.Drawing.Size(196, 20)
        Me.K6.TabIndex = 0
        '
        'Y6
        '
        Me.Y6.BackColor = System.Drawing.SystemColors.Window
        Me.Y6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y6.Location = New System.Drawing.Point(553, 142)
        Me.Y6.Name = "Y6"
        Me.Y6.ReadOnly = True
        Me.Y6.Size = New System.Drawing.Size(61, 20)
        Me.Y6.TabIndex = 0
        Me.Y6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M6
        '
        Me.M6.BackColor = System.Drawing.SystemColors.Window
        Me.M6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M6.Location = New System.Drawing.Point(50, 142)
        Me.M6.MaxLength = 7
        Me.M6.Name = "M6"
        Me.M6.ReadOnly = True
        Me.M6.Size = New System.Drawing.Size(61, 20)
        Me.M6.TabIndex = 0
        Me.M6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E10
        '
        Me.E10.BackColor = System.Drawing.SystemColors.Window
        Me.E10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E10.Location = New System.Drawing.Point(499, 234)
        Me.E10.MaxLength = 6
        Me.E10.Name = "E10"
        Me.E10.ReadOnly = True
        Me.E10.Size = New System.Drawing.Size(35, 20)
        Me.E10.TabIndex = 0
        Me.E10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D10
        '
        Me.D10.BackColor = System.Drawing.SystemColors.Window
        Me.D10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D10.Location = New System.Drawing.Point(458, 234)
        Me.D10.MaxLength = 6
        Me.D10.Name = "D10"
        Me.D10.ReadOnly = True
        Me.D10.Size = New System.Drawing.Size(35, 20)
        Me.D10.TabIndex = 0
        Me.D10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C10
        '
        Me.C10.BackColor = System.Drawing.SystemColors.Window
        Me.C10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C10.Location = New System.Drawing.Point(417, 234)
        Me.C10.MaxLength = 6
        Me.C10.Name = "C10"
        Me.C10.ReadOnly = True
        Me.C10.Size = New System.Drawing.Size(35, 20)
        Me.C10.TabIndex = 0
        Me.C10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B10
        '
        Me.B10.BackColor = System.Drawing.SystemColors.Window
        Me.B10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B10.Location = New System.Drawing.Point(376, 234)
        Me.B10.MaxLength = 6
        Me.B10.Name = "B10"
        Me.B10.ReadOnly = True
        Me.B10.Size = New System.Drawing.Size(35, 20)
        Me.B10.TabIndex = 0
        Me.B10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A10
        '
        Me.A10.BackColor = System.Drawing.SystemColors.Window
        Me.A10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A10.Location = New System.Drawing.Point(335, 234)
        Me.A10.MaxLength = 6
        Me.A10.Name = "A10"
        Me.A10.ReadOnly = True
        Me.A10.Size = New System.Drawing.Size(35, 20)
        Me.A10.TabIndex = 0
        Me.A10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K10
        '
        Me.K10.BackColor = System.Drawing.SystemColors.Window
        Me.K10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K10.Location = New System.Drawing.Point(133, 234)
        Me.K10.MaxLength = 50
        Me.K10.Name = "K10"
        Me.K10.ReadOnly = True
        Me.K10.Size = New System.Drawing.Size(196, 20)
        Me.K10.TabIndex = 0
        '
        'Y10
        '
        Me.Y10.BackColor = System.Drawing.SystemColors.Window
        Me.Y10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y10.Location = New System.Drawing.Point(553, 234)
        Me.Y10.Name = "Y10"
        Me.Y10.ReadOnly = True
        Me.Y10.Size = New System.Drawing.Size(61, 20)
        Me.Y10.TabIndex = 0
        Me.Y10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M10
        '
        Me.M10.BackColor = System.Drawing.SystemColors.Window
        Me.M10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M10.Location = New System.Drawing.Point(50, 234)
        Me.M10.MaxLength = 7
        Me.M10.Name = "M10"
        Me.M10.ReadOnly = True
        Me.M10.Size = New System.Drawing.Size(61, 20)
        Me.M10.TabIndex = 0
        Me.M10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E2
        '
        Me.E2.BackColor = System.Drawing.SystemColors.Window
        Me.E2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E2.Location = New System.Drawing.Point(499, 50)
        Me.E2.MaxLength = 6
        Me.E2.Name = "E2"
        Me.E2.ReadOnly = True
        Me.E2.Size = New System.Drawing.Size(35, 20)
        Me.E2.TabIndex = 0
        Me.E2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D2
        '
        Me.D2.BackColor = System.Drawing.SystemColors.Window
        Me.D2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D2.Location = New System.Drawing.Point(458, 50)
        Me.D2.MaxLength = 6
        Me.D2.Name = "D2"
        Me.D2.ReadOnly = True
        Me.D2.Size = New System.Drawing.Size(35, 20)
        Me.D2.TabIndex = 0
        Me.D2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C2
        '
        Me.C2.BackColor = System.Drawing.SystemColors.Window
        Me.C2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C2.Location = New System.Drawing.Point(417, 50)
        Me.C2.MaxLength = 6
        Me.C2.Name = "C2"
        Me.C2.ReadOnly = True
        Me.C2.Size = New System.Drawing.Size(35, 20)
        Me.C2.TabIndex = 0
        Me.C2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B2
        '
        Me.B2.BackColor = System.Drawing.SystemColors.Window
        Me.B2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B2.Location = New System.Drawing.Point(376, 50)
        Me.B2.MaxLength = 6
        Me.B2.Name = "B2"
        Me.B2.ReadOnly = True
        Me.B2.Size = New System.Drawing.Size(35, 20)
        Me.B2.TabIndex = 0
        Me.B2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A2
        '
        Me.A2.BackColor = System.Drawing.SystemColors.Window
        Me.A2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A2.Location = New System.Drawing.Point(335, 50)
        Me.A2.MaxLength = 6
        Me.A2.Name = "A2"
        Me.A2.ReadOnly = True
        Me.A2.Size = New System.Drawing.Size(35, 20)
        Me.A2.TabIndex = 0
        Me.A2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K2
        '
        Me.K2.BackColor = System.Drawing.SystemColors.Window
        Me.K2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K2.Location = New System.Drawing.Point(133, 50)
        Me.K2.MaxLength = 50
        Me.K2.Name = "K2"
        Me.K2.ReadOnly = True
        Me.K2.Size = New System.Drawing.Size(196, 20)
        Me.K2.TabIndex = 0
        '
        'Y2
        '
        Me.Y2.BackColor = System.Drawing.SystemColors.Window
        Me.Y2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y2.Location = New System.Drawing.Point(553, 50)
        Me.Y2.Name = "Y2"
        Me.Y2.ReadOnly = True
        Me.Y2.Size = New System.Drawing.Size(61, 20)
        Me.Y2.TabIndex = 0
        Me.Y2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M2
        '
        Me.M2.BackColor = System.Drawing.SystemColors.Window
        Me.M2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M2.Location = New System.Drawing.Point(50, 50)
        Me.M2.MaxLength = 7
        Me.M2.Name = "M2"
        Me.M2.ReadOnly = True
        Me.M2.Size = New System.Drawing.Size(61, 20)
        Me.M2.TabIndex = 0
        Me.M2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E19
        '
        Me.E19.BackColor = System.Drawing.SystemColors.Window
        Me.E19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E19.Location = New System.Drawing.Point(499, 441)
        Me.E19.MaxLength = 6
        Me.E19.Name = "E19"
        Me.E19.ReadOnly = True
        Me.E19.Size = New System.Drawing.Size(35, 20)
        Me.E19.TabIndex = 0
        Me.E19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D19
        '
        Me.D19.BackColor = System.Drawing.SystemColors.Window
        Me.D19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D19.Location = New System.Drawing.Point(458, 441)
        Me.D19.MaxLength = 6
        Me.D19.Name = "D19"
        Me.D19.ReadOnly = True
        Me.D19.Size = New System.Drawing.Size(35, 20)
        Me.D19.TabIndex = 0
        Me.D19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C19
        '
        Me.C19.BackColor = System.Drawing.SystemColors.Window
        Me.C19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C19.Location = New System.Drawing.Point(417, 441)
        Me.C19.MaxLength = 6
        Me.C19.Name = "C19"
        Me.C19.ReadOnly = True
        Me.C19.Size = New System.Drawing.Size(35, 20)
        Me.C19.TabIndex = 0
        Me.C19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B19
        '
        Me.B19.BackColor = System.Drawing.SystemColors.Window
        Me.B19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B19.Location = New System.Drawing.Point(376, 441)
        Me.B19.MaxLength = 6
        Me.B19.Name = "B19"
        Me.B19.ReadOnly = True
        Me.B19.Size = New System.Drawing.Size(35, 20)
        Me.B19.TabIndex = 0
        Me.B19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A19
        '
        Me.A19.BackColor = System.Drawing.SystemColors.Window
        Me.A19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A19.Location = New System.Drawing.Point(335, 441)
        Me.A19.MaxLength = 6
        Me.A19.Name = "A19"
        Me.A19.ReadOnly = True
        Me.A19.Size = New System.Drawing.Size(35, 20)
        Me.A19.TabIndex = 0
        Me.A19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K19
        '
        Me.K19.BackColor = System.Drawing.SystemColors.Window
        Me.K19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K19.Location = New System.Drawing.Point(133, 441)
        Me.K19.MaxLength = 50
        Me.K19.Name = "K19"
        Me.K19.ReadOnly = True
        Me.K19.Size = New System.Drawing.Size(196, 20)
        Me.K19.TabIndex = 0
        '
        'Y19
        '
        Me.Y19.BackColor = System.Drawing.SystemColors.Window
        Me.Y19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y19.Location = New System.Drawing.Point(553, 441)
        Me.Y19.Name = "Y19"
        Me.Y19.ReadOnly = True
        Me.Y19.Size = New System.Drawing.Size(61, 20)
        Me.Y19.TabIndex = 0
        Me.Y19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M19
        '
        Me.M19.BackColor = System.Drawing.SystemColors.Window
        Me.M19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M19.Location = New System.Drawing.Point(50, 441)
        Me.M19.MaxLength = 7
        Me.M19.Name = "M19"
        Me.M19.ReadOnly = True
        Me.M19.Size = New System.Drawing.Size(61, 20)
        Me.M19.TabIndex = 0
        Me.M19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E15
        '
        Me.E15.BackColor = System.Drawing.SystemColors.Window
        Me.E15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E15.Location = New System.Drawing.Point(499, 349)
        Me.E15.MaxLength = 6
        Me.E15.Name = "E15"
        Me.E15.ReadOnly = True
        Me.E15.Size = New System.Drawing.Size(35, 20)
        Me.E15.TabIndex = 0
        Me.E15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D15
        '
        Me.D15.BackColor = System.Drawing.SystemColors.Window
        Me.D15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D15.Location = New System.Drawing.Point(458, 349)
        Me.D15.MaxLength = 6
        Me.D15.Name = "D15"
        Me.D15.ReadOnly = True
        Me.D15.Size = New System.Drawing.Size(35, 20)
        Me.D15.TabIndex = 0
        Me.D15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C15
        '
        Me.C15.BackColor = System.Drawing.SystemColors.Window
        Me.C15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C15.Location = New System.Drawing.Point(417, 349)
        Me.C15.MaxLength = 6
        Me.C15.Name = "C15"
        Me.C15.ReadOnly = True
        Me.C15.Size = New System.Drawing.Size(35, 20)
        Me.C15.TabIndex = 0
        Me.C15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B15
        '
        Me.B15.BackColor = System.Drawing.SystemColors.Window
        Me.B15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B15.Location = New System.Drawing.Point(376, 349)
        Me.B15.MaxLength = 6
        Me.B15.Name = "B15"
        Me.B15.ReadOnly = True
        Me.B15.Size = New System.Drawing.Size(35, 20)
        Me.B15.TabIndex = 0
        Me.B15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A15
        '
        Me.A15.BackColor = System.Drawing.SystemColors.Window
        Me.A15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A15.Location = New System.Drawing.Point(335, 349)
        Me.A15.MaxLength = 6
        Me.A15.Name = "A15"
        Me.A15.ReadOnly = True
        Me.A15.Size = New System.Drawing.Size(35, 20)
        Me.A15.TabIndex = 0
        Me.A15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K15
        '
        Me.K15.BackColor = System.Drawing.SystemColors.Window
        Me.K15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K15.Location = New System.Drawing.Point(133, 349)
        Me.K15.MaxLength = 50
        Me.K15.Name = "K15"
        Me.K15.ReadOnly = True
        Me.K15.Size = New System.Drawing.Size(196, 20)
        Me.K15.TabIndex = 0
        '
        'Y15
        '
        Me.Y15.BackColor = System.Drawing.SystemColors.Window
        Me.Y15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y15.Location = New System.Drawing.Point(553, 349)
        Me.Y15.Name = "Y15"
        Me.Y15.ReadOnly = True
        Me.Y15.Size = New System.Drawing.Size(61, 20)
        Me.Y15.TabIndex = 0
        Me.Y15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M15
        '
        Me.M15.BackColor = System.Drawing.SystemColors.Window
        Me.M15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M15.Location = New System.Drawing.Point(50, 349)
        Me.M15.MaxLength = 7
        Me.M15.Name = "M15"
        Me.M15.ReadOnly = True
        Me.M15.Size = New System.Drawing.Size(61, 20)
        Me.M15.TabIndex = 0
        Me.M15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E7
        '
        Me.E7.BackColor = System.Drawing.SystemColors.Window
        Me.E7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E7.Location = New System.Drawing.Point(499, 165)
        Me.E7.MaxLength = 6
        Me.E7.Name = "E7"
        Me.E7.ReadOnly = True
        Me.E7.Size = New System.Drawing.Size(35, 20)
        Me.E7.TabIndex = 0
        Me.E7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D7
        '
        Me.D7.BackColor = System.Drawing.SystemColors.Window
        Me.D7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D7.Location = New System.Drawing.Point(458, 165)
        Me.D7.MaxLength = 6
        Me.D7.Name = "D7"
        Me.D7.ReadOnly = True
        Me.D7.Size = New System.Drawing.Size(35, 20)
        Me.D7.TabIndex = 0
        Me.D7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C7
        '
        Me.C7.BackColor = System.Drawing.SystemColors.Window
        Me.C7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C7.Location = New System.Drawing.Point(417, 165)
        Me.C7.MaxLength = 6
        Me.C7.Name = "C7"
        Me.C7.ReadOnly = True
        Me.C7.Size = New System.Drawing.Size(35, 20)
        Me.C7.TabIndex = 0
        Me.C7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B7
        '
        Me.B7.BackColor = System.Drawing.SystemColors.Window
        Me.B7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B7.Location = New System.Drawing.Point(376, 165)
        Me.B7.MaxLength = 6
        Me.B7.Name = "B7"
        Me.B7.ReadOnly = True
        Me.B7.Size = New System.Drawing.Size(35, 20)
        Me.B7.TabIndex = 0
        Me.B7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A7
        '
        Me.A7.BackColor = System.Drawing.SystemColors.Window
        Me.A7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A7.Location = New System.Drawing.Point(335, 165)
        Me.A7.MaxLength = 6
        Me.A7.Name = "A7"
        Me.A7.ReadOnly = True
        Me.A7.Size = New System.Drawing.Size(35, 20)
        Me.A7.TabIndex = 0
        Me.A7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K7
        '
        Me.K7.BackColor = System.Drawing.SystemColors.Window
        Me.K7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K7.Location = New System.Drawing.Point(133, 165)
        Me.K7.MaxLength = 50
        Me.K7.Name = "K7"
        Me.K7.ReadOnly = True
        Me.K7.Size = New System.Drawing.Size(196, 20)
        Me.K7.TabIndex = 0
        '
        'Y7
        '
        Me.Y7.BackColor = System.Drawing.SystemColors.Window
        Me.Y7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y7.Location = New System.Drawing.Point(553, 165)
        Me.Y7.Name = "Y7"
        Me.Y7.ReadOnly = True
        Me.Y7.Size = New System.Drawing.Size(61, 20)
        Me.Y7.TabIndex = 0
        Me.Y7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M7
        '
        Me.M7.BackColor = System.Drawing.SystemColors.Window
        Me.M7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M7.Location = New System.Drawing.Point(50, 165)
        Me.M7.MaxLength = 7
        Me.M7.Name = "M7"
        Me.M7.ReadOnly = True
        Me.M7.Size = New System.Drawing.Size(61, 20)
        Me.M7.TabIndex = 0
        Me.M7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E11
        '
        Me.E11.BackColor = System.Drawing.SystemColors.Window
        Me.E11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E11.Location = New System.Drawing.Point(499, 257)
        Me.E11.MaxLength = 6
        Me.E11.Name = "E11"
        Me.E11.ReadOnly = True
        Me.E11.Size = New System.Drawing.Size(35, 20)
        Me.E11.TabIndex = 0
        Me.E11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D11
        '
        Me.D11.BackColor = System.Drawing.SystemColors.Window
        Me.D11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D11.Location = New System.Drawing.Point(458, 257)
        Me.D11.MaxLength = 6
        Me.D11.Name = "D11"
        Me.D11.ReadOnly = True
        Me.D11.Size = New System.Drawing.Size(35, 20)
        Me.D11.TabIndex = 0
        Me.D11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C11
        '
        Me.C11.BackColor = System.Drawing.SystemColors.Window
        Me.C11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C11.Location = New System.Drawing.Point(417, 257)
        Me.C11.MaxLength = 6
        Me.C11.Name = "C11"
        Me.C11.ReadOnly = True
        Me.C11.Size = New System.Drawing.Size(35, 20)
        Me.C11.TabIndex = 0
        Me.C11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B11
        '
        Me.B11.BackColor = System.Drawing.SystemColors.Window
        Me.B11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B11.Location = New System.Drawing.Point(376, 257)
        Me.B11.MaxLength = 6
        Me.B11.Name = "B11"
        Me.B11.ReadOnly = True
        Me.B11.Size = New System.Drawing.Size(35, 20)
        Me.B11.TabIndex = 0
        Me.B11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A11
        '
        Me.A11.BackColor = System.Drawing.SystemColors.Window
        Me.A11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A11.Location = New System.Drawing.Point(335, 257)
        Me.A11.MaxLength = 6
        Me.A11.Name = "A11"
        Me.A11.ReadOnly = True
        Me.A11.Size = New System.Drawing.Size(35, 20)
        Me.A11.TabIndex = 0
        Me.A11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K11
        '
        Me.K11.BackColor = System.Drawing.SystemColors.Window
        Me.K11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K11.Location = New System.Drawing.Point(133, 257)
        Me.K11.MaxLength = 50
        Me.K11.Name = "K11"
        Me.K11.ReadOnly = True
        Me.K11.Size = New System.Drawing.Size(196, 20)
        Me.K11.TabIndex = 0
        '
        'Y11
        '
        Me.Y11.BackColor = System.Drawing.SystemColors.Window
        Me.Y11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y11.Location = New System.Drawing.Point(553, 257)
        Me.Y11.Name = "Y11"
        Me.Y11.ReadOnly = True
        Me.Y11.Size = New System.Drawing.Size(61, 20)
        Me.Y11.TabIndex = 0
        Me.Y11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M11
        '
        Me.M11.BackColor = System.Drawing.SystemColors.Window
        Me.M11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M11.Location = New System.Drawing.Point(50, 257)
        Me.M11.MaxLength = 7
        Me.M11.Name = "M11"
        Me.M11.ReadOnly = True
        Me.M11.Size = New System.Drawing.Size(61, 20)
        Me.M11.TabIndex = 0
        Me.M11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E3
        '
        Me.E3.BackColor = System.Drawing.SystemColors.Window
        Me.E3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E3.Location = New System.Drawing.Point(499, 73)
        Me.E3.MaxLength = 6
        Me.E3.Name = "E3"
        Me.E3.ReadOnly = True
        Me.E3.Size = New System.Drawing.Size(35, 20)
        Me.E3.TabIndex = 0
        Me.E3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D3
        '
        Me.D3.BackColor = System.Drawing.SystemColors.Window
        Me.D3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D3.Location = New System.Drawing.Point(458, 73)
        Me.D3.MaxLength = 6
        Me.D3.Name = "D3"
        Me.D3.ReadOnly = True
        Me.D3.Size = New System.Drawing.Size(35, 20)
        Me.D3.TabIndex = 0
        Me.D3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C3
        '
        Me.C3.BackColor = System.Drawing.SystemColors.Window
        Me.C3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C3.Location = New System.Drawing.Point(417, 73)
        Me.C3.MaxLength = 6
        Me.C3.Name = "C3"
        Me.C3.ReadOnly = True
        Me.C3.Size = New System.Drawing.Size(35, 20)
        Me.C3.TabIndex = 0
        Me.C3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B3
        '
        Me.B3.BackColor = System.Drawing.SystemColors.Window
        Me.B3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B3.Location = New System.Drawing.Point(376, 73)
        Me.B3.MaxLength = 6
        Me.B3.Name = "B3"
        Me.B3.ReadOnly = True
        Me.B3.Size = New System.Drawing.Size(35, 20)
        Me.B3.TabIndex = 0
        Me.B3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A3
        '
        Me.A3.BackColor = System.Drawing.SystemColors.Window
        Me.A3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A3.Location = New System.Drawing.Point(335, 73)
        Me.A3.MaxLength = 6
        Me.A3.Name = "A3"
        Me.A3.ReadOnly = True
        Me.A3.Size = New System.Drawing.Size(35, 20)
        Me.A3.TabIndex = 0
        Me.A3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K3
        '
        Me.K3.BackColor = System.Drawing.SystemColors.Window
        Me.K3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K3.Location = New System.Drawing.Point(133, 73)
        Me.K3.MaxLength = 50
        Me.K3.Name = "K3"
        Me.K3.ReadOnly = True
        Me.K3.Size = New System.Drawing.Size(196, 20)
        Me.K3.TabIndex = 0
        '
        'Y3
        '
        Me.Y3.BackColor = System.Drawing.SystemColors.Window
        Me.Y3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y3.Location = New System.Drawing.Point(553, 73)
        Me.Y3.Name = "Y3"
        Me.Y3.ReadOnly = True
        Me.Y3.Size = New System.Drawing.Size(61, 20)
        Me.Y3.TabIndex = 0
        Me.Y3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M3
        '
        Me.M3.BackColor = System.Drawing.SystemColors.Window
        Me.M3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M3.Location = New System.Drawing.Point(50, 73)
        Me.M3.MaxLength = 7
        Me.M3.Name = "M3"
        Me.M3.ReadOnly = True
        Me.M3.Size = New System.Drawing.Size(61, 20)
        Me.M3.TabIndex = 0
        Me.M3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E17
        '
        Me.E17.BackColor = System.Drawing.SystemColors.Window
        Me.E17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E17.Location = New System.Drawing.Point(499, 395)
        Me.E17.MaxLength = 6
        Me.E17.Name = "E17"
        Me.E17.ReadOnly = True
        Me.E17.Size = New System.Drawing.Size(35, 20)
        Me.E17.TabIndex = 0
        Me.E17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D17
        '
        Me.D17.BackColor = System.Drawing.SystemColors.Window
        Me.D17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D17.Location = New System.Drawing.Point(458, 395)
        Me.D17.MaxLength = 6
        Me.D17.Name = "D17"
        Me.D17.ReadOnly = True
        Me.D17.Size = New System.Drawing.Size(35, 20)
        Me.D17.TabIndex = 0
        Me.D17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C17
        '
        Me.C17.BackColor = System.Drawing.SystemColors.Window
        Me.C17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C17.Location = New System.Drawing.Point(417, 395)
        Me.C17.MaxLength = 6
        Me.C17.Name = "C17"
        Me.C17.ReadOnly = True
        Me.C17.Size = New System.Drawing.Size(35, 20)
        Me.C17.TabIndex = 0
        Me.C17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B17
        '
        Me.B17.BackColor = System.Drawing.SystemColors.Window
        Me.B17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B17.Location = New System.Drawing.Point(376, 395)
        Me.B17.MaxLength = 6
        Me.B17.Name = "B17"
        Me.B17.ReadOnly = True
        Me.B17.Size = New System.Drawing.Size(35, 20)
        Me.B17.TabIndex = 0
        Me.B17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A17
        '
        Me.A17.BackColor = System.Drawing.SystemColors.Window
        Me.A17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A17.Location = New System.Drawing.Point(335, 395)
        Me.A17.MaxLength = 6
        Me.A17.Name = "A17"
        Me.A17.ReadOnly = True
        Me.A17.Size = New System.Drawing.Size(35, 20)
        Me.A17.TabIndex = 0
        Me.A17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K17
        '
        Me.K17.BackColor = System.Drawing.SystemColors.Window
        Me.K17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K17.Location = New System.Drawing.Point(133, 395)
        Me.K17.MaxLength = 50
        Me.K17.Name = "K17"
        Me.K17.ReadOnly = True
        Me.K17.Size = New System.Drawing.Size(196, 20)
        Me.K17.TabIndex = 0
        '
        'Y17
        '
        Me.Y17.BackColor = System.Drawing.SystemColors.Window
        Me.Y17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y17.Location = New System.Drawing.Point(553, 395)
        Me.Y17.Name = "Y17"
        Me.Y17.ReadOnly = True
        Me.Y17.Size = New System.Drawing.Size(61, 20)
        Me.Y17.TabIndex = 0
        Me.Y17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M17
        '
        Me.M17.BackColor = System.Drawing.SystemColors.Window
        Me.M17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M17.Location = New System.Drawing.Point(50, 395)
        Me.M17.MaxLength = 7
        Me.M17.Name = "M17"
        Me.M17.ReadOnly = True
        Me.M17.Size = New System.Drawing.Size(61, 20)
        Me.M17.TabIndex = 0
        Me.M17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E13
        '
        Me.E13.BackColor = System.Drawing.SystemColors.Window
        Me.E13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E13.Location = New System.Drawing.Point(499, 303)
        Me.E13.MaxLength = 6
        Me.E13.Name = "E13"
        Me.E13.ReadOnly = True
        Me.E13.Size = New System.Drawing.Size(35, 20)
        Me.E13.TabIndex = 0
        Me.E13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D13
        '
        Me.D13.BackColor = System.Drawing.SystemColors.Window
        Me.D13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D13.Location = New System.Drawing.Point(458, 303)
        Me.D13.MaxLength = 6
        Me.D13.Name = "D13"
        Me.D13.ReadOnly = True
        Me.D13.Size = New System.Drawing.Size(35, 20)
        Me.D13.TabIndex = 0
        Me.D13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C13
        '
        Me.C13.BackColor = System.Drawing.SystemColors.Window
        Me.C13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C13.Location = New System.Drawing.Point(417, 303)
        Me.C13.MaxLength = 6
        Me.C13.Name = "C13"
        Me.C13.ReadOnly = True
        Me.C13.Size = New System.Drawing.Size(35, 20)
        Me.C13.TabIndex = 0
        Me.C13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B13
        '
        Me.B13.BackColor = System.Drawing.SystemColors.Window
        Me.B13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B13.Location = New System.Drawing.Point(376, 303)
        Me.B13.MaxLength = 6
        Me.B13.Name = "B13"
        Me.B13.ReadOnly = True
        Me.B13.Size = New System.Drawing.Size(35, 20)
        Me.B13.TabIndex = 0
        Me.B13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A13
        '
        Me.A13.BackColor = System.Drawing.SystemColors.Window
        Me.A13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A13.Location = New System.Drawing.Point(335, 303)
        Me.A13.MaxLength = 6
        Me.A13.Name = "A13"
        Me.A13.ReadOnly = True
        Me.A13.Size = New System.Drawing.Size(35, 20)
        Me.A13.TabIndex = 0
        Me.A13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K13
        '
        Me.K13.BackColor = System.Drawing.SystemColors.Window
        Me.K13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K13.Location = New System.Drawing.Point(133, 303)
        Me.K13.MaxLength = 50
        Me.K13.Name = "K13"
        Me.K13.ReadOnly = True
        Me.K13.Size = New System.Drawing.Size(196, 20)
        Me.K13.TabIndex = 0
        '
        'Y13
        '
        Me.Y13.BackColor = System.Drawing.SystemColors.Window
        Me.Y13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y13.Location = New System.Drawing.Point(553, 303)
        Me.Y13.Name = "Y13"
        Me.Y13.ReadOnly = True
        Me.Y13.Size = New System.Drawing.Size(61, 20)
        Me.Y13.TabIndex = 0
        Me.Y13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M13
        '
        Me.M13.BackColor = System.Drawing.SystemColors.Window
        Me.M13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M13.Location = New System.Drawing.Point(50, 303)
        Me.M13.MaxLength = 7
        Me.M13.Name = "M13"
        Me.M13.ReadOnly = True
        Me.M13.Size = New System.Drawing.Size(61, 20)
        Me.M13.TabIndex = 0
        Me.M13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E5
        '
        Me.E5.BackColor = System.Drawing.SystemColors.Window
        Me.E5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E5.Location = New System.Drawing.Point(499, 119)
        Me.E5.MaxLength = 6
        Me.E5.Name = "E5"
        Me.E5.ReadOnly = True
        Me.E5.Size = New System.Drawing.Size(35, 20)
        Me.E5.TabIndex = 0
        Me.E5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D5
        '
        Me.D5.BackColor = System.Drawing.SystemColors.Window
        Me.D5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D5.Location = New System.Drawing.Point(458, 119)
        Me.D5.MaxLength = 6
        Me.D5.Name = "D5"
        Me.D5.ReadOnly = True
        Me.D5.Size = New System.Drawing.Size(35, 20)
        Me.D5.TabIndex = 0
        Me.D5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C5
        '
        Me.C5.BackColor = System.Drawing.SystemColors.Window
        Me.C5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C5.Location = New System.Drawing.Point(417, 119)
        Me.C5.MaxLength = 6
        Me.C5.Name = "C5"
        Me.C5.ReadOnly = True
        Me.C5.Size = New System.Drawing.Size(35, 20)
        Me.C5.TabIndex = 0
        Me.C5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B5
        '
        Me.B5.BackColor = System.Drawing.SystemColors.Window
        Me.B5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B5.Location = New System.Drawing.Point(376, 119)
        Me.B5.MaxLength = 6
        Me.B5.Name = "B5"
        Me.B5.ReadOnly = True
        Me.B5.Size = New System.Drawing.Size(35, 20)
        Me.B5.TabIndex = 0
        Me.B5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A5
        '
        Me.A5.BackColor = System.Drawing.SystemColors.Window
        Me.A5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A5.Location = New System.Drawing.Point(335, 119)
        Me.A5.MaxLength = 6
        Me.A5.Name = "A5"
        Me.A5.ReadOnly = True
        Me.A5.Size = New System.Drawing.Size(35, 20)
        Me.A5.TabIndex = 0
        Me.A5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K5
        '
        Me.K5.BackColor = System.Drawing.SystemColors.Window
        Me.K5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K5.Location = New System.Drawing.Point(133, 119)
        Me.K5.MaxLength = 50
        Me.K5.Name = "K5"
        Me.K5.ReadOnly = True
        Me.K5.Size = New System.Drawing.Size(196, 20)
        Me.K5.TabIndex = 0
        '
        'Y5
        '
        Me.Y5.BackColor = System.Drawing.SystemColors.Window
        Me.Y5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y5.Location = New System.Drawing.Point(553, 119)
        Me.Y5.Name = "Y5"
        Me.Y5.ReadOnly = True
        Me.Y5.Size = New System.Drawing.Size(61, 20)
        Me.Y5.TabIndex = 0
        Me.Y5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M5
        '
        Me.M5.BackColor = System.Drawing.SystemColors.Window
        Me.M5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M5.Location = New System.Drawing.Point(50, 119)
        Me.M5.MaxLength = 7
        Me.M5.Name = "M5"
        Me.M5.ReadOnly = True
        Me.M5.Size = New System.Drawing.Size(61, 20)
        Me.M5.TabIndex = 0
        Me.M5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E9
        '
        Me.E9.BackColor = System.Drawing.SystemColors.Window
        Me.E9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E9.Location = New System.Drawing.Point(499, 211)
        Me.E9.MaxLength = 6
        Me.E9.Name = "E9"
        Me.E9.ReadOnly = True
        Me.E9.Size = New System.Drawing.Size(35, 20)
        Me.E9.TabIndex = 0
        Me.E9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D9
        '
        Me.D9.BackColor = System.Drawing.SystemColors.Window
        Me.D9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D9.Location = New System.Drawing.Point(458, 211)
        Me.D9.MaxLength = 6
        Me.D9.Name = "D9"
        Me.D9.ReadOnly = True
        Me.D9.Size = New System.Drawing.Size(35, 20)
        Me.D9.TabIndex = 0
        Me.D9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C9
        '
        Me.C9.BackColor = System.Drawing.SystemColors.Window
        Me.C9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C9.Location = New System.Drawing.Point(417, 211)
        Me.C9.MaxLength = 6
        Me.C9.Name = "C9"
        Me.C9.ReadOnly = True
        Me.C9.Size = New System.Drawing.Size(35, 20)
        Me.C9.TabIndex = 0
        Me.C9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B9
        '
        Me.B9.BackColor = System.Drawing.SystemColors.Window
        Me.B9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B9.Location = New System.Drawing.Point(376, 211)
        Me.B9.MaxLength = 6
        Me.B9.Name = "B9"
        Me.B9.ReadOnly = True
        Me.B9.Size = New System.Drawing.Size(35, 20)
        Me.B9.TabIndex = 0
        Me.B9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A9
        '
        Me.A9.BackColor = System.Drawing.SystemColors.Window
        Me.A9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A9.Location = New System.Drawing.Point(335, 211)
        Me.A9.MaxLength = 6
        Me.A9.Name = "A9"
        Me.A9.ReadOnly = True
        Me.A9.Size = New System.Drawing.Size(35, 20)
        Me.A9.TabIndex = 0
        Me.A9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K9
        '
        Me.K9.BackColor = System.Drawing.SystemColors.Window
        Me.K9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K9.Location = New System.Drawing.Point(133, 211)
        Me.K9.MaxLength = 50
        Me.K9.Name = "K9"
        Me.K9.ReadOnly = True
        Me.K9.Size = New System.Drawing.Size(196, 20)
        Me.K9.TabIndex = 0
        '
        'Y9
        '
        Me.Y9.BackColor = System.Drawing.SystemColors.Window
        Me.Y9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y9.Location = New System.Drawing.Point(553, 211)
        Me.Y9.Name = "Y9"
        Me.Y9.ReadOnly = True
        Me.Y9.Size = New System.Drawing.Size(61, 20)
        Me.Y9.TabIndex = 0
        Me.Y9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M9
        '
        Me.M9.BackColor = System.Drawing.SystemColors.Window
        Me.M9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M9.Location = New System.Drawing.Point(50, 211)
        Me.M9.MaxLength = 7
        Me.M9.Name = "M9"
        Me.M9.ReadOnly = True
        Me.M9.Size = New System.Drawing.Size(61, 20)
        Me.M9.TabIndex = 0
        Me.M9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'E1
        '
        Me.E1.BackColor = System.Drawing.SystemColors.Window
        Me.E1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.E1.Location = New System.Drawing.Point(499, 27)
        Me.E1.MaxLength = 6
        Me.E1.Name = "E1"
        Me.E1.ReadOnly = True
        Me.E1.Size = New System.Drawing.Size(35, 20)
        Me.E1.TabIndex = 0
        Me.E1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'D1
        '
        Me.D1.BackColor = System.Drawing.SystemColors.Window
        Me.D1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.D1.Location = New System.Drawing.Point(458, 27)
        Me.D1.MaxLength = 6
        Me.D1.Name = "D1"
        Me.D1.ReadOnly = True
        Me.D1.Size = New System.Drawing.Size(35, 20)
        Me.D1.TabIndex = 0
        Me.D1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'C1
        '
        Me.C1.BackColor = System.Drawing.SystemColors.Window
        Me.C1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.C1.Location = New System.Drawing.Point(417, 27)
        Me.C1.MaxLength = 6
        Me.C1.Name = "C1"
        Me.C1.ReadOnly = True
        Me.C1.Size = New System.Drawing.Size(35, 20)
        Me.C1.TabIndex = 0
        Me.C1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'B1
        '
        Me.B1.BackColor = System.Drawing.SystemColors.Window
        Me.B1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.B1.Location = New System.Drawing.Point(376, 27)
        Me.B1.MaxLength = 6
        Me.B1.Name = "B1"
        Me.B1.ReadOnly = True
        Me.B1.Size = New System.Drawing.Size(35, 20)
        Me.B1.TabIndex = 0
        Me.B1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'A1
        '
        Me.A1.BackColor = System.Drawing.SystemColors.Window
        Me.A1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.A1.Location = New System.Drawing.Point(335, 27)
        Me.A1.MaxLength = 6
        Me.A1.Name = "A1"
        Me.A1.ReadOnly = True
        Me.A1.Size = New System.Drawing.Size(35, 20)
        Me.A1.TabIndex = 0
        Me.A1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'K1
        '
        Me.K1.BackColor = System.Drawing.SystemColors.Window
        Me.K1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.K1.Location = New System.Drawing.Point(133, 27)
        Me.K1.MaxLength = 50
        Me.K1.Name = "K1"
        Me.K1.ReadOnly = True
        Me.K1.Size = New System.Drawing.Size(196, 20)
        Me.K1.TabIndex = 0
        '
        'Y1
        '
        Me.Y1.BackColor = System.Drawing.SystemColors.Window
        Me.Y1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Y1.Location = New System.Drawing.Point(553, 27)
        Me.Y1.Name = "Y1"
        Me.Y1.ReadOnly = True
        Me.Y1.Size = New System.Drawing.Size(61, 20)
        Me.Y1.TabIndex = 0
        Me.Y1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'M1
        '
        Me.M1.BackColor = System.Drawing.SystemColors.Window
        Me.M1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.M1.Location = New System.Drawing.Point(50, 27)
        Me.M1.MaxLength = 7
        Me.M1.Name = "M1"
        Me.M1.ReadOnly = True
        Me.M1.Size = New System.Drawing.Size(61, 20)
        Me.M1.TabIndex = 0
        Me.M1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.SystemColors.Control
        Me.Panel4.Controls.Add(Me.EW25)
        Me.Panel4.Controls.Add(Me.DW25)
        Me.Panel4.Controls.Add(Me.CW25)
        Me.Panel4.Controls.Add(Me.BW25)
        Me.Panel4.Controls.Add(Me.AW25)
        Me.Panel4.Controls.Add(Me.EW21)
        Me.Panel4.Controls.Add(Me.DW21)
        Me.Panel4.Controls.Add(Me.CW21)
        Me.Panel4.Controls.Add(Me.BW21)
        Me.Panel4.Controls.Add(Me.AW21)
        Me.Panel4.Controls.Add(Me.EW23)
        Me.Panel4.Controls.Add(Me.DW23)
        Me.Panel4.Controls.Add(Me.CW23)
        Me.Panel4.Controls.Add(Me.BW23)
        Me.Panel4.Controls.Add(Me.AW23)
        Me.Panel4.Controls.Add(Me.EW24)
        Me.Panel4.Controls.Add(Me.DW24)
        Me.Panel4.Controls.Add(Me.CW24)
        Me.Panel4.Controls.Add(Me.BW24)
        Me.Panel4.Controls.Add(Me.AW24)
        Me.Panel4.Controls.Add(Me.EW22)
        Me.Panel4.Controls.Add(Me.DW22)
        Me.Panel4.Controls.Add(Me.CW22)
        Me.Panel4.Controls.Add(Me.BW22)
        Me.Panel4.Controls.Add(Me.AW22)
        Me.Panel4.Controls.Add(Me.EW20)
        Me.Panel4.Controls.Add(Me.DW20)
        Me.Panel4.Controls.Add(Me.CW20)
        Me.Panel4.Controls.Add(Me.BW20)
        Me.Panel4.Controls.Add(Me.AW20)
        Me.Panel4.Controls.Add(Me.EW16)
        Me.Panel4.Controls.Add(Me.DW16)
        Me.Panel4.Controls.Add(Me.CW16)
        Me.Panel4.Controls.Add(Me.BW16)
        Me.Panel4.Controls.Add(Me.AW16)
        Me.Panel4.Controls.Add(Me.EW8)
        Me.Panel4.Controls.Add(Me.DW8)
        Me.Panel4.Controls.Add(Me.CW8)
        Me.Panel4.Controls.Add(Me.BW8)
        Me.Panel4.Controls.Add(Me.AW8)
        Me.Panel4.Controls.Add(Me.EW12)
        Me.Panel4.Controls.Add(Me.DW12)
        Me.Panel4.Controls.Add(Me.CW12)
        Me.Panel4.Controls.Add(Me.BW12)
        Me.Panel4.Controls.Add(Me.AW12)
        Me.Panel4.Controls.Add(Me.EW4)
        Me.Panel4.Controls.Add(Me.DW4)
        Me.Panel4.Controls.Add(Me.CW4)
        Me.Panel4.Controls.Add(Me.BW4)
        Me.Panel4.Controls.Add(Me.AW4)
        Me.Panel4.Controls.Add(Me.EW18)
        Me.Panel4.Controls.Add(Me.DW18)
        Me.Panel4.Controls.Add(Me.CW18)
        Me.Panel4.Controls.Add(Me.BW18)
        Me.Panel4.Controls.Add(Me.AW18)
        Me.Panel4.Controls.Add(Me.EW14)
        Me.Panel4.Controls.Add(Me.DW14)
        Me.Panel4.Controls.Add(Me.CW14)
        Me.Panel4.Controls.Add(Me.BW14)
        Me.Panel4.Controls.Add(Me.AW14)
        Me.Panel4.Controls.Add(Me.EW6)
        Me.Panel4.Controls.Add(Me.DW6)
        Me.Panel4.Controls.Add(Me.CW6)
        Me.Panel4.Controls.Add(Me.BW6)
        Me.Panel4.Controls.Add(Me.AW6)
        Me.Panel4.Controls.Add(Me.EW10)
        Me.Panel4.Controls.Add(Me.DW10)
        Me.Panel4.Controls.Add(Me.CW10)
        Me.Panel4.Controls.Add(Me.BW10)
        Me.Panel4.Controls.Add(Me.AW10)
        Me.Panel4.Controls.Add(Me.EW2)
        Me.Panel4.Controls.Add(Me.DW2)
        Me.Panel4.Controls.Add(Me.CW2)
        Me.Panel4.Controls.Add(Me.BW2)
        Me.Panel4.Controls.Add(Me.AW2)
        Me.Panel4.Controls.Add(Me.EW19)
        Me.Panel4.Controls.Add(Me.DW19)
        Me.Panel4.Controls.Add(Me.CW19)
        Me.Panel4.Controls.Add(Me.BW19)
        Me.Panel4.Controls.Add(Me.AW19)
        Me.Panel4.Controls.Add(Me.EW15)
        Me.Panel4.Controls.Add(Me.DW15)
        Me.Panel4.Controls.Add(Me.CW15)
        Me.Panel4.Controls.Add(Me.BW15)
        Me.Panel4.Controls.Add(Me.AW15)
        Me.Panel4.Controls.Add(Me.EW7)
        Me.Panel4.Controls.Add(Me.DW7)
        Me.Panel4.Controls.Add(Me.CW7)
        Me.Panel4.Controls.Add(Me.BW7)
        Me.Panel4.Controls.Add(Me.AW7)
        Me.Panel4.Controls.Add(Me.EW11)
        Me.Panel4.Controls.Add(Me.DW11)
        Me.Panel4.Controls.Add(Me.CW11)
        Me.Panel4.Controls.Add(Me.BW11)
        Me.Panel4.Controls.Add(Me.AW11)
        Me.Panel4.Controls.Add(Me.EW3)
        Me.Panel4.Controls.Add(Me.DW3)
        Me.Panel4.Controls.Add(Me.CW3)
        Me.Panel4.Controls.Add(Me.BW3)
        Me.Panel4.Controls.Add(Me.AW3)
        Me.Panel4.Controls.Add(Me.EW17)
        Me.Panel4.Controls.Add(Me.DW17)
        Me.Panel4.Controls.Add(Me.CW17)
        Me.Panel4.Controls.Add(Me.BW17)
        Me.Panel4.Controls.Add(Me.AW17)
        Me.Panel4.Controls.Add(Me.EW13)
        Me.Panel4.Controls.Add(Me.DW13)
        Me.Panel4.Controls.Add(Me.CW13)
        Me.Panel4.Controls.Add(Me.BW13)
        Me.Panel4.Controls.Add(Me.AW13)
        Me.Panel4.Controls.Add(Me.EW5)
        Me.Panel4.Controls.Add(Me.DW5)
        Me.Panel4.Controls.Add(Me.CW5)
        Me.Panel4.Controls.Add(Me.BW5)
        Me.Panel4.Controls.Add(Me.AW5)
        Me.Panel4.Controls.Add(Me.EW9)
        Me.Panel4.Controls.Add(Me.DW9)
        Me.Panel4.Controls.Add(Me.CW9)
        Me.Panel4.Controls.Add(Me.BW9)
        Me.Panel4.Controls.Add(Me.AW9)
        Me.Panel4.Controls.Add(Me.EW1)
        Me.Panel4.Controls.Add(Me.DW1)
        Me.Panel4.Controls.Add(Me.CW1)
        Me.Panel4.Controls.Add(Me.BW1)
        Me.Panel4.Controls.Add(Me.AW1)
        Me.Panel4.Location = New System.Drawing.Point(441, 17)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(159, 608)
        Me.Panel4.TabIndex = 114
        '
        'EW25
        '
        Me.EW25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW25.Location = New System.Drawing.Point(125, 570)
        Me.EW25.MaxLength = 7
        Me.EW25.Name = "EW25"
        Me.EW25.Size = New System.Drawing.Size(30, 20)
        Me.EW25.TabIndex = 192
        Me.EW25.Text = "W"
        Me.EW25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW25
        '
        Me.DW25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW25.Location = New System.Drawing.Point(95, 570)
        Me.DW25.MaxLength = 7
        Me.DW25.Name = "DW25"
        Me.DW25.Size = New System.Drawing.Size(30, 20)
        Me.DW25.TabIndex = 191
        Me.DW25.Text = "W"
        Me.DW25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW25
        '
        Me.CW25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW25.Location = New System.Drawing.Point(65, 570)
        Me.CW25.MaxLength = 7
        Me.CW25.Name = "CW25"
        Me.CW25.Size = New System.Drawing.Size(30, 20)
        Me.CW25.TabIndex = 190
        Me.CW25.Text = "W"
        Me.CW25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW25
        '
        Me.BW25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW25.Location = New System.Drawing.Point(35, 570)
        Me.BW25.MaxLength = 7
        Me.BW25.Name = "BW25"
        Me.BW25.Size = New System.Drawing.Size(30, 20)
        Me.BW25.TabIndex = 189
        Me.BW25.Text = "W"
        Me.BW25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW25
        '
        Me.AW25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW25.Location = New System.Drawing.Point(5, 570)
        Me.AW25.MaxLength = 7
        Me.AW25.Name = "AW25"
        Me.AW25.Size = New System.Drawing.Size(30, 20)
        Me.AW25.TabIndex = 188
        Me.AW25.Text = "W"
        Me.AW25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW21
        '
        Me.EW21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW21.Location = New System.Drawing.Point(125, 478)
        Me.EW21.MaxLength = 7
        Me.EW21.Name = "EW21"
        Me.EW21.Size = New System.Drawing.Size(30, 20)
        Me.EW21.TabIndex = 183
        Me.EW21.Text = "W"
        Me.EW21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW21
        '
        Me.DW21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW21.Location = New System.Drawing.Point(95, 478)
        Me.DW21.MaxLength = 7
        Me.DW21.Name = "DW21"
        Me.DW21.Size = New System.Drawing.Size(30, 20)
        Me.DW21.TabIndex = 184
        Me.DW21.Text = "W"
        Me.DW21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW21
        '
        Me.CW21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW21.Location = New System.Drawing.Point(65, 478)
        Me.CW21.MaxLength = 7
        Me.CW21.Name = "CW21"
        Me.CW21.Size = New System.Drawing.Size(30, 20)
        Me.CW21.TabIndex = 187
        Me.CW21.Text = "W"
        Me.CW21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW21
        '
        Me.BW21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW21.Location = New System.Drawing.Point(35, 478)
        Me.BW21.MaxLength = 7
        Me.BW21.Name = "BW21"
        Me.BW21.Size = New System.Drawing.Size(30, 20)
        Me.BW21.TabIndex = 186
        Me.BW21.Text = "W"
        Me.BW21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW21
        '
        Me.AW21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW21.Location = New System.Drawing.Point(5, 478)
        Me.AW21.MaxLength = 7
        Me.AW21.Name = "AW21"
        Me.AW21.Size = New System.Drawing.Size(30, 20)
        Me.AW21.TabIndex = 185
        Me.AW21.Text = "W"
        Me.AW21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW23
        '
        Me.EW23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW23.Location = New System.Drawing.Point(125, 524)
        Me.EW23.MaxLength = 7
        Me.EW23.Name = "EW23"
        Me.EW23.Size = New System.Drawing.Size(30, 20)
        Me.EW23.TabIndex = 168
        Me.EW23.Text = "W"
        Me.EW23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW23
        '
        Me.DW23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW23.Location = New System.Drawing.Point(95, 524)
        Me.DW23.MaxLength = 7
        Me.DW23.Name = "DW23"
        Me.DW23.Size = New System.Drawing.Size(30, 20)
        Me.DW23.TabIndex = 169
        Me.DW23.Text = "W"
        Me.DW23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW23
        '
        Me.CW23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW23.Location = New System.Drawing.Point(65, 524)
        Me.CW23.MaxLength = 7
        Me.CW23.Name = "CW23"
        Me.CW23.Size = New System.Drawing.Size(30, 20)
        Me.CW23.TabIndex = 170
        Me.CW23.Text = "W"
        Me.CW23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW23
        '
        Me.BW23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW23.Location = New System.Drawing.Point(35, 524)
        Me.BW23.MaxLength = 7
        Me.BW23.Name = "BW23"
        Me.BW23.Size = New System.Drawing.Size(30, 20)
        Me.BW23.TabIndex = 172
        Me.BW23.Text = "W"
        Me.BW23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW23
        '
        Me.AW23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW23.Location = New System.Drawing.Point(5, 524)
        Me.AW23.MaxLength = 7
        Me.AW23.Name = "AW23"
        Me.AW23.Size = New System.Drawing.Size(30, 20)
        Me.AW23.TabIndex = 171
        Me.AW23.Text = "W"
        Me.AW23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW24
        '
        Me.EW24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW24.Location = New System.Drawing.Point(125, 547)
        Me.EW24.MaxLength = 7
        Me.EW24.Name = "EW24"
        Me.EW24.Size = New System.Drawing.Size(30, 20)
        Me.EW24.TabIndex = 176
        Me.EW24.Text = "W"
        Me.EW24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW24
        '
        Me.DW24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW24.Location = New System.Drawing.Point(95, 547)
        Me.DW24.MaxLength = 7
        Me.DW24.Name = "DW24"
        Me.DW24.Size = New System.Drawing.Size(30, 20)
        Me.DW24.TabIndex = 175
        Me.DW24.Text = "W"
        Me.DW24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW24
        '
        Me.CW24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW24.Location = New System.Drawing.Point(65, 547)
        Me.CW24.MaxLength = 7
        Me.CW24.Name = "CW24"
        Me.CW24.Size = New System.Drawing.Size(30, 20)
        Me.CW24.TabIndex = 174
        Me.CW24.Text = "W"
        Me.CW24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW24
        '
        Me.BW24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW24.Location = New System.Drawing.Point(35, 547)
        Me.BW24.MaxLength = 7
        Me.BW24.Name = "BW24"
        Me.BW24.Size = New System.Drawing.Size(30, 20)
        Me.BW24.TabIndex = 173
        Me.BW24.Text = "W"
        Me.BW24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW24
        '
        Me.AW24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW24.Location = New System.Drawing.Point(5, 547)
        Me.AW24.MaxLength = 7
        Me.AW24.Name = "AW24"
        Me.AW24.Size = New System.Drawing.Size(30, 20)
        Me.AW24.TabIndex = 177
        Me.AW24.Text = "W"
        Me.AW24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW22
        '
        Me.EW22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW22.Location = New System.Drawing.Point(125, 501)
        Me.EW22.MaxLength = 7
        Me.EW22.Name = "EW22"
        Me.EW22.Size = New System.Drawing.Size(30, 20)
        Me.EW22.TabIndex = 178
        Me.EW22.Text = "W"
        Me.EW22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW22
        '
        Me.DW22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW22.Location = New System.Drawing.Point(95, 501)
        Me.DW22.MaxLength = 7
        Me.DW22.Name = "DW22"
        Me.DW22.Size = New System.Drawing.Size(30, 20)
        Me.DW22.TabIndex = 179
        Me.DW22.Text = "W"
        Me.DW22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW22
        '
        Me.CW22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW22.Location = New System.Drawing.Point(65, 501)
        Me.CW22.MaxLength = 7
        Me.CW22.Name = "CW22"
        Me.CW22.Size = New System.Drawing.Size(30, 20)
        Me.CW22.TabIndex = 180
        Me.CW22.Text = "W"
        Me.CW22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW22
        '
        Me.BW22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW22.Location = New System.Drawing.Point(35, 501)
        Me.BW22.MaxLength = 7
        Me.BW22.Name = "BW22"
        Me.BW22.Size = New System.Drawing.Size(30, 20)
        Me.BW22.TabIndex = 181
        Me.BW22.Text = "W"
        Me.BW22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW22
        '
        Me.AW22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW22.Location = New System.Drawing.Point(5, 501)
        Me.AW22.MaxLength = 7
        Me.AW22.Name = "AW22"
        Me.AW22.Size = New System.Drawing.Size(30, 20)
        Me.AW22.TabIndex = 182
        Me.AW22.Text = "W"
        Me.AW22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW20
        '
        Me.EW20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW20.Location = New System.Drawing.Point(125, 455)
        Me.EW20.MaxLength = 7
        Me.EW20.Name = "EW20"
        Me.EW20.Size = New System.Drawing.Size(30, 20)
        Me.EW20.TabIndex = 161
        Me.EW20.Text = "W"
        Me.EW20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW20
        '
        Me.DW20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW20.Location = New System.Drawing.Point(95, 455)
        Me.DW20.MaxLength = 7
        Me.DW20.Name = "DW20"
        Me.DW20.Size = New System.Drawing.Size(30, 20)
        Me.DW20.TabIndex = 160
        Me.DW20.Text = "W"
        Me.DW20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW20
        '
        Me.CW20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW20.Location = New System.Drawing.Point(65, 455)
        Me.CW20.MaxLength = 7
        Me.CW20.Name = "CW20"
        Me.CW20.Size = New System.Drawing.Size(30, 20)
        Me.CW20.TabIndex = 159
        Me.CW20.Text = "W"
        Me.CW20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW20
        '
        Me.BW20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW20.Location = New System.Drawing.Point(35, 455)
        Me.BW20.MaxLength = 7
        Me.BW20.Name = "BW20"
        Me.BW20.Size = New System.Drawing.Size(30, 20)
        Me.BW20.TabIndex = 158
        Me.BW20.Text = "W"
        Me.BW20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW20
        '
        Me.AW20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW20.Location = New System.Drawing.Point(5, 455)
        Me.AW20.MaxLength = 7
        Me.AW20.Name = "AW20"
        Me.AW20.Size = New System.Drawing.Size(30, 20)
        Me.AW20.TabIndex = 162
        Me.AW20.Text = "W"
        Me.AW20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW16
        '
        Me.EW16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW16.Location = New System.Drawing.Point(125, 363)
        Me.EW16.MaxLength = 7
        Me.EW16.Name = "EW16"
        Me.EW16.Size = New System.Drawing.Size(30, 20)
        Me.EW16.TabIndex = 157
        Me.EW16.Text = "W"
        Me.EW16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW16
        '
        Me.DW16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW16.Location = New System.Drawing.Point(95, 363)
        Me.DW16.MaxLength = 7
        Me.DW16.Name = "DW16"
        Me.DW16.Size = New System.Drawing.Size(30, 20)
        Me.DW16.TabIndex = 156
        Me.DW16.Text = "W"
        Me.DW16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW16
        '
        Me.CW16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW16.Location = New System.Drawing.Point(65, 363)
        Me.CW16.MaxLength = 7
        Me.CW16.Name = "CW16"
        Me.CW16.Size = New System.Drawing.Size(30, 20)
        Me.CW16.TabIndex = 155
        Me.CW16.Text = "W"
        Me.CW16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW16
        '
        Me.BW16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW16.Location = New System.Drawing.Point(35, 363)
        Me.BW16.MaxLength = 7
        Me.BW16.Name = "BW16"
        Me.BW16.Size = New System.Drawing.Size(30, 20)
        Me.BW16.TabIndex = 154
        Me.BW16.Text = "W"
        Me.BW16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW16
        '
        Me.AW16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW16.Location = New System.Drawing.Point(5, 363)
        Me.AW16.MaxLength = 7
        Me.AW16.Name = "AW16"
        Me.AW16.Size = New System.Drawing.Size(30, 20)
        Me.AW16.TabIndex = 153
        Me.AW16.Text = "W"
        Me.AW16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW8
        '
        Me.EW8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW8.Location = New System.Drawing.Point(125, 179)
        Me.EW8.MaxLength = 7
        Me.EW8.Name = "EW8"
        Me.EW8.Size = New System.Drawing.Size(30, 20)
        Me.EW8.TabIndex = 152
        Me.EW8.Text = "W"
        Me.EW8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW8
        '
        Me.DW8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW8.Location = New System.Drawing.Point(95, 179)
        Me.DW8.MaxLength = 7
        Me.DW8.Name = "DW8"
        Me.DW8.Size = New System.Drawing.Size(30, 20)
        Me.DW8.TabIndex = 151
        Me.DW8.Text = "W"
        Me.DW8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW8
        '
        Me.CW8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW8.Location = New System.Drawing.Point(65, 179)
        Me.CW8.MaxLength = 7
        Me.CW8.Name = "CW8"
        Me.CW8.Size = New System.Drawing.Size(30, 20)
        Me.CW8.TabIndex = 150
        Me.CW8.Text = "W"
        Me.CW8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW8
        '
        Me.BW8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW8.Location = New System.Drawing.Point(35, 179)
        Me.BW8.MaxLength = 7
        Me.BW8.Name = "BW8"
        Me.BW8.Size = New System.Drawing.Size(30, 20)
        Me.BW8.TabIndex = 149
        Me.BW8.Text = "W"
        Me.BW8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW8
        '
        Me.AW8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW8.Location = New System.Drawing.Point(5, 179)
        Me.AW8.MaxLength = 7
        Me.AW8.Name = "AW8"
        Me.AW8.Size = New System.Drawing.Size(30, 20)
        Me.AW8.TabIndex = 148
        Me.AW8.Text = "W"
        Me.AW8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW12
        '
        Me.EW12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW12.Location = New System.Drawing.Point(125, 271)
        Me.EW12.MaxLength = 7
        Me.EW12.Name = "EW12"
        Me.EW12.Size = New System.Drawing.Size(30, 20)
        Me.EW12.TabIndex = 143
        Me.EW12.Text = "W"
        Me.EW12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW12
        '
        Me.DW12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW12.Location = New System.Drawing.Point(95, 271)
        Me.DW12.MaxLength = 7
        Me.DW12.Name = "DW12"
        Me.DW12.Size = New System.Drawing.Size(30, 20)
        Me.DW12.TabIndex = 144
        Me.DW12.Text = "W"
        Me.DW12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW12
        '
        Me.CW12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW12.Location = New System.Drawing.Point(65, 271)
        Me.CW12.MaxLength = 7
        Me.CW12.Name = "CW12"
        Me.CW12.Size = New System.Drawing.Size(30, 20)
        Me.CW12.TabIndex = 145
        Me.CW12.Text = "W"
        Me.CW12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW12
        '
        Me.BW12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW12.Location = New System.Drawing.Point(35, 271)
        Me.BW12.MaxLength = 7
        Me.BW12.Name = "BW12"
        Me.BW12.Size = New System.Drawing.Size(30, 20)
        Me.BW12.TabIndex = 146
        Me.BW12.Text = "W"
        Me.BW12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW12
        '
        Me.AW12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW12.Location = New System.Drawing.Point(5, 271)
        Me.AW12.MaxLength = 7
        Me.AW12.Name = "AW12"
        Me.AW12.Size = New System.Drawing.Size(30, 20)
        Me.AW12.TabIndex = 147
        Me.AW12.Text = "W"
        Me.AW12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW4
        '
        Me.EW4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW4.Location = New System.Drawing.Point(125, 87)
        Me.EW4.MaxLength = 7
        Me.EW4.Name = "EW4"
        Me.EW4.Size = New System.Drawing.Size(30, 20)
        Me.EW4.TabIndex = 142
        Me.EW4.Text = "W"
        Me.EW4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW4
        '
        Me.DW4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW4.Location = New System.Drawing.Point(95, 87)
        Me.DW4.MaxLength = 7
        Me.DW4.Name = "DW4"
        Me.DW4.Size = New System.Drawing.Size(30, 20)
        Me.DW4.TabIndex = 141
        Me.DW4.Text = "W"
        Me.DW4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW4
        '
        Me.CW4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW4.Location = New System.Drawing.Point(65, 87)
        Me.CW4.MaxLength = 7
        Me.CW4.Name = "CW4"
        Me.CW4.Size = New System.Drawing.Size(30, 20)
        Me.CW4.TabIndex = 140
        Me.CW4.Text = "W"
        Me.CW4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW4
        '
        Me.BW4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW4.Location = New System.Drawing.Point(35, 87)
        Me.BW4.MaxLength = 7
        Me.BW4.Name = "BW4"
        Me.BW4.Size = New System.Drawing.Size(30, 20)
        Me.BW4.TabIndex = 139
        Me.BW4.Text = "W"
        Me.BW4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW4
        '
        Me.AW4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW4.Location = New System.Drawing.Point(5, 87)
        Me.AW4.MaxLength = 7
        Me.AW4.Name = "AW4"
        Me.AW4.Size = New System.Drawing.Size(30, 20)
        Me.AW4.TabIndex = 138
        Me.AW4.Text = "W"
        Me.AW4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW18
        '
        Me.EW18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW18.Location = New System.Drawing.Point(125, 409)
        Me.EW18.MaxLength = 7
        Me.EW18.Name = "EW18"
        Me.EW18.Size = New System.Drawing.Size(30, 20)
        Me.EW18.TabIndex = 137
        Me.EW18.Text = "W"
        Me.EW18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW18
        '
        Me.DW18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW18.Location = New System.Drawing.Point(95, 409)
        Me.DW18.MaxLength = 7
        Me.DW18.Name = "DW18"
        Me.DW18.Size = New System.Drawing.Size(30, 20)
        Me.DW18.TabIndex = 136
        Me.DW18.Text = "W"
        Me.DW18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW18
        '
        Me.CW18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW18.Location = New System.Drawing.Point(65, 409)
        Me.CW18.MaxLength = 7
        Me.CW18.Name = "CW18"
        Me.CW18.Size = New System.Drawing.Size(30, 20)
        Me.CW18.TabIndex = 135
        Me.CW18.Text = "W"
        Me.CW18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW18
        '
        Me.BW18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW18.Location = New System.Drawing.Point(35, 409)
        Me.BW18.MaxLength = 7
        Me.BW18.Name = "BW18"
        Me.BW18.Size = New System.Drawing.Size(30, 20)
        Me.BW18.TabIndex = 134
        Me.BW18.Text = "W"
        Me.BW18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW18
        '
        Me.AW18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW18.Location = New System.Drawing.Point(5, 409)
        Me.AW18.MaxLength = 7
        Me.AW18.Name = "AW18"
        Me.AW18.Size = New System.Drawing.Size(30, 20)
        Me.AW18.TabIndex = 133
        Me.AW18.Text = "W"
        Me.AW18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW14
        '
        Me.EW14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW14.Location = New System.Drawing.Point(125, 317)
        Me.EW14.MaxLength = 7
        Me.EW14.Name = "EW14"
        Me.EW14.Size = New System.Drawing.Size(30, 20)
        Me.EW14.TabIndex = 167
        Me.EW14.Text = "W"
        Me.EW14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW14
        '
        Me.DW14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW14.Location = New System.Drawing.Point(95, 317)
        Me.DW14.MaxLength = 7
        Me.DW14.Name = "DW14"
        Me.DW14.Size = New System.Drawing.Size(30, 20)
        Me.DW14.TabIndex = 164
        Me.DW14.Text = "W"
        Me.DW14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW14
        '
        Me.CW14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW14.Location = New System.Drawing.Point(65, 317)
        Me.CW14.MaxLength = 7
        Me.CW14.Name = "CW14"
        Me.CW14.Size = New System.Drawing.Size(30, 20)
        Me.CW14.TabIndex = 165
        Me.CW14.Text = "W"
        Me.CW14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW14
        '
        Me.BW14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW14.Location = New System.Drawing.Point(35, 317)
        Me.BW14.MaxLength = 7
        Me.BW14.Name = "BW14"
        Me.BW14.Size = New System.Drawing.Size(30, 20)
        Me.BW14.TabIndex = 166
        Me.BW14.Text = "W"
        Me.BW14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW14
        '
        Me.AW14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW14.Location = New System.Drawing.Point(5, 317)
        Me.AW14.MaxLength = 7
        Me.AW14.Name = "AW14"
        Me.AW14.Size = New System.Drawing.Size(30, 20)
        Me.AW14.TabIndex = 163
        Me.AW14.Text = "W"
        Me.AW14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW6
        '
        Me.EW6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW6.Location = New System.Drawing.Point(125, 133)
        Me.EW6.MaxLength = 7
        Me.EW6.Name = "EW6"
        Me.EW6.Size = New System.Drawing.Size(30, 20)
        Me.EW6.TabIndex = 69
        Me.EW6.Text = "W"
        Me.EW6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW6
        '
        Me.DW6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW6.Location = New System.Drawing.Point(95, 133)
        Me.DW6.MaxLength = 7
        Me.DW6.Name = "DW6"
        Me.DW6.Size = New System.Drawing.Size(30, 20)
        Me.DW6.TabIndex = 71
        Me.DW6.Text = "W"
        Me.DW6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW6
        '
        Me.CW6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW6.Location = New System.Drawing.Point(65, 133)
        Me.CW6.MaxLength = 7
        Me.CW6.Name = "CW6"
        Me.CW6.Size = New System.Drawing.Size(30, 20)
        Me.CW6.TabIndex = 68
        Me.CW6.Text = "W"
        Me.CW6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW6
        '
        Me.BW6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW6.Location = New System.Drawing.Point(35, 133)
        Me.BW6.MaxLength = 7
        Me.BW6.Name = "BW6"
        Me.BW6.Size = New System.Drawing.Size(30, 20)
        Me.BW6.TabIndex = 72
        Me.BW6.Text = "W"
        Me.BW6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW6
        '
        Me.AW6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW6.Location = New System.Drawing.Point(5, 133)
        Me.AW6.MaxLength = 7
        Me.AW6.Name = "AW6"
        Me.AW6.Size = New System.Drawing.Size(30, 20)
        Me.AW6.TabIndex = 70
        Me.AW6.Text = "W"
        Me.AW6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW10
        '
        Me.EW10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW10.Location = New System.Drawing.Point(125, 225)
        Me.EW10.MaxLength = 7
        Me.EW10.Name = "EW10"
        Me.EW10.Size = New System.Drawing.Size(30, 20)
        Me.EW10.TabIndex = 121
        Me.EW10.Text = "W"
        Me.EW10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW10
        '
        Me.DW10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW10.Location = New System.Drawing.Point(95, 225)
        Me.DW10.MaxLength = 7
        Me.DW10.Name = "DW10"
        Me.DW10.Size = New System.Drawing.Size(30, 20)
        Me.DW10.TabIndex = 120
        Me.DW10.Text = "W"
        Me.DW10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW10
        '
        Me.CW10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW10.Location = New System.Drawing.Point(65, 225)
        Me.CW10.MaxLength = 7
        Me.CW10.Name = "CW10"
        Me.CW10.Size = New System.Drawing.Size(30, 20)
        Me.CW10.TabIndex = 118
        Me.CW10.Text = "W"
        Me.CW10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW10
        '
        Me.BW10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW10.Location = New System.Drawing.Point(35, 225)
        Me.BW10.MaxLength = 7
        Me.BW10.Name = "BW10"
        Me.BW10.Size = New System.Drawing.Size(30, 20)
        Me.BW10.TabIndex = 119
        Me.BW10.Text = "W"
        Me.BW10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW10
        '
        Me.AW10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW10.Location = New System.Drawing.Point(5, 225)
        Me.AW10.MaxLength = 7
        Me.AW10.Name = "AW10"
        Me.AW10.Size = New System.Drawing.Size(30, 20)
        Me.AW10.TabIndex = 122
        Me.AW10.Text = "W"
        Me.AW10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW2
        '
        Me.EW2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW2.Location = New System.Drawing.Point(125, 41)
        Me.EW2.MaxLength = 7
        Me.EW2.Name = "EW2"
        Me.EW2.Size = New System.Drawing.Size(30, 20)
        Me.EW2.TabIndex = 117
        Me.EW2.Text = "W"
        Me.EW2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW2
        '
        Me.DW2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW2.Location = New System.Drawing.Point(95, 41)
        Me.DW2.MaxLength = 7
        Me.DW2.Name = "DW2"
        Me.DW2.Size = New System.Drawing.Size(30, 20)
        Me.DW2.TabIndex = 116
        Me.DW2.Text = "W"
        Me.DW2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW2
        '
        Me.CW2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW2.Location = New System.Drawing.Point(65, 41)
        Me.CW2.MaxLength = 7
        Me.CW2.Name = "CW2"
        Me.CW2.Size = New System.Drawing.Size(30, 20)
        Me.CW2.TabIndex = 115
        Me.CW2.Text = "W"
        Me.CW2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW2
        '
        Me.BW2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW2.Location = New System.Drawing.Point(35, 41)
        Me.BW2.MaxLength = 7
        Me.BW2.Name = "BW2"
        Me.BW2.Size = New System.Drawing.Size(30, 20)
        Me.BW2.TabIndex = 114
        Me.BW2.Text = "W"
        Me.BW2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW2
        '
        Me.AW2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW2.Location = New System.Drawing.Point(5, 41)
        Me.AW2.MaxLength = 7
        Me.AW2.Name = "AW2"
        Me.AW2.Size = New System.Drawing.Size(30, 20)
        Me.AW2.TabIndex = 113
        Me.AW2.Text = "W"
        Me.AW2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW19
        '
        Me.EW19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW19.Location = New System.Drawing.Point(125, 432)
        Me.EW19.MaxLength = 7
        Me.EW19.Name = "EW19"
        Me.EW19.Size = New System.Drawing.Size(30, 20)
        Me.EW19.TabIndex = 108
        Me.EW19.Text = "W"
        Me.EW19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW19
        '
        Me.DW19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW19.Location = New System.Drawing.Point(95, 432)
        Me.DW19.MaxLength = 7
        Me.DW19.Name = "DW19"
        Me.DW19.Size = New System.Drawing.Size(30, 20)
        Me.DW19.TabIndex = 109
        Me.DW19.Text = "W"
        Me.DW19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW19
        '
        Me.CW19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW19.Location = New System.Drawing.Point(65, 432)
        Me.CW19.MaxLength = 7
        Me.CW19.Name = "CW19"
        Me.CW19.Size = New System.Drawing.Size(30, 20)
        Me.CW19.TabIndex = 112
        Me.CW19.Text = "W"
        Me.CW19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW19
        '
        Me.BW19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW19.Location = New System.Drawing.Point(35, 432)
        Me.BW19.MaxLength = 7
        Me.BW19.Name = "BW19"
        Me.BW19.Size = New System.Drawing.Size(30, 20)
        Me.BW19.TabIndex = 110
        Me.BW19.Text = "W"
        Me.BW19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW19
        '
        Me.AW19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW19.Location = New System.Drawing.Point(5, 432)
        Me.AW19.MaxLength = 7
        Me.AW19.Name = "AW19"
        Me.AW19.Size = New System.Drawing.Size(30, 20)
        Me.AW19.TabIndex = 111
        Me.AW19.Text = "W"
        Me.AW19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW15
        '
        Me.EW15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW15.Location = New System.Drawing.Point(125, 340)
        Me.EW15.MaxLength = 7
        Me.EW15.Name = "EW15"
        Me.EW15.Size = New System.Drawing.Size(30, 20)
        Me.EW15.TabIndex = 107
        Me.EW15.Text = "W"
        Me.EW15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW15
        '
        Me.DW15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW15.Location = New System.Drawing.Point(95, 340)
        Me.DW15.MaxLength = 7
        Me.DW15.Name = "DW15"
        Me.DW15.Size = New System.Drawing.Size(30, 20)
        Me.DW15.TabIndex = 103
        Me.DW15.Text = "W"
        Me.DW15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW15
        '
        Me.CW15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW15.Location = New System.Drawing.Point(65, 340)
        Me.CW15.MaxLength = 7
        Me.CW15.Name = "CW15"
        Me.CW15.Size = New System.Drawing.Size(30, 20)
        Me.CW15.TabIndex = 106
        Me.CW15.Text = "W"
        Me.CW15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW15
        '
        Me.BW15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW15.Location = New System.Drawing.Point(35, 340)
        Me.BW15.MaxLength = 7
        Me.BW15.Name = "BW15"
        Me.BW15.Size = New System.Drawing.Size(30, 20)
        Me.BW15.TabIndex = 105
        Me.BW15.Text = "W"
        Me.BW15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW15
        '
        Me.AW15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW15.Location = New System.Drawing.Point(5, 340)
        Me.AW15.MaxLength = 7
        Me.AW15.Name = "AW15"
        Me.AW15.Size = New System.Drawing.Size(30, 20)
        Me.AW15.TabIndex = 104
        Me.AW15.Text = "W"
        Me.AW15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW7
        '
        Me.EW7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW7.Location = New System.Drawing.Point(125, 156)
        Me.EW7.MaxLength = 7
        Me.EW7.Name = "EW7"
        Me.EW7.Size = New System.Drawing.Size(30, 20)
        Me.EW7.TabIndex = 102
        Me.EW7.Text = "W"
        Me.EW7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW7
        '
        Me.DW7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW7.Location = New System.Drawing.Point(95, 156)
        Me.DW7.MaxLength = 7
        Me.DW7.Name = "DW7"
        Me.DW7.Size = New System.Drawing.Size(30, 20)
        Me.DW7.TabIndex = 101
        Me.DW7.Text = "W"
        Me.DW7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW7
        '
        Me.CW7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW7.Location = New System.Drawing.Point(65, 156)
        Me.CW7.MaxLength = 7
        Me.CW7.Name = "CW7"
        Me.CW7.Size = New System.Drawing.Size(30, 20)
        Me.CW7.TabIndex = 100
        Me.CW7.Text = "W"
        Me.CW7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW7
        '
        Me.BW7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW7.Location = New System.Drawing.Point(35, 156)
        Me.BW7.MaxLength = 7
        Me.BW7.Name = "BW7"
        Me.BW7.Size = New System.Drawing.Size(30, 20)
        Me.BW7.TabIndex = 99
        Me.BW7.Text = "W"
        Me.BW7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW7
        '
        Me.AW7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW7.Location = New System.Drawing.Point(5, 156)
        Me.AW7.MaxLength = 7
        Me.AW7.Name = "AW7"
        Me.AW7.Size = New System.Drawing.Size(30, 20)
        Me.AW7.TabIndex = 98
        Me.AW7.Text = "W"
        Me.AW7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW11
        '
        Me.EW11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW11.Location = New System.Drawing.Point(125, 248)
        Me.EW11.MaxLength = 7
        Me.EW11.Name = "EW11"
        Me.EW11.Size = New System.Drawing.Size(30, 20)
        Me.EW11.TabIndex = 96
        Me.EW11.Text = "W"
        Me.EW11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW11
        '
        Me.DW11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW11.Location = New System.Drawing.Point(95, 248)
        Me.DW11.MaxLength = 7
        Me.DW11.Name = "DW11"
        Me.DW11.Size = New System.Drawing.Size(30, 20)
        Me.DW11.TabIndex = 95
        Me.DW11.Text = "W"
        Me.DW11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW11
        '
        Me.CW11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW11.Location = New System.Drawing.Point(65, 248)
        Me.CW11.MaxLength = 7
        Me.CW11.Name = "CW11"
        Me.CW11.Size = New System.Drawing.Size(30, 20)
        Me.CW11.TabIndex = 97
        Me.CW11.Text = "W"
        Me.CW11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW11
        '
        Me.BW11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW11.Location = New System.Drawing.Point(35, 248)
        Me.BW11.MaxLength = 7
        Me.BW11.Name = "BW11"
        Me.BW11.Size = New System.Drawing.Size(30, 20)
        Me.BW11.TabIndex = 94
        Me.BW11.Text = "W"
        Me.BW11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW11
        '
        Me.AW11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW11.Location = New System.Drawing.Point(5, 248)
        Me.AW11.MaxLength = 7
        Me.AW11.Name = "AW11"
        Me.AW11.Size = New System.Drawing.Size(30, 20)
        Me.AW11.TabIndex = 93
        Me.AW11.Text = "W"
        Me.AW11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW3
        '
        Me.EW3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW3.Location = New System.Drawing.Point(125, 64)
        Me.EW3.MaxLength = 7
        Me.EW3.Name = "EW3"
        Me.EW3.Size = New System.Drawing.Size(30, 20)
        Me.EW3.TabIndex = 91
        Me.EW3.Text = "W"
        Me.EW3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW3
        '
        Me.DW3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW3.Location = New System.Drawing.Point(95, 64)
        Me.DW3.MaxLength = 7
        Me.DW3.Name = "DW3"
        Me.DW3.Size = New System.Drawing.Size(30, 20)
        Me.DW3.TabIndex = 92
        Me.DW3.Text = "W"
        Me.DW3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW3
        '
        Me.CW3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW3.Location = New System.Drawing.Point(65, 64)
        Me.CW3.MaxLength = 7
        Me.CW3.Name = "CW3"
        Me.CW3.Size = New System.Drawing.Size(30, 20)
        Me.CW3.TabIndex = 90
        Me.CW3.Text = "W"
        Me.CW3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW3
        '
        Me.BW3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW3.Location = New System.Drawing.Point(35, 64)
        Me.BW3.MaxLength = 7
        Me.BW3.Name = "BW3"
        Me.BW3.Size = New System.Drawing.Size(30, 20)
        Me.BW3.TabIndex = 89
        Me.BW3.Text = "W"
        Me.BW3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW3
        '
        Me.AW3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW3.Location = New System.Drawing.Point(5, 64)
        Me.AW3.MaxLength = 7
        Me.AW3.Name = "AW3"
        Me.AW3.Size = New System.Drawing.Size(30, 20)
        Me.AW3.TabIndex = 88
        Me.AW3.Text = "W"
        Me.AW3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW17
        '
        Me.EW17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW17.Location = New System.Drawing.Point(125, 386)
        Me.EW17.MaxLength = 7
        Me.EW17.Name = "EW17"
        Me.EW17.Size = New System.Drawing.Size(30, 20)
        Me.EW17.TabIndex = 83
        Me.EW17.Text = "W"
        Me.EW17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW17
        '
        Me.DW17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW17.Location = New System.Drawing.Point(95, 386)
        Me.DW17.MaxLength = 7
        Me.DW17.Name = "DW17"
        Me.DW17.Size = New System.Drawing.Size(30, 20)
        Me.DW17.TabIndex = 87
        Me.DW17.Text = "W"
        Me.DW17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW17
        '
        Me.CW17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW17.Location = New System.Drawing.Point(65, 386)
        Me.CW17.MaxLength = 7
        Me.CW17.Name = "CW17"
        Me.CW17.Size = New System.Drawing.Size(30, 20)
        Me.CW17.TabIndex = 86
        Me.CW17.Text = "W"
        Me.CW17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW17
        '
        Me.BW17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW17.Location = New System.Drawing.Point(35, 386)
        Me.BW17.MaxLength = 7
        Me.BW17.Name = "BW17"
        Me.BW17.Size = New System.Drawing.Size(30, 20)
        Me.BW17.TabIndex = 85
        Me.BW17.Text = "W"
        Me.BW17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW17
        '
        Me.AW17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW17.Location = New System.Drawing.Point(5, 386)
        Me.AW17.MaxLength = 7
        Me.AW17.Name = "AW17"
        Me.AW17.Size = New System.Drawing.Size(30, 20)
        Me.AW17.TabIndex = 84
        Me.AW17.Text = "W"
        Me.AW17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW13
        '
        Me.EW13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW13.Location = New System.Drawing.Point(125, 294)
        Me.EW13.MaxLength = 7
        Me.EW13.Name = "EW13"
        Me.EW13.Size = New System.Drawing.Size(30, 20)
        Me.EW13.TabIndex = 78
        Me.EW13.Text = "W"
        Me.EW13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW13
        '
        Me.DW13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW13.Location = New System.Drawing.Point(95, 294)
        Me.DW13.MaxLength = 7
        Me.DW13.Name = "DW13"
        Me.DW13.Size = New System.Drawing.Size(30, 20)
        Me.DW13.TabIndex = 79
        Me.DW13.Text = "W"
        Me.DW13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW13
        '
        Me.CW13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW13.Location = New System.Drawing.Point(65, 294)
        Me.CW13.MaxLength = 7
        Me.CW13.Name = "CW13"
        Me.CW13.Size = New System.Drawing.Size(30, 20)
        Me.CW13.TabIndex = 82
        Me.CW13.Text = "W"
        Me.CW13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW13
        '
        Me.BW13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW13.Location = New System.Drawing.Point(35, 294)
        Me.BW13.MaxLength = 7
        Me.BW13.Name = "BW13"
        Me.BW13.Size = New System.Drawing.Size(30, 20)
        Me.BW13.TabIndex = 80
        Me.BW13.Text = "W"
        Me.BW13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW13
        '
        Me.AW13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW13.Location = New System.Drawing.Point(5, 294)
        Me.AW13.MaxLength = 7
        Me.AW13.Name = "AW13"
        Me.AW13.Size = New System.Drawing.Size(30, 20)
        Me.AW13.TabIndex = 81
        Me.AW13.Text = "W"
        Me.AW13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW5
        '
        Me.EW5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW5.Location = New System.Drawing.Point(125, 110)
        Me.EW5.MaxLength = 7
        Me.EW5.Name = "EW5"
        Me.EW5.Size = New System.Drawing.Size(30, 20)
        Me.EW5.TabIndex = 73
        Me.EW5.Text = "W"
        Me.EW5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW5
        '
        Me.DW5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW5.Location = New System.Drawing.Point(95, 110)
        Me.DW5.MaxLength = 7
        Me.DW5.Name = "DW5"
        Me.DW5.Size = New System.Drawing.Size(30, 20)
        Me.DW5.TabIndex = 77
        Me.DW5.Text = "W"
        Me.DW5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW5
        '
        Me.CW5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW5.Location = New System.Drawing.Point(65, 110)
        Me.CW5.MaxLength = 7
        Me.CW5.Name = "CW5"
        Me.CW5.Size = New System.Drawing.Size(30, 20)
        Me.CW5.TabIndex = 76
        Me.CW5.Text = "W"
        Me.CW5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW5
        '
        Me.BW5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW5.Location = New System.Drawing.Point(35, 110)
        Me.BW5.MaxLength = 7
        Me.BW5.Name = "BW5"
        Me.BW5.Size = New System.Drawing.Size(30, 20)
        Me.BW5.TabIndex = 75
        Me.BW5.Text = "W"
        Me.BW5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW5
        '
        Me.AW5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW5.Location = New System.Drawing.Point(5, 110)
        Me.AW5.MaxLength = 7
        Me.AW5.Name = "AW5"
        Me.AW5.Size = New System.Drawing.Size(30, 20)
        Me.AW5.TabIndex = 74
        Me.AW5.Text = "W"
        Me.AW5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW9
        '
        Me.EW9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW9.Location = New System.Drawing.Point(125, 202)
        Me.EW9.MaxLength = 7
        Me.EW9.Name = "EW9"
        Me.EW9.Size = New System.Drawing.Size(30, 20)
        Me.EW9.TabIndex = 126
        Me.EW9.Text = "W"
        Me.EW9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW9
        '
        Me.DW9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW9.Location = New System.Drawing.Point(95, 202)
        Me.DW9.MaxLength = 7
        Me.DW9.Name = "DW9"
        Me.DW9.Size = New System.Drawing.Size(30, 20)
        Me.DW9.TabIndex = 123
        Me.DW9.Text = "W"
        Me.DW9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW9
        '
        Me.CW9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW9.Location = New System.Drawing.Point(65, 202)
        Me.CW9.MaxLength = 7
        Me.CW9.Name = "CW9"
        Me.CW9.Size = New System.Drawing.Size(30, 20)
        Me.CW9.TabIndex = 125
        Me.CW9.Text = "W"
        Me.CW9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW9
        '
        Me.BW9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW9.Location = New System.Drawing.Point(35, 202)
        Me.BW9.MaxLength = 7
        Me.BW9.Name = "BW9"
        Me.BW9.Size = New System.Drawing.Size(30, 20)
        Me.BW9.TabIndex = 127
        Me.BW9.Text = "W"
        Me.BW9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW9
        '
        Me.AW9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW9.Location = New System.Drawing.Point(5, 202)
        Me.AW9.MaxLength = 7
        Me.AW9.Name = "AW9"
        Me.AW9.Size = New System.Drawing.Size(30, 20)
        Me.AW9.TabIndex = 124
        Me.AW9.Text = "W"
        Me.AW9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'EW1
        '
        Me.EW1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EW1.Location = New System.Drawing.Point(125, 18)
        Me.EW1.MaxLength = 7
        Me.EW1.Name = "EW1"
        Me.EW1.Size = New System.Drawing.Size(30, 20)
        Me.EW1.TabIndex = 128
        Me.EW1.Text = "W"
        Me.EW1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'DW1
        '
        Me.DW1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.DW1.Location = New System.Drawing.Point(95, 18)
        Me.DW1.MaxLength = 7
        Me.DW1.Name = "DW1"
        Me.DW1.Size = New System.Drawing.Size(30, 20)
        Me.DW1.TabIndex = 129
        Me.DW1.Text = "W"
        Me.DW1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CW1
        '
        Me.CW1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CW1.Location = New System.Drawing.Point(65, 18)
        Me.CW1.MaxLength = 7
        Me.CW1.Name = "CW1"
        Me.CW1.Size = New System.Drawing.Size(30, 20)
        Me.CW1.TabIndex = 130
        Me.CW1.Text = "W"
        Me.CW1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'BW1
        '
        Me.BW1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BW1.Location = New System.Drawing.Point(35, 18)
        Me.BW1.MaxLength = 7
        Me.BW1.Name = "BW1"
        Me.BW1.Size = New System.Drawing.Size(30, 20)
        Me.BW1.TabIndex = 132
        Me.BW1.Text = "W"
        Me.BW1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'AW1
        '
        Me.AW1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.AW1.Location = New System.Drawing.Point(5, 18)
        Me.AW1.MaxLength = 7
        Me.AW1.Name = "AW1"
        Me.AW1.Size = New System.Drawing.Size(30, 20)
        Me.AW1.TabIndex = 131
        Me.AW1.Text = "W"
        Me.AW1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ComboBox1
        '
        Me.ComboBox1.Enabled = False
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(104, 36)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(99, 21)
        Me.ComboBox1.TabIndex = 70
        '
        'form_print_hasil_packing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1041, 642)
        Me.Controls.Add(Me.Label56)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Panel4)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_print_hasil_packing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ts_print As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txt_gulung As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents dtp_jatuh_tempo As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_gl_claim_jadi As System.Windows.Forms.TextBox
    Friend WithEvents txt_gl_claim_celup As System.Windows.Forms.TextBox
    Friend WithEvents txt_gl_grade_b As System.Windows.Forms.TextBox
    Friend WithEvents txt_gl_grade_a As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txt_claim_celup As System.Windows.Forms.TextBox
    Friend WithEvents txt_grade_a As System.Windows.Forms.TextBox
    Friend WithEvents txt_claim_jadi As System.Windows.Forms.TextBox
    Friend WithEvents txt_grade_b As System.Windows.Forms.TextBox
    Friend WithEvents txt_susut As System.Windows.Forms.TextBox
    Friend WithEvents txt_yards As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer As System.Windows.Forms.TextBox
    Friend WithEvents txt_baris_po As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_harga As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_po As System.Windows.Forms.TextBox
    Friend WithEvents txt_baris_sj As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_sj_packing As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan As System.Windows.Forms.RichTextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents S25 As System.Windows.Forms.TextBox
    Friend WithEvents S21 As System.Windows.Forms.TextBox
    Friend WithEvents S23 As System.Windows.Forms.TextBox
    Friend WithEvents S24 As System.Windows.Forms.TextBox
    Friend WithEvents S22 As System.Windows.Forms.TextBox
    Friend WithEvents S20 As System.Windows.Forms.TextBox
    Friend WithEvents S16 As System.Windows.Forms.TextBox
    Friend WithEvents S8 As System.Windows.Forms.TextBox
    Friend WithEvents S12 As System.Windows.Forms.TextBox
    Friend WithEvents S4 As System.Windows.Forms.TextBox
    Friend WithEvents S18 As System.Windows.Forms.TextBox
    Friend WithEvents S14 As System.Windows.Forms.TextBox
    Friend WithEvents S6 As System.Windows.Forms.TextBox
    Friend WithEvents S10 As System.Windows.Forms.TextBox
    Friend WithEvents S2 As System.Windows.Forms.TextBox
    Friend WithEvents S19 As System.Windows.Forms.TextBox
    Friend WithEvents S15 As System.Windows.Forms.TextBox
    Friend WithEvents S7 As System.Windows.Forms.TextBox
    Friend WithEvents S11 As System.Windows.Forms.TextBox
    Friend WithEvents S3 As System.Windows.Forms.TextBox
    Friend WithEvents S17 As System.Windows.Forms.TextBox
    Friend WithEvents S13 As System.Windows.Forms.TextBox
    Friend WithEvents S5 As System.Windows.Forms.TextBox
    Friend WithEvents S9 As System.Windows.Forms.TextBox
    Friend WithEvents S1 As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents E25 As System.Windows.Forms.TextBox
    Friend WithEvents D25 As System.Windows.Forms.TextBox
    Friend WithEvents C25 As System.Windows.Forms.TextBox
    Friend WithEvents B25 As System.Windows.Forms.TextBox
    Friend WithEvents A25 As System.Windows.Forms.TextBox
    Friend WithEvents K25 As System.Windows.Forms.TextBox
    Friend WithEvents Y25 As System.Windows.Forms.TextBox
    Friend WithEvents M25 As System.Windows.Forms.TextBox
    Friend WithEvents E21 As System.Windows.Forms.TextBox
    Friend WithEvents D21 As System.Windows.Forms.TextBox
    Friend WithEvents C21 As System.Windows.Forms.TextBox
    Friend WithEvents B21 As System.Windows.Forms.TextBox
    Friend WithEvents A21 As System.Windows.Forms.TextBox
    Friend WithEvents K21 As System.Windows.Forms.TextBox
    Friend WithEvents Y21 As System.Windows.Forms.TextBox
    Friend WithEvents M21 As System.Windows.Forms.TextBox
    Friend WithEvents E23 As System.Windows.Forms.TextBox
    Friend WithEvents D23 As System.Windows.Forms.TextBox
    Friend WithEvents C23 As System.Windows.Forms.TextBox
    Friend WithEvents B23 As System.Windows.Forms.TextBox
    Friend WithEvents A23 As System.Windows.Forms.TextBox
    Friend WithEvents K23 As System.Windows.Forms.TextBox
    Friend WithEvents Y23 As System.Windows.Forms.TextBox
    Friend WithEvents M23 As System.Windows.Forms.TextBox
    Friend WithEvents E24 As System.Windows.Forms.TextBox
    Friend WithEvents D24 As System.Windows.Forms.TextBox
    Friend WithEvents C24 As System.Windows.Forms.TextBox
    Friend WithEvents B24 As System.Windows.Forms.TextBox
    Friend WithEvents A24 As System.Windows.Forms.TextBox
    Friend WithEvents K24 As System.Windows.Forms.TextBox
    Friend WithEvents Y24 As System.Windows.Forms.TextBox
    Friend WithEvents M24 As System.Windows.Forms.TextBox
    Friend WithEvents E22 As System.Windows.Forms.TextBox
    Friend WithEvents D22 As System.Windows.Forms.TextBox
    Friend WithEvents C22 As System.Windows.Forms.TextBox
    Friend WithEvents B22 As System.Windows.Forms.TextBox
    Friend WithEvents A22 As System.Windows.Forms.TextBox
    Friend WithEvents K22 As System.Windows.Forms.TextBox
    Friend WithEvents Y22 As System.Windows.Forms.TextBox
    Friend WithEvents M22 As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents E20 As System.Windows.Forms.TextBox
    Friend WithEvents D20 As System.Windows.Forms.TextBox
    Friend WithEvents C20 As System.Windows.Forms.TextBox
    Friend WithEvents B20 As System.Windows.Forms.TextBox
    Friend WithEvents A20 As System.Windows.Forms.TextBox
    Friend WithEvents K20 As System.Windows.Forms.TextBox
    Friend WithEvents Y20 As System.Windows.Forms.TextBox
    Friend WithEvents M20 As System.Windows.Forms.TextBox
    Friend WithEvents E16 As System.Windows.Forms.TextBox
    Friend WithEvents D16 As System.Windows.Forms.TextBox
    Friend WithEvents C16 As System.Windows.Forms.TextBox
    Friend WithEvents B16 As System.Windows.Forms.TextBox
    Friend WithEvents A16 As System.Windows.Forms.TextBox
    Friend WithEvents K16 As System.Windows.Forms.TextBox
    Friend WithEvents Y16 As System.Windows.Forms.TextBox
    Friend WithEvents M16 As System.Windows.Forms.TextBox
    Friend WithEvents E8 As System.Windows.Forms.TextBox
    Friend WithEvents D8 As System.Windows.Forms.TextBox
    Friend WithEvents C8 As System.Windows.Forms.TextBox
    Friend WithEvents B8 As System.Windows.Forms.TextBox
    Friend WithEvents A8 As System.Windows.Forms.TextBox
    Friend WithEvents K8 As System.Windows.Forms.TextBox
    Friend WithEvents Y8 As System.Windows.Forms.TextBox
    Friend WithEvents M8 As System.Windows.Forms.TextBox
    Friend WithEvents E12 As System.Windows.Forms.TextBox
    Friend WithEvents D12 As System.Windows.Forms.TextBox
    Friend WithEvents C12 As System.Windows.Forms.TextBox
    Friend WithEvents B12 As System.Windows.Forms.TextBox
    Friend WithEvents A12 As System.Windows.Forms.TextBox
    Friend WithEvents K12 As System.Windows.Forms.TextBox
    Friend WithEvents Y12 As System.Windows.Forms.TextBox
    Friend WithEvents M12 As System.Windows.Forms.TextBox
    Friend WithEvents E4 As System.Windows.Forms.TextBox
    Friend WithEvents D4 As System.Windows.Forms.TextBox
    Friend WithEvents C4 As System.Windows.Forms.TextBox
    Friend WithEvents B4 As System.Windows.Forms.TextBox
    Friend WithEvents A4 As System.Windows.Forms.TextBox
    Friend WithEvents K4 As System.Windows.Forms.TextBox
    Friend WithEvents Y4 As System.Windows.Forms.TextBox
    Friend WithEvents M4 As System.Windows.Forms.TextBox
    Friend WithEvents E18 As System.Windows.Forms.TextBox
    Friend WithEvents D18 As System.Windows.Forms.TextBox
    Friend WithEvents C18 As System.Windows.Forms.TextBox
    Friend WithEvents B18 As System.Windows.Forms.TextBox
    Friend WithEvents A18 As System.Windows.Forms.TextBox
    Friend WithEvents K18 As System.Windows.Forms.TextBox
    Friend WithEvents Y18 As System.Windows.Forms.TextBox
    Friend WithEvents M18 As System.Windows.Forms.TextBox
    Friend WithEvents E14 As System.Windows.Forms.TextBox
    Friend WithEvents D14 As System.Windows.Forms.TextBox
    Friend WithEvents C14 As System.Windows.Forms.TextBox
    Friend WithEvents B14 As System.Windows.Forms.TextBox
    Friend WithEvents A14 As System.Windows.Forms.TextBox
    Friend WithEvents K14 As System.Windows.Forms.TextBox
    Friend WithEvents Y14 As System.Windows.Forms.TextBox
    Friend WithEvents M14 As System.Windows.Forms.TextBox
    Friend WithEvents E6 As System.Windows.Forms.TextBox
    Friend WithEvents D6 As System.Windows.Forms.TextBox
    Friend WithEvents C6 As System.Windows.Forms.TextBox
    Friend WithEvents B6 As System.Windows.Forms.TextBox
    Friend WithEvents A6 As System.Windows.Forms.TextBox
    Friend WithEvents K6 As System.Windows.Forms.TextBox
    Friend WithEvents Y6 As System.Windows.Forms.TextBox
    Friend WithEvents M6 As System.Windows.Forms.TextBox
    Friend WithEvents E10 As System.Windows.Forms.TextBox
    Friend WithEvents D10 As System.Windows.Forms.TextBox
    Friend WithEvents C10 As System.Windows.Forms.TextBox
    Friend WithEvents B10 As System.Windows.Forms.TextBox
    Friend WithEvents A10 As System.Windows.Forms.TextBox
    Friend WithEvents K10 As System.Windows.Forms.TextBox
    Friend WithEvents Y10 As System.Windows.Forms.TextBox
    Friend WithEvents M10 As System.Windows.Forms.TextBox
    Friend WithEvents E2 As System.Windows.Forms.TextBox
    Friend WithEvents D2 As System.Windows.Forms.TextBox
    Friend WithEvents C2 As System.Windows.Forms.TextBox
    Friend WithEvents B2 As System.Windows.Forms.TextBox
    Friend WithEvents A2 As System.Windows.Forms.TextBox
    Friend WithEvents K2 As System.Windows.Forms.TextBox
    Friend WithEvents Y2 As System.Windows.Forms.TextBox
    Friend WithEvents M2 As System.Windows.Forms.TextBox
    Friend WithEvents E19 As System.Windows.Forms.TextBox
    Friend WithEvents D19 As System.Windows.Forms.TextBox
    Friend WithEvents C19 As System.Windows.Forms.TextBox
    Friend WithEvents B19 As System.Windows.Forms.TextBox
    Friend WithEvents A19 As System.Windows.Forms.TextBox
    Friend WithEvents K19 As System.Windows.Forms.TextBox
    Friend WithEvents Y19 As System.Windows.Forms.TextBox
    Friend WithEvents M19 As System.Windows.Forms.TextBox
    Friend WithEvents E15 As System.Windows.Forms.TextBox
    Friend WithEvents D15 As System.Windows.Forms.TextBox
    Friend WithEvents C15 As System.Windows.Forms.TextBox
    Friend WithEvents B15 As System.Windows.Forms.TextBox
    Friend WithEvents A15 As System.Windows.Forms.TextBox
    Friend WithEvents K15 As System.Windows.Forms.TextBox
    Friend WithEvents Y15 As System.Windows.Forms.TextBox
    Friend WithEvents M15 As System.Windows.Forms.TextBox
    Friend WithEvents E7 As System.Windows.Forms.TextBox
    Friend WithEvents D7 As System.Windows.Forms.TextBox
    Friend WithEvents C7 As System.Windows.Forms.TextBox
    Friend WithEvents B7 As System.Windows.Forms.TextBox
    Friend WithEvents A7 As System.Windows.Forms.TextBox
    Friend WithEvents K7 As System.Windows.Forms.TextBox
    Friend WithEvents Y7 As System.Windows.Forms.TextBox
    Friend WithEvents M7 As System.Windows.Forms.TextBox
    Friend WithEvents E11 As System.Windows.Forms.TextBox
    Friend WithEvents D11 As System.Windows.Forms.TextBox
    Friend WithEvents C11 As System.Windows.Forms.TextBox
    Friend WithEvents B11 As System.Windows.Forms.TextBox
    Friend WithEvents A11 As System.Windows.Forms.TextBox
    Friend WithEvents K11 As System.Windows.Forms.TextBox
    Friend WithEvents Y11 As System.Windows.Forms.TextBox
    Friend WithEvents M11 As System.Windows.Forms.TextBox
    Friend WithEvents E3 As System.Windows.Forms.TextBox
    Friend WithEvents D3 As System.Windows.Forms.TextBox
    Friend WithEvents C3 As System.Windows.Forms.TextBox
    Friend WithEvents B3 As System.Windows.Forms.TextBox
    Friend WithEvents A3 As System.Windows.Forms.TextBox
    Friend WithEvents K3 As System.Windows.Forms.TextBox
    Friend WithEvents Y3 As System.Windows.Forms.TextBox
    Friend WithEvents M3 As System.Windows.Forms.TextBox
    Friend WithEvents E17 As System.Windows.Forms.TextBox
    Friend WithEvents D17 As System.Windows.Forms.TextBox
    Friend WithEvents C17 As System.Windows.Forms.TextBox
    Friend WithEvents B17 As System.Windows.Forms.TextBox
    Friend WithEvents A17 As System.Windows.Forms.TextBox
    Friend WithEvents K17 As System.Windows.Forms.TextBox
    Friend WithEvents Y17 As System.Windows.Forms.TextBox
    Friend WithEvents M17 As System.Windows.Forms.TextBox
    Friend WithEvents E13 As System.Windows.Forms.TextBox
    Friend WithEvents D13 As System.Windows.Forms.TextBox
    Friend WithEvents C13 As System.Windows.Forms.TextBox
    Friend WithEvents B13 As System.Windows.Forms.TextBox
    Friend WithEvents A13 As System.Windows.Forms.TextBox
    Friend WithEvents K13 As System.Windows.Forms.TextBox
    Friend WithEvents Y13 As System.Windows.Forms.TextBox
    Friend WithEvents M13 As System.Windows.Forms.TextBox
    Friend WithEvents E5 As System.Windows.Forms.TextBox
    Friend WithEvents D5 As System.Windows.Forms.TextBox
    Friend WithEvents C5 As System.Windows.Forms.TextBox
    Friend WithEvents B5 As System.Windows.Forms.TextBox
    Friend WithEvents A5 As System.Windows.Forms.TextBox
    Friend WithEvents K5 As System.Windows.Forms.TextBox
    Friend WithEvents Y5 As System.Windows.Forms.TextBox
    Friend WithEvents M5 As System.Windows.Forms.TextBox
    Friend WithEvents E9 As System.Windows.Forms.TextBox
    Friend WithEvents D9 As System.Windows.Forms.TextBox
    Friend WithEvents C9 As System.Windows.Forms.TextBox
    Friend WithEvents B9 As System.Windows.Forms.TextBox
    Friend WithEvents A9 As System.Windows.Forms.TextBox
    Friend WithEvents K9 As System.Windows.Forms.TextBox
    Friend WithEvents Y9 As System.Windows.Forms.TextBox
    Friend WithEvents M9 As System.Windows.Forms.TextBox
    Friend WithEvents E1 As System.Windows.Forms.TextBox
    Friend WithEvents D1 As System.Windows.Forms.TextBox
    Friend WithEvents C1 As System.Windows.Forms.TextBox
    Friend WithEvents B1 As System.Windows.Forms.TextBox
    Friend WithEvents A1 As System.Windows.Forms.TextBox
    Friend WithEvents K1 As System.Windows.Forms.TextBox
    Friend WithEvents Y1 As System.Windows.Forms.TextBox
    Friend WithEvents M1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents EW25 As System.Windows.Forms.TextBox
    Friend WithEvents DW25 As System.Windows.Forms.TextBox
    Friend WithEvents CW25 As System.Windows.Forms.TextBox
    Friend WithEvents BW25 As System.Windows.Forms.TextBox
    Friend WithEvents AW25 As System.Windows.Forms.TextBox
    Friend WithEvents EW21 As System.Windows.Forms.TextBox
    Friend WithEvents DW21 As System.Windows.Forms.TextBox
    Friend WithEvents CW21 As System.Windows.Forms.TextBox
    Friend WithEvents BW21 As System.Windows.Forms.TextBox
    Friend WithEvents AW21 As System.Windows.Forms.TextBox
    Friend WithEvents EW23 As System.Windows.Forms.TextBox
    Friend WithEvents DW23 As System.Windows.Forms.TextBox
    Friend WithEvents CW23 As System.Windows.Forms.TextBox
    Friend WithEvents BW23 As System.Windows.Forms.TextBox
    Friend WithEvents AW23 As System.Windows.Forms.TextBox
    Friend WithEvents EW24 As System.Windows.Forms.TextBox
    Friend WithEvents DW24 As System.Windows.Forms.TextBox
    Friend WithEvents CW24 As System.Windows.Forms.TextBox
    Friend WithEvents BW24 As System.Windows.Forms.TextBox
    Friend WithEvents AW24 As System.Windows.Forms.TextBox
    Friend WithEvents EW22 As System.Windows.Forms.TextBox
    Friend WithEvents DW22 As System.Windows.Forms.TextBox
    Friend WithEvents CW22 As System.Windows.Forms.TextBox
    Friend WithEvents BW22 As System.Windows.Forms.TextBox
    Friend WithEvents AW22 As System.Windows.Forms.TextBox
    Friend WithEvents EW20 As System.Windows.Forms.TextBox
    Friend WithEvents DW20 As System.Windows.Forms.TextBox
    Friend WithEvents CW20 As System.Windows.Forms.TextBox
    Friend WithEvents BW20 As System.Windows.Forms.TextBox
    Friend WithEvents AW20 As System.Windows.Forms.TextBox
    Friend WithEvents EW16 As System.Windows.Forms.TextBox
    Friend WithEvents DW16 As System.Windows.Forms.TextBox
    Friend WithEvents CW16 As System.Windows.Forms.TextBox
    Friend WithEvents BW16 As System.Windows.Forms.TextBox
    Friend WithEvents AW16 As System.Windows.Forms.TextBox
    Friend WithEvents EW8 As System.Windows.Forms.TextBox
    Friend WithEvents DW8 As System.Windows.Forms.TextBox
    Friend WithEvents CW8 As System.Windows.Forms.TextBox
    Friend WithEvents BW8 As System.Windows.Forms.TextBox
    Friend WithEvents AW8 As System.Windows.Forms.TextBox
    Friend WithEvents EW12 As System.Windows.Forms.TextBox
    Friend WithEvents DW12 As System.Windows.Forms.TextBox
    Friend WithEvents CW12 As System.Windows.Forms.TextBox
    Friend WithEvents BW12 As System.Windows.Forms.TextBox
    Friend WithEvents AW12 As System.Windows.Forms.TextBox
    Friend WithEvents EW4 As System.Windows.Forms.TextBox
    Friend WithEvents DW4 As System.Windows.Forms.TextBox
    Friend WithEvents CW4 As System.Windows.Forms.TextBox
    Friend WithEvents BW4 As System.Windows.Forms.TextBox
    Friend WithEvents AW4 As System.Windows.Forms.TextBox
    Friend WithEvents EW18 As System.Windows.Forms.TextBox
    Friend WithEvents DW18 As System.Windows.Forms.TextBox
    Friend WithEvents CW18 As System.Windows.Forms.TextBox
    Friend WithEvents BW18 As System.Windows.Forms.TextBox
    Friend WithEvents AW18 As System.Windows.Forms.TextBox
    Friend WithEvents EW14 As System.Windows.Forms.TextBox
    Friend WithEvents DW14 As System.Windows.Forms.TextBox
    Friend WithEvents CW14 As System.Windows.Forms.TextBox
    Friend WithEvents BW14 As System.Windows.Forms.TextBox
    Friend WithEvents AW14 As System.Windows.Forms.TextBox
    Friend WithEvents EW6 As System.Windows.Forms.TextBox
    Friend WithEvents DW6 As System.Windows.Forms.TextBox
    Friend WithEvents CW6 As System.Windows.Forms.TextBox
    Friend WithEvents BW6 As System.Windows.Forms.TextBox
    Friend WithEvents AW6 As System.Windows.Forms.TextBox
    Friend WithEvents EW10 As System.Windows.Forms.TextBox
    Friend WithEvents DW10 As System.Windows.Forms.TextBox
    Friend WithEvents CW10 As System.Windows.Forms.TextBox
    Friend WithEvents BW10 As System.Windows.Forms.TextBox
    Friend WithEvents AW10 As System.Windows.Forms.TextBox
    Friend WithEvents EW2 As System.Windows.Forms.TextBox
    Friend WithEvents DW2 As System.Windows.Forms.TextBox
    Friend WithEvents CW2 As System.Windows.Forms.TextBox
    Friend WithEvents BW2 As System.Windows.Forms.TextBox
    Friend WithEvents AW2 As System.Windows.Forms.TextBox
    Friend WithEvents EW19 As System.Windows.Forms.TextBox
    Friend WithEvents DW19 As System.Windows.Forms.TextBox
    Friend WithEvents CW19 As System.Windows.Forms.TextBox
    Friend WithEvents BW19 As System.Windows.Forms.TextBox
    Friend WithEvents AW19 As System.Windows.Forms.TextBox
    Friend WithEvents EW15 As System.Windows.Forms.TextBox
    Friend WithEvents DW15 As System.Windows.Forms.TextBox
    Friend WithEvents CW15 As System.Windows.Forms.TextBox
    Friend WithEvents BW15 As System.Windows.Forms.TextBox
    Friend WithEvents AW15 As System.Windows.Forms.TextBox
    Friend WithEvents EW7 As System.Windows.Forms.TextBox
    Friend WithEvents DW7 As System.Windows.Forms.TextBox
    Friend WithEvents CW7 As System.Windows.Forms.TextBox
    Friend WithEvents BW7 As System.Windows.Forms.TextBox
    Friend WithEvents AW7 As System.Windows.Forms.TextBox
    Friend WithEvents EW11 As System.Windows.Forms.TextBox
    Friend WithEvents DW11 As System.Windows.Forms.TextBox
    Friend WithEvents CW11 As System.Windows.Forms.TextBox
    Friend WithEvents BW11 As System.Windows.Forms.TextBox
    Friend WithEvents AW11 As System.Windows.Forms.TextBox
    Friend WithEvents EW3 As System.Windows.Forms.TextBox
    Friend WithEvents DW3 As System.Windows.Forms.TextBox
    Friend WithEvents CW3 As System.Windows.Forms.TextBox
    Friend WithEvents BW3 As System.Windows.Forms.TextBox
    Friend WithEvents AW3 As System.Windows.Forms.TextBox
    Friend WithEvents EW17 As System.Windows.Forms.TextBox
    Friend WithEvents DW17 As System.Windows.Forms.TextBox
    Friend WithEvents CW17 As System.Windows.Forms.TextBox
    Friend WithEvents BW17 As System.Windows.Forms.TextBox
    Friend WithEvents AW17 As System.Windows.Forms.TextBox
    Friend WithEvents EW13 As System.Windows.Forms.TextBox
    Friend WithEvents DW13 As System.Windows.Forms.TextBox
    Friend WithEvents CW13 As System.Windows.Forms.TextBox
    Friend WithEvents BW13 As System.Windows.Forms.TextBox
    Friend WithEvents AW13 As System.Windows.Forms.TextBox
    Friend WithEvents EW5 As System.Windows.Forms.TextBox
    Friend WithEvents DW5 As System.Windows.Forms.TextBox
    Friend WithEvents CW5 As System.Windows.Forms.TextBox
    Friend WithEvents BW5 As System.Windows.Forms.TextBox
    Friend WithEvents AW5 As System.Windows.Forms.TextBox
    Friend WithEvents EW9 As System.Windows.Forms.TextBox
    Friend WithEvents DW9 As System.Windows.Forms.TextBox
    Friend WithEvents CW9 As System.Windows.Forms.TextBox
    Friend WithEvents BW9 As System.Windows.Forms.TextBox
    Friend WithEvents AW9 As System.Windows.Forms.TextBox
    Friend WithEvents EW1 As System.Windows.Forms.TextBox
    Friend WithEvents DW1 As System.Windows.Forms.TextBox
    Friend WithEvents CW1 As System.Windows.Forms.TextBox
    Friend WithEvents BW1 As System.Windows.Forms.TextBox
    Friend WithEvents AW1 As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
End Class
