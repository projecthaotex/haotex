﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_report_tracking_id_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource10 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource11 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource12 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource13 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource14 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource15 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource16 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource17 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource18 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.tbheaderfooterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ds_tracking_id = New HAOTEX.ds_tracking_id()
        Me.tbkontrakBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tbpembelianBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tbpenjualanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tbpoprosesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tbsjprosesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tbpopackingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tbsjpackingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tbsjpenjualanBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.tbheaderfooterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ds_tracking_id, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbkontrakBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpembelianBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpenjualanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpoprosesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbsjprosesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbpopackingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbsjpackingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbsjpenjualanBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource10.Name = "tbheaderfooter"
        ReportDataSource10.Value = Me.tbheaderfooterBindingSource
        ReportDataSource11.Name = "tbkontrak"
        ReportDataSource11.Value = Me.tbkontrakBindingSource
        ReportDataSource12.Name = "tbpembelian"
        ReportDataSource12.Value = Me.tbpembelianBindingSource
        ReportDataSource13.Name = "tbpenjualan"
        ReportDataSource13.Value = Me.tbpenjualanBindingSource
        ReportDataSource14.Name = "tbpoproses"
        ReportDataSource14.Value = Me.tbpoprosesBindingSource
        ReportDataSource15.Name = "tbsjproses"
        ReportDataSource15.Value = Me.tbsjprosesBindingSource
        ReportDataSource16.Name = "tbpopacking"
        ReportDataSource16.Value = Me.tbpopackingBindingSource
        ReportDataSource17.Name = "tbsjpacking"
        ReportDataSource17.Value = Me.tbsjpackingBindingSource
        ReportDataSource18.Name = "tbsjpenjualan"
        ReportDataSource18.Value = Me.tbsjpenjualanBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource10)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource11)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource12)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource13)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource14)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource15)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource16)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource17)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource18)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "HAOTEX.report_tracking_id_grey.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(284, 262)
        Me.ReportViewer1.TabIndex = 0
        '
        'tbheaderfooterBindingSource
        '
        Me.tbheaderfooterBindingSource.DataMember = "tbheaderfooter"
        Me.tbheaderfooterBindingSource.DataSource = Me.ds_tracking_id
        '
        'ds_tracking_id
        '
        Me.ds_tracking_id.DataSetName = "ds_tracking_id"
        Me.ds_tracking_id.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'tbkontrakBindingSource
        '
        Me.tbkontrakBindingSource.DataMember = "tbkontrak"
        Me.tbkontrakBindingSource.DataSource = Me.ds_tracking_id
        '
        'tbpembelianBindingSource
        '
        Me.tbpembelianBindingSource.DataMember = "tbpembelian"
        Me.tbpembelianBindingSource.DataSource = Me.ds_tracking_id
        '
        'tbpenjualanBindingSource
        '
        Me.tbpenjualanBindingSource.DataMember = "tbpenjualan"
        Me.tbpenjualanBindingSource.DataSource = Me.ds_tracking_id
        '
        'tbpoprosesBindingSource
        '
        Me.tbpoprosesBindingSource.DataMember = "tbpoproses"
        Me.tbpoprosesBindingSource.DataSource = Me.ds_tracking_id
        '
        'tbsjprosesBindingSource
        '
        Me.tbsjprosesBindingSource.DataMember = "tbsjproses"
        Me.tbsjprosesBindingSource.DataSource = Me.ds_tracking_id
        '
        'tbpopackingBindingSource
        '
        Me.tbpopackingBindingSource.DataMember = "tbpopacking"
        Me.tbpopackingBindingSource.DataSource = Me.ds_tracking_id
        '
        'tbsjpackingBindingSource
        '
        Me.tbsjpackingBindingSource.DataMember = "tbsjpacking"
        Me.tbsjpackingBindingSource.DataSource = Me.ds_tracking_id
        '
        'tbsjpenjualanBindingSource
        '
        Me.tbsjpenjualanBindingSource.DataMember = "tbsjpenjualan"
        Me.tbsjpenjualanBindingSource.DataSource = Me.ds_tracking_id
        '
        'form_report_tracking_id_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "form_report_tracking_id_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "REPORT TRACKING ID GREY"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.tbheaderfooterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ds_tracking_id, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbkontrakBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpembelianBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpenjualanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpoprosesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbsjprosesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbpopackingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbsjpackingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbsjpenjualanBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents tbheaderfooterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ds_tracking_id As HAOTEX.ds_tracking_id
    Friend WithEvents tbkontrakBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbpembelianBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbpenjualanBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbpoprosesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbsjprosesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbpopackingBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbsjpackingBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents tbsjpenjualanBindingSource As System.Windows.Forms.BindingSource
End Class
