﻿Imports MySql.Data.MySqlClient

Public Class form_input_nama_warna

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Me.Close()
    End Sub
    Private Sub simpan_tbwarnaresep()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbwarnaresep (Jenis_Kain,Warna,Resep,Tambah1,Tambah2,Tambah3) VALUES (@1,@2,@3,@4,@5,@6)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", (txt_jenis_kain.Text))
                    .Parameters.AddWithValue("@2", (txt_nama_warna.Text))
                    .Parameters.AddWithValue("@3", (txt_nama_resep.Text))
                    .Parameters.AddWithValue("@4", (""))
                    .Parameters.AddWithValue("@5", (""))
                    .Parameters.AddWithValue("@6", ("0"))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbwarnaresep()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbwarnaresep SET Jenis_Kain=@0,Warna=@1,Resep=@2,Tambah1=@3,Tambah2=@4,Tambah3=@5 WHERE Jenis_Kain = '" & form_input_warna_resep.txt_cari_jenis_kain.Text & "' AND Warna='" & form_input_warna_resep.txt_cari_warna.Text & "' AND Resep='" & form_input_warna_resep.txt_cari_resep.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (txt_jenis_kain.Text))
                    .Parameters.AddWithValue("@1", (txt_nama_warna.Text))
                    .Parameters.AddWithValue("@2", (txt_nama_resep.Text))
                    .Parameters.AddWithValue("@3", (""))
                    .Parameters.AddWithValue("@4", (""))
                    .Parameters.AddWithValue("@5", (""))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub btn_simpan_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_baru.Click
        Try
            If Label1.Text = "Input Warna dan Resep Baru" Then
                If txt_jenis_kain.Text = "" Then
                    MsgBox("JENIS KAIN Belum Diinput")
                    txt_nama_warna.Focus()
                ElseIf txt_nama_warna.Text = "" Then
                    MsgBox("Nama WARNA Belum Diinput")
                    txt_nama_warna.Focus()
                ElseIf txt_nama_resep.Text = "" Then
                    MsgBox("Nama RESEP Belum Diinput")
                    txt_nama_resep.Focus()
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx = "SELECT Jenis_Kain,Warna,Resep from tbwarnaresep WHERE Jenis_Kain = '" & txt_jenis_kain.Text & "' AND Warna ='" & txt_nama_warna.Text & "' AND Resep ='" & txt_nama_resep.Text & "'"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using drx As MySqlDataReader = cmdx.ExecuteReader
                                drx.Read()
                                If drx.HasRows Then
                                    MsgBox("WARNA dan RESEP Sudah Ada")
                                    txt_nama_warna.Focus()
                                Else
                                    Call simpan_tbwarnaresep()
                                    txt_jenis_kain.Text = ""
                                    txt_nama_warna.Text = ""
                                    txt_nama_resep.Text = ""
                                    txt_nama_warna.Focus()
                                    form_input_warna_resep.ts_perbarui.PerformClick()
                                    MsgBox("WARNA dan RESEP Baru Berhasil Disimpan")
                                End If
                            End Using
                        End Using
                    End Using
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_simpan_tutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_tutup.Click
        Try
            If Label1.Text = "Input Warna dan Resep Baru" Then
                If txt_nama_warna.Text = "" Then
                    MsgBox("Nama WARNA Belum Diisi")
                    txt_nama_warna.Focus()
                ElseIf txt_nama_resep.Text = "" Then
                    MsgBox("Nama RESEP Belum Diisi")
                    txt_nama_resep.Focus()
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx = "SELECT Jenis_Kain,Warna,Resep from tbwarnaresep WHERE Jenis_Kain = '" & txt_jenis_kain.Text & "' AND Warna ='" & txt_nama_warna.Text & "' AND Resep ='" & txt_nama_resep.Text & "'"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using drx As MySqlDataReader = cmdx.ExecuteReader
                                drx.Read()
                                If drx.HasRows Then
                                    MsgBox("WARNA dan RESEP Sudah Ada")
                                    txt_nama_warna.Focus()
                                Else
                                    Call simpan_tbwarnaresep()
                                    txt_jenis_kain.Text = ""
                                    txt_nama_warna.Text = ""
                                    txt_nama_resep.Text = ""
                                    txt_nama_warna.Focus()
                                    form_input_warna_resep.ts_perbarui.PerformClick()
                                    MsgBox("WARNA dan RESEP Baru Berhasil Disimpan")
                                    Me.Close()
                                End If
                            End Using
                        End Using
                    End Using
                End If
            ElseIf Label1.Text = "Ubah Warna dan Resep" Then
                If txt_nama_warna.Text = form_input_warna_resep.txt_cari_warna.Text And txt_nama_resep.Text = form_input_warna_resep.txt_cari_resep.Text _
                    And txt_jenis_kain.Text = form_input_warna_resep.txt_cari_jenis_kain.Text Then
                    MsgBox("WARNA atau RESEP belum diubah")
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx = "SELECT Jenis_Kain,Warna,Resep from tbwarnaresep WHERE Jenis_Kain = '" & txt_jenis_kain.Text & "' AND Warna ='" & txt_nama_warna.Text & "' AND Resep ='" & txt_nama_resep.Text & "'"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using drx As MySqlDataReader = cmdx.ExecuteReader
                                drx.Read()
                                If drx.HasRows Then
                                    MsgBox("WARNA atau RESEP sudah terdapat di Database")
                                    txt_nama_warna.Focus()
                                Else
                                    Call update_tbwarnaresep()
                                    txt_jenis_kain.Text = ""
                                    txt_nama_warna.Text = ""
                                    txt_nama_resep.Text = ""
                                    txt_nama_warna.Focus()
                                    form_input_warna_resep.ts_perbarui.PerformClick()
                                    MsgBox("WARNA dan RESEP berhasil Diubah")
                                    Me.Close()
                                End If
                            End Using
                        End Using
                    End Using
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_jenis_kain.GotFocus
        form_input_jenis_kain.MdiParent = form_menu_utama
        form_input_jenis_kain.Show()
        form_input_jenis_kain.TxtForm.Text = "form_input_nama_warna"
        form_input_jenis_kain.Focus()
    End Sub
End Class