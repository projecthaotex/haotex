﻿Imports MySql.Data.MySqlClient

Public Class form_input_supplier

    Private Sub form_input_supplier_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If TxtForm.Text = "form_input_pembelian_grey" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_1" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_2" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_3" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_4" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_5" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_6" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_7" Or _
          TxtForm.Text = "form_input_pembelian_grey_baru_8" Then
            form_input_pembelian_grey_baru.Focus()
            form_input_pembelian_grey_baru.Label1.Focus()
        ElseIf TxtForm.Text = "form_input_kontrak_grey" Then
            form_input_kontrak_grey.Focus()
            form_input_kontrak_grey.Label1.Focus()
        End If
    End Sub
    Private Sub form_input_supplier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
        TxtCari.Text = ""
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Nama_Supplier FROM tbsupplier ORDER BY Nama_Supplier"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbsupplier")
                            Dgv1.DataSource = dsx.Tables("tbsupplier")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_nama_supplier()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Nama_Supplier FROM tbsupplier WHERE Nama_Supplier like '%" & TxtCari.Text & "%' ORDER BY Nama_Supplier"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbsupplier")
                            Dgv1.DataSource = dsx.Tables("tbsupplier")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub TxtCari_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCari.TextChanged
        Call cari_nama_supplier()
    End Sub
    Private Sub Dgv1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Dgv1.MouseClick
        Dim i As Integer
        i = Me.Dgv1.CurrentRow.Index
        With Dgv1.Rows.Item(i)
            TxtCari.Text = .Cells(0).Value.ToString
        End With
    End Sub
    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Try
            If TxtCari.Text = "" Then
                MsgBox("NAMA SUPPLIER belum diinput")
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Supplier from tbsupplier WHERE Nama_Supplier ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("NAMA SUPPLIER belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama.MdiParent = form_menu_utama
                                    form_input_nama.Show()
                                    form_input_nama.txt_frm.Text = "form_input_supplier"
                                    form_input_nama.Label1.Text = "Input Supplier Baru"
                                    form_input_nama.Label2.Text = "Nama Supplier"
                                    form_input_nama.txt_nama.Text = TxtCari.Text
                                    form_input_nama.txt_nama.Focus()
                                    form_input_nama.Focus()
                                End If
                            Else
                                If TxtForm.Text = "form_input_kontrak_grey" Then
                                    form_input_kontrak_grey.MdiParent = form_menu_utama
                                    form_input_kontrak_grey.Show()
                                    form_input_kontrak_grey.Focus()
                                    form_input_kontrak_grey.txt_supplier.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_kontrak_grey.txt_total.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_1" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_1.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_1.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_2" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_2.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_2.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_3" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_3.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_3.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_4" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_4.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_4.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_5" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_5.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_5.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_6" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_6.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_6.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_7" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_7.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_7.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_8" Then
                                    form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                    form_input_pembelian_grey_baru.Show()
                                    form_input_pembelian_grey_baru.Focus()
                                    form_input_pembelian_grey_baru.txt_supplier_8.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey_baru.txt_harga_8.Focus()
                                ElseIf TxtForm.Text = "form_input_pembelian_grey" Then
                                    form_input_pembelian_grey.MdiParent = form_menu_utama
                                    form_input_pembelian_grey.Show()
                                    form_input_pembelian_grey.txt_supplier.Text = TxtCari.Text
                                    Me.Close()
                                    form_input_pembelian_grey.txt_keterangan.Focus()
                                    form_input_pembelian_grey.Focus()
                                End If
                            End If
                        End Using
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub ts_keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_keluar.Click
        If TxtForm.Text = "form_input_kontrak_grey" Then
            form_input_kontrak_grey.txt_no_kontrak.Focus()
            form_input_kontrak_grey.Focus()
            Me.Close()
        ElseIf TxtForm.Text = "form_input_pembelian_grey" Then
            form_input_pembelian_grey.txt_no_kontrak.Focus()
            form_input_pembelian_grey.Focus()
            Me.Close()
        Else
            Me.Close()
        End If
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_nama.MdiParent = form_menu_utama
        form_input_nama.Show()
        form_input_nama.txt_frm.Text = "form_input_supplier"
        form_input_nama.Label1.Text = "Input Supplier Baru"
        form_input_nama.Label2.Text = "Nama Supplier"
        form_input_nama.Focus()
    End Sub
    Private Sub btn_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_perbarui.Click
        Call isidgv()
    End Sub
    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        If TxtCari.Text = "" Then
            MsgBox("NAMA SUPPLIER yang akan dirubah belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Supplier from tbsupplier WHERE Nama_Supplier ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("NAMA SUPPLIER belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama.MdiParent = form_menu_utama
                                    form_input_nama.Show()
                                    form_input_nama.txt_frm.Text = "form_input_supplier"
                                    form_input_nama.Label1.Text = "Input Supplier Baru"
                                    form_input_nama.Label2.Text = "Nama Supplier"
                                    form_input_nama.txt_nama.Text = TxtCari.Text
                                    form_input_nama.txt_nama.Focus()
                                    form_input_nama.Focus()
                                End If
                            Else
                                form_input_nama.MdiParent = form_menu_utama
                                form_input_nama.Show()
                                form_input_nama.btn_simpan_baru.Visible = False
                                form_input_nama.txt_frm.Text = "form_input_supplier"
                                form_input_nama.Label1.Text = "Ubah Nama Supplier"
                                form_input_nama.Label2.Text = "Nama Supplier"
                                form_input_nama.txt_nama.Text = TxtCari.Text
                                form_input_nama.txt_nama.Focus()
                                form_input_nama.Focus()
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        If TxtCari.Text = "" Then
            MsgBox("NAMA SUPPLIER yang akan dihapus belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Supplier FROM tbsupplier WHERE Nama_Supplier ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                If MsgBox("Yakin NAMA SUPPLIER Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Data") = vbYes Then
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "DELETE FROM tbsupplier WHERE Nama_Supplier='" & TxtCari.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            cmdy.ExecuteNonQuery()
                                        End Using
                                        TxtCari.Text = ""
                                        MsgBox("NAMA SUPPLIER berhasil di Hapus")
                                    End Using
                                End If
                            Else
                                MsgBox("NAMA SUPPLIER Belum Tersimpan Di Database")
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

End Class