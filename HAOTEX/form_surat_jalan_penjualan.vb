﻿Imports MySql.Data.MySqlClient

Public Class form_surat_jalan_penjualan

    Private Sub form_surat_jalan_penjualan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call awal()
        dtp_hari_ini.Text = Today
        ts_hapus.Visible = False
        ts_ubah.Visible = False
    End Sub
    Private Sub awal()
        Call isidtpawal()
        Call isidgv()
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(-3)
        dtp_awal.Text = tanggal
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Gudang"
        dgv1.Columns(2).HeaderText = "SJ Penjualan"
        dgv1.Columns(3).HeaderText = "Hasil Packing"
        dgv1.Columns(4).HeaderText = "SJ Packing"
        dgv1.Columns(5).HeaderText = "Customer"
        dgv1.Columns(6).HeaderText = "Jenis Kain"
        dgv1.Columns(7).HeaderText = "Warna"
        dgv1.Columns(8).HeaderText = "Gulung"
        dgv1.Columns(9).HeaderText = "Yards"
        dgv1.Columns(10).HeaderText = "Harga"
        dgv1.Columns(11).HeaderText = "Total Harga"
        dgv1.Columns(12).HeaderText = "Keterangan"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 120
        dgv1.Columns(2).Width = 100
        dgv1.Columns(3).Width = 100
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 120
        dgv1.Columns(6).Width = 120
        dgv1.Columns(7).Width = 100
        dgv1.Columns(8).Width = 80
        dgv1.Columns(9).Width = 100
        dgv1.Columns(10).Width = 100
        dgv1.Columns(11).Width = 150
        dgv1.Columns(12).Width = 120
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Format = "N"
        dgv1.Columns(10).DefaultCellStyle.Format = "C"
        dgv1.Columns(11).DefaultCellStyle.Format = "C"
        dgv1.Columns(13).Visible = False
        dgv1.Columns(14).Visible = False
        dgv1.Columns(15).Visible = False
    End Sub
    Private Sub hitungjumlah()
        Dim totalgulung, totalyard, jumlahtotalharga As Double
        Dim gradea, gradeb, claimjadi, glgradea, glgradeb, glclaimjadi As Double
        totalgulung = 0
        totalyard = 0
        jumlahtotalharga = 0
        gradea = 0
        glgradea = 0
        gradeb = 0
        glgradeb = 0
        claimjadi = 0
        glclaimjadi = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            totalgulung = totalgulung + Val(dgv1.Rows(i).Cells(8).Value)
            totalyard = totalyard + Val(dgv1.Rows(i).Cells(9).Value)
            jumlahtotalharga = jumlahtotalharga + Val(dgv1.Rows(i).Cells(11).Value)
            If dgv1.Rows(i).Cells(3).Value = "Grade A" Then
                gradea = gradea + Val(dgv1.Rows(i).Cells(9).Value)
                glgradea = glgradea + Val(dgv1.Rows(i).Cells(8).Value)
            ElseIf dgv1.Rows(i).Cells(3).Value = "Grade B" Then
                gradeb = gradeb + Val(dgv1.Rows(i).Cells(9).Value)
                glgradeb = glgradeb + Val(dgv1.Rows(i).Cells(8).Value)
            ElseIf dgv1.Rows(i).Cells(3).Value = "Claim Jadi" Then
                claimjadi = claimjadi + Val(dgv1.Rows(i).Cells(9).Value)
                glclaimjadi = glclaimjadi + Val(dgv1.Rows(i).Cells(8).Value)
            End If
        Next
        txt_total_gulung.Text = totalgulung
        txt_total_yard.Text = totalyard
        txt_jumlah_total_harga.Text = jumlahtotalharga
        txt_jumlah_grade_a.Text = gradea
        txt_jumlah_grade_b.Text = gradeb
        txt_jumlah_claim_Jadi.Text = claimjadi
        txt_gulung_grade_a.Text = glgradea
        txt_gulung_grade_b.Text = glgradeb
        txt_gulung_claim_jadi.Text = glclaimjadi
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbsuratjalanpenjualan")
                            dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_awal.Text = dtp_akhir.Text
        End If
        Call isidgv()
    End Sub
    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
        Call isidgv()
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_surat_jalan_penjualan.MdiParent = form_menu_utama
        form_input_surat_jalan_penjualan.Show()
        form_input_surat_jalan_penjualan.Focus()
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        Call awal()
        txt_cari_sj_penjualan.Text = ""
        txt_cari_sj_packing.Text = ""
        txt_cari_gudang_packing.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_customer.Text = ""
        Panel2.Enabled = True
        btn_cari.Text = "CARI"
        btn_cari.Image = HAOTEX.My.Resources.search
        dgv1.Focus()
    End Sub
    Private Sub txt_cari_sj_penjualan_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_sj_penjualan.GotFocus
        txt_cari_sj_packing.Text = ""
        txt_cari_gudang_packing.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_customer.Text = ""
    End Sub
    Private Sub txt_cari_sj_packing_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_sj_packing.GotFocus
        txt_cari_sj_penjualan.Text = ""
        txt_cari_gudang_packing.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_customer.Text = ""
    End Sub
    Private Sub txt_cari_gudang_packing_GotFocus1(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang_packing.GotFocus
        txt_cari_sj_penjualan.Text = ""
        txt_cari_sj_packing.Text = ""
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        txt_cari_sj_penjualan.Text = ""
        txt_cari_sj_packing.Text = ""
    End Sub
    Private Sub txt_cari_customer_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_customer.GotFocus
        txt_cari_sj_penjualan.Text = ""
        txt_cari_sj_packing.Text = ""
    End Sub
    Private Sub reportsuratjalanpenjualan()
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
        End With
        tbheaderfooter.Rows.Add(dtp_awal.Text, dtp_akhir.Text, dtp_hari_ini.Text, txt_jumlah_grade_a.Text, _
                                txt_gulung_grade_a.Text, txt_jumlah_grade_b.Text, txt_gulung_grade_b.Text, _
                                txt_jumlah_claim_Jadi.Text, txt_gulung_claim_jadi.Text, txt_total_yard.Text, _
                                txt_total_gulung.Text, txt_jumlah_total_harga.Text)

        Dim dtreportsuratjalanpenjualan As New DataTable
        With dtreportsuratjalanpenjualan
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
            .Columns.Add("DataColumn13")
            .Columns.Add("DataColumn14")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            dtreportsuratjalanpenjualan.Rows.Add(row.Cells(15).Value, row.Cells(0).FormattedValue, row.Cells(1).Value, _
                                      row.Cells(2).Value, row.Cells(3).Value, row.Cells(4).Value, row.Cells(7).Value, _
                                      row.Cells(8).Value, row.Cells(9).Value, row.Cells(10).Value, _
                                      row.Cells(11).FormattedValue, row.Cells(12).FormattedValue, row.Cells(13).FormattedValue, _
                                      row.Cells(14).Value)
        Next
        form_report_surat_jalan_penjualan.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_surat_jalan_penjualan.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportsuratjalanpenjualan
        form_report_surat_jalan_penjualan.ShowDialog()
        form_report_surat_jalan_penjualan.Dispose()
    End Sub
    Private Sub ts_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportsuratjalanpenjualan()
        End If
    End Sub
    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        If btn_cari.Text = "CARI" Then
            btn_cari.Text = "RESET"
            btn_cari.Image = HAOTEX.My.Resources.refresh
            Panel2.Enabled = False
            If txt_cari_sj_penjualan.Text = "" And txt_cari_sj_packing.Text = "" And txt_cari_gudang_packing.Text = "" _
                And txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv()
            ElseIf Not txt_cari_sj_penjualan.Text = "" Then
                Call isidgv_surat_jalan_penjualan()
            ElseIf Not txt_cari_sj_packing.Text = "" Then
                Call isidgv_sj_packing()
            ElseIf Not txt_cari_gudang_packing.Text = "" And txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing()
            ElseIf Not txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing_jenis_kain()
            ElseIf Not txt_cari_gudang_packing.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing_customer()
            ElseIf Not txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_gudang_packing_jenis_kain_customer()
            ElseIf txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_customer.Text = "" Then
                Call isidgv_jenis_kain()
            ElseIf txt_cari_gudang_packing.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_customer()
            ElseIf txt_cari_gudang_packing.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_customer.Text = "" Then
                Call isidgv_jenis_kain_customer()
            End If
        Else
            ts_perbarui.PerformClick()
        End If
    End Sub
    Private Sub isidgv_surat_jalan_penjualan()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE SJ_Penjualan='" & txt_cari_sj_penjualan.Text & "' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Baris_SJ"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_sj_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE SJ_Packing='" & txt_cari_sj_packing.Text & "' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Baris_SJ_Packing"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_gudang_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Penjualan"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_gudang_packing_jenis_kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Penjualan"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_gudang_packing_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Penjualan"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_gudang_packing_jenis_kain_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Gudang LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Penjualan"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_jenis_kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Penjualan"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Penjualan"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub isidgv_jenis_kain_customer()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,SJ_Packing,Customer,Jenis_Kain,Warna,Gulung,Yards,Harga,Total_Harga,Keterangan,Id_Grey,Id_Penjualan,Id_Grade FROM tbsuratjalanpenjualan WHERE Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Customer LIKE '%" & txt_cari_customer.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Penjualan"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        Call hitungjumlah()
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub txt_jumlah_total_harga_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_total_harga.TextChanged
        txt_jumlah_total_harga.Text = FormatCurrency(txt_jumlah_total_harga.Text)
    End Sub

    Private Sub txt_total_gulung_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_gulung.TextChanged
        If txt_total_gulung.Text <> String.Empty Then
            Dim temp As String = txt_total_gulung.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_gulung.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_gulung.Select(txt_total_gulung.Text.Length, 0)
            ElseIf txt_total_gulung.Text = "-"c Then

            Else
                txt_total_gulung.Text = CDec(temp).ToString("N0")
                txt_total_gulung.Select(txt_total_gulung.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_total_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_yard.TextChanged
        If txt_total_yard.Text <> String.Empty Then
            Dim temp As String = txt_total_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            ElseIf txt_total_yard.Text = "-"c Then

            Else
                txt_total_yard.Text = CDec(temp).ToString("N0")
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_gulung_claim_jadi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gulung_claim_jadi.TextChanged
        If txt_gulung_claim_jadi.Text <> String.Empty Then
            Dim temp As String = txt_gulung_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_gulung_claim_jadi.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_gulung_claim_jadi.Select(txt_gulung_claim_jadi.Text.Length, 0)
            ElseIf txt_gulung_claim_jadi.Text = "-"c Then

            Else
                txt_gulung_claim_jadi.Text = CDec(temp).ToString("N0")
                txt_gulung_claim_jadi.Select(txt_gulung_claim_jadi.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_jumlah_claim_Jadi_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_claim_Jadi.TextChanged
        If txt_gulung_claim_jadi.Text <> String.Empty Then
            Dim temp As String = txt_gulung_claim_jadi.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_gulung_claim_jadi.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_gulung_claim_jadi.Select(txt_gulung_claim_jadi.Text.Length, 0)
            ElseIf txt_gulung_claim_jadi.Text = "-"c Then

            Else
                txt_gulung_claim_jadi.Text = CDec(temp).ToString("N0")
                txt_gulung_claim_jadi.Select(txt_gulung_claim_jadi.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_gulung_grade_b_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gulung_grade_b.TextChanged
        If txt_gulung_grade_b.Text <> String.Empty Then
            Dim temp As String = txt_gulung_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_gulung_grade_b.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_gulung_grade_b.Select(txt_gulung_grade_b.Text.Length, 0)
            ElseIf txt_gulung_grade_b.Text = "-"c Then

            Else
                txt_gulung_grade_b.Text = CDec(temp).ToString("N0")
                txt_gulung_grade_b.Select(txt_gulung_grade_b.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_jumlah_grade_b_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_grade_b.TextChanged
        If txt_jumlah_grade_b.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_grade_b.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_grade_b.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_grade_b.Select(txt_jumlah_grade_b.Text.Length, 0)
            ElseIf txt_jumlah_grade_b.Text = "-"c Then

            Else
                txt_jumlah_grade_b.Text = CDec(temp).ToString("N0")
                txt_jumlah_grade_b.Select(txt_jumlah_grade_b.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_gulung_grade_a_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gulung_grade_a.TextChanged
        If txt_gulung_grade_a.Text <> String.Empty Then
            Dim temp As String = txt_gulung_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_gulung_grade_a.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_gulung_grade_a.Select(txt_gulung_grade_a.Text.Length, 0)
            ElseIf txt_gulung_grade_a.Text = "-"c Then

            Else
                txt_gulung_grade_a.Text = CDec(temp).ToString("N0")
                txt_gulung_grade_a.Select(txt_gulung_grade_a.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub txt_jumlah_grade_a_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_grade_a.TextChanged
        If txt_jumlah_grade_a.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_grade_a.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_grade_a.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_grade_a.Select(txt_jumlah_grade_a.Text.Length, 0)
            ElseIf txt_jumlah_grade_a.Text = "-"c Then

            Else
                txt_jumlah_grade_a.Text = CDec(temp).ToString("N0")
                txt_jumlah_grade_a.Select(txt_jumlah_grade_a.Text.Length, 0)
            End If
        End If
    End Sub
End Class