﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_penjualan_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_id_grey = New System.Windows.Forms.TextBox()
        Me.cb_stok = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_id_jual = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtp_jatuh_tempo = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txt_asal_sj = New System.Windows.Forms.TextBox()
        Me.txt_jumlah_awal = New System.Windows.Forms.TextBox()
        Me.txt_stok_tersedia = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_surat_jalan = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_customer = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_qty = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_jumlah = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txt_penyimpanan = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_keterangan = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_harga = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_simpan_baru = New System.Windows.Forms.Button()
        Me.btn_simpan_tutup = New System.Windows.Forms.Button()
        Me.btn_batal = New System.Windows.Forms.Button()
        Me.txt_qty_awal = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(441, 24)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(108, 20)
        Me.dtp_hari_ini.TabIndex = 78
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(45, 25)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(43, 13)
        Me.Label15.TabIndex = 37
        Me.Label15.Text = "ID Grey"
        '
        'txt_id_grey
        '
        Me.txt_id_grey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey.Location = New System.Drawing.Point(125, 21)
        Me.txt_id_grey.Name = "txt_id_grey"
        Me.txt_id_grey.ReadOnly = True
        Me.txt_id_grey.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_grey.TabIndex = 1
        '
        'cb_stok
        '
        Me.cb_stok.BackColor = System.Drawing.SystemColors.Window
        Me.cb_stok.FormattingEnabled = True
        Me.cb_stok.Items.AddRange(New Object() {"Titipan", "Proses", "Ex Retur Celup"})
        Me.cb_stok.Location = New System.Drawing.Point(125, 201)
        Me.cb_stok.Name = "cb_stok"
        Me.cb_stok.Size = New System.Drawing.Size(121, 21)
        Me.cb_stok.TabIndex = 43
        Me.cb_stok.Text = "--Pilih Stok--"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(45, 205)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(29, 13)
        Me.Label18.TabIndex = 42
        Me.Label18.Text = "Stok"
        '
        'txt_id_jual
        '
        Me.txt_id_jual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_jual.Location = New System.Drawing.Point(125, 51)
        Me.txt_id_jual.Name = "txt_id_jual"
        Me.txt_id_jual.ReadOnly = True
        Me.txt_id_jual.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_jual.TabIndex = 2
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(45, 55)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(40, 13)
        Me.Label16.TabIndex = 38
        Me.Label16.Text = "ID Jual"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(125, 385)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(24, 13)
        Me.Label14.TabIndex = 32
        Me.Label14.Text = "Rp."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(125, 355)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(24, 13)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Rp."
        '
        'dtp_jatuh_tempo
        '
        Me.dtp_jatuh_tempo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_jatuh_tempo.Location = New System.Drawing.Point(125, 111)
        Me.dtp_jatuh_tempo.Name = "dtp_jatuh_tempo"
        Me.dtp_jatuh_tempo.Size = New System.Drawing.Size(99, 20)
        Me.dtp_jatuh_tempo.TabIndex = 4
        '
        'dtp_tanggal
        '
        Me.dtp_tanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal.Location = New System.Drawing.Point(125, 81)
        Me.dtp_tanggal.Name = "dtp_tanggal"
        Me.dtp_tanggal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_tanggal.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Window
        Me.Panel1.Controls.Add(Me.txt_qty_awal)
        Me.Panel1.Controls.Add(Me.txt_asal_sj)
        Me.Panel1.Controls.Add(Me.txt_jumlah_awal)
        Me.Panel1.Controls.Add(Me.txt_stok_tersedia)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.cb_stok)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.txt_id_jual)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.txt_id_grey)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.dtp_jatuh_tempo)
        Me.Panel1.Controls.Add(Me.dtp_tanggal)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txt_surat_jalan)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.txt_customer)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txt_qty)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txt_jumlah)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txt_penyimpanan)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_keterangan)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txt_harga)
        Me.Panel1.Controls.Add(Me.txt_jenis_kain)
        Me.Panel1.Location = New System.Drawing.Point(-1, 68)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(441, 465)
        Me.Panel1.TabIndex = 74
        '
        'txt_asal_sj
        '
        Me.txt_asal_sj.Location = New System.Drawing.Point(445, 111)
        Me.txt_asal_sj.Name = "txt_asal_sj"
        Me.txt_asal_sj.Size = New System.Drawing.Size(100, 20)
        Me.txt_asal_sj.TabIndex = 48
        '
        'txt_jumlah_awal
        '
        Me.txt_jumlah_awal.Location = New System.Drawing.Point(446, 85)
        Me.txt_jumlah_awal.Name = "txt_jumlah_awal"
        Me.txt_jumlah_awal.Size = New System.Drawing.Size(100, 20)
        Me.txt_jumlah_awal.TabIndex = 47
        '
        'txt_stok_tersedia
        '
        Me.txt_stok_tersedia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_stok_tersedia.Location = New System.Drawing.Point(125, 291)
        Me.txt_stok_tersedia.Name = "txt_stok_tersedia"
        Me.txt_stok_tersedia.ReadOnly = True
        Me.txt_stok_tersedia.Size = New System.Drawing.Size(271, 20)
        Me.txt_stok_tersedia.TabIndex = 45
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 295)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 44
        Me.Label3.Text = "Tersedia"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(45, 415)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Keterangan"
        '
        'txt_surat_jalan
        '
        Me.txt_surat_jalan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_surat_jalan.Location = New System.Drawing.Point(125, 141)
        Me.txt_surat_jalan.Name = "txt_surat_jalan"
        Me.txt_surat_jalan.Size = New System.Drawing.Size(271, 20)
        Me.txt_surat_jalan.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(45, 235)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(45, 13)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Gudang"
        '
        'txt_customer
        '
        Me.txt_customer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer.Location = New System.Drawing.Point(125, 171)
        Me.txt_customer.Name = "txt_customer"
        Me.txt_customer.Size = New System.Drawing.Size(271, 20)
        Me.txt_customer.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(45, 385)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 13)
        Me.Label8.TabIndex = 29
        Me.Label8.Text = "Total Harga"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(45, 325)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 13)
        Me.Label7.TabIndex = 28
        Me.Label7.Text = "Qty"
        '
        'txt_qty
        '
        Me.txt_qty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_qty.Location = New System.Drawing.Point(125, 321)
        Me.txt_qty.Name = "txt_qty"
        Me.txt_qty.Size = New System.Drawing.Size(271, 20)
        Me.txt_qty.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(45, 355)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Harga"
        '
        'txt_jumlah
        '
        Me.txt_jumlah.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah.Location = New System.Drawing.Point(149, 381)
        Me.txt_jumlah.Name = "txt_jumlah"
        Me.txt_jumlah.Size = New System.Drawing.Size(247, 20)
        Me.txt_jumlah.TabIndex = 18
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 175)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Customer"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(45, 145)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(60, 13)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Surat Jalan"
        '
        'txt_penyimpanan
        '
        Me.txt_penyimpanan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_penyimpanan.Location = New System.Drawing.Point(125, 231)
        Me.txt_penyimpanan.Name = "txt_penyimpanan"
        Me.txt_penyimpanan.Size = New System.Drawing.Size(271, 20)
        Me.txt_penyimpanan.TabIndex = 19
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(45, 265)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Jenis Kain"
        '
        'txt_keterangan
        '
        Me.txt_keterangan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan.Location = New System.Drawing.Point(125, 412)
        Me.txt_keterangan.Name = "txt_keterangan"
        Me.txt_keterangan.Size = New System.Drawing.Size(271, 20)
        Me.txt_keterangan.TabIndex = 20
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(45, 115)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(69, 13)
        Me.Label13.TabIndex = 23
        Me.Label13.Text = "Jatuh Tempo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(45, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Tanggal"
        '
        'txt_harga
        '
        Me.txt_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga.Location = New System.Drawing.Point(150, 351)
        Me.txt_harga.Name = "txt_harga"
        Me.txt_harga.Size = New System.Drawing.Size(246, 20)
        Me.txt_harga.TabIndex = 15
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain.Location = New System.Drawing.Point(125, 261)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.Size = New System.Drawing.Size(271, 20)
        Me.txt_jenis_kain.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 25)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "Label1"
        '
        'btn_simpan_baru
        '
        Me.btn_simpan_baru.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_baru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_baru.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_baru.Image = Global.HAOTEX.My.Resources.Resources.action_add
        Me.btn_simpan_baru.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_baru.Location = New System.Drawing.Point(43, 560)
        Me.btn_simpan_baru.Name = "btn_simpan_baru"
        Me.btn_simpan_baru.Size = New System.Drawing.Size(120, 23)
        Me.btn_simpan_baru.TabIndex = 77
        Me.btn_simpan_baru.Text = "Simpan & Baru"
        Me.btn_simpan_baru.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_baru.UseMnemonic = False
        Me.btn_simpan_baru.UseVisualStyleBackColor = True
        '
        'btn_simpan_tutup
        '
        Me.btn_simpan_tutup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_tutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_tutup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_tutup.Image = Global.HAOTEX.My.Resources.Resources.save1
        Me.btn_simpan_tutup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_tutup.Location = New System.Drawing.Point(184, 560)
        Me.btn_simpan_tutup.Name = "btn_simpan_tutup"
        Me.btn_simpan_tutup.Size = New System.Drawing.Size(127, 23)
        Me.btn_simpan_tutup.TabIndex = 75
        Me.btn_simpan_tutup.Text = "Simpan & Tutup"
        Me.btn_simpan_tutup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_tutup.UseMnemonic = False
        Me.btn_simpan_tutup.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Image = Global.HAOTEX.My.Resources.Resources.action_delete
        Me.btn_batal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal.Location = New System.Drawing.Point(332, 560)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(64, 23)
        Me.btn_batal.TabIndex = 76
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal.UseMnemonic = False
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'txt_qty_awal
        '
        Me.txt_qty_awal.Location = New System.Drawing.Point(448, 153)
        Me.txt_qty_awal.Name = "txt_qty_awal"
        Me.txt_qty_awal.Size = New System.Drawing.Size(100, 20)
        Me.txt_qty_awal.TabIndex = 49
        '
        'form_input_penjualan_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(439, 606)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.btn_simpan_baru)
        Me.Controls.Add(Me.btn_simpan_tutup)
        Me.Controls.Add(Me.btn_batal)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_penjualan_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents btn_simpan_baru As System.Windows.Forms.Button
    Friend WithEvents btn_simpan_tutup As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_id_grey As System.Windows.Forms.TextBox
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents cb_stok As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_id_jual As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtp_jatuh_tempo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txt_surat_jalan As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txt_customer As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_harga As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_qty As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txt_jumlah As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txt_penyimpanan As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_keterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txt_stok_tersedia As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_jumlah_awal As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_awal As System.Windows.Forms.TextBox
End Class
