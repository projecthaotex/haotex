﻿Imports MySql.Data.MySqlClient

Public Class form_pembelian_grey

    Private Sub form_pembelian_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If form_menu_utama.Status2.Text = "User" Then
            ts_ubah.Visible = False
            ts_hapus.Visible = False
        ElseIf form_menu_utama.Status2.Text = "Admin" Then
            ts_ubah.Visible = True
            ts_hapus.Visible = True
        End If
        Call awal()
    End Sub
    Private Sub awal()
        dtp_hari_ini.Text = Today
        dtp_akhir.Text = Today
        Call isidtpawal()
        Call isidgv()
        Call hitungjumlah()
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        If form_menu_utama.Status2.Text = "User" Then
            tanggal = tanggal.AddMonths(-1)
            dtp_awal.Text = tanggal
        ElseIf form_menu_utama.Status2.Text = "Admin" Then
            tanggal = tanggal.AddMonths(-3)
            dtp_awal.Text = tanggal
        End If
    End Sub
    Private Sub hitungjumlah()
        Dim jumlahqty, jumlahtotalharga, jumlahqty_yard, jumlahtotalharga_yard As Double
        jumlahqty = 0
        jumlahtotalharga = 0
        jumlahqty_yard = 0
        jumlahtotalharga_yard = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(6).Value = "Meter" Then
                jumlahqty = jumlahqty + Val(dgv1.Rows(i).Cells(5).Value)
                jumlahtotalharga = jumlahtotalharga + Val(dgv1.Rows(i).Cells(8).Value)
            ElseIf dgv1.Rows(i).Cells(6).Value = "Yard" Then
                jumlahqty_yard = jumlahqty_yard + Val(dgv1.Rows(i).Cells(5).Value)
                jumlahtotalharga_yard = jumlahtotalharga_yard + Val(dgv1.Rows(i).Cells(8).Value)
            End If
        Next
        txt_jumlah_qty.Text = jumlahqty
        txt_jumlah_total_harga.Text = jumlahtotalharga
        txt_qty_yard.Text = jumlahqty_yard
        txt_jumlah_yard.Text = jumlahtotalharga_yard
        txt_grand_total.Text = jumlahtotalharga + jumlahtotalharga_yard
    End Sub
    Private Sub headertable()
        dgv1.Columns(0).HeaderText = "TANGGAL"
        dgv1.Columns(1).HeaderText = "KONTRAK"
        dgv1.Columns(2).HeaderText = "SJ"
        dgv1.Columns(3).HeaderText = "JENIS KAIN"
        dgv1.Columns(4).HeaderText = "SUPPLIER"
        dgv1.Columns(5).HeaderText = "QTY"
        dgv1.Columns(6).HeaderText = "SATUAN"
        dgv1.Columns(7).HeaderText = "HARGA"
        dgv1.Columns(8).HeaderText = "TOTAL"
        dgv1.Columns(9).HeaderText = "GUDANG"
        dgv1.Columns(10).HeaderText = "KETERANGAN"
        dgv1.Columns(11).HeaderText = "JTH TEMPO"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 90
        dgv1.Columns(2).Width = 80
        dgv1.Columns(3).Width = 150
        dgv1.Columns(4).Width = 120
        dgv1.Columns(5).Width = 100
        dgv1.Columns(6).Width = 70
        dgv1.Columns(7).Width = 100
        dgv1.Columns(8).Width = 140
        dgv1.Columns(9).Width = 120
        dgv1.Columns(10).Width = 150
        dgv1.Columns(11).Width = 100
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Format = "N"
        dgv1.Columns(7).DefaultCellStyle.Format = "C"
        dgv1.Columns(8).DefaultCellStyle.Format = "C"
        dgv1.Columns(11).Visible = False
        dgv1.Columns(12).Visible = False
        dgv1.Columns(13).Visible = False
        dgv1.Columns(14).Visible = False
        dgv1.Columns(15).Visible = False
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If form_menu_utama.Status2.Text = "User" Then
            Dim tanggal As DateTime
            tanggal = Today
            tanggal = tanggal.AddMonths(-1)
            If dtp_awal.Value < tanggal Then
                dtp_awal.Text = tanggal
            ElseIf dtp_awal.Value > dtp_akhir.Value Then
                dtp_awal.Text = dtp_akhir.Text
            End If
        ElseIf form_menu_utama.Status2.Text = "Admin" Then
            If dtp_awal.Value > dtp_akhir.Value Then
                dtp_awal.Text = dtp_akhir.Text
            End If
        End If
    End Sub
    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        Call awal()
        txt_cari_jenis_kain.Text = ""
        txt_cari_supplier.Text = ""
        txt_cari_penyimpanan.Text = ""
        cb_kontrak.Checked = True
        cb_non_kontrak.Checked = True
        Panel3.Enabled = True
        btn_cari.Text = "CARI"
        btn_cari.Image = HAOTEX.My.Resources.search
        dgv1.Focus()
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_pembelian_grey_baru.MdiParent = form_menu_utama
        form_input_pembelian_grey_baru.Show()
        form_input_pembelian_grey_baru.Focus()
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        Try
            If dgv1.RowCount = 0 Then
                MsgBox("Tidak Terdapat Data Untuk DiHAPUS")
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx As String = "SELECT * FROM tbpoproses WHERE Id_Beli='" & dgv1.CurrentRow.Cells(12).Value.ToString & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                MsgBox("Barang Yang Dipilih Sudah Masuk PO Proses Tidak Bisa DIHAPUS")
                            Else
                                form_input_pembelian_grey.MdiParent = form_menu_utama
                                form_input_pembelian_grey.Show()
                                form_input_pembelian_grey.Label1.Text = "Hapus Pembelian Grey"
                                form_input_pembelian_grey.btn_simpan_tutup.Visible = False
                                form_input_pembelian_grey.btn_batal.Visible = False
                                form_input_pembelian_grey.txt_sisa_kontrak.Visible = False
                                form_input_pembelian_grey.Label17.Visible = False
                                form_input_pembelian_grey.txt_konversi.Text = "tidak"
                                Dim i As Integer
                                i = Me.dgv1.CurrentRow.Index
                                With dgv1.Rows.Item(i)
                                    form_input_pembelian_grey.dtp_tanggal.Text = .Cells(0).Value.ToString
                                    form_input_pembelian_grey.txt_no_kontrak.Text = .Cells(1).Value.ToString
                                    form_input_pembelian_grey.txt_surat_jalan.Text = .Cells(2).Value.ToString
                                    form_input_pembelian_grey.txt_jenis_kain.Text = .Cells(3).Value.ToString
                                    form_input_pembelian_grey.txt_supplier.Text = .Cells(4).Value.ToString
                                    form_input_pembelian_grey.txt_asal_supplier.Text = .Cells(15).Value.ToString
                                    form_input_pembelian_grey.txt_qty.Text = .Cells(5).Value.ToString
                                    form_input_pembelian_grey.cb_satuan.Text = .Cells(6).Value.ToString
                                    form_input_pembelian_grey.txt_harga.Text = .Cells(7).Value.ToString
                                    form_input_pembelian_grey.txt_jumlah.Text = .Cells(8).Value.ToString
                                    form_input_pembelian_grey.txt_penyimpanan.Text = .Cells(9).Value.ToString
                                    form_input_pembelian_grey.txt_keterangan.Text = .Cells(10).Value.ToString
                                    form_input_pembelian_grey.dtp_jatuh_tempo.Text = .Cells(11).Value.ToString
                                    form_input_pembelian_grey.txt_id_beli.Text = .Cells(12).Value.ToString
                                    form_input_pembelian_grey.txt_id_grey.Text = .Cells(13).Value.ToString
                                    form_input_pembelian_grey.ComboBox1.Text = .Cells(14).Value.ToString
                                End With
                                If MsgBox("Yakin PEMBELIAN GREY Akan DIHAPUS ?", vbYesNo + vbQuestion, "Hapus Pembelian") = vbYes Then
                                    Call hapus_tbpembeliangrey()
                                    Call hapus_tbstokproses()
                                    Call hapus_tbdetil_hutang()
                                    Call hapus_update_tbkontrak_grey()
                                    ts_perbarui.PerformClick()
                                    form_kontrak_grey.ts_perbarui.PerformClick()
                                    form_input_pembelian_grey.Close()
                                    MsgBox("PEMBELIAN Berhasil di HAPUS")
                                Else
                                    form_input_pembelian_grey.Close()
                                End If
                            End If
                        End Using
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        Try
            If dgv1.RowCount = 0 Then
                MsgBox("Tidak Terdapat Data Untuk DiUBAH")
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx As String = "SELECT * FROM tbpoproses WHERE Id_Beli='" & dgv1.CurrentRow.Cells(12).Value.ToString & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                MsgBox("Barang Yang Dipilih Sudah Masuk PO Proses Tidak Bisa DIUBAH")
                            Else
                                form_input_pembelian_grey.MdiParent = form_menu_utama
                                form_input_pembelian_grey.Show()
                                form_input_pembelian_grey.Focus()
                                form_input_pembelian_grey.Label1.Text = "Ubah Pembelian Grey"
                                form_input_pembelian_grey.txt_no_kontrak.ReadOnly = True
                                form_input_pembelian_grey.txt_konversi.Text = "tidak"
                                Dim i As Integer
                                i = dgv1.CurrentRow.Index
                                If dgv1.Rows(i).Cells(1).Value.ToString = "-" Then
                                    form_input_pembelian_grey.txt_sisa_kontrak.Visible = False
                                    form_input_pembelian_grey.Label17.Visible = False
                                Else

                                End If
                                With dgv1.Rows.Item(i)
                                    form_input_pembelian_grey.dtp_tanggal.Text = .Cells(0).Value.ToString
                                    form_input_pembelian_grey.txt_no_kontrak.Text = .Cells(1).Value.ToString
                                    form_input_pembelian_grey.txt_surat_jalan.Text = .Cells(2).Value.ToString
                                    form_input_pembelian_grey.txt_jenis_kain.Text = .Cells(3).Value.ToString
                                    form_input_pembelian_grey.txt_supplier.Text = .Cells(4).Value.ToString
                                    form_input_pembelian_grey.txt_asal_supplier.Text = .Cells(15).Value.ToString
                                    form_input_pembelian_grey.txt_qty.Text = .Cells(5).Value.ToString
                                    form_input_pembelian_grey.txt_qty_awal.Text = .Cells(5).Value.ToString
                                    form_input_pembelian_grey.cb_satuan.Text = .Cells(6).Value.ToString
                                    form_input_pembelian_grey.txt_satuan_awal.Text = .Cells(6).Value.ToString
                                    form_input_pembelian_grey.txt_harga.Text = .Cells(7).Value.ToString
                                    form_input_pembelian_grey.txt_jumlah.Text = .Cells(8).Value.ToString
                                    form_input_pembelian_grey.txt_penyimpanan.Text = .Cells(9).Value.ToString
                                    form_input_pembelian_grey.txt_keterangan.Text = .Cells(10).Value.ToString
                                    form_input_pembelian_grey.dtp_jatuh_tempo.Text = .Cells(11).Value.ToString
                                    form_input_pembelian_grey.txt_id_beli.Text = .Cells(12).Value.ToString
                                    form_input_pembelian_grey.txt_id_grey.Text = .Cells(13).Value.ToString
                                    form_input_pembelian_grey.ComboBox1.Text = .Cells(14).Value.ToString
                                End With
                            End If
                        End Using
                    End Using
                End Using
                If Not dgv1.CurrentRow.Cells(1).Value = "-" Then
                    Using conz As New MySqlConnection(sLocalConn)
                        conz.Open()
                        Dim sqlz As String = "SELECT Total,Dikirim,Sisa,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & form_input_pembelian_grey.txt_id_grey.Text & "'"
                        Using cmdz As New MySqlCommand(sqlz, conz)
                            Using drz As MySqlDataReader = cmdz.ExecuteReader
                                drz.Read()
                                If drz.HasRows Then
                                    form_input_pembelian_grey.txt_total_kontrak.Text = drz(0)
                                    form_input_pembelian_grey.txt_dikirim_awal.Text = drz(1)
                                    form_input_pembelian_grey.txt_sisa_awal.Text = drz(2)
                                    form_input_pembelian_grey.txt_satuan.Text = drz(3)
                                End If
                                Dim s As Double
                                If form_input_pembelian_grey.txt_satuan.Text = form_input_pembelian_grey.cb_satuan.Text Then
                                    form_input_pembelian_grey.txt_sisa_kontrak.Text = Math.Round(Val(form_input_pembelian_grey.txt_sisa_awal.Text) + dgv1.CurrentRow.Cells(5).Value, 2)
                                ElseIf form_input_pembelian_grey.txt_satuan.Text = "Meter" And form_input_pembelian_grey.cb_satuan.Text = "Yard" Then
                                    s = Math.Round(Val(form_input_pembelian_grey.txt_sisa_awal.Text) * 1.0936, 0)
                                    form_input_pembelian_grey.txt_sisa_kontrak.Text = Math.Round(Val(form_input_pembelian_grey.txt_qty_awal.Text) + s, 2)
                                ElseIf form_input_pembelian_grey.txt_satuan.Text = "Yard" And form_input_pembelian_grey.cb_satuan.Text = "Meter" Then
                                    s = Math.Round(Val(form_input_pembelian_grey.txt_sisa_awal.Text) * 0.9144, 0)
                                    form_input_pembelian_grey.txt_sisa_kontrak.Text = Math.Round(Val(form_input_pembelian_grey.txt_qty_awal.Text) + s, 2)
                                End If
                            End Using
                        End Using
                    End Using
                End If
                form_input_pembelian_grey.txt_konversi.Text = ""
                form_input_pembelian_grey.txt_keterangan.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub hapus_tbpembeliangrey()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpembeliangrey WHERE Id_Beli ='" & form_input_pembelian_grey.txt_id_beli.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokproses()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokprosesgrey WHERE Id_Beli ='" & form_input_pembelian_grey.txt_id_beli.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetil_hutang()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_hutang ='" & form_input_pembelian_grey.txt_id_hutang.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbkontrak_grey()
        Dim qty As String = form_input_pembelian_grey.txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Total,Dikirim,Satuan FROM tbkontrakgrey WHERE Id_Grey='" & form_input_pembelian_grey.txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m, d As Double
                        m = drx.Item(0)
                        d = drx.Item(1)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbkontrakgrey SET Dikirim=@1,Sisa=@2 WHERE Id_Grey='" & form_input_pembelian_grey.txt_id_grey.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(2) = form_input_pembelian_grey.cb_satuan.Text Then
                                        .Parameters.AddWithValue("@1", Math.Round(d - Val(qty.Replace(",", ".")), 2))
                                        .Parameters.AddWithValue("@2", Math.Round(m - (d - Val(qty.Replace(",", "."))), 2))
                                    ElseIf drx(2) = "Meter" And form_input_pembelian_grey.cb_satuan.Text = "Yard" Then
                                        q = Math.Round(Val(qty.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(d - q, 2))
                                        .Parameters.AddWithValue("@2", Math.Round(m - (d - q), 2))
                                    ElseIf drx(2) = "Yard" And form_input_pembelian_grey.cb_satuan.Text = "Meter" Then
                                        q = Math.Round(Val(qty.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", Math.Round(d - q, 2))
                                        .Parameters.AddWithValue("@2", Math.Round(m - (d - q), 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub reportbeligrey()
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
        End With
        tbheaderfooter.Rows.Add(dtp_awal.Text, dtp_akhir.Text, dtp_hari_ini.Text, _
                                txt_jumlah_qty.Text, txt_jumlah_total_harga.Text, _
                                txt_qty_yard.Text, txt_jumlah_yard.Text, txt_grand_total.Text)
        Dim dtreportkontrakgrey As New DataTable
        With dtreportkontrakgrey
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            If Not row.Cells(8).Value = 0 Then
                dtreportkontrakgrey.Rows.Add(row.Cells(0).FormattedValue, row.Cells(1).Value, row.Cells(2).Value, _
                                         row.Cells(3).Value, row.Cells(4).Value, row.Cells(5).FormattedValue, _
                                         row.Cells(6).Value, row.Cells(7).FormattedValue, row.Cells(8).FormattedValue, _
                                         row.Cells(9).Value, row.Cells(10).Value)
            End If
        Next
        form_report_pembelian_grey.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_pembelian_grey.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportkontrakgrey
        form_report_pembelian_grey.ShowDialog()
        form_report_pembelian_grey.Dispose()
    End Sub
    Private Sub ts_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportbeligrey()
        End If
    End Sub

    Private Sub K_N()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_N_S()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_S()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N_S()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_N_J()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_J()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N_J()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_N_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_N_S_J()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_S_J()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N_S_J()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_N_S_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_S_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N_S_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_N_J_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_J_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N_J_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_N_S_J_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub K_S_J_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND NOT No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub N_S_J_G()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,No_Kontrak,SJ,Jenis_Kain,Supplier,QTY,Satuan,Harga,Jumlah,Gudang,Keterangan,Jatuh_Tempo,Id_Beli,Id_Grey,Lama_Jt,Asal_Supplier FROM tbpembeliangrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Gudang like '%" & txt_cari_penyimpanan.Text & "%' AND No_Kontrak = '-' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpembeliangrey")
                            dgv1.DataSource = dsx.Tables("tbpembeliangrey")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        If btn_cari.Text = "CARI" Then
            If cb_kontrak.Checked = False And cb_non_kontrak.Checked = False Then
                MsgBox("Pilihan KONTRAK dan NON KONTRAK Tidak Boleh Kosong Keduanya", MsgBoxStyle.Exclamation)
                cb_kontrak.Focus()
            Else
                btn_cari.Text = "RESET"
                btn_cari.Image = HAOTEX.My.Resources.refresh
                Panel3.Enabled = False
                If txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "" And txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N()
                    End If
                ElseIf Not txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "" And txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N_S()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K_S()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N_S()
                    End If
                ElseIf txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N_J()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K_J()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N_J()
                    End If
                ElseIf txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N_G()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K_G()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N_G()
                    End If
                ElseIf Not txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N_S_J()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K_S_J()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N_S_J()
                    End If
                ElseIf Not txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N_S_G()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K_S_G()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N_S_G()
                    End If
                ElseIf txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N_J_G()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K_J_G()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N_J_G()
                    End If
                ElseIf Not txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_penyimpanan.Text = "" Then
                    If cb_kontrak.Checked = True And cb_non_kontrak.Checked = True Then
                        Call K_N_S_J_G()
                    ElseIf cb_kontrak.Checked = True And cb_non_kontrak.Checked = False Then
                        Call K_S_J_G()
                    ElseIf cb_kontrak.Checked = False And cb_non_kontrak.Checked = True Then
                        Call N_S_J_G()
                    End If
                End If
            End If
        Else
            ts_perbarui.PerformClick()
        End If
    End Sub

    Private Sub txt_jumlah_qty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_jumlah_qty.KeyPress, txt_jumlah_total_harga.KeyPress, _
        txt_qty_yard.KeyPress, txt_jumlah_yard.KeyPress, txt_grand_total.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub txt_jumlah_qty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_qty.TextChanged
        If txt_jumlah_qty.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_qty.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_qty.Select(txt_jumlah_qty.Text.Length, 0)
            ElseIf txt_jumlah_qty.Text = "-"c Then

            Else
                txt_jumlah_qty.Text = CDec(temp).ToString("N0")
                txt_jumlah_qty.Select(txt_jumlah_qty.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_jumlah_total_harga_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_total_harga.TextChanged
        txt_jumlah_total_harga.Text = FormatCurrency(txt_jumlah_total_harga.Text)
    End Sub
    Private Sub txt_grand_total_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_grand_total.TextChanged
        txt_grand_total.Text = FormatCurrency(txt_grand_total.Text)
    End Sub
    Private Sub txt_jumlah_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_yard.TextChanged
        txt_jumlah_yard.Text = FormatCurrency(txt_jumlah_yard.Text)
    End Sub
    Private Sub txt_qty_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_yard.TextChanged
        If txt_qty_yard.Text <> String.Empty Then
            Dim temp As String = txt_qty_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_yard.Select(txt_qty_yard.Text.Length, 0)
            ElseIf txt_qty_yard.Text = "-"c Then

            Else
                txt_qty_yard.Text = CDec(temp).ToString("N0")
                txt_qty_yard.Select(txt_qty_yard.Text.Length, 0)
            End If
        End If
    End Sub
End Class