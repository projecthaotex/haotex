﻿Imports MySql.Data.MySqlClient

Public Class form_tracking_id_grey

    Private Sub form_tracking_id_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btn_print.Enabled = False
    End Sub

    Private Sub isi_jenis_kain_kontrak()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly As String = "SELECT Jenis_Kain FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                Using dry As MySqlDataReader = cmdy.ExecuteReader
                    dry.Read()
                    If dry.HasRows Then
                        txt_jenis_kain.Text = dry.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub isi_jenis_kain_pembelian()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly As String = "SELECT Jenis_Kain FROM tbpembeliangrey WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                Using dry As MySqlDataReader = cmdy.ExecuteReader
                    dry.Read()
                    If dry.HasRows Then
                        txt_jenis_kain.Text = dry.Item(0)
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertabel_kontrak()
        dgv_kontrak.Columns(0).HeaderText = "Tanggal"
        dgv_kontrak.Columns(1).HeaderText = "No. Kontrak"
        dgv_kontrak.Columns(2).HeaderText = "Supplier"
        dgv_kontrak.Columns(3).HeaderText = "Total"
        dgv_kontrak.Columns(4).HeaderText = "Dikirim"
        dgv_kontrak.Columns(5).HeaderText = "Sisa"
        dgv_kontrak.Columns(0).Width = 80
        dgv_kontrak.Columns(1).Width = 100
        dgv_kontrak.Columns(2).Width = 120
        dgv_kontrak.Columns(3).Width = 70
        dgv_kontrak.Columns(4).Width = 70
        dgv_kontrak.Columns(5).Width = 70
        dgv_kontrak.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_kontrak.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv_kontrak.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv_kontrak.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgvkontrakgrey()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,No_Kontrak,Supplier,Total,Dikirim,Sisa FROM tbkontrakgrey WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkontrakgrey")
                        dgv_kontrak.DataSource = dsx.Tables("tbkontrakgrey")
                        Call headertabel_kontrak()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertable_pembelian()
        dgv_pembelian.Columns(0).HeaderText = "Tanggal"
        dgv_pembelian.Columns(1).HeaderText = "Surat Jalan"
        dgv_pembelian.Columns(2).HeaderText = "Supplier"
        dgv_pembelian.Columns(3).HeaderText = "Qty"
        dgv_pembelian.Columns(4).HeaderText = "Gudang"
        dgv_pembelian.Columns(0).Width = 80
        dgv_pembelian.Columns(1).Width = 100
        dgv_pembelian.Columns(2).Width = 120
        dgv_pembelian.Columns(3).Width = 80
        dgv_pembelian.Columns(4).Width = 120
        dgv_pembelian.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_pembelian.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgv_pembelian()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,SJ,Supplier,QTY,Gudang FROM tbpembeliangrey WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpembeliangrey")
                        dgv_pembelian.DataSource = dsx.Tables("tbpembeliangrey")
                        Call headertable_pembelian()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertabel_po_proses()
        dgv_po_proses.Columns(0).HeaderText = "Tanggal"
        dgv_po_proses.Columns(1).HeaderText = "Gudang"
        dgv_po_proses.Columns(2).HeaderText = "No PO"
        dgv_po_proses.Columns(3).HeaderText = "Baris"
        dgv_po_proses.Columns(4).HeaderText = "QTY"
        dgv_po_proses.Columns(5).HeaderText = "Warna"
        dgv_po_proses.Columns(6).HeaderText = "Resep"
        dgv_po_proses.Columns(0).Width = 80
        dgv_po_proses.Columns(1).Width = 100
        dgv_po_proses.Columns(2).Width = 80
        dgv_po_proses.Columns(3).Width = 80
        dgv_po_proses.Columns(4).Width = 80
        dgv_po_proses.Columns(5).Width = 100
        dgv_po_proses.Columns(6).Width = 100
        dgv_po_proses.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_proses.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_proses.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_proses.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgv_po_proses()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,No_Urut,QTY,Warna,Resep FROM tbpoproses WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpoproses")
                        dgv_po_proses.DataSource = dsx.Tables("tbpoproses")
                        Call headertabel_po_proses()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertabel_sj_proses()
        dgv_sj_proses.Columns(0).HeaderText = "Tanggal"
        dgv_sj_proses.Columns(1).HeaderText = "Gudang"
        dgv_sj_proses.Columns(2).HeaderText = "SJ Proses"
        dgv_sj_proses.Columns(3).HeaderText = "Baris SJ"
        dgv_sj_proses.Columns(4).HeaderText = "Warna"
        dgv_sj_proses.Columns(5).HeaderText = "Resep"
        dgv_sj_proses.Columns(6).HeaderText = "Partai"
        dgv_sj_proses.Columns(7).HeaderText = "Gulung"
        dgv_sj_proses.Columns(8).HeaderText = "Meter"
        dgv_sj_proses.Columns(9).HeaderText = "Gudang Packing"
        dgv_sj_proses.Columns(0).Width = 80
        dgv_sj_proses.Columns(1).Width = 120
        dgv_sj_proses.Columns(2).Width = 100
        dgv_sj_proses.Columns(3).Width = 80
        dgv_sj_proses.Columns(4).Width = 120
        dgv_sj_proses.Columns(5).Width = 120
        dgv_sj_proses.Columns(6).Width = 100
        dgv_sj_proses.Columns(7).Width = 80
        dgv_sj_proses.Columns(8).Width = 80
        dgv_sj_proses.Columns(9).Width = 150
        dgv_sj_proses.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_proses.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_proses.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_proses.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
       
    End Sub

    Private Sub isidgv_sj_proses()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,Baris_SJ,Warna,Resep,Partai,Gulung,Meter,Gudang_Packing FROM tbsuratjalanproses WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv_sj_proses.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel_sj_proses()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertabel_po_packing()
        dgv_po_packing.Columns(0).HeaderText = "Tanggal"
        dgv_po_packing.Columns(1).HeaderText = "Gudang"
        dgv_po_packing.Columns(2).HeaderText = "No PO"
        dgv_po_packing.Columns(3).HeaderText = "Baris PO"
        dgv_po_packing.Columns(4).HeaderText = "Warna"
        dgv_po_packing.Columns(5).HeaderText = "Resep"
        dgv_po_packing.Columns(6).HeaderText = "Partai"
        dgv_po_packing.Columns(7).HeaderText = "Gulung"
        dgv_po_packing.Columns(8).HeaderText = "Meter"
        dgv_po_packing.Columns(9).HeaderText = "Customer"
        dgv_po_packing.Columns(0).Width = 80
        dgv_po_packing.Columns(1).Width = 120
        dgv_po_packing.Columns(2).Width = 100
        dgv_po_packing.Columns(3).Width = 75
        dgv_po_packing.Columns(4).Width = 120
        dgv_po_packing.Columns(5).Width = 120
        dgv_po_packing.Columns(6).Width = 100
        dgv_po_packing.Columns(7).Width = 80
        dgv_po_packing.Columns(8).Width = 80
        dgv_po_packing.Columns(9).Width = 150
        dgv_po_packing.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_packing.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_packing.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_packing.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_packing.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_packing.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_packing.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_po_packing.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgv_po_packing()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,No_PO,Baris_PO,Warna,Resep,Partai,Gulung,Meter,Customer FROM tbpopacking WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpopacking")
                        dgv_po_packing.DataSource = dsx.Tables("tbpopacking")
                        Call headertabel_po_packing()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertabel_sj_packing()
        dgv_sj_packing.Columns(0).HeaderText = "Tanggal"
        dgv_sj_packing.Columns(1).HeaderText = "Gudang"
        dgv_sj_packing.Columns(2).HeaderText = "SJ Packing"
        dgv_sj_packing.Columns(3).HeaderText = "Baris SJ"
        dgv_sj_packing.Columns(4).HeaderText = "Warna"
        dgv_sj_packing.Columns(5).HeaderText = "Gulung"
        dgv_sj_packing.Columns(6).HeaderText = "Yards"
        dgv_sj_packing.Columns(7).HeaderText = "Asal Meter"
        dgv_sj_packing.Columns(8).HeaderText = "Susut (%)"
        dgv_sj_packing.Columns(9).HeaderText = "Customer"
        dgv_sj_packing.Columns(0).Width = 80
        dgv_sj_packing.Columns(1).Width = 120
        dgv_sj_packing.Columns(2).Width = 100
        dgv_sj_packing.Columns(3).Width = 80
        dgv_sj_packing.Columns(4).Width = 120
        dgv_sj_packing.Columns(5).Width = 80
        dgv_sj_packing.Columns(6).Width = 90
        dgv_sj_packing.Columns(7).Width = 90
        dgv_sj_packing.Columns(8).Width = 90
        dgv_sj_packing.Columns(9).Width = 120
        dgv_sj_packing.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_packing.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_packing.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_packing.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv_sj_packing.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv_sj_packing.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv_sj_packing.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgv_sj_packing()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Packing,Baris_SJ,Warna,Gulung,Yards,Asal_Meter,Susut,Customer FROM tbkeluarbarangjadi WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkeluarbarangjadi")
                        dgv_sj_packing.DataSource = dsx.Tables("tbkeluarbarangjadi")
                        Call headertabel_sj_packing()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertabel_sj_penjualan()
        dgv_sj_penjualan.Columns(0).HeaderText = "Tanggal"
        dgv_sj_penjualan.Columns(1).HeaderText = "Gudang"
        dgv_sj_penjualan.Columns(2).HeaderText = "SJ Penjualan"
        dgv_sj_penjualan.Columns(3).HeaderText = "Grade / Claim"
        dgv_sj_penjualan.Columns(4).HeaderText = "Customer"
        dgv_sj_penjualan.Columns(5).HeaderText = "Warna"
        dgv_sj_penjualan.Columns(6).HeaderText = "Gulung"
        dgv_sj_penjualan.Columns(7).HeaderText = "Yards"
        dgv_sj_penjualan.Columns(0).Width = 80
        dgv_sj_penjualan.Columns(1).Width = 120
        dgv_sj_penjualan.Columns(2).Width = 100
        dgv_sj_penjualan.Columns(3).Width = 100
        dgv_sj_penjualan.Columns(4).Width = 130
        dgv_sj_penjualan.Columns(5).Width = 120
        dgv_sj_penjualan.Columns(6).Width = 80
        dgv_sj_penjualan.Columns(7).Width = 80
        dgv_sj_penjualan.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_penjualan.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_penjualan.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_penjualan.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_sj_penjualan.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgv_sj_penjualan()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Penjualan,Hasil_Packing,Customer,Warna,Gulung,Yards FROM tbsuratjalanpenjualan WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanpenjualan")
                        dgv_sj_penjualan.DataSource = dsx.Tables("tbsuratjalanpenjualan")
                        Call headertabel_sj_penjualan()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub headertable_penjualan_grey()
        dgv_penjualan_grey.Columns(0).HeaderText = "Tanggal"
        dgv_penjualan_grey.Columns(1).HeaderText = "Surat Jalan"
        dgv_penjualan_grey.Columns(2).HeaderText = "Customer"
        dgv_penjualan_grey.Columns(3).HeaderText = "Qty"
        dgv_penjualan_grey.Columns(4).HeaderText = "Gudang"
        dgv_penjualan_grey.Columns(0).Width = 80
        dgv_penjualan_grey.Columns(1).Width = 100
        dgv_penjualan_grey.Columns(2).Width = 120
        dgv_penjualan_grey.Columns(3).Width = 80
        dgv_penjualan_grey.Columns(4).Width = 120
        dgv_penjualan_grey.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv_penjualan_grey.Columns(3).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
    End Sub

    Private Sub isidgv_penjualan_grey()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,SJ,Customer,Qty,Gudang FROM tbpenjualangrey WHERE Id_Grey='" & txt_id_grey.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbpenjualangrey")
                        dgv_penjualan_grey.DataSource = dsx.Tables("tbpenjualangrey")
                        Call headertable_penjualan_grey()
                    End Using
                End Using
            End Using
        End Using
    End Sub

    Private Sub btn_tracking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tracking.Click
        Try
            If btn_tracking.Text = "TRACKING" Then
                If txt_id_grey.Text = "" Then
                    MsgBox("ID Grey Belum Diinput")
                    txt_id_grey.Focus()
                Else
                    Call isi_jenis_kain_kontrak()
                    Call isi_jenis_kain_pembelian()
                    Call isidgvkontrakgrey()
                    Call isidgv_pembelian()
                    Call isidgv_po_proses()
                    Call isidgv_sj_proses()
                    Call isidgv_po_packing()
                    Call isidgv_sj_packing()
                    Call isidgv_sj_penjualan()
                    Call isidgv_penjualan_grey()
                    If dgv_kontrak.RowCount = 0 And dgv_pembelian.RowCount = 0 And dgv_po_proses.RowCount = 0 _
                        And dgv_sj_proses.RowCount = 0 And dgv_po_packing.RowCount = 0 And dgv_sj_packing.RowCount = 0 _
                        And dgv_sj_penjualan.RowCount = 0 And dgv_penjualan_grey.RowCount = 0 Then
                        MsgBox("Tidak Terdapat Data Untuk ID Grey " + txt_id_grey.Text)
                        txt_id_grey.Focus()
                    Else
                        txt_id_grey.ReadOnly = True
                        btn_tracking.Text = "RESET"
                        btn_print.Enabled = True
                    End If
                End If
            ElseIf btn_tracking.Text = "RESET" Then
                txt_id_grey.Text = ""
                txt_id_grey.ReadOnly = False
                txt_jenis_kain.Text = ""
                txt_id_grey.Focus()
                btn_tracking.Text = "TRACKING"
                dgv_kontrak.DataSource = Nothing
                dgv_pembelian.DataSource = Nothing
                dgv_po_proses.DataSource = Nothing
                dgv_sj_proses.DataSource = Nothing
                dgv_po_packing.DataSource = Nothing
                dgv_sj_packing.DataSource = Nothing
                dgv_sj_penjualan.DataSource = Nothing
                dgv_penjualan_grey.DataSource = Nothing
                btn_print.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub reporttrackingidgrey()
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
        End With
        tbheaderfooter.Rows.Add(txt_id_grey.Text, txt_jenis_kain.Text, dtp_hari_ini.Text)

        Dim tbkontrak As New DataTable
        With tbkontrak
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
        End With
        Dim x1 As Integer = 0
        Dim r1 As Integer
        r1 = dgv_kontrak.RowCount
        If r1 > 0 Then
            Do
                tbkontrak.Rows.Add(dgv_kontrak.Rows(x1).Cells(0).FormattedValue, dgv_kontrak.Rows(x1).Cells(1).Value, _
                                   dgv_kontrak.Rows(x1).Cells(2).Value, dgv_kontrak.Rows(x1).Cells(3).Value, dgv_kontrak.Rows(x1).Cells(4).Value, dgv_kontrak.Rows(x1).Cells(5).Value)
                x1 = x1 + 1
            Loop Until x1 = r1
        End If

        Dim tbpembelian As New DataTable
        With tbpembelian
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
        End With
        Dim x2 As Integer = 0
        Dim r2 As Integer
        r2 = dgv_pembelian.RowCount
        If r2 > 0 Then
            Do
                tbpembelian.Rows.Add(dgv_pembelian.Rows(x2).Cells(0).FormattedValue, dgv_pembelian.Rows(x2).Cells(1).Value, _
                                     dgv_pembelian.Rows(x2).Cells(2).Value, dgv_pembelian.Rows(x2).Cells(3).Value, dgv_pembelian.Rows(x2).Cells(4).Value)
                x2 = x2 + 1
            Loop Until x2 = r2
        End If

        Dim tbpenjualan As New DataTable
        With tbpenjualan
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
        End With
        Dim x3 As Integer = 0
        Dim r3 As Integer
        r3 = dgv_penjualan_grey.RowCount
        If r3 > 0 Then
            Do
                tbpenjualan.Rows.Add(dgv_penjualan_grey.Rows(x3).Cells(0).FormattedValue, dgv_penjualan_grey.Rows(x3).Cells(1).Value, _
                                     dgv_penjualan_grey.Rows(x3).Cells(2).Value, dgv_penjualan_grey.Rows(x3).Cells(3).Value, dgv_penjualan_grey.Rows(x3).Cells(4).Value)
                x3 = x3 + 1
            Loop Until x3 = r3
        End If

        Dim tbpoproses As New DataTable
        With tbpoproses
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
        End With
        Dim x4 As Integer = 0
        Dim r4 As Integer
        r4 = dgv_po_proses.RowCount
        If r4 > 0 Then
            Do
                tbpoproses.Rows.Add(dgv_po_proses.Rows(x4).Cells(0).FormattedValue, dgv_po_proses.Rows(x4).Cells(1).Value, _
                                     dgv_po_proses.Rows(x4).Cells(2).Value, dgv_po_proses.Rows(x4).Cells(3).Value, dgv_po_proses.Rows(x4).Cells(4).Value, _
                                     dgv_po_proses.Rows(x4).Cells(5).Value, dgv_po_proses.Rows(x4).Cells(6).Value)
                x4 = x4 + 1
            Loop Until x4 = r4
        End If

        Dim tbsjproses As New DataTable
        With tbsjproses
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
        End With
        Dim x5 As Integer = 0
        Dim r5 As Integer
        r5 = dgv_sj_proses.RowCount
        If r5 > 0 Then
            Do
                tbsjproses.Rows.Add(dgv_sj_proses.Rows(x5).Cells(0).FormattedValue, dgv_sj_proses.Rows(x5).Cells(1).Value, _
                                     dgv_sj_proses.Rows(x5).Cells(2).Value, dgv_sj_proses.Rows(x5).Cells(3).Value, dgv_sj_proses.Rows(x5).Cells(4).Value, _
                                     dgv_sj_proses.Rows(x5).Cells(5).Value, dgv_sj_proses.Rows(x5).Cells(6).Value, _
                                     dgv_sj_proses.Rows(x5).Cells(7).Value, dgv_sj_proses.Rows(x5).Cells(8).Value, dgv_sj_proses.Rows(x5).Cells(9).Value)
                x5 = x5 + 1
            Loop Until x5 = r5
        End If

        Dim tbpopacking As New DataTable
        With tbpopacking
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
        End With
        Dim x6 As Integer = 0
        Dim r6 As Integer
        r6 = dgv_po_packing.RowCount
        If r6 > 0 Then
            Do
                tbpopacking.Rows.Add(dgv_po_packing.Rows(x6).Cells(0).FormattedValue, dgv_po_packing.Rows(x6).Cells(1).Value, _
                                     dgv_po_packing.Rows(x6).Cells(2).Value, dgv_po_packing.Rows(x6).Cells(3).Value, dgv_po_packing.Rows(x6).Cells(4).Value, _
                                     dgv_po_packing.Rows(x6).Cells(5).Value, dgv_po_packing.Rows(x6).Cells(6).Value, _
                                     dgv_po_packing.Rows(x6).Cells(7).Value, dgv_po_packing.Rows(x6).Cells(8).Value, dgv_po_packing.Rows(x6).Cells(9).Value)
                x6 = x6 + 1
            Loop Until x6 = r6
        End If

        Dim tbsjpacking As New DataTable
        With tbsjpacking
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
        End With
        Dim x7 As Integer = 0
        Dim r7 As Integer
        r7 = dgv_sj_packing.RowCount
        If r7 > 0 Then
            Do
                tbsjpacking.Rows.Add(dgv_sj_packing.Rows(x7).Cells(0).FormattedValue, dgv_sj_packing.Rows(x7).Cells(1).Value, _
                                     dgv_sj_packing.Rows(x7).Cells(2).Value, dgv_sj_packing.Rows(x7).Cells(3).Value, dgv_sj_packing.Rows(x7).Cells(4).Value, _
                                     dgv_sj_packing.Rows(x7).Cells(5).Value, dgv_sj_packing.Rows(x7).Cells(6).Value, _
                                     dgv_sj_packing.Rows(x7).Cells(7).Value, dgv_sj_packing.Rows(x7).Cells(8).Value, dgv_sj_packing.Rows(x7).Cells(9).Value)
                x7 = x7 + 1
            Loop Until x7 = r7
        End If

        Dim tbsjpenjualan As New DataTable
        With tbsjpenjualan
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
        End With
        Dim x8 As Integer = 0
        Dim r8 As Integer
        r8 = dgv_sj_penjualan.RowCount
        If r8 > 0 Then
            Do
                tbsjpenjualan.Rows.Add(dgv_sj_penjualan.Rows(x8).Cells(0).FormattedValue, dgv_sj_penjualan.Rows(x8).Cells(1).Value, _
                                     dgv_sj_penjualan.Rows(x8).Cells(2).Value, dgv_sj_penjualan.Rows(x8).Cells(3).Value, dgv_sj_penjualan.Rows(x8).Cells(4).Value, _
                                     dgv_sj_penjualan.Rows(x8).Cells(5).Value, dgv_sj_penjualan.Rows(x8).Cells(6).Value, _
                                     dgv_sj_penjualan.Rows(x8).Cells(7).Value)
                x8 = x8 + 1
            Loop Until x8 = r8
        End If

        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(1).Value = tbkontrak
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(2).Value = tbpembelian
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(3).Value = tbpenjualan
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(4).Value = tbpoproses
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(5).Value = tbsjproses
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(6).Value = tbpopacking
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(7).Value = tbsjpacking
        form_report_tracking_id_grey.ReportViewer1.LocalReport.DataSources.Item(8).Value = tbsjpenjualan
        form_report_tracking_id_grey.ShowDialog()
        form_report_tracking_id_grey.Dispose()
    End Sub

    Private Sub btn_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_print.Click
        Call reporttrackingidgrey()
    End Sub
    
    Private Sub txt_id_grey_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_id_grey.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        If btn_tracking.Text = "TRACKING" Then
            If e.KeyChar = Chr(13) Then
                btn_tracking.PerformClick()
            End If
        End If
    End Sub
End Class