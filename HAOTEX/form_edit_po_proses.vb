﻿Imports MySql.Data.MySqlClient

Public Class form_edit_po_proses

    Private Sub txt_qty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty.Text = String.Empty Then
                        txt_qty.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty.Select(txt_qty.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty.Text = String.Empty Then
                        txt_qty.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty.Select(txt_qty.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty.TextChanged
        If txt_qty.Text <> String.Empty Then
            Dim temp As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty.Select(txt_qty.Text.Length, 0)
            ElseIf txt_qty.Text = "-"c Then

            Else
                txt_qty.Text = CDec(temp).ToString("N0")
                txt_qty.Select(txt_qty.Text.Length, 0)
            End If
        End If
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty As String = txt_sisa_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        qty = qty.Replace(",", ".")
        sisa_qty = sisa_qty.Replace(",", ".")
        If Val(qty) > Val(sisa_qty) Then
            txt_qty.Text = txt_sisa_qty.Text
        End If
    End Sub
    Private Sub txt_sisa_qty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_qty.TextChanged
        If txt_sisa_qty.Text <> String.Empty Then
            Dim temp As String = txt_sisa_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_qty.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_qty.Select(txt_sisa_qty.Text.Length, 0)
            ElseIf txt_sisa_qty.Text = "-"c Then

            Else
                txt_sisa_qty.Text = CDec(temp).ToString("N0")
                txt_sisa_qty.Select(txt_sisa_qty.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_warna_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna.GotFocus, txt_resep.GotFocus
        form_input_warna_resep.MdiParent = form_menu_utama
        form_input_warna_resep.Show()
        form_input_warna_resep.TxtForm.Text = "form_edit_po_proses"
        form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain.Text
        form_input_warna_resep.Focus()
    End Sub
    Private Sub cb_satuan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan.TextChanged
        Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim stok As String = txt_sisa_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q, s, h As Double
        If cb_satuan.Text = "Yard" Then
            q = Math.Round(Val(qty.Replace(",", ".")) * 1.0936, 0)
            s = Math.Round(Val(stok.Replace(",", ".")) * 1.0936, 0)
            h = Math.Round(Val(txt_harga.Text.Replace(",", ".")) / 1.0936, 0)
        ElseIf cb_satuan.Text = "Meter" Then
            q = Math.Round(Val(qty.Replace(",", ".")) * 0.9144, 0)
            s = Math.Round(Val(stok.Replace(",", ".")) * 0.9144, 0)
            h = Math.Round(Val(txt_harga.Text.Replace(",", ".")) / 0.9144, 0)
        End If
        txt_qty.Text = q
        txt_sisa_qty.Text = s
        txt_harga.Text = h
    End Sub
    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Me.Close()
    End Sub
    Private Sub txt_tarik_lebar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill.Focus()
        If txt_tarik_lebar.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub

    Private Sub btn_simpan_tutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_tutup.Click
        Try
            If Val(txt_qty.Text) = 0 Then
                MsgBox("QTY Belum Diinput")
                txt_qty.Focus()
            ElseIf txt_qty.Text = "" Then
                MsgBox("QTY QTY Belum Diinput")
                txt_qty.Focus()
            ElseIf txt_tarik_lebar.Text = "" Then
                MsgBox("Tarik Lebar tidak boleh Kosong")
                txt_tarik_lebar.Focus()
            Else
                If Label1.Text = "Ubah PO Proses" Then
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses.Text & "'"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            cmdy.ExecuteNonQuery()
                        End Using
                    End Using
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses.Text & "'"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            cmdy.ExecuteNonQuery()
                        End Using
                    End Using
                    Dim qty As String = txt_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim stok As String = txt_sisa_qty.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim q, s As Double
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli.Text & "'"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            With cmdy
                                .Parameters.Clear()
                                If txt_asal_satuan.Text = cb_satuan.Text Then
                                    .Parameters.AddWithValue("@1", Math.Round(Val(stok.Replace(",", ".")) - Val(qty.Replace(",", ".")), 2))
                                ElseIf txt_asal_satuan.Text = "Meter" And cb_satuan.Text = "Yard" Then
                                    q = Math.Round(Val(stok.Replace(",", ".")) - Val(qty.Replace(",", ".")), 2)
                                    s = Math.Round(q * 0.9144, 1)
                                    .Parameters.AddWithValue("@1", s)
                                ElseIf txt_asal_satuan.Text = "Yard" And cb_satuan.Text = "Meter" Then
                                    q = Math.Round(Val(stok.Replace(",", ".")) - Val(qty.Replace(",", ".")), 2)
                                    s = Math.Round(q * 1.0936, 1)
                                    .Parameters.AddWithValue("@1", s)
                                End If
                                .ExecuteNonQuery()
                            End With
                        End Using
                    End Using
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            With cmdy
                                dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                                .Parameters.Clear()
                                .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                                .Parameters.AddWithValue("@1", txt_gudang.Text)
                                .Parameters.AddWithValue("@2", txt_no_po.Text)
                                .Parameters.AddWithValue("@3", txt_jenis_kain.Text)
                                .Parameters.AddWithValue("@4", txt_surat_jalan.Text)
                                .Parameters.AddWithValue("@5", txt_keterangan.Text)
                                .Parameters.AddWithValue("@6", qty.Replace(",", "."))
                                .Parameters.AddWithValue("@7", txt_warna.Text)
                                .Parameters.AddWithValue("@8", txt_resep.Text)
                                .Parameters.AddWithValue("@9", txt_tarik_lebar.Text)
                                .Parameters.AddWithValue("@10", txt_hand_fill.Text)
                                .Parameters.AddWithValue("@11", txt_note.Text)
                                .Parameters.AddWithValue("@12", txt_id_grey.Text)
                                .Parameters.AddWithValue("@13", txt_id_beli.Text)
                                .Parameters.AddWithValue("@14", txt_id_po_proses.Text)
                                .Parameters.AddWithValue("@15", cb_satuan.Text)
                                .Parameters.AddWithValue("@16", "")
                                .Parameters.AddWithValue("@17", txt_harga.Text.Replace(",", "."))
                                .Parameters.AddWithValue("@18", "")
                                .Parameters.AddWithValue("@19", "")
                                .Parameters.AddWithValue("@20", "")
                                .Parameters.AddWithValue("@21", 0)
                                .Parameters.AddWithValue("@22", 0)
                                .ExecuteNonQuery()
                            End With
                            dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                        End Using
                    End Using
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            With cmdy
                                dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                                .Parameters.Clear()
                                .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                                .Parameters.AddWithValue("@1", txt_id_grey.Text)
                                .Parameters.AddWithValue("@2", txt_id_beli.Text)
                                .Parameters.AddWithValue("@3", txt_id_po_proses.Text)
                                .Parameters.AddWithValue("@4", txt_no_po.Text)
                                .Parameters.AddWithValue("@5", txt_gudang.Text)
                                .Parameters.AddWithValue("@6", txt_jenis_kain.Text)
                                .Parameters.AddWithValue("@7", txt_warna.Text)
                                .Parameters.AddWithValue("@8", txt_resep.Text)
                                .Parameters.AddWithValue("@9", qty.Replace(",", "."))
                                .Parameters.AddWithValue("@10", cb_satuan.Text)
                                .Parameters.AddWithValue("@11", txt_harga.Text.Replace(",", "."))
                                .Parameters.AddWithValue("@12", "")
                                .Parameters.AddWithValue("@13", "")
                                .Parameters.AddWithValue("@14", 0)
                                .ExecuteNonQuery()
                            End With
                            dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                        End Using
                    End Using
                    form_po_proses.Focus()
                    form_po_proses.ts_perbarui.PerformClick()
                    MsgBox("PO PROSES Berhasil DIUBAH")
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class