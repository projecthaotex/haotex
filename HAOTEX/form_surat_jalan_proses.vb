﻿Imports MySql.Data.MySqlClient

Public Class form_surat_jalan_proses

    Private Sub form_surat_jalan_proses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call awal()
        dtp_hari_ini.Text = Today
    End Sub
    Private Sub awal()
        Call isidtpawal()
        Call isidgv()
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(-3)
        dtp_awal.Text = tanggal
        dtp_akhir.Text = Today
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Gudang"
        dgv1.Columns(2).HeaderText = "SJ Proses"
        dgv1.Columns(3).HeaderText = "No PO"
        dgv1.Columns(4).HeaderText = "Jenis Kain"
        dgv1.Columns(5).HeaderText = "Warna"
        dgv1.Columns(6).HeaderText = "Resep"
        dgv1.Columns(7).HeaderText = "Partai"
        dgv1.Columns(8).HeaderText = "Gulung"
        dgv1.Columns(9).HeaderText = "QTY"
        dgv1.Columns(10).HeaderText = "Satuan"
        dgv1.Columns(11).HeaderText = "KG"
        dgv1.Columns(12).HeaderText = "Gramasi"
        dgv1.Columns(13).HeaderText = "Harga"
        dgv1.Columns(14).HeaderText = "Total_Harga"
        dgv1.Columns(15).HeaderText = "Gudang Packing"
        dgv1.Columns(16).HeaderText = "Keterangan"
        dgv1.Columns(17).HeaderText = "Status"
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(10).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(11).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(12).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(13).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(14).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 120
        dgv1.Columns(2).Width = 80
        dgv1.Columns(3).Width = 80
        dgv1.Columns(4).Width = 120
        dgv1.Columns(5).Width = 100
        dgv1.Columns(6).Width = 100
        dgv1.Columns(7).Width = 80
        dgv1.Columns(8).Width = 70
        dgv1.Columns(9).Width = 100
        dgv1.Columns(10).Width = 70
        dgv1.Columns(11).Width = 80
        dgv1.Columns(12).Width = 70
        dgv1.Columns(13).Width = 100
        dgv1.Columns(14).Width = 120
        dgv1.Columns(15).Width = 150
        dgv1.Columns(16).Width = 150
        dgv1.Columns(17).Width = 80
        dgv1.Columns(9).DefaultCellStyle.Format = "N"
        dgv1.Columns(11).DefaultCellStyle.Format = "N"
        dgv1.Columns(12).DefaultCellStyle.Format = "N"
        dgv1.Columns(13).DefaultCellStyle.Format = "C"
        dgv1.Columns(14).DefaultCellStyle.Format = "C"
        dgv1.Columns(18).Visible = False
        dgv1.Columns(19).Visible = False
        dgv1.Columns(20).Visible = False
        dgv1.Columns(21).Visible = False
    End Sub
    Private Sub dgv1_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgv1.CellFormatting
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(17).Value = "Closed" Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Red
            End If
        Next
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbsuratjalanproses")
                            dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_awal.Text = dtp_akhir.Text
        End If
    End Sub
    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
    End Sub

    Private Sub txt_jumlah_total_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_qty_yard.TextChanged
        If txt_total_qty_yard.Text <> String.Empty Then
            Dim temp As String = txt_total_qty_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_qty_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_qty_yard.Select(txt_total_qty_yard.Text.Length, 0)
            ElseIf txt_total_qty_yard.Text = "-"c Then

            Else
                txt_total_qty_yard.Text = CDec(temp).ToString("N0")
                txt_total_qty_yard.Select(txt_total_qty_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_belum_po_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_belum_po_yard.TextChanged
        If txt_belum_po_yard.Text <> String.Empty Then
            Dim temp As String = txt_belum_po_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_belum_po_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_belum_po_yard.Select(txt_belum_po_yard.Text.Length, 0)
            ElseIf txt_belum_po_yard.Text = "-"c Then

            Else
                txt_belum_po_yard.Text = CDec(temp).ToString("N0")
                txt_belum_po_yard.Select(txt_belum_po_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sudah_po_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_keluar_po_yard.TextChanged
        If txt_keluar_po_yard.Text <> String.Empty Then
            Dim temp As String = txt_keluar_po_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_keluar_po_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_keluar_po_yard.Select(txt_keluar_po_yard.Text.Length, 0)
            ElseIf txt_keluar_po_yard.Text = "-"c Then

            Else
                txt_keluar_po_yard.Text = CDec(temp).ToString("N0")
                txt_keluar_po_yard.Select(txt_keluar_po_yard.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub hitungjumlah()
        Dim jumlahtotal, belumsj, keluarsj, jumlahtotalyard, belumsjyard, keluarsjyard As Double
        jumlahtotal = 0
        belumsj = 0
        keluarsj = 0
        jumlahtotalyard = 0
        belumsjyard = 0
        keluarsjyard = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(10).Value = "Meter" Then
                jumlahtotal = jumlahtotal + Val(dgv1.Rows(i).Cells(9).Value)
            ElseIf dgv1.Rows(i).Cells(10).Value = "Yard" Then
                jumlahtotalyard = jumlahtotalyard + Val(dgv1.Rows(i).Cells(9).Value)
            End If

            If dgv1.Rows(i).Cells(17).Value = "Closed" Then
                If dgv1.Rows(i).Cells(10).Value = "Meter" Then
                    keluarsj = keluarsj + Val(dgv1.Rows(i).Cells(9).Value)
                ElseIf dgv1.Rows(i).Cells(10).Value = "Yard" Then
                    keluarsjyard = keluarsjyard + Val(dgv1.Rows(i).Cells(9).Value)
                End If
            Else
                If dgv1.Rows(i).Cells(10).Value = "Meter" Then
                    belumsj = belumsj + Val(dgv1.Rows(i).Cells(9).Value)
                ElseIf dgv1.Rows(i).Cells(10).Value = "Yard" Then
                    belumsjyard = belumsjyard + Val(dgv1.Rows(i).Cells(9).Value)
                End If
            End If
        Next
        txt_total_qty.Text = jumlahtotal
        txt_belum_po.Text = belumsj
        txt_keluar_po.Text = keluarsj
        txt_total_qty_yard.Text = jumlahtotalyard
        txt_belum_po_yard.Text = belumsjyard
        txt_keluar_po_yard.Text = keluarsjyard
    End Sub

    Private Sub reportsuratjalanproses()
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
        End With
        tbheaderfooter.Rows.Add(dtp_awal.Text, dtp_akhir.Text, dtp_hari_ini.Text, txt_total_qty_yard.Text, _
                                txt_belum_po_yard.Text, txt_keluar_po_yard.Text)

        Dim dtreportpoproses As New DataTable
        With dtreportpoproses
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
            .Columns.Add("DataColumn12")
            .Columns.Add("DataColumn13")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            dtreportpoproses.Rows.Add(row.Cells(18).Value, row.Cells(0).FormattedValue, row.Cells(1).Value, _
                                      row.Cells(2).Value, row.Cells(3).Value, row.Cells(6).Value, row.Cells(7).Value, _
                                      row.Cells(8).Value, row.Cells(9).Value, row.Cells(10).Value, _
                                      row.Cells(11).FormattedValue, row.Cells(16).Value, row.Cells(17).Value)
        Next
        form_report_surat_jalan_proses.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_surat_jalan_proses.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportpoproses
        form_report_surat_jalan_proses.ShowDialog()
        form_report_surat_jalan_proses.Dispose()
    End Sub
    Private Sub ts_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportsuratjalanproses()
        End If
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_surat_jalan_proses.MdiParent = form_menu_utama
        form_input_surat_jalan_proses.Show()
        form_input_surat_jalan_proses.Focus()
        form_input_surat_jalan_proses.txt_surat_jalan_proses.Focus()
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        Call awal()
        txt_cari_sj_proses.Text = ""
        txt_cari_no_partai.Text = ""
        txt_cari_gudang_proses.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_gudang_packing.Text = ""
        Panel1.Enabled = True
        btn_cari.Text = "CARI"
        btn_cari.Image = HAOTEX.My.Resources.search
        dgv1.Focus()
    End Sub

    Private Sub isidgv_surat_jalan_proses()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE SJ_Proses LIKE '%" & txt_cari_sj_proses.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_no_partai()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Partai LIKE '%" & txt_cari_no_partai.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Partai"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_gudang_proses()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Gudang LIKE '%" & txt_cari_gudang_proses.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_gudang_proses_jenis_kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Gudang LIKE '%" & txt_cari_gudang_proses.Text & "%' AND Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_gudang_proses_gudang_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Gudang LIKE '%" & txt_cari_gudang_proses.Text & "%' AND Gudang_Packing LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_gudang_proses_jenis_kain_gudang_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Gudang LIKE '%" & txt_cari_gudang_proses.Text & "%' AND Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Gudang_Packing LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_jenis_kain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_gudang_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Gudang_Packing LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub isidgv_jenis_kain_gudang_packing()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Status,Jatuh_Tempo,Id_Grey,Id_Po_Proses,Id_SJ_Proses FROM tbsuratjalanproses WHERE Jenis_Kain LIKE '%" & txt_cari_jenis_kain.Text & "%' AND Gudang_Packing LIKE '%" & txt_cari_gudang_packing.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY SJ_Proses"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbsuratjalanproses")
                        dgv1.DataSource = dsx.Tables("tbsuratjalanproses")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Call hitungjumlah()
    End Sub
    Private Sub btn_cari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_cari.Click
        If btn_cari.Text = "CARI" Then
            btn_cari.Text = "RESET"
            btn_cari.Image = HAOTEX.My.Resources.refresh
            Panel1.Enabled = False
            If txt_cari_sj_proses.Text = "" And txt_cari_no_partai.Text = "" And txt_cari_gudang_proses.Text = "" _
                And txt_cari_jenis_kain.Text = "" And txt_cari_gudang_packing.Text = "" Then
                Call isidgv()
            ElseIf Not txt_cari_sj_proses.Text = "" Then
                Call isidgv_surat_jalan_proses()
            ElseIf Not txt_cari_no_partai.Text = "" Then
                Call isidgv_no_partai()
            ElseIf Not txt_cari_gudang_proses.Text = "" And txt_cari_jenis_kain.Text = "" And txt_cari_gudang_packing.Text = "" Then
                Call isidgv_gudang_proses()
            ElseIf Not txt_cari_gudang_proses.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang_packing.Text = "" Then
                Call isidgv_gudang_proses_jenis_kain()
            ElseIf Not txt_cari_gudang_proses.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang_packing.Text = "" Then
                Call isidgv_gudang_proses_gudang_packing()
            ElseIf Not txt_cari_gudang_proses.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang_packing.Text = "" Then
                Call isidgv_gudang_proses_jenis_kain_gudang_packing()
            ElseIf txt_cari_gudang_proses.Text = "" And Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang_packing.Text = "" Then
                Call isidgv_jenis_kain()
            ElseIf txt_cari_gudang_proses.Text = "" And txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang_packing.Text = "" Then
                Call isidgv_gudang_packing()
            ElseIf txt_cari_gudang_proses.Text = "" And Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang_packing.Text = "" Then
                Call isidgv_jenis_kain_gudang_packing()
            End If
        Else
            ts_perbarui.PerformClick()
        End If
    End Sub

    Private Sub txt_cari_sj_proses_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_sj_proses.GotFocus
        txt_cari_no_partai.Text = ""
        txt_cari_gudang_proses.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_gudang_packing.Text = ""
    End Sub
    Private Sub txt_cari_no_partai_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_no_partai.GotFocus
        txt_cari_sj_proses.Text = ""
        txt_cari_gudang_proses.Text = ""
        txt_cari_jenis_kain.Text = ""
        txt_cari_gudang_packing.Text = ""
    End Sub
    Private Sub txt_cari_gudang_proses_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang_proses.GotFocus
        txt_cari_sj_proses.Text = ""
        txt_cari_no_partai.Text = ""
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        txt_cari_sj_proses.Text = ""
        txt_cari_no_partai.Text = ""
    End Sub
    Private Sub txt_cari_gudang_packing_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang_packing.GotFocus
        txt_cari_sj_proses.Text = ""
        txt_cari_no_partai.Text = ""
    End Sub

    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT * FROM tbpopacking WHERE Id_Grey='" & dgv1.CurrentRow.Cells(18).Value.ToString & "' AND Asal_SJ='" & dgv1.CurrentRow.Cells(2).Value.ToString & "' AND Baris_SJ='" & dgv1.CurrentRow.Cells(3).Value.ToString & "' AND Gudang='" & dgv1.CurrentRow.Cells(16).Value.ToString & "' AND Jenis_Kain='" & dgv1.CurrentRow.Cells(6).Value.ToString & "' AND Warna='" & dgv1.CurrentRow.Cells(7).Value.ToString & "' AND Resep='" & dgv1.CurrentRow.Cells(8).Value.ToString & "' AND Partai='" & dgv1.CurrentRow.Cells(9).Value.ToString & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using drx As MySqlDataReader = cmdx.ExecuteReader
                        drx.Read()
                        If drx.HasRows Then
                            MsgBox("Surat Jalan Yang Dipilih Sudah Keluar PO Packing, Tidak Bisa DIUBAH")
                        Else
                            form_edit_surat_jalan_proses.MdiParent = form_menu_utama
                            form_edit_surat_jalan_proses.Show()
                            form_edit_surat_jalan_proses.Focus()
                            form_edit_surat_jalan_proses.Label1.Text = "Ubah Surat Jalan Proses"
                            Dim i As Integer
                            i = dgv1.CurrentRow.Index
                            With dgv1.Rows.Item(i)
                                form_edit_surat_jalan_proses.txt_id_grey.Text = .Cells(18).Value.ToString
                                form_edit_surat_jalan_proses.dtp_tanggal.Text = .Cells(0).Value.ToString
                                form_edit_surat_jalan_proses.dtp_tanggal_1.Text = .Cells(0).Value.ToString
                                form_edit_surat_jalan_proses.dtp_jatuh_tempo.Text = .Cells(20).Value.ToString
                                form_edit_surat_jalan_proses.dtp_jatuh_tempo_1.Text = .Cells(20).Value.ToString
                                form_edit_surat_jalan_proses.txt_gudang.Text = .Cells(1).Value.ToString
                                form_edit_surat_jalan_proses.txt_surat_jalan.Text = .Cells(2).Value.ToString
                                form_edit_surat_jalan_proses.txt_baris_sj.Text = .Cells(3).Value.ToString
                                form_edit_surat_jalan_proses.txt_no_po.Text = .Cells(4).Value.ToString
                                form_edit_surat_jalan_proses.txt_baris_po.Text = .Cells(5).Value.ToString
                                form_edit_surat_jalan_proses.txt_jenis_kain.Text = .Cells(6).Value.ToString
                                form_edit_surat_jalan_proses.txt_warna.Text = .Cells(7).Value.ToString
                                form_edit_surat_jalan_proses.txt_resep.Text = .Cells(8).Value.ToString
                                form_edit_surat_jalan_proses.txt_partai.Text = .Cells(9).Value.ToString
                                form_edit_surat_jalan_proses.txt_gulung.Text = .Cells(10).Value.ToString
                                form_edit_surat_jalan_proses.txt_gudang_packing.Text = .Cells(16).Value.ToString
                                form_edit_surat_jalan_proses.txt_gudang_awal.Text = .Cells(16).Value.ToString
                                form_edit_surat_jalan_proses.txt_keterangan.Text = .Cells(17).Value.ToString
                                Using conz As New MySqlConnection(sLocalConn)
                                    conz.Open()
                                    Dim sqlz As String = "SELECT Asal_SJ,QTY,Harga FROM tbpoproses WHERE Id_Grey='" & .Cells(18).Value.ToString & "' AND Gudang='" & .Cells(1).Value.ToString & "' AND No_PO='" & .Cells(4).Value.ToString & "' AND No_Urut='" & .Cells(5).Value.ToString & "' AND  Jenis_Kain='" & .Cells(6).Value.ToString & "' AND Warna='" & .Cells(7).Value.ToString & "' AND  Resep='" & .Cells(8).Value.ToString & "'"
                                    Using cmdz As New MySqlCommand(sqlz, conz)
                                        Using drz As MySqlDataReader = cmdz.ExecuteReader
                                            drz.Read()
                                            If drz.HasRows Then
                                                form_edit_surat_jalan_proses.txt_sj_pembelian.Text = drz.Item(0).ToString
                                                form_edit_surat_jalan_proses.txt_qty_po_proses.Text = drz.Item(1).ToString
                                                form_edit_surat_jalan_proses.txt_harga_wip_proses.Text = drz.Item(2).ToString
                                            End If
                                        End Using
                                    End Using
                                End Using
                                Using conz As New MySqlConnection(sLocalConn)
                                    conz.Open()
                                    Dim sqlz As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Grey='" & .Cells(18).Value.ToString & "' AND Gudang='" & .Cells(1).Value.ToString & "' AND Jenis_Kain='" & .Cells(6).Value.ToString & "' AND SJ='" & form_edit_surat_jalan_proses.txt_sj_pembelian.Text & "'"
                                    Using cmdz As New MySqlCommand(sqlz, conz)
                                        Using drz As MySqlDataReader = cmdz.ExecuteReader
                                            drz.Read()
                                            If drz.HasRows Then
                                                form_edit_surat_jalan_proses.txt_sisa_meter.Text = Val(drz.Item(0).ToString.Replace(",", ".")) + Val(.Cells(11).Value.ToString.Replace(",", "."))
                                            End If
                                        End Using
                                    End Using
                                End Using
                                form_edit_surat_jalan_proses.txt_meter.Text = .Cells(11).Value.ToString
                                form_edit_surat_jalan_proses.txt_kg.Text = .Cells(12).Value.ToString
                                form_edit_surat_jalan_proses.txt_gramasi.Text = .Cells(13).Value.ToString
                                form_edit_surat_jalan_proses.txt_harga.Text = .Cells(14).Value.ToString
                                form_edit_surat_jalan_proses.txt_total_harga.Text = .Cells(15).Value.ToString
                                form_edit_surat_jalan_proses.txt_total_harga_1.Text = .Cells(15).Value.ToString
                                form_edit_surat_jalan_proses.Label1.Focus()
                            End With
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show("Tidak Terdapat Data Untuk Diubah")
        End Try
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT * FROM tbpopacking WHERE Id_Grey='" & dgv1.CurrentRow.Cells(18).Value.ToString & "' AND Asal_SJ='" & dgv1.CurrentRow.Cells(2).Value.ToString & "' AND Baris_SJ='" & dgv1.CurrentRow.Cells(3).Value.ToString & "' AND Gudang='" & dgv1.CurrentRow.Cells(16).Value.ToString & "' AND Jenis_Kain='" & dgv1.CurrentRow.Cells(6).Value.ToString & "' AND Warna='" & dgv1.CurrentRow.Cells(7).Value.ToString & "' AND Resep='" & dgv1.CurrentRow.Cells(8).Value.ToString & "' AND Partai='" & dgv1.CurrentRow.Cells(9).Value.ToString & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using drx As MySqlDataReader = cmdx.ExecuteReader
                        drx.Read()
                        If drx.HasRows Then
                            MsgBox("Surat Jalan Yang Dipilih Sudah Keluar PO Packing, Tidak Bisa DIHAPUS")
                        Else
                            form_edit_surat_jalan_proses.MdiParent = form_menu_utama
                            form_edit_surat_jalan_proses.Show()
                            form_edit_surat_jalan_proses.Focus()
                            form_edit_surat_jalan_proses.Label1.Text = "Hapus Surat Jalan Proses"
                            form_edit_surat_jalan_proses.btn_simpan_tutup.Visible = False
                            form_edit_surat_jalan_proses.btn_batal.Visible = False
                            Dim i As Integer
                            i = dgv1.CurrentRow.Index
                            With dgv1.Rows.Item(i)
                                form_edit_surat_jalan_proses.txt_id_grey.Text = .Cells(18).Value.ToString
                                form_edit_surat_jalan_proses.dtp_tanggal.Text = .Cells(0).Value.ToString
                                form_edit_surat_jalan_proses.dtp_tanggal_1.Text = .Cells(0).Value.ToString
                                form_edit_surat_jalan_proses.dtp_jatuh_tempo.Text = .Cells(20).Value.ToString
                                form_edit_surat_jalan_proses.dtp_jatuh_tempo_1.Text = .Cells(20).Value.ToString
                                form_edit_surat_jalan_proses.txt_gudang.Text = .Cells(1).Value.ToString
                                form_edit_surat_jalan_proses.txt_surat_jalan.Text = .Cells(2).Value.ToString
                                form_edit_surat_jalan_proses.txt_baris_sj.Text = .Cells(3).Value.ToString
                                form_edit_surat_jalan_proses.txt_no_po.Text = .Cells(4).Value.ToString
                                form_edit_surat_jalan_proses.txt_baris_po.Text = .Cells(5).Value.ToString
                                form_edit_surat_jalan_proses.txt_jenis_kain.Text = .Cells(6).Value.ToString
                                form_edit_surat_jalan_proses.txt_warna.Text = .Cells(7).Value.ToString
                                form_edit_surat_jalan_proses.txt_resep.Text = .Cells(8).Value.ToString
                                form_edit_surat_jalan_proses.txt_partai.Text = .Cells(9).Value.ToString
                                form_edit_surat_jalan_proses.txt_gulung.Text = .Cells(10).Value.ToString
                                form_edit_surat_jalan_proses.txt_gudang_packing.Text = .Cells(16).Value.ToString
                                form_edit_surat_jalan_proses.txt_gudang_awal.Text = .Cells(16).Value.ToString
                                form_edit_surat_jalan_proses.txt_keterangan.Text = .Cells(17).Value.ToString
                                Using conz As New MySqlConnection(sLocalConn)
                                    conz.Open()
                                    Dim sqlz As String = "SELECT Asal_SJ,QTY,Harga FROM tbpoproses WHERE Id_Grey='" & .Cells(18).Value.ToString & "' AND Gudang='" & .Cells(1).Value.ToString & "' AND No_PO='" & .Cells(4).Value.ToString & "' AND No_Urut='" & .Cells(5).Value.ToString & "' AND  Jenis_Kain='" & .Cells(6).Value.ToString & "' AND Warna='" & .Cells(7).Value.ToString & "' AND  Resep='" & .Cells(8).Value.ToString & "'"
                                    Using cmdz As New MySqlCommand(sqlz, conz)
                                        Using drz As MySqlDataReader = cmdz.ExecuteReader
                                            drz.Read()
                                            If drz.HasRows Then
                                                form_edit_surat_jalan_proses.txt_sj_pembelian.Text = drz.Item(0).ToString
                                                form_edit_surat_jalan_proses.txt_qty_po_proses.Text = drz.Item(1).ToString
                                                form_edit_surat_jalan_proses.txt_harga_wip_proses.Text = drz.Item(2).ToString
                                            End If
                                        End Using
                                    End Using
                                End Using
                                Using conz As New MySqlConnection(sLocalConn)
                                    conz.Open()
                                    Dim sqlz As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Grey='" & .Cells(18).Value.ToString & "' AND Gudang='" & .Cells(1).Value.ToString & "' AND Jenis_Kain='" & .Cells(6).Value.ToString & "' AND SJ='" & form_edit_surat_jalan_proses.txt_sj_pembelian.Text & "'"
                                    Using cmdz As New MySqlCommand(sqlz, conz)
                                        Using drz As MySqlDataReader = cmdz.ExecuteReader
                                            drz.Read()
                                            If drz.HasRows Then
                                                form_edit_surat_jalan_proses.txt_sisa_meter.Text = Val(drz.Item(0).ToString.Replace(",", ".")) + Val(.Cells(11).Value.ToString.Replace(",", "."))
                                            End If
                                        End Using
                                    End Using
                                End Using
                                form_edit_surat_jalan_proses.txt_meter.Text = .Cells(11).Value.ToString
                                form_edit_surat_jalan_proses.txt_kg.Text = .Cells(12).Value.ToString
                                form_edit_surat_jalan_proses.txt_gramasi.Text = .Cells(13).Value.ToString
                                form_edit_surat_jalan_proses.txt_harga.Text = .Cells(14).Value.ToString
                                form_edit_surat_jalan_proses.txt_total_harga.Text = .Cells(15).Value.ToString
                                form_edit_surat_jalan_proses.txt_total_harga_1.Text = .Cells(15).Value.ToString
                                form_edit_surat_jalan_proses.Label1.Focus()
                            End With

                            If MsgBox("Yakin Surat Jalan Proses Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Surat Jalan Proses") = vbYes Then
                                Using cony As New MySqlConnection(sLocalConn)
                                    cony.Open()
                                    Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_Grey='" & form_edit_surat_jalan_proses.txt_id_grey.Text & "' AND No_PO='" & form_edit_surat_jalan_proses.txt_no_po.Text & "' AND Baris_PO='" & form_edit_surat_jalan_proses.txt_baris_po.Text & "' AND Gudang='" & form_edit_surat_jalan_proses.txt_gudang.Text & "' AND Jenis_Kain='" & form_edit_surat_jalan_proses.txt_jenis_kain.Text & "' AND SJ_Proses='" & form_edit_surat_jalan_proses.txt_surat_jalan.Text & "' AND Baris_SJ='" & form_edit_surat_jalan_proses.txt_baris_sj.Text & "' AND Warna='" & form_edit_surat_jalan_proses.txt_warna.Text & "' AND Resep='" & form_edit_surat_jalan_proses.txt_resep.Text & "' AND Partai='" & form_edit_surat_jalan_proses.txt_partai.Text & "'"
                                    Using cmdy As New MySqlCommand(sqly, cony)
                                        cmdy.ExecuteNonQuery()
                                    End Using
                                End Using
                                Using conz As New MySqlConnection(sLocalConn)
                                    conz.Open()
                                    Dim sqlz As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Grey='" & form_edit_surat_jalan_proses.txt_id_grey.Text & "' AND Gudang='" & form_edit_surat_jalan_proses.txt_gudang.Text & "' AND Jenis_Kain='" & form_edit_surat_jalan_proses.txt_jenis_kain.Text & "' AND SJ='" & form_edit_surat_jalan_proses.txt_sj_pembelian.Text & "'"
                                    Using cmdz As New MySqlCommand(sqlz, conz)
                                        Using drz As MySqlDataReader = cmdz.ExecuteReader
                                            drz.Read()
                                            If drz.HasRows Then
                                                Dim s As Double
                                                s = drz.Item(0)
                                                Using cony As New MySqlConnection(sLocalConn)
                                                    cony.Open()
                                                    Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Grey='" & form_edit_surat_jalan_proses.txt_id_grey.Text & "' AND Gudang='" & form_edit_surat_jalan_proses.txt_gudang.Text & "' AND Jenis_Kain='" & form_edit_surat_jalan_proses.txt_jenis_kain.Text & "' AND SJ='" & form_edit_surat_jalan_proses.txt_sj_pembelian.Text & "'"
                                                    Using cmdy As New MySqlCommand(sqly, cony)
                                                        With cmdy
                                                            .Parameters.Clear()
                                                            Dim meter As String = form_edit_surat_jalan_proses.txt_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                                                            .Parameters.AddWithValue("@1", (s + Val(meter.Replace(",", "."))))
                                                            .ExecuteNonQuery()
                                                        End With
                                                    End Using
                                                End Using
                                            End If
                                        End Using
                                    End Using
                                End Using
                                Using cony As New MySqlConnection(sLocalConn)
                                    cony.Open()
                                    Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Gudang='" & form_edit_surat_jalan_proses.txt_gudang.Text & "' AND No_PO='" & form_edit_surat_jalan_proses.txt_no_po.Text & "' AND Id_Grey='" & form_edit_surat_jalan_proses.txt_id_grey.Text & "' AND No_Urut='" & form_edit_surat_jalan_proses.txt_baris_po.Text & "' AND Jenis_Kain='" & form_edit_surat_jalan_proses.txt_jenis_kain.Text & "' AND Warna='" & form_edit_surat_jalan_proses.txt_warna.Text & "' AND Resep='" & form_edit_surat_jalan_proses.txt_resep.Text & "'"
                                    Using cmdy As New MySqlCommand(sqly, cony)
                                        With cmdy
                                            .Parameters.Clear()
                                            .Parameters.AddWithValue("@1", form_edit_surat_jalan_proses.txt_qty_po_proses.Text)
                                            .ExecuteNonQuery()
                                        End With
                                    End Using
                                End Using
                                Using cony As New MySqlConnection(sLocalConn)
                                    cony.Open()
                                    Dim sqly = "DELETE FROM tbstokexproses WHERE Id_Grey='" & form_edit_surat_jalan_proses.txt_id_grey.Text & "' AND SJ_Proses='" & form_edit_surat_jalan_proses.txt_surat_jalan.Text & "' AND No_Urut='" & form_edit_surat_jalan_proses.txt_baris_sj.Text & "' AND Gudang='" & form_edit_surat_jalan_proses.txt_gudang_packing.Text & "' AND Jenis_Kain='" & form_edit_surat_jalan_proses.txt_jenis_kain.Text & "' AND Warna='" & form_edit_surat_jalan_proses.txt_warna.Text & "' AND Resep='" & form_edit_surat_jalan_proses.txt_resep.Text & "' AND Partai='" & form_edit_surat_jalan_proses.txt_partai.Text & "'"
                                    Using cmdy As New MySqlCommand(sqly, cony)
                                        cmdy.ExecuteNonQuery()
                                    End Using
                                End Using
                                Using cona As New MySqlConnection(sLocalConn)
                                    cona.Open()
                                    Dim sqla As String = "SELECT Sisa_Hutang FROM tbhutang WHERE Nama='" & form_edit_surat_jalan_proses.txt_gudang.Text & "'"
                                    Using cmda As New MySqlCommand(sqla, cona)
                                        Using dra As MySqlDataReader = cmda.ExecuteReader
                                            dra.Read()
                                            If dra.HasRows Then
                                                Dim h As Double
                                                h = dra.Item(0)
                                                Using cony As New MySqlConnection(sLocalConn)
                                                    cony.Open()
                                                    Dim total_harga As String = form_edit_surat_jalan_proses.txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                                                    Dim sqly = "UPDATE tbhutang SET Sisa_Hutang=@1 WHERE Nama='" & form_edit_surat_jalan_proses.txt_gudang.Text & "'"
                                                    Using cmdy As New MySqlCommand(sqly, cony)
                                                        With cmdy
                                                            .Parameters.Clear()
                                                            .Parameters.AddWithValue("@1", (h - Val(total_harga.Replace(",", "."))))
                                                            .ExecuteNonQuery()
                                                        End With
                                                    End Using
                                                End Using
                                            End If
                                        End Using
                                    End Using
                                End Using
                                Using cony As New MySqlConnection(sLocalConn)
                                    cony.Open()
                                    form_edit_surat_jalan_proses.dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                                    Dim total_harga As String = form_edit_surat_jalan_proses.txt_total_harga.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                                    Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Grey='" & form_edit_surat_jalan_proses.txt_id_grey.Text & "' AND SJ='" & form_edit_surat_jalan_proses.txt_surat_jalan.Text & "' AND Tambah1='" & form_edit_surat_jalan_proses.txt_baris_sj.Text & "' AND Nama='" & form_edit_surat_jalan_proses.txt_gudang.Text & "' AND Jenis_Kain='" & form_edit_surat_jalan_proses.txt_jenis_kain.Text & "' AND Tanggal='" & form_edit_surat_jalan_proses.dtp_tanggal.Text & "' AND Jumlah='" & total_harga.Replace(",", ".") & "'"
                                    Using cmdy As New MySqlCommand(sqly, cony)
                                        cmdy.ExecuteNonQuery()
                                    End Using
                                    form_edit_surat_jalan_proses.dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                                End Using
                                Using cony As New MySqlConnection(sLocalConn)
                                    cony.Open()
                                    Dim sqly = "UPDATE tbpoproses SET Tambah1=@1 WHERE Id_Grey='" & form_edit_surat_jalan_proses.txt_id_grey.Text & "' AND Gudang='" & form_edit_surat_jalan_proses.txt_gudang.Text & "' AND Jenis_Kain='" & form_edit_surat_jalan_proses.txt_jenis_kain.Text & "' AND No_PO='" & form_edit_surat_jalan_proses.txt_no_po.Text & "' AND No_Urut='" & form_edit_surat_jalan_proses.txt_baris_po.Text & "' AND Warna='" & form_edit_surat_jalan_proses.txt_warna.Text & "' AND Resep='" & form_edit_surat_jalan_proses.txt_resep.Text & "'"
                                    Using cmdy As New MySqlCommand(sqly, cony)
                                        With cmdy
                                            .Parameters.Clear()
                                            .Parameters.AddWithValue("@1", "")
                                            .ExecuteNonQuery()
                                        End With
                                    End Using
                                End Using
                                ts_perbarui.PerformClick()
                                form_edit_surat_jalan_proses.Close()
                                MsgBox("Surat Jalan Proses berhasil di HAPUS")
                            Else
                                form_edit_surat_jalan_proses.Close()
                            End If
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show("Tidak Terdapat Data Untuk Dihapus")
        End Try
    End Sub
End Class