﻿Imports MySql.Data.MySqlClient

Public Class form_input_po_proses

    Private Sub form_input_po_proses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel_2.Visible = False
        Panel_3.Visible = False
        Panel_4.Visible = False
        Panel_5.Visible = False
        Panel_6.Visible = False
        Panel_7.Visible = False
        Panel_8.Visible = False
        Panel_9.Visible = False
        Panel_10.Visible = False
        btn_hapus_2.Visible = False
        btn_hapus_3.Visible = False
        btn_hapus_4.Visible = False
        btn_hapus_5.Visible = False
        btn_hapus_6.Visible = False
        btn_hapus_7.Visible = False
        btn_hapus_8.Visible = False
        btn_hapus_9.Visible = False
        btn_hapus_10.Visible = False
        btn_tambah_baris_2.Visible = False
        btn_tambah_baris_3.Visible = False
        btn_tambah_baris_4.Visible = False
        btn_tambah_baris_5.Visible = False
        btn_tambah_baris_6.Visible = False
        btn_tambah_baris_7.Visible = False
        btn_tambah_baris_8.Visible = False
        btn_tambah_baris_9.Visible = False
        btn_selesai_2.Visible = False
        btn_selesai_3.Visible = False
        btn_selesai_4.Visible = False
        btn_selesai_5.Visible = False
        btn_selesai_6.Visible = False
        btn_selesai_7.Visible = False
        btn_selesai_8.Visible = False
        btn_selesai_9.Visible = False
        btn_selesai_10.Visible = False
        btn_batal_2.Visible = False
        btn_batal_3.Visible = False
        btn_batal_4.Visible = False
        btn_batal_5.Visible = False
        btn_batal_6.Visible = False
        btn_batal_7.Visible = False
        btn_batal_8.Visible = False
        btn_batal_9.Visible = False
        btn_batal_10.Visible = False
    End Sub
    Private Sub txt_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang.GotFocus
        If txt_gudang.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_po_proses"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gudang.TextChanged
        txt_asal_sj_1.Text = ""
        txt_asal_sj_1.ReadOnly = False
        txt_jenis_kain_1.Text = ""
        txt_jenis_kain_1.ReadOnly = False
        txt_id_grey_1.Text = ""
        txt_harga_1.Text = ""
        txt_qty_asal_1.Text = ""
        txt_qty_1.Text = ""
        txt_warna_1.Text = ""
        txt_resep_1.Text = ""
        txt_hand_fill_1.Text = ""
        txt_tarik_lebar_1.Text = ""
        txt_asal_keterangan_1.Text = ""
        txt_note_1.Text = ""
    End Sub
    Private Sub txt_asal_sj_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_1.GotFocus, txt_jenis_kain_1.GotFocus
        If txt_gudang.Text = "" Then
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_po_proses"
            form_input_gudang.Focus()
        Else
            form_stok_proses_grey.MdiParent = form_menu_utama
            form_stok_proses_grey.Show()
            form_stok_proses_grey.btn_perbarui.Visible = False
            form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_1"
            form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
            form_stok_proses_grey.Focus()
        End If
    End Sub
    Private Sub txt_asal_sj_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_2.GotFocus, txt_jenis_kain_2.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_2"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_3.GotFocus, txt_jenis_kain_3.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_3"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_4.GotFocus, txt_jenis_kain_4.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_4"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_5.GotFocus, txt_jenis_kain_5.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_5"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_6.GotFocus, txt_jenis_kain_6.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_6"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_7.GotFocus, txt_jenis_kain_7.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_7"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_8.GotFocus, txt_jenis_kain_8.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_8"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_9.GotFocus, txt_jenis_kain_9.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_9"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub
    Private Sub txt_asal_sj_10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_asal_sj_10.GotFocus, txt_jenis_kain_10.GotFocus
        form_stok_proses_grey.MdiParent = form_menu_utama
        form_stok_proses_grey.Show()
        form_stok_proses_grey.btn_perbarui.Visible = False
        form_stok_proses_grey.txt_form.Text = "form_input_po_proses_baris_10"
        form_stok_proses_grey.txt_cari_gudang.Text = txt_gudang.Text
        form_stok_proses_grey.txt_cari_gudang.ReadOnly = True
        form_stok_proses_grey.Focus()
    End Sub

    Private Sub simpan_note_po_proses()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Note FROM tbnotepoproses WHERE No_PO='" & txt_no_po.Text & "' AND Tanggal ='" & dtp_awal.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbnotepoproses SET Note=@1 WHERE No_PO='" & txt_no_po.Text & "' AND Tanggal ='" & dtp_awal.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (txt_note_po.Text))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    Else
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "INSERT INTO tbnotepoproses (Tanggal,No_PO,Note,Tambah1,Tambah2,Tambah3) VALUES (@1,@2,@3,@4,@5,@6)"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    .Parameters.AddWithValue("@1", (dtp_awal.Text))
                                    .Parameters.AddWithValue("@2", (txt_no_po.Text))
                                    .Parameters.AddWithValue("@3", (txt_note_po.Text))
                                    .Parameters.AddWithValue("@4", (""))
                                    .Parameters.AddWithValue("@5", (""))
                                    .Parameters.AddWithValue("@6", (0))
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub hapus_note_po_proses()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbnotepoproses WHERE No_PO='" & txt_no_po.Text & "' AND Tanggal ='" & dtp_awal.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
    End Sub

    Private Sub update_tbstokprosesgrey_baris_1()
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_1 As Double
                        m_1 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_1.Text Then
                                        .Parameters.AddWithValue("@1", m_1 - Math.Round(Val(qty_1.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_1.Text = "Yard" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_1 - Math.Round(q_1, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_1.Text = "Meter" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_1 - Math.Round(q_1, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_2()
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_2 As Double
                        m_2 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_2.Text Then
                                        .Parameters.AddWithValue("@1", m_2 - Math.Round(Val(qty_2.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_2.Text = "Yard" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_2 - Math.Round(q_2, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_2.Text = "Meter" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_2 - Math.Round(q_2, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_3()
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_3 As Double
                        m_3 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_3.Text Then
                                        .Parameters.AddWithValue("@1", m_3 - Math.Round(Val(qty_3.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_3.Text = "Yard" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_3 - Math.Round(q_3, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_3.Text = "Meter" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_3 - Math.Round(q_3, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_4()
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_4 As Double
                        m_4 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_4.Text Then
                                        .Parameters.AddWithValue("@1", m_4 - Math.Round(Val(qty_4.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_4.Text = "Yard" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_4 - Math.Round(q_4, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_4.Text = "Meter" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_4 - Math.Round(q_4, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_5()
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_5 As Double
                        m_5 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_5.Text Then
                                        .Parameters.AddWithValue("@1", m_5 - Math.Round(Val(qty_5.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_5.Text = "Yard" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_5 - Math.Round(q_5, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_5.Text = "Meter" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_5 - Math.Round(q_5, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_6()
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_6 As Double
                        m_6 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_6.Text Then
                                        .Parameters.AddWithValue("@1", m_6 - Math.Round(Val(qty_6.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_6.Text = "Yard" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_6 - Math.Round(q_6, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_6.Text = "Meter" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_6 - Math.Round(q_6, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_7()
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_7 As Double
                        m_7 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_7.Text Then
                                        .Parameters.AddWithValue("@1", m_7 - Math.Round(Val(qty_7.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_7.Text = "Yard" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_7 - Math.Round(q_7, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_7.Text = "Meter" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_7 - Math.Round(q_7, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_8()
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_8 As Double
                        m_8 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_8.Text Then
                                        .Parameters.AddWithValue("@1", m_8 - Math.Round(Val(qty_8.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_8.Text = "Yard" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_8 - Math.Round(q_8, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_8.Text = "Meter" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_8 - Math.Round(q_8, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_9()
        Dim qty_9 As String = txt_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_9 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_9 As Double
                        m_9 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_9.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_9.Text Then
                                        .Parameters.AddWithValue("@1", m_9 - Math.Round(Val(qty_9.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_9.Text = "Yard" Then
                                        q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_9 - Math.Round(q_9, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_9.Text = "Meter" Then
                                        q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_9 - Math.Round(q_9, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_baris_10()
        Dim qty_10 As String = txt_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_10 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_10 As Double
                        m_10 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_10.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_10.Text Then
                                        .Parameters.AddWithValue("@1", m_10 - Math.Round(Val(qty_10.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_10.Text = "Yard" Then
                                        q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_10 - Math.Round(q_10, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_10.Text = "Meter" Then
                                        q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_10 - Math.Round(q_10, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub isiidpoproses_1()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_1.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_2()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_2.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_3()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_3.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_4()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_4.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_5()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_5.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_6()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_6.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_7()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_7.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_8()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_8.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_9()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_9.Text = x.ToString
    End Sub
    Private Sub isiidpoproses_10()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Po_Proses FROM tbpoproses WHERE Id_Po_Proses in(select max(Id_Po_Proses) FROM tbpoproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Po_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_po_proses_10.Text = x.ToString
    End Sub

    Private Sub simpan_tbpoproses_baris_1()
        Call isiidpoproses_1()
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_1.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_1.Text)
                    .Parameters.AddWithValue("@6", qty_1.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_1.Text)
                    .Parameters.AddWithValue("@8", txt_resep_1.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_1.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_1.Text)
                    .Parameters.AddWithValue("@11", txt_note_1.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_1.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_1.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_1.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_2()
        Call isiidpoproses_2()
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_2.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_2.Text)
                    .Parameters.AddWithValue("@6", qty_2.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_2.Text)
                    .Parameters.AddWithValue("@8", txt_resep_2.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_2.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_2.Text)
                    .Parameters.AddWithValue("@11", txt_note_2.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_2.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_2.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_2.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_3()
        Call isiidpoproses_3()
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_3.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_3.Text)
                    .Parameters.AddWithValue("@6", qty_3.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_3.Text)
                    .Parameters.AddWithValue("@8", txt_resep_3.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_3.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_3.Text)
                    .Parameters.AddWithValue("@11", txt_note_3.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_3.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_3.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_3.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_4()
        Call isiidpoproses_4()
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_4.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_4.Text)
                    .Parameters.AddWithValue("@6", qty_4.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_4.Text)
                    .Parameters.AddWithValue("@8", txt_resep_4.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_4.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_4.Text)
                    .Parameters.AddWithValue("@11", txt_note_4.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_4.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_4.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_4.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_5()
        Call isiidpoproses_5()
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_5.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_5.Text)
                    .Parameters.AddWithValue("@6", qty_5.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_5.Text)
                    .Parameters.AddWithValue("@8", txt_resep_5.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_5.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_5.Text)
                    .Parameters.AddWithValue("@11", txt_note_5.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_5.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_5.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_5.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_6()
        Call isiidpoproses_6()
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_6.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_6.Text)
                    .Parameters.AddWithValue("@6", qty_6.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_6.Text)
                    .Parameters.AddWithValue("@8", txt_resep_6.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_6.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_6.Text)
                    .Parameters.AddWithValue("@11", txt_note_6.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_6.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_6.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_6.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_7()
        Call isiidpoproses_7()
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_7.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_7.Text)
                    .Parameters.AddWithValue("@6", qty_7.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_7.Text)
                    .Parameters.AddWithValue("@8", txt_resep_7.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_7.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_7.Text)
                    .Parameters.AddWithValue("@11", txt_note_7.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_7.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_7.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_7.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_8()
        Call isiidpoproses_8()
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_8.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_8.Text)
                    .Parameters.AddWithValue("@6", qty_8.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_8.Text)
                    .Parameters.AddWithValue("@8", txt_resep_8.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_8.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_8.Text)
                    .Parameters.AddWithValue("@11", txt_note_8.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_8.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_8.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_8.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_9()
        Call isiidpoproses_9()
        Dim qty_9 As String = txt_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_9.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_9.Text)
                    .Parameters.AddWithValue("@6", qty_9.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_9.Text)
                    .Parameters.AddWithValue("@8", txt_resep_9.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_9.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_9.Text)
                    .Parameters.AddWithValue("@11", txt_note_9.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_9.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_9.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_9.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_9.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbpoproses_baris_10()
        Call isiidpoproses_10()
        Dim qty_10 As String = txt_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpoproses (Tanggal,Gudang,No_PO,Jenis_Kain,Asal_SJ,Asal_Ket,QTY,Warna,Resep,Tarik_Lebar,Hand_Fill,Keterangan,Id_Grey,Id_Beli,Id_Po_Proses,Satuan,Status,Harga,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_no_po.Text)
                    .Parameters.AddWithValue("@3", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@4", txt_asal_sj_10.Text)
                    .Parameters.AddWithValue("@5", txt_asal_keterangan_10.Text)
                    .Parameters.AddWithValue("@6", qty_10.Replace(",", "."))
                    .Parameters.AddWithValue("@7", txt_warna_10.Text)
                    .Parameters.AddWithValue("@8", txt_resep_10.Text)
                    .Parameters.AddWithValue("@9", txt_tarik_lebar_10.Text)
                    .Parameters.AddWithValue("@10", txt_hand_fill_10.Text)
                    .Parameters.AddWithValue("@11", txt_note_10.Text)
                    .Parameters.AddWithValue("@12", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@13", txt_id_beli_10.Text)
                    .Parameters.AddWithValue("@14", txt_id_po_proses_10.Text)
                    .Parameters.AddWithValue("@15", cb_satuan_10.Text)
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", txt_harga_10.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@18", "")
                    .Parameters.AddWithValue("@19", "")
                    .Parameters.AddWithValue("@20", "")
                    .Parameters.AddWithValue("@21", 0)
                    .Parameters.AddWithValue("@22", 0)
                    .ExecuteNonQuery()
                End With
                dtp_awal.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub

    Private Sub simpan_tbstokwipproses_baris_1()
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_1.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_1.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@7", txt_warna_1.Text)
                    .Parameters.AddWithValue("@8", txt_resep_1.Text)
                    .Parameters.AddWithValue("@9", qty_1.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@11", txt_harga_1.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_2()
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_2.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_2.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@7", txt_warna_2.Text)
                    .Parameters.AddWithValue("@8", txt_resep_2.Text)
                    .Parameters.AddWithValue("@9", qty_2.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@11", txt_harga_2.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_3()
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_3.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_3.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@7", txt_warna_3.Text)
                    .Parameters.AddWithValue("@8", txt_resep_3.Text)
                    .Parameters.AddWithValue("@9", qty_3.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@11", txt_harga_3.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_4()
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_4.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_4.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@7", txt_warna_4.Text)
                    .Parameters.AddWithValue("@8", txt_resep_4.Text)
                    .Parameters.AddWithValue("@9", qty_4.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@11", txt_harga_4.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_5()
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_5.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_5.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@7", txt_warna_5.Text)
                    .Parameters.AddWithValue("@8", txt_resep_5.Text)
                    .Parameters.AddWithValue("@9", qty_5.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@11", txt_harga_5.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_6()
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_6.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_6.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@7", txt_warna_6.Text)
                    .Parameters.AddWithValue("@8", txt_resep_6.Text)
                    .Parameters.AddWithValue("@9", qty_6.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@11", txt_harga_6.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_7()
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_7.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_7.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@7", txt_warna_7.Text)
                    .Parameters.AddWithValue("@8", txt_resep_7.Text)
                    .Parameters.AddWithValue("@9", qty_7.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@11", txt_harga_7.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_8()
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_8.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_8.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@7", txt_warna_8.Text)
                    .Parameters.AddWithValue("@8", txt_resep_8.Text)
                    .Parameters.AddWithValue("@9", qty_8.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@11", txt_harga_8.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_9()
        Dim qty_9 As String = txt_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_9.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_9.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@7", txt_warna_9.Text)
                    .Parameters.AddWithValue("@8", txt_resep_9.Text)
                    .Parameters.AddWithValue("@9", qty_9.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_9.Text)
                    .Parameters.AddWithValue("@11", txt_harga_9.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokwipproses_baris_10()
        Dim qty_10 As String = txt_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokwipproses (Tanggal,id_Grey,Id_Beli,Id_Po_Proses,No_PO,Gudang,Jenis_Kain,Warna,Resep,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_awal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_awal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@2", txt_id_beli_10.Text)
                    .Parameters.AddWithValue("@3", txt_id_po_proses_10.Text)
                    .Parameters.AddWithValue("@4", txt_no_po.Text)
                    .Parameters.AddWithValue("@5", txt_gudang.Text)
                    .Parameters.AddWithValue("@6", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@7", txt_warna_10.Text)
                    .Parameters.AddWithValue("@8", txt_resep_10.Text)
                    .Parameters.AddWithValue("@9", qty_10.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_10.Text)
                    .Parameters.AddWithValue("@11", txt_harga_10.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@12", "")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", 0)
                    .ExecuteNonQuery()
                    dtp_awal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub hapus_tbpoproses_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbpoproses_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbpoproses WHERE Id_Po_Proses ='" & txt_id_po_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_tbstokwipproses_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokwipproses_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokwipproses WHERE Id_Po_Proses ='" & txt_id_po_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_update_tbstokprosesgrey_baris_1()
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_1 As Double
                        m_1 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_1.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_1.Text Then
                                        .Parameters.AddWithValue("@1", m_1 + Math.Round(Val(qty_1.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_1.Text = "Yard" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_1 + Math.Round(q_1, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_1.Text = "Meter" Then
                                        q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_1 + Math.Round(q_1, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_2()
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_2 As Double
                        m_2 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_2.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_2.Text Then
                                        .Parameters.AddWithValue("@1", m_2 + Math.Round(Val(qty_2.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_2.Text = "Yard" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_2 + Math.Round(q_2, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_2.Text = "Meter" Then
                                        q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_2 + Math.Round(q_2, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_3()
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_3 As Double
                        m_3 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_3.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_3.Text Then
                                        .Parameters.AddWithValue("@1", m_3 + Math.Round(Val(qty_3.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_3.Text = "Yard" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_3 + Math.Round(q_3, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_3.Text = "Meter" Then
                                        q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_3 + Math.Round(q_3, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_4()
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_4 As Double
                        m_4 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_4.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_4.Text Then
                                        .Parameters.AddWithValue("@1", m_4 + Math.Round(Val(qty_4.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_4.Text = "Yard" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_4 + Math.Round(q_4, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_4.Text = "Meter" Then
                                        q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_4 + Math.Round(q_4, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_5()
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_5 As Double
                        m_5 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_5.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_5.Text Then
                                        .Parameters.AddWithValue("@1", m_5 + Math.Round(Val(qty_5.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_5.Text = "Yard" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_5 + Math.Round(q_5, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_5.Text = "Meter" Then
                                        q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_5 + Math.Round(q_5, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_6()
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_6 As Double
                        m_6 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_6.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_6.Text Then
                                        .Parameters.AddWithValue("@1", m_6 + Math.Round(Val(qty_6.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_6.Text = "Yard" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_6 + Math.Round(q_6, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_6.Text = "Meter" Then
                                        q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_6 + Math.Round(q_6, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_7()
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_7 As Double
                        m_7 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_7.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_7.Text Then
                                        .Parameters.AddWithValue("@1", m_7 + Math.Round(Val(qty_7.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_7.Text = "Yard" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_7 + Math.Round(q_7, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_7.Text = "Meter" Then
                                        q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_7 + Math.Round(q_7, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_8()
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_8 As Double
                        m_8 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_8.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_8.Text Then
                                        .Parameters.AddWithValue("@1", m_8 + Math.Round(Val(qty_8.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_8.Text = "Yard" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_8 + Math.Round(q_8, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_8.Text = "Meter" Then
                                        q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_8 + Math.Round(q_8, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_9()
        Dim qty_9 As String = txt_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_9 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_9 As Double
                        m_9 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_9.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_9.Text Then
                                        .Parameters.AddWithValue("@1", m_9 + Math.Round(Val(qty_9.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_9.Text = "Yard" Then
                                        q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_9 + Math.Round(q_9, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_9.Text = "Meter" Then
                                        q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_9 + Math.Round(q_9, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_baris_10()
        Dim qty_10 As String = txt_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_10 As Double
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Stok,Satuan FROM tbstokprosesgrey WHERE Id_Beli='" & txt_id_beli_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        Dim m_10 As Double
                        m_10 = drx.Item(0)
                        Using cony As New MySqlConnection(sLocalConn)
                            cony.Open()
                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_10.Text & "'"
                            Using cmdy As New MySqlCommand(sqly, cony)
                                With cmdy
                                    .Parameters.Clear()
                                    If drx(1) = cb_satuan_10.Text Then
                                        .Parameters.AddWithValue("@1", m_10 + Math.Round(Val(qty_10.Replace(",", ".")), 2))
                                    ElseIf drx(1) = "Meter" And cb_satuan_10.Text = "Yard" Then
                                        q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 0.9144, 0)
                                        .Parameters.AddWithValue("@1", m_10 + Math.Round(q_10, 2))
                                    ElseIf drx(1) = "Yard" And cb_satuan_10.Text = "Meter" Then
                                        q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 1.0936, 0)
                                        .Parameters.AddWithValue("@1", m_10 + Math.Round(q_10, 2))
                                    End If
                                    .ExecuteNonQuery()
                                End With
                            End Using
                        End Using
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub btn_tambah_baris_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_1.Click
        Try
            If txt_no_po.Text = "" Then
                MsgBox("Input No PO Terlebih Dahulu")
                txt_no_po.Focus()
            ElseIf txt_gudang.Text = "" Then
                MsgBox("Input GUDANG Terlebih Dahulu")
                txt_gudang.Focus()
            ElseIf txt_asal_sj_1.Text = "" And txt_jenis_kain_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_1.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_1.Focus()
            ElseIf txt_warna_1.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_1.Focus()
            ElseIf txt_resep_1.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_1.Focus()
            ElseIf txt_tarik_lebar_1.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_1.Focus()
            Else
                dtp_awal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_no_po.ReadOnly = True
                Panel_1.Enabled = False
                btn_tambah_baris_1.Visible = False
                btn_selesai_1.Visible = False
                btn_batal_1.Visible = False
                Panel_2.Visible = True
                btn_hapus_2.Visible = True
                btn_tambah_baris_2.Visible = True
                btn_selesai_2.Visible = True
                btn_batal_2.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_1()
                Call update_tbstokprosesgrey_baris_1()
                Call simpan_tbstokwipproses_baris_1()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_2.Click
        Try
            If txt_asal_sj_2.Text = "" And txt_jenis_kain_2.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_2.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_2.Focus()
            ElseIf txt_warna_2.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_2.Focus()
            ElseIf txt_resep_2.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_2.Focus()
            ElseIf txt_tarik_lebar_2.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_2.Focus()
            Else
                Panel_2.Enabled = False
                btn_hapus_2.Visible = False
                btn_tambah_baris_2.Visible = False
                btn_selesai_2.Visible = False
                btn_batal_2.Visible = False
                Panel_3.Visible = True
                btn_hapus_3.Visible = True
                btn_tambah_baris_3.Visible = True
                btn_selesai_3.Visible = True
                btn_batal_3.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_2()
                Call update_tbstokprosesgrey_baris_2()
                Call simpan_tbstokwipproses_baris_2()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_3.Click
        Try
            If txt_asal_sj_3.Text = "" And txt_jenis_kain_3.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_3.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_3.Focus()
            ElseIf txt_warna_3.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_3.Focus()
            ElseIf txt_resep_3.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_3.Focus()
            ElseIf txt_tarik_lebar_3.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_3.Focus()
            Else
                Panel_3.Enabled = False
                btn_hapus_3.Visible = False
                btn_tambah_baris_3.Visible = False
                btn_selesai_3.Visible = False
                btn_batal_3.Visible = False
                Panel_4.Visible = True
                btn_hapus_4.Visible = True
                btn_tambah_baris_4.Visible = True
                btn_selesai_4.Visible = True
                btn_batal_4.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_3()
                Call update_tbstokprosesgrey_baris_3()
                Call simpan_tbstokwipproses_baris_3()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_4.Click
        Try
            If txt_asal_sj_4.Text = "" And txt_jenis_kain_4.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_4.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_4.Focus()
            ElseIf txt_warna_4.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_4.Focus()
            ElseIf txt_resep_4.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_4.Focus()
            ElseIf txt_tarik_lebar_4.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_4.Focus()
            Else
                Panel_4.Enabled = False
                btn_hapus_4.Visible = False
                btn_tambah_baris_4.Visible = False
                btn_selesai_4.Visible = False
                btn_batal_4.Visible = False
                Panel_5.Visible = True
                btn_hapus_5.Visible = True
                btn_tambah_baris_5.Visible = True
                btn_selesai_5.Visible = True
                btn_batal_5.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_4()
                Call update_tbstokprosesgrey_baris_4()
                Call simpan_tbstokwipproses_baris_4()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_5.Click
        Try
            If txt_asal_sj_5.Text = "" And txt_jenis_kain_5.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_5.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_5.Focus()
            ElseIf txt_warna_5.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_5.Focus()
            ElseIf txt_resep_5.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_5.Focus()
            ElseIf txt_tarik_lebar_5.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_5.Focus()
            Else
                Panel_5.Enabled = False
                btn_hapus_5.Visible = False
                btn_tambah_baris_5.Visible = False
                btn_selesai_5.Visible = False
                btn_batal_5.Visible = False
                Panel_6.Visible = True
                btn_hapus_6.Visible = True
                btn_tambah_baris_6.Visible = True
                btn_selesai_6.Visible = True
                btn_batal_6.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_5()
                Call update_tbstokprosesgrey_baris_5()
                Call simpan_tbstokwipproses_baris_5()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_6.Click
        Try
            If txt_asal_sj_6.Text = "" And txt_jenis_kain_6.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_6.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_6.Focus()
            ElseIf txt_warna_6.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_6.Focus()
            ElseIf txt_resep_6.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_6.Focus()
            ElseIf txt_tarik_lebar_6.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_6.Focus()
            Else
                Panel_6.Enabled = False
                btn_hapus_6.Visible = False
                btn_tambah_baris_6.Visible = False
                btn_selesai_6.Visible = False
                btn_batal_6.Visible = False
                Panel_7.Visible = True
                btn_hapus_7.Visible = True
                btn_tambah_baris_7.Visible = True
                btn_selesai_7.Visible = True
                btn_batal_7.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_6()
                Call update_tbstokprosesgrey_baris_6()
                Call simpan_tbstokwipproses_baris_6()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_7.Click
        Try
            If txt_asal_sj_7.Text = "" And txt_jenis_kain_7.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_7.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_7.Focus()
            ElseIf txt_warna_7.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_7.Focus()
            ElseIf txt_resep_7.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_7.Focus()
            ElseIf txt_tarik_lebar_7.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_7.Focus()
            Else
                Panel_7.Enabled = False
                btn_hapus_7.Visible = False
                btn_tambah_baris_7.Visible = False
                btn_selesai_7.Visible = False
                btn_batal_7.Visible = False
                Panel_8.Visible = True
                btn_hapus_8.Visible = True
                btn_tambah_baris_8.Visible = True
                btn_selesai_8.Visible = True
                btn_batal_8.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_7()
                Call update_tbstokprosesgrey_baris_7()
                Call simpan_tbstokwipproses_baris_7()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_8.Click
        Try
            If txt_asal_sj_8.Text = "" And txt_jenis_kain_8.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_8.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_8.Focus()
            ElseIf txt_warna_8.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_8.Focus()
            ElseIf txt_resep_8.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_8.Focus()
            ElseIf txt_tarik_lebar_8.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_8.Focus()
            Else
                Panel_8.Enabled = False
                btn_hapus_8.Visible = False
                btn_tambah_baris_8.Visible = False
                btn_selesai_8.Visible = False
                btn_batal_8.Visible = False
                Panel_9.Visible = True
                btn_hapus_9.Visible = True
                btn_tambah_baris_9.Visible = True
                btn_selesai_9.Visible = True
                btn_batal_9.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_8()
                Call update_tbstokprosesgrey_baris_8()
                Call simpan_tbstokwipproses_baris_8()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_9.Click
        Try
            If txt_asal_sj_9.Text = "" And txt_jenis_kain_9.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_9.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_9.Focus()
            ElseIf txt_warna_9.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_9.Focus()
            ElseIf txt_resep_9.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_9.Focus()
            ElseIf txt_tarik_lebar_9.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_9.Focus()
            Else
                Panel_9.Enabled = False
                btn_hapus_9.Visible = False
                btn_tambah_baris_9.Visible = False
                btn_selesai_9.Visible = False
                btn_batal_9.Visible = False
                Panel_10.Visible = True
                btn_hapus_10.Visible = True
                btn_selesai_10.Visible = True
                btn_batal_10.Visible = True
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_9()
                Call update_tbstokprosesgrey_baris_9()
                Call simpan_tbstokwipproses_baris_9()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_selesai_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_1.Click
        Try
            If txt_no_po.Text = "" Then
                MsgBox("Input No PO Terlebih Dahulu")
                txt_no_po.Focus()
            ElseIf txt_gudang.Text = "" Then
                MsgBox("Input GUDANG Terlebih Dahulu")
                txt_gudang.Focus()
            ElseIf txt_asal_sj_1.Text = "" And txt_jenis_kain_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf Val(txt_qty_1.Text) = 0 Then
                MsgBox("QUANTITY Tidak Boleh Kosong")
                txt_qty_1.Focus()
            ElseIf txt_warna_1.Text = "" Then
                MsgBox("WARNA Belum Diinput")
                txt_warna_1.Focus()
            ElseIf txt_resep_1.Text = "" Then
                MsgBox("RESEP Belum Diinput")
                txt_resep_1.Focus()
            ElseIf txt_tarik_lebar_1.Text = "" Then
                MsgBox("TARIK LEBAR Belum Diinput")
                txt_tarik_lebar_1.Focus()
            Else
                Call simpan_note_po_proses()
                Call simpan_tbpoproses_baris_1()
                Call update_tbstokprosesgrey_baris_1()
                Call simpan_tbstokwipproses_baris_1()
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_2.Click
        Try
            If txt_asal_sj_2.Text = "" And txt_jenis_kain_2.Text = "" And txt_id_grey_2.Text = "" _
                And txt_harga_2.Text = "" And txt_qty_asal_2.Text = "" And txt_qty_2.Text = "" _
                And txt_warna_2.Text = "" And txt_resep_2.Text = "" And txt_hand_fill_2.Text = "" _
                And txt_tarik_lebar_2.Text = "" And txt_asal_keterangan_2.Text = "" And txt_note_2.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_2.Text = "" And txt_jenis_kain_2.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_2.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_2.Focus()
                ElseIf txt_warna_2.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_2.Focus()
                ElseIf txt_resep_2.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_2.Focus()
                ElseIf txt_tarik_lebar_2.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_2.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_2()
                    Call update_tbstokprosesgrey_baris_2()
                    Call simpan_tbstokwipproses_baris_2()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_3.Click
        Try
            If txt_asal_sj_3.Text = "" And txt_jenis_kain_3.Text = "" And txt_id_grey_3.Text = "" _
                And txt_harga_3.Text = "" And txt_qty_asal_3.Text = "" And txt_qty_3.Text = "" _
                And txt_warna_3.Text = "" And txt_resep_3.Text = "" And txt_hand_fill_3.Text = "" _
                And txt_tarik_lebar_3.Text = "" And txt_asal_keterangan_3.Text = "" And txt_note_3.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_3.Text = "" And txt_jenis_kain_3.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_3.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_3.Focus()
                ElseIf txt_warna_3.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_3.Focus()
                ElseIf txt_resep_3.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_3.Focus()
                ElseIf txt_tarik_lebar_3.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_3.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_3()
                    Call update_tbstokprosesgrey_baris_3()
                    Call simpan_tbstokwipproses_baris_3()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_4.Click
        Try
            If txt_asal_sj_4.Text = "" And txt_jenis_kain_4.Text = "" And txt_id_grey_4.Text = "" _
                And txt_harga_4.Text = "" And txt_qty_asal_4.Text = "" And txt_qty_4.Text = "" _
                And txt_warna_4.Text = "" And txt_resep_4.Text = "" And txt_hand_fill_4.Text = "" _
                And txt_tarik_lebar_4.Text = "" And txt_asal_keterangan_4.Text = "" And txt_note_4.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_4.Text = "" And txt_jenis_kain_4.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_4.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_4.Focus()
                ElseIf txt_warna_4.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_4.Focus()
                ElseIf txt_resep_4.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_4.Focus()
                ElseIf txt_tarik_lebar_4.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_4.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_4()
                    Call update_tbstokprosesgrey_baris_4()
                    Call simpan_tbstokwipproses_baris_4()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_5.Click
        Try
            If txt_asal_sj_5.Text = "" And txt_jenis_kain_5.Text = "" And txt_id_grey_5.Text = "" _
                And txt_harga_5.Text = "" And txt_qty_asal_5.Text = "" And txt_qty_5.Text = "" _
                And txt_warna_5.Text = "" And txt_resep_5.Text = "" And txt_hand_fill_5.Text = "" _
                And txt_tarik_lebar_5.Text = "" And txt_asal_keterangan_5.Text = "" And txt_note_5.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_5.Text = "" And txt_jenis_kain_5.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_5.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_5.Focus()
                ElseIf txt_warna_5.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_5.Focus()
                ElseIf txt_resep_5.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_5.Focus()
                ElseIf txt_tarik_lebar_5.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_5.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_5()
                    Call update_tbstokprosesgrey_baris_5()
                    Call simpan_tbstokwipproses_baris_5()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_6.Click
        Try
            If txt_asal_sj_6.Text = "" And txt_jenis_kain_6.Text = "" And txt_id_grey_6.Text = "" _
                And txt_harga_6.Text = "" And txt_qty_asal_6.Text = "" And txt_qty_6.Text = "" _
                And txt_warna_6.Text = "" And txt_resep_6.Text = "" And txt_hand_fill_6.Text = "" _
                And txt_tarik_lebar_6.Text = "" And txt_asal_keterangan_6.Text = "" And txt_note_6.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_6.Text = "" And txt_jenis_kain_6.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_6.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_6.Focus()
                ElseIf txt_warna_6.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_6.Focus()
                ElseIf txt_resep_6.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_6.Focus()
                ElseIf txt_tarik_lebar_6.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_6.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_6()
                    Call update_tbstokprosesgrey_baris_6()
                    Call simpan_tbstokwipproses_baris_6()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_7.Click
        Try
            If txt_asal_sj_7.Text = "" And txt_jenis_kain_7.Text = "" And txt_id_grey_7.Text = "" _
                And txt_harga_7.Text = "" And txt_qty_asal_7.Text = "" And txt_qty_7.Text = "" _
                And txt_warna_7.Text = "" And txt_resep_7.Text = "" And txt_hand_fill_7.Text = "" _
                And txt_tarik_lebar_7.Text = "" And txt_asal_keterangan_7.Text = "" And txt_note_7.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_7.Text = "" And txt_jenis_kain_7.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_7.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_7.Focus()
                ElseIf txt_warna_7.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_7.Focus()
                ElseIf txt_resep_7.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_7.Focus()
                ElseIf txt_tarik_lebar_7.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_7.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_7()
                    Call update_tbstokprosesgrey_baris_7()
                    Call simpan_tbstokwipproses_baris_7()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_8.Click
        Try
            If txt_asal_sj_8.Text = "" And txt_jenis_kain_8.Text = "" And txt_id_grey_8.Text = "" _
                And txt_harga_8.Text = "" And txt_qty_asal_8.Text = "" And txt_qty_8.Text = "" _
                And txt_warna_8.Text = "" And txt_resep_8.Text = "" And txt_hand_fill_8.Text = "" _
                And txt_tarik_lebar_8.Text = "" And txt_asal_keterangan_8.Text = "" And txt_note_8.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_8.Text = "" And txt_jenis_kain_8.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_8.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_8.Focus()
                ElseIf txt_warna_8.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_8.Focus()
                ElseIf txt_resep_8.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_8.Focus()
                ElseIf txt_tarik_lebar_8.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_8.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_8()
                    Call update_tbstokprosesgrey_baris_8()
                    Call simpan_tbstokwipproses_baris_8()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_9.Click
        Try
            If txt_asal_sj_9.Text = "" And txt_jenis_kain_9.Text = "" And txt_id_grey_9.Text = "" _
                And txt_harga_9.Text = "" And txt_qty_asal_9.Text = "" And txt_qty_9.Text = "" _
                And txt_warna_9.Text = "" And txt_resep_9.Text = "" And txt_hand_fill_9.Text = "" _
                And txt_tarik_lebar_9.Text = "" And txt_asal_keterangan_9.Text = "" And txt_note_9.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_9.Text = "" And txt_jenis_kain_9.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_9.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_9.Focus()
                ElseIf txt_warna_9.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_9.Focus()
                ElseIf txt_resep_9.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_9.Focus()
                ElseIf txt_tarik_lebar_9.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_9.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_9()
                    Call update_tbstokprosesgrey_baris_9()
                    Call simpan_tbstokwipproses_baris_9()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_10.Click
        Try
            If txt_asal_sj_10.Text = "" And txt_jenis_kain_10.Text = "" And txt_id_grey_10.Text = "" _
                And txt_harga_10.Text = "" And txt_qty_asal_10.Text = "" And txt_qty_10.Text = "" _
                And txt_warna_10.Text = "" And txt_resep_10.Text = "" And txt_hand_fill_10.Text = "" _
                And txt_tarik_lebar_10.Text = "" And txt_asal_keterangan_10.Text = "" And txt_note_10.Text = "" Then
                MsgBox("PO PROSES Baru Berhasil Disimpan")
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            Else
                If txt_asal_sj_10.Text = "" And txt_jenis_kain_10.Text = "" Then
                    MsgBox("Baris 2 Belum Diisi Data")
                ElseIf Val(txt_qty_10.Text) = 0 Then
                    MsgBox("QUANTITY Tidak Boleh Kosong")
                    txt_qty_10.Focus()
                ElseIf txt_warna_10.Text = "" Then
                    MsgBox("WARNA Tidak Boleh Kosong")
                    txt_warna_10.Focus()
                ElseIf txt_resep_10.Text = "" Then
                    MsgBox("RESEP Tidak Boleh Kosong")
                    txt_resep_10.Focus()
                ElseIf txt_tarik_lebar_10.Text = "" Then
                    MsgBox("TARIK LEBAR Tidak Boleh Kosong")
                    txt_tarik_lebar_10.Focus()
                Else
                    Call simpan_note_po_proses()
                    Call simpan_tbpoproses_baris_10()
                    Call update_tbstokprosesgrey_baris_10()
                    Call simpan_tbstokwipproses_baris_10()
                    MsgBox("PO PROSES Baru Berhasil Disimpan")
                    form_po_proses.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_hapus_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_2.Click
        txt_asal_sj_2.Text = ""
        txt_asal_sj_2.ReadOnly = False
        txt_jenis_kain_2.Text = ""
        txt_jenis_kain_2.ReadOnly = False
        txt_id_grey_2.Text = ""
        txt_harga_2.Text = ""
        txt_qty_asal_2.Text = ""
        txt_qty_2.Text = ""
        txt_warna_2.Text = ""
        txt_resep_2.Text = ""
        txt_hand_fill_2.Text = ""
        txt_tarik_lebar_2.Text = ""
        txt_asal_keterangan_2.Text = ""
        txt_note_2.Text = ""
    End Sub
    Private Sub btn_hapus_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_3.Click
        txt_asal_sj_3.Text = ""
        txt_asal_sj_3.ReadOnly = False
        txt_jenis_kain_3.Text = ""
        txt_jenis_kain_3.ReadOnly = False
        txt_id_grey_3.Text = ""
        txt_harga_3.Text = ""
        txt_qty_asal_3.Text = ""
        txt_qty_3.Text = ""
        txt_warna_3.Text = ""
        txt_resep_3.Text = ""
        txt_hand_fill_3.Text = ""
        txt_tarik_lebar_3.Text = ""
        txt_asal_keterangan_3.Text = ""
        txt_note_3.Text = ""
    End Sub
    Private Sub btn_hapus_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_4.Click
        txt_asal_sj_4.Text = ""
        txt_asal_sj_4.ReadOnly = False
        txt_jenis_kain_4.Text = ""
        txt_jenis_kain_4.ReadOnly = False
        txt_id_grey_4.Text = ""
        txt_harga_4.Text = ""
        txt_qty_asal_4.Text = ""
        txt_qty_4.Text = ""
        txt_warna_4.Text = ""
        txt_resep_4.Text = ""
        txt_hand_fill_4.Text = ""
        txt_tarik_lebar_4.Text = ""
        txt_asal_keterangan_4.Text = ""
        txt_note_4.Text = ""
    End Sub
    Private Sub btn_hapus_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_5.Click
        txt_asal_sj_5.Text = ""
        txt_asal_sj_5.ReadOnly = False
        txt_jenis_kain_5.Text = ""
        txt_jenis_kain_5.ReadOnly = False
        txt_id_grey_5.Text = ""
        txt_harga_5.Text = ""
        txt_qty_asal_5.Text = ""
        txt_qty_5.Text = ""
        txt_warna_5.Text = ""
        txt_resep_5.Text = ""
        txt_hand_fill_5.Text = ""
        txt_tarik_lebar_5.Text = ""
        txt_asal_keterangan_5.Text = ""
        txt_note_5.Text = ""
    End Sub
    Private Sub btn_hapus_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_6.Click
        txt_asal_sj_6.Text = ""
        txt_asal_sj_6.ReadOnly = False
        txt_jenis_kain_6.Text = ""
        txt_jenis_kain_6.ReadOnly = False
        txt_id_grey_6.Text = ""
        txt_harga_6.Text = ""
        txt_qty_asal_6.Text = ""
        txt_qty_6.Text = ""
        txt_warna_6.Text = ""
        txt_resep_6.Text = ""
        txt_hand_fill_6.Text = ""
        txt_tarik_lebar_6.Text = ""
        txt_asal_keterangan_6.Text = ""
        txt_note_6.Text = ""
    End Sub
    Private Sub btn_hapus_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_7.Click
        txt_asal_sj_7.Text = ""
        txt_asal_sj_7.ReadOnly = False
        txt_jenis_kain_7.Text = ""
        txt_jenis_kain_7.ReadOnly = False
        txt_id_grey_7.Text = ""
        txt_harga_7.Text = ""
        txt_qty_asal_7.Text = ""
        txt_qty_7.Text = ""
        txt_warna_7.Text = ""
        txt_resep_7.Text = ""
        txt_hand_fill_7.Text = ""
        txt_tarik_lebar_7.Text = ""
        txt_asal_keterangan_7.Text = ""
        txt_note_7.Text = ""
    End Sub
    Private Sub btn_hapus_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_8.Click
        txt_asal_sj_8.Text = ""
        txt_asal_sj_8.ReadOnly = False
        txt_jenis_kain_8.Text = ""
        txt_jenis_kain_8.ReadOnly = False
        txt_id_grey_8.Text = ""
        txt_harga_8.Text = ""
        txt_qty_asal_8.Text = ""
        txt_qty_8.Text = ""
        txt_warna_8.Text = ""
        txt_resep_8.Text = ""
        txt_hand_fill_8.Text = ""
        txt_tarik_lebar_8.Text = ""
        txt_asal_keterangan_8.Text = ""
        txt_note_8.Text = ""
    End Sub
    Private Sub btn_hapus_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_9.Click
        txt_asal_sj_9.Text = ""
        txt_asal_sj_9.ReadOnly = False
        txt_jenis_kain_9.Text = ""
        txt_jenis_kain_9.ReadOnly = False
        txt_id_grey_9.Text = ""
        txt_harga_9.Text = ""
        txt_qty_asal_9.Text = ""
        txt_qty_9.Text = ""
        txt_warna_9.Text = ""
        txt_resep_9.Text = ""
        txt_hand_fill_9.Text = ""
        txt_tarik_lebar_9.Text = ""
        txt_asal_keterangan_9.Text = ""
        txt_note_9.Text = ""
    End Sub
    Private Sub btn_hapus_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_10.Click
        txt_asal_sj_10.Text = ""
        txt_asal_sj_10.ReadOnly = False
        txt_jenis_kain_10.Text = ""
        txt_jenis_kain_10.ReadOnly = False
        txt_id_grey_10.Text = ""
        txt_harga_10.Text = ""
        txt_qty_asal_10.Text = ""
        txt_qty_10.Text = ""
        txt_warna_10.Text = ""
        txt_resep_10.Text = ""
        txt_hand_fill_10.Text = ""
        txt_tarik_lebar_10.Text = ""
        txt_asal_keterangan_10.Text = ""
        txt_note_10.Text = ""
    End Sub

    Private Sub txt_qty_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_1.Text = String.Empty Then
                        txt_qty_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_1.Select(txt_qty_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_1.Text = String.Empty Then
                        txt_qty_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_1.Select(txt_qty_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_2.Text = String.Empty Then
                        txt_qty_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_2.Select(txt_qty_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_2.Text = String.Empty Then
                        txt_qty_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_2.Select(txt_qty_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_3.Text = String.Empty Then
                        txt_qty_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_3.Select(txt_qty_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_3.Text = String.Empty Then
                        txt_qty_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_3.Select(txt_qty_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_4.Text = String.Empty Then
                        txt_qty_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_4.Select(txt_qty_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_4.Text = String.Empty Then
                        txt_qty_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_4.Select(txt_qty_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_5.Text = String.Empty Then
                        txt_qty_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_5.Select(txt_qty_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_5.Text = String.Empty Then
                        txt_qty_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_5.Select(txt_qty_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_6.Text = String.Empty Then
                        txt_qty_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_6.Select(txt_qty_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_6.Text = String.Empty Then
                        txt_qty_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_6.Select(txt_qty_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_7.Text = String.Empty Then
                        txt_qty_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_7.Select(txt_qty_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_7.Text = String.Empty Then
                        txt_qty_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_7.Select(txt_qty_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_8.Text = String.Empty Then
                        txt_qty_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_8.Select(txt_qty_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_8.Text = String.Empty Then
                        txt_qty_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_8.Select(txt_qty_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_9.Text = String.Empty Then
                        txt_qty_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_9.Select(txt_qty_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_9.Text = String.Empty Then
                        txt_qty_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_9.Select(txt_qty_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_qty_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_qty_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_qty_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_qty_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_10.Text = String.Empty Then
                        txt_qty_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_10.Select(txt_qty_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_qty_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_qty_10.Text = String.Empty Then
                        txt_qty_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_qty_10.Select(txt_qty_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_id_beli_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_1.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_1.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_1.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_2.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_2.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_2.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_3.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_3.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_3.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_4.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_4.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_4.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_5.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_5.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_5.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_6.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_6.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_6.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_7.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_7.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_7.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_8.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_8.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_8.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_9.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_9.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_9.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub
    Private Sub txt_id_beli_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_id_beli_10.TextChanged
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Keterangan FROM tbpembeliangrey WHERE Id_Beli='" & txt_id_beli_10.Text & "'"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_asal_keterangan_10.Text = drx.Item(0).ToString
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub txt_qty_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_1.TextChanged
        If txt_qty_1.Text <> String.Empty Then
            Dim temp As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_1.Select(txt_qty_1.Text.Length, 0)
            ElseIf txt_qty_1.Text = "-"c Then

            Else
                txt_qty_1.Text = CDec(temp).ToString("N0")
                txt_qty_1.Select(txt_qty_1.Text.Length, 0)
            End If
        End If
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_1.Replace(",", ".")) > Val(txt_qty_asal_1.Text.Replace(",", ".")) Then
            txt_qty_1.Text = txt_qty_asal_1.Text
        End If
    End Sub
    Private Sub txt_qty_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_2.TextChanged
        If txt_qty_2.Text <> String.Empty Then
            Dim temp As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_2.Select(txt_qty_2.Text.Length, 0)
            ElseIf txt_qty_2.Text = "-"c Then

            Else
                txt_qty_2.Text = CDec(temp).ToString("N0")
                txt_qty_2.Select(txt_qty_2.Text.Length, 0)
            End If
        End If
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_2.Replace(",", ".")) > Val(txt_qty_asal_2.Text.Replace(",", ".")) Then
            txt_qty_2.Text = txt_qty_asal_2.Text
        End If
    End Sub
    Private Sub txt_qty_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_3.TextChanged
        If txt_qty_3.Text <> String.Empty Then
            Dim temp As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_3.Select(txt_qty_3.Text.Length, 0)
            ElseIf txt_qty_3.Text = "-"c Then

            Else
                txt_qty_3.Text = CDec(temp).ToString("N0")
                txt_qty_3.Select(txt_qty_3.Text.Length, 0)
            End If
        End If
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_3.Replace(",", ".")) > Val(txt_qty_asal_3.Text.Replace(",", ".")) Then
            txt_qty_3.Text = txt_qty_asal_3.Text
        End If
    End Sub
    Private Sub txt_qty_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_4.TextChanged
        If txt_qty_4.Text <> String.Empty Then
            Dim temp As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_4.Select(txt_qty_4.Text.Length, 0)
            ElseIf txt_qty_4.Text = "-"c Then

            Else
                txt_qty_4.Text = CDec(temp).ToString("N0")
                txt_qty_4.Select(txt_qty_4.Text.Length, 0)
            End If
        End If
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_4.Replace(",", ".")) > Val(txt_qty_asal_4.Text.Replace(",", ".")) Then
            txt_qty_4.Text = txt_qty_asal_4.Text
        End If
    End Sub
    Private Sub txt_qty_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_5.TextChanged
        If txt_qty_5.Text <> String.Empty Then
            Dim temp As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_5.Select(txt_qty_5.Text.Length, 0)
            ElseIf txt_qty_5.Text = "-"c Then

            Else
                txt_qty_5.Text = CDec(temp).ToString("N0")
                txt_qty_5.Select(txt_qty_5.Text.Length, 0)
            End If
        End If
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_5.Replace(",", ".")) > Val(txt_qty_asal_5.Text.Replace(",", ".")) Then
            txt_qty_5.Text = txt_qty_asal_5.Text
        End If
    End Sub
    Private Sub txt_qty_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_6.TextChanged
        If txt_qty_6.Text <> String.Empty Then
            Dim temp As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_6.Select(txt_qty_6.Text.Length, 0)
            ElseIf txt_qty_6.Text = "-"c Then

            Else
                txt_qty_6.Text = CDec(temp).ToString("N0")
                txt_qty_6.Select(txt_qty_6.Text.Length, 0)
            End If
        End If
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_6.Replace(",", ".")) > Val(txt_qty_asal_6.Text.Replace(",", ".")) Then
            txt_qty_6.Text = txt_qty_asal_6.Text
        End If
    End Sub
    Private Sub txt_qty_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_7.TextChanged
        If txt_qty_7.Text <> String.Empty Then
            Dim temp As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_7.Select(txt_qty_7.Text.Length, 0)
            ElseIf txt_qty_7.Text = "-"c Then

            Else
                txt_qty_7.Text = CDec(temp).ToString("N0")
                txt_qty_7.Select(txt_qty_7.Text.Length, 0)
            End If
        End If
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_7.Replace(",", ".")) > Val(txt_qty_asal_7.Text.Replace(",", ".")) Then
            txt_qty_7.Text = txt_qty_asal_7.Text
        End If
    End Sub
    Private Sub txt_qty_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_8.TextChanged
        If txt_qty_8.Text <> String.Empty Then
            Dim temp As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_8.Select(txt_qty_8.Text.Length, 0)
            ElseIf txt_qty_8.Text = "-"c Then

            Else
                txt_qty_8.Text = CDec(temp).ToString("N0")
                txt_qty_8.Select(txt_qty_8.Text.Length, 0)
            End If
        End If
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_8.Replace(",", ".")) > Val(txt_qty_asal_8.Text.Replace(",", ".")) Then
            txt_qty_8.Text = txt_qty_asal_8.Text
        End If
    End Sub
    Private Sub txt_qty_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_9.TextChanged
        If txt_qty_9.Text <> String.Empty Then
            Dim temp As String = txt_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_9.Select(txt_qty_9.Text.Length, 0)
            ElseIf txt_qty_9.Text = "-"c Then

            Else
                txt_qty_9.Text = CDec(temp).ToString("N0")
                txt_qty_9.Select(txt_qty_9.Text.Length, 0)
            End If
        End If
        Dim qty_9 As String = txt_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_9.Replace(",", ".")) > Val(txt_qty_asal_9.Text.Replace(",", ".")) Then
            txt_qty_9.Text = txt_qty_asal_9.Text
        End If
    End Sub
    Private Sub txt_qty_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty_10.TextChanged
        If txt_qty_10.Text <> String.Empty Then
            Dim temp As String = txt_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_qty_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_qty_10.Select(txt_qty_10.Text.Length, 0)
            ElseIf txt_qty_10.Text = "-"c Then

            Else
                txt_qty_10.Text = CDec(temp).ToString("N0")
                txt_qty_10.Select(txt_qty_10.Text.Length, 0)
            End If
        End If
        Dim qty_10 As String = txt_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If Val(qty_10.Replace(",", ".")) > Val(txt_qty_asal_10.Text.Replace(",", ".")) Then
            txt_qty_10.Text = txt_qty_asal_10.Text
        End If
    End Sub

    Private Sub txt_warna_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_1.GotFocus, txt_resep_1.GotFocus
        If txt_jenis_kain_1.Text = "" Then
            txt_tarik_lebar_1.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_1.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_1"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_1.Text
        End If
    End Sub
    Private Sub txt_warna_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_2.GotFocus, txt_resep_2.GotFocus
        If txt_jenis_kain_2.Text = "" Then
            txt_tarik_lebar_2.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_2.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_2"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_2.Text
        End If
    End Sub
    Private Sub txt_warna_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_3.GotFocus, txt_resep_3.GotFocus
        If txt_jenis_kain_3.Text = "" Then
            txt_tarik_lebar_3.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_3.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_3"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_3.Text
        End If
    End Sub
    Private Sub txt_warna_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_4.GotFocus, txt_resep_4.GotFocus
        If txt_jenis_kain_4.Text = "" Then
            txt_tarik_lebar_4.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_4.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_4"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_4.Text
        End If
    End Sub
    Private Sub txt_warna_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_5.GotFocus, txt_resep_5.GotFocus
        If txt_jenis_kain_5.Text = "" Then
            txt_tarik_lebar_5.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_5.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_5"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_5.Text
        End If
    End Sub
    Private Sub txt_warna_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_6.GotFocus, txt_resep_6.GotFocus
        If txt_jenis_kain_6.Text = "" Then
            txt_tarik_lebar_6.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_6.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_6"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_6.Text
        End If
    End Sub
    Private Sub txt_warna_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_7.GotFocus, txt_resep_7.GotFocus
        If txt_jenis_kain_7.Text = "" Then
            txt_tarik_lebar_7.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_7.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_7"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_7.Text
        End If
    End Sub
    Private Sub txt_warna_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_8.GotFocus, txt_resep_8.GotFocus
        If txt_jenis_kain_8.Text = "" Then
            txt_tarik_lebar_8.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_8.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_8"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_8.Text
        End If
    End Sub
    Private Sub txt_warna_9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_9.GotFocus, txt_resep_9.GotFocus
        If txt_jenis_kain_9.Text = "" Then
            txt_tarik_lebar_9.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_9.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_9"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_9.Text
        End If
    End Sub
    Private Sub txt_warna_10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_warna_10.GotFocus, txt_resep_10.GotFocus
        If txt_jenis_kain_10.Text = "" Then
            txt_tarik_lebar_10.Focus()
            MsgBox("JENIS KAIN Belum Diinput")
            txt_jenis_kain_10.Focus()
        Else
            form_input_warna_resep.MdiParent = form_menu_utama
            form_input_warna_resep.Show()
            form_input_warna_resep.TxtForm.Text = "form_input_po_proses_baris_10"
            form_input_warna_resep.Focus()
            form_input_warna_resep.txt_cari_jenis_kain.Text = txt_jenis_kain_10.Text
        End If
    End Sub

    Private Sub btn_batal_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_1.Click
        Try
            form_po_proses.Show()
            form_po_proses.Focus()
            form_po_proses.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_2.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_3.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_4.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_3()
                Call hapus_tbstokwipproses_3()
                Call hapus_update_tbstokprosesgrey_baris_3()
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_5.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_4()
                Call hapus_tbstokwipproses_4()
                Call hapus_update_tbstokprosesgrey_baris_4()
                Call hapus_tbpoproses_3()
                Call hapus_tbstokwipproses_3()
                Call hapus_update_tbstokprosesgrey_baris_3()
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_6.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_5()
                Call hapus_tbstokwipproses_5()
                Call hapus_update_tbstokprosesgrey_baris_5()
                Call hapus_tbpoproses_4()
                Call hapus_tbstokwipproses_4()
                Call hapus_update_tbstokprosesgrey_baris_4()
                Call hapus_tbpoproses_3()
                Call hapus_tbstokwipproses_3()
                Call hapus_update_tbstokprosesgrey_baris_3()
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_7.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_6()
                Call hapus_tbstokwipproses_6()
                Call hapus_update_tbstokprosesgrey_baris_6()
                Call hapus_tbpoproses_5()
                Call hapus_tbstokwipproses_5()
                Call hapus_update_tbstokprosesgrey_baris_5()
                Call hapus_tbpoproses_4()
                Call hapus_tbstokwipproses_4()
                Call hapus_update_tbstokprosesgrey_baris_4()
                Call hapus_tbpoproses_3()
                Call hapus_tbstokwipproses_3()
                Call hapus_update_tbstokprosesgrey_baris_3()
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_8.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_7()
                Call hapus_tbstokwipproses_7()
                Call hapus_update_tbstokprosesgrey_baris_7()
                Call hapus_tbpoproses_6()
                Call hapus_tbstokwipproses_6()
                Call hapus_update_tbstokprosesgrey_baris_6()
                Call hapus_tbpoproses_5()
                Call hapus_tbstokwipproses_5()
                Call hapus_update_tbstokprosesgrey_baris_5()
                Call hapus_tbpoproses_4()
                Call hapus_tbstokwipproses_4()
                Call hapus_update_tbstokprosesgrey_baris_4()
                Call hapus_tbpoproses_3()
                Call hapus_tbstokwipproses_3()
                Call hapus_update_tbstokprosesgrey_baris_3()
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_9.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_8()
                Call hapus_tbstokwipproses_8()
                Call hapus_update_tbstokprosesgrey_baris_8()
                Call hapus_tbpoproses_7()
                Call hapus_tbstokwipproses_7()
                Call hapus_update_tbstokprosesgrey_baris_7()
                Call hapus_tbpoproses_6()
                Call hapus_tbstokwipproses_6()
                Call hapus_update_tbstokprosesgrey_baris_6()
                Call hapus_tbpoproses_5()
                Call hapus_tbstokwipproses_5()
                Call hapus_update_tbstokprosesgrey_baris_5()
                Call hapus_tbpoproses_4()
                Call hapus_tbstokwipproses_4()
                Call hapus_update_tbstokprosesgrey_baris_4()
                Call hapus_tbpoproses_3()
                Call hapus_tbstokwipproses_3()
                Call hapus_update_tbstokprosesgrey_baris_3()
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_10.Click
        Try
            If MsgBox("Yakin PO PROSES Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbpoproses_9()
                Call hapus_tbstokwipproses_9()
                Call hapus_update_tbstokprosesgrey_baris_9()
                Call hapus_tbpoproses_8()
                Call hapus_tbstokwipproses_8()
                Call hapus_update_tbstokprosesgrey_baris_8()
                Call hapus_tbpoproses_7()
                Call hapus_tbstokwipproses_7()
                Call hapus_update_tbstokprosesgrey_baris_7()
                Call hapus_tbpoproses_6()
                Call hapus_tbstokwipproses_6()
                Call hapus_update_tbstokprosesgrey_baris_6()
                Call hapus_tbpoproses_5()
                Call hapus_tbstokwipproses_5()
                Call hapus_update_tbstokprosesgrey_baris_5()
                Call hapus_tbpoproses_4()
                Call hapus_tbstokwipproses_4()
                Call hapus_update_tbstokprosesgrey_baris_4()
                Call hapus_tbpoproses_3()
                Call hapus_tbstokwipproses_3()
                Call hapus_update_tbstokprosesgrey_baris_3()
                Call hapus_tbpoproses_2()
                Call hapus_tbstokwipproses_2()
                Call hapus_update_tbstokprosesgrey_baris_2()
                Call hapus_tbpoproses_1()
                Call hapus_tbstokwipproses_1()
                Call hapus_update_tbstokprosesgrey_baris_1()
                Call hapus_note_po_proses()
                form_po_proses.Show()
                form_po_proses.Focus()
                form_po_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txt_tarik_lebar_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_1.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_1.Focus()
        If txt_tarik_lebar_1.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_1.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_2.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_2.Focus()
        If txt_tarik_lebar_2.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_2.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_3.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_3.Focus()
        If txt_tarik_lebar_3.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_3.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_4.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_4.Focus()
        If txt_tarik_lebar_4.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_4.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_5.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_5.Focus()
        If txt_tarik_lebar_5.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_5.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_6.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_6.Focus()
        If txt_tarik_lebar_6.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_6.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_7.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_7.Focus()
        If txt_tarik_lebar_7.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_7.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_8.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_8.Focus()
        If txt_tarik_lebar_8.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_8.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_9.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_9.Focus()
        If txt_tarik_lebar_9.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_9.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub
    Private Sub txt_tarik_lebar_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_tarik_lebar_10.KeyPress
        If e.KeyChar = Chr(13) Then txt_hand_fill_10.Focus()
        If txt_tarik_lebar_10.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_tarik_lebar_10.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub

    Private Sub txt_hand_fill_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_1.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_1.Focus()
    End Sub
    Private Sub txt_hand_fill_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_2.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_2.Focus()
    End Sub
    Private Sub txt_hand_fill_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_3.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_3.Focus()
    End Sub
    Private Sub txt_hand_fill_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_4.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_4.Focus()
    End Sub
    Private Sub txt_hand_fill_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_5.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_5.Focus()
    End Sub
    Private Sub txt_hand_fill_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_6.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_6.Focus()
    End Sub
    Private Sub txt_hand_fill_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_7.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_7.Focus()
    End Sub
    Private Sub txt_hand_fill_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_8.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_8.Focus()
    End Sub
    Private Sub txt_hand_fill_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_9.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_9.Focus()
    End Sub
    Private Sub txt_hand_fill_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_hand_fill_10.KeyPress
        If e.KeyChar = Chr(13) Then txt_asal_keterangan_10.Focus()
    End Sub

    Private Sub txt_asal_keterangan_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_1.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_1.Focus()
    End Sub
    Private Sub txt_asal_keterangan_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_2.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_2.Focus()
    End Sub
    Private Sub txt_asal_keterangan_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_3.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_3.Focus()
    End Sub
    Private Sub txt_asal_keterangan_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_4.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_4.Focus()
    End Sub
    Private Sub txt_asal_keterangan_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_5.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_5.Focus()
    End Sub
    Private Sub txt_asal_keterangan_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_6.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_6.Focus()
    End Sub
    Private Sub txt_asal_keterangan_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_7.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_7.Focus()
    End Sub
    Private Sub txt_asal_keterangan_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_8.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_8.Focus()
    End Sub
    Private Sub txt_asal_keterangan_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_9.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_9.Focus()
    End Sub
    Private Sub txt_asal_keterangan_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_asal_keterangan_10.KeyPress
        If e.KeyChar = Chr(13) Then txt_note_10.Focus()
    End Sub

    Private Sub cb_satuan_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_1.TextChanged
        Dim qty_1 As String = txt_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_1, qa_1, h_1 As Double
        If txt_qty_1.Text = "" Then
        ElseIf Val(txt_qty_1.Text) = 0 Then
        Else
            If cb_satuan_1.Text = "Yard" Then
                qa_1 = Math.Round(Val(txt_qty_asal_1.Text.Replace(",", ".")) * 1.0936, 0)
                q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_1.Text = "Meter" Then
                qa_1 = Math.Round(Val(txt_qty_asal_1.Text.Replace(",", ".")) * 0.9144, 0)
                q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_1.Text = qa_1
            txt_qty_1.Text = q_1
            txt_harga_1.Text = h_1
        End If
    End Sub
    Private Sub cb_satuan_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_2.TextChanged
        Dim qty_2 As String = txt_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_2, qa_2, h_2 As Double
        If txt_qty_2.Text = "" Then
        ElseIf Val(txt_qty_2.Text) = 0 Then
        Else
            If cb_satuan_2.Text = "Yard" Then
                qa_2 = Math.Round(Val(txt_qty_asal_2.Text.Replace(",", ".")) * 1.0936, 0)
                q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_2.Text = "Meter" Then
                qa_2 = Math.Round(Val(txt_qty_asal_2.Text.Replace(",", ".")) * 0.9144, 0)
                q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_2.Text = qa_2
            txt_qty_2.Text = q_2
            txt_harga_2.Text = h_2
        End If
    End Sub
    Private Sub cb_satuan_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_3.TextChanged
        Dim qty_3 As String = txt_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_3, qa_3, h_3 As Double
        If txt_qty_3.Text = "" Then
        ElseIf Val(txt_qty_3.Text) = 0 Then
        Else
            If cb_satuan_3.Text = "Yard" Then
                qa_3 = Math.Round(Val(txt_qty_asal_3.Text.Replace(",", ".")) * 1.0936, 0)
                q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_3.Text = "Meter" Then
                qa_3 = Math.Round(Val(txt_qty_asal_3.Text.Replace(",", ".")) * 0.9144, 0)
                q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_3.Text = qa_3
            txt_qty_3.Text = q_3
            txt_harga_3.Text = h_3
        End If
    End Sub
    Private Sub cb_satuan_4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_4.TextChanged
        Dim qty_4 As String = txt_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_4, qa_4, h_4 As Double
        If txt_qty_4.Text = "" Then
        ElseIf Val(txt_qty_4.Text) = 0 Then
        Else
            If cb_satuan_4.Text = "Yard" Then
                qa_4 = Math.Round(Val(txt_qty_asal_4.Text.Replace(",", ".")) * 1.0936, 0)
                q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_4.Text = "Meter" Then
                qa_4 = Math.Round(Val(txt_qty_asal_4.Text.Replace(",", ".")) * 0.9144, 0)
                q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_4.Text = qa_4
            txt_qty_4.Text = q_4
            txt_harga_4.Text = h_4
        End If
    End Sub
    Private Sub cb_satuan_5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_5.TextChanged
        Dim qty_5 As String = txt_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_5, qa_5, h_5 As Double
        If txt_qty_5.Text = "" Then
        ElseIf Val(txt_qty_5.Text) = 0 Then
        Else
            If cb_satuan_5.Text = "Yard" Then
                qa_5 = Math.Round(Val(txt_qty_asal_5.Text.Replace(",", ".")) * 1.0936, 0)
                q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_5.Text = "Meter" Then
                qa_5 = Math.Round(Val(txt_qty_asal_5.Text.Replace(",", ".")) * 0.9144, 0)
                q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_5.Text = qa_5
            txt_qty_5.Text = q_5
            txt_harga_5.Text = h_5
        End If
    End Sub
    Private Sub cb_satuan_6_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_6.TextChanged
        Dim qty_6 As String = txt_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_6, qa_6, h_6 As Double
        If txt_qty_6.Text = "" Then
        ElseIf Val(txt_qty_6.Text) = 0 Then
        Else
            If cb_satuan_6.Text = "Yard" Then
                qa_6 = Math.Round(Val(txt_qty_asal_6.Text.Replace(",", ".")) * 1.0936, 0)
                q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_6.Text = "Meter" Then
                qa_6 = Math.Round(Val(txt_qty_asal_6.Text.Replace(",", ".")) * 0.9144, 0)
                q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_6.Text = qa_6
            txt_qty_6.Text = q_6
            txt_harga_6.Text = h_6
        End If
    End Sub
    Private Sub cb_satuan_7_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_7.TextChanged
        Dim qty_7 As String = txt_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_7, qa_7, h_7 As Double
        If txt_qty_7.Text = "" Then
        ElseIf Val(txt_qty_7.Text) = 0 Then
        Else
            If cb_satuan_7.Text = "Yard" Then
                qa_7 = Math.Round(Val(txt_qty_asal_7.Text.Replace(",", ".")) * 1.0936, 0)
                q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_7.Text = "Meter" Then
                qa_7 = Math.Round(Val(txt_qty_asal_7.Text.Replace(",", ".")) * 0.9144, 0)
                q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_7.Text = qa_7
            txt_qty_7.Text = q_7
            txt_harga_7.Text = h_7
        End If
    End Sub
    Private Sub cb_satuan_8_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_8.TextChanged
        Dim qty_8 As String = txt_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_8, qa_8, h_8 As Double
        If txt_qty_8.Text = "" Then
        ElseIf Val(txt_qty_8.Text) = 0 Then
        Else
            If cb_satuan_8.Text = "Yard" Then
                qa_8 = Math.Round(Val(txt_qty_asal_8.Text.Replace(",", ".")) * 1.0936, 0)
                q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_8.Text = "Meter" Then
                qa_8 = Math.Round(Val(txt_qty_asal_8.Text.Replace(",", ".")) * 0.9144, 0)
                q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_8.Text = qa_8
            txt_qty_8.Text = q_8
            txt_harga_8.Text = h_8
        End If
    End Sub
    Private Sub cb_satuan_9_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_9.TextChanged
        Dim qty_9 As String = txt_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_9 As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_9, qa_9, h_9 As Double
        If txt_qty_9.Text = "" Then
        ElseIf Val(txt_qty_9.Text) = 0 Then
        Else
            If cb_satuan_9.Text = "Yard" Then
                qa_9 = Math.Round(Val(txt_qty_asal_9.Text.Replace(",", ".")) * 1.0936, 0)
                q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 1.0936, 0)
                h_9 = Math.Round(Val(harga_9.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_9.Text = "Meter" Then
                qa_9 = Math.Round(Val(txt_qty_asal_9.Text.Replace(",", ".")) * 0.9144, 0)
                q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 0.9144, 0)
                h_9 = Math.Round(Val(harga_9.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_9.Text = qa_9
            txt_qty_9.Text = q_9
            txt_harga_9.Text = h_9
        End If
    End Sub
    Private Sub cb_satuan_10_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_10.TextChanged
        Dim qty_10 As String = txt_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_10 As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)

        Dim q_10, qa_10, h_10 As Double
        If txt_qty_10.Text = "" Then
        ElseIf Val(txt_qty_10.Text) = 0 Then
        Else
            If cb_satuan_10.Text = "Yard" Then
                qa_10 = Math.Round(Val(txt_qty_asal_10.Text.Replace(",", ".")) * 1.0936, 0)
                q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 1.0936, 0)
                h_10 = Math.Round(Val(harga_10.Replace(",", ".")) / 1.0936, 0)
            ElseIf cb_satuan_10.Text = "Meter" Then
                qa_10 = Math.Round(Val(txt_qty_asal_10.Text.Replace(",", ".")) * 0.9144, 0)
                q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 0.9144, 0)
                h_10 = Math.Round(Val(harga_10.Replace(",", ".")) / 0.9144, 0)
            End If
            txt_qty_asal_10.Text = qa_10
            txt_qty_10.Text = q_10
            txt_harga_10.Text = h_10
        End If
    End Sub
End Class