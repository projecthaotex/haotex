﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_surat_jalan_penjualan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ts_baru = New System.Windows.Forms.ToolStripButton()
        Me.ts_ubah = New System.Windows.Forms.ToolStripButton()
        Me.ts_hapus = New System.Windows.Forms.ToolStripButton()
        Me.ts_perbarui = New System.Windows.Forms.ToolStripButton()
        Me.ts_print = New System.Windows.Forms.ToolStripButton()
        Me.dtp_akhir = New System.Windows.Forms.DateTimePicker()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.txt_cari_sj_penjualan = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_cari = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_cari_customer = New System.Windows.Forms.TextBox()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_sj_packing = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txt_cari_gudang_packing = New System.Windows.Forms.TextBox()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txt_jumlah_total_harga = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txt_total_gulung = New System.Windows.Forms.TextBox()
        Me.txt_total_yard = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_gulung_claim_jadi = New System.Windows.Forms.TextBox()
        Me.txt_jumlah_claim_Jadi = New System.Windows.Forms.TextBox()
        Me.txt_gulung_grade_b = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_jumlah_grade_b = New System.Windows.Forms.TextBox()
        Me.txt_gulung_grade_a = New System.Windows.Forms.TextBox()
        Me.txt_jumlah_grade_a = New System.Windows.Forms.TextBox()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ts_baru, Me.ts_ubah, Me.ts_hapus, Me.ts_perbarui, Me.ts_print})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1305, 25)
        Me.ToolStrip1.TabIndex = 35
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ts_baru
        '
        Me.ts_baru.Image = Global.HAOTEX.My.Resources.Resources.application_add
        Me.ts_baru.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_baru.Name = "ts_baru"
        Me.ts_baru.Size = New System.Drawing.Size(51, 22)
        Me.ts_baru.Text = "Baru"
        '
        'ts_ubah
        '
        Me.ts_ubah.Image = Global.HAOTEX.My.Resources.Resources.application_edit
        Me.ts_ubah.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_ubah.Name = "ts_ubah"
        Me.ts_ubah.Size = New System.Drawing.Size(55, 22)
        Me.ts_ubah.Text = "Ubah"
        '
        'ts_hapus
        '
        Me.ts_hapus.Image = Global.HAOTEX.My.Resources.Resources.application_remove
        Me.ts_hapus.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_hapus.Name = "ts_hapus"
        Me.ts_hapus.Size = New System.Drawing.Size(61, 22)
        Me.ts_hapus.Text = "Hapus"
        '
        'ts_perbarui
        '
        Me.ts_perbarui.Image = Global.HAOTEX.My.Resources.Resources.application_search
        Me.ts_perbarui.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_perbarui.Name = "ts_perbarui"
        Me.ts_perbarui.Size = New System.Drawing.Size(71, 22)
        Me.ts_perbarui.Text = "Perbarui"
        '
        'ts_print
        '
        Me.ts_print.Image = Global.HAOTEX.My.Resources.Resources.printer
        Me.ts_print.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ts_print.Name = "ts_print"
        Me.ts_print.Size = New System.Drawing.Size(52, 22)
        Me.ts_print.Text = "Print"
        '
        'dtp_akhir
        '
        Me.dtp_akhir.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_akhir.Location = New System.Drawing.Point(45, 56)
        Me.dtp_akhir.Name = "dtp_akhir"
        Me.dtp_akhir.Size = New System.Drawing.Size(99, 20)
        Me.dtp_akhir.TabIndex = 5
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(45, 29)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 4
        '
        'txt_cari_sj_penjualan
        '
        Me.txt_cari_sj_penjualan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_sj_penjualan.Location = New System.Drawing.Point(13, 126)
        Me.txt_cari_sj_penjualan.Name = "txt_cari_sj_penjualan"
        Me.txt_cari_sj_penjualan.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_sj_penjualan.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_cari)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(12, 72)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(200, 456)
        Me.Panel1.TabIndex = 37
        '
        'btn_cari
        '
        Me.btn_cari.BackColor = System.Drawing.SystemColors.Control
        Me.btn_cari.Image = Global.HAOTEX.My.Resources.Resources.search
        Me.btn_cari.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btn_cari.Location = New System.Drawing.Point(62, 388)
        Me.btn_cari.Name = "btn_cari"
        Me.btn_cari.Size = New System.Drawing.Size(75, 43)
        Me.btn_cari.TabIndex = 49
        Me.btn_cari.Text = "CARI"
        Me.btn_cari.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btn_cari.UseVisualStyleBackColor = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.dtp_akhir)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.dtp_awal)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.txt_cari_sj_penjualan)
        Me.Panel2.Controls.Add(Me.txt_cari_customer)
        Me.Panel2.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel2.Controls.Add(Me.txt_cari_sj_packing)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.txt_cari_gudang_packing)
        Me.Panel2.Location = New System.Drawing.Point(1, 23)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(196, 350)
        Me.Panel2.TabIndex = 50
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(13, 293)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(51, 13)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Customer"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(13, 247)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(55, 13)
        Me.Label11.TabIndex = 17
        Me.Label11.Text = "Jenis Kain"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 201)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Gudang Packing"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(13, 155)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(102, 13)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Surat Jalan Packing"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(13, 109)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(110, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Surat Jalan Penjualan"
        '
        'txt_cari_customer
        '
        Me.txt_cari_customer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_customer.Location = New System.Drawing.Point(13, 310)
        Me.txt_cari_customer.MaxLength = 100
        Me.txt_cari_customer.Name = "txt_cari_customer"
        Me.txt_cari_customer.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_customer.TabIndex = 13
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(13, 264)
        Me.txt_cari_jenis_kain.MaxLength = 100
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 12
        '
        'txt_cari_sj_packing
        '
        Me.txt_cari_sj_packing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_sj_packing.Location = New System.Drawing.Point(13, 172)
        Me.txt_cari_sj_packing.Name = "txt_cari_sj_packing"
        Me.txt_cari_sj_packing.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_sj_packing.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 60)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(23, 13)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "s/d"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 33)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(26, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Dari"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(13, 6)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(53, 13)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Tanggal"
        '
        'txt_cari_gudang_packing
        '
        Me.txt_cari_gudang_packing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_gudang_packing.Location = New System.Drawing.Point(13, 218)
        Me.txt_cari_gudang_packing.MaxLength = 100
        Me.txt_cari_gudang_packing.Name = "txt_cari_gudang_packing"
        Me.txt_cari_gudang_packing.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_gudang_packing.TabIndex = 19
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(214, 72)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(1079, 456)
        Me.dgv1.TabIndex = 36
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label7.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Window
        Me.Label7.Location = New System.Drawing.Point(12, 36)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(1281, 34)
        Me.Label7.TabIndex = 48
        Me.Label7.Text = "SURAT JALAN PENJUALAN"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label18)
        Me.Panel3.Controls.Add(Me.txt_jumlah_total_harga)
        Me.Panel3.Controls.Add(Me.Label16)
        Me.Panel3.Controls.Add(Me.Label17)
        Me.Panel3.Controls.Add(Me.txt_total_gulung)
        Me.Panel3.Controls.Add(Me.txt_total_yard)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.Label14)
        Me.Panel3.Controls.Add(Me.Label15)
        Me.Panel3.Controls.Add(Me.txt_gulung_claim_jadi)
        Me.Panel3.Controls.Add(Me.txt_jumlah_claim_Jadi)
        Me.Panel3.Controls.Add(Me.txt_gulung_grade_b)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txt_jumlah_grade_b)
        Me.Panel3.Controls.Add(Me.txt_gulung_grade_a)
        Me.Panel3.Controls.Add(Me.txt_jumlah_grade_a)
        Me.Panel3.Location = New System.Drawing.Point(12, 530)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1281, 64)
        Me.Panel3.TabIndex = 49
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(1110, 13)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(99, 13)
        Me.Label18.TabIndex = 33
        Me.Label18.Text = "Jumlah Total Harga"
        '
        'txt_jumlah_total_harga
        '
        Me.txt_jumlah_total_harga.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jumlah_total_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_total_harga.Location = New System.Drawing.Point(1053, 29)
        Me.txt_jumlah_total_harga.Name = "txt_jumlah_total_harga"
        Me.txt_jumlah_total_harga.ReadOnly = True
        Me.txt_jumlah_total_harga.Size = New System.Drawing.Size(212, 20)
        Me.txt_jumlah_total_harga.TabIndex = 32
        Me.txt_jumlah_total_harga.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(976, 13)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(68, 13)
        Me.Label16.TabIndex = 31
        Me.Label16.Text = "Total Gulung"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(864, 13)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(56, 13)
        Me.Label17.TabIndex = 30
        Me.Label17.Text = "Total Yard"
        '
        'txt_total_gulung
        '
        Me.txt_total_gulung.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_gulung.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_gulung.Location = New System.Drawing.Point(971, 29)
        Me.txt_total_gulung.Name = "txt_total_gulung"
        Me.txt_total_gulung.ReadOnly = True
        Me.txt_total_gulung.Size = New System.Drawing.Size(78, 20)
        Me.txt_total_gulung.TabIndex = 29
        Me.txt_total_gulung.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_total_yard
        '
        Me.txt_total_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_yard.Location = New System.Drawing.Point(817, 29)
        Me.txt_total_yard.Name = "txt_total_yard"
        Me.txt_total_yard.ReadOnly = True
        Me.txt_total_yard.Size = New System.Drawing.Size(150, 20)
        Me.txt_total_yard.TabIndex = 28
        Me.txt_total_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(709, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 13)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Gulung"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(569, 13)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(81, 13)
        Me.Label14.TabIndex = 26
        Me.Label14.Text = "Total Claim Jadi"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(448, 13)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(41, 13)
        Me.Label15.TabIndex = 25
        Me.Label15.Text = "Gulung"
        '
        'txt_gulung_claim_jadi
        '
        Me.txt_gulung_claim_jadi.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gulung_claim_jadi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_claim_jadi.Location = New System.Drawing.Point(690, 29)
        Me.txt_gulung_claim_jadi.Name = "txt_gulung_claim_jadi"
        Me.txt_gulung_claim_jadi.ReadOnly = True
        Me.txt_gulung_claim_jadi.Size = New System.Drawing.Size(78, 20)
        Me.txt_gulung_claim_jadi.TabIndex = 24
        Me.txt_gulung_claim_jadi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jumlah_claim_Jadi
        '
        Me.txt_jumlah_claim_Jadi.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jumlah_claim_Jadi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_claim_Jadi.Location = New System.Drawing.Point(536, 29)
        Me.txt_jumlah_claim_Jadi.Name = "txt_jumlah_claim_Jadi"
        Me.txt_jumlah_claim_Jadi.ReadOnly = True
        Me.txt_jumlah_claim_Jadi.Size = New System.Drawing.Size(150, 20)
        Me.txt_jumlah_claim_Jadi.TabIndex = 23
        Me.txt_jumlah_claim_Jadi.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_gulung_grade_b
        '
        Me.txt_gulung_grade_b.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gulung_grade_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_grade_b.Location = New System.Drawing.Point(429, 29)
        Me.txt_gulung_grade_b.Name = "txt_gulung_grade_b"
        Me.txt_gulung_grade_b.ReadOnly = True
        Me.txt_gulung_grade_b.Size = New System.Drawing.Size(78, 20)
        Me.txt_gulung_grade_b.TabIndex = 22
        Me.txt_gulung_grade_b.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(314, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Total Grade B"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(187, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 13)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Gulung"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(53, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 13)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Total Grade A"
        '
        'txt_jumlah_grade_b
        '
        Me.txt_jumlah_grade_b.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jumlah_grade_b.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_grade_b.Location = New System.Drawing.Point(275, 29)
        Me.txt_jumlah_grade_b.Name = "txt_jumlah_grade_b"
        Me.txt_jumlah_grade_b.ReadOnly = True
        Me.txt_jumlah_grade_b.Size = New System.Drawing.Size(150, 20)
        Me.txt_jumlah_grade_b.TabIndex = 17
        Me.txt_jumlah_grade_b.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_gulung_grade_a
        '
        Me.txt_gulung_grade_a.BackColor = System.Drawing.SystemColors.Window
        Me.txt_gulung_grade_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_grade_a.Location = New System.Drawing.Point(168, 29)
        Me.txt_gulung_grade_a.Name = "txt_gulung_grade_a"
        Me.txt_gulung_grade_a.ReadOnly = True
        Me.txt_gulung_grade_a.Size = New System.Drawing.Size(78, 20)
        Me.txt_gulung_grade_a.TabIndex = 16
        Me.txt_gulung_grade_a.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_jumlah_grade_a
        '
        Me.txt_jumlah_grade_a.BackColor = System.Drawing.SystemColors.Window
        Me.txt_jumlah_grade_a.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jumlah_grade_a.Location = New System.Drawing.Point(14, 29)
        Me.txt_jumlah_grade_a.Name = "txt_jumlah_grade_a"
        Me.txt_jumlah_grade_a.ReadOnly = True
        Me.txt_jumlah_grade_a.Size = New System.Drawing.Size(150, 20)
        Me.txt_jumlah_grade_a.TabIndex = 15
        Me.txt_jumlah_grade_a.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1177, 4)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(104, 20)
        Me.dtp_hari_ini.TabIndex = 40
        '
        'form_surat_jalan_penjualan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1305, 606)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.Label7)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_surat_jalan_penjualan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ts_baru As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_ubah As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_hapus As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_perbarui As System.Windows.Forms.ToolStripButton
    Friend WithEvents ts_print As System.Windows.Forms.ToolStripButton
    Friend WithEvents dtp_akhir As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_cari_sj_penjualan As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btn_cari As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_cari_customer As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txt_jumlah_grade_b As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_grade_a As System.Windows.Forms.TextBox
    Friend WithEvents txt_jumlah_grade_a As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txt_jumlah_total_harga As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txt_total_gulung As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_yard As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txt_gulung_claim_jadi As System.Windows.Forms.TextBox
    Friend WithEvents txt_jumlah_claim_Jadi As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_grade_b As System.Windows.Forms.TextBox
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_cari_sj_packing As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_gudang_packing As System.Windows.Forms.TextBox
End Class
