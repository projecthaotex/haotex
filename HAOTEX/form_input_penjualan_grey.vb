﻿Imports MySql.Data.MySqlClient

Public Class form_input_penjualan_grey

    Private Sub form_input_penjualan_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isikodejual()
        Call isi_dtp_jatuh_tempo()
        txt_id_grey.ReadOnly = True
        txt_id_jual.ReadOnly = True
    End Sub

    Private Sub bersih()
        Call isikodejual()
        Call isi_dtp_jatuh_tempo()
        dtp_tanggal.Text = Today
        txt_id_grey.Text = ""
        txt_surat_jalan.Text = ""
        txt_jenis_kain.Text = ""
        txt_customer.Text = ""
        txt_harga.Text = ""
        txt_qty.Text = ""
        txt_jumlah.Text = ""
        txt_penyimpanan.Text = ""
        txt_keterangan.Text = ""
        cb_stok.Text = "--Pilih Stok--"
        txt_stok_tersedia.Text = ""
    End Sub

    Private Sub isi_dtp_jatuh_tempo()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(+1)
        dtp_jatuh_tempo.Text = tanggal
    End Sub

    Private Sub isikodejual()
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Jual FROM tbpenjualangrey WHERE Id_Jual in(select max(Id_Jual) from tbpenjualangrey)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        txt_id_jual.Text = Val(drx.Item("Id_Jual")) + 1
                    Else
                        txt_id_jual.Text = "1"
                    End If
                End Using
            End Using
        End Using
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        Me.Close()
    End Sub

    Private Sub txt_customer_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_customer.GotFocus
        If Label1.Text = "Input Penjualan Grey Baru" Then
            form_input_customer.MdiParent = form_menu_utama
            form_input_customer.Show()
            form_input_customer.TxtForm.Text = "form_input_penjualan_grey"
            form_input_customer.Focus()
        End If
    End Sub

    Private Sub cb_stok_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_stok.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub

    Private Sub txt_penyimpanan_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_penyimpanan.GotFocus, txt_jenis_kain.GotFocus
        If Label1.Text = "Input Penjualan Grey Baru" Then
            If cb_stok.Text = "--Pilih Stok--" Then
                cb_stok.Focus()
                MsgBox("STOK Belum Dipilih")
            ElseIf cb_stok.Text = "Proses" Then
                form_stok_proses_grey.MdiParent = form_menu_utama
                form_stok_proses_grey.Show()
                form_stok_proses_grey.txt_form.Text = "form_input_penjualan_grey"
                form_stok_proses_grey.Focus()
            End If
        End If
    End Sub

    Private Sub cb_stok_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_stok.TextChanged
        If Label1.Text = "Input Penjualan Grey Baru" Then
            txt_id_grey.Text = ""
            txt_penyimpanan.Text = ""
            txt_jenis_kain.Text = ""
            txt_stok_tersedia.Text = ""
            txt_qty.Text = ""
            txt_harga.Text = ""
            txt_jumlah.Text = ""
            txt_asal_sj.Text = ""
        End If
    End Sub

    Private Sub txt_qty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_qty.KeyPress
        If txt_qty.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_qty.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub

    Private Sub txt_harga_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga.KeyPress
        If txt_harga.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_harga.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub

    Private Sub txt_jumlah_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_jumlah.KeyPress
        If txt_jumlah.Text.Contains(",") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        ElseIf txt_jumlah.Text.Contains(".") Then
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
        Else
            If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13) Or e.KeyChar = ",") Then e.Handled = True
        End If
    End Sub

    Private Sub txt_qty_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_qty.TextChanged
        If Label1.Text = "Input Penjualan Grey Baru" Then
            If Val(txt_qty.Text.Replace(",", ".")) > Val(txt_stok_tersedia.Text.Replace(",", ".")) Then
                txt_qty.Text = txt_stok_tersedia.Text
            End If
        End If

        If Val(txt_harga.Text.Replace(",", ".")) = 0 Then
            txt_jumlah.Text = ""
        Else
            txt_jumlah.Text = Val(txt_qty.Text.Replace(",", ".")) * Val(txt_harga.Text.Replace(",", "."))
        End If
    End Sub

    Private Sub txt_harga_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_harga.TextChanged
        If Val(txt_qty.Text.Replace(",", ".")) = 0 Then
            txt_jumlah.Text = "0"
        Else
            txt_jumlah.Text = Val(txt_qty.Text.Replace(",", ".")) * Val(txt_harga.Text.Replace(",", "."))
        End If
    End Sub

    Private Sub simpan_tbjualgrey()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpenjualangrey (Id_Grey,Id_Jual,Tanggal,Jatuh_Tempo,SJ,Jenis_Kain,Customer,Harga,QTY,Jumlah,Gudang,Stok,Asal_SJ,Keterangan,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (txt_id_grey.Text))
                    .Parameters.AddWithValue("@1", (txt_id_jual.Text))
                    .Parameters.AddWithValue("@2", (dtp_tanggal.Text))
                    .Parameters.AddWithValue("@3", (dtp_jatuh_tempo.Text))
                    .Parameters.AddWithValue("@4", (txt_surat_jalan.Text))
                    .Parameters.AddWithValue("@5", (txt_jenis_kain.Text))
                    .Parameters.AddWithValue("@6", (txt_customer.Text))
                    .Parameters.AddWithValue("@7", (txt_harga.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@8", (txt_qty.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@9", (txt_jumlah.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@10", (txt_penyimpanan.Text))
                    .Parameters.AddWithValue("@11", (cb_stok.Text))
                    .Parameters.AddWithValue("@12", (txt_asal_sj.Text))
                    .Parameters.AddWithValue("@13", (txt_keterangan.Text))
                    .Parameters.AddWithValue("@14", (""))
                    .Parameters.AddWithValue("@15", (""))
                    .Parameters.AddWithValue("@16", (""))
                    .Parameters.AddWithValue("@17", (0))
                    .Parameters.AddWithValue("@18", (0))
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
            End Using
        End Using
    End Sub

    Private Sub simpan_tbpiutang()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbpiutang (Nama,Sisa_Piutang,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", (txt_customer.Text))
                    .Parameters.AddWithValue("@1", (txt_jumlah.Text.Replace(",", ".")))
                    .Parameters.AddWithValue("@2", (""))
                    .Parameters.AddWithValue("@3", (""))
                    .Parameters.AddWithValue("@4", (0))
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Sub btn_simpan_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_baru.Click
        Try
            If Label1.Text = "Input Penjualan Grey Baru" Then
                If txt_surat_jalan.Text = "" Then
                    MsgBox("SURAT JALAN tidak boleh kosong")
                    txt_surat_jalan.Focus()
                ElseIf txt_customer.Text = "" Then
                    MsgBox("CUSTOMER tidak boleh kosong")
                    txt_customer.Focus()
                ElseIf cb_stok.Text = "--Pilih Stok--" Then
                    MsgBox("STOK belum dipilih")
                    cb_stok.Focus()
                ElseIf txt_penyimpanan.Text = "" Then
                    MsgBox("GUDANG tidak boleh kosong")
                    txt_penyimpanan.Focus()
                ElseIf txt_jenis_kain.Text = "" Then
                    MsgBox("JENIS KAIN tidak boleh kosong")
                    txt_jenis_kain.Focus()
                ElseIf txt_qty.Text = "" Then
                    MsgBox("QUANTITY tidak boleh kosong")
                    txt_qty.Focus()
                ElseIf txt_harga.Text = "" Then
                    MsgBox("HARGA tidak boleh kosong")
                    txt_harga.Focus()
                Else
                    Call simpan_tbjualgrey()

                    If cb_stok.Text = "Titipan" Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Stok FROM tbstoktitipangrey WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbstoktitipangrey SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s - Val(txt_qty.Text.Replace(",", "."))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using
                    ElseIf cb_stok.Text = "Proses" Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s - Val(txt_qty.Text.Replace(",", "."))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using
                    ElseIf cb_stok.Text = "Ex Retur Celup" Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Stok FROM tbstokexreturcelup WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbstokexreturcelup SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s - Val(txt_qty.Text.Replace(",", "."))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using
                    End If

                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Sisa_Piutang FROM tbpiutang WHERE Nama='" & txt_customer.Text & "'"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using drx As MySqlDataReader = cmdx.ExecuteReader
                                drx.Read()
                                If drx.HasRows Then
                                    Dim h As Double
                                    h = drx.Item(0)
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "UPDATE tbpiutang SET Sisa_Piutang=@1 WHERE Nama='" & txt_customer.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            With cmdy
                                                .Parameters.Clear()
                                                .Parameters.AddWithValue("@1", (h + Val(txt_jumlah.Text.Replace(",", "."))))
                                                .ExecuteNonQuery()
                                            End With
                                        End Using
                                    End Using
                                Else
                                    Call simpan_tbpiutang()
                                End If
                            End Using
                        End Using
                    End Using

                    MsgBox("PENJUALAN GREY Baru Berhasil Disimpan")
                    form_penjualan_grey.ts_perbarui.PerformClick()
                    Call bersih()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_simpan_tutup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan_tutup.Click
        Try
            If txt_surat_jalan.Text = "" Then
                MsgBox("SURAT JALAN tidak boleh kosong")
                txt_surat_jalan.Focus()
            ElseIf txt_customer.Text = "" Then
                MsgBox("CUSTOMER tidak boleh kosong")
                txt_customer.Focus()
            ElseIf cb_stok.Text = "--Pilih Stok--" Then
                MsgBox("STOK belum dipilih")
                cb_stok.Focus()
            ElseIf txt_penyimpanan.Text = "" Then
                MsgBox("GUDANG tidak boleh kosong")
                txt_penyimpanan.Focus()
            ElseIf txt_jenis_kain.Text = "" Then
                MsgBox("JENIS KAIN tidak boleh kosong")
                txt_jenis_kain.Focus()
            ElseIf txt_qty.Text = "" Then
                MsgBox("QUANTITY tidak boleh kosong")
                txt_qty.Focus()
            ElseIf txt_harga.Text = "" Then
                MsgBox("HARGA tidak boleh kosong")
                txt_harga.Focus()
            Else
                If Label1.Text = "Input Penjualan Grey Baru" Then
                    Call simpan_tbjualgrey()

                    If cb_stok.Text = "Titipan" Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Stok FROM tbstoktitipangrey WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbstoktitipangrey SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s - Val(txt_qty.Text.Replace(",", "."))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using

                    ElseIf cb_stok.Text = "Proses" Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Stok FROM tbstokprosesgrey WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s - Val(txt_qty.Text.Replace(",", "."))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using

                    ElseIf cb_stok.Text = "Ex Retur Celup" Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Stok FROM tbstokexreturcelup WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbstokexreturcelup SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "' AND SJ='" & txt_asal_sj.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s - Val(txt_qty.Text.Replace(",", "."))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using
                    End If

                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx As String = "SELECT Sisa_Piutang FROM tbpiutang WHERE Nama='" & txt_customer.Text & "'"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using drx As MySqlDataReader = cmdx.ExecuteReader
                                drx.Read()
                                If drx.HasRows Then
                                    Dim h As Double
                                    h = drx.Item(0)
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "UPDATE tbpiutang SET Sisa_Piutang=@1 WHERE Nama='" & txt_customer.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            With cmdy
                                                .Parameters.Clear()
                                                .Parameters.AddWithValue("@1", (h + Val(txt_jumlah.Text.Replace(",", "."))))
                                                .ExecuteNonQuery()
                                            End With
                                        End Using
                                    End Using
                                Else
                                    Call simpan_tbpiutang()
                                End If
                            End Using
                        End Using
                    End Using
                    MsgBox("PENJUALAN GREY Baru Berhasil Disimpan")
                    form_penjualan_grey.ts_perbarui.PerformClick()
                    Me.Close()

                ElseIf Label1.Text = "Ubah Penjualan Grey" Then
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "UPDATE tbpenjualangrey SET Tanggal=@2,Jatuh_Tempo=@3,SJ=@4,Harga=@7,QTY=@8,Jumlah=@9,Keterangan=@10 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Id_Jual='" & txt_id_jual.Text & "'"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            With cmdy
                                dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                                .Parameters.Clear()
                                .Parameters.AddWithValue("@2", (dtp_tanggal.Text))
                                .Parameters.AddWithValue("@3", (dtp_jatuh_tempo.Text))
                                .Parameters.AddWithValue("@4", (txt_surat_jalan.Text))
                                .Parameters.AddWithValue("@7", (txt_harga.Text.Replace(",", ".")))
                                .Parameters.AddWithValue("@8", (txt_qty.Text.Replace(",", ".")))
                                .Parameters.AddWithValue("@9", (txt_jumlah.Text.Replace(",", ".")))
                                .Parameters.AddWithValue("@10", (txt_keterangan.Text))
                                .ExecuteNonQuery()
                            End With
                            dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                            dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                        End Using
                    End Using

                ElseIf cb_stok.Text = "Proses" Then
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Grey='" & txt_id_grey.Text & "' AND Gudang='" & txt_penyimpanan.Text & "' AND Jenis_Kain='" & txt_jenis_kain.Text & "'"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            With cmdy
                                .Parameters.Clear()
                                .Parameters.AddWithValue("@1", (Val(txt_stok_tersedia.Text.Replace(",", ".")) - Val(txt_qty.Text.Replace(",", "."))))
                                .ExecuteNonQuery()
                            End With
                        End Using
                    End Using

                    If Val(txt_jumlah.Text.Replace(",", ".")) > Val(txt_jumlah_awal.Text.Replace(",", ".")) Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Sisa_piutang FROM tbpiutang WHERE Nama='" & txt_customer.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbpiutang SET Sisa_Piutang=@1 WHERE Nama='" & txt_customer.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s + (Val(txt_jumlah.Text.Replace(",", ".")) - Val(txt_jumlah_awal.Text.Replace(",", ".")))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using

                    ElseIf Val(txt_jumlah.Text.Replace(",", ".")) < Val(txt_jumlah_awal.Text.Replace(",", ".")) Then
                        Using conx As New MySqlConnection(sLocalConn)
                            conx.Open()
                            Dim sqlx As String = "SELECT Sisa_piutang FROM tbpiutang WHERE Nama='" & txt_customer.Text & "'"
                            Using cmdx As New MySqlCommand(sqlx, conx)
                                Using drx As MySqlDataReader = cmdx.ExecuteReader
                                    drx.Read()
                                    If drx.HasRows Then
                                        Dim s As Double
                                        s = drx.Item(0)
                                        Using cony As New MySqlConnection(sLocalConn)
                                            cony.Open()
                                            Dim sqly = "UPDATE tbpiutang SET Sisa_Piutang=@1 WHERE Nama='" & txt_customer.Text & "'"
                                            Using cmdy As New MySqlCommand(sqly, cony)
                                                With cmdy
                                                    .Parameters.Clear()
                                                    .Parameters.AddWithValue("@1", (s - (Val(txt_jumlah_awal.Text.Replace(",", ".")) - Val(txt_jumlah.Text.Replace(",", ".")))))
                                                    .ExecuteNonQuery()
                                                End With
                                            End Using
                                        End Using
                                    End If
                                End Using
                            End Using
                        End Using
                    End If

                    MsgBox("PENJUALAN GREY Berhasil Diubah")
                    form_penjualan_grey.ts_perbarui.PerformClick()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class