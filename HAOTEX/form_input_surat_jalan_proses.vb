﻿Imports MySql.Data.MySqlClient

Public Class form_input_surat_jalan_proses

    Private Sub form_input_surat_jalan_proses_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Panel_2.Visible = False
        Panel_3.Visible = False
        Panel_4.Visible = False
        Panel_5.Visible = False
        Panel_6.Visible = False
        Panel_7.Visible = False
        Panel_8.Visible = False
        Panel_9.Visible = False
        Panel_10.Visible = False
        btn_hapus_2.Visible = False
        btn_hapus_3.Visible = False
        btn_hapus_4.Visible = False
        btn_hapus_5.Visible = False
        btn_hapus_6.Visible = False
        btn_hapus_7.Visible = False
        btn_hapus_8.Visible = False
        btn_hapus_9.Visible = False
        btn_hapus_10.Visible = False
        btn_tambah_baris_2.Visible = False
        btn_tambah_baris_3.Visible = False
        btn_tambah_baris_4.Visible = False
        btn_tambah_baris_5.Visible = False
        btn_tambah_baris_6.Visible = False
        btn_tambah_baris_7.Visible = False
        btn_tambah_baris_8.Visible = False
        btn_tambah_baris_9.Visible = False
        btn_selesai_2.Visible = False
        btn_selesai_3.Visible = False
        btn_selesai_4.Visible = False
        btn_selesai_5.Visible = False
        btn_selesai_6.Visible = False
        btn_selesai_7.Visible = False
        btn_selesai_8.Visible = False
        btn_selesai_9.Visible = False
        btn_selesai_10.Visible = False
        btn_batal_2.Visible = False
        btn_batal_3.Visible = False
        btn_batal_4.Visible = False
        btn_batal_5.Visible = False
        btn_batal_6.Visible = False
        btn_batal_7.Visible = False
        btn_batal_8.Visible = False
        btn_batal_9.Visible = False
        btn_batal_10.Visible = False
    End Sub
    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        If Not e.KeyChar = Chr(13) Then e.Handled = True
    End Sub
    Private Sub ComboBox1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.TextChanged
        Dim tanggal As DateTime
        tanggal = dtp_tanggal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub dtp_tanggal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_tanggal.ValueChanged
        Dim tanggal As DateTime
        tanggal = dtp_tanggal.Value
        tanggal = tanggal.AddDays(ComboBox1.Text)
        dtp_jatuh_tempo.Text = tanggal
    End Sub
    Private Sub txt_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang.GotFocus
        If txt_gudang.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_1.GotFocus
        If txt_gudang_packing_1.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_1"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_2.GotFocus
        If txt_gudang_packing_2.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_2"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_3.GotFocus
        If txt_gudang_packing_3.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_3"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_4.GotFocus
        If txt_gudang_packing_4.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_4"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_5.GotFocus
        If txt_gudang_packing_5.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_5"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_6.GotFocus
        If txt_gudang_packing_6.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_6"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_7.GotFocus
        If txt_gudang_packing_7.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_7"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_8.GotFocus
        If txt_gudang_packing_8.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_8"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_9.GotFocus
        If txt_gudang_packing_9.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_9"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_gudang_packing_10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_gudang_packing_10.GotFocus
        If txt_gudang_packing_10.ReadOnly = True Then
        Else
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses_gudang_packing_10"
            form_input_gudang.Focus()
        End If
    End Sub
    Private Sub txt_no_po_1_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_1.GotFocus, _
        txt_jenis_kain_1.GotFocus, txt_warna_1.GotFocus, txt_resep_1.GotFocus
        If txt_gudang.Text = "" Then
            form_input_gudang.MdiParent = form_menu_utama
            form_input_gudang.Show()
            form_input_gudang.TxtForm.Text = "form_input_surat_jalan_proses"
            form_input_gudang.Focus()
        Else
            If txt_no_po_1.ReadOnly = True Then

            Else
                form_stok_wip_proses.MdiParent = form_menu_utama
                form_stok_wip_proses.Show()
                form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_1"
                form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
                form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
                form_stok_wip_proses.Focus()
            End If
        End If
    End Sub
    Private Sub txt_no_po_2_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_2.GotFocus, _
        txt_jenis_kain_2.GotFocus, txt_warna_2.GotFocus, txt_resep_2.GotFocus
        If txt_no_po_2.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_2"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_3_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_3.GotFocus, _
        txt_jenis_kain_3.GotFocus, txt_warna_3.GotFocus, txt_resep_3.GotFocus
        If txt_no_po_3.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_3"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_4_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_4.GotFocus, _
        txt_jenis_kain_4.GotFocus, txt_warna_4.GotFocus, txt_resep_4.GotFocus
        If txt_no_po_4.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_4"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_5_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_5.GotFocus, _
        txt_jenis_kain_5.GotFocus, txt_warna_5.GotFocus, txt_resep_5.GotFocus
        If txt_no_po_5.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_5"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_6_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_6.GotFocus, _
        txt_jenis_kain_6.GotFocus, txt_warna_6.GotFocus, txt_resep_6.GotFocus
        If txt_no_po_6.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_6"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_7_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_7.GotFocus, _
        txt_jenis_kain_7.GotFocus, txt_warna_7.GotFocus, txt_resep_7.GotFocus
        If txt_no_po_7.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_7"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_8_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_8.GotFocus, _
        txt_jenis_kain_8.GotFocus, txt_warna_8.GotFocus, txt_resep_8.GotFocus
        If txt_no_po_8.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_8"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_9_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_9.GotFocus, _
        txt_jenis_kain_9.GotFocus, txt_warna_9.GotFocus, txt_resep_9.GotFocus
        If txt_no_po_9.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_9"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub
    Private Sub txt_no_po_10_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_no_po_10.GotFocus, _
        txt_jenis_kain_10.GotFocus, txt_warna_10.GotFocus, txt_resep_10.GotFocus
        If txt_no_po_10.ReadOnly = True Then

        Else
            form_stok_wip_proses.MdiParent = form_menu_utama
            form_stok_wip_proses.Show()
            form_stok_wip_proses.txt_form.Text = "form_input_surat_jalan_proses_baris_10"
            form_stok_wip_proses.txt_cari_gudang.Text = txt_gudang.Text
            form_stok_wip_proses.txt_cari_gudang.ReadOnly = True
            form_stok_wip_proses.Focus()
        End If
    End Sub

    Private Sub btn_hapus_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_2.Click
        txt_no_po_2.Text = ""
        txt_jenis_kain_2.Text = ""
        txt_warna_2.Text = ""
        txt_resep_2.Text = ""
        txt_partai_2.Text = ""
        txt_gulung_2.Text = ""
        txt_meter_2.Text = ""
        txt_kiloan_2.Text = ""
        txt_gramasi_2.Text = ""
        txt_harga_proses_2.Text = ""
        txt_total_harga_2.Text = ""
        txt_keterangan_2.Text = ""
        txt_qty_asal_2.Text = ""
        txt_id_grey_2.Text = ""
        txt_sisa_qty_2.Text = ""
        txt_asal_satuan_2.Text = ""
        txt_id_hutang_2.Text = ""
        txt_asal_satuan_rubah_2.Text = ""
        txt_asal_satuan_wip_2.Text = ""
        txt_harga_2.Text = ""
        txt_id_sj_proses_2.Text = ""
        txt_id_po_proses_2.Text = ""
        txt_id_beli_2.Text = ""
    End Sub
    Private Sub btn_hapus_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_3.Click
        txt_no_po_3.Text = ""
        txt_jenis_kain_3.Text = ""
        txt_warna_3.Text = ""
        txt_resep_3.Text = ""
        txt_partai_3.Text = ""
        txt_gulung_3.Text = ""
        txt_meter_3.Text = ""
        txt_kiloan_3.Text = ""
        txt_gramasi_3.Text = ""
        txt_harga_proses_3.Text = ""
        txt_total_harga_3.Text = ""
        txt_keterangan_3.Text = ""
        txt_qty_asal_3.Text = ""
        txt_id_grey_3.Text = ""
        txt_sisa_qty_3.Text = ""
        txt_asal_satuan_3.Text = ""
        txt_id_hutang_3.Text = ""
        txt_asal_satuan_rubah_3.Text = ""
        txt_asal_satuan_wip_3.Text = ""
        txt_harga_3.Text = ""
        txt_id_sj_proses_3.Text = ""
        txt_id_po_proses_3.Text = ""
        txt_id_beli_3.Text = ""
    End Sub
    Private Sub btn_hapus_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_4.Click
        txt_no_po_4.Text = ""
        txt_jenis_kain_4.Text = ""
        txt_warna_4.Text = ""
        txt_resep_4.Text = ""
        txt_partai_4.Text = ""
        txt_gulung_4.Text = ""
        txt_meter_4.Text = ""
        txt_kiloan_4.Text = ""
        txt_gramasi_4.Text = ""
        txt_harga_proses_4.Text = ""
        txt_total_harga_4.Text = ""
        txt_keterangan_4.Text = ""
        txt_qty_asal_4.Text = ""
        txt_id_grey_4.Text = ""
        txt_sisa_qty_4.Text = ""
        txt_asal_satuan_4.Text = ""
        txt_id_hutang_4.Text = ""
        txt_asal_satuan_rubah_4.Text = ""
        txt_asal_satuan_wip_4.Text = ""
        txt_harga_4.Text = ""
        txt_id_sj_proses_4.Text = ""
        txt_id_po_proses_4.Text = ""
        txt_id_beli_4.Text = ""
    End Sub
    Private Sub btn_hapus_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_5.Click
        txt_no_po_5.Text = ""
        txt_jenis_kain_5.Text = ""
        txt_warna_5.Text = ""
        txt_resep_5.Text = ""
        txt_partai_5.Text = ""
        txt_gulung_5.Text = ""
        txt_meter_5.Text = ""
        txt_kiloan_5.Text = ""
        txt_gramasi_5.Text = ""
        txt_harga_proses_5.Text = ""
        txt_total_harga_5.Text = ""
        txt_keterangan_5.Text = ""
        txt_qty_asal_5.Text = ""
        txt_id_grey_5.Text = ""
        txt_sisa_qty_5.Text = ""
        txt_asal_satuan_5.Text = ""
        txt_id_hutang_5.Text = ""
        txt_asal_satuan_rubah_5.Text = ""
        txt_asal_satuan_wip_5.Text = ""
        txt_harga_5.Text = ""
        txt_id_sj_proses_5.Text = ""
        txt_id_po_proses_5.Text = ""
        txt_id_beli_5.Text = ""
    End Sub
    Private Sub btn_hapus_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_6.Click
        txt_no_po_6.Text = ""
        txt_jenis_kain_6.Text = ""
        txt_warna_6.Text = ""
        txt_resep_6.Text = ""
        txt_partai_6.Text = ""
        txt_gulung_6.Text = ""
        txt_meter_6.Text = ""
        txt_kiloan_6.Text = ""
        txt_gramasi_6.Text = ""
        txt_harga_proses_6.Text = ""
        txt_total_harga_6.Text = ""
        txt_keterangan_6.Text = ""
        txt_qty_asal_6.Text = ""
        txt_id_grey_6.Text = ""
        txt_sisa_qty_6.Text = ""
        txt_asal_satuan_6.Text = ""
        txt_id_hutang_6.Text = ""
        txt_asal_satuan_rubah_6.Text = ""
        txt_asal_satuan_wip_6.Text = ""
        txt_harga_6.Text = ""
        txt_id_sj_proses_6.Text = ""
        txt_id_po_proses_6.Text = ""
        txt_id_beli_6.Text = ""
    End Sub
    Private Sub btn_hapus_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_7.Click
        txt_no_po_7.Text = ""
        txt_jenis_kain_7.Text = ""
        txt_warna_7.Text = ""
        txt_resep_7.Text = ""
        txt_partai_7.Text = ""
        txt_gulung_7.Text = ""
        txt_meter_7.Text = ""
        txt_kiloan_7.Text = ""
        txt_gramasi_7.Text = ""
        txt_harga_proses_7.Text = ""
        txt_total_harga_7.Text = ""
        txt_keterangan_7.Text = ""
        txt_qty_asal_7.Text = ""
        txt_id_grey_7.Text = ""
        txt_sisa_qty_7.Text = ""
        txt_asal_satuan_7.Text = ""
        txt_id_hutang_7.Text = ""
        txt_asal_satuan_rubah_7.Text = ""
        txt_asal_satuan_wip_7.Text = ""
        txt_harga_7.Text = ""
        txt_id_sj_proses_7.Text = ""
        txt_id_po_proses_7.Text = ""
        txt_id_beli_7.Text = ""
    End Sub
    Private Sub btn_hapus_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_8.Click
        txt_no_po_8.Text = ""
        txt_jenis_kain_8.Text = ""
        txt_warna_8.Text = ""
        txt_resep_8.Text = ""
        txt_partai_8.Text = ""
        txt_gulung_8.Text = ""
        txt_meter_8.Text = ""
        txt_kiloan_8.Text = ""
        txt_gramasi_8.Text = ""
        txt_harga_proses_8.Text = ""
        txt_total_harga_8.Text = ""
        txt_keterangan_8.Text = ""
        txt_qty_asal_8.Text = ""
        txt_id_grey_8.Text = ""
        txt_sisa_qty_8.Text = ""
        txt_asal_satuan_8.Text = ""
        txt_id_hutang_8.Text = ""
        txt_asal_satuan_rubah_8.Text = ""
        txt_asal_satuan_wip_8.Text = ""
        txt_harga_8.Text = ""
        txt_id_sj_proses_8.Text = ""
        txt_id_po_proses_8.Text = ""
        txt_id_beli_8.Text = ""
    End Sub
    Private Sub btn_hapus_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_9.Click
        txt_no_po_9.Text = ""
        txt_jenis_kain_9.Text = ""
        txt_warna_9.Text = ""
        txt_resep_9.Text = ""
        txt_partai_9.Text = ""
        txt_gulung_9.Text = ""
        txt_meter_9.Text = ""
        txt_kiloan_9.Text = ""
        txt_gramasi_9.Text = ""
        txt_harga_proses_9.Text = ""
        txt_total_harga_9.Text = ""
        txt_keterangan_9.Text = ""
        txt_qty_asal_9.Text = ""
        txt_id_grey_9.Text = ""
        txt_sisa_qty_9.Text = ""
        txt_asal_satuan_9.Text = ""
        txt_id_hutang_9.Text = ""
        txt_asal_satuan_rubah_9.Text = ""
        txt_asal_satuan_wip_9.Text = ""
        txt_harga_9.Text = ""
        txt_id_sj_proses_9.Text = ""
        txt_id_po_proses_9.Text = ""
        txt_id_beli_9.Text = ""
    End Sub
    Private Sub btn_hapus_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus_10.Click
        txt_no_po_10.Text = ""
        txt_jenis_kain_10.Text = ""
        txt_warna_10.Text = ""
        txt_resep_10.Text = ""
        txt_partai_10.Text = ""
        txt_gulung_10.Text = ""
        txt_meter_10.Text = ""
        txt_kiloan_10.Text = ""
        txt_gramasi_10.Text = ""
        txt_harga_proses_10.Text = ""
        txt_total_harga_10.Text = ""
        txt_keterangan_10.Text = ""
        txt_qty_asal_10.Text = ""
        txt_id_grey_10.Text = ""
        txt_sisa_qty_10.Text = ""
        txt_asal_satuan_10.Text = ""
        txt_id_hutang_10.Text = ""
        txt_asal_satuan_rubah_10.Text = ""
        txt_asal_satuan_wip_10.Text = ""
        txt_harga_10.Text = ""
        txt_id_sj_proses_10.Text = ""
        txt_id_po_proses_10.Text = ""
        txt_id_beli_10.Text = ""
    End Sub
   
    Private Sub txt_gulung_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_gulung_1.KeyPress, _
        txt_gulung_2.KeyPress, txt_gulung_3.KeyPress, txt_gulung_4.KeyPress, txt_gulung_5.KeyPress, txt_gulung_6.KeyPress, _
        txt_gulung_7.KeyPress, txt_gulung_8.KeyPress, txt_gulung_9.KeyPress, txt_gulung_10.KeyPress
        If Not ((e.KeyChar >= "0" And e.KeyChar <= "9") Or e.KeyChar = vbBack Or e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub txt_meter_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_1.Text = String.Empty Then
                        txt_meter_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_1.Select(txt_meter_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_1.Text = String.Empty Then
                        txt_meter_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_1.Select(txt_meter_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_2.Text = String.Empty Then
                        txt_meter_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_2.Select(txt_meter_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_2.Text = String.Empty Then
                        txt_meter_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_2.Select(txt_meter_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_3.Text = String.Empty Then
                        txt_meter_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_3.Select(txt_meter_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_3.Text = String.Empty Then
                        txt_meter_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_3.Select(txt_meter_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_4.Text = String.Empty Then
                        txt_meter_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_4.Select(txt_meter_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_4.Text = String.Empty Then
                        txt_meter_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_4.Select(txt_meter_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_5.Text = String.Empty Then
                        txt_meter_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_5.Select(txt_meter_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_5.Text = String.Empty Then
                        txt_meter_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_5.Select(txt_meter_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_6.Text = String.Empty Then
                        txt_meter_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_6.Select(txt_meter_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_6.Text = String.Empty Then
                        txt_meter_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_6.Select(txt_meter_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_7.Text = String.Empty Then
                        txt_meter_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_7.Select(txt_meter_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_7.Text = String.Empty Then
                        txt_meter_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_7.Select(txt_meter_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_8.Text = String.Empty Then
                        txt_meter_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_8.Select(txt_meter_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_8.Text = String.Empty Then
                        txt_meter_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_8.Select(txt_meter_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_9.Text = String.Empty Then
                        txt_meter_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_9.Select(txt_meter_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_9.Text = String.Empty Then
                        txt_meter_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_9.Select(txt_meter_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_meter_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_meter_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_meter_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_meter_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_meter_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_10.Text = String.Empty Then
                        txt_meter_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_10.Select(txt_meter_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_meter_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_meter_10.Text = String.Empty Then
                        txt_meter_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_meter_10.Select(txt_meter_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_1.Text = String.Empty Then
                        txt_kiloan_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_1.Select(txt_kiloan_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_1.Text = String.Empty Then
                        txt_kiloan_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_1.Select(txt_kiloan_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_2.Text = String.Empty Then
                        txt_kiloan_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_2.Select(txt_kiloan_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_2.Text = String.Empty Then
                        txt_kiloan_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_2.Select(txt_kiloan_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_3.Text = String.Empty Then
                        txt_kiloan_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_3.Select(txt_kiloan_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_3.Text = String.Empty Then
                        txt_kiloan_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_3.Select(txt_kiloan_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_4.Text = String.Empty Then
                        txt_kiloan_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_4.Select(txt_kiloan_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_4.Text = String.Empty Then
                        txt_kiloan_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_4.Select(txt_kiloan_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_5.Text = String.Empty Then
                        txt_kiloan_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_5.Select(txt_kiloan_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_5.Text = String.Empty Then
                        txt_kiloan_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_5.Select(txt_kiloan_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_6.Text = String.Empty Then
                        txt_kiloan_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_6.Select(txt_kiloan_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_6.Text = String.Empty Then
                        txt_kiloan_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_6.Select(txt_kiloan_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_7.Text = String.Empty Then
                        txt_kiloan_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_7.Select(txt_kiloan_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_7.Text = String.Empty Then
                        txt_kiloan_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_7.Select(txt_kiloan_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_8.Text = String.Empty Then
                        txt_kiloan_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_8.Select(txt_kiloan_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_8.Text = String.Empty Then
                        txt_kiloan_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_8.Select(txt_kiloan_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_9.Text = String.Empty Then
                        txt_kiloan_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_9.Select(txt_kiloan_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_9.Text = String.Empty Then
                        txt_kiloan_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_9.Select(txt_kiloan_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_kiloan_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_kiloan_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_kiloan_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_kiloan_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_10.Text = String.Empty Then
                        txt_kiloan_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_10.Select(txt_kiloan_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_kiloan_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_kiloan_10.Text = String.Empty Then
                        txt_kiloan_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_kiloan_10.Select(txt_kiloan_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_1.Text = String.Empty Then
                        txt_harga_proses_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_1.Select(txt_harga_proses_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_1.Text = String.Empty Then
                        txt_harga_proses_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_1.Select(txt_harga_proses_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_2.Text = String.Empty Then
                        txt_harga_proses_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_2.Select(txt_harga_proses_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_2.Text = String.Empty Then
                        txt_harga_proses_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_2.Select(txt_harga_proses_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_3.Text = String.Empty Then
                        txt_harga_proses_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_3.Select(txt_harga_proses_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_3.Text = String.Empty Then
                        txt_harga_proses_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_3.Select(txt_harga_proses_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_4.Text = String.Empty Then
                        txt_harga_proses_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_4.Select(txt_harga_proses_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_4.Text = String.Empty Then
                        txt_harga_proses_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_4.Select(txt_harga_proses_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_5.Text = String.Empty Then
                        txt_harga_proses_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_5.Select(txt_harga_proses_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_5.Text = String.Empty Then
                        txt_harga_proses_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_5.Select(txt_harga_proses_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_6.Text = String.Empty Then
                        txt_harga_proses_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_6.Select(txt_harga_proses_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_6.Text = String.Empty Then
                        txt_harga_proses_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_6.Select(txt_harga_proses_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_7.Text = String.Empty Then
                        txt_harga_proses_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_7.Select(txt_harga_proses_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_7.Text = String.Empty Then
                        txt_harga_proses_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_7.Select(txt_harga_proses_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_8.Text = String.Empty Then
                        txt_harga_proses_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_8.Select(txt_harga_proses_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_8.Text = String.Empty Then
                        txt_harga_proses_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_8.Select(txt_harga_proses_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_9.Text = String.Empty Then
                        txt_harga_proses_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_9.Select(txt_harga_proses_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_9.Text = String.Empty Then
                        txt_harga_proses_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_9.Select(txt_harga_proses_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_harga_proses_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_harga_proses_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_harga_proses_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_harga_proses_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_10.Text = String.Empty Then
                        txt_harga_proses_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_10.Select(txt_harga_proses_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_harga_proses_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_harga_proses_10.Text = String.Empty Then
                        txt_harga_proses_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_harga_proses_10.Select(txt_harga_proses_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_1.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_1.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        txt_total_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_1.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_1.Text = String.Empty Then
                        txt_total_harga_1.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_2.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_2.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        txt_total_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_2.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_2.Text = String.Empty Then
                        txt_total_harga_2.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_3.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_3.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        txt_total_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_3.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_3.Text = String.Empty Then
                        txt_total_harga_3.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_4.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_4.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        txt_total_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_4.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_4.Text = String.Empty Then
                        txt_total_harga_4.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_5_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_5.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_5.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        txt_total_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_5.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_5.Text = String.Empty Then
                        txt_total_harga_5.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_6_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_6.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_6.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        txt_total_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_6.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_6.Text = String.Empty Then
                        txt_total_harga_6.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_7_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_7.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_7.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        txt_total_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_7.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_7.Text = String.Empty Then
                        txt_total_harga_7.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_8_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_8.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_8.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        txt_total_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_8.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_8.Text = String.Empty Then
                        txt_total_harga_8.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_9_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_9.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_9.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_9.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_9.Text = String.Empty Then
                        txt_total_harga_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_9.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_9.Text = String.Empty Then
                        txt_total_harga_9.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub
    Private Sub txt_total_harga_10_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_total_harga_10.KeyPress
        Select Case e.KeyChar
            Case CChar(vbBack)
                e.Handled = False
            Case "0"c, "1"c, "2"c, "3"c, "4"c, "5"c, "6"c, "7"c, "8"c, "9"c
                e.Handled = False
            Case "-"c
                If txt_total_harga_10.Text.Contains("-"c) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_10.Text = String.Empty Then
                        e.Handled = False
                    Else
                        e.Handled = True
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                If txt_total_harga_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_10.Text = String.Empty Then
                        txt_total_harga_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
                    Else
                        e.Handled = False
                    End If
                End If
            Case CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator)
                If txt_total_harga_10.Text.Contains(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)) = True Then
                    e.Handled = True
                Else
                    If txt_total_harga_10.Text = String.Empty Then
                        txt_total_harga_10.Text = "0"c & CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                        e.Handled = True
                        txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
                    Else
                        e.KeyChar = CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator)
                    End If
                End If
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub txt_meter_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_1.TextChanged
        If txt_meter_1.Text <> String.Empty Then
            Dim temp As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_1.Select(txt_meter_1.Text.Length, 0)
            ElseIf txt_meter_1.Text = "-"c Then

            Else
                txt_meter_1.Text = CDec(temp).ToString("N0")
                txt_meter_1.Select(txt_meter_1.Text.Length, 0)
            End If
        End If
        txt_kiloan_1.Text = ""
        txt_gramasi_1.Text = ""
        txt_harga_proses_1.Text = ""
        txt_total_harga_1.Text = ""
        Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_1 As String = txt_qty_asal_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_1 As String = txt_sisa_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_1 As Double = Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 0.9144, 0)
        Dim b_1 As Double = Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Meter" Then
            If Val(meter_1.Replace(",", ".")) > (Val(qty_asal_1.Replace(",", ".")) + Val(sisa_qty_1.Replace(",", "."))) Then
                txt_meter_1.Text = Math.Round((Val(qty_asal_1.Replace(",", ".")) + Val(sisa_qty_1.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Yard" Then
            If Val(meter_1.Replace(",", ".")) > (Val(qty_asal_1.Replace(",", ".")) + Val(sisa_qty_1.Replace(",", "."))) Then
                txt_meter_1.Text = Math.Round((Val(qty_asal_1.Replace(",", ".")) + Val(sisa_qty_1.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Yard" Then
            If Val(meter_1.Replace(",", ".")) > (Val(qty_asal_1.Replace(",", ".")) + b_1) Then
                txt_meter_1.Text = Math.Round((Val(qty_asal_1.Replace(",", ".")) + b_1), 2)
            End If
        ElseIf txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Meter" Then
            If Val(meter_1.Replace(",", ".")) > (Val(qty_asal_1.Replace(",", ".")) + a_1) Then
                txt_meter_1.Text = Math.Round((Val(qty_asal_1.Replace(",", ".")) + a_1), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_2.TextChanged
        If txt_meter_2.Text <> String.Empty Then
            Dim temp As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_2.Select(txt_meter_2.Text.Length, 0)
            ElseIf txt_meter_2.Text = "-"c Then

            Else
                txt_meter_2.Text = CDec(temp).ToString("N0")
                txt_meter_2.Select(txt_meter_2.Text.Length, 0)
            End If
        End If
        txt_kiloan_2.Text = ""
        txt_gramasi_2.Text = ""
        txt_harga_proses_2.Text = ""
        txt_total_harga_2.Text = ""
        Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_2 As String = txt_qty_asal_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_2 As String = txt_sisa_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_2 As Double = Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 0.9144, 0)
        Dim b_2 As Double = Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Meter" Then
            If Val(meter_2.Replace(",", ".")) > (Val(qty_asal_2.Replace(",", ".")) + Val(sisa_qty_2.Replace(",", "."))) Then
                txt_meter_2.Text = Math.Round((Val(qty_asal_2.Replace(",", ".")) + Val(sisa_qty_2.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Yard" Then
            If Val(meter_2.Replace(",", ".")) > (Val(qty_asal_2.Replace(",", ".")) + Val(sisa_qty_2.Replace(",", "."))) Then
                txt_meter_2.Text = Math.Round((Val(qty_asal_2.Replace(",", ".")) + Val(sisa_qty_2.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Yard" Then
            If Val(meter_2.Replace(",", ".")) > (Val(qty_asal_2.Replace(",", ".")) + b_2) Then
                txt_meter_2.Text = Math.Round((Val(qty_asal_2.Replace(",", ".")) + b_2), 2)
            End If
        ElseIf txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Meter" Then
            If Val(meter_2.Replace(",", ".")) > (Val(qty_asal_2.Replace(",", ".")) + a_2) Then
                txt_meter_2.Text = Math.Round((Val(qty_asal_2.Replace(",", ".")) + a_2), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_3.TextChanged
        If txt_meter_3.Text <> String.Empty Then
            Dim temp As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_3.Select(txt_meter_3.Text.Length, 0)
            ElseIf txt_meter_3.Text = "-"c Then

            Else
                txt_meter_3.Text = CDec(temp).ToString("N0")
                txt_meter_3.Select(txt_meter_3.Text.Length, 0)
            End If
        End If
        txt_kiloan_3.Text = ""
        txt_gramasi_3.Text = ""
        txt_harga_proses_3.Text = ""
        txt_total_harga_3.Text = ""
        Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_3 As String = txt_qty_asal_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_3 As String = txt_sisa_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_3 As Double = Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 0.9144, 0)
        Dim b_3 As Double = Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Meter" Then
            If Val(meter_3.Replace(",", ".")) > (Val(qty_asal_3.Replace(",", ".")) + Val(sisa_qty_3.Replace(",", "."))) Then
                txt_meter_3.Text = Math.Round((Val(qty_asal_3.Replace(",", ".")) + Val(sisa_qty_3.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Yard" Then
            If Val(meter_3.Replace(",", ".")) > (Val(qty_asal_3.Replace(",", ".")) + Val(sisa_qty_3.Replace(",", "."))) Then
                txt_meter_3.Text = Math.Round((Val(qty_asal_3.Replace(",", ".")) + Val(sisa_qty_3.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Yard" Then
            If Val(meter_3.Replace(",", ".")) > (Val(qty_asal_3.Replace(",", ".")) + b_3) Then
                txt_meter_3.Text = Math.Round((Val(qty_asal_3.Replace(",", ".")) + b_3), 2)
            End If
        ElseIf txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Meter" Then
            If Val(meter_3.Replace(",", ".")) > (Val(qty_asal_3.Replace(",", ".")) + a_3) Then
                txt_meter_3.Text = Math.Round((Val(qty_asal_3.Replace(",", ".")) + a_3), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_4.TextChanged
        If txt_meter_4.Text <> String.Empty Then
            Dim temp As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_4.Select(txt_meter_4.Text.Length, 0)
            ElseIf txt_meter_4.Text = "-"c Then

            Else
                txt_meter_4.Text = CDec(temp).ToString("N0")
                txt_meter_4.Select(txt_meter_4.Text.Length, 0)
            End If
        End If
        txt_kiloan_4.Text = ""
        txt_gramasi_4.Text = ""
        txt_harga_proses_4.Text = ""
        txt_total_harga_4.Text = ""
        Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_4 As String = txt_qty_asal_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_4 As String = txt_sisa_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_4 As Double = Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 0.9144, 0)
        Dim b_4 As Double = Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Meter" Then
            If Val(meter_4.Replace(",", ".")) > (Val(qty_asal_4.Replace(",", ".")) + Val(sisa_qty_4.Replace(",", "."))) Then
                txt_meter_4.Text = Math.Round((Val(qty_asal_4.Replace(",", ".")) + Val(sisa_qty_4.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Yard" Then
            If Val(meter_4.Replace(",", ".")) > (Val(qty_asal_4.Replace(",", ".")) + Val(sisa_qty_4.Replace(",", "."))) Then
                txt_meter_4.Text = Math.Round((Val(qty_asal_4.Replace(",", ".")) + Val(sisa_qty_4.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Yard" Then
            If Val(meter_4.Replace(",", ".")) > (Val(qty_asal_4.Replace(",", ".")) + b_4) Then
                txt_meter_4.Text = Math.Round((Val(qty_asal_4.Replace(",", ".")) + b_4), 2)
            End If
        ElseIf txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Meter" Then
            If Val(meter_4.Replace(",", ".")) > (Val(qty_asal_4.Replace(",", ".")) + a_4) Then
                txt_meter_4.Text = Math.Round((Val(qty_asal_4.Replace(",", ".")) + a_4), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_5.TextChanged
        If txt_meter_5.Text <> String.Empty Then
            Dim temp As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_5.Select(txt_meter_5.Text.Length, 0)
            ElseIf txt_meter_5.Text = "-"c Then

            Else
                txt_meter_5.Text = CDec(temp).ToString("N0")
                txt_meter_5.Select(txt_meter_5.Text.Length, 0)
            End If
        End If
        txt_kiloan_5.Text = ""
        txt_gramasi_5.Text = ""
        txt_harga_proses_5.Text = ""
        txt_total_harga_5.Text = ""
        Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_5 As String = txt_qty_asal_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_5 As String = txt_sisa_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_5 As Double = Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 0.9144, 0)
        Dim b_5 As Double = Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Meter" Then
            If Val(meter_5.Replace(",", ".")) > (Val(qty_asal_5.Replace(",", ".")) + Val(sisa_qty_5.Replace(",", "."))) Then
                txt_meter_5.Text = Math.Round((Val(qty_asal_5.Replace(",", ".")) + Val(sisa_qty_5.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Yard" Then
            If Val(meter_5.Replace(",", ".")) > (Val(qty_asal_5.Replace(",", ".")) + Val(sisa_qty_5.Replace(",", "."))) Then
                txt_meter_5.Text = Math.Round((Val(qty_asal_5.Replace(",", ".")) + Val(sisa_qty_5.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Yard" Then
            If Val(meter_5.Replace(",", ".")) > (Val(qty_asal_5.Replace(",", ".")) + b_5) Then
                txt_meter_5.Text = Math.Round((Val(qty_asal_5.Replace(",", ".")) + b_5), 2)
            End If
        ElseIf txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Meter" Then
            If Val(meter_5.Replace(",", ".")) > (Val(qty_asal_5.Replace(",", ".")) + a_5) Then
                txt_meter_5.Text = Math.Round((Val(qty_asal_5.Replace(",", ".")) + a_5), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_6.TextChanged
        If txt_meter_6.Text <> String.Empty Then
            Dim temp As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_6.Select(txt_meter_6.Text.Length, 0)
            ElseIf txt_meter_6.Text = "-"c Then

            Else
                txt_meter_6.Text = CDec(temp).ToString("N0")
                txt_meter_6.Select(txt_meter_6.Text.Length, 0)
            End If
        End If
        txt_kiloan_6.Text = ""
        txt_gramasi_6.Text = ""
        txt_harga_proses_6.Text = ""
        txt_total_harga_6.Text = ""
        Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_6 As String = txt_qty_asal_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_6 As String = txt_sisa_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_6 As Double = Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 0.9144, 0)
        Dim b_6 As Double = Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Meter" Then
            If Val(meter_6.Replace(",", ".")) > (Val(qty_asal_6.Replace(",", ".")) + Val(sisa_qty_6.Replace(",", "."))) Then
                txt_meter_6.Text = Math.Round((Val(qty_asal_6.Replace(",", ".")) + Val(sisa_qty_6.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Yard" Then
            If Val(meter_6.Replace(",", ".")) > (Val(qty_asal_6.Replace(",", ".")) + Val(sisa_qty_6.Replace(",", "."))) Then
                txt_meter_6.Text = Math.Round((Val(qty_asal_6.Replace(",", ".")) + Val(sisa_qty_6.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Yard" Then
            If Val(meter_6.Replace(",", ".")) > (Val(qty_asal_6.Replace(",", ".")) + b_6) Then
                txt_meter_6.Text = Math.Round((Val(qty_asal_6.Replace(",", ".")) + b_6), 2)
            End If
        ElseIf txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Meter" Then
            If Val(meter_6.Replace(",", ".")) > (Val(qty_asal_6.Replace(",", ".")) + a_6) Then
                txt_meter_6.Text = Math.Round((Val(qty_asal_6.Replace(",", ".")) + a_6), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_7.TextChanged
        If txt_meter_7.Text <> String.Empty Then
            Dim temp As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_7.Select(txt_meter_7.Text.Length, 0)
            ElseIf txt_meter_7.Text = "-"c Then

            Else
                txt_meter_7.Text = CDec(temp).ToString("N0")
                txt_meter_7.Select(txt_meter_7.Text.Length, 0)
            End If
        End If
        txt_kiloan_7.Text = ""
        txt_gramasi_7.Text = ""
        txt_harga_proses_7.Text = ""
        txt_total_harga_7.Text = ""
        Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_7 As String = txt_qty_asal_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_7 As String = txt_sisa_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_7 As Double = Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 0.9144, 0)
        Dim b_7 As Double = Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Meter" Then
            If Val(meter_7.Replace(",", ".")) > (Val(qty_asal_7.Replace(",", ".")) + Val(sisa_qty_7.Replace(",", "."))) Then
                txt_meter_7.Text = Math.Round((Val(qty_asal_7.Replace(",", ".")) + Val(sisa_qty_7.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Yard" Then
            If Val(meter_7.Replace(",", ".")) > (Val(qty_asal_7.Replace(",", ".")) + Val(sisa_qty_7.Replace(",", "."))) Then
                txt_meter_7.Text = Math.Round((Val(qty_asal_7.Replace(",", ".")) + Val(sisa_qty_7.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Yard" Then
            If Val(meter_7.Replace(",", ".")) > (Val(qty_asal_7.Replace(",", ".")) + b_7) Then
                txt_meter_7.Text = Math.Round((Val(qty_asal_7.Replace(",", ".")) + b_7), 2)
            End If
        ElseIf txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Meter" Then
            If Val(meter_7.Replace(",", ".")) > (Val(qty_asal_7.Replace(",", ".")) + a_7) Then
                txt_meter_7.Text = Math.Round((Val(qty_asal_7.Replace(",", ".")) + a_7), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_8.TextChanged
        If txt_meter_8.Text <> String.Empty Then
            Dim temp As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_8.Select(txt_meter_8.Text.Length, 0)
            ElseIf txt_meter_8.Text = "-"c Then

            Else
                txt_meter_8.Text = CDec(temp).ToString("N0")
                txt_meter_8.Select(txt_meter_8.Text.Length, 0)
            End If
        End If
        txt_kiloan_8.Text = ""
        txt_gramasi_8.Text = ""
        txt_harga_proses_8.Text = ""
        txt_total_harga_8.Text = ""
        Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_8 As String = txt_qty_asal_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_8 As String = txt_sisa_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_8 As Double = Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 0.9144, 0)
        Dim b_8 As Double = Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Meter" Then
            If Val(meter_8.Replace(",", ".")) > (Val(qty_asal_8.Replace(",", ".")) + Val(sisa_qty_8.Replace(",", "."))) Then
                txt_meter_8.Text = Math.Round((Val(qty_asal_8.Replace(",", ".")) + Val(sisa_qty_8.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Yard" Then
            If Val(meter_8.Replace(",", ".")) > (Val(qty_asal_8.Replace(",", ".")) + Val(sisa_qty_8.Replace(",", "."))) Then
                txt_meter_8.Text = Math.Round((Val(qty_asal_8.Replace(",", ".")) + Val(sisa_qty_8.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Yard" Then
            If Val(meter_8.Replace(",", ".")) > (Val(qty_asal_8.Replace(",", ".")) + b_8) Then
                txt_meter_8.Text = Math.Round((Val(qty_asal_8.Replace(",", ".")) + b_8), 2)
            End If
        ElseIf txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Meter" Then
            If Val(meter_8.Replace(",", ".")) > (Val(qty_asal_8.Replace(",", ".")) + a_8) Then
                txt_meter_8.Text = Math.Round((Val(qty_asal_8.Replace(",", ".")) + a_8), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_9.TextChanged
        If txt_meter_9.Text <> String.Empty Then
            Dim temp As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_9.Select(txt_meter_9.Text.Length, 0)
            ElseIf txt_meter_9.Text = "-"c Then

            Else
                txt_meter_9.Text = CDec(temp).ToString("N0")
                txt_meter_9.Select(txt_meter_9.Text.Length, 0)
            End If
        End If
        txt_kiloan_9.Text = ""
        txt_gramasi_9.Text = ""
        txt_harga_proses_9.Text = ""
        txt_total_harga_9.Text = ""
        Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_9 As String = txt_qty_asal_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_9 As String = txt_sisa_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_9 As Double = Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 0.9144, 0)
        Dim b_9 As Double = Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Meter" Then
            If Val(meter_9.Replace(",", ".")) > (Val(qty_asal_9.Replace(",", ".")) + Val(sisa_qty_9.Replace(",", "."))) Then
                txt_meter_9.Text = Math.Round((Val(qty_asal_9.Replace(",", ".")) + Val(sisa_qty_9.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Yard" Then
            If Val(meter_9.Replace(",", ".")) > (Val(qty_asal_9.Replace(",", ".")) + Val(sisa_qty_9.Replace(",", "."))) Then
                txt_meter_9.Text = Math.Round((Val(qty_asal_9.Replace(",", ".")) + Val(sisa_qty_9.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Yard" Then
            If Val(meter_9.Replace(",", ".")) > (Val(qty_asal_9.Replace(",", ".")) + b_9) Then
                txt_meter_9.Text = Math.Round((Val(qty_asal_9.Replace(",", ".")) + b_9), 2)
            End If
        ElseIf txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Meter" Then
            If Val(meter_9.Replace(",", ".")) > (Val(qty_asal_9.Replace(",", ".")) + a_9) Then
                txt_meter_9.Text = Math.Round((Val(qty_asal_9.Replace(",", ".")) + a_9), 2)
            End If
        End If
    End Sub
    Private Sub txt_meter_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_meter_10.TextChanged
        If txt_meter_10.Text <> String.Empty Then
            Dim temp As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_meter_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_meter_10.Select(txt_meter_10.Text.Length, 0)
            ElseIf txt_meter_10.Text = "-"c Then

            Else
                txt_meter_10.Text = CDec(temp).ToString("N0")
                txt_meter_10.Select(txt_meter_10.Text.Length, 0)
            End If
        End If
        txt_kiloan_10.Text = ""
        txt_gramasi_10.Text = ""
        txt_harga_proses_10.Text = ""
        txt_total_harga_10.Text = ""
        Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_10 As String = txt_qty_asal_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_10 As String = txt_sisa_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim a_10 As Double = Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 0.9144, 0)
        Dim b_10 As Double = Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 1.0936, 0)
        If txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Meter" Then
            If Val(meter_10.Replace(",", ".")) > (Val(qty_asal_10.Replace(",", ".")) + Val(sisa_qty_10.Replace(",", "."))) Then
                txt_meter_10.Text = Math.Round((Val(qty_asal_10.Replace(",", ".")) + Val(sisa_qty_10.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Yard" Then
            If Val(meter_10.Replace(",", ".")) > (Val(qty_asal_10.Replace(",", ".")) + Val(sisa_qty_10.Replace(",", "."))) Then
                txt_meter_10.Text = Math.Round((Val(qty_asal_10.Replace(",", ".")) + Val(sisa_qty_10.Replace(",", "."))), 2)
            End If
        ElseIf txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Yard" Then
            If Val(meter_10.Replace(",", ".")) > (Val(qty_asal_10.Replace(",", ".")) + b_10) Then
                txt_meter_10.Text = Math.Round((Val(qty_asal_10.Replace(",", ".")) + b_10), 2)
            End If
        ElseIf txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Meter" Then
            If Val(meter_10.Replace(",", ".")) > (Val(qty_asal_10.Replace(",", ".")) + a_10) Then
                txt_meter_10.Text = Math.Round((Val(qty_asal_10.Replace(",", ".")) + a_10), 2)
            End If
        End If
    End Sub

    Private Sub txt_gudang_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_gudang.TextChanged
        txt_no_po_1.Text = ""
        txt_no_po_1.ReadOnly = False
        txt_jenis_kain_1.Text = ""
        txt_jenis_kain_1.ReadOnly = False
        txt_warna_1.Text = ""
        txt_warna_1.ReadOnly = False
        txt_resep_1.Text = ""
        txt_resep_1.ReadOnly = False
        txt_partai_1.Text = ""
        txt_gulung_1.Text = ""
        txt_meter_1.Text = ""
        txt_kiloan_1.Text = ""
        txt_gramasi_1.Text = ""
        txt_harga_proses_1.Text = ""
        txt_total_harga_1.Text = ""
        txt_gudang_packing_1.Text = ""
        txt_keterangan_1.Text = ""
        txt_qty_asal_1.Text = ""
        txt_harga_1.Text = ""
        txt_id_grey_1.Text = ""
        txt_id_sj_proses_1.Text = ""
        txt_sisa_qty_1.Text = ""
    End Sub
    Private Sub txt_kiloan_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_1.TextChanged
        If txt_kiloan_1.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_1.Select(txt_kiloan_1.Text.Length, 0)
            ElseIf txt_kiloan_1.Text = "-"c Then

            Else
                txt_kiloan_1.Text = CDec(temp).ToString("N0")
                txt_kiloan_1.Select(txt_kiloan_1.Text.Length, 0)
            End If
        End If
        txt_harga_proses_1.Text = ""
        txt_total_harga_1.Text = ""
        Dim kiloan_1 As String = txt_kiloan_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_gramasi_1.Text = Math.Round(Val(kiloan_1.Replace(",", ".")) / Val(meter_1.Replace(",", ".")), 3)
        If cb_satuan_1.Text = "Yard" Then
            txt_gramasi_1.Text = Math.Round(Val(kiloan_1.Replace(",", ".")) / (Val(meter_1.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_1.Text = "Meter" Then
            txt_gramasi_1.Text = Math.Round(Val(kiloan_1.Replace(",", ".")) / Val(meter_1.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_2.TextChanged
        If txt_kiloan_2.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_2.Select(txt_kiloan_2.Text.Length, 0)
            ElseIf txt_kiloan_2.Text = "-"c Then

            Else
                txt_kiloan_2.Text = CDec(temp).ToString("N0")
                txt_kiloan_2.Select(txt_kiloan_2.Text.Length, 0)
            End If
        End If
        txt_harga_proses_2.Text = ""
        txt_total_harga_2.Text = ""
        Dim kiloan_2 As String = txt_kiloan_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_2.Text = "Yard" Then
            txt_gramasi_2.Text = Math.Round(Val(kiloan_2.Replace(",", ".")) / (Val(meter_2.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_2.Text = "Meter" Then
            txt_gramasi_2.Text = Math.Round(Val(kiloan_2.Replace(",", ".")) / Val(meter_2.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_3.TextChanged
        If txt_kiloan_3.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_3.Select(txt_kiloan_3.Text.Length, 0)
            ElseIf txt_kiloan_3.Text = "-"c Then

            Else
                txt_kiloan_3.Text = CDec(temp).ToString("N0")
                txt_kiloan_3.Select(txt_kiloan_3.Text.Length, 0)
            End If
        End If
        txt_harga_proses_3.Text = ""
        txt_total_harga_3.Text = ""
        Dim kiloan_3 As String = txt_kiloan_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_3.Text = "Yard" Then
            txt_gramasi_3.Text = Math.Round(Val(kiloan_3.Replace(",", ".")) / (Val(meter_3.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_3.Text = "Meter" Then
            txt_gramasi_3.Text = Math.Round(Val(kiloan_3.Replace(",", ".")) / Val(meter_3.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_4.TextChanged
        If txt_kiloan_4.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_4.Select(txt_kiloan_4.Text.Length, 0)
            ElseIf txt_kiloan_4.Text = "-"c Then

            Else
                txt_kiloan_4.Text = CDec(temp).ToString("N0")
                txt_kiloan_4.Select(txt_kiloan_4.Text.Length, 0)
            End If
        End If
        txt_harga_proses_4.Text = ""
        txt_total_harga_4.Text = ""
        Dim kiloan_4 As String = txt_kiloan_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_4.Text = "Yard" Then
            txt_gramasi_4.Text = Math.Round(Val(kiloan_4.Replace(",", ".")) / (Val(meter_4.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_4.Text = "Meter" Then
            txt_gramasi_4.Text = Math.Round(Val(kiloan_4.Replace(",", ".")) / Val(meter_4.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_5.TextChanged
        If txt_kiloan_5.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_5.Select(txt_kiloan_5.Text.Length, 0)
            ElseIf txt_kiloan_5.Text = "-"c Then

            Else
                txt_kiloan_5.Text = CDec(temp).ToString("N0")
                txt_kiloan_5.Select(txt_kiloan_5.Text.Length, 0)
            End If
        End If
        txt_harga_proses_5.Text = ""
        txt_total_harga_5.Text = ""
        Dim kiloan_5 As String = txt_kiloan_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_5.Text = "Yard" Then
            txt_gramasi_5.Text = Math.Round(Val(kiloan_5.Replace(",", ".")) / (Val(meter_5.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_5.Text = "Meter" Then
            txt_gramasi_5.Text = Math.Round(Val(kiloan_5.Replace(",", ".")) / Val(meter_5.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_6.TextChanged
        If txt_kiloan_6.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_6.Select(txt_kiloan_6.Text.Length, 0)
            ElseIf txt_kiloan_6.Text = "-"c Then

            Else
                txt_kiloan_6.Text = CDec(temp).ToString("N0")
                txt_kiloan_6.Select(txt_kiloan_6.Text.Length, 0)
            End If
        End If
        txt_harga_proses_6.Text = ""
        txt_total_harga_6.Text = ""
        Dim kiloan_6 As String = txt_kiloan_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_6.Text = "Yard" Then
            txt_gramasi_6.Text = Math.Round(Val(kiloan_6.Replace(",", ".")) / (Val(meter_6.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_6.Text = "Meter" Then
            txt_gramasi_6.Text = Math.Round(Val(kiloan_6.Replace(",", ".")) / Val(meter_6.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_7.TextChanged
        If txt_kiloan_7.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_7.Select(txt_kiloan_7.Text.Length, 0)
            ElseIf txt_kiloan_7.Text = "-"c Then

            Else
                txt_kiloan_7.Text = CDec(temp).ToString("N0")
                txt_kiloan_7.Select(txt_kiloan_7.Text.Length, 0)
            End If
        End If
        txt_harga_proses_7.Text = ""
        txt_total_harga_7.Text = ""
        Dim kiloan_7 As String = txt_kiloan_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_7.Text = "Yard" Then
            txt_gramasi_7.Text = Math.Round(Val(kiloan_7.Replace(",", ".")) / (Val(meter_7.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_7.Text = "Meter" Then
            txt_gramasi_7.Text = Math.Round(Val(kiloan_7.Replace(",", ".")) / Val(meter_7.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_8.TextChanged
        If txt_kiloan_8.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_8.Select(txt_kiloan_8.Text.Length, 0)
            ElseIf txt_kiloan_8.Text = "-"c Then

            Else
                txt_kiloan_8.Text = CDec(temp).ToString("N0")
                txt_kiloan_8.Select(txt_kiloan_8.Text.Length, 0)
            End If
        End If
        txt_harga_proses_8.Text = ""
        txt_total_harga_8.Text = ""
        Dim kiloan_8 As String = txt_kiloan_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_8.Text = "Yard" Then
            txt_gramasi_8.Text = Math.Round(Val(kiloan_8.Replace(",", ".")) / (Val(meter_8.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_8.Text = "Meter" Then
            txt_gramasi_8.Text = Math.Round(Val(kiloan_8.Replace(",", ".")) / Val(meter_8.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_9.TextChanged
        If txt_kiloan_9.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_9.Select(txt_kiloan_9.Text.Length, 0)
            ElseIf txt_kiloan_9.Text = "-"c Then

            Else
                txt_kiloan_9.Text = CDec(temp).ToString("N0")
                txt_kiloan_9.Select(txt_kiloan_9.Text.Length, 0)
            End If
        End If
        txt_harga_proses_9.Text = ""
        txt_total_harga_9.Text = ""
        Dim kiloan_9 As String = txt_kiloan_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_9.Text = "Yard" Then
            txt_gramasi_9.Text = Math.Round(Val(kiloan_9.Replace(",", ".")) / (Val(meter_9.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_9.Text = "Meter" Then
            txt_gramasi_9.Text = Math.Round(Val(kiloan_9.Replace(",", ".")) / Val(meter_9.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_kiloan_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_kiloan_10.TextChanged
        If txt_kiloan_10.Text <> String.Empty Then
            Dim temp As String = txt_kiloan_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_kiloan_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_kiloan_10.Select(txt_kiloan_10.Text.Length, 0)
            ElseIf txt_kiloan_10.Text = "-"c Then

            Else
                txt_kiloan_10.Text = CDec(temp).ToString("N0")
                txt_kiloan_10.Select(txt_kiloan_10.Text.Length, 0)
            End If
        End If
        txt_harga_proses_10.Text = ""
        txt_total_harga_10.Text = ""
        Dim kiloan_10 As String = txt_kiloan_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        If cb_satuan_10.Text = "Yard" Then
            txt_gramasi_10.Text = Math.Round(Val(kiloan_10.Replace(",", ".")) / (Val(meter_10.Replace(",", ".") * 0.9144)), 3)
        ElseIf cb_satuan_10.Text = "Meter" Then
            txt_gramasi_10.Text = Math.Round(Val(kiloan_10.Replace(",", ".")) / Val(meter_10.Replace(",", ".")), 3)
        End If
    End Sub
    Private Sub txt_harga_proses_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_1.TextChanged
        If txt_harga_proses_1.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_1.Select(txt_harga_proses_1.Text.Length, 0)
            ElseIf txt_harga_proses_1.Text = "-"c Then

            Else
                txt_harga_proses_1.Text = CDec(temp).ToString("N0")
                txt_harga_proses_1.Select(txt_harga_proses_1.Text.Length, 0)
            End If
        End If
        Dim kiloan_1 As String = txt_kiloan_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_1 As String = txt_harga_proses_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_1.Text = Math.Round(Val(harga_1.Replace(",", ".")) * Val(kiloan_1.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_2.TextChanged
        If txt_harga_proses_2.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_2.Select(txt_harga_proses_2.Text.Length, 0)
            ElseIf txt_harga_proses_2.Text = "-"c Then

            Else
                txt_harga_proses_2.Text = CDec(temp).ToString("N0")
                txt_harga_proses_2.Select(txt_harga_proses_2.Text.Length, 0)
            End If
        End If
        Dim kiloan_2 As String = txt_kiloan_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_2 As String = txt_harga_proses_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_2.Text = Math.Round(Val(harga_2.Replace(",", ".")) * Val(kiloan_2.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_3.TextChanged
        If txt_harga_proses_3.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_3.Select(txt_harga_proses_3.Text.Length, 0)
            ElseIf txt_harga_proses_3.Text = "-"c Then

            Else
                txt_harga_proses_3.Text = CDec(temp).ToString("N0")
                txt_harga_proses_3.Select(txt_harga_proses_3.Text.Length, 0)
            End If
        End If
        Dim kiloan_3 As String = txt_kiloan_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_3 As String = txt_harga_proses_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_3.Text = Math.Round(Val(harga_3.Replace(",", ".")) * Val(kiloan_3.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_4.TextChanged
        If txt_harga_proses_4.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_4.Select(txt_harga_proses_4.Text.Length, 0)
            ElseIf txt_harga_proses_4.Text = "-"c Then

            Else
                txt_harga_proses_4.Text = CDec(temp).ToString("N0")
                txt_harga_proses_4.Select(txt_harga_proses_4.Text.Length, 0)
            End If
        End If
        Dim kiloan_4 As String = txt_kiloan_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_4 As String = txt_harga_proses_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_4.Text = Math.Round(Val(harga_4.Replace(",", ".")) * Val(kiloan_4.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_5.TextChanged
        If txt_harga_proses_5.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_5.Select(txt_harga_proses_5.Text.Length, 0)
            ElseIf txt_harga_proses_5.Text = "-"c Then

            Else
                txt_harga_proses_5.Text = CDec(temp).ToString("N0")
                txt_harga_proses_5.Select(txt_harga_proses_5.Text.Length, 0)
            End If
        End If
        Dim kiloan_5 As String = txt_kiloan_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_5 As String = txt_harga_proses_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_5.Text = Math.Round(Val(harga_5.Replace(",", ".")) * Val(kiloan_5.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_6.TextChanged
        If txt_harga_proses_6.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_6.Select(txt_harga_proses_6.Text.Length, 0)
            ElseIf txt_harga_proses_6.Text = "-"c Then

            Else
                txt_harga_proses_6.Text = CDec(temp).ToString("N0")
                txt_harga_proses_6.Select(txt_harga_proses_6.Text.Length, 0)
            End If
        End If
        Dim kiloan_6 As String = txt_kiloan_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_6 As String = txt_harga_proses_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_6.Text = Math.Round(Val(harga_6.Replace(",", ".")) * Val(kiloan_6.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_7.TextChanged
        If txt_harga_proses_7.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_7.Select(txt_harga_proses_7.Text.Length, 0)
            ElseIf txt_harga_proses_7.Text = "-"c Then

            Else
                txt_harga_proses_7.Text = CDec(temp).ToString("N0")
                txt_harga_proses_7.Select(txt_harga_proses_7.Text.Length, 0)
            End If
        End If
        Dim kiloan_7 As String = txt_kiloan_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_7 As String = txt_harga_proses_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_7.Text = Math.Round(Val(harga_7.Replace(",", ".")) * Val(kiloan_7.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_8.TextChanged
        If txt_harga_proses_8.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_8.Select(txt_harga_proses_8.Text.Length, 0)
            ElseIf txt_harga_proses_8.Text = "-"c Then

            Else
                txt_harga_proses_8.Text = CDec(temp).ToString("N0")
                txt_harga_proses_8.Select(txt_harga_proses_8.Text.Length, 0)
            End If
        End If
        Dim kiloan_8 As String = txt_kiloan_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_8 As String = txt_harga_proses_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_8.Text = Math.Round(Val(harga_8.Replace(",", ".")) * Val(kiloan_8.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_9.TextChanged
        If txt_harga_proses_9.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_9.Select(txt_harga_proses_9.Text.Length, 0)
            ElseIf txt_harga_proses_9.Text = "-"c Then

            Else
                txt_harga_proses_9.Text = CDec(temp).ToString("N0")
                txt_harga_proses_9.Select(txt_harga_proses_9.Text.Length, 0)
            End If
        End If
        Dim kiloan_9 As String = txt_kiloan_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_9 As String = txt_harga_proses_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_9.Text = Math.Round(Val(harga_9.Replace(",", ".")) * Val(kiloan_9.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_harga_proses_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_harga_proses_10.TextChanged
        If txt_harga_proses_10.Text <> String.Empty Then
            Dim temp As String = txt_harga_proses_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_harga_proses_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_harga_proses_10.Select(txt_harga_proses_10.Text.Length, 0)
            ElseIf txt_harga_proses_10.Text = "-"c Then

            Else
                txt_harga_proses_10.Text = CDec(temp).ToString("N0")
                txt_harga_proses_10.Select(txt_harga_proses_10.Text.Length, 0)
            End If
        End If
        Dim kiloan_10 As String = txt_kiloan_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_10 As String = txt_harga_proses_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        txt_total_harga_10.Text = Math.Round(Val(harga_10.Replace(",", ".")) * Val(kiloan_10.Replace(",", ".")), 0)
    End Sub
    Private Sub txt_total_harga_1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_1.TextChanged
        If txt_total_harga_1.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_1.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
            ElseIf txt_total_harga_1.Text = "-"c Then

            Else
                txt_total_harga_1.Text = CDec(temp).ToString("N0")
                txt_total_harga_1.Select(txt_total_harga_1.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_2.TextChanged
        If txt_total_harga_2.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_2.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
            ElseIf txt_total_harga_2.Text = "-"c Then

            Else
                txt_total_harga_2.Text = CDec(temp).ToString("N0")
                txt_total_harga_2.Select(txt_total_harga_2.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_3.TextChanged
        If txt_total_harga_3.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_3.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
            ElseIf txt_total_harga_3.Text = "-"c Then

            Else
                txt_total_harga_3.Text = CDec(temp).ToString("N0")
                txt_total_harga_3.Select(txt_total_harga_3.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_4_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_4.TextChanged
        If txt_total_harga_4.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_4.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
            ElseIf txt_total_harga_4.Text = "-"c Then

            Else
                txt_total_harga_4.Text = CDec(temp).ToString("N0")
                txt_total_harga_4.Select(txt_total_harga_4.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_5.TextChanged
        If txt_total_harga_5.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_5.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
            ElseIf txt_total_harga_5.Text = "-"c Then

            Else
                txt_total_harga_5.Text = CDec(temp).ToString("N0")
                txt_total_harga_5.Select(txt_total_harga_5.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_6_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_6.TextChanged
        If txt_total_harga_6.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_6.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
            ElseIf txt_total_harga_6.Text = "-"c Then

            Else
                txt_total_harga_6.Text = CDec(temp).ToString("N0")
                txt_total_harga_6.Select(txt_total_harga_6.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_7_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_7.TextChanged
        If txt_total_harga_7.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_7.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
            ElseIf txt_total_harga_7.Text = "-"c Then

            Else
                txt_total_harga_7.Text = CDec(temp).ToString("N0")
                txt_total_harga_7.Select(txt_total_harga_7.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_8_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_8.TextChanged
        If txt_total_harga_8.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_8.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
            ElseIf txt_total_harga_8.Text = "-"c Then

            Else
                txt_total_harga_8.Text = CDec(temp).ToString("N0")
                txt_total_harga_8.Select(txt_total_harga_8.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_9_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_9.TextChanged
        If txt_total_harga_9.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_9.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
            ElseIf txt_total_harga_9.Text = "-"c Then

            Else
                txt_total_harga_9.Text = CDec(temp).ToString("N0")
                txt_total_harga_9.Select(txt_total_harga_9.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_harga_10_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_harga_10.TextChanged
        If txt_total_harga_10.Text <> String.Empty Then
            Dim temp As String = txt_total_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_harga_10.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
            ElseIf txt_total_harga_10.Text = "-"c Then

            Else
                txt_total_harga_10.Text = CDec(temp).ToString("N0")
                txt_total_harga_10.Select(txt_total_harga_10.Text.Length, 0)
            End If
        End If
    End Sub

    Private Sub btn_tambah_baris_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_1.Click
        Try
            If txt_surat_jalan_proses.Text = "" Then
                MsgBox("Input Surat Jalan Terlebih Dahulu")
                txt_surat_jalan_proses.Focus()
            ElseIf txt_gudang.Text = "" Then
                MsgBox("Input GUDANG Terlebih Dahulu")
                txt_gudang.Focus()
            ElseIf txt_no_po_1.Text = "" And txt_jenis_kain_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_1.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_1.Focus()
            ElseIf txt_gulung_1.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_1.Focus()
            ElseIf Val(txt_meter_1.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_1.Focus()
            ElseIf Val(txt_kiloan_1.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_1.Focus()
            ElseIf txt_harga_proses_1.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_1.Focus()
            ElseIf txt_total_harga_1.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_1.Focus()
            ElseIf txt_gudang_packing_1.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_1.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_1.Enabled = False
                btn_tambah_baris_1.Visible = False
                btn_selesai_1.Visible = False
                btn_batal_1.Visible = False
                Panel_2.Visible = True
                btn_hapus_2.Visible = True
                btn_tambah_baris_2.Visible = True
                btn_selesai_2.Visible = True
                btn_batal_2.Visible = True
                txt_gudang_packing_2.Text = txt_gudang_packing_1.Text
                Call isiidsjproses_1()
                Call isikodehutang_1()
                Call simpan_tbsuratjalanproses_baris_1()
                Call update_status_tbpoproses_1()
                Call update_tbstokprosesgrey_1()
                Call update_tbstokwipproses_baris_1()
                Call simpan_tbdetilhutang_1()
                Call simpan_tbstokexproses_baris_1()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_2.Click
        Try
            If txt_no_po_2.Text = "" And txt_jenis_kain_2.Text = "" Then
                MsgBox("Baris 2 Belum Diisi Data")
            ElseIf txt_partai_2.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_2.Focus()
            ElseIf txt_gulung_2.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_2.Focus()
            ElseIf Val(txt_meter_2.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_2.Focus()
            ElseIf Val(txt_kiloan_2.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_2.Focus()
            ElseIf txt_harga_proses_2.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_2.Focus()
            ElseIf txt_total_harga_2.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_2.Focus()
            ElseIf txt_gudang_packing_2.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_2.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_2.Enabled = False
                btn_tambah_baris_2.Visible = False
                btn_selesai_2.Visible = False
                btn_batal_2.Visible = False
                btn_hapus_2.Visible = False
                Panel_3.Visible = True
                btn_hapus_3.Visible = True
                btn_tambah_baris_3.Visible = True
                btn_selesai_3.Visible = True
                btn_batal_3.Visible = True
                txt_gudang_packing_3.Text = txt_gudang_packing_2.Text
                Call isiidsjproses_2()
                Call isikodehutang_2()
                Call simpan_tbsuratjalanproses_baris_2()
                Call update_status_tbpoproses_2()
                Call update_tbstokprosesgrey_2()
                Call update_tbstokwipproses_baris_2()
                Call simpan_tbdetilhutang_2()
                Call simpan_tbstokexproses_baris_2()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_3.Click
        Try
            If txt_no_po_3.Text = "" And txt_jenis_kain_3.Text = "" Then
                MsgBox("Baris 3 Belum Diisi Data")
            ElseIf txt_partai_3.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_3.Focus()
            ElseIf txt_gulung_3.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_3.Focus()
            ElseIf Val(txt_meter_3.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_3.Focus()
            ElseIf Val(txt_kiloan_3.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_3.Focus()
            ElseIf txt_harga_proses_3.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_3.Focus()
            ElseIf txt_total_harga_3.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_3.Focus()
            ElseIf txt_gudang_packing_3.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_3.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_3.Enabled = False
                btn_tambah_baris_3.Visible = False
                btn_selesai_3.Visible = False
                btn_batal_3.Visible = False
                btn_hapus_3.Visible = False
                Panel_4.Visible = True
                btn_hapus_4.Visible = True
                btn_tambah_baris_4.Visible = True
                btn_selesai_4.Visible = True
                btn_batal_4.Visible = True
                txt_gudang_packing_4.Text = txt_gudang_packing_3.Text
                Call isiidsjproses_3()
                Call isikodehutang_3()
                Call simpan_tbsuratjalanproses_baris_3()
                Call update_status_tbpoproses_3()
                Call update_tbstokprosesgrey_3()
                Call update_tbstokwipproses_baris_3()
                Call simpan_tbdetilhutang_3()
                Call simpan_tbstokexproses_baris_3()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_4.Click
        Try
            If txt_no_po_4.Text = "" And txt_jenis_kain_4.Text = "" Then
                MsgBox("Baris 4 Belum Diisi Data")
            ElseIf txt_partai_4.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_4.Focus()
            ElseIf txt_gulung_4.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_4.Focus()
            ElseIf Val(txt_meter_4.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_4.Focus()
            ElseIf Val(txt_kiloan_4.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_4.Focus()
            ElseIf txt_harga_proses_4.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_4.Focus()
            ElseIf txt_total_harga_4.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_4.Focus()
            ElseIf txt_gudang_packing_4.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_4.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_4.Enabled = False
                btn_tambah_baris_4.Visible = False
                btn_selesai_4.Visible = False
                btn_batal_4.Visible = False
                btn_hapus_4.Visible = False
                Panel_5.Visible = True
                btn_hapus_5.Visible = True
                btn_tambah_baris_5.Visible = True
                btn_selesai_5.Visible = True
                btn_batal_5.Visible = True
                txt_gudang_packing_5.Text = txt_gudang_packing_4.Text
                Call isiidsjproses_4()
                Call isikodehutang_4()
                Call simpan_tbsuratjalanproses_baris_4()
                Call update_status_tbpoproses_4()
                Call update_tbstokprosesgrey_4()
                Call update_tbstokwipproses_baris_4()
                Call simpan_tbdetilhutang_4()
                Call simpan_tbstokexproses_baris_4()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_5.Click
        Try
            If txt_no_po_5.Text = "" And txt_jenis_kain_5.Text = "" Then
                MsgBox("Baris 5 Belum Diisi Data")
            ElseIf txt_partai_5.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_5.Focus()
            ElseIf txt_gulung_5.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_5.Focus()
            ElseIf Val(txt_meter_5.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_5.Focus()
            ElseIf Val(txt_kiloan_5.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_5.Focus()
            ElseIf txt_harga_proses_5.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_5.Focus()
            ElseIf txt_total_harga_5.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_5.Focus()
            ElseIf txt_gudang_packing_5.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_5.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_5.Enabled = False
                btn_tambah_baris_5.Visible = False
                btn_selesai_5.Visible = False
                btn_batal_5.Visible = False
                btn_hapus_5.Visible = False
                Panel_6.Visible = True
                btn_hapus_6.Visible = True
                btn_tambah_baris_6.Visible = True
                btn_selesai_6.Visible = True
                btn_batal_6.Visible = True
                txt_gudang_packing_6.Text = txt_gudang_packing_5.Text
                Call isiidsjproses_5()
                Call isikodehutang_5()
                Call simpan_tbsuratjalanproses_baris_5()
                Call update_status_tbpoproses_5()
                Call update_tbstokprosesgrey_5()
                Call update_tbstokwipproses_baris_5()
                Call simpan_tbdetilhutang_5()
                Call simpan_tbstokexproses_baris_5()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_6.Click
        Try
            If txt_no_po_6.Text = "" And txt_jenis_kain_6.Text = "" Then
                MsgBox("Baris 6 Belum Diisi Data")
            ElseIf txt_partai_6.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_6.Focus()
            ElseIf txt_gulung_6.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_6.Focus()
            ElseIf Val(txt_meter_6.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_6.Focus()
            ElseIf Val(txt_kiloan_6.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_6.Focus()
            ElseIf txt_harga_proses_6.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_6.Focus()
            ElseIf txt_total_harga_6.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_6.Focus()
            ElseIf txt_gudang_packing_6.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_6.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_6.Enabled = False
                btn_tambah_baris_6.Visible = False
                btn_selesai_6.Visible = False
                btn_batal_6.Visible = False
                btn_hapus_6.Visible = False
                Panel_7.Visible = True
                btn_hapus_7.Visible = True
                btn_tambah_baris_7.Visible = True
                btn_selesai_7.Visible = True
                btn_batal_7.Visible = True
                txt_gudang_packing_7.Text = txt_gudang_packing_6.Text
                Call isiidsjproses_6()
                Call isikodehutang_6()
                Call simpan_tbsuratjalanproses_baris_6()
                Call update_status_tbpoproses_6()
                Call update_tbstokprosesgrey_6()
                Call update_tbstokwipproses_baris_6()
                Call simpan_tbdetilhutang_6()
                Call simpan_tbstokexproses_baris_6()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_7.Click
        Try
            If txt_no_po_7.Text = "" And txt_jenis_kain_7.Text = "" Then
                MsgBox("Baris 7 Belum Diisi Data")
            ElseIf txt_partai_7.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_7.Focus()
            ElseIf txt_gulung_7.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_7.Focus()
            ElseIf Val(txt_meter_7.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_7.Focus()
            ElseIf Val(txt_kiloan_7.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_7.Focus()
            ElseIf txt_harga_proses_7.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_7.Focus()
            ElseIf txt_total_harga_7.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_7.Focus()
            ElseIf txt_gudang_packing_7.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_7.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_7.Enabled = False
                btn_tambah_baris_7.Visible = False
                btn_selesai_7.Visible = False
                btn_batal_7.Visible = False
                btn_hapus_7.Visible = False
                Panel_8.Visible = True
                btn_hapus_8.Visible = True
                btn_tambah_baris_8.Visible = True
                btn_selesai_8.Visible = True
                btn_batal_8.Visible = True
                txt_gudang_packing_8.Text = txt_gudang_packing_7.Text
                Call isiidsjproses_7()
                Call isikodehutang_7()
                Call simpan_tbsuratjalanproses_baris_7()
                Call update_status_tbpoproses_7()
                Call update_tbstokprosesgrey_7()
                Call update_tbstokwipproses_baris_7()
                Call simpan_tbdetilhutang_7()
                Call simpan_tbstokexproses_baris_7()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_8.Click
        Try
            If txt_no_po_8.Text = "" And txt_jenis_kain_8.Text = "" Then
                MsgBox("Baris 8 Belum Diisi Data")
            ElseIf txt_partai_8.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_8.Focus()
            ElseIf txt_gulung_8.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_8.Focus()
            ElseIf Val(txt_meter_8.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_8.Focus()
            ElseIf Val(txt_kiloan_8.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_8.Focus()
            ElseIf txt_harga_proses_8.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_8.Focus()
            ElseIf txt_total_harga_8.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_8.Focus()
            ElseIf txt_gudang_packing_8.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_8.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_8.Enabled = False
                btn_tambah_baris_8.Visible = False
                btn_selesai_8.Visible = False
                btn_batal_8.Visible = False
                btn_hapus_8.Visible = False
                Panel_9.Visible = True
                btn_hapus_9.Visible = True
                btn_tambah_baris_9.Visible = True
                btn_selesai_9.Visible = True
                btn_batal_9.Visible = True
                txt_gudang_packing_9.Text = txt_gudang_packing_8.Text
                Call isiidsjproses_8()
                Call isikodehutang_8()
                Call simpan_tbsuratjalanproses_baris_8()
                Call update_status_tbpoproses_8()
                Call update_tbstokprosesgrey_8()
                Call update_tbstokwipproses_baris_8()
                Call simpan_tbdetilhutang_8()
                Call simpan_tbstokexproses_baris_8()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_tambah_baris_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_tambah_baris_9.Click
        Try
            If txt_no_po_9.Text = "" And txt_jenis_kain_9.Text = "" Then
                MsgBox("Baris 9 Belum Diisi Data")
            ElseIf txt_partai_9.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_9.Focus()
            ElseIf txt_gulung_9.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_9.Focus()
            ElseIf Val(txt_meter_9.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_9.Focus()
            ElseIf Val(txt_kiloan_9.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_9.Focus()
            ElseIf txt_harga_proses_9.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_9.Focus()
            ElseIf txt_total_harga_9.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_9.Focus()
            ElseIf txt_gudang_packing_9.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_9.Focus()
            Else
                dtp_tanggal.Enabled = False
                txt_gudang.ReadOnly = True
                txt_surat_jalan_proses.ReadOnly = True
                Panel_9.Enabled = False
                btn_tambah_baris_9.Visible = False
                btn_selesai_9.Visible = False
                btn_batal_9.Visible = False
                btn_hapus_9.Visible = False
                Panel_10.Visible = True
                btn_hapus_10.Visible = True
                btn_selesai_10.Visible = True
                btn_batal_10.Visible = True
                txt_gudang_packing_10.Text = txt_gudang_packing_9.Text
                Call isiidsjproses_9()
                Call isikodehutang_9()
                Call simpan_tbsuratjalanproses_baris_9()
                Call update_status_tbpoproses_9()
                Call update_tbstokprosesgrey_9()
                Call update_tbstokwipproses_baris_9()
                Call simpan_tbdetilhutang_9()
                Call simpan_tbstokexproses_baris_9()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_selesai_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_1.Click
        Try
            If txt_surat_jalan_proses.Text = "" Then
                MsgBox("Input Surat Jalan Terlebih Dahulu")
                txt_surat_jalan_proses.Focus()
            ElseIf txt_gudang.Text = "" Then
                MsgBox("Input GUDANG Terlebih Dahulu")
                txt_gudang.Focus()
            ElseIf txt_no_po_1.Text = "" And txt_jenis_kain_1.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_1.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_1.Focus()
            ElseIf txt_gulung_1.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_1.Focus()
            ElseIf Val(txt_meter_1.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_1.Focus()
            ElseIf Val(txt_kiloan_1.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_1.Focus()
            ElseIf txt_harga_proses_1.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_1.Focus()
            ElseIf txt_total_harga_1.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_1.Focus()
            ElseIf txt_gudang_packing_1.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_1.Focus()
            Else
                Call isiidsjproses_1()
                Call isikodehutang_1()
                Call simpan_tbsuratjalanproses_baris_1()
                Call update_status_tbpoproses_1()
                Call update_tbstokprosesgrey_1()
                Call update_tbstokwipproses_baris_1()
                Call simpan_tbdetilhutang_1()
                Call simpan_tbstokexproses_baris_1()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_2.Click
        Try
            If txt_no_po_2.Text = "" And txt_jenis_kain_2.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_2.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_2.Focus()
            ElseIf txt_gulung_2.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_2.Focus()
            ElseIf Val(txt_meter_2.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_2.Focus()
            ElseIf Val(txt_kiloan_2.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_2.Focus()
            ElseIf txt_harga_proses_2.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_2.Focus()
            ElseIf txt_total_harga_2.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_2.Focus()
            ElseIf txt_gudang_packing_2.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_2.Focus()
            Else
                Call isiidsjproses_2()
                Call isikodehutang_2()
                Call simpan_tbsuratjalanproses_baris_2()
                Call update_status_tbpoproses_2()
                Call update_tbstokprosesgrey_2()
                Call update_tbstokwipproses_baris_2()
                Call simpan_tbdetilhutang_2()
                Call simpan_tbstokexproses_baris_2()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_3.Click
        Try
            If txt_no_po_3.Text = "" And txt_jenis_kain_3.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_3.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_3.Focus()
            ElseIf txt_gulung_3.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_3.Focus()
            ElseIf Val(txt_meter_3.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_3.Focus()
            ElseIf Val(txt_kiloan_3.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_3.Focus()
            ElseIf txt_harga_proses_3.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_3.Focus()
            ElseIf txt_total_harga_3.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_3.Focus()
            ElseIf txt_gudang_packing_3.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_3.Focus()
            Else
                Call isiidsjproses_3()
                Call isikodehutang_3()
                Call simpan_tbsuratjalanproses_baris_3()
                Call update_status_tbpoproses_3()
                Call update_tbstokprosesgrey_3()
                Call update_tbstokwipproses_baris_3()
                Call simpan_tbdetilhutang_3()
                Call simpan_tbstokexproses_baris_3()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_4.Click
        Try
            If txt_no_po_4.Text = "" And txt_jenis_kain_4.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_4.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_4.Focus()
            ElseIf txt_gulung_4.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_4.Focus()
            ElseIf Val(txt_meter_4.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_4.Focus()
            ElseIf Val(txt_kiloan_4.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_4.Focus()
            ElseIf txt_harga_proses_4.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_4.Focus()
            ElseIf txt_total_harga_4.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_4.Focus()
            ElseIf txt_gudang_packing_4.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_4.Focus()
            Else
                Call isiidsjproses_4()
                Call isikodehutang_4()
                Call simpan_tbsuratjalanproses_baris_4()
                Call update_status_tbpoproses_4()
                Call update_tbstokprosesgrey_4()
                Call update_tbstokwipproses_baris_4()
                Call simpan_tbdetilhutang_4()
                Call simpan_tbstokexproses_baris_4()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_5.Click
        Try
            If txt_no_po_5.Text = "" And txt_jenis_kain_5.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_5.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_5.Focus()
            ElseIf txt_gulung_5.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_5.Focus()
            ElseIf Val(txt_meter_5.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_5.Focus()
            ElseIf Val(txt_kiloan_5.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_5.Focus()
            ElseIf txt_harga_proses_5.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_5.Focus()
            ElseIf txt_total_harga_5.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_5.Focus()
            ElseIf txt_gudang_packing_5.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_5.Focus()
            Else
                Call isiidsjproses_5()
                Call isikodehutang_5()
                Call simpan_tbsuratjalanproses_baris_5()
                Call update_status_tbpoproses_5()
                Call update_tbstokprosesgrey_5()
                Call update_tbstokwipproses_baris_5()
                Call simpan_tbdetilhutang_5()
                Call simpan_tbstokexproses_baris_5()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_6.Click
        Try
            If txt_no_po_6.Text = "" And txt_jenis_kain_6.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_6.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_6.Focus()
            ElseIf txt_gulung_6.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_6.Focus()
            ElseIf Val(txt_meter_6.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_6.Focus()
            ElseIf Val(txt_kiloan_6.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_6.Focus()
            ElseIf txt_harga_proses_6.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_6.Focus()
            ElseIf txt_total_harga_6.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_6.Focus()
            ElseIf txt_gudang_packing_6.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_6.Focus()
            Else
                Call isiidsjproses_6()
                Call isikodehutang_6()
                Call simpan_tbsuratjalanproses_baris_6()
                Call update_status_tbpoproses_6()
                Call update_tbstokprosesgrey_6()
                Call update_tbstokwipproses_baris_6()
                Call simpan_tbdetilhutang_6()
                Call simpan_tbstokexproses_baris_6()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_7.Click
        Try
            If txt_no_po_7.Text = "" And txt_jenis_kain_7.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_7.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_7.Focus()
            ElseIf txt_gulung_7.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_7.Focus()
            ElseIf Val(txt_meter_7.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_7.Focus()
            ElseIf Val(txt_kiloan_7.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_7.Focus()
            ElseIf txt_harga_proses_7.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_7.Focus()
            ElseIf txt_total_harga_7.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_7.Focus()
            ElseIf txt_gudang_packing_7.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_7.Focus()
            Else
                Call isiidsjproses_7()
                Call isikodehutang_7()
                Call simpan_tbsuratjalanproses_baris_7()
                Call update_status_tbpoproses_7()
                Call update_tbstokprosesgrey_7()
                Call update_tbstokwipproses_baris_7()
                Call simpan_tbdetilhutang_7()
                Call simpan_tbstokexproses_baris_7()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_8.Click
        Try
            If txt_no_po_8.Text = "" And txt_jenis_kain_8.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_8.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_8.Focus()
            ElseIf txt_gulung_8.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_8.Focus()
            ElseIf Val(txt_meter_8.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_8.Focus()
            ElseIf Val(txt_kiloan_8.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_8.Focus()
            ElseIf txt_harga_proses_8.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_8.Focus()
            ElseIf txt_total_harga_8.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_8.Focus()
            ElseIf txt_gudang_packing_8.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_8.Focus()
            Else
                Call isiidsjproses_8()
                Call isikodehutang_8()
                Call simpan_tbsuratjalanproses_baris_8()
                Call update_status_tbpoproses_8()
                Call update_tbstokprosesgrey_8()
                Call update_tbstokwipproses_baris_8()
                Call simpan_tbdetilhutang_8()
                Call simpan_tbstokexproses_baris_8()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_9.Click
        Try
            If txt_no_po_9.Text = "" And txt_jenis_kain_9.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_9.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_9.Focus()
            ElseIf txt_gulung_9.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_9.Focus()
            ElseIf Val(txt_meter_9.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_9.Focus()
            ElseIf Val(txt_kiloan_9.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_9.Focus()
            ElseIf txt_harga_proses_9.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_9.Focus()
            ElseIf txt_total_harga_9.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_9.Focus()
            ElseIf txt_gudang_packing_9.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_9.Focus()
            Else
                Call isiidsjproses_9()
                Call isikodehutang_9()
                Call simpan_tbsuratjalanproses_baris_9()
                Call update_status_tbpoproses_9()
                Call update_tbstokprosesgrey_9()
                Call update_tbstokwipproses_baris_9()
                Call simpan_tbdetilhutang_9()
                Call simpan_tbstokexproses_baris_9()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_selesai_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_selesai_10.Click
        Try
            If txt_no_po_10.Text = "" And txt_jenis_kain_10.Text = "" Then
                MsgBox("Baris 1 Belum Diisi Data")
            ElseIf txt_partai_10.Text = "" Then
                MsgBox("PARTAI Tidak Boleh Kosong")
                txt_partai_10.Focus()
            ElseIf txt_gulung_10.Text = "" Then
                MsgBox("GULUNG Tidak Boleh Kosong")
                txt_gulung_10.Focus()
            ElseIf Val(txt_meter_10.Text) = 0 Then
                MsgBox("METER Tidak Boleh Kosong")
                txt_meter_10.Focus()
            ElseIf Val(txt_kiloan_10.Text) = 0 Then
                MsgBox("KILOAN Tidak Boleh Kosong")
                txt_kiloan_10.Focus()
            ElseIf txt_harga_proses_10.Text = "" Then
                MsgBox("HARGA Tidak Boleh Kosong")
                txt_harga_proses_10.Focus()
            ElseIf txt_total_harga_10.Text = "" Then
                MsgBox("TOTAL HARGA Tidak Boleh Kosong")
                txt_total_harga_10.Focus()
            ElseIf txt_gudang_packing_10.Text = "" Then
                MsgBox("GUDANG PACKING Tidak Boleh Kosong")
                txt_gudang_packing_10.Focus()
            Else
                Call isiidsjproses_10()
                Call isikodehutang_10()
                Call simpan_tbsuratjalanproses_baris_10()
                Call update_status_tbpoproses_10()
                Call update_tbstokprosesgrey_10()
                Call update_tbstokwipproses_baris_10()
                Call simpan_tbdetilhutang_10()
                Call simpan_tbstokexproses_baris_10()
                MsgBox("Surat Jalan Proses Baru Berhasil Disimpan")
                Me.Close()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub isiidsjproses_1()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_1.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_2()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_2.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_3()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_3.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_4()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_4.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_5()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_5.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_6()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_6.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_7()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_7.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_8()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_8.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_9()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_9.Text = x.ToString
    End Sub
    Private Sub isiidsjproses_10()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_SJ_Proses FROM tbsuratjalanproses WHERE Id_SJ_Proses in(select max(Id_SJ_Proses) FROM tbsuratjalanproses)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_SJ_Proses")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_sj_proses_10.Text = x.ToString
    End Sub

    Private Sub isikodehutang_1()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_1.Text = x.ToString
    End Sub
    Private Sub isikodehutang_2()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_2.Text = x.ToString
    End Sub
    Private Sub isikodehutang_3()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_3.Text = x.ToString
    End Sub
    Private Sub isikodehutang_4()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_4.Text = x.ToString
    End Sub
    Private Sub isikodehutang_5()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_5.Text = x.ToString
    End Sub
    Private Sub isikodehutang_6()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_6.Text = x.ToString
    End Sub
    Private Sub isikodehutang_7()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_7.Text = x.ToString
    End Sub
    Private Sub isikodehutang_8()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_8.Text = x.ToString
    End Sub
    Private Sub isikodehutang_9()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_9.Text = x.ToString
    End Sub
    Private Sub isikodehutang_10()
        Dim x As Integer
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Hutang FROM tbdetilhutang WHERE Id_Hutang in(select max(Id_Hutang) FROM tbdetilhutang)"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using drx As MySqlDataReader = cmdx.ExecuteReader
                    drx.Read()
                    If drx.HasRows Then
                        x = Val(drx.Item("Id_Hutang")) + 1
                    Else
                        x = 1
                    End If
                End Using
            End Using
        End Using
        txt_id_hutang_10.Text = x.ToString
    End Sub

    Private Sub simpan_tbsuratjalanproses_baris_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_1 As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_1 As String = txt_kiloan_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_1 As String = txt_harga_proses_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_1.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@5", txt_warna_1.Text)
                    .Parameters.AddWithValue("@6", txt_resep_1.Text)
                    .Parameters.AddWithValue("@7", txt_partai_1.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_1.Text)
                    .Parameters.AddWithValue("@9", meter_1.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@11", kiloan_1.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_1.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_1.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_1.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_1.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_1.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_1.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_1.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_1.Replace(",", ".")) / Val(meter_1.Replace(",", "."))) + Val(txt_harga_1.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_2 As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_2 As String = txt_kiloan_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_2 As String = txt_harga_proses_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_2.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@5", txt_warna_2.Text)
                    .Parameters.AddWithValue("@6", txt_resep_2.Text)
                    .Parameters.AddWithValue("@7", txt_partai_2.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_2.Text)
                    .Parameters.AddWithValue("@9", meter_2.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@11", kiloan_2.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_2.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_2.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_2.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_2.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_2.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_2.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_2.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_2.Replace(",", ".")) / Val(meter_2.Replace(",", "."))) + Val(txt_harga_2.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_3 As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_3 As String = txt_kiloan_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_3 As String = txt_harga_proses_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_3.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@5", txt_warna_3.Text)
                    .Parameters.AddWithValue("@6", txt_resep_3.Text)
                    .Parameters.AddWithValue("@7", txt_partai_3.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_3.Text)
                    .Parameters.AddWithValue("@9", meter_3.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@11", kiloan_3.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_3.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_3.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_3.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_3.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_3.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_3.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_3.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_3.Replace(",", ".")) / Val(meter_3.Replace(",", "."))) + Val(txt_harga_3.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_4 As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_4 As String = txt_kiloan_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_4 As String = txt_harga_proses_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_4.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@5", txt_warna_4.Text)
                    .Parameters.AddWithValue("@6", txt_resep_4.Text)
                    .Parameters.AddWithValue("@7", txt_partai_4.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_4.Text)
                    .Parameters.AddWithValue("@9", meter_4.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@11", kiloan_4.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_4.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_4.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_4.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_4.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_4.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_4.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_4.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_4.Replace(",", ".")) / Val(meter_4.Replace(",", "."))) + Val(txt_harga_4.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_5 As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_5 As String = txt_kiloan_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_5 As String = txt_harga_proses_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_5.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@5", txt_warna_5.Text)
                    .Parameters.AddWithValue("@6", txt_resep_5.Text)
                    .Parameters.AddWithValue("@7", txt_partai_5.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_5.Text)
                    .Parameters.AddWithValue("@9", meter_5.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@11", kiloan_5.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_5.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_5.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_5.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_5.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_5.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_5.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_5.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_5.Replace(",", ".")) / Val(meter_5.Replace(",", "."))) + Val(txt_harga_5.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_6 As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_6 As String = txt_kiloan_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_6 As String = txt_harga_proses_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_6.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@5", txt_warna_6.Text)
                    .Parameters.AddWithValue("@6", txt_resep_6.Text)
                    .Parameters.AddWithValue("@7", txt_partai_6.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_6.Text)
                    .Parameters.AddWithValue("@9", meter_6.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@11", kiloan_6.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_6.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_6.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_6.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_6.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_6.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_6.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_6.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_6.Replace(",", ".")) / Val(meter_6.Replace(",", "."))) + Val(txt_harga_6.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_7 As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_7 As String = txt_kiloan_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_7 As String = txt_harga_proses_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_7.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@5", txt_warna_7.Text)
                    .Parameters.AddWithValue("@6", txt_resep_7.Text)
                    .Parameters.AddWithValue("@7", txt_partai_7.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_7.Text)
                    .Parameters.AddWithValue("@9", meter_7.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@11", kiloan_7.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_7.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_7.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_7.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_7.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_7.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_7.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_7.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_7.Replace(",", ".")) / Val(meter_7.Replace(",", "."))) + Val(txt_harga_7.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_8 As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_8 As String = txt_kiloan_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_8 As String = txt_harga_proses_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_8.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@5", txt_warna_8.Text)
                    .Parameters.AddWithValue("@6", txt_resep_8.Text)
                    .Parameters.AddWithValue("@7", txt_partai_8.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_8.Text)
                    .Parameters.AddWithValue("@9", meter_8.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@11", kiloan_8.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_8.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_8.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_8.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_8.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_8.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_8.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_8.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_8.Replace(",", ".")) / Val(meter_8.Replace(",", "."))) + Val(txt_harga_8.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_9 As String = txt_total_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_9 As String = txt_kiloan_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_9 As String = txt_harga_proses_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_9.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@5", txt_warna_9.Text)
                    .Parameters.AddWithValue("@6", txt_resep_9.Text)
                    .Parameters.AddWithValue("@7", txt_partai_9.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_9.Text)
                    .Parameters.AddWithValue("@9", meter_9.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_9.Text)
                    .Parameters.AddWithValue("@11", kiloan_9.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_9.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_9.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_9.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_9.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_9.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_9.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_9.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_9.Replace(",", ".")) / Val(meter_9.Replace(",", "."))) + Val(txt_harga_9.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub
    Private Sub simpan_tbsuratjalanproses_baris_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbsuratjalanproses (Tanggal,Gudang,SJ_Proses,No_PO,Jenis_Kain,Warna,Resep,Partai,Gulung,QTY,Satuan,Kiloan,Gramasi,Harga,Total_Harga,Gudang_Packing,Keterangan,Id_Grey,Id_Po_Proses,Id_SJ_Proses,Harga_Satuan,Jatuh_Tempo,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_10 As String = txt_total_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim kiloan_10 As String = txt_kiloan_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_proses_10 As String = txt_harga_proses_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_gudang.Text)
                    .Parameters.AddWithValue("@2", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@3", txt_no_po_10.Text)
                    .Parameters.AddWithValue("@4", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@5", txt_warna_10.Text)
                    .Parameters.AddWithValue("@6", txt_resep_10.Text)
                    .Parameters.AddWithValue("@7", txt_partai_10.Text)
                    .Parameters.AddWithValue("@8", txt_gulung_10.Text)
                    .Parameters.AddWithValue("@9", meter_10.Replace(",", "."))
                    .Parameters.AddWithValue("@10", cb_satuan_10.Text)
                    .Parameters.AddWithValue("@11", kiloan_10.Replace(",", "."))
                    .Parameters.AddWithValue("@12", txt_gramasi_10.Text.Replace(",", "."))
                    .Parameters.AddWithValue("@13", harga_proses_10.Replace(",", "."))
                    .Parameters.AddWithValue("@14", total_harga_10.Replace(",", "."))
                    .Parameters.AddWithValue("@15", txt_gudang_packing_10.Text)
                    .Parameters.AddWithValue("@16", txt_keterangan_10.Text)
                    .Parameters.AddWithValue("@17", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@18", txt_id_po_proses_10.Text)
                    .Parameters.AddWithValue("@19", txt_id_sj_proses_10.Text)
                    .Parameters.AddWithValue("@20", (Math.Round((Val(total_harga_10.Replace(",", ".")) / Val(meter_10.Replace(",", "."))) + Val(txt_harga_10.Text.Replace(",", ".")), 0)))
                    .Parameters.AddWithValue("@21", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@22", "")
                    .Parameters.AddWithValue("@23", ComboBox1.Text)
                    .Parameters.AddWithValue("@24", "")
                    .Parameters.AddWithValue("@25", "")
                    .Parameters.AddWithValue("@26", "")
                    .Parameters.AddWithValue("@27", 0)
                    .Parameters.AddWithValue("@28", 0)
                    .ExecuteNonQuery()
                End With
                dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
            End Using
        End Using
    End Sub

    Private Sub update_status_tbpoproses_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub update_status_tbpoproses_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "Closed")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub

    Private Sub update_tbstokprosesgrey_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_1 As String = txt_qty_asal_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_1 As String = txt_sisa_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_1.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_1.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_1.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_1.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_1.Replace(",", ".")) + Val(sisa_qty_1.Replace(",", "."))) - Val(meter_1.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_1.Replace(",", ".")) + Val(sisa_qty_1.Replace(",", "."))) - Val(meter_1.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_1.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_1.Replace(",", ".")) + c) - Val(meter_1.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_1.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_1.Replace(",", ".")) + f) - Val(meter_1.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_2 As String = txt_qty_asal_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_2 As String = txt_sisa_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_2.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_2.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_2.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_2.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_2.Replace(",", ".")) + Val(sisa_qty_2.Replace(",", "."))) - Val(meter_2.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_2.Replace(",", ".")) + Val(sisa_qty_2.Replace(",", "."))) - Val(meter_2.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_2.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_2.Replace(",", ".")) + c) - Val(meter_2.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_2.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_2.Replace(",", ".")) + f) - Val(meter_2.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_3 As String = txt_qty_asal_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_3 As String = txt_sisa_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_3.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_3.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_3.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_3.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_3.Replace(",", ".")) + Val(sisa_qty_3.Replace(",", "."))) - Val(meter_3.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_3.Replace(",", ".")) + Val(sisa_qty_3.Replace(",", "."))) - Val(meter_3.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_3.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_3.Replace(",", ".")) + c) - Val(meter_3.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_3.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_3.Replace(",", ".")) + f) - Val(meter_3.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_4 As String = txt_qty_asal_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_4 As String = txt_sisa_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_4.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_4.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_4.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_4.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_4.Replace(",", ".")) + Val(sisa_qty_4.Replace(",", "."))) - Val(meter_4.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_4.Replace(",", ".")) + Val(sisa_qty_4.Replace(",", "."))) - Val(meter_4.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_4.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_4.Replace(",", ".")) + c) - Val(meter_4.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_4.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_4.Replace(",", ".")) + f) - Val(meter_4.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_5 As String = txt_qty_asal_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_5 As String = txt_sisa_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_5.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_5.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_5.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_5.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_5.Replace(",", ".")) + Val(sisa_qty_5.Replace(",", "."))) - Val(meter_5.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_5.Replace(",", ".")) + Val(sisa_qty_5.Replace(",", "."))) - Val(meter_5.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_5.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_5.Replace(",", ".")) + c) - Val(meter_5.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_5.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_5.Replace(",", ".")) + f) - Val(meter_5.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_6 As String = txt_qty_asal_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_6 As String = txt_sisa_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_6.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_6.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_6.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_6.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_6.Replace(",", ".")) + Val(sisa_qty_6.Replace(",", "."))) - Val(meter_6.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_6.Replace(",", ".")) + Val(sisa_qty_6.Replace(",", "."))) - Val(meter_6.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_6.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_6.Replace(",", ".")) + c) - Val(meter_6.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_6.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_6.Replace(",", ".")) + f) - Val(meter_6.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_7 As String = txt_qty_asal_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_7 As String = txt_sisa_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_7.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_7.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_7.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_7.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_7.Replace(",", ".")) + Val(sisa_qty_7.Replace(",", "."))) - Val(meter_7.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_7.Replace(",", ".")) + Val(sisa_qty_7.Replace(",", "."))) - Val(meter_7.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_7.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_7.Replace(",", ".")) + c) - Val(meter_7.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_7.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_7.Replace(",", ".")) + f) - Val(meter_7.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_8 As String = txt_qty_asal_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_8 As String = txt_sisa_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_8.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_8.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_8.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_8.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_8.Replace(",", ".")) + Val(sisa_qty_8.Replace(",", "."))) - Val(meter_8.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_8.Replace(",", ".")) + Val(sisa_qty_8.Replace(",", "."))) - Val(meter_8.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_8.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_8.Replace(",", ".")) + c) - Val(meter_8.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_8.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_8.Replace(",", ".")) + f) - Val(meter_8.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_9 As String = txt_qty_asal_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_9 As String = txt_sisa_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_9.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_9.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_9.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_9.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_9.Replace(",", ".")) + Val(sisa_qty_9.Replace(",", "."))) - Val(meter_9.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_9.Replace(",", ".")) + Val(sisa_qty_9.Replace(",", "."))) - Val(meter_9.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_9.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_9.Replace(",", ".")) + c) - Val(meter_9.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_9.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_9.Replace(",", ".")) + f) - Val(meter_9.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokprosesgrey_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_10 As String = txt_qty_asal_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_10 As String = txt_sisa_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim a As Double = Math.Round(Val(meter_10.Replace(",", ".")) * 0.9144, 0)
                    Dim b As Double = Math.Round(Val(qty_asal_10.Replace(",", ".")) * 0.9144, 0)
                    Dim c As Double = Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 0.9144, 0)
                    Dim d As Double = Math.Round(Val(meter_10.Replace(",", ".")) * 1.0936, 0)
                    Dim e As Double = Math.Round(Val(qty_asal_10.Replace(",", ".")) * 1.0936, 0)
                    Dim f As Double = Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 1.0936, 0)
                    .Parameters.Clear()
                    If txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_10.Replace(",", ".")) + Val(sisa_qty_10.Replace(",", "."))) - Val(meter_10.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_10.Replace(",", ".")) + Val(sisa_qty_10.Replace(",", "."))) - Val(meter_10.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + c) - a, 0))
                    ElseIf txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((b + Val(sisa_qty_10.Replace(",", "."))) - a, 0))
                    ElseIf txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_10.Replace(",", ".")) + c) - Val(meter_10.Replace(",", ".")), 0))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + f) - d, 0))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round((e + Val(sisa_qty_10.Replace(",", "."))) - d, 0))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round((Val(qty_asal_10.Replace(",", ".")) + f) - Val(meter_10.Replace(",", ".")), 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Sub update_tbstokwipproses_baris_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub update_tbstokwipproses_baris_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", 0)
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Sub simpan_tbdetilhutang_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_1.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_1.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_1 As String = txt_harga_proses_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_1 As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_1.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_1.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_1.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_2.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_2.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_2 As String = txt_harga_proses_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_2 As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_2.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_2.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_2.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_3.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_3.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_3 As String = txt_harga_proses_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_3 As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_3.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_3.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_3.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_4.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_4.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_4 As String = txt_harga_proses_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_4 As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_4.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_4.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_4.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_5.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_5.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_5 As String = txt_harga_proses_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_5 As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_5.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_5.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_5.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_6.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_6.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_6 As String = txt_harga_proses_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_6 As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_6.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_6.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_6.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_7.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_7.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_7 As String = txt_harga_proses_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_7 As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_7.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_7.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_7.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_8.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_8.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_8 As String = txt_harga_proses_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_8 As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_8.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_8.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_8.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_9.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_9.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_9 As String = txt_harga_proses_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_9 As String = txt_total_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_9.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_9.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_9.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbdetilhutang_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbdetilhutang (Tanggal,Id_Grey,Id_Asal,Id_Hutang,SJ,Jenis_Kain,Nama,Harga,QTY,Satuan,Jumlah,Jatuh_Tempo,Keterangan,Status,Lama_Jt,Tambah1,Tambah2,Tambah3,Tambah4,Tambah5) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    dtp_jatuh_tempo.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_10.Text)
                    .Parameters.AddWithValue("@3", txt_id_hutang_10.Text)
                    .Parameters.AddWithValue("@4", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@6", txt_gudang.Text)
                    Dim harga_proses_10 As String = txt_harga_proses_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim qty_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_10 As String = txt_total_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.AddWithValue("@7", harga_proses_10.Replace(",", "."))
                    .Parameters.AddWithValue("@8", qty_10.Replace(",", "."))
                    .Parameters.AddWithValue("@9", "Kg")
                    .Parameters.AddWithValue("@10", total_harga_10.Replace(",", "."))
                    .Parameters.AddWithValue("@11", dtp_jatuh_tempo.Text)
                    .Parameters.AddWithValue("@12", "Proses")
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", ComboBox1.Text)
                    .Parameters.AddWithValue("@15", "")
                    .Parameters.AddWithValue("@16", "")
                    .Parameters.AddWithValue("@17", "")
                    .Parameters.AddWithValue("@18", 0)
                    .Parameters.AddWithValue("@19", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                    dtp_jatuh_tempo.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub simpan_tbstokexproses_baris_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_1 As String = txt_total_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_1.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_1.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_1.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_1.Text)
                    .Parameters.AddWithValue("@6", txt_warna_1.Text)
                    .Parameters.AddWithValue("@7", txt_resep_1.Text)
                    .Parameters.AddWithValue("@8", txt_partai_1.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_1.Text)
                    .Parameters.AddWithValue("@10", meter_1.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_1.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_1.Replace(",", ".")) / Val(meter_1.Replace(",", "."))) + Val(harga_1.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_2 As String = txt_total_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_2.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_2.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_2.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_2.Text)
                    .Parameters.AddWithValue("@6", txt_warna_2.Text)
                    .Parameters.AddWithValue("@7", txt_resep_2.Text)
                    .Parameters.AddWithValue("@8", txt_partai_2.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_2.Text)
                    .Parameters.AddWithValue("@10", meter_2.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_2.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_2.Replace(",", ".")) / Val(meter_2.Replace(",", "."))) + Val(harga_2.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_3 As String = txt_total_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_3.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_3.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_3.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_3.Text)
                    .Parameters.AddWithValue("@6", txt_warna_3.Text)
                    .Parameters.AddWithValue("@7", txt_resep_3.Text)
                    .Parameters.AddWithValue("@8", txt_partai_3.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_3.Text)
                    .Parameters.AddWithValue("@10", meter_3.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_3.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_3.Replace(",", ".")) / Val(meter_3.Replace(",", "."))) + Val(harga_3.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_4 As String = txt_total_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_4.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_4.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_4.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_4.Text)
                    .Parameters.AddWithValue("@6", txt_warna_4.Text)
                    .Parameters.AddWithValue("@7", txt_resep_4.Text)
                    .Parameters.AddWithValue("@8", txt_partai_4.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_4.Text)
                    .Parameters.AddWithValue("@10", meter_4.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_4.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_4.Replace(",", ".")) / Val(meter_4.Replace(",", "."))) + Val(harga_4.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_5 As String = txt_total_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_5.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_5.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_5.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_5.Text)
                    .Parameters.AddWithValue("@6", txt_warna_5.Text)
                    .Parameters.AddWithValue("@7", txt_resep_5.Text)
                    .Parameters.AddWithValue("@8", txt_partai_5.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_5.Text)
                    .Parameters.AddWithValue("@10", meter_5.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_5.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_5.Replace(",", ".")) / Val(meter_5.Replace(",", "."))) + Val(harga_5.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_6 As String = txt_total_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_6.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_6.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_6.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_6.Text)
                    .Parameters.AddWithValue("@6", txt_warna_6.Text)
                    .Parameters.AddWithValue("@7", txt_resep_6.Text)
                    .Parameters.AddWithValue("@8", txt_partai_6.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_6.Text)
                    .Parameters.AddWithValue("@10", meter_6.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_6.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_6.Replace(",", ".")) / Val(meter_6.Replace(",", "."))) + Val(harga_6.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_7 As String = txt_total_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_7.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_7.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_7.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_7.Text)
                    .Parameters.AddWithValue("@6", txt_warna_7.Text)
                    .Parameters.AddWithValue("@7", txt_resep_7.Text)
                    .Parameters.AddWithValue("@8", txt_partai_7.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_7.Text)
                    .Parameters.AddWithValue("@10", meter_7.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_7.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_7.Replace(",", ".")) / Val(meter_7.Replace(",", "."))) + Val(harga_7.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_8 As String = txt_total_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_8.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_8.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_8.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_8.Text)
                    .Parameters.AddWithValue("@6", txt_warna_8.Text)
                    .Parameters.AddWithValue("@7", txt_resep_8.Text)
                    .Parameters.AddWithValue("@8", txt_partai_8.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_8.Text)
                    .Parameters.AddWithValue("@10", meter_8.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_8.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_8.Replace(",", ".")) / Val(meter_8.Replace(",", "."))) + Val(harga_8.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_9 As String = txt_total_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_9 As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_9.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_9.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_9.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_9.Text)
                    .Parameters.AddWithValue("@6", txt_warna_9.Text)
                    .Parameters.AddWithValue("@7", txt_resep_9.Text)
                    .Parameters.AddWithValue("@8", txt_partai_9.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_9.Text)
                    .Parameters.AddWithValue("@10", meter_9.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_9.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_9.Replace(",", ".")) / Val(meter_9.Replace(",", "."))) + Val(harga_9.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub
    Private Sub simpan_tbstokexproses_baris_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "INSERT INTO tbstokexproses (Tanggal,Id_Grey,Id_SJ_Proses,SJ_Proses,Gudang,Jenis_Kain,Warna,Resep,Partai,Gulung,Stok,Satuan,Harga,Tambah1,Tambah2,Tambah3) VALUES (@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim total_harga_10 As String = txt_total_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim harga_10 As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    dtp_tanggal.CustomFormat = "yyyy/MM/dd"
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@0", dtp_tanggal.Text)
                    .Parameters.AddWithValue("@1", txt_id_grey_10.Text)
                    .Parameters.AddWithValue("@2", txt_id_sj_proses_10.Text)
                    .Parameters.AddWithValue("@3", txt_surat_jalan_proses.Text)
                    .Parameters.AddWithValue("@4", txt_gudang_packing_10.Text)
                    .Parameters.AddWithValue("@5", txt_jenis_kain_10.Text)
                    .Parameters.AddWithValue("@6", txt_warna_10.Text)
                    .Parameters.AddWithValue("@7", txt_resep_10.Text)
                    .Parameters.AddWithValue("@8", txt_partai_10.Text)
                    .Parameters.AddWithValue("@9", txt_gulung_10.Text)
                    .Parameters.AddWithValue("@10", meter_10.Replace(",", "."))
                    .Parameters.AddWithValue("@11", cb_satuan_10.Text)
                    .Parameters.AddWithValue("@12", Math.Round((Val(total_harga_10.Replace(",", ".")) / Val(meter_10.Replace(",", "."))) + Val(harga_10.Replace(",", ".")), 0))
                    .Parameters.AddWithValue("@13", "")
                    .Parameters.AddWithValue("@14", "")
                    .Parameters.AddWithValue("@15", 0)
                    .ExecuteNonQuery()
                    dtp_tanggal.CustomFormat = "dd/MM/yyyy"
                End With
            End Using
        End Using
    End Sub

    Private Sub hapus_tbsuratjalanproses_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbsuratjalanproses_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbsuratjalanproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_update_status_tbpoproses_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub
    Private Sub hapus_update_status_tbpoproses_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbpoproses SET Status=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    .Parameters.Clear()
                    .Parameters.AddWithValue("@1", "")
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
        form_po_proses.ts_perbarui.PerformClick()
    End Sub

    Private Sub hapus_tbdetilhutang_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbdetilhutang_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbdetilhutang WHERE Id_Hutang ='" & txt_id_hutang_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_tbstokexproses_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub
    Private Sub hapus_tbstokexproses_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "DELETE FROM tbstokexproses WHERE Id_SJ_proses ='" & txt_id_sj_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                cmdy.ExecuteNonQuery()
            End Using
        End Using
    End Sub

    Private Sub hapus_update_tbstokwipproses_baris_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_1 As String = txt_qty_asal_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_1 As String = txt_sisa_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_1.Text = cb_satuan_1.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_1.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_1.Text = "Meter" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_1.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_1.Text = "Yard" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_1.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_2 As String = txt_qty_asal_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_2 As String = txt_sisa_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_2.Text = cb_satuan_2.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_2.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_2.Text = "Meter" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_2.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_2.Text = "Yard" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_2.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_3 As String = txt_qty_asal_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_3 As String = txt_sisa_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_3.Text = cb_satuan_3.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_3.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_3.Text = "Meter" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_3.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_3.Text = "Yard" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_3.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_4 As String = txt_qty_asal_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_4 As String = txt_sisa_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_4.Text = cb_satuan_4.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_4.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_4.Text = "Meter" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_4.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_4.Text = "Yard" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_4.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_5 As String = txt_qty_asal_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_5 As String = txt_sisa_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_5.Text = cb_satuan_5.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_5.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_5.Text = "Meter" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_5.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_5.Text = "Yard" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_5.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_6 As String = txt_qty_asal_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_6 As String = txt_sisa_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_6.Text = cb_satuan_6.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_6.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_6.Text = "Meter" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_6.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_6.Text = "Yard" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_6.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_7 As String = txt_qty_asal_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_7 As String = txt_sisa_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_7.Text = cb_satuan_7.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_7.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_7.Text = "Meter" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_7.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_7.Text = "Yard" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_7.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_8 As String = txt_qty_asal_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_8 As String = txt_sisa_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_8.Text = cb_satuan_8.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_8.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_8.Text = "Meter" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_8.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_8.Text = "Yard" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_8.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_9 As String = txt_qty_asal_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_9 As String = txt_sisa_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_9.Text = cb_satuan_9.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_9.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_9.Text = "Meter" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_9.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_9.Text = "Yard" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_9.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokwipproses_baris_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokwipproses SET Stok=@1 WHERE Id_Po_Proses='" & txt_id_po_proses_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim qty_asal_10 As String = txt_qty_asal_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim sisa_qty_10 As String = txt_sisa_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    Dim meter_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_wip_10.Text = cb_satuan_10.Text Then
                        .Parameters.AddWithValue("@1", Val(qty_asal_10.Replace(",", ".")))
                    ElseIf txt_asal_satuan_wip_10.Text = "Meter" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_10.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_wip_10.Text = "Yard" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(qty_asal_10.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Sub hapus_update_tbstokprosesgrey_1()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_1.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_1 As String = txt_sisa_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_1.Replace(",", ".")))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_1.Replace(",", ".")))
                    ElseIf txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_1.Replace(",", ".")))
                    ElseIf txt_asal_satuan_1.Text = "Meter" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Yard" And cb_satuan_1.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_1.Replace(",", ".")))
                    ElseIf txt_asal_satuan_1.Text = "Yard" And txt_asal_satuan_rubah_1.Text = "Meter" And cb_satuan_1.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_2()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_2.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_2 As String = txt_sisa_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_2.Replace(",", ".")))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_2.Replace(",", ".")))
                    ElseIf txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_2.Replace(",", ".")))
                    ElseIf txt_asal_satuan_2.Text = "Meter" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Yard" And cb_satuan_2.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_2.Replace(",", ".")))
                    ElseIf txt_asal_satuan_2.Text = "Yard" And txt_asal_satuan_rubah_2.Text = "Meter" And cb_satuan_2.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_3()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_3.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_3 As String = txt_sisa_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_3.Replace(",", ".")))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_3.Replace(",", ".")))
                    ElseIf txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_3.Replace(",", ".")))
                    ElseIf txt_asal_satuan_3.Text = "Meter" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Yard" And cb_satuan_3.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_3.Replace(",", ".")))
                    ElseIf txt_asal_satuan_3.Text = "Yard" And txt_asal_satuan_rubah_3.Text = "Meter" And cb_satuan_3.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_4()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_4.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_4 As String = txt_sisa_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_4.Replace(",", ".")))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_4.Replace(",", ".")))
                    ElseIf txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_4.Replace(",", ".")))
                    ElseIf txt_asal_satuan_4.Text = "Meter" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Yard" And cb_satuan_4.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_4.Replace(",", ".")))
                    ElseIf txt_asal_satuan_4.Text = "Yard" And txt_asal_satuan_rubah_4.Text = "Meter" And cb_satuan_4.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_5()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_5.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_5 As String = txt_sisa_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_5.Replace(",", ".")))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_5.Replace(",", ".")))
                    ElseIf txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_5.Replace(",", ".")))
                    ElseIf txt_asal_satuan_5.Text = "Meter" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Yard" And cb_satuan_5.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_5.Replace(",", ".")))
                    ElseIf txt_asal_satuan_5.Text = "Yard" And txt_asal_satuan_rubah_5.Text = "Meter" And cb_satuan_5.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_6()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_6.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_6 As String = txt_sisa_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_6.Replace(",", ".")))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_6.Replace(",", ".")))
                    ElseIf txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_6.Replace(",", ".")))
                    ElseIf txt_asal_satuan_6.Text = "Meter" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Yard" And cb_satuan_6.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_6.Replace(",", ".")))
                    ElseIf txt_asal_satuan_6.Text = "Yard" And txt_asal_satuan_rubah_6.Text = "Meter" And cb_satuan_6.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_7()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_7.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_7 As String = txt_sisa_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_7.Replace(",", ".")))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_7.Replace(",", ".")))
                    ElseIf txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_7.Replace(",", ".")))
                    ElseIf txt_asal_satuan_7.Text = "Meter" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Yard" And cb_satuan_7.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_7.Replace(",", ".")))
                    ElseIf txt_asal_satuan_7.Text = "Yard" And txt_asal_satuan_rubah_7.Text = "Meter" And cb_satuan_7.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_8()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_8.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_8 As String = txt_sisa_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_8.Replace(",", ".")))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_8.Replace(",", ".")))
                    ElseIf txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_8.Replace(",", ".")))
                    ElseIf txt_asal_satuan_8.Text = "Meter" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Yard" And cb_satuan_8.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_8.Replace(",", ".")))
                    ElseIf txt_asal_satuan_8.Text = "Yard" And txt_asal_satuan_rubah_8.Text = "Meter" And cb_satuan_8.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_9()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_9.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_9 As String = txt_sisa_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_9.Replace(",", ".")))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_9.Replace(",", ".")))
                    ElseIf txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_9.Replace(",", ".")))
                    ElseIf txt_asal_satuan_9.Text = "Meter" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Yard" And cb_satuan_9.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_9.Replace(",", ".")))
                    ElseIf txt_asal_satuan_9.Text = "Yard" And txt_asal_satuan_rubah_9.Text = "Meter" And cb_satuan_9.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub
    Private Sub hapus_update_tbstokprosesgrey_10()
        Using cony As New MySqlConnection(sLocalConn)
            cony.Open()
            Dim sqly = "UPDATE tbstokprosesgrey SET Stok=@1 WHERE Id_Beli='" & txt_id_beli_10.Text & "'"
            Using cmdy As New MySqlCommand(sqly, cony)
                With cmdy
                    Dim sisa_qty_10 As String = txt_sisa_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
                    .Parameters.Clear()
                    If txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_10.Replace(",", ".")))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_10.Replace(",", ".")))
                    ElseIf txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_10.Replace(",", ".")))
                    ElseIf txt_asal_satuan_10.Text = "Meter" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 0.9144, 0))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 1.0936, 0))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Yard" And cb_satuan_10.Text = "Meter" Then
                        .Parameters.AddWithValue("@1", Val(sisa_qty_10.Replace(",", ".")))
                    ElseIf txt_asal_satuan_10.Text = "Yard" And txt_asal_satuan_rubah_10.Text = "Meter" And cb_satuan_10.Text = "Yard" Then
                        .Parameters.AddWithValue("@1", Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 1.0936, 0))
                    End If
                    .ExecuteNonQuery()
                End With
            End Using
        End Using
    End Sub

    Private Sub btn_batal_1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_1.Click
        Try
            form_surat_jalan_proses.Show()
            form_surat_jalan_proses.Focus()
            form_surat_jalan_proses.ts_perbarui.PerformClick()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_2.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_3.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_4.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_3()
                Call hapus_update_status_tbpoproses_3()
                Call hapus_update_tbstokprosesgrey_3()
                Call hapus_update_tbstokwipproses_baris_3()
                Call hapus_tbdetilhutang_3()
                Call hapus_tbstokexproses_3()
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_5.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_4()
                Call hapus_update_status_tbpoproses_4()
                Call hapus_update_tbstokprosesgrey_4()
                Call hapus_update_tbstokwipproses_baris_4()
                Call hapus_tbdetilhutang_4()
                Call hapus_tbstokexproses_4()
                Call hapus_tbsuratjalanproses_3()
                Call hapus_update_status_tbpoproses_3()
                Call hapus_update_tbstokprosesgrey_3()
                Call hapus_update_tbstokwipproses_baris_3()
                Call hapus_tbdetilhutang_3()
                Call hapus_tbstokexproses_3()
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_6.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_5()
                Call hapus_update_status_tbpoproses_5()
                Call hapus_update_tbstokprosesgrey_5()
                Call hapus_update_tbstokwipproses_baris_5()
                Call hapus_tbdetilhutang_5()
                Call hapus_tbstokexproses_5()
                Call hapus_tbsuratjalanproses_4()
                Call hapus_update_status_tbpoproses_4()
                Call hapus_update_tbstokprosesgrey_4()
                Call hapus_update_tbstokwipproses_baris_4()
                Call hapus_tbdetilhutang_4()
                Call hapus_tbstokexproses_4()
                Call hapus_tbsuratjalanproses_3()
                Call hapus_update_status_tbpoproses_3()
                Call hapus_update_tbstokprosesgrey_3()
                Call hapus_update_tbstokwipproses_baris_3()
                Call hapus_tbdetilhutang_3()
                Call hapus_tbstokexproses_3()
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_7.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_6()
                Call hapus_update_status_tbpoproses_6()
                Call hapus_update_tbstokprosesgrey_6()
                Call hapus_update_tbstokwipproses_baris_6()
                Call hapus_tbdetilhutang_6()
                Call hapus_tbstokexproses_6()
                Call hapus_tbsuratjalanproses_5()
                Call hapus_update_status_tbpoproses_5()
                Call hapus_update_tbstokprosesgrey_5()
                Call hapus_update_tbstokwipproses_baris_5()
                Call hapus_tbdetilhutang_5()
                Call hapus_tbstokexproses_5()
                Call hapus_tbsuratjalanproses_4()
                Call hapus_update_status_tbpoproses_4()
                Call hapus_update_tbstokprosesgrey_4()
                Call hapus_update_tbstokwipproses_baris_4()
                Call hapus_tbdetilhutang_4()
                Call hapus_tbstokexproses_4()
                Call hapus_tbsuratjalanproses_3()
                Call hapus_update_status_tbpoproses_3()
                Call hapus_update_tbstokprosesgrey_3()
                Call hapus_update_tbstokwipproses_baris_3()
                Call hapus_tbdetilhutang_3()
                Call hapus_tbstokexproses_3()
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_8.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_7()
                Call hapus_update_status_tbpoproses_7()
                Call hapus_update_tbstokprosesgrey_7()
                Call hapus_update_tbstokwipproses_baris_7()
                Call hapus_tbdetilhutang_7()
                Call hapus_tbstokexproses_7()
                Call hapus_tbsuratjalanproses_6()
                Call hapus_update_status_tbpoproses_6()
                Call hapus_update_tbstokprosesgrey_6()
                Call hapus_update_tbstokwipproses_baris_6()
                Call hapus_tbdetilhutang_6()
                Call hapus_tbstokexproses_6()
                Call hapus_tbsuratjalanproses_5()
                Call hapus_update_status_tbpoproses_5()
                Call hapus_update_tbstokprosesgrey_5()
                Call hapus_update_tbstokwipproses_baris_5()
                Call hapus_tbdetilhutang_5()
                Call hapus_tbstokexproses_5()
                Call hapus_tbsuratjalanproses_4()
                Call hapus_update_status_tbpoproses_4()
                Call hapus_update_tbstokprosesgrey_4()
                Call hapus_update_tbstokwipproses_baris_4()
                Call hapus_tbdetilhutang_4()
                Call hapus_tbstokexproses_4()
                Call hapus_tbsuratjalanproses_3()
                Call hapus_update_status_tbpoproses_3()
                Call hapus_update_tbstokprosesgrey_3()
                Call hapus_update_tbstokwipproses_baris_3()
                Call hapus_tbdetilhutang_3()
                Call hapus_tbstokexproses_3()
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_9.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_8()
                Call hapus_update_status_tbpoproses_8()
                Call hapus_update_tbstokprosesgrey_8()
                Call hapus_update_tbstokwipproses_baris_8()
                Call hapus_tbdetilhutang_8()
                Call hapus_tbstokexproses_8()
                Call hapus_tbsuratjalanproses_7()
                Call hapus_update_status_tbpoproses_7()
                Call hapus_update_tbstokprosesgrey_7()
                Call hapus_update_tbstokwipproses_baris_7()
                Call hapus_tbdetilhutang_7()
                Call hapus_tbstokexproses_7()
                Call hapus_tbsuratjalanproses_6()
                Call hapus_update_status_tbpoproses_6()
                Call hapus_update_tbstokprosesgrey_6()
                Call hapus_update_tbstokwipproses_baris_6()
                Call hapus_tbdetilhutang_6()
                Call hapus_tbstokexproses_6()
                Call hapus_tbsuratjalanproses_5()
                Call hapus_update_status_tbpoproses_5()
                Call hapus_update_tbstokprosesgrey_5()
                Call hapus_update_tbstokwipproses_baris_5()
                Call hapus_tbdetilhutang_5()
                Call hapus_tbstokexproses_5()
                Call hapus_tbsuratjalanproses_4()
                Call hapus_update_status_tbpoproses_4()
                Call hapus_update_tbstokprosesgrey_4()
                Call hapus_update_tbstokwipproses_baris_4()
                Call hapus_tbdetilhutang_4()
                Call hapus_tbstokexproses_4()
                Call hapus_tbsuratjalanproses_3()
                Call hapus_update_status_tbpoproses_3()
                Call hapus_update_tbstokprosesgrey_3()
                Call hapus_update_tbstokwipproses_baris_3()
                Call hapus_tbdetilhutang_3()
                Call hapus_tbstokexproses_3()
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub btn_batal_10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal_10.Click
        Try
            If MsgBox("Yakin SURAT JALAN Akan DIBATALKAN ?", vbYesNo + vbQuestion, "Konfirmasi") = vbYes Then
                Call hapus_tbsuratjalanproses_9()
                Call hapus_update_status_tbpoproses_9()
                Call hapus_update_tbstokprosesgrey_9()
                Call hapus_update_tbstokwipproses_baris_9()
                Call hapus_tbdetilhutang_9()
                Call hapus_tbstokexproses_9()
                Call hapus_tbsuratjalanproses_8()
                Call hapus_update_status_tbpoproses_8()
                Call hapus_update_tbstokprosesgrey_8()
                Call hapus_update_tbstokwipproses_baris_8()
                Call hapus_tbdetilhutang_8()
                Call hapus_tbstokexproses_8()
                Call hapus_tbsuratjalanproses_7()
                Call hapus_update_status_tbpoproses_7()
                Call hapus_update_tbstokprosesgrey_7()
                Call hapus_update_tbstokwipproses_baris_7()
                Call hapus_tbdetilhutang_7()
                Call hapus_tbstokexproses_7()
                Call hapus_tbsuratjalanproses_6()
                Call hapus_update_status_tbpoproses_6()
                Call hapus_update_tbstokprosesgrey_6()
                Call hapus_update_tbstokwipproses_baris_6()
                Call hapus_tbdetilhutang_6()
                Call hapus_tbstokexproses_6()
                Call hapus_tbsuratjalanproses_5()
                Call hapus_update_status_tbpoproses_5()
                Call hapus_update_tbstokprosesgrey_5()
                Call hapus_update_tbstokwipproses_baris_5()
                Call hapus_tbdetilhutang_5()
                Call hapus_tbstokexproses_5()
                Call hapus_tbsuratjalanproses_4()
                Call hapus_update_status_tbpoproses_4()
                Call hapus_update_tbstokprosesgrey_4()
                Call hapus_update_tbstokwipproses_baris_4()
                Call hapus_tbdetilhutang_4()
                Call hapus_tbstokexproses_4()
                Call hapus_tbsuratjalanproses_3()
                Call hapus_update_status_tbpoproses_3()
                Call hapus_update_tbstokprosesgrey_3()
                Call hapus_update_tbstokwipproses_baris_3()
                Call hapus_tbdetilhutang_3()
                Call hapus_tbstokexproses_3()
                Call hapus_tbsuratjalanproses_2()
                Call hapus_update_status_tbpoproses_2()
                Call hapus_update_tbstokprosesgrey_2()
                Call hapus_update_tbstokwipproses_baris_2()
                Call hapus_tbdetilhutang_2()
                Call hapus_tbstokexproses_2()
                Call hapus_tbsuratjalanproses_1()
                Call hapus_update_status_tbpoproses_1()
                Call hapus_update_tbstokprosesgrey_1()
                Call hapus_update_tbstokwipproses_baris_1()
                Call hapus_tbdetilhutang_1()
                Call hapus_tbstokexproses_1()
                form_surat_jalan_proses.Show()
                form_surat_jalan_proses.Focus()
                form_surat_jalan_proses.ts_perbarui.PerformClick()
                Me.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub cb_satuan_1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_1.TextChanged
        Dim qty_1 As String = txt_meter_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_1 As String = txt_qty_asal_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_1 As String = txt_harga_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_1 As String = txt_sisa_qty_1.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_1, s_1, h_1, sq_1 As Double
        If txt_meter_1.Text = "" Then
        ElseIf Val(txt_meter_1.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_1.Text = "Meter" Then
                If cb_satuan_1.Text = "Meter" Then
                    s_1 = Math.Round(Val(qty_asal_1.Replace(",", ".")) * 0.9144, 0)
                    sq_1 = Math.Round(Val(sisa_qty_1.Replace(",", ".")), 0)
                    q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                    h_1 = Math.Round(Val(harga_1.Replace(",", ".")), 0)
                ElseIf cb_satuan_1.Text = "Yard" Then
                    s_1 = Math.Round(Val(qty_asal_1.Replace(",", ".")) * 1.0936, 0)
                    sq_1 = Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 1.0936, 0)
                    q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                    h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_1.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_1.Text = "Yard" Then
                If cb_satuan_1.Text = "Meter" Then
                    s_1 = Math.Round(Val(qty_asal_1.Replace(",", ".")) * 0.9144, 0)
                    sq_1 = Math.Round(Val(sisa_qty_1.Replace(",", ".")) * 0.9144, 0)
                    q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 0.9144, 0)
                    h_1 = Math.Round(Val(harga_1.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_1.Text = "Meter"
                ElseIf cb_satuan_1.Text = "Yard" Then
                    s_1 = Math.Round(Val(qty_asal_1.Replace(",", ".")) * 1.0936, 0)
                    sq_1 = Math.Round(Val(sisa_qty_1.Replace(",", ".")), 0)
                    q_1 = Math.Round(Val(qty_1.Replace(",", ".")) * 1.0936, 0)
                    h_1 = Math.Round(Val(harga_1.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_1.Text = h_1
            txt_qty_asal_1.Text = s_1
            txt_meter_1.Text = q_1
            txt_sisa_qty_1.Text = sq_1
            txt_kiloan_1.Focus()
        End If
    End Sub
    Private Sub cb_satuan_2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_2.TextChanged
        Dim qty_2 As String = txt_meter_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_2 As String = txt_qty_asal_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_2 As String = txt_harga_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_2 As String = txt_sisa_qty_2.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_2, s_2, h_2, sq_2 As Double
        If txt_meter_2.Text = "" Then
        ElseIf Val(txt_meter_2.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_2.Text = "Meter" Then
                If cb_satuan_2.Text = "Meter" Then
                    s_2 = Math.Round(Val(qty_asal_2.Replace(",", ".")) * 0.9144, 0)
                    sq_2 = Math.Round(Val(sisa_qty_2.Replace(",", ".")), 0)
                    q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                    h_2 = Math.Round(Val(harga_2.Replace(",", ".")), 0)
                ElseIf cb_satuan_2.Text = "Yard" Then
                    s_2 = Math.Round(Val(qty_asal_2.Replace(",", ".")) * 1.0936, 0)
                    sq_2 = Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 1.0936, 0)
                    q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                    h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_2.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_2.Text = "Yard" Then
                If cb_satuan_2.Text = "Meter" Then
                    s_2 = Math.Round(Val(qty_asal_2.Replace(",", ".")) * 0.9144, 0)
                    sq_2 = Math.Round(Val(sisa_qty_2.Replace(",", ".")) * 0.9144, 0)
                    q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 0.9144, 0)
                    h_2 = Math.Round(Val(harga_2.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_2.Text = "Meter"
                ElseIf cb_satuan_2.Text = "Yard" Then
                    s_2 = Math.Round(Val(qty_asal_2.Replace(",", ".")) * 1.0936, 0)
                    sq_2 = Math.Round(Val(sisa_qty_2.Replace(",", ".")), 0)
                    q_2 = Math.Round(Val(qty_2.Replace(",", ".")) * 1.0936, 0)
                    h_2 = Math.Round(Val(harga_2.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_2.Text = h_2
            txt_qty_asal_2.Text = s_2
            txt_meter_2.Text = q_2
            txt_sisa_qty_2.Text = sq_2
            txt_kiloan_2.Focus()
        End If
    End Sub
    Private Sub cb_satuan_3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_3.TextChanged
        Dim qty_3 As String = txt_meter_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_3 As String = txt_qty_asal_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_3 As String = txt_harga_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_3 As String = txt_sisa_qty_3.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_3, s_3, h_3, sq_3 As Double
        If txt_meter_3.Text = "" Then
        ElseIf Val(txt_meter_3.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_3.Text = "Meter" Then
                If cb_satuan_3.Text = "Meter" Then
                    s_3 = Math.Round(Val(qty_asal_3.Replace(",", ".")) * 0.9144, 0)
                    sq_3 = Math.Round(Val(sisa_qty_3.Replace(",", ".")), 0)
                    q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                    h_3 = Math.Round(Val(harga_3.Replace(",", ".")), 0)
                ElseIf cb_satuan_3.Text = "Yard" Then
                    s_3 = Math.Round(Val(qty_asal_3.Replace(",", ".")) * 1.0936, 0)
                    sq_3 = Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 1.0936, 0)
                    q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                    h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_3.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_3.Text = "Yard" Then
                If cb_satuan_3.Text = "Meter" Then
                    s_3 = Math.Round(Val(qty_asal_3.Replace(",", ".")) * 0.9144, 0)
                    sq_3 = Math.Round(Val(sisa_qty_3.Replace(",", ".")) * 0.9144, 0)
                    q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 0.9144, 0)
                    h_3 = Math.Round(Val(harga_3.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_3.Text = "Meter"
                ElseIf cb_satuan_3.Text = "Yard" Then
                    s_3 = Math.Round(Val(qty_asal_3.Replace(",", ".")) * 1.0936, 0)
                    sq_3 = Math.Round(Val(sisa_qty_3.Replace(",", ".")), 0)
                    q_3 = Math.Round(Val(qty_3.Replace(",", ".")) * 1.0936, 0)
                    h_3 = Math.Round(Val(harga_3.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_3.Text = h_3
            txt_qty_asal_3.Text = s_3
            txt_meter_3.Text = q_3
            txt_sisa_qty_3.Text = sq_3
            txt_kiloan_3.Focus()
        End If
    End Sub
    Private Sub cb_satuan_4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_4.TextChanged
        Dim qty_4 As String = txt_meter_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_4 As String = txt_qty_asal_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_4 As String = txt_harga_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_4 As String = txt_sisa_qty_4.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_4, s_4, h_4, sq_4 As Double
        If txt_meter_4.Text = "" Then
        ElseIf Val(txt_meter_4.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_4.Text = "Meter" Then
                If cb_satuan_4.Text = "Meter" Then
                    s_4 = Math.Round(Val(qty_asal_4.Replace(",", ".")) * 0.9144, 0)
                    sq_4 = Math.Round(Val(sisa_qty_4.Replace(",", ".")), 0)
                    q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                    h_4 = Math.Round(Val(harga_4.Replace(",", ".")), 0)
                ElseIf cb_satuan_4.Text = "Yard" Then
                    s_4 = Math.Round(Val(qty_asal_4.Replace(",", ".")) * 1.0936, 0)
                    sq_4 = Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 1.0936, 0)
                    q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                    h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_4.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_4.Text = "Yard" Then
                If cb_satuan_4.Text = "Meter" Then
                    s_4 = Math.Round(Val(qty_asal_4.Replace(",", ".")) * 0.9144, 0)
                    sq_4 = Math.Round(Val(sisa_qty_4.Replace(",", ".")) * 0.9144, 0)
                    q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 0.9144, 0)
                    h_4 = Math.Round(Val(harga_4.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_4.Text = "Meter"
                ElseIf cb_satuan_4.Text = "Yard" Then
                    s_4 = Math.Round(Val(qty_asal_4.Replace(",", ".")) * 1.0936, 0)
                    sq_4 = Math.Round(Val(sisa_qty_4.Replace(",", ".")), 0)
                    q_4 = Math.Round(Val(qty_4.Replace(",", ".")) * 1.0936, 0)
                    h_4 = Math.Round(Val(harga_4.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_4.Text = h_4
            txt_qty_asal_4.Text = s_4
            txt_meter_4.Text = q_4
            txt_sisa_qty_4.Text = sq_4
            txt_kiloan_4.Focus()
        End If
    End Sub
    Private Sub cb_satuan_5_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_5.TextChanged
        Dim qty_5 As String = txt_meter_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_5 As String = txt_qty_asal_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_5 As String = txt_harga_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_5 As String = txt_sisa_qty_5.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_5, s_5, h_5, sq_5 As Double
        If txt_meter_5.Text = "" Then
        ElseIf Val(txt_meter_5.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_5.Text = "Meter" Then
                If cb_satuan_5.Text = "Meter" Then
                    s_5 = Math.Round(Val(qty_asal_5.Replace(",", ".")) * 0.9144, 0)
                    sq_5 = Math.Round(Val(sisa_qty_5.Replace(",", ".")), 0)
                    q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                    h_5 = Math.Round(Val(harga_5.Replace(",", ".")), 0)
                ElseIf cb_satuan_5.Text = "Yard" Then
                    s_5 = Math.Round(Val(qty_asal_5.Replace(",", ".")) * 1.0936, 0)
                    sq_5 = Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 1.0936, 0)
                    q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                    h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_5.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_5.Text = "Yard" Then
                If cb_satuan_5.Text = "Meter" Then
                    s_5 = Math.Round(Val(qty_asal_5.Replace(",", ".")) * 0.9144, 0)
                    sq_5 = Math.Round(Val(sisa_qty_5.Replace(",", ".")) * 0.9144, 0)
                    q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 0.9144, 0)
                    h_5 = Math.Round(Val(harga_5.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_5.Text = "Meter"
                ElseIf cb_satuan_5.Text = "Yard" Then
                    s_5 = Math.Round(Val(qty_asal_5.Replace(",", ".")) * 1.0936, 0)
                    sq_5 = Math.Round(Val(sisa_qty_5.Replace(",", ".")), 0)
                    q_5 = Math.Round(Val(qty_5.Replace(",", ".")) * 1.0936, 0)
                    h_5 = Math.Round(Val(harga_5.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_5.Text = h_5
            txt_qty_asal_5.Text = s_5
            txt_meter_5.Text = q_5
            txt_sisa_qty_5.Text = sq_5
            txt_kiloan_5.Focus()
        End If
    End Sub
    Private Sub cb_satuan_6_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_6.TextChanged
        Dim qty_6 As String = txt_meter_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_6 As String = txt_qty_asal_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_6 As String = txt_harga_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_6 As String = txt_sisa_qty_6.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_6, s_6, h_6, sq_6 As Double
        If txt_meter_6.Text = "" Then
        ElseIf Val(txt_meter_6.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_6.Text = "Meter" Then
                If cb_satuan_6.Text = "Meter" Then
                    s_6 = Math.Round(Val(qty_asal_6.Replace(",", ".")) * 0.9144, 0)
                    sq_6 = Math.Round(Val(sisa_qty_6.Replace(",", ".")), 0)
                    q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                    h_6 = Math.Round(Val(harga_6.Replace(",", ".")), 0)
                ElseIf cb_satuan_6.Text = "Yard" Then
                    s_6 = Math.Round(Val(qty_asal_6.Replace(",", ".")) * 1.0936, 0)
                    sq_6 = Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 1.0936, 0)
                    q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                    h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_6.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_6.Text = "Yard" Then
                If cb_satuan_6.Text = "Meter" Then
                    s_6 = Math.Round(Val(qty_asal_6.Replace(",", ".")) * 0.9144, 0)
                    sq_6 = Math.Round(Val(sisa_qty_6.Replace(",", ".")) * 0.9144, 0)
                    q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 0.9144, 0)
                    h_6 = Math.Round(Val(harga_6.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_6.Text = "Meter"
                ElseIf cb_satuan_6.Text = "Yard" Then
                    s_6 = Math.Round(Val(qty_asal_6.Replace(",", ".")) * 1.0936, 0)
                    sq_6 = Math.Round(Val(sisa_qty_6.Replace(",", ".")), 0)
                    q_6 = Math.Round(Val(qty_6.Replace(",", ".")) * 1.0936, 0)
                    h_6 = Math.Round(Val(harga_6.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_6.Text = h_6
            txt_qty_asal_6.Text = s_6
            txt_meter_6.Text = q_6
            txt_sisa_qty_6.Text = sq_6
            txt_kiloan_6.Focus()
        End If
    End Sub
    Private Sub cb_satuan_7_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_7.TextChanged
        Dim qty_7 As String = txt_meter_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_7 As String = txt_qty_asal_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_7 As String = txt_harga_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_7 As String = txt_sisa_qty_7.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_7, s_7, h_7, sq_7 As Double
        If txt_meter_7.Text = "" Then
        ElseIf Val(txt_meter_7.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_7.Text = "Meter" Then
                If cb_satuan_7.Text = "Meter" Then
                    s_7 = Math.Round(Val(qty_asal_7.Replace(",", ".")) * 0.9144, 0)
                    sq_7 = Math.Round(Val(sisa_qty_7.Replace(",", ".")), 0)
                    q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                    h_7 = Math.Round(Val(harga_7.Replace(",", ".")), 0)
                ElseIf cb_satuan_7.Text = "Yard" Then
                    s_7 = Math.Round(Val(qty_asal_7.Replace(",", ".")) * 1.0936, 0)
                    sq_7 = Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 1.0936, 0)
                    q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                    h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_7.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_7.Text = "Yard" Then
                If cb_satuan_7.Text = "Meter" Then
                    s_7 = Math.Round(Val(qty_asal_7.Replace(",", ".")) * 0.9144, 0)
                    sq_7 = Math.Round(Val(sisa_qty_7.Replace(",", ".")) * 0.9144, 0)
                    q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 0.9144, 0)
                    h_7 = Math.Round(Val(harga_7.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_7.Text = "Meter"
                ElseIf cb_satuan_7.Text = "Yard" Then
                    s_7 = Math.Round(Val(qty_asal_7.Replace(",", ".")) * 1.0936, 0)
                    sq_7 = Math.Round(Val(sisa_qty_7.Replace(",", ".")), 0)
                    q_7 = Math.Round(Val(qty_7.Replace(",", ".")) * 1.0936, 0)
                    h_7 = Math.Round(Val(harga_7.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_7.Text = h_7
            txt_qty_asal_7.Text = s_7
            txt_meter_7.Text = q_7
            txt_sisa_qty_7.Text = sq_7
            txt_kiloan_7.Focus()
        End If
    End Sub
    Private Sub cb_satuan_8_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_8.TextChanged
        Dim qty_8 As String = txt_meter_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_8 As String = txt_qty_asal_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_8 As String = txt_harga_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_8 As String = txt_sisa_qty_8.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_8, s_8, h_8, sq_8 As Double
        If txt_meter_8.Text = "" Then
        ElseIf Val(txt_meter_8.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_8.Text = "Meter" Then
                If cb_satuan_8.Text = "Meter" Then
                    s_8 = Math.Round(Val(qty_asal_8.Replace(",", ".")) * 0.9144, 0)
                    sq_8 = Math.Round(Val(sisa_qty_8.Replace(",", ".")), 0)
                    q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                    h_8 = Math.Round(Val(harga_8.Replace(",", ".")), 0)
                ElseIf cb_satuan_8.Text = "Yard" Then
                    s_8 = Math.Round(Val(qty_asal_8.Replace(",", ".")) * 1.0936, 0)
                    sq_8 = Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 1.0936, 0)
                    q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                    h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_8.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_8.Text = "Yard" Then
                If cb_satuan_8.Text = "Meter" Then
                    s_8 = Math.Round(Val(qty_asal_8.Replace(",", ".")) * 0.9144, 0)
                    sq_8 = Math.Round(Val(sisa_qty_8.Replace(",", ".")) * 0.9144, 0)
                    q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 0.9144, 0)
                    h_8 = Math.Round(Val(harga_8.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_8.Text = "Meter"
                ElseIf cb_satuan_8.Text = "Yard" Then
                    s_8 = Math.Round(Val(qty_asal_8.Replace(",", ".")) * 1.0936, 0)
                    sq_8 = Math.Round(Val(sisa_qty_8.Replace(",", ".")), 0)
                    q_8 = Math.Round(Val(qty_8.Replace(",", ".")) * 1.0936, 0)
                    h_8 = Math.Round(Val(harga_8.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_8.Text = h_8
            txt_qty_asal_8.Text = s_8
            txt_meter_8.Text = q_8
            txt_sisa_qty_8.Text = sq_8
            txt_kiloan_8.Focus()
        End If
    End Sub
    Private Sub cb_satuan_9_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_9.TextChanged
        Dim qty_9 As String = txt_meter_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_9 As String = txt_qty_asal_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_9 As String = txt_harga_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_9 As String = txt_sisa_qty_9.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_9, s_9, h_9, sq_9 As Double
        If txt_meter_9.Text = "" Then
        ElseIf Val(txt_meter_9.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_9.Text = "Meter" Then
                If cb_satuan_9.Text = "Meter" Then
                    s_9 = Math.Round(Val(qty_asal_9.Replace(",", ".")) * 0.9144, 0)
                    sq_9 = Math.Round(Val(sisa_qty_9.Replace(",", ".")), 0)
                    q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 0.9144, 0)
                    h_9 = Math.Round(Val(harga_9.Replace(",", ".")), 0)
                ElseIf cb_satuan_9.Text = "Yard" Then
                    s_9 = Math.Round(Val(qty_asal_9.Replace(",", ".")) * 1.0936, 0)
                    sq_9 = Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 1.0936, 0)
                    q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 1.0936, 0)
                    h_9 = Math.Round(Val(harga_9.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_9.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_9.Text = "Yard" Then
                If cb_satuan_9.Text = "Meter" Then
                    s_9 = Math.Round(Val(qty_asal_9.Replace(",", ".")) * 0.9144, 0)
                    sq_9 = Math.Round(Val(sisa_qty_9.Replace(",", ".")) * 0.9144, 0)
                    q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 0.9144, 0)
                    h_9 = Math.Round(Val(harga_9.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_9.Text = "Meter"
                ElseIf cb_satuan_9.Text = "Yard" Then
                    s_9 = Math.Round(Val(qty_asal_9.Replace(",", ".")) * 1.0936, 0)
                    sq_9 = Math.Round(Val(sisa_qty_9.Replace(",", ".")), 0)
                    q_9 = Math.Round(Val(qty_9.Replace(",", ".")) * 1.0936, 0)
                    h_9 = Math.Round(Val(harga_9.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_9.Text = h_9
            txt_qty_asal_9.Text = s_9
            txt_meter_9.Text = q_9
            txt_sisa_qty_9.Text = sq_9
            txt_kiloan_9.Focus()
        End If
    End Sub
    Private Sub cb_satuan_10_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan_10.TextChanged
        Dim qty_10 As String = txt_meter_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim qty_asal_10 As String = txt_qty_asal_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim harga_10 As String = txt_harga_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim sisa_qty_10 As String = txt_sisa_qty_10.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
        Dim q_10, s_10, h_10, sq_10 As Double
        If txt_meter_10.Text = "" Then
        ElseIf Val(txt_meter_10.Text) = 0 Then
        Else
            If txt_asal_satuan_rubah_10.Text = "Meter" Then
                If cb_satuan_10.Text = "Meter" Then
                    s_10 = Math.Round(Val(qty_asal_10.Replace(",", ".")) * 0.9144, 0)
                    sq_10 = Math.Round(Val(sisa_qty_10.Replace(",", ".")), 0)
                    q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 0.9144, 0)
                    h_10 = Math.Round(Val(harga_10.Replace(",", ".")), 0)
                ElseIf cb_satuan_10.Text = "Yard" Then
                    s_10 = Math.Round(Val(qty_asal_10.Replace(",", ".")) * 1.0936, 0)
                    sq_10 = Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 1.0936, 0)
                    q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 1.0936, 0)
                    h_10 = Math.Round(Val(harga_10.Replace(",", ".")) / 1.0936, 0)
                    txt_asal_satuan_rubah_10.Text = "Yard"
                End If
            ElseIf txt_asal_satuan_rubah_10.Text = "Yard" Then
                If cb_satuan_10.Text = "Meter" Then
                    s_10 = Math.Round(Val(qty_asal_10.Replace(",", ".")) * 0.9144, 0)
                    sq_10 = Math.Round(Val(sisa_qty_10.Replace(",", ".")) * 0.9144, 0)
                    q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 0.9144, 0)
                    h_10 = Math.Round(Val(harga_10.Replace(",", ".")) / 0.9144, 0)
                    txt_asal_satuan_rubah_10.Text = "Meter"
                ElseIf cb_satuan_10.Text = "Yard" Then
                    s_10 = Math.Round(Val(qty_asal_10.Replace(",", ".")) * 1.0936, 0)
                    sq_10 = Math.Round(Val(sisa_qty_10.Replace(",", ".")), 0)
                    q_10 = Math.Round(Val(qty_10.Replace(",", ".")) * 1.0936, 0)
                    h_10 = Math.Round(Val(harga_10.Replace(",", ".")), 0)
                End If
            End If
            txt_harga_10.Text = h_10
            txt_qty_asal_10.Text = s_10
            txt_meter_10.Text = q_10
            txt_sisa_qty_10.Text = sq_10
            txt_kiloan_10.Focus()
        End If
    End Sub
End Class