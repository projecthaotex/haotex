﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_po_packing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_input_po_packing))
        Me.txt_keterangan_3 = New System.Windows.Forms.TextBox()
        Me.txt_customer_3 = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel_1 = New System.Windows.Forms.Panel()
        Me.cb_satuan_1 = New System.Windows.Forms.ComboBox()
        Me.txt_aksesoris_1 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_1 = New System.Windows.Forms.TextBox()
        Me.txt_customer_1 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_1 = New System.Windows.Forms.TextBox()
        Me.txt_partai_1 = New System.Windows.Forms.TextBox()
        Me.txt_meter_1 = New System.Windows.Forms.TextBox()
        Me.txt_resep_1 = New System.Windows.Forms.TextBox()
        Me.txt_warna_1 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_1 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_1 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_1 = New System.Windows.Forms.Label()
        Me.txt_aksesoris_3 = New System.Windows.Forms.TextBox()
        Me.txt_warna_6 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_6 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_6 = New System.Windows.Forms.Label()
        Me.Panel_5 = New System.Windows.Forms.Panel()
        Me.cb_satuan_5 = New System.Windows.Forms.ComboBox()
        Me.txt_gulung_5 = New System.Windows.Forms.TextBox()
        Me.txt_aksesoris_5 = New System.Windows.Forms.TextBox()
        Me.txt_customer_5 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_5 = New System.Windows.Forms.TextBox()
        Me.txt_partai_5 = New System.Windows.Forms.TextBox()
        Me.txt_meter_5 = New System.Windows.Forms.TextBox()
        Me.txt_resep_5 = New System.Windows.Forms.TextBox()
        Me.txt_warna_5 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_5 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_5 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_5 = New System.Windows.Forms.Label()
        Me.Panel_4 = New System.Windows.Forms.Panel()
        Me.cb_satuan_4 = New System.Windows.Forms.ComboBox()
        Me.txt_customer_4 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_4 = New System.Windows.Forms.TextBox()
        Me.txt_partai_4 = New System.Windows.Forms.TextBox()
        Me.txt_aksesoris_4 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_4 = New System.Windows.Forms.TextBox()
        Me.txt_meter_4 = New System.Windows.Forms.TextBox()
        Me.txt_resep_4 = New System.Windows.Forms.TextBox()
        Me.txt_warna_4 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_4 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_4 = New System.Windows.Forms.Label()
        Me.txt_gulung_3 = New System.Windows.Forms.TextBox()
        Me.txt_partai_3 = New System.Windows.Forms.TextBox()
        Me.txt_meter_3 = New System.Windows.Forms.TextBox()
        Me.txt_resep_3 = New System.Windows.Forms.TextBox()
        Me.txt_warna_3 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_3 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_3 = New System.Windows.Forms.Label()
        Me.Panel_3 = New System.Windows.Forms.Panel()
        Me.cb_satuan_3 = New System.Windows.Forms.ComboBox()
        Me.Panel_2 = New System.Windows.Forms.Panel()
        Me.cb_satuan_2 = New System.Windows.Forms.ComboBox()
        Me.txt_aksesoris_2 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_2 = New System.Windows.Forms.TextBox()
        Me.txt_customer_2 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_2 = New System.Windows.Forms.TextBox()
        Me.txt_partai_2 = New System.Windows.Forms.TextBox()
        Me.txt_meter_2 = New System.Windows.Forms.TextBox()
        Me.txt_resep_2 = New System.Windows.Forms.TextBox()
        Me.txt_warna_2 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_2 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_2 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btn_batal_10 = New System.Windows.Forms.Button()
        Me.btn_batal_9 = New System.Windows.Forms.Button()
        Me.btn_batal_8 = New System.Windows.Forms.Button()
        Me.btn_batal_7 = New System.Windows.Forms.Button()
        Me.btn_batal_6 = New System.Windows.Forms.Button()
        Me.btn_batal_5 = New System.Windows.Forms.Button()
        Me.btn_batal_4 = New System.Windows.Forms.Button()
        Me.btn_batal_3 = New System.Windows.Forms.Button()
        Me.btn_batal_2 = New System.Windows.Forms.Button()
        Me.btn_batal_1 = New System.Windows.Forms.Button()
        Me.btn_hapus_10 = New System.Windows.Forms.Button()
        Me.btn_selesai_10 = New System.Windows.Forms.Button()
        Me.btn_hapus_9 = New System.Windows.Forms.Button()
        Me.btn_selesai_9 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_9 = New System.Windows.Forms.Button()
        Me.btn_hapus_8 = New System.Windows.Forms.Button()
        Me.btn_selesai_8 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_8 = New System.Windows.Forms.Button()
        Me.btn_hapus_7 = New System.Windows.Forms.Button()
        Me.btn_selesai_7 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_7 = New System.Windows.Forms.Button()
        Me.btn_hapus_6 = New System.Windows.Forms.Button()
        Me.btn_selesai_6 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_6 = New System.Windows.Forms.Button()
        Me.btn_hapus_5 = New System.Windows.Forms.Button()
        Me.btn_selesai_5 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_5 = New System.Windows.Forms.Button()
        Me.btn_hapus_4 = New System.Windows.Forms.Button()
        Me.btn_selesai_4 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_4 = New System.Windows.Forms.Button()
        Me.btn_hapus_3 = New System.Windows.Forms.Button()
        Me.btn_selesai_3 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_3 = New System.Windows.Forms.Button()
        Me.btn_hapus_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_2 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_2 = New System.Windows.Forms.Button()
        Me.btn_selesai_1 = New System.Windows.Forms.Button()
        Me.btn_tambah_baris_1 = New System.Windows.Forms.Button()
        Me.txt_id_grey_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_1 = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txt_qty_asal_10 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_9 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_8 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_7 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_6 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_5 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_4 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_3 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_2 = New System.Windows.Forms.TextBox()
        Me.txt_qty_asal_1 = New System.Windows.Forms.TextBox()
        Me.Panel_10 = New System.Windows.Forms.Panel()
        Me.cb_satuan_10 = New System.Windows.Forms.ComboBox()
        Me.txt_customer_10 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_10 = New System.Windows.Forms.TextBox()
        Me.txt_partai_10 = New System.Windows.Forms.TextBox()
        Me.txt_aksesoris_10 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_10 = New System.Windows.Forms.TextBox()
        Me.txt_meter_10 = New System.Windows.Forms.TextBox()
        Me.txt_resep_10 = New System.Windows.Forms.TextBox()
        Me.txt_warna_10 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_10 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_10 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_10 = New System.Windows.Forms.Label()
        Me.txt_id_grey_2 = New System.Windows.Forms.TextBox()
        Me.Panel_9 = New System.Windows.Forms.Panel()
        Me.cb_satuan_9 = New System.Windows.Forms.ComboBox()
        Me.txt_partai_9 = New System.Windows.Forms.TextBox()
        Me.txt_customer_9 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_9 = New System.Windows.Forms.TextBox()
        Me.txt_aksesoris_9 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_9 = New System.Windows.Forms.TextBox()
        Me.txt_meter_9 = New System.Windows.Forms.TextBox()
        Me.txt_resep_9 = New System.Windows.Forms.TextBox()
        Me.txt_warna_9 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_9 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_9 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_9 = New System.Windows.Forms.Label()
        Me.txt_resep_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_4 = New System.Windows.Forms.TextBox()
        Me.txt_harga_10 = New System.Windows.Forms.TextBox()
        Me.txt_harga_9 = New System.Windows.Forms.TextBox()
        Me.txt_harga_8 = New System.Windows.Forms.TextBox()
        Me.txt_harga_7 = New System.Windows.Forms.TextBox()
        Me.txt_harga_6 = New System.Windows.Forms.TextBox()
        Me.txt_harga_5 = New System.Windows.Forms.TextBox()
        Me.txt_harga_4 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.dtp_awal = New System.Windows.Forms.DateTimePicker()
        Me.txt_gudang = New System.Windows.Forms.TextBox()
        Me.txt_no_po = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txt_harga_3 = New System.Windows.Forms.TextBox()
        Me.txt_harga_2 = New System.Windows.Forms.TextBox()
        Me.txt_harga_1 = New System.Windows.Forms.TextBox()
        Me.dtp_hari_ini = New System.Windows.Forms.DateTimePicker()
        Me.txt_id_grey_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_grey_10 = New System.Windows.Forms.TextBox()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Panel_6 = New System.Windows.Forms.Panel()
        Me.cb_satuan_6 = New System.Windows.Forms.ComboBox()
        Me.txt_customer_6 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_6 = New System.Windows.Forms.TextBox()
        Me.txt_partai_6 = New System.Windows.Forms.TextBox()
        Me.txt_aksesoris_6 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_6 = New System.Windows.Forms.TextBox()
        Me.txt_meter_6 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_7 = New System.Windows.Forms.TextBox()
        Me.txt_customer_7 = New System.Windows.Forms.TextBox()
        Me.txt_aksesoris_7 = New System.Windows.Forms.TextBox()
        Me.txt_partai_7 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_7 = New System.Windows.Forms.TextBox()
        Me.Panel_8 = New System.Windows.Forms.Panel()
        Me.cb_satuan_8 = New System.Windows.Forms.ComboBox()
        Me.txt_customer_8 = New System.Windows.Forms.TextBox()
        Me.txt_gulung_8 = New System.Windows.Forms.TextBox()
        Me.txt_partai_8 = New System.Windows.Forms.TextBox()
        Me.txt_aksesoris_8 = New System.Windows.Forms.TextBox()
        Me.txt_keterangan_8 = New System.Windows.Forms.TextBox()
        Me.txt_meter_8 = New System.Windows.Forms.TextBox()
        Me.txt_resep_8 = New System.Windows.Forms.TextBox()
        Me.txt_warna_8 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_8 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_8 = New System.Windows.Forms.Label()
        Me.txt_resep_7 = New System.Windows.Forms.TextBox()
        Me.txt_warna_7 = New System.Windows.Forms.TextBox()
        Me.txt_jenis_kain_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_sj_7 = New System.Windows.Forms.TextBox()
        Me.txt_no_urut_7 = New System.Windows.Forms.Label()
        Me.Panel_7 = New System.Windows.Forms.Panel()
        Me.cb_satuan_7 = New System.Windows.Forms.ComboBox()
        Me.txt_meter_7 = New System.Windows.Forms.TextBox()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txt_id_sj_proses_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_sj_proses_1 = New System.Windows.Forms.TextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txt_id_po_packing_10 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_9 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_8 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_7 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_6 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_5 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_4 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_3 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_2 = New System.Windows.Forms.TextBox()
        Me.txt_id_po_packing_1 = New System.Windows.Forms.TextBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txt_asal_satuan_10 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_9 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_8 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_7 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_6 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_5 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_4 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_3 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_2 = New System.Windows.Forms.TextBox()
        Me.txt_asal_satuan_1 = New System.Windows.Forms.TextBox()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.dtp_tanggal_sj_6 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_7 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_10 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_9 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_8 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_5 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_4 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_3 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_2 = New System.Windows.Forms.DateTimePicker()
        Me.dtp_tanggal_sj_1 = New System.Windows.Forms.DateTimePicker()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txt_status_sj_proses_10 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_9 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_8 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_7 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_6 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_5 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_4 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_3 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_2 = New System.Windows.Forms.TextBox()
        Me.txt_status_sj_proses_1 = New System.Windows.Forms.TextBox()
        Me.Panel2.SuspendLayout()
        Me.Panel_1.SuspendLayout()
        Me.Panel_5.SuspendLayout()
        Me.Panel_4.SuspendLayout()
        Me.Panel_3.SuspendLayout()
        Me.Panel_2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel_10.SuspendLayout()
        Me.Panel_9.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel_6.SuspendLayout()
        Me.Panel_8.SuspendLayout()
        Me.Panel_7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.SuspendLayout()
        '
        'txt_keterangan_3
        '
        Me.txt_keterangan_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_3.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_3.Name = "txt_keterangan_3"
        Me.txt_keterangan_3.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_3.TabIndex = 7
        '
        'txt_customer_3
        '
        Me.txt_customer_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_3.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_3.Name = "txt_customer_3"
        Me.txt_customer_3.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_3.TabIndex = 87
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label26)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Location = New System.Drawing.Point(-3, 137)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1342, 30)
        Me.Panel2.TabIndex = 75
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.Window
        Me.Label26.Location = New System.Drawing.Point(733, 7)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(47, 13)
        Me.Label26.TabIndex = 16
        Me.Label26.Text = "Satuan"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.Window
        Me.Label19.Location = New System.Drawing.Point(1180, 8)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(72, 13)
        Me.Label19.TabIndex = 13
        Me.Label19.Text = "Keterangan"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.Window
        Me.Label18.Location = New System.Drawing.Point(985, 7)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(61, 13)
        Me.Label18.TabIndex = 12
        Me.Label18.Text = "Aksesoris"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.Window
        Me.Label16.Location = New System.Drawing.Point(821, 8)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(59, 13)
        Me.Label16.TabIndex = 10
        Me.Label16.Text = "Customer"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.Window
        Me.Label13.Location = New System.Drawing.Point(671, 7)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(32, 13)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "QTY"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.Window
        Me.Label12.Location = New System.Drawing.Point(598, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(47, 13)
        Me.Label12.TabIndex = 7
        Me.Label12.Text = "Gulung"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.Window
        Me.Label11.Location = New System.Drawing.Point(535, 8)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(40, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Partai"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Window
        Me.Label10.Location = New System.Drawing.Point(446, 7)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(43, 13)
        Me.Label10.TabIndex = 5
        Me.Label10.Text = "Resep"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Window
        Me.Label9.Location = New System.Drawing.Point(334, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(44, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Warna"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Window
        Me.Label8.Location = New System.Drawing.Point(183, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(65, 13)
        Me.Label8.TabIndex = 3
        Me.Label8.Text = "Jenis Kain"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Window
        Me.Label6.Location = New System.Drawing.Point(50, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 13)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "SJ Proses"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Window
        Me.Label5.Location = New System.Drawing.Point(8, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(27, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "No."
        '
        'Panel_1
        '
        Me.Panel_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_1.Controls.Add(Me.cb_satuan_1)
        Me.Panel_1.Controls.Add(Me.txt_aksesoris_1)
        Me.Panel_1.Controls.Add(Me.txt_keterangan_1)
        Me.Panel_1.Controls.Add(Me.txt_customer_1)
        Me.Panel_1.Controls.Add(Me.txt_gulung_1)
        Me.Panel_1.Controls.Add(Me.txt_partai_1)
        Me.Panel_1.Controls.Add(Me.txt_meter_1)
        Me.Panel_1.Controls.Add(Me.txt_resep_1)
        Me.Panel_1.Controls.Add(Me.txt_warna_1)
        Me.Panel_1.Controls.Add(Me.txt_jenis_kain_1)
        Me.Panel_1.Controls.Add(Me.txt_asal_sj_1)
        Me.Panel_1.Controls.Add(Me.txt_no_urut_1)
        Me.Panel_1.Location = New System.Drawing.Point(-3, 167)
        Me.Panel_1.Name = "Panel_1"
        Me.Panel_1.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_1.TabIndex = 76
        '
        'cb_satuan_1
        '
        Me.cb_satuan_1.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_1.FormattingEnabled = True
        Me.cb_satuan_1.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_1.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_1.Name = "cb_satuan_1"
        Me.cb_satuan_1.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_1.TabIndex = 511
        Me.cb_satuan_1.Text = "Meter"
        '
        'txt_aksesoris_1
        '
        Me.txt_aksesoris_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_1.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_1.Name = "txt_aksesoris_1"
        Me.txt_aksesoris_1.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_1.TabIndex = 8
        '
        'txt_keterangan_1
        '
        Me.txt_keterangan_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_1.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_1.Name = "txt_keterangan_1"
        Me.txt_keterangan_1.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_1.TabIndex = 7
        '
        'txt_customer_1
        '
        Me.txt_customer_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_1.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_1.Name = "txt_customer_1"
        Me.txt_customer_1.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_1.TabIndex = 89
        '
        'txt_gulung_1
        '
        Me.txt_gulung_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_1.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_1.Name = "txt_gulung_1"
        Me.txt_gulung_1.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_1.TabIndex = 89
        Me.txt_gulung_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_1
        '
        Me.txt_partai_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_1.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_1.Name = "txt_partai_1"
        Me.txt_partai_1.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_1.TabIndex = 89
        '
        'txt_meter_1
        '
        Me.txt_meter_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_1.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_1.Name = "txt_meter_1"
        Me.txt_meter_1.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_1.TabIndex = 6
        Me.txt_meter_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_1
        '
        Me.txt_resep_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_1.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_1.Name = "txt_resep_1"
        Me.txt_resep_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_1.TabIndex = 5
        '
        'txt_warna_1
        '
        Me.txt_warna_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_1.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_1.Name = "txt_warna_1"
        Me.txt_warna_1.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_1.TabIndex = 4
        '
        'txt_jenis_kain_1
        '
        Me.txt_jenis_kain_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_1.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_1.Name = "txt_jenis_kain_1"
        Me.txt_jenis_kain_1.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_1.TabIndex = 2
        '
        'txt_asal_sj_1
        '
        Me.txt_asal_sj_1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_1.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_1.Name = "txt_asal_sj_1"
        Me.txt_asal_sj_1.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_1.TabIndex = 1
        '
        'txt_no_urut_1
        '
        Me.txt_no_urut_1.AutoSize = True
        Me.txt_no_urut_1.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_1.Name = "txt_no_urut_1"
        Me.txt_no_urut_1.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_1.TabIndex = 0
        Me.txt_no_urut_1.Text = "1"
        '
        'txt_aksesoris_3
        '
        Me.txt_aksesoris_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_3.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_3.Name = "txt_aksesoris_3"
        Me.txt_aksesoris_3.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_3.TabIndex = 8
        '
        'txt_warna_6
        '
        Me.txt_warna_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_6.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_6.Name = "txt_warna_6"
        Me.txt_warna_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_6.TabIndex = 4
        '
        'txt_jenis_kain_6
        '
        Me.txt_jenis_kain_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_6.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_6.Name = "txt_jenis_kain_6"
        Me.txt_jenis_kain_6.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_6.TabIndex = 2
        '
        'txt_asal_sj_6
        '
        Me.txt_asal_sj_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_6.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_6.Name = "txt_asal_sj_6"
        Me.txt_asal_sj_6.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_6.TabIndex = 1
        '
        'txt_no_urut_6
        '
        Me.txt_no_urut_6.AutoSize = True
        Me.txt_no_urut_6.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_6.Name = "txt_no_urut_6"
        Me.txt_no_urut_6.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_6.TabIndex = 0
        Me.txt_no_urut_6.Text = "6"
        '
        'Panel_5
        '
        Me.Panel_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_5.Controls.Add(Me.cb_satuan_5)
        Me.Panel_5.Controls.Add(Me.txt_gulung_5)
        Me.Panel_5.Controls.Add(Me.txt_aksesoris_5)
        Me.Panel_5.Controls.Add(Me.txt_customer_5)
        Me.Panel_5.Controls.Add(Me.txt_keterangan_5)
        Me.Panel_5.Controls.Add(Me.txt_partai_5)
        Me.Panel_5.Controls.Add(Me.txt_meter_5)
        Me.Panel_5.Controls.Add(Me.txt_resep_5)
        Me.Panel_5.Controls.Add(Me.txt_warna_5)
        Me.Panel_5.Controls.Add(Me.txt_jenis_kain_5)
        Me.Panel_5.Controls.Add(Me.txt_asal_sj_5)
        Me.Panel_5.Controls.Add(Me.txt_no_urut_5)
        Me.Panel_5.Location = New System.Drawing.Point(-3, 307)
        Me.Panel_5.Name = "Panel_5"
        Me.Panel_5.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_5.TabIndex = 80
        '
        'cb_satuan_5
        '
        Me.cb_satuan_5.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_5.FormattingEnabled = True
        Me.cb_satuan_5.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_5.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_5.Name = "cb_satuan_5"
        Me.cb_satuan_5.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_5.TabIndex = 503
        Me.cb_satuan_5.Text = "Meter"
        '
        'txt_gulung_5
        '
        Me.txt_gulung_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_5.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_5.Name = "txt_gulung_5"
        Me.txt_gulung_5.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_5.TabIndex = 92
        Me.txt_gulung_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_aksesoris_5
        '
        Me.txt_aksesoris_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_5.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_5.Name = "txt_aksesoris_5"
        Me.txt_aksesoris_5.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_5.TabIndex = 8
        '
        'txt_customer_5
        '
        Me.txt_customer_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_5.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_5.Name = "txt_customer_5"
        Me.txt_customer_5.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_5.TabIndex = 86
        '
        'txt_keterangan_5
        '
        Me.txt_keterangan_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_5.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_5.Name = "txt_keterangan_5"
        Me.txt_keterangan_5.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_5.TabIndex = 7
        '
        'txt_partai_5
        '
        Me.txt_partai_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_5.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_5.Name = "txt_partai_5"
        Me.txt_partai_5.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_5.TabIndex = 86
        '
        'txt_meter_5
        '
        Me.txt_meter_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_5.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_5.Name = "txt_meter_5"
        Me.txt_meter_5.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_5.TabIndex = 6
        Me.txt_meter_5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_5
        '
        Me.txt_resep_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_5.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_5.Name = "txt_resep_5"
        Me.txt_resep_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_5.TabIndex = 5
        '
        'txt_warna_5
        '
        Me.txt_warna_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_5.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_5.Name = "txt_warna_5"
        Me.txt_warna_5.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_5.TabIndex = 4
        '
        'txt_jenis_kain_5
        '
        Me.txt_jenis_kain_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_5.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_5.Name = "txt_jenis_kain_5"
        Me.txt_jenis_kain_5.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_5.TabIndex = 2
        '
        'txt_asal_sj_5
        '
        Me.txt_asal_sj_5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_5.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_5.Name = "txt_asal_sj_5"
        Me.txt_asal_sj_5.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_5.TabIndex = 1
        '
        'txt_no_urut_5
        '
        Me.txt_no_urut_5.AutoSize = True
        Me.txt_no_urut_5.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_5.Name = "txt_no_urut_5"
        Me.txt_no_urut_5.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_5.TabIndex = 0
        Me.txt_no_urut_5.Text = "5"
        '
        'Panel_4
        '
        Me.Panel_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_4.Controls.Add(Me.cb_satuan_4)
        Me.Panel_4.Controls.Add(Me.txt_customer_4)
        Me.Panel_4.Controls.Add(Me.txt_gulung_4)
        Me.Panel_4.Controls.Add(Me.txt_partai_4)
        Me.Panel_4.Controls.Add(Me.txt_aksesoris_4)
        Me.Panel_4.Controls.Add(Me.txt_keterangan_4)
        Me.Panel_4.Controls.Add(Me.txt_meter_4)
        Me.Panel_4.Controls.Add(Me.txt_resep_4)
        Me.Panel_4.Controls.Add(Me.txt_warna_4)
        Me.Panel_4.Controls.Add(Me.txt_jenis_kain_4)
        Me.Panel_4.Controls.Add(Me.txt_asal_sj_4)
        Me.Panel_4.Controls.Add(Me.txt_no_urut_4)
        Me.Panel_4.Location = New System.Drawing.Point(-3, 272)
        Me.Panel_4.Name = "Panel_4"
        Me.Panel_4.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_4.TabIndex = 79
        '
        'cb_satuan_4
        '
        Me.cb_satuan_4.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_4.FormattingEnabled = True
        Me.cb_satuan_4.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_4.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_4.Name = "cb_satuan_4"
        Me.cb_satuan_4.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_4.TabIndex = 506
        Me.cb_satuan_4.Text = "Meter"
        '
        'txt_customer_4
        '
        Me.txt_customer_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_4.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_4.Name = "txt_customer_4"
        Me.txt_customer_4.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_4.TabIndex = 93
        '
        'txt_gulung_4
        '
        Me.txt_gulung_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_4.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_4.Name = "txt_gulung_4"
        Me.txt_gulung_4.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_4.TabIndex = 93
        Me.txt_gulung_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_4
        '
        Me.txt_partai_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_4.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_4.Name = "txt_partai_4"
        Me.txt_partai_4.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_4.TabIndex = 93
        '
        'txt_aksesoris_4
        '
        Me.txt_aksesoris_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_4.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_4.Name = "txt_aksesoris_4"
        Me.txt_aksesoris_4.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_4.TabIndex = 8
        '
        'txt_keterangan_4
        '
        Me.txt_keterangan_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_4.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_4.Name = "txt_keterangan_4"
        Me.txt_keterangan_4.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_4.TabIndex = 7
        '
        'txt_meter_4
        '
        Me.txt_meter_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_4.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_4.Name = "txt_meter_4"
        Me.txt_meter_4.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_4.TabIndex = 6
        Me.txt_meter_4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_4
        '
        Me.txt_resep_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_4.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_4.Name = "txt_resep_4"
        Me.txt_resep_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_4.TabIndex = 5
        '
        'txt_warna_4
        '
        Me.txt_warna_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_4.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_4.Name = "txt_warna_4"
        Me.txt_warna_4.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_4.TabIndex = 4
        '
        'txt_jenis_kain_4
        '
        Me.txt_jenis_kain_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_4.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_4.Name = "txt_jenis_kain_4"
        Me.txt_jenis_kain_4.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_4.TabIndex = 2
        '
        'txt_asal_sj_4
        '
        Me.txt_asal_sj_4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_4.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_4.Name = "txt_asal_sj_4"
        Me.txt_asal_sj_4.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_4.TabIndex = 1
        '
        'txt_no_urut_4
        '
        Me.txt_no_urut_4.AutoSize = True
        Me.txt_no_urut_4.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_4.Name = "txt_no_urut_4"
        Me.txt_no_urut_4.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_4.TabIndex = 0
        Me.txt_no_urut_4.Text = "4"
        '
        'txt_gulung_3
        '
        Me.txt_gulung_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_3.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_3.Name = "txt_gulung_3"
        Me.txt_gulung_3.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_3.TabIndex = 87
        Me.txt_gulung_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_3
        '
        Me.txt_partai_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_3.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_3.Name = "txt_partai_3"
        Me.txt_partai_3.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_3.TabIndex = 87
        '
        'txt_meter_3
        '
        Me.txt_meter_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_3.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_3.Name = "txt_meter_3"
        Me.txt_meter_3.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_3.TabIndex = 6
        Me.txt_meter_3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_3
        '
        Me.txt_resep_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_3.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_3.Name = "txt_resep_3"
        Me.txt_resep_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_3.TabIndex = 5
        '
        'txt_warna_3
        '
        Me.txt_warna_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_3.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_3.Name = "txt_warna_3"
        Me.txt_warna_3.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_3.TabIndex = 4
        '
        'txt_jenis_kain_3
        '
        Me.txt_jenis_kain_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_3.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_3.Name = "txt_jenis_kain_3"
        Me.txt_jenis_kain_3.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_3.TabIndex = 2
        '
        'txt_asal_sj_3
        '
        Me.txt_asal_sj_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_3.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_3.Name = "txt_asal_sj_3"
        Me.txt_asal_sj_3.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_3.TabIndex = 1
        '
        'txt_no_urut_3
        '
        Me.txt_no_urut_3.AutoSize = True
        Me.txt_no_urut_3.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_3.Name = "txt_no_urut_3"
        Me.txt_no_urut_3.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_3.TabIndex = 0
        Me.txt_no_urut_3.Text = "3"
        '
        'Panel_3
        '
        Me.Panel_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_3.Controls.Add(Me.cb_satuan_3)
        Me.Panel_3.Controls.Add(Me.txt_aksesoris_3)
        Me.Panel_3.Controls.Add(Me.txt_keterangan_3)
        Me.Panel_3.Controls.Add(Me.txt_customer_3)
        Me.Panel_3.Controls.Add(Me.txt_gulung_3)
        Me.Panel_3.Controls.Add(Me.txt_partai_3)
        Me.Panel_3.Controls.Add(Me.txt_meter_3)
        Me.Panel_3.Controls.Add(Me.txt_resep_3)
        Me.Panel_3.Controls.Add(Me.txt_warna_3)
        Me.Panel_3.Controls.Add(Me.txt_jenis_kain_3)
        Me.Panel_3.Controls.Add(Me.txt_asal_sj_3)
        Me.Panel_3.Controls.Add(Me.txt_no_urut_3)
        Me.Panel_3.Location = New System.Drawing.Point(-3, 237)
        Me.Panel_3.Name = "Panel_3"
        Me.Panel_3.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_3.TabIndex = 78
        '
        'cb_satuan_3
        '
        Me.cb_satuan_3.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_3.FormattingEnabled = True
        Me.cb_satuan_3.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_3.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_3.Name = "cb_satuan_3"
        Me.cb_satuan_3.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_3.TabIndex = 505
        Me.cb_satuan_3.Text = "Meter"
        '
        'Panel_2
        '
        Me.Panel_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_2.Controls.Add(Me.cb_satuan_2)
        Me.Panel_2.Controls.Add(Me.txt_aksesoris_2)
        Me.Panel_2.Controls.Add(Me.txt_keterangan_2)
        Me.Panel_2.Controls.Add(Me.txt_customer_2)
        Me.Panel_2.Controls.Add(Me.txt_gulung_2)
        Me.Panel_2.Controls.Add(Me.txt_partai_2)
        Me.Panel_2.Controls.Add(Me.txt_meter_2)
        Me.Panel_2.Controls.Add(Me.txt_resep_2)
        Me.Panel_2.Controls.Add(Me.txt_warna_2)
        Me.Panel_2.Controls.Add(Me.txt_jenis_kain_2)
        Me.Panel_2.Controls.Add(Me.txt_asal_sj_2)
        Me.Panel_2.Controls.Add(Me.txt_no_urut_2)
        Me.Panel_2.Location = New System.Drawing.Point(-3, 202)
        Me.Panel_2.Name = "Panel_2"
        Me.Panel_2.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_2.TabIndex = 77
        '
        'cb_satuan_2
        '
        Me.cb_satuan_2.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_2.FormattingEnabled = True
        Me.cb_satuan_2.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_2.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_2.Name = "cb_satuan_2"
        Me.cb_satuan_2.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_2.TabIndex = 504
        Me.cb_satuan_2.Text = "Meter"
        '
        'txt_aksesoris_2
        '
        Me.txt_aksesoris_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_2.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_2.Name = "txt_aksesoris_2"
        Me.txt_aksesoris_2.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_2.TabIndex = 8
        '
        'txt_keterangan_2
        '
        Me.txt_keterangan_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_2.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_2.Name = "txt_keterangan_2"
        Me.txt_keterangan_2.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_2.TabIndex = 7
        '
        'txt_customer_2
        '
        Me.txt_customer_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_2.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_2.Name = "txt_customer_2"
        Me.txt_customer_2.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_2.TabIndex = 88
        '
        'txt_gulung_2
        '
        Me.txt_gulung_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_2.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_2.Name = "txt_gulung_2"
        Me.txt_gulung_2.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_2.TabIndex = 88
        Me.txt_gulung_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_2
        '
        Me.txt_partai_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_2.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_2.Name = "txt_partai_2"
        Me.txt_partai_2.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_2.TabIndex = 88
        '
        'txt_meter_2
        '
        Me.txt_meter_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_2.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_2.Name = "txt_meter_2"
        Me.txt_meter_2.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_2.TabIndex = 6
        Me.txt_meter_2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_2
        '
        Me.txt_resep_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_2.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_2.Name = "txt_resep_2"
        Me.txt_resep_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_2.TabIndex = 5
        '
        'txt_warna_2
        '
        Me.txt_warna_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_2.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_2.Name = "txt_warna_2"
        Me.txt_warna_2.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_2.TabIndex = 4
        '
        'txt_jenis_kain_2
        '
        Me.txt_jenis_kain_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_2.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_2.Name = "txt_jenis_kain_2"
        Me.txt_jenis_kain_2.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_2.TabIndex = 2
        '
        'txt_asal_sj_2
        '
        Me.txt_asal_sj_2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_2.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_2.Name = "txt_asal_sj_2"
        Me.txt_asal_sj_2.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_2.TabIndex = 1
        '
        'txt_no_urut_2
        '
        Me.txt_no_urut_2.AutoSize = True
        Me.txt_no_urut_2.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_2.Name = "txt_no_urut_2"
        Me.txt_no_urut_2.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_2.TabIndex = 0
        Me.txt_no_urut_2.Text = "2"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btn_batal_10)
        Me.Panel3.Controls.Add(Me.btn_batal_9)
        Me.Panel3.Controls.Add(Me.btn_batal_8)
        Me.Panel3.Controls.Add(Me.btn_batal_7)
        Me.Panel3.Controls.Add(Me.btn_batal_6)
        Me.Panel3.Controls.Add(Me.btn_batal_5)
        Me.Panel3.Controls.Add(Me.btn_batal_4)
        Me.Panel3.Controls.Add(Me.btn_batal_3)
        Me.Panel3.Controls.Add(Me.btn_batal_2)
        Me.Panel3.Controls.Add(Me.btn_batal_1)
        Me.Panel3.Controls.Add(Me.btn_hapus_10)
        Me.Panel3.Controls.Add(Me.btn_selesai_10)
        Me.Panel3.Controls.Add(Me.btn_hapus_9)
        Me.Panel3.Controls.Add(Me.btn_selesai_9)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_9)
        Me.Panel3.Controls.Add(Me.btn_hapus_8)
        Me.Panel3.Controls.Add(Me.btn_selesai_8)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_8)
        Me.Panel3.Controls.Add(Me.btn_hapus_7)
        Me.Panel3.Controls.Add(Me.btn_selesai_7)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_7)
        Me.Panel3.Controls.Add(Me.btn_hapus_6)
        Me.Panel3.Controls.Add(Me.btn_selesai_6)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_6)
        Me.Panel3.Controls.Add(Me.btn_hapus_5)
        Me.Panel3.Controls.Add(Me.btn_selesai_5)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_5)
        Me.Panel3.Controls.Add(Me.btn_hapus_4)
        Me.Panel3.Controls.Add(Me.btn_selesai_4)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_4)
        Me.Panel3.Controls.Add(Me.btn_hapus_3)
        Me.Panel3.Controls.Add(Me.btn_selesai_3)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_3)
        Me.Panel3.Controls.Add(Me.btn_hapus_2)
        Me.Panel3.Controls.Add(Me.btn_selesai_2)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_2)
        Me.Panel3.Controls.Add(Me.btn_selesai_1)
        Me.Panel3.Controls.Add(Me.btn_tambah_baris_1)
        Me.Panel3.Location = New System.Drawing.Point(478, 224)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(469, 389)
        Me.Panel3.TabIndex = 86
        '
        'btn_batal_10
        '
        Me.btn_batal_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_10.Image = CType(resources.GetObject("btn_batal_10.Image"), System.Drawing.Image)
        Me.btn_batal_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_10.Location = New System.Drawing.Point(310, 329)
        Me.btn_batal_10.Name = "btn_batal_10"
        Me.btn_batal_10.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_10.TabIndex = 68
        Me.btn_batal_10.Text = "Batal"
        Me.btn_batal_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_10.UseVisualStyleBackColor = False
        '
        'btn_batal_9
        '
        Me.btn_batal_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_9.Image = CType(resources.GetObject("btn_batal_9.Image"), System.Drawing.Image)
        Me.btn_batal_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_9.Location = New System.Drawing.Point(381, 293)
        Me.btn_batal_9.Name = "btn_batal_9"
        Me.btn_batal_9.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_9.TabIndex = 67
        Me.btn_batal_9.Text = "Batal"
        Me.btn_batal_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_9.UseVisualStyleBackColor = False
        '
        'btn_batal_8
        '
        Me.btn_batal_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_8.Image = CType(resources.GetObject("btn_batal_8.Image"), System.Drawing.Image)
        Me.btn_batal_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_8.Location = New System.Drawing.Point(381, 257)
        Me.btn_batal_8.Name = "btn_batal_8"
        Me.btn_batal_8.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_8.TabIndex = 66
        Me.btn_batal_8.Text = "Batal"
        Me.btn_batal_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_8.UseVisualStyleBackColor = False
        '
        'btn_batal_7
        '
        Me.btn_batal_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_7.Image = CType(resources.GetObject("btn_batal_7.Image"), System.Drawing.Image)
        Me.btn_batal_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_7.Location = New System.Drawing.Point(381, 223)
        Me.btn_batal_7.Name = "btn_batal_7"
        Me.btn_batal_7.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_7.TabIndex = 65
        Me.btn_batal_7.Text = "Batal"
        Me.btn_batal_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_7.UseVisualStyleBackColor = False
        '
        'btn_batal_6
        '
        Me.btn_batal_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_6.Image = CType(resources.GetObject("btn_batal_6.Image"), System.Drawing.Image)
        Me.btn_batal_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_6.Location = New System.Drawing.Point(381, 188)
        Me.btn_batal_6.Name = "btn_batal_6"
        Me.btn_batal_6.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_6.TabIndex = 64
        Me.btn_batal_6.Text = "Batal"
        Me.btn_batal_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_6.UseVisualStyleBackColor = False
        '
        'btn_batal_5
        '
        Me.btn_batal_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_5.Image = CType(resources.GetObject("btn_batal_5.Image"), System.Drawing.Image)
        Me.btn_batal_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_5.Location = New System.Drawing.Point(381, 154)
        Me.btn_batal_5.Name = "btn_batal_5"
        Me.btn_batal_5.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_5.TabIndex = 63
        Me.btn_batal_5.Text = "Batal"
        Me.btn_batal_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_5.UseVisualStyleBackColor = False
        '
        'btn_batal_4
        '
        Me.btn_batal_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_4.Image = CType(resources.GetObject("btn_batal_4.Image"), System.Drawing.Image)
        Me.btn_batal_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_4.Location = New System.Drawing.Point(381, 119)
        Me.btn_batal_4.Name = "btn_batal_4"
        Me.btn_batal_4.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_4.TabIndex = 62
        Me.btn_batal_4.Text = "Batal"
        Me.btn_batal_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_4.UseVisualStyleBackColor = False
        '
        'btn_batal_3
        '
        Me.btn_batal_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_3.Image = CType(resources.GetObject("btn_batal_3.Image"), System.Drawing.Image)
        Me.btn_batal_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_3.Location = New System.Drawing.Point(381, 83)
        Me.btn_batal_3.Name = "btn_batal_3"
        Me.btn_batal_3.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_3.TabIndex = 61
        Me.btn_batal_3.Text = "Batal"
        Me.btn_batal_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_3.UseVisualStyleBackColor = False
        '
        'btn_batal_2
        '
        Me.btn_batal_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_2.Image = CType(resources.GetObject("btn_batal_2.Image"), System.Drawing.Image)
        Me.btn_batal_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_2.Location = New System.Drawing.Point(381, 47)
        Me.btn_batal_2.Name = "btn_batal_2"
        Me.btn_batal_2.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_2.TabIndex = 60
        Me.btn_batal_2.Text = "Batal"
        Me.btn_batal_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_2.UseVisualStyleBackColor = False
        '
        'btn_batal_1
        '
        Me.btn_batal_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_batal_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal_1.Image = CType(resources.GetObject("btn_batal_1.Image"), System.Drawing.Image)
        Me.btn_batal_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal_1.Location = New System.Drawing.Point(327, 11)
        Me.btn_batal_1.Name = "btn_batal_1"
        Me.btn_batal_1.Size = New System.Drawing.Size(65, 23)
        Me.btn_batal_1.TabIndex = 59
        Me.btn_batal_1.Text = "Batal"
        Me.btn_batal_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal_1.UseVisualStyleBackColor = False
        '
        'btn_hapus_10
        '
        Me.btn_hapus_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_10.Image = CType(resources.GetObject("btn_hapus_10.Image"), System.Drawing.Image)
        Me.btn_hapus_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_10.Location = New System.Drawing.Point(202, 329)
        Me.btn_hapus_10.Name = "btn_hapus_10"
        Me.btn_hapus_10.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_10.TabIndex = 37
        Me.btn_hapus_10.Text = "Hapus"
        Me.btn_hapus_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_10.UseVisualStyleBackColor = False
        '
        'btn_selesai_10
        '
        Me.btn_selesai_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_10.Image = CType(resources.GetObject("btn_selesai_10.Image"), System.Drawing.Image)
        Me.btn_selesai_10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_10.Location = New System.Drawing.Point(94, 329)
        Me.btn_selesai_10.Name = "btn_selesai_10"
        Me.btn_selesai_10.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_10.TabIndex = 36
        Me.btn_selesai_10.Text = "Selesai"
        Me.btn_selesai_10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_10.UseVisualStyleBackColor = False
        '
        'btn_hapus_9
        '
        Me.btn_hapus_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_9.Image = CType(resources.GetObject("btn_hapus_9.Image"), System.Drawing.Image)
        Me.btn_hapus_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_9.Location = New System.Drawing.Point(273, 293)
        Me.btn_hapus_9.Name = "btn_hapus_9"
        Me.btn_hapus_9.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_9.TabIndex = 34
        Me.btn_hapus_9.Text = "Hapus"
        Me.btn_hapus_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_9.UseVisualStyleBackColor = False
        '
        'btn_selesai_9
        '
        Me.btn_selesai_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_9.Image = CType(resources.GetObject("btn_selesai_9.Image"), System.Drawing.Image)
        Me.btn_selesai_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_9.Location = New System.Drawing.Point(165, 293)
        Me.btn_selesai_9.Name = "btn_selesai_9"
        Me.btn_selesai_9.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_9.TabIndex = 33
        Me.btn_selesai_9.Text = "Selesai"
        Me.btn_selesai_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_9.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_9
        '
        Me.btn_tambah_baris_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_9.Image = CType(resources.GetObject("btn_tambah_baris_9.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_9.Location = New System.Drawing.Point(22, 293)
        Me.btn_tambah_baris_9.Name = "btn_tambah_baris_9"
        Me.btn_tambah_baris_9.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_9.TabIndex = 32
        Me.btn_tambah_baris_9.Text = "Tambah Baris"
        Me.btn_tambah_baris_9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_9.UseVisualStyleBackColor = False
        '
        'btn_hapus_8
        '
        Me.btn_hapus_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_8.Image = CType(resources.GetObject("btn_hapus_8.Image"), System.Drawing.Image)
        Me.btn_hapus_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_8.Location = New System.Drawing.Point(273, 257)
        Me.btn_hapus_8.Name = "btn_hapus_8"
        Me.btn_hapus_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_8.TabIndex = 31
        Me.btn_hapus_8.Text = "Hapus"
        Me.btn_hapus_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_8.UseVisualStyleBackColor = False
        '
        'btn_selesai_8
        '
        Me.btn_selesai_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_8.Image = CType(resources.GetObject("btn_selesai_8.Image"), System.Drawing.Image)
        Me.btn_selesai_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_8.Location = New System.Drawing.Point(165, 257)
        Me.btn_selesai_8.Name = "btn_selesai_8"
        Me.btn_selesai_8.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_8.TabIndex = 30
        Me.btn_selesai_8.Text = "Selesai"
        Me.btn_selesai_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_8.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_8
        '
        Me.btn_tambah_baris_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_8.Image = CType(resources.GetObject("btn_tambah_baris_8.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_8.Location = New System.Drawing.Point(22, 257)
        Me.btn_tambah_baris_8.Name = "btn_tambah_baris_8"
        Me.btn_tambah_baris_8.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_8.TabIndex = 29
        Me.btn_tambah_baris_8.Text = "Tambah Baris"
        Me.btn_tambah_baris_8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_8.UseVisualStyleBackColor = False
        '
        'btn_hapus_7
        '
        Me.btn_hapus_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_7.Image = CType(resources.GetObject("btn_hapus_7.Image"), System.Drawing.Image)
        Me.btn_hapus_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_7.Location = New System.Drawing.Point(273, 223)
        Me.btn_hapus_7.Name = "btn_hapus_7"
        Me.btn_hapus_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_7.TabIndex = 28
        Me.btn_hapus_7.Text = "Hapus"
        Me.btn_hapus_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_7.UseVisualStyleBackColor = False
        '
        'btn_selesai_7
        '
        Me.btn_selesai_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_7.Image = CType(resources.GetObject("btn_selesai_7.Image"), System.Drawing.Image)
        Me.btn_selesai_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_7.Location = New System.Drawing.Point(165, 223)
        Me.btn_selesai_7.Name = "btn_selesai_7"
        Me.btn_selesai_7.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_7.TabIndex = 27
        Me.btn_selesai_7.Text = "Selesai"
        Me.btn_selesai_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_7.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_7
        '
        Me.btn_tambah_baris_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_7.Image = CType(resources.GetObject("btn_tambah_baris_7.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_7.Location = New System.Drawing.Point(22, 223)
        Me.btn_tambah_baris_7.Name = "btn_tambah_baris_7"
        Me.btn_tambah_baris_7.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_7.TabIndex = 26
        Me.btn_tambah_baris_7.Text = "Tambah Baris"
        Me.btn_tambah_baris_7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_7.UseVisualStyleBackColor = False
        '
        'btn_hapus_6
        '
        Me.btn_hapus_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_6.Image = CType(resources.GetObject("btn_hapus_6.Image"), System.Drawing.Image)
        Me.btn_hapus_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_6.Location = New System.Drawing.Point(273, 188)
        Me.btn_hapus_6.Name = "btn_hapus_6"
        Me.btn_hapus_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_6.TabIndex = 25
        Me.btn_hapus_6.Text = "Hapus"
        Me.btn_hapus_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_6.UseVisualStyleBackColor = False
        '
        'btn_selesai_6
        '
        Me.btn_selesai_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_6.Image = CType(resources.GetObject("btn_selesai_6.Image"), System.Drawing.Image)
        Me.btn_selesai_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_6.Location = New System.Drawing.Point(165, 188)
        Me.btn_selesai_6.Name = "btn_selesai_6"
        Me.btn_selesai_6.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_6.TabIndex = 24
        Me.btn_selesai_6.Text = "Selesai"
        Me.btn_selesai_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_6.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_6
        '
        Me.btn_tambah_baris_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_6.Image = CType(resources.GetObject("btn_tambah_baris_6.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_6.Location = New System.Drawing.Point(22, 188)
        Me.btn_tambah_baris_6.Name = "btn_tambah_baris_6"
        Me.btn_tambah_baris_6.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_6.TabIndex = 23
        Me.btn_tambah_baris_6.Text = "Tambah Baris"
        Me.btn_tambah_baris_6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_6.UseVisualStyleBackColor = False
        '
        'btn_hapus_5
        '
        Me.btn_hapus_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_5.Image = CType(resources.GetObject("btn_hapus_5.Image"), System.Drawing.Image)
        Me.btn_hapus_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_5.Location = New System.Drawing.Point(273, 154)
        Me.btn_hapus_5.Name = "btn_hapus_5"
        Me.btn_hapus_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_5.TabIndex = 22
        Me.btn_hapus_5.Text = "Hapus"
        Me.btn_hapus_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_5.UseVisualStyleBackColor = False
        '
        'btn_selesai_5
        '
        Me.btn_selesai_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_5.Image = CType(resources.GetObject("btn_selesai_5.Image"), System.Drawing.Image)
        Me.btn_selesai_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_5.Location = New System.Drawing.Point(165, 154)
        Me.btn_selesai_5.Name = "btn_selesai_5"
        Me.btn_selesai_5.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_5.TabIndex = 21
        Me.btn_selesai_5.Text = "Selesai"
        Me.btn_selesai_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_5.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_5
        '
        Me.btn_tambah_baris_5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_5.Image = CType(resources.GetObject("btn_tambah_baris_5.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_5.Location = New System.Drawing.Point(22, 154)
        Me.btn_tambah_baris_5.Name = "btn_tambah_baris_5"
        Me.btn_tambah_baris_5.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_5.TabIndex = 20
        Me.btn_tambah_baris_5.Text = "Tambah Baris"
        Me.btn_tambah_baris_5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_5.UseVisualStyleBackColor = False
        '
        'btn_hapus_4
        '
        Me.btn_hapus_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_4.Image = CType(resources.GetObject("btn_hapus_4.Image"), System.Drawing.Image)
        Me.btn_hapus_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_4.Location = New System.Drawing.Point(273, 119)
        Me.btn_hapus_4.Name = "btn_hapus_4"
        Me.btn_hapus_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_4.TabIndex = 19
        Me.btn_hapus_4.Text = "Hapus"
        Me.btn_hapus_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_4.UseVisualStyleBackColor = False
        '
        'btn_selesai_4
        '
        Me.btn_selesai_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_4.Image = CType(resources.GetObject("btn_selesai_4.Image"), System.Drawing.Image)
        Me.btn_selesai_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_4.Location = New System.Drawing.Point(165, 119)
        Me.btn_selesai_4.Name = "btn_selesai_4"
        Me.btn_selesai_4.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_4.TabIndex = 18
        Me.btn_selesai_4.Text = "Selesai"
        Me.btn_selesai_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_4.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_4
        '
        Me.btn_tambah_baris_4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_4.Image = CType(resources.GetObject("btn_tambah_baris_4.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_4.Location = New System.Drawing.Point(22, 119)
        Me.btn_tambah_baris_4.Name = "btn_tambah_baris_4"
        Me.btn_tambah_baris_4.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_4.TabIndex = 17
        Me.btn_tambah_baris_4.Text = "Tambah Baris"
        Me.btn_tambah_baris_4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_4.UseVisualStyleBackColor = False
        '
        'btn_hapus_3
        '
        Me.btn_hapus_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_3.Image = CType(resources.GetObject("btn_hapus_3.Image"), System.Drawing.Image)
        Me.btn_hapus_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_3.Location = New System.Drawing.Point(273, 83)
        Me.btn_hapus_3.Name = "btn_hapus_3"
        Me.btn_hapus_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_3.TabIndex = 16
        Me.btn_hapus_3.Text = "Hapus"
        Me.btn_hapus_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_3.UseVisualStyleBackColor = False
        '
        'btn_selesai_3
        '
        Me.btn_selesai_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_3.Image = CType(resources.GetObject("btn_selesai_3.Image"), System.Drawing.Image)
        Me.btn_selesai_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_3.Location = New System.Drawing.Point(165, 83)
        Me.btn_selesai_3.Name = "btn_selesai_3"
        Me.btn_selesai_3.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_3.TabIndex = 15
        Me.btn_selesai_3.Text = "Selesai"
        Me.btn_selesai_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_3.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_3
        '
        Me.btn_tambah_baris_3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_3.Image = CType(resources.GetObject("btn_tambah_baris_3.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_3.Location = New System.Drawing.Point(22, 83)
        Me.btn_tambah_baris_3.Name = "btn_tambah_baris_3"
        Me.btn_tambah_baris_3.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_3.TabIndex = 14
        Me.btn_tambah_baris_3.Text = "Tambah Baris"
        Me.btn_tambah_baris_3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_3.UseVisualStyleBackColor = False
        '
        'btn_hapus_2
        '
        Me.btn_hapus_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_hapus_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_hapus_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_hapus_2.Image = CType(resources.GetObject("btn_hapus_2.Image"), System.Drawing.Image)
        Me.btn_hapus_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_hapus_2.Location = New System.Drawing.Point(273, 47)
        Me.btn_hapus_2.Name = "btn_hapus_2"
        Me.btn_hapus_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_hapus_2.TabIndex = 13
        Me.btn_hapus_2.Text = "Hapus"
        Me.btn_hapus_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_hapus_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_2
        '
        Me.btn_selesai_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_2.Image = CType(resources.GetObject("btn_selesai_2.Image"), System.Drawing.Image)
        Me.btn_selesai_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_2.Location = New System.Drawing.Point(165, 47)
        Me.btn_selesai_2.Name = "btn_selesai_2"
        Me.btn_selesai_2.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_2.TabIndex = 12
        Me.btn_selesai_2.Text = "Selesai"
        Me.btn_selesai_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_2.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_2
        '
        Me.btn_tambah_baris_2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_2.Image = CType(resources.GetObject("btn_tambah_baris_2.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_2.Location = New System.Drawing.Point(22, 47)
        Me.btn_tambah_baris_2.Name = "btn_tambah_baris_2"
        Me.btn_tambah_baris_2.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_2.TabIndex = 11
        Me.btn_tambah_baris_2.Text = "Tambah Baris"
        Me.btn_tambah_baris_2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_2.UseVisualStyleBackColor = False
        '
        'btn_selesai_1
        '
        Me.btn_selesai_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_selesai_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_selesai_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_selesai_1.Image = CType(resources.GetObject("btn_selesai_1.Image"), System.Drawing.Image)
        Me.btn_selesai_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_selesai_1.Location = New System.Drawing.Point(219, 11)
        Me.btn_selesai_1.Name = "btn_selesai_1"
        Me.btn_selesai_1.Size = New System.Drawing.Size(75, 23)
        Me.btn_selesai_1.TabIndex = 4
        Me.btn_selesai_1.Text = "Selesai"
        Me.btn_selesai_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_selesai_1.UseVisualStyleBackColor = False
        '
        'btn_tambah_baris_1
        '
        Me.btn_tambah_baris_1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tambah_baris_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_tambah_baris_1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_tambah_baris_1.Image = CType(resources.GetObject("btn_tambah_baris_1.Image"), System.Drawing.Image)
        Me.btn_tambah_baris_1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_tambah_baris_1.Location = New System.Drawing.Point(76, 11)
        Me.btn_tambah_baris_1.Name = "btn_tambah_baris_1"
        Me.btn_tambah_baris_1.Size = New System.Drawing.Size(110, 23)
        Me.btn_tambah_baris_1.TabIndex = 3
        Me.btn_tambah_baris_1.Text = "Tambah Baris"
        Me.btn_tambah_baris_1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_tambah_baris_1.UseVisualStyleBackColor = False
        '
        'txt_id_grey_3
        '
        Me.txt_id_grey_3.Location = New System.Drawing.Point(8, 51)
        Me.txt_id_grey_3.Name = "txt_id_grey_3"
        Me.txt_id_grey_3.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_3.TabIndex = 2
        '
        'txt_id_grey_1
        '
        Me.txt_id_grey_1.Location = New System.Drawing.Point(8, 11)
        Me.txt_id_grey_1.Name = "txt_id_grey_1"
        Me.txt_id_grey_1.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_1.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Label15)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_10)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_9)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_8)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_7)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_6)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_5)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_4)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_3)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_2)
        Me.Panel5.Controls.Add(Me.txt_qty_asal_1)
        Me.Panel5.Location = New System.Drawing.Point(4, 5)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(118, 134)
        Me.Panel5.TabIndex = 87
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(33, 116)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(52, 13)
        Me.Label15.TabIndex = 10
        Me.Label15.Text = "QTY Asal"
        '
        'txt_qty_asal_10
        '
        Me.txt_qty_asal_10.Location = New System.Drawing.Point(58, 91)
        Me.txt_qty_asal_10.Name = "txt_qty_asal_10"
        Me.txt_qty_asal_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_10.TabIndex = 9
        '
        'txt_qty_asal_9
        '
        Me.txt_qty_asal_9.Location = New System.Drawing.Point(58, 71)
        Me.txt_qty_asal_9.Name = "txt_qty_asal_9"
        Me.txt_qty_asal_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_9.TabIndex = 8
        '
        'txt_qty_asal_8
        '
        Me.txt_qty_asal_8.Location = New System.Drawing.Point(58, 51)
        Me.txt_qty_asal_8.Name = "txt_qty_asal_8"
        Me.txt_qty_asal_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_8.TabIndex = 7
        '
        'txt_qty_asal_7
        '
        Me.txt_qty_asal_7.Location = New System.Drawing.Point(58, 31)
        Me.txt_qty_asal_7.Name = "txt_qty_asal_7"
        Me.txt_qty_asal_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_7.TabIndex = 6
        '
        'txt_qty_asal_6
        '
        Me.txt_qty_asal_6.Location = New System.Drawing.Point(58, 11)
        Me.txt_qty_asal_6.Name = "txt_qty_asal_6"
        Me.txt_qty_asal_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_6.TabIndex = 5
        '
        'txt_qty_asal_5
        '
        Me.txt_qty_asal_5.Location = New System.Drawing.Point(8, 91)
        Me.txt_qty_asal_5.Name = "txt_qty_asal_5"
        Me.txt_qty_asal_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_5.TabIndex = 4
        '
        'txt_qty_asal_4
        '
        Me.txt_qty_asal_4.Location = New System.Drawing.Point(8, 71)
        Me.txt_qty_asal_4.Name = "txt_qty_asal_4"
        Me.txt_qty_asal_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_4.TabIndex = 3
        '
        'txt_qty_asal_3
        '
        Me.txt_qty_asal_3.Location = New System.Drawing.Point(8, 51)
        Me.txt_qty_asal_3.Name = "txt_qty_asal_3"
        Me.txt_qty_asal_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_3.TabIndex = 2
        '
        'txt_qty_asal_2
        '
        Me.txt_qty_asal_2.Location = New System.Drawing.Point(8, 31)
        Me.txt_qty_asal_2.Name = "txt_qty_asal_2"
        Me.txt_qty_asal_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_2.TabIndex = 1
        '
        'txt_qty_asal_1
        '
        Me.txt_qty_asal_1.Location = New System.Drawing.Point(8, 11)
        Me.txt_qty_asal_1.Name = "txt_qty_asal_1"
        Me.txt_qty_asal_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_qty_asal_1.TabIndex = 0
        '
        'Panel_10
        '
        Me.Panel_10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_10.Controls.Add(Me.cb_satuan_10)
        Me.Panel_10.Controls.Add(Me.txt_customer_10)
        Me.Panel_10.Controls.Add(Me.txt_gulung_10)
        Me.Panel_10.Controls.Add(Me.txt_partai_10)
        Me.Panel_10.Controls.Add(Me.txt_aksesoris_10)
        Me.Panel_10.Controls.Add(Me.txt_keterangan_10)
        Me.Panel_10.Controls.Add(Me.txt_meter_10)
        Me.Panel_10.Controls.Add(Me.txt_resep_10)
        Me.Panel_10.Controls.Add(Me.txt_warna_10)
        Me.Panel_10.Controls.Add(Me.txt_jenis_kain_10)
        Me.Panel_10.Controls.Add(Me.txt_asal_sj_10)
        Me.Panel_10.Controls.Add(Me.txt_no_urut_10)
        Me.Panel_10.Location = New System.Drawing.Point(-3, 482)
        Me.Panel_10.Name = "Panel_10"
        Me.Panel_10.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_10.TabIndex = 84
        '
        'cb_satuan_10
        '
        Me.cb_satuan_10.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_10.FormattingEnabled = True
        Me.cb_satuan_10.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_10.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_10.Name = "cb_satuan_10"
        Me.cb_satuan_10.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_10.TabIndex = 507
        Me.cb_satuan_10.Text = "Meter"
        '
        'txt_customer_10
        '
        Me.txt_customer_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_10.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_10.Name = "txt_customer_10"
        Me.txt_customer_10.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_10.TabIndex = 90
        '
        'txt_gulung_10
        '
        Me.txt_gulung_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_10.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_10.Name = "txt_gulung_10"
        Me.txt_gulung_10.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_10.TabIndex = 90
        Me.txt_gulung_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_10
        '
        Me.txt_partai_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_10.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_10.Name = "txt_partai_10"
        Me.txt_partai_10.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_10.TabIndex = 90
        '
        'txt_aksesoris_10
        '
        Me.txt_aksesoris_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_10.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_10.Name = "txt_aksesoris_10"
        Me.txt_aksesoris_10.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_10.TabIndex = 8
        '
        'txt_keterangan_10
        '
        Me.txt_keterangan_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_10.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_10.Name = "txt_keterangan_10"
        Me.txt_keterangan_10.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_10.TabIndex = 7
        '
        'txt_meter_10
        '
        Me.txt_meter_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_10.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_10.Name = "txt_meter_10"
        Me.txt_meter_10.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_10.TabIndex = 6
        Me.txt_meter_10.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_10
        '
        Me.txt_resep_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_10.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_10.Name = "txt_resep_10"
        Me.txt_resep_10.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_10.TabIndex = 5
        '
        'txt_warna_10
        '
        Me.txt_warna_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_10.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_10.Name = "txt_warna_10"
        Me.txt_warna_10.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_10.TabIndex = 4
        '
        'txt_jenis_kain_10
        '
        Me.txt_jenis_kain_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_10.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_10.Name = "txt_jenis_kain_10"
        Me.txt_jenis_kain_10.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_10.TabIndex = 2
        '
        'txt_asal_sj_10
        '
        Me.txt_asal_sj_10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_10.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_10.Name = "txt_asal_sj_10"
        Me.txt_asal_sj_10.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_10.TabIndex = 1
        '
        'txt_no_urut_10
        '
        Me.txt_no_urut_10.AutoSize = True
        Me.txt_no_urut_10.Location = New System.Drawing.Point(11, 12)
        Me.txt_no_urut_10.Name = "txt_no_urut_10"
        Me.txt_no_urut_10.Size = New System.Drawing.Size(19, 13)
        Me.txt_no_urut_10.TabIndex = 0
        Me.txt_no_urut_10.Text = "10"
        '
        'txt_id_grey_2
        '
        Me.txt_id_grey_2.Location = New System.Drawing.Point(8, 31)
        Me.txt_id_grey_2.Name = "txt_id_grey_2"
        Me.txt_id_grey_2.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_2.TabIndex = 1
        '
        'Panel_9
        '
        Me.Panel_9.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_9.Controls.Add(Me.cb_satuan_9)
        Me.Panel_9.Controls.Add(Me.txt_partai_9)
        Me.Panel_9.Controls.Add(Me.txt_customer_9)
        Me.Panel_9.Controls.Add(Me.txt_gulung_9)
        Me.Panel_9.Controls.Add(Me.txt_aksesoris_9)
        Me.Panel_9.Controls.Add(Me.txt_keterangan_9)
        Me.Panel_9.Controls.Add(Me.txt_meter_9)
        Me.Panel_9.Controls.Add(Me.txt_resep_9)
        Me.Panel_9.Controls.Add(Me.txt_warna_9)
        Me.Panel_9.Controls.Add(Me.txt_jenis_kain_9)
        Me.Panel_9.Controls.Add(Me.txt_asal_sj_9)
        Me.Panel_9.Controls.Add(Me.txt_no_urut_9)
        Me.Panel_9.Location = New System.Drawing.Point(-3, 447)
        Me.Panel_9.Name = "Panel_9"
        Me.Panel_9.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_9.TabIndex = 83
        '
        'cb_satuan_9
        '
        Me.cb_satuan_9.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_9.FormattingEnabled = True
        Me.cb_satuan_9.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_9.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_9.Name = "cb_satuan_9"
        Me.cb_satuan_9.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_9.TabIndex = 508
        Me.cb_satuan_9.Text = "Meter"
        '
        'txt_partai_9
        '
        Me.txt_partai_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_9.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_9.Name = "txt_partai_9"
        Me.txt_partai_9.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_9.TabIndex = 91
        '
        'txt_customer_9
        '
        Me.txt_customer_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_9.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_9.Name = "txt_customer_9"
        Me.txt_customer_9.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_9.TabIndex = 84
        '
        'txt_gulung_9
        '
        Me.txt_gulung_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_9.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_9.Name = "txt_gulung_9"
        Me.txt_gulung_9.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_9.TabIndex = 84
        Me.txt_gulung_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_aksesoris_9
        '
        Me.txt_aksesoris_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_9.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_9.Name = "txt_aksesoris_9"
        Me.txt_aksesoris_9.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_9.TabIndex = 8
        '
        'txt_keterangan_9
        '
        Me.txt_keterangan_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_9.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_9.Name = "txt_keterangan_9"
        Me.txt_keterangan_9.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_9.TabIndex = 7
        '
        'txt_meter_9
        '
        Me.txt_meter_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_9.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_9.Name = "txt_meter_9"
        Me.txt_meter_9.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_9.TabIndex = 6
        Me.txt_meter_9.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_9
        '
        Me.txt_resep_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_9.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_9.Name = "txt_resep_9"
        Me.txt_resep_9.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_9.TabIndex = 5
        '
        'txt_warna_9
        '
        Me.txt_warna_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_9.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_9.Name = "txt_warna_9"
        Me.txt_warna_9.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_9.TabIndex = 4
        '
        'txt_jenis_kain_9
        '
        Me.txt_jenis_kain_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_9.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_9.Name = "txt_jenis_kain_9"
        Me.txt_jenis_kain_9.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_9.TabIndex = 2
        '
        'txt_asal_sj_9
        '
        Me.txt_asal_sj_9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_9.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_9.Name = "txt_asal_sj_9"
        Me.txt_asal_sj_9.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_9.TabIndex = 1
        '
        'txt_no_urut_9
        '
        Me.txt_no_urut_9.AutoSize = True
        Me.txt_no_urut_9.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_9.Name = "txt_no_urut_9"
        Me.txt_no_urut_9.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_9.TabIndex = 0
        Me.txt_no_urut_9.Text = "9"
        '
        'txt_resep_6
        '
        Me.txt_resep_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_6.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_6.Name = "txt_resep_6"
        Me.txt_resep_6.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_6.TabIndex = 5
        '
        'txt_id_grey_4
        '
        Me.txt_id_grey_4.Location = New System.Drawing.Point(8, 71)
        Me.txt_id_grey_4.Name = "txt_id_grey_4"
        Me.txt_id_grey_4.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_4.TabIndex = 3
        '
        'txt_harga_10
        '
        Me.txt_harga_10.Location = New System.Drawing.Point(56, 91)
        Me.txt_harga_10.Name = "txt_harga_10"
        Me.txt_harga_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_10.TabIndex = 9
        '
        'txt_harga_9
        '
        Me.txt_harga_9.Location = New System.Drawing.Point(56, 71)
        Me.txt_harga_9.Name = "txt_harga_9"
        Me.txt_harga_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_9.TabIndex = 8
        '
        'txt_harga_8
        '
        Me.txt_harga_8.Location = New System.Drawing.Point(56, 51)
        Me.txt_harga_8.Name = "txt_harga_8"
        Me.txt_harga_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_8.TabIndex = 7
        '
        'txt_harga_7
        '
        Me.txt_harga_7.Location = New System.Drawing.Point(56, 31)
        Me.txt_harga_7.Name = "txt_harga_7"
        Me.txt_harga_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_7.TabIndex = 6
        '
        'txt_harga_6
        '
        Me.txt_harga_6.Location = New System.Drawing.Point(56, 11)
        Me.txt_harga_6.Name = "txt_harga_6"
        Me.txt_harga_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_6.TabIndex = 5
        '
        'txt_harga_5
        '
        Me.txt_harga_5.Location = New System.Drawing.Point(6, 91)
        Me.txt_harga_5.Name = "txt_harga_5"
        Me.txt_harga_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_5.TabIndex = 4
        '
        'txt_harga_4
        '
        Me.txt_harga_4.Location = New System.Drawing.Point(6, 71)
        Me.txt_harga_4.Name = "txt_harga_4"
        Me.txt_harga_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_4.TabIndex = 3
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.Controls.Add(Me.Panel10)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Location = New System.Drawing.Point(-3, 9)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1342, 133)
        Me.Panel1.TabIndex = 74
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel10.Controls.Add(Me.dtp_awal)
        Me.Panel10.Controls.Add(Me.txt_gudang)
        Me.Panel10.Controls.Add(Me.txt_no_po)
        Me.Panel10.Controls.Add(Me.Label2)
        Me.Panel10.Controls.Add(Me.Label3)
        Me.Panel10.Controls.Add(Me.Label4)
        Me.Panel10.Location = New System.Drawing.Point(521, 31)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(300, 88)
        Me.Panel10.TabIndex = 81
        '
        'dtp_awal
        '
        Me.dtp_awal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_awal.Location = New System.Drawing.Point(127, 5)
        Me.dtp_awal.Name = "dtp_awal"
        Me.dtp_awal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_awal.TabIndex = 5
        '
        'txt_gudang
        '
        Me.txt_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gudang.Location = New System.Drawing.Point(127, 32)
        Me.txt_gudang.Name = "txt_gudang"
        Me.txt_gudang.Size = New System.Drawing.Size(151, 20)
        Me.txt_gudang.TabIndex = 4
        '
        'txt_no_po
        '
        Me.txt_no_po.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_po.Location = New System.Drawing.Point(127, 59)
        Me.txt_no_po.Name = "txt_no_po"
        Me.txt_no_po.Size = New System.Drawing.Size(151, 20)
        Me.txt_no_po.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(37, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tanggal"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(37, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Gudang"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(37, 61)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "NO PO"
        '
        'Label20
        '
        Me.Label20.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label20.Font = New System.Drawing.Font("Cambria", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.SystemColors.Window
        Me.Label20.Location = New System.Drawing.Point(521, 5)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(300, 26)
        Me.Label20.TabIndex = 80
        Me.Label20.Text = "ORDER PACKING"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Label1)
        Me.Panel7.Controls.Add(Me.txt_harga_10)
        Me.Panel7.Controls.Add(Me.txt_harga_9)
        Me.Panel7.Controls.Add(Me.txt_harga_8)
        Me.Panel7.Controls.Add(Me.txt_harga_7)
        Me.Panel7.Controls.Add(Me.txt_harga_6)
        Me.Panel7.Controls.Add(Me.txt_harga_5)
        Me.Panel7.Controls.Add(Me.txt_harga_4)
        Me.Panel7.Controls.Add(Me.txt_harga_3)
        Me.Panel7.Controls.Add(Me.txt_harga_2)
        Me.Panel7.Controls.Add(Me.txt_harga_1)
        Me.Panel7.Location = New System.Drawing.Point(1134, 5)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(113, 134)
        Me.Panel7.TabIndex = 89
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(34, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "HARGA"
        '
        'txt_harga_3
        '
        Me.txt_harga_3.Location = New System.Drawing.Point(6, 51)
        Me.txt_harga_3.Name = "txt_harga_3"
        Me.txt_harga_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_3.TabIndex = 2
        '
        'txt_harga_2
        '
        Me.txt_harga_2.Location = New System.Drawing.Point(6, 31)
        Me.txt_harga_2.Name = "txt_harga_2"
        Me.txt_harga_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_2.TabIndex = 1
        '
        'txt_harga_1
        '
        Me.txt_harga_1.Location = New System.Drawing.Point(6, 11)
        Me.txt_harga_1.Name = "txt_harga_1"
        Me.txt_harga_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_harga_1.TabIndex = 0
        '
        'dtp_hari_ini
        '
        Me.dtp_hari_ini.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_hari_ini.Location = New System.Drawing.Point(1221, 20)
        Me.dtp_hari_ini.Name = "dtp_hari_ini"
        Me.dtp_hari_ini.Size = New System.Drawing.Size(108, 20)
        Me.dtp_hari_ini.TabIndex = 90
        '
        'txt_id_grey_9
        '
        Me.txt_id_grey_9.Location = New System.Drawing.Point(38, 71)
        Me.txt_id_grey_9.Name = "txt_id_grey_9"
        Me.txt_id_grey_9.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_9.TabIndex = 8
        '
        'txt_id_grey_8
        '
        Me.txt_id_grey_8.Location = New System.Drawing.Point(38, 51)
        Me.txt_id_grey_8.Name = "txt_id_grey_8"
        Me.txt_id_grey_8.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_8.TabIndex = 7
        '
        'txt_id_grey_7
        '
        Me.txt_id_grey_7.Location = New System.Drawing.Point(38, 31)
        Me.txt_id_grey_7.Name = "txt_id_grey_7"
        Me.txt_id_grey_7.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_7.TabIndex = 6
        '
        'txt_id_grey_6
        '
        Me.txt_id_grey_6.Location = New System.Drawing.Point(38, 11)
        Me.txt_id_grey_6.Name = "txt_id_grey_6"
        Me.txt_id_grey_6.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_6.TabIndex = 5
        '
        'txt_id_grey_5
        '
        Me.txt_id_grey_5.Location = New System.Drawing.Point(8, 91)
        Me.txt_id_grey_5.Name = "txt_id_grey_5"
        Me.txt_id_grey_5.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_5.TabIndex = 4
        '
        'txt_id_grey_10
        '
        Me.txt_id_grey_10.Location = New System.Drawing.Point(38, 91)
        Me.txt_id_grey_10.Name = "txt_id_grey_10"
        Me.txt_id_grey_10.Size = New System.Drawing.Size(30, 20)
        Me.txt_id_grey_10.TabIndex = 9
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label14)
        Me.Panel6.Controls.Add(Me.txt_id_grey_10)
        Me.Panel6.Controls.Add(Me.txt_id_grey_9)
        Me.Panel6.Controls.Add(Me.txt_id_grey_8)
        Me.Panel6.Controls.Add(Me.txt_id_grey_7)
        Me.Panel6.Controls.Add(Me.txt_id_grey_6)
        Me.Panel6.Controls.Add(Me.txt_id_grey_5)
        Me.Panel6.Controls.Add(Me.txt_id_grey_4)
        Me.Panel6.Controls.Add(Me.txt_id_grey_3)
        Me.Panel6.Controls.Add(Me.txt_id_grey_2)
        Me.Panel6.Controls.Add(Me.txt_id_grey_1)
        Me.Panel6.Location = New System.Drawing.Point(822, 5)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(76, 134)
        Me.Panel6.TabIndex = 88
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(13, 114)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(51, 13)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "ID GREY"
        '
        'Panel_6
        '
        Me.Panel_6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_6.Controls.Add(Me.cb_satuan_6)
        Me.Panel_6.Controls.Add(Me.txt_customer_6)
        Me.Panel_6.Controls.Add(Me.txt_gulung_6)
        Me.Panel_6.Controls.Add(Me.txt_partai_6)
        Me.Panel_6.Controls.Add(Me.txt_aksesoris_6)
        Me.Panel_6.Controls.Add(Me.txt_keterangan_6)
        Me.Panel_6.Controls.Add(Me.txt_meter_6)
        Me.Panel_6.Controls.Add(Me.txt_resep_6)
        Me.Panel_6.Controls.Add(Me.txt_warna_6)
        Me.Panel_6.Controls.Add(Me.txt_jenis_kain_6)
        Me.Panel_6.Controls.Add(Me.txt_asal_sj_6)
        Me.Panel_6.Controls.Add(Me.txt_no_urut_6)
        Me.Panel_6.Location = New System.Drawing.Point(-3, 342)
        Me.Panel_6.Name = "Panel_6"
        Me.Panel_6.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_6.TabIndex = 81
        '
        'cb_satuan_6
        '
        Me.cb_satuan_6.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_6.FormattingEnabled = True
        Me.cb_satuan_6.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_6.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_6.Name = "cb_satuan_6"
        Me.cb_satuan_6.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_6.TabIndex = 512
        Me.cb_satuan_6.Text = "Meter"
        '
        'txt_customer_6
        '
        Me.txt_customer_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_6.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_6.Name = "txt_customer_6"
        Me.txt_customer_6.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_6.TabIndex = 92
        '
        'txt_gulung_6
        '
        Me.txt_gulung_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_6.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_6.Name = "txt_gulung_6"
        Me.txt_gulung_6.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_6.TabIndex = 91
        Me.txt_gulung_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_6
        '
        Me.txt_partai_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_6.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_6.Name = "txt_partai_6"
        Me.txt_partai_6.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_6.TabIndex = 92
        '
        'txt_aksesoris_6
        '
        Me.txt_aksesoris_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_6.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_6.Name = "txt_aksesoris_6"
        Me.txt_aksesoris_6.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_6.TabIndex = 8
        '
        'txt_keterangan_6
        '
        Me.txt_keterangan_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_6.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_6.Name = "txt_keterangan_6"
        Me.txt_keterangan_6.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_6.TabIndex = 7
        '
        'txt_meter_6
        '
        Me.txt_meter_6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_6.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_6.Name = "txt_meter_6"
        Me.txt_meter_6.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_6.TabIndex = 6
        Me.txt_meter_6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_gulung_7
        '
        Me.txt_gulung_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_7.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_7.Name = "txt_gulung_7"
        Me.txt_gulung_7.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_7.TabIndex = 86
        Me.txt_gulung_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_customer_7
        '
        Me.txt_customer_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_7.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_7.Name = "txt_customer_7"
        Me.txt_customer_7.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_7.TabIndex = 85
        '
        'txt_aksesoris_7
        '
        Me.txt_aksesoris_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_7.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_7.Name = "txt_aksesoris_7"
        Me.txt_aksesoris_7.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_7.TabIndex = 8
        '
        'txt_partai_7
        '
        Me.txt_partai_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_7.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_7.Name = "txt_partai_7"
        Me.txt_partai_7.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_7.TabIndex = 85
        '
        'txt_keterangan_7
        '
        Me.txt_keterangan_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_7.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_7.Name = "txt_keterangan_7"
        Me.txt_keterangan_7.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_7.TabIndex = 7
        '
        'Panel_8
        '
        Me.Panel_8.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_8.Controls.Add(Me.cb_satuan_8)
        Me.Panel_8.Controls.Add(Me.txt_customer_8)
        Me.Panel_8.Controls.Add(Me.txt_gulung_8)
        Me.Panel_8.Controls.Add(Me.txt_partai_8)
        Me.Panel_8.Controls.Add(Me.txt_aksesoris_8)
        Me.Panel_8.Controls.Add(Me.txt_keterangan_8)
        Me.Panel_8.Controls.Add(Me.txt_meter_8)
        Me.Panel_8.Controls.Add(Me.txt_resep_8)
        Me.Panel_8.Controls.Add(Me.txt_warna_8)
        Me.Panel_8.Controls.Add(Me.txt_jenis_kain_8)
        Me.Panel_8.Controls.Add(Me.txt_asal_sj_8)
        Me.Panel_8.Controls.Add(Me.txt_no_urut_8)
        Me.Panel_8.Location = New System.Drawing.Point(-3, 412)
        Me.Panel_8.Name = "Panel_8"
        Me.Panel_8.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_8.TabIndex = 85
        '
        'cb_satuan_8
        '
        Me.cb_satuan_8.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_8.FormattingEnabled = True
        Me.cb_satuan_8.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_8.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_8.Name = "cb_satuan_8"
        Me.cb_satuan_8.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_8.TabIndex = 510
        Me.cb_satuan_8.Text = "Meter"
        '
        'txt_customer_8
        '
        Me.txt_customer_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_customer_8.Location = New System.Drawing.Point(792, 8)
        Me.txt_customer_8.Name = "txt_customer_8"
        Me.txt_customer_8.Size = New System.Drawing.Size(117, 20)
        Me.txt_customer_8.TabIndex = 91
        '
        'txt_gulung_8
        '
        Me.txt_gulung_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_gulung_8.Location = New System.Drawing.Point(594, 8)
        Me.txt_gulung_8.Name = "txt_gulung_8"
        Me.txt_gulung_8.Size = New System.Drawing.Size(55, 20)
        Me.txt_gulung_8.TabIndex = 85
        Me.txt_gulung_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_partai_8
        '
        Me.txt_partai_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_partai_8.Location = New System.Drawing.Point(528, 8)
        Me.txt_partai_8.Name = "txt_partai_8"
        Me.txt_partai_8.Size = New System.Drawing.Size(55, 20)
        Me.txt_partai_8.TabIndex = 84
        '
        'txt_aksesoris_8
        '
        Me.txt_aksesoris_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_aksesoris_8.Location = New System.Drawing.Point(920, 8)
        Me.txt_aksesoris_8.Name = "txt_aksesoris_8"
        Me.txt_aksesoris_8.Size = New System.Drawing.Size(190, 20)
        Me.txt_aksesoris_8.TabIndex = 8
        '
        'txt_keterangan_8
        '
        Me.txt_keterangan_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan_8.Location = New System.Drawing.Point(1121, 8)
        Me.txt_keterangan_8.Name = "txt_keterangan_8"
        Me.txt_keterangan_8.Size = New System.Drawing.Size(190, 20)
        Me.txt_keterangan_8.TabIndex = 7
        '
        'txt_meter_8
        '
        Me.txt_meter_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_8.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_8.Name = "txt_meter_8"
        Me.txt_meter_8.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_8.TabIndex = 6
        Me.txt_meter_8.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt_resep_8
        '
        Me.txt_resep_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_8.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_8.Name = "txt_resep_8"
        Me.txt_resep_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_8.TabIndex = 5
        '
        'txt_warna_8
        '
        Me.txt_warna_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_8.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_8.Name = "txt_warna_8"
        Me.txt_warna_8.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_8.TabIndex = 4
        '
        'txt_jenis_kain_8
        '
        Me.txt_jenis_kain_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_8.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_8.Name = "txt_jenis_kain_8"
        Me.txt_jenis_kain_8.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_8.TabIndex = 2
        '
        'txt_asal_sj_8
        '
        Me.txt_asal_sj_8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_8.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_8.Name = "txt_asal_sj_8"
        Me.txt_asal_sj_8.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_8.TabIndex = 1
        '
        'txt_no_urut_8
        '
        Me.txt_no_urut_8.AutoSize = True
        Me.txt_no_urut_8.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_8.Name = "txt_no_urut_8"
        Me.txt_no_urut_8.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_8.TabIndex = 0
        Me.txt_no_urut_8.Text = "8"
        '
        'txt_resep_7
        '
        Me.txt_resep_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_resep_7.Location = New System.Drawing.Point(417, 8)
        Me.txt_resep_7.Name = "txt_resep_7"
        Me.txt_resep_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_resep_7.TabIndex = 5
        '
        'txt_warna_7
        '
        Me.txt_warna_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_warna_7.Location = New System.Drawing.Point(306, 8)
        Me.txt_warna_7.Name = "txt_warna_7"
        Me.txt_warna_7.Size = New System.Drawing.Size(100, 20)
        Me.txt_warna_7.TabIndex = 4
        '
        'txt_jenis_kain_7
        '
        Me.txt_jenis_kain_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain_7.Location = New System.Drawing.Point(135, 8)
        Me.txt_jenis_kain_7.Name = "txt_jenis_kain_7"
        Me.txt_jenis_kain_7.Size = New System.Drawing.Size(160, 20)
        Me.txt_jenis_kain_7.TabIndex = 2
        '
        'txt_asal_sj_7
        '
        Me.txt_asal_sj_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_asal_sj_7.Location = New System.Drawing.Point(39, 8)
        Me.txt_asal_sj_7.Name = "txt_asal_sj_7"
        Me.txt_asal_sj_7.Size = New System.Drawing.Size(85, 20)
        Me.txt_asal_sj_7.TabIndex = 1
        '
        'txt_no_urut_7
        '
        Me.txt_no_urut_7.AutoSize = True
        Me.txt_no_urut_7.Location = New System.Drawing.Point(14, 12)
        Me.txt_no_urut_7.Name = "txt_no_urut_7"
        Me.txt_no_urut_7.Size = New System.Drawing.Size(13, 13)
        Me.txt_no_urut_7.TabIndex = 0
        Me.txt_no_urut_7.Text = "7"
        '
        'Panel_7
        '
        Me.Panel_7.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_7.Controls.Add(Me.cb_satuan_7)
        Me.Panel_7.Controls.Add(Me.txt_gulung_7)
        Me.Panel_7.Controls.Add(Me.txt_customer_7)
        Me.Panel_7.Controls.Add(Me.txt_aksesoris_7)
        Me.Panel_7.Controls.Add(Me.txt_partai_7)
        Me.Panel_7.Controls.Add(Me.txt_keterangan_7)
        Me.Panel_7.Controls.Add(Me.txt_meter_7)
        Me.Panel_7.Controls.Add(Me.txt_resep_7)
        Me.Panel_7.Controls.Add(Me.txt_warna_7)
        Me.Panel_7.Controls.Add(Me.txt_jenis_kain_7)
        Me.Panel_7.Controls.Add(Me.txt_asal_sj_7)
        Me.Panel_7.Controls.Add(Me.txt_no_urut_7)
        Me.Panel_7.Location = New System.Drawing.Point(-3, 377)
        Me.Panel_7.Name = "Panel_7"
        Me.Panel_7.Size = New System.Drawing.Size(1342, 36)
        Me.Panel_7.TabIndex = 82
        '
        'cb_satuan_7
        '
        Me.cb_satuan_7.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan_7.FormattingEnabled = True
        Me.cb_satuan_7.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan_7.Location = New System.Drawing.Point(731, 6)
        Me.cb_satuan_7.Name = "cb_satuan_7"
        Me.cb_satuan_7.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan_7.TabIndex = 509
        Me.cb_satuan_7.Text = "Meter"
        '
        'txt_meter_7
        '
        Me.txt_meter_7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_meter_7.Location = New System.Drawing.Point(660, 8)
        Me.txt_meter_7.Name = "txt_meter_7"
        Me.txt_meter_7.Size = New System.Drawing.Size(60, 20)
        Me.txt_meter_7.TabIndex = 6
        Me.txt_meter_7.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label24)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_10)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_9)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_8)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_7)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_6)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_5)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_4)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_3)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_2)
        Me.Panel8.Controls.Add(Me.txt_id_sj_proses_1)
        Me.Panel8.Location = New System.Drawing.Point(1014, 5)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(120, 141)
        Me.Panel8.TabIndex = 91
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(26, 117)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(68, 13)
        Me.Label24.TabIndex = 10
        Me.Label24.Text = "ID SJ Proses"
        '
        'txt_id_sj_proses_10
        '
        Me.txt_id_sj_proses_10.Location = New System.Drawing.Point(60, 94)
        Me.txt_id_sj_proses_10.Name = "txt_id_sj_proses_10"
        Me.txt_id_sj_proses_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_10.TabIndex = 9
        '
        'txt_id_sj_proses_9
        '
        Me.txt_id_sj_proses_9.Location = New System.Drawing.Point(60, 74)
        Me.txt_id_sj_proses_9.Name = "txt_id_sj_proses_9"
        Me.txt_id_sj_proses_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_9.TabIndex = 8
        '
        'txt_id_sj_proses_8
        '
        Me.txt_id_sj_proses_8.Location = New System.Drawing.Point(60, 54)
        Me.txt_id_sj_proses_8.Name = "txt_id_sj_proses_8"
        Me.txt_id_sj_proses_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_8.TabIndex = 7
        '
        'txt_id_sj_proses_7
        '
        Me.txt_id_sj_proses_7.Location = New System.Drawing.Point(60, 34)
        Me.txt_id_sj_proses_7.Name = "txt_id_sj_proses_7"
        Me.txt_id_sj_proses_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_7.TabIndex = 6
        '
        'txt_id_sj_proses_6
        '
        Me.txt_id_sj_proses_6.Location = New System.Drawing.Point(60, 14)
        Me.txt_id_sj_proses_6.Name = "txt_id_sj_proses_6"
        Me.txt_id_sj_proses_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_6.TabIndex = 5
        '
        'txt_id_sj_proses_5
        '
        Me.txt_id_sj_proses_5.Location = New System.Drawing.Point(10, 94)
        Me.txt_id_sj_proses_5.Name = "txt_id_sj_proses_5"
        Me.txt_id_sj_proses_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_5.TabIndex = 4
        '
        'txt_id_sj_proses_4
        '
        Me.txt_id_sj_proses_4.Location = New System.Drawing.Point(10, 74)
        Me.txt_id_sj_proses_4.Name = "txt_id_sj_proses_4"
        Me.txt_id_sj_proses_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_4.TabIndex = 3
        '
        'txt_id_sj_proses_3
        '
        Me.txt_id_sj_proses_3.Location = New System.Drawing.Point(10, 54)
        Me.txt_id_sj_proses_3.Name = "txt_id_sj_proses_3"
        Me.txt_id_sj_proses_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_3.TabIndex = 2
        '
        'txt_id_sj_proses_2
        '
        Me.txt_id_sj_proses_2.Location = New System.Drawing.Point(10, 34)
        Me.txt_id_sj_proses_2.Name = "txt_id_sj_proses_2"
        Me.txt_id_sj_proses_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_2.TabIndex = 1
        '
        'txt_id_sj_proses_1
        '
        Me.txt_id_sj_proses_1.Location = New System.Drawing.Point(10, 14)
        Me.txt_id_sj_proses_1.Name = "txt_id_sj_proses_1"
        Me.txt_id_sj_proses_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_sj_proses_1.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label7)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_10)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_9)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_8)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_7)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_6)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_5)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_4)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_3)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_2)
        Me.Panel4.Controls.Add(Me.txt_id_po_packing_1)
        Me.Panel4.Location = New System.Drawing.Point(898, 5)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(116, 134)
        Me.Panel4.TabIndex = 112
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(23, 114)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "ID PO Packing"
        '
        'txt_id_po_packing_10
        '
        Me.txt_id_po_packing_10.Location = New System.Drawing.Point(58, 90)
        Me.txt_id_po_packing_10.Name = "txt_id_po_packing_10"
        Me.txt_id_po_packing_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_10.TabIndex = 9
        '
        'txt_id_po_packing_9
        '
        Me.txt_id_po_packing_9.Location = New System.Drawing.Point(58, 70)
        Me.txt_id_po_packing_9.Name = "txt_id_po_packing_9"
        Me.txt_id_po_packing_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_9.TabIndex = 8
        '
        'txt_id_po_packing_8
        '
        Me.txt_id_po_packing_8.Location = New System.Drawing.Point(58, 50)
        Me.txt_id_po_packing_8.Name = "txt_id_po_packing_8"
        Me.txt_id_po_packing_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_8.TabIndex = 7
        '
        'txt_id_po_packing_7
        '
        Me.txt_id_po_packing_7.Location = New System.Drawing.Point(58, 30)
        Me.txt_id_po_packing_7.Name = "txt_id_po_packing_7"
        Me.txt_id_po_packing_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_7.TabIndex = 6
        '
        'txt_id_po_packing_6
        '
        Me.txt_id_po_packing_6.Location = New System.Drawing.Point(58, 10)
        Me.txt_id_po_packing_6.Name = "txt_id_po_packing_6"
        Me.txt_id_po_packing_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_6.TabIndex = 5
        '
        'txt_id_po_packing_5
        '
        Me.txt_id_po_packing_5.Location = New System.Drawing.Point(8, 90)
        Me.txt_id_po_packing_5.Name = "txt_id_po_packing_5"
        Me.txt_id_po_packing_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_5.TabIndex = 4
        '
        'txt_id_po_packing_4
        '
        Me.txt_id_po_packing_4.Location = New System.Drawing.Point(8, 70)
        Me.txt_id_po_packing_4.Name = "txt_id_po_packing_4"
        Me.txt_id_po_packing_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_4.TabIndex = 3
        '
        'txt_id_po_packing_3
        '
        Me.txt_id_po_packing_3.Location = New System.Drawing.Point(8, 50)
        Me.txt_id_po_packing_3.Name = "txt_id_po_packing_3"
        Me.txt_id_po_packing_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_3.TabIndex = 2
        '
        'txt_id_po_packing_2
        '
        Me.txt_id_po_packing_2.Location = New System.Drawing.Point(8, 30)
        Me.txt_id_po_packing_2.Name = "txt_id_po_packing_2"
        Me.txt_id_po_packing_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_2.TabIndex = 1
        '
        'txt_id_po_packing_1
        '
        Me.txt_id_po_packing_1.Location = New System.Drawing.Point(8, 10)
        Me.txt_id_po_packing_1.Name = "txt_id_po_packing_1"
        Me.txt_id_po_packing_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_id_po_packing_1.TabIndex = 0
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.Label28)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_10)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_9)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_8)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_7)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_6)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_5)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_4)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_3)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_2)
        Me.Panel12.Controls.Add(Me.txt_asal_satuan_1)
        Me.Panel12.Location = New System.Drawing.Point(122, 5)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(109, 134)
        Me.Panel12.TabIndex = 114
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(22, 113)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(64, 13)
        Me.Label28.TabIndex = 10
        Me.Label28.Text = "Asal Satuan"
        '
        'txt_asal_satuan_10
        '
        Me.txt_asal_satuan_10.Location = New System.Drawing.Point(54, 89)
        Me.txt_asal_satuan_10.Name = "txt_asal_satuan_10"
        Me.txt_asal_satuan_10.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_10.TabIndex = 9
        '
        'txt_asal_satuan_9
        '
        Me.txt_asal_satuan_9.Location = New System.Drawing.Point(54, 69)
        Me.txt_asal_satuan_9.Name = "txt_asal_satuan_9"
        Me.txt_asal_satuan_9.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_9.TabIndex = 8
        '
        'txt_asal_satuan_8
        '
        Me.txt_asal_satuan_8.Location = New System.Drawing.Point(54, 49)
        Me.txt_asal_satuan_8.Name = "txt_asal_satuan_8"
        Me.txt_asal_satuan_8.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_8.TabIndex = 7
        '
        'txt_asal_satuan_7
        '
        Me.txt_asal_satuan_7.Location = New System.Drawing.Point(54, 29)
        Me.txt_asal_satuan_7.Name = "txt_asal_satuan_7"
        Me.txt_asal_satuan_7.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_7.TabIndex = 6
        '
        'txt_asal_satuan_6
        '
        Me.txt_asal_satuan_6.Location = New System.Drawing.Point(54, 9)
        Me.txt_asal_satuan_6.Name = "txt_asal_satuan_6"
        Me.txt_asal_satuan_6.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_6.TabIndex = 5
        '
        'txt_asal_satuan_5
        '
        Me.txt_asal_satuan_5.Location = New System.Drawing.Point(4, 89)
        Me.txt_asal_satuan_5.Name = "txt_asal_satuan_5"
        Me.txt_asal_satuan_5.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_5.TabIndex = 4
        '
        'txt_asal_satuan_4
        '
        Me.txt_asal_satuan_4.Location = New System.Drawing.Point(4, 69)
        Me.txt_asal_satuan_4.Name = "txt_asal_satuan_4"
        Me.txt_asal_satuan_4.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_4.TabIndex = 3
        '
        'txt_asal_satuan_3
        '
        Me.txt_asal_satuan_3.Location = New System.Drawing.Point(4, 49)
        Me.txt_asal_satuan_3.Name = "txt_asal_satuan_3"
        Me.txt_asal_satuan_3.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_3.TabIndex = 2
        '
        'txt_asal_satuan_2
        '
        Me.txt_asal_satuan_2.Location = New System.Drawing.Point(4, 29)
        Me.txt_asal_satuan_2.Name = "txt_asal_satuan_2"
        Me.txt_asal_satuan_2.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_2.TabIndex = 1
        '
        'txt_asal_satuan_1
        '
        Me.txt_asal_satuan_1.Location = New System.Drawing.Point(4, 9)
        Me.txt_asal_satuan_1.Name = "txt_asal_satuan_1"
        Me.txt_asal_satuan_1.Size = New System.Drawing.Size(50, 20)
        Me.txt_asal_satuan_1.TabIndex = 0
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_6)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_7)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_10)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_9)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_8)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_5)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_4)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_3)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_2)
        Me.Panel11.Controls.Add(Me.dtp_tanggal_sj_1)
        Me.Panel11.Controls.Add(Me.Label27)
        Me.Panel11.Location = New System.Drawing.Point(231, 5)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(183, 129)
        Me.Panel11.TabIndex = 118
        '
        'dtp_tanggal_sj_6
        '
        Me.dtp_tanggal_sj_6.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_6.Location = New System.Drawing.Point(94, 9)
        Me.dtp_tanggal_sj_6.Name = "dtp_tanggal_sj_6"
        Me.dtp_tanggal_sj_6.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_6.TabIndex = 88
        '
        'dtp_tanggal_sj_7
        '
        Me.dtp_tanggal_sj_7.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_7.Location = New System.Drawing.Point(94, 29)
        Me.dtp_tanggal_sj_7.Name = "dtp_tanggal_sj_7"
        Me.dtp_tanggal_sj_7.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_7.TabIndex = 87
        '
        'dtp_tanggal_sj_10
        '
        Me.dtp_tanggal_sj_10.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_10.Location = New System.Drawing.Point(94, 89)
        Me.dtp_tanggal_sj_10.Name = "dtp_tanggal_sj_10"
        Me.dtp_tanggal_sj_10.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_10.TabIndex = 86
        '
        'dtp_tanggal_sj_9
        '
        Me.dtp_tanggal_sj_9.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_9.Location = New System.Drawing.Point(94, 69)
        Me.dtp_tanggal_sj_9.Name = "dtp_tanggal_sj_9"
        Me.dtp_tanggal_sj_9.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_9.TabIndex = 85
        '
        'dtp_tanggal_sj_8
        '
        Me.dtp_tanggal_sj_8.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_8.Location = New System.Drawing.Point(94, 49)
        Me.dtp_tanggal_sj_8.Name = "dtp_tanggal_sj_8"
        Me.dtp_tanggal_sj_8.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_8.TabIndex = 84
        '
        'dtp_tanggal_sj_5
        '
        Me.dtp_tanggal_sj_5.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_5.Location = New System.Drawing.Point(8, 89)
        Me.dtp_tanggal_sj_5.Name = "dtp_tanggal_sj_5"
        Me.dtp_tanggal_sj_5.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_5.TabIndex = 83
        '
        'dtp_tanggal_sj_4
        '
        Me.dtp_tanggal_sj_4.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_4.Location = New System.Drawing.Point(8, 69)
        Me.dtp_tanggal_sj_4.Name = "dtp_tanggal_sj_4"
        Me.dtp_tanggal_sj_4.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_4.TabIndex = 82
        '
        'dtp_tanggal_sj_3
        '
        Me.dtp_tanggal_sj_3.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_3.Location = New System.Drawing.Point(8, 49)
        Me.dtp_tanggal_sj_3.Name = "dtp_tanggal_sj_3"
        Me.dtp_tanggal_sj_3.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_3.TabIndex = 81
        '
        'dtp_tanggal_sj_2
        '
        Me.dtp_tanggal_sj_2.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_2.Location = New System.Drawing.Point(8, 29)
        Me.dtp_tanggal_sj_2.Name = "dtp_tanggal_sj_2"
        Me.dtp_tanggal_sj_2.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_2.TabIndex = 80
        '
        'dtp_tanggal_sj_1
        '
        Me.dtp_tanggal_sj_1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal_sj_1.Location = New System.Drawing.Point(8, 9)
        Me.dtp_tanggal_sj_1.Name = "dtp_tanggal_sj_1"
        Me.dtp_tanggal_sj_1.Size = New System.Drawing.Size(80, 20)
        Me.dtp_tanggal_sj_1.TabIndex = 79
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(61, 112)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(61, 13)
        Me.Label27.TabIndex = 78
        Me.Label27.Text = "Tanggal SJ"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Label21)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_10)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_9)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_8)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_7)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_6)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_5)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_4)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_3)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_2)
        Me.Panel9.Controls.Add(Me.txt_status_sj_proses_1)
        Me.Panel9.Location = New System.Drawing.Point(418, 5)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(94, 134)
        Me.Panel9.TabIndex = 119
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(4, 113)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(87, 13)
        Me.Label21.TabIndex = 10
        Me.Label21.Text = "Status SJ Proses"
        '
        'txt_status_sj_proses_10
        '
        Me.txt_status_sj_proses_10.Location = New System.Drawing.Point(43, 89)
        Me.txt_status_sj_proses_10.Name = "txt_status_sj_proses_10"
        Me.txt_status_sj_proses_10.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_10.TabIndex = 9
        '
        'txt_status_sj_proses_9
        '
        Me.txt_status_sj_proses_9.Location = New System.Drawing.Point(43, 69)
        Me.txt_status_sj_proses_9.Name = "txt_status_sj_proses_9"
        Me.txt_status_sj_proses_9.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_9.TabIndex = 8
        '
        'txt_status_sj_proses_8
        '
        Me.txt_status_sj_proses_8.Location = New System.Drawing.Point(43, 49)
        Me.txt_status_sj_proses_8.Name = "txt_status_sj_proses_8"
        Me.txt_status_sj_proses_8.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_8.TabIndex = 7
        '
        'txt_status_sj_proses_7
        '
        Me.txt_status_sj_proses_7.Location = New System.Drawing.Point(43, 29)
        Me.txt_status_sj_proses_7.Name = "txt_status_sj_proses_7"
        Me.txt_status_sj_proses_7.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_7.TabIndex = 6
        '
        'txt_status_sj_proses_6
        '
        Me.txt_status_sj_proses_6.Location = New System.Drawing.Point(43, 9)
        Me.txt_status_sj_proses_6.Name = "txt_status_sj_proses_6"
        Me.txt_status_sj_proses_6.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_6.TabIndex = 5
        '
        'txt_status_sj_proses_5
        '
        Me.txt_status_sj_proses_5.Location = New System.Drawing.Point(8, 89)
        Me.txt_status_sj_proses_5.Name = "txt_status_sj_proses_5"
        Me.txt_status_sj_proses_5.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_5.TabIndex = 4
        '
        'txt_status_sj_proses_4
        '
        Me.txt_status_sj_proses_4.Location = New System.Drawing.Point(8, 69)
        Me.txt_status_sj_proses_4.Name = "txt_status_sj_proses_4"
        Me.txt_status_sj_proses_4.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_4.TabIndex = 3
        '
        'txt_status_sj_proses_3
        '
        Me.txt_status_sj_proses_3.Location = New System.Drawing.Point(8, 49)
        Me.txt_status_sj_proses_3.Name = "txt_status_sj_proses_3"
        Me.txt_status_sj_proses_3.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_3.TabIndex = 2
        '
        'txt_status_sj_proses_2
        '
        Me.txt_status_sj_proses_2.Location = New System.Drawing.Point(8, 29)
        Me.txt_status_sj_proses_2.Name = "txt_status_sj_proses_2"
        Me.txt_status_sj_proses_2.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_2.TabIndex = 1
        '
        'txt_status_sj_proses_1
        '
        Me.txt_status_sj_proses_1.Location = New System.Drawing.Point(8, 9)
        Me.txt_status_sj_proses_1.Name = "txt_status_sj_proses_1"
        Me.txt_status_sj_proses_1.Size = New System.Drawing.Size(35, 20)
        Me.txt_status_sj_proses_1.TabIndex = 0
        '
        'form_input_po_packing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1336, 618)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.Panel11)
        Me.Controls.Add(Me.Panel12)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel_1)
        Me.Controls.Add(Me.Panel_5)
        Me.Controls.Add(Me.Panel_4)
        Me.Controls.Add(Me.Panel_3)
        Me.Controls.Add(Me.Panel_2)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel_10)
        Me.Controls.Add(Me.Panel_9)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.Panel6)
        Me.Controls.Add(Me.Panel_6)
        Me.Controls.Add(Me.Panel_8)
        Me.Controls.Add(Me.Panel_7)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.dtp_hari_ini)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_po_packing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FORM INPUT PO PACKING BARU"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel_1.ResumeLayout(False)
        Me.Panel_1.PerformLayout()
        Me.Panel_5.ResumeLayout(False)
        Me.Panel_5.PerformLayout()
        Me.Panel_4.ResumeLayout(False)
        Me.Panel_4.PerformLayout()
        Me.Panel_3.ResumeLayout(False)
        Me.Panel_3.PerformLayout()
        Me.Panel_2.ResumeLayout(False)
        Me.Panel_2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel_10.ResumeLayout(False)
        Me.Panel_10.PerformLayout()
        Me.Panel_9.ResumeLayout(False)
        Me.Panel_9.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel_6.ResumeLayout(False)
        Me.Panel_6.PerformLayout()
        Me.Panel_8.ResumeLayout(False)
        Me.Panel_8.PerformLayout()
        Me.Panel_7.ResumeLayout(False)
        Me.Panel_7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txt_keterangan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer_3 As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel_1 As System.Windows.Forms.Panel
    Friend WithEvents txt_aksesoris_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_1 As System.Windows.Forms.Label
    Friend WithEvents txt_aksesoris_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_6 As System.Windows.Forms.Label
    Friend WithEvents Panel_5 As System.Windows.Forms.Panel
    Friend WithEvents txt_gulung_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_aksesoris_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_5 As System.Windows.Forms.Label
    Friend WithEvents btn_selesai_1 As System.Windows.Forms.Button
    Friend WithEvents Panel_4 As System.Windows.Forms.Panel
    Friend WithEvents txt_customer_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_aksesoris_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_4 As System.Windows.Forms.Label
    Friend WithEvents txt_gulung_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_3 As System.Windows.Forms.Label
    Friend WithEvents Panel_3 As System.Windows.Forms.Panel
    Friend WithEvents btn_hapus_10 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_10 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_9 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_9 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_9 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_8 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_8 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_8 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_7 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_7 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_7 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_6 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_6 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_6 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_5 As System.Windows.Forms.Button
    Friend WithEvents Panel_2 As System.Windows.Forms.Panel
    Friend WithEvents txt_aksesoris_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_2 As System.Windows.Forms.Label
    Friend WithEvents btn_selesai_5 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_5 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_4 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_4 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_4 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_3 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_3 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_3 As System.Windows.Forms.Button
    Friend WithEvents btn_hapus_2 As System.Windows.Forms.Button
    Friend WithEvents btn_selesai_2 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_2 As System.Windows.Forms.Button
    Friend WithEvents btn_tambah_baris_1 As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txt_id_grey_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txt_qty_asal_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_qty_asal_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_10 As System.Windows.Forms.Panel
    Friend WithEvents txt_customer_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_aksesoris_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_10 As System.Windows.Forms.Label
    Friend WithEvents txt_id_grey_2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_9 As System.Windows.Forms.Panel
    Friend WithEvents txt_partai_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_aksesoris_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_9 As System.Windows.Forms.Label
    Friend WithEvents txt_resep_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_4 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dtp_awal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_no_po As System.Windows.Forms.TextBox
    Friend WithEvents txt_gudang As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents txt_harga_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga_1 As System.Windows.Forms.TextBox
    Friend WithEvents dtp_hari_ini As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_id_grey_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_grey_10 As System.Windows.Forms.TextBox
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel_6 As System.Windows.Forms.Panel
    Friend WithEvents txt_customer_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_aksesoris_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_customer_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_aksesoris_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_7 As System.Windows.Forms.TextBox
    Friend WithEvents Panel_8 As System.Windows.Forms.Panel
    Friend WithEvents txt_customer_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_gulung_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_partai_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_aksesoris_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_meter_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_resep_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_8 As System.Windows.Forms.Label
    Friend WithEvents txt_resep_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_warna_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_sj_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_no_urut_7 As System.Windows.Forms.Label
    Friend WithEvents Panel_7 As System.Windows.Forms.Panel
    Friend WithEvents txt_meter_7 As System.Windows.Forms.TextBox
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txt_id_sj_proses_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_sj_proses_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txt_id_po_packing_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_id_po_packing_1 As System.Windows.Forms.TextBox
    Friend WithEvents btn_batal_10 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_9 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_8 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_7 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_6 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_5 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_4 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_3 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_2 As System.Windows.Forms.Button
    Friend WithEvents btn_batal_1 As System.Windows.Forms.Button
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cb_satuan_1 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_5 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_4 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_3 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_2 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_10 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_9 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_6 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_8 As System.Windows.Forms.ComboBox
    Friend WithEvents cb_satuan_7 As System.Windows.Forms.ComboBox
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txt_asal_satuan_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_asal_satuan_1 As System.Windows.Forms.TextBox
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents dtp_tanggal_sj_6 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_7 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_10 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_9 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_8 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_5 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_4 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtp_tanggal_sj_1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txt_status_sj_proses_10 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_9 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_8 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_7 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_6 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_5 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_4 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_status_sj_proses_1 As System.Windows.Forms.TextBox
End Class
