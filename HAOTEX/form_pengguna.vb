﻿Imports MySql.Data.MySqlClient

Public Class form_pengguna

    Private Sub form_pengguna_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call bersih_text()
        Call isidgv()
    End Sub

    Private Sub bersih_text()
        txt_username.Text = ""
        txt_password.Text = ""
        txt_nama.Text = ""
        btn_hapus.Enabled = False
        btn_simpan.Enabled = True
        btn_simpan.Text = "Simpan"
        cb_hak_akses.Text = "-- Pilih --"
        txt_username.Enabled = True
    End Sub

    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Username,Password,Nama,Hak_Akses FROM tbpengguna"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbpengguna")
                            DataGridView1.DataSource = dsx.Tables("tbpengguna")
                            DataGridView1.ReadOnly = True
                            DataGridView1.Columns(0).Width = 120
                            DataGridView1.Columns(1).Width = 120
                            DataGridView1.Columns(2).Width = 120
                            DataGridView1.Columns(2).Width = 80
                            DataGridView1.Columns(0).HeaderText = "Username"
                            DataGridView1.Columns(1).HeaderText = "Password"
                            DataGridView1.Columns(2).HeaderText = "Nama"
                            DataGridView1.Columns(3).HeaderText = "Hak Akses"
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.Click
        Try
            Dim i As Integer
            i = Me.DataGridView1.CurrentRow.Index
            With DataGridView1.Rows.Item(i)
                txt_username.Text = .Cells(0).Value
                txt_password.Text = .Cells(1).Value
                txt_nama.Text = .Cells(2).Value
                cb_hak_akses.Text = .Cells(3).Value
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try
        txt_username.Enabled = False
        btn_hapus.Enabled = True
        btn_simpan.Text = "Ubah"
    End Sub

    Private Sub btn_simpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_simpan.Click
        Try
            If btn_simpan.Text = "Simpan" Then
                If txt_username.Text = "" Then
                    MsgBox("Username Belum Diinput")
                    txt_username.Focus()
                ElseIf txt_password.Text = "" Then
                    MsgBox("Password Belum Diinput")
                    txt_password.Focus()
                ElseIf txt_nama.Text = "" Then
                    MsgBox("Nama Belum Diinput")
                    txt_nama.Focus()
                ElseIf cb_hak_akses.Text = "-- Pilih --" Then
                    MsgBox("Hak Akses Belum Diinput")
                    cb_hak_akses.Focus()
                Else
                    Using conx As New MySqlConnection(sLocalConn)
                        conx.Open()
                        Dim sqlx = "SELECT username from tbpengguna WHERE username ='" & txt_username.Text & "'"
                        Using cmdx As New MySqlCommand(sqlx, conx)
                            Using drx As MySqlDataReader = cmdx.ExecuteReader
                                drx.Read()
                                If drx.HasRows Then
                                    MsgBox("USERNAME Sudah Ada")
                                    txt_username.Focus()
                                Else
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "INSERT INTO tbpengguna (Username,Password,Nama,Hak_Akses,Tambah1,Tambah2,Tambah3) VALUES (@1,@2,@3,@4,@5,@6,@7)"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            With cmdy
                                                .Parameters.Clear()
                                                .Parameters.AddWithValue("@1", (txt_username.Text))
                                                .Parameters.AddWithValue("@2", (txt_password.Text))
                                                .Parameters.AddWithValue("@3", (txt_nama.Text))
                                                .Parameters.AddWithValue("@4", (cb_hak_akses.Text))
                                                .Parameters.AddWithValue("@5", (""))
                                                .Parameters.AddWithValue("@6", (""))
                                                .Parameters.AddWithValue("@7", (0))
                                                .ExecuteNonQuery()
                                            End With
                                            MsgBox("Data Pengguna Baru Berhasil Disimpan")
                                            Call bersih_text()
                                            Call isidgv()
                                            txt_username.Focus()
                                        End Using
                                    End Using
                                End If
                            End Using
                        End Using
                    End Using
                End If
            Else
                Using cony As New MySqlConnection(sLocalConn)
                    cony.Open()
                    Dim sqly = "UPDATE tbpengguna SET Password=@1,Nama=@2,Hak_Akses=@3,Tambah1=@4,Tambah2=@5,Tambah3=@6 WHERE username='" & txt_username.Text & "'"
                    Using cmdy As New MySqlCommand(sqly, cony)
                        With cmdy
                            .Parameters.Clear()
                            .Parameters.AddWithValue("@1", (txt_password.Text))
                            .Parameters.AddWithValue("@2", (txt_nama.Text))
                            .Parameters.AddWithValue("@3", (cb_hak_akses.Text))
                            .Parameters.AddWithValue("@4", (""))
                            .Parameters.AddWithValue("@5", (""))
                            .Parameters.AddWithValue("@6", (0))
                            .ExecuteNonQuery()
                        End With
                        MsgBox("Data Pengguna Berhasil Diubah")
                        Call bersih_text()
                        Call isidgv()
                        txt_username.Focus()
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_hapus.Click
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT username from tbpengguna WHERE username ='" & txt_username.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using drx As MySqlDataReader = cmdx.ExecuteReader
                        drx.Read()
                        If drx.HasRows Then
                            If MsgBox("Yakin Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Data") = vbYes Then
                                Using cony As New MySqlConnection(sLocalConn)
                                    cony.Open()
                                    Dim sqly = "DELETE FROM tbpengguna WHERE username='" & txt_username.Text & "'"
                                    Using cmdy As New MySqlCommand(sqly, cony)
                                        cmdy.ExecuteNonQuery()
                                    End Using
                                End Using
                                MsgBox("Data Pengguna Berhasil Di HAPUS")
                                Call bersih_text()
                                Call isidgv()
                                txt_username.Focus()
                            ElseIf vbNo Then
                                txt_password.Focus()
                            End If
                        End If
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btn_batal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_batal.Click
        If txt_username.Text = "" And txt_password.Text = "" And txt_nama.Text = "" And cb_hak_akses.Text = "-- Pilih --" Then
            Me.Close()
        Else
            Call bersih_text()
            Call isidgv()
            txt_username.Focus()
        End If
    End Sub

    Private Sub cb_hak_akses_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cb_hak_akses.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
End Class