﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_stok_proses_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.txt_form = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btn_perbarui = New System.Windows.Forms.Button()
        Me.txt_cari_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_cari_gudang = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cb_satuan = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txt_total_stok = New System.Windows.Forms.TextBox()
        Me.txt_total_yard = New System.Windows.Forms.TextBox()
        Me.txt_total_meter = New System.Windows.Forms.TextBox()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv1
        '
        Me.dgv1.AllowUserToAddRows = False
        Me.dgv1.AllowUserToDeleteRows = False
        Me.dgv1.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Location = New System.Drawing.Point(10, 83)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.ReadOnly = True
        Me.dgv1.Size = New System.Drawing.Size(815, 400)
        Me.dgv1.TabIndex = 0
        '
        'txt_form
        '
        Me.txt_form.Location = New System.Drawing.Point(717, 45)
        Me.txt_form.Name = "txt_form"
        Me.txt_form.Size = New System.Drawing.Size(100, 20)
        Me.txt_form.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btn_perbarui)
        Me.Panel1.Controls.Add(Me.txt_cari_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_cari_gudang)
        Me.Panel1.Location = New System.Drawing.Point(10, 37)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(815, 47)
        Me.Panel1.TabIndex = 13
        '
        'btn_perbarui
        '
        Me.btn_perbarui.BackColor = System.Drawing.SystemColors.ControlLight
        Me.btn_perbarui.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_perbarui.Image = Global.HAOTEX.My.Resources.Resources.refresh
        Me.btn_perbarui.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_perbarui.Location = New System.Drawing.Point(717, 11)
        Me.btn_perbarui.Name = "btn_perbarui"
        Me.btn_perbarui.Size = New System.Drawing.Size(75, 23)
        Me.btn_perbarui.TabIndex = 10
        Me.btn_perbarui.Text = "Perbarui"
        Me.btn_perbarui.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_perbarui.UseVisualStyleBackColor = False
        '
        'txt_cari_jenis_kain
        '
        Me.txt_cari_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_jenis_kain.Location = New System.Drawing.Point(438, 12)
        Me.txt_cari_jenis_kain.MaxLength = 100
        Me.txt_cari_jenis_kain.Name = "txt_cari_jenis_kain"
        Me.txt_cari_jenis_kain.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_jenis_kain.TabIndex = 9
        Me.txt_cari_jenis_kain.Text = "< Jenis Kain >"
        Me.txt_cari_jenis_kain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_cari_gudang
        '
        Me.txt_cari_gudang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_cari_gudang.Location = New System.Drawing.Point(203, 12)
        Me.txt_cari_gudang.MaxLength = 100
        Me.txt_cari_gudang.Name = "txt_cari_gudang"
        Me.txt_cari_gudang.Size = New System.Drawing.Size(171, 20)
        Me.txt_cari_gudang.TabIndex = 8
        Me.txt_cari_gudang.Text = "< Gudang >"
        Me.txt_cari_gudang.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.Font = New System.Drawing.Font("Cambria", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Window
        Me.Label1.Location = New System.Drawing.Point(10, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(815, 26)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "STOK PROSES"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.cb_satuan)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Controls.Add(Me.Label9)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.txt_total_stok)
        Me.Panel3.Controls.Add(Me.txt_total_yard)
        Me.Panel3.Controls.Add(Me.txt_total_meter)
        Me.Panel3.Location = New System.Drawing.Point(10, 482)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(815, 60)
        Me.Panel3.TabIndex = 23
        '
        'cb_satuan
        '
        Me.cb_satuan.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan.FormattingEnabled = True
        Me.cb_satuan.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan.Location = New System.Drawing.Point(691, 26)
        Me.cb_satuan.Name = "cb_satuan"
        Me.cb_satuan.Size = New System.Drawing.Size(50, 21)
        Me.cb_satuan.TabIndex = 501
        Me.cb_satuan.Text = "Meter"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(574, 11)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(74, 13)
        Me.Label10.TabIndex = 21
        Me.Label10.Text = "TOTAL STOK"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(268, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(79, 13)
        Me.Label9.TabIndex = 20
        Me.Label9.Text = "TOTAL ( Yard )"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(105, 11)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(84, 13)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "TOTAL ( Meter )"
        '
        'txt_total_stok
        '
        Me.txt_total_stok.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_stok.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_stok.Location = New System.Drawing.Point(535, 27)
        Me.txt_total_stok.Name = "txt_total_stok"
        Me.txt_total_stok.ReadOnly = True
        Me.txt_total_stok.Size = New System.Drawing.Size(150, 20)
        Me.txt_total_stok.TabIndex = 17
        Me.txt_total_stok.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_total_yard
        '
        Me.txt_total_yard.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_yard.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_yard.Location = New System.Drawing.Point(238, 27)
        Me.txt_total_yard.Name = "txt_total_yard"
        Me.txt_total_yard.ReadOnly = True
        Me.txt_total_yard.Size = New System.Drawing.Size(150, 20)
        Me.txt_total_yard.TabIndex = 16
        Me.txt_total_yard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_total_meter
        '
        Me.txt_total_meter.BackColor = System.Drawing.SystemColors.Window
        Me.txt_total_meter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total_meter.Location = New System.Drawing.Point(72, 27)
        Me.txt_total_meter.Name = "txt_total_meter"
        Me.txt_total_meter.ReadOnly = True
        Me.txt_total_meter.Size = New System.Drawing.Size(150, 20)
        Me.txt_total_meter.TabIndex = 15
        Me.txt_total_meter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'form_stok_proses_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 552)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txt_form)
        Me.Controls.Add(Me.dgv1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_stok_proses_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv1 As System.Windows.Forms.DataGridView
    Friend WithEvents txt_form As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txt_cari_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_cari_gudang As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents cb_satuan As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txt_total_stok As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_yard As System.Windows.Forms.TextBox
    Friend WithEvents txt_total_meter As System.Windows.Forms.TextBox
    Friend WithEvents btn_perbarui As System.Windows.Forms.Button
End Class
