﻿Imports MySql.Data.MySqlClient

Public Class form_kontrak_grey

    Private Sub form_kontrak_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If form_menu_utama.Status2.Text = "User" Then
            ts_ubah.Visible = False
            ts_hapus.Visible = False
        ElseIf form_menu_utama.Status2.Text = "Admin" Then
            ts_ubah.Visible = True
            ts_hapus.Visible = True
        End If
        Call awal()
    End Sub
    Private Sub awal()
        dtp_hari_ini.Text = Today
        dtp_akhir.Text = Today
        Call isidtpawal()
        Call isidgv()
        Call hitungjumlah()
    End Sub
    Private Sub isidtpawal()
        If form_menu_utama.Status2.Text = "User" Then
            Dim tanggal As DateTime
            tanggal = Today
            tanggal = tanggal.AddMonths(-1)
            dtp_awal.Text = tanggal
        ElseIf form_menu_utama.Status2.Text = "Admin" Then
            Dim tanggal As DateTime
            tanggal = Today
            tanggal = tanggal.AddMonths(-3)
            dtp_awal.Text = tanggal
        End If
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "ID Grey"
        dgv1.Columns(1).HeaderText = "Tanggal"
        dgv1.Columns(2).HeaderText = "No. Kontrak"
        dgv1.Columns(3).HeaderText = "Jenis Kain"
        dgv1.Columns(4).HeaderText = "Supplier"
        dgv1.Columns(5).HeaderText = "Harga"
        dgv1.Columns(6).HeaderText = "Total"
        dgv1.Columns(7).HeaderText = "Dikirim"
        dgv1.Columns(8).HeaderText = "Sisa"
        dgv1.Columns(9).HeaderText = "Satuan"
        dgv1.Columns(10).HeaderText = "Keterangan"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 80
        dgv1.Columns(2).Width = 90
        dgv1.Columns(3).Width = 130
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 100
        dgv1.Columns(6).Width = 90
        dgv1.Columns(7).Width = 90
        dgv1.Columns(8).Width = 90
        dgv1.Columns(9).Width = 80
        dgv1.Columns(10).Width = 130
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Format = "C"
        dgv1.Columns(6).DefaultCellStyle.Format = "N"
        dgv1.Columns(7).DefaultCellStyle.Format = "N"
        dgv1.Columns(8).DefaultCellStyle.Format = "N"
    End Sub
    Private Sub dgv1_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgv1.CellFormatting
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(8).Value = 0 Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Red
            ElseIf Not dgv1.Rows(i).Cells(7).Value = 0 Then
                dgv1.Rows(i).DefaultCellStyle.ForeColor = Color.Blue
            End If
        Next
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Id_Grey DESC"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbkontrakgrey")
                            dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        Call awal()
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_supplier.Text = "< Supplier >"
        dgv1.Focus()
    End Sub
    Private Sub txt_cari_supplier_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_supplier.GotFocus
        If txt_cari_supplier.Text = "< Supplier >" Then
            txt_cari_supplier.Text = ""
        End If
    End Sub
    Private Sub txt_cari_supplier_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_supplier.LostFocus
        If txt_cari_supplier.Text = "" Then
            txt_cari_supplier.Text = "< Supplier >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub dtp_awal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_awal.TextChanged
        If form_menu_utama.Status2.Text = "User" Then
            Dim tanggal As DateTime
            tanggal = Today
            tanggal = tanggal.AddMonths(-1)
            If dtp_awal.Value < tanggal Then
                dtp_awal.Text = tanggal
            ElseIf dtp_awal.Value > dtp_akhir.Value Then
                dtp_awal.Text = dtp_akhir.Text
            End If
        ElseIf form_menu_utama.Status2.Text = "Admin" Then
            If dtp_awal.Value > dtp_akhir.Value Then
                dtp_awal.Text = dtp_akhir.Text
            End If
        End If
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_supplier.Text = "< Supplier >"
        Call isidgv()
        Call hitungjumlah()
    End Sub
    Private Sub dtp_akhir_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtp_akhir.TextChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_supplier.Text = "< Supplier >"
        Call isidgv()
        Call hitungjumlah()
    End Sub

    Private Sub isidgv_supplier()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Id_Grey DESC"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkontrakgrey")
                        dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_jeniskain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Id_Grey DESC"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkontrakgrey")
                        dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_supplier_jeniskain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Id_Grey DESC"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkontrakgrey")
                        dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub txt_cari_supplier_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_supplier.TextChanged
        Try
            If txt_cari_supplier.Text = "< Supplier >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call hitungjumlah()
            ElseIf txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
                Call hitungjumlah()
            ElseIf txt_cari_supplier.Text = "< Supplier >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
                Call hitungjumlah()
            ElseIf txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
                Call hitungjumlah()
            ElseIf Not txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_supplier()
                Call hitungjumlah()
            ElseIf Not txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_supplier_jeniskain()
                Call hitungjumlah()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_supplier.Text = "< Supplier >" Then
                Call hitungjumlah()
            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_supplier.Text = "< Supplier >" Then
                Call isidgv()
                Call hitungjumlah()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_supplier.Text = "" Then
                Call isidgv_supplier()
                Call hitungjumlah()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_supplier.Text = "" Then
                Call isidgv_supplier()
                Call hitungjumlah()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_supplier.Text = "< Supplier >" Then
                Call isidgv_jeniskain()
                Call hitungjumlah()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_supplier.Text = "" Then
                Call isidgv_supplier_jeniskain()
                Call hitungjumlah()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        Try
            form_input_kontrak_grey.MdiParent = form_menu_utama
            form_input_kontrak_grey.Show()
            form_input_kontrak_grey.Label1.Text = "Input Kontrak Baru"
            form_input_kontrak_grey.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Diubah")
        Else
            Try
                form_input_kontrak_grey.MdiParent = form_menu_utama
                form_input_kontrak_grey.Show()
                form_input_kontrak_grey.Label1.Text = "Ubah Kontrak"
                form_input_kontrak_grey.btn_simpan_baru.Visible = False
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_kontrak_grey.txt_id_grey.Text = .Cells(0).Value.ToString
                    form_input_kontrak_grey.dtp_tanggal.Text = .Cells(1).Value.ToString
                    form_input_kontrak_grey.txt_no_kontrak.Text = .Cells(2).Value.ToString
                    form_input_kontrak_grey.txt_jenis_kain.Text = .Cells(3).Value.ToString
                    form_input_kontrak_grey.txt_supplier.Text = .Cells(4).Value.ToString
                    form_input_kontrak_grey.txt_harga.Text = .Cells(5).Value.ToString
                    form_input_kontrak_grey.txt_total.Text = .Cells(6).Value.ToString
                    form_input_kontrak_grey.txt_dikirim.Text = .Cells(7).Value.ToString
                    form_input_kontrak_grey.cb_satuan.Text = .Cells(9).Value.ToString
                    form_input_kontrak_grey.txt_keterangan.Text = .Cells(10).Value.ToString
                End With
                If Not Val(form_input_kontrak_grey.txt_dikirim.Text) = 0 Then
                    form_input_kontrak_grey.txt_total.Focus()
                    form_input_kontrak_grey.txt_no_kontrak.Enabled = False
                    form_input_kontrak_grey.txt_jenis_kain.Enabled = False
                    form_input_kontrak_grey.txt_supplier.Enabled = False
                    form_input_kontrak_grey.cb_satuan.Enabled = False
                End If
                form_input_kontrak_grey.Focus()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dihapus")
        ElseIf dgv1.CurrentRow.Cells(8).Value.ToString = "0" Then
            MsgBox("GREY Sudah Dikirim Semua / KONTRAK sudah Selesai Tidak Bisa DIHAPUS")
        ElseIf Not dgv1.CurrentRow.Cells(7).Value.ToString = "0" Then
            MsgBox("KONTRAK Sudah Ada Transaksi Pembelian Tidak Bisa DIHAPUS")
        Else
            Try
                form_input_kontrak_grey.MdiParent = form_menu_utama
                form_input_kontrak_grey.Show()
                form_input_kontrak_grey.Label1.Text = "Hapus Kontrak"
                form_input_kontrak_grey.txt_no_kontrak.ReadOnly = True
                form_input_kontrak_grey.txt_jenis_kain.ReadOnly = True
                form_input_kontrak_grey.txt_jenis_kain.Enabled = False
                form_input_kontrak_grey.txt_supplier.ReadOnly = True
                form_input_kontrak_grey.txt_supplier.Enabled = False
                form_input_kontrak_grey.txt_harga.ReadOnly = True
                form_input_kontrak_grey.txt_total.ReadOnly = True
                form_input_kontrak_grey.txt_keterangan.ReadOnly = True
                form_input_kontrak_grey.btn_batal.Visible = False
                form_input_kontrak_grey.btn_simpan_baru.Visible = False
                form_input_kontrak_grey.btn_simpan_tutup.Visible = False
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_kontrak_grey.txt_id_grey.Text = .Cells(0).Value.ToString
                    form_input_kontrak_grey.dtp_tanggal.Text = .Cells(1).Value.ToString
                    form_input_kontrak_grey.txt_no_kontrak.Text = .Cells(2).Value.ToString
                    form_input_kontrak_grey.txt_jenis_kain.Text = .Cells(3).Value.ToString
                    form_input_kontrak_grey.txt_supplier.Text = .Cells(4).Value.ToString
                    form_input_kontrak_grey.txt_harga.Text = .Cells(5).Value.ToString
                    form_input_kontrak_grey.txt_total.Text = .Cells(6).Value.ToString
                    form_input_kontrak_grey.cb_satuan.Text = .Cells(9).Value.ToString
                    form_input_kontrak_grey.txt_keterangan.Text = .Cells(10).Value.ToString
                End With
                form_input_kontrak_grey.Focus()

                If MsgBox("Yakin KONTRAK Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Kontrak") = vbYes Then
                    Using cony As New MySqlConnection(sLocalConn)
                        cony.Open()
                        Dim sqly = "DELETE FROM tbkontrakgrey WHERE Id_Grey ='" & form_input_kontrak_grey.txt_id_grey.Text & "'"
                        Using cmdy As New MySqlCommand(sqly, cony)
                            cmdy.ExecuteNonQuery()
                        End Using
                    End Using
                    ts_perbarui.PerformClick()
                    form_input_kontrak_grey.Close()
                    MsgBox("KONTRAK berhasil di HAPUS")
                Else
                    form_input_kontrak_grey.Close()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub reportkontrekgrey()
        Dim tbheaderfooter As New DataTable
        With tbheaderfooter
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
        End With
        tbheaderfooter.Rows.Add(dtp_awal.Text, dtp_akhir.Text, dtp_hari_ini.Text, _
                                txt_jumlah_total.Text, txt_jumlah_dikirim.Text, txt_jumlah_sisa.Text,
                                txt_total_yard.Text, txt_dikirim_yard.Text, txt_sisa_yard.Text)

        Dim dtreportkontrakgrey As New DataTable
        With dtreportkontrakgrey
            .Columns.Add("DataColumn1")
            .Columns.Add("DataColumn2")
            .Columns.Add("DataColumn3")
            .Columns.Add("DataColumn4")
            .Columns.Add("DataColumn5")
            .Columns.Add("DataColumn6")
            .Columns.Add("DataColumn7")
            .Columns.Add("DataColumn8")
            .Columns.Add("DataColumn9")
            .Columns.Add("DataColumn10")
            .Columns.Add("DataColumn11")
        End With
        For Each row As DataGridViewRow In dgv1.Rows
            If Not row.Cells(6).Value = 0 Then
                dtreportkontrakgrey.Rows.Add(row.Cells(0).Value, row.Cells(1).FormattedValue, row.Cells(2).Value, _
                                         row.Cells(3).Value, row.Cells(4).Value, row.Cells(5).FormattedValue, _
                                         row.Cells(6).FormattedValue, row.Cells(7).FormattedValue, row.Cells(8).FormattedValue, _
                                         row.Cells(9).Value, row.Cells(10).Value)
            End If
        Next

        form_report_kontrak_grey.ReportViewer1.LocalReport.DataSources.Item(0).Value = tbheaderfooter
        form_report_kontrak_grey.ReportViewer1.LocalReport.DataSources.Item(1).Value = dtreportkontrakgrey
        form_report_kontrak_grey.ShowDialog()
        form_report_kontrak_grey.Dispose()
    End Sub
    Private Sub ts_print_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_print.Click
        If dgv1.RowCount = 0 Then
            MsgBox("Tidak Terdapat Data Untuk Dicetak")
        Else
            Call reportkontrekgrey()
        End If
    End Sub

    Private Sub hitungjumlah()
        Dim jumlahtotal, jumlahdikirim, jumlahsisa As Double
        Dim totalyard, yarddikirim, yardsisa As Double
        jumlahtotal = 0
        jumlahdikirim = 0
        jumlahsisa = 0
        totalyard = 0
        yarddikirim = 0
        yardsisa = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(9).Value = "Meter" Then
                jumlahtotal = jumlahtotal + Val(dgv1.Rows(i).Cells(6).Value)
                jumlahdikirim = jumlahdikirim + Val(dgv1.Rows(i).Cells(7).Value)
                jumlahsisa = jumlahsisa + Val(dgv1.Rows(i).Cells(8).Value)
            ElseIf dgv1.Rows(i).Cells(9).Value = "Yard" Then
                totalyard = totalyard + Val(dgv1.Rows(i).Cells(6).Value)
                yarddikirim = yarddikirim + Val(dgv1.Rows(i).Cells(7).Value)
                yardsisa = yardsisa + Val(dgv1.Rows(i).Cells(8).Value)
            End If
        Next
        txt_jumlah_total.Text = jumlahtotal
        txt_jumlah_dikirim.Text = jumlahdikirim
        txt_jumlah_sisa.Text = jumlahsisa
        txt_total_yard.Text = totalyard
        txt_dikirim_yard.Text = yarddikirim
        txt_sisa_yard.Text = yardsisa
    End Sub
    Private Sub txt_jumlah_total_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txt_jumlah_total.KeyPress, txt_jumlah_dikirim.KeyPress, txt_jumlah_sisa.KeyPress
        If Not (e.KeyChar = Chr(13)) Then e.Handled = True
    End Sub
    Private Sub txt_jumlah_total_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_total.TextChanged
        If txt_jumlah_total.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_total.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_total.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_total.Select(txt_jumlah_total.Text.Length, 0)
            ElseIf txt_jumlah_total.Text = "-"c Then

            Else
                txt_jumlah_total.Text = CDec(temp).ToString("N0")
                txt_jumlah_total.Select(txt_jumlah_total.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_jumlah_dikirim_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_dikirim.TextChanged
        If txt_jumlah_dikirim.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_dikirim.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_dikirim.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_dikirim.Select(txt_jumlah_dikirim.Text.Length, 0)
            ElseIf txt_jumlah_dikirim.Text = "-"c Then

            Else
                txt_jumlah_dikirim.Text = CDec(temp).ToString("N0")
                txt_jumlah_dikirim.Select(txt_jumlah_dikirim.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_jumlah_sisa_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_jumlah_sisa.TextChanged
        If txt_jumlah_sisa.Text <> String.Empty Then
            Dim temp As String = txt_jumlah_sisa.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_jumlah_sisa.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_jumlah_sisa.Select(txt_jumlah_sisa.Text.Length, 0)
            ElseIf txt_jumlah_sisa.Text = "-"c Then

            Else
                txt_jumlah_sisa.Text = CDec(temp).ToString("N0")
                txt_jumlah_sisa.Select(txt_jumlah_sisa.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_sisa_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_sisa_yard.TextChanged
        If txt_sisa_yard.Text <> String.Empty Then
            Dim temp As String = txt_sisa_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_sisa_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_sisa_yard.Select(txt_sisa_yard.Text.Length, 0)
            ElseIf txt_sisa_yard.Text = "-"c Then

            Else
                txt_sisa_yard.Text = CDec(temp).ToString("N0")
                txt_sisa_yard.Select(txt_sisa_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_dikirim_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_dikirim_yard.TextChanged
        If txt_dikirim_yard.Text <> String.Empty Then
            Dim temp As String = txt_dikirim_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_dikirim_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_dikirim_yard.Select(txt_dikirim_yard.Text.Length, 0)
            ElseIf txt_dikirim_yard.Text = "-"c Then

            Else
                txt_dikirim_yard.Text = CDec(temp).ToString("N0")
                txt_dikirim_yard.Select(txt_dikirim_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_yard.TextChanged
        If txt_total_yard.Text <> String.Empty Then
            Dim temp As String = txt_total_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            ElseIf txt_total_yard.Text = "-"c Then

            Else
                txt_total_yard.Text = CDec(temp).ToString("N0")
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            End If
        End If
    End Sub
End Class