﻿Imports MySql.Data.MySqlClient

Public Class form_input_warna_resep

    Private Sub form_input_warna_resep_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If TxtForm.Text = "form_input_po_proses_baris_1" Or _
            TxtForm.Text = "form_input_po_proses_baris_2" Or _
            TxtForm.Text = "form_input_po_proses_baris_3" Or _
            TxtForm.Text = "form_input_po_proses_baris_4" Or _
            TxtForm.Text = "form_input_po_proses_baris_5" Or _
            TxtForm.Text = "form_input_po_proses_baris_6" Or _
            TxtForm.Text = "form_input_po_proses_baris_7" Or _
            TxtForm.Text = "form_input_po_proses_baris_8" Or _
            TxtForm.Text = "form_input_po_proses_baris_9" Or _
            TxtForm.Text = "form_input_po_proses_baris_10" Then
            form_input_po_proses.Focus()
            form_input_po_proses.Label1.Focus()
        ElseIf TxtForm.Text = "form_edit_po_proses" Then
            form_edit_po_proses.Focus()
            form_edit_po_proses.Label1.Focus()
        End If
    End Sub
    Private Sub form_input_warna_resep_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertable()
        Dgv1.Columns(0).HeaderText = "Jenis Kain"
        Dgv1.Columns(1).HeaderText = "Warna"
        Dgv1.Columns(2).HeaderText = "Resep"
        Dgv1.Columns(0).Width = 130
        Dgv1.Columns(1).Width = 100
        Dgv1.Columns(2).Width = 100
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_J_W_R()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Warna like '%" & txt_cari_warna.Text & "%' AND Resep like '%" & txt_cari_resep.Text & "%' ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_J()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_W()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Warna like '%" & txt_cari_warna.Text & "%' ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_R()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Resep like '%" & txt_cari_resep.Text & "%' ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_J_W()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Warna like '%" & txt_cari_warna.Text & "%' ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_J_R()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Resep like '%" & txt_cari_resep.Text & "%' ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub cari_W_R()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Warna like '%" & txt_cari_warna.Text & "%' AND Resep like '%" & txt_cari_resep.Text & "%' ORDER BY Jenis_Kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbwarnaresep")
                            Dgv1.DataSource = dsx.Tables("tbwarnaresep")
                            Call headertable()
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged, _
        txt_cari_warna.TextChanged, txt_cari_resep.TextChanged
        If txt_cari_jenis_kain.Text = "" And txt_cari_warna.Text = "" And txt_cari_resep.Text = "" Then
            Call isidgv()
        ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_warna.Text = "" And Not txt_cari_resep.Text = "" Then
            Call cari_J_W_R()
        ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_warna.Text = "" And txt_cari_resep.Text = "" Then
            Call cari_J()
        ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_warna.Text = "" And txt_cari_resep.Text = "" Then
            Call cari_W()
        ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_warna.Text = "" And Not txt_cari_resep.Text = "" Then
            Call cari_R()
        ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_warna.Text = "" And txt_cari_resep.Text = "" Then
            Call cari_J_W()
        ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_warna.Text = "" And Not txt_cari_resep.Text = "" Then
            Call cari_J_R()
        ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_warna.Text = "" And Not txt_cari_resep.Text = "" Then
            Call cari_W_R()
        End If
    End Sub

    Private Sub Dgv1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Dgv1.MouseClick
        Dim i As Integer
        Dim jenis_kain, warna, resep As String
        i = Me.Dgv1.CurrentRow.Index
        With Dgv1.Rows.Item(i)
            jenis_kain = .Cells(0).Value.ToString
            warna = .Cells(1).Value.ToString
            resep = .Cells(2).Value.ToString
        End With
        txt_cari_warna.Text = warna
        txt_cari_resep.Text = resep
        txt_cari_jenis_kain.Text = jenis_kain
    End Sub
    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        Try
            If txt_cari_jenis_kain.Text = "" Then
                MsgBox("JENIS KAIN Belum Dipilih")
            ElseIf txt_cari_warna.Text = "" Then
                MsgBox("WARNA Belum dipilih")
                txt_cari_warna.Focus()
            ElseIf txt_cari_resep.Text = "" Then
                MsgBox("RESEP Belum dipilih")
                txt_cari_resep.Focus()
            Else
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Jenis_Kain = '" & txt_cari_jenis_kain.Text & "' AND Warna ='" & txt_cari_warna.Text & "' AND Resep ='" & txt_cari_resep.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("WARNA dan RESEP belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama_warna.MdiParent = form_menu_utama
                                    form_input_nama_warna.Show()
                                    form_input_nama_warna.Focus()
                                    form_input_nama_warna.txt_frm.Text = "form_input_warna_resep"
                                    form_input_nama_warna.Label1.Text = "Input Warna dan Resep Baru"
                                    form_input_nama_warna.txt_jenis_kain.Text = txt_cari_jenis_kain.Text
                                    form_input_nama_warna.txt_nama_warna.Text = txt_cari_warna.Text
                                    form_input_nama_warna.txt_nama_resep.Text = txt_cari_resep.Text
                                End If
                            Else
                                If TxtForm.Text = "form_input_po_proses_baris_1" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_1.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_1.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_1.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_2" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_2.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_2.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_2.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_3" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_3.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_3.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_3.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_4" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_4.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_4.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_4.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_5" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_5.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_5.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_5.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_6" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_6.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_6.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_6.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_7" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_7.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_7.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_7.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_8" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_8.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_8.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_8.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_9" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_9.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_9.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_9.Focus()
                                ElseIf TxtForm.Text = "form_input_po_proses_baris_10" Then
                                    form_input_po_proses.MdiParent = form_menu_utama
                                    form_input_po_proses.Show()
                                    form_input_po_proses.Focus()
                                    form_input_po_proses.txt_warna_10.Text = txt_cari_warna.Text
                                    form_input_po_proses.txt_resep_10.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_input_po_proses.txt_tarik_lebar_10.Focus()
                                ElseIf TxtForm.Text = "form_edit_po_proses" Then
                                    form_edit_po_proses.MdiParent = form_menu_utama
                                    form_edit_po_proses.Show()
                                    form_edit_po_proses.Focus()
                                    form_edit_po_proses.txt_warna.Text = txt_cari_warna.Text
                                    form_edit_po_proses.txt_resep.Text = txt_cari_resep.Text
                                    Me.Close()
                                    form_edit_po_proses.Label1.Focus()
                                End If
                            End If
                        End Using
                    End Using
                End Using
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_perbarui.Click
        Try
            Call isidgv()
            txt_cari_jenis_kain.Text = ""
            txt_cari_warna.Text = ""
            txt_cari_resep.Text = ""
            BtnOk.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub ts_keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_keluar.Click
        Me.Close()
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_nama_warna.MdiParent = form_menu_utama
        form_input_nama_warna.Show()
        form_input_nama_warna.Focus()
        form_input_nama_warna.txt_frm.Text = "form_input_warna_resep"
        form_input_nama_warna.Label1.Text = "Input Warna dan Resep Baru"
        form_input_nama_warna.txt_nama_warna.Focus()
    End Sub
    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        If txt_cari_jenis_kain.Text = "" Then
            MsgBox("JENIS KAIN Belum Dipilih")
        ElseIf txt_cari_warna.Text = "" Then
            MsgBox("WARNA Belum Dipilih")
            txt_cari_warna.Focus()
        ElseIf txt_cari_resep.Text = "" Then
            MsgBox("RESEP Belum Dipilih")
            txt_cari_resep.Focus()
        Else
            Try
                BtnOk.Focus()
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Jenis_Kain = '" & txt_cari_jenis_kain.Text & "' AND Warna ='" & txt_cari_warna.Text & "' AND Resep ='" & txt_cari_resep.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("WARNA dan RESEP belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama_warna.MdiParent = form_menu_utama
                                    form_input_nama_warna.Show()
                                    form_input_nama_warna.Focus()
                                    form_input_nama_warna.txt_frm.Text = "form_input_warna_resep"
                                    form_input_nama_warna.Label1.Text = "Input Warna dan Resep Baru"
                                    form_input_nama_warna.txt_jenis_kain.Text = txt_cari_jenis_kain.Text
                                    form_input_nama_warna.txt_nama_warna.Text = txt_cari_warna.Text
                                    form_input_nama_warna.txt_nama_resep.Text = txt_cari_resep.Text
                                End If
                            Else
                                form_input_nama_warna.MdiParent = form_menu_utama
                                form_input_nama_warna.Show()
                                form_input_nama_warna.Focus()
                                form_input_nama_warna.btn_simpan_baru.Visible = False
                                form_input_nama_warna.txt_frm.Text = "form_input_warna_resep"
                                form_input_nama_warna.Label1.Text = "Ubah Warna dan Resep"
                                form_input_nama_warna.txt_jenis_kain.Text = txt_cari_jenis_kain.Text
                                form_input_nama_warna.txt_nama_warna.Text = txt_cari_warna.Text
                                form_input_nama_warna.txt_nama_resep.Text = txt_cari_resep.Text
                                form_input_nama_warna.txt_nama_warna.Focus()
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        If txt_cari_jenis_kain.Text = "" Then
            MsgBox("JENIS KAIN Belum Dipilih")
        ElseIf txt_cari_warna.Text = "" Then
            MsgBox("WARNA Belum Dipilih")
            txt_cari_warna.Focus()
        ElseIf txt_cari_resep.Text = "" Then
            MsgBox("RESEP Belum Dipilih")
            txt_cari_resep.Focus()
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Jenis_Kain,Warna,Resep FROM tbwarnaresep WHERE Jenis_Kain='" & txt_cari_jenis_kain.Text & "' AND Warna ='" & txt_cari_warna.Text & "' AND Resep ='" & txt_cari_resep.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                If MsgBox("Yakin WARNA dan RESEP Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Data") = vbYes Then
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "DELETE FROM tbwarnaresep WHERE Jenis_Kain='" & txt_cari_jenis_kain.Text & "' AND Warna='" & txt_cari_warna.Text & "' AND Resep='" & txt_cari_resep.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            cmdy.ExecuteNonQuery()
                                        End Using
                                        ts_perbarui.PerformClick()
                                        MsgBox("WARNA dan RESEP berhasil di Hapus")
                                    End Using
                                End If
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class