﻿Imports MySql.Data.MySqlClient

Public Class form_input_jenis_kain

    Private Sub form_input_jenis_kain_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If TxtForm.Text = "form_input_pembelian_grey_baru_1" Or _
           TxtForm.Text = "form_input_pembelian_grey_baru_2" Or _
           TxtForm.Text = "form_input_pembelian_grey_baru_3" Or _
           TxtForm.Text = "form_input_pembelian_grey_baru_4" Or _
           TxtForm.Text = "form_input_pembelian_grey_baru_5" Or _
           TxtForm.Text = "form_input_pembelian_grey_baru_6" Or _
           TxtForm.Text = "form_input_pembelian_grey_baru_7" Or _
           TxtForm.Text = "form_input_pembelian_grey_baru_8" Then
            form_input_pembelian_grey_baru.Focus()
            form_input_pembelian_grey_baru.Label1.Focus()
        ElseIf TxtForm.Text = "form_input_kontrak_grey" Then
            form_input_kontrak_grey.Focus()
            form_input_kontrak_grey.Label1.Focus()
        ElseIf TxtForm.Text = "form_input_nama_warna" Then
            form_input_nama_warna.Focus()
            form_input_nama_warna.Label1.Focus()
        End If
    End Sub
    Private Sub form_input_jenis_kain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
        TxtCari.Text = ""
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Nama_Grey FROM tbjenisgrey ORDER BY Nama_Grey"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbjenisgrey")
                            Dgv1.DataSource = dsx.Tables("tbjenisgrey")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub carijenisgrey()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Nama_Grey FROM tbjenisgrey WHERE Nama_Grey like '%" & TxtCari.Text & "%' ORDER BY Nama_Grey"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbjenisgrey")
                            Dgv1.DataSource = dsx.Tables("tbjenisgrey")
                            Dgv1.Columns(0).Width = 200
                        End Using
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub TxtCari_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TxtCari.TextChanged
        Call carijenisgrey()
    End Sub
    Private Sub Dgv1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Dgv1.MouseClick
        Dim i As Integer
        i = Me.Dgv1.CurrentRow.Index
        With Dgv1.Rows.Item(i)
            TxtCari.Text = .Cells(0).Value.ToString
        End With
    End Sub
    Private Sub BtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnOk.Click
        If TxtCari.Text = "" Then
            MsgBox("JENIS KAIN belum diinput")
        Else
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx = "SELECT Nama_Grey from tbjenisgrey WHERE Nama_Grey ='" & TxtCari.Text & "'"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using drx As MySqlDataReader = cmdx.ExecuteReader
                        drx.Read()
                        If Not drx.HasRows Then
                            If MsgBox("JENIS KAIN belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                form_input_nama.MdiParent = form_menu_utama
                                form_input_nama.Show()
                                form_input_nama.txt_frm.Text = "form_input_jenis_kain"
                                form_input_nama.Label1.Text = "Input Jenis Kain Baru"
                                form_input_nama.Label2.Text = "Jenis Kain"
                                form_input_nama.txt_nama.Text = TxtCari.Text
                                form_input_nama.txt_nama.Focus()
                                form_input_nama.Focus()
                            End If
                        Else
                            If TxtForm.Text = "form_input_kontrak_grey" Then
                                form_input_kontrak_grey.MdiParent = form_menu_utama
                                form_input_kontrak_grey.Show()
                                form_input_kontrak_grey.txt_jenis_kain.Text = TxtCari.Text
                                If form_input_kontrak_grey.Label1.Text = "Ubah Kontrak" Then
                                    form_input_kontrak_grey.txt_harga.Focus()
                                Else
                                    form_input_kontrak_grey.txt_supplier.Focus()
                                End If
                                form_input_kontrak_grey.Focus()
                                Me.Close()
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_1" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_1.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_1.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_1.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_1.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_2" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_2.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_2.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_2.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_2.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_3" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_3.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_3.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_3.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_3.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_4" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_4.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_4.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_4.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_4.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_5" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_5.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_5.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_5.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_5.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_6" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_6.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_6.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_6.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_6.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_7" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_7.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_7.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_7.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_7.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_8" Then
                                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                                form_input_pembelian_grey_baru.Show()
                                form_input_pembelian_grey_baru.Focus()
                                form_input_pembelian_grey_baru.txt_jenis_kain_8.Text = TxtCari.Text
                                Me.Close()
                                If form_input_pembelian_grey_baru.txt_supplier_8.Text = "" Then
                                    form_input_pembelian_grey_baru.txt_supplier_8.Focus()
                                Else
                                    form_input_pembelian_grey_baru.txt_harga_8.Focus()
                                End If
                            ElseIf TxtForm.Text = "form_input_nama_warna" Then
                                form_input_nama_warna.MdiParent = form_menu_utama
                                form_input_nama_warna.Show()
                                form_input_nama_warna.txt_nama_warna.Focus()
                                form_input_nama_warna.txt_jenis_kain.Text = TxtCari.Text
                                Me.Close()
                            End If
                        End If
                    End Using
                End Using
            End Using
        End If
    End Sub
    Private Sub ts_keluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_keluar.Click
        If TxtForm.Text = "form_input_kontrak_grey" Then
            form_input_kontrak_grey.txt_no_kontrak.Focus()
            form_input_kontrak_grey.Focus()
            Me.Close()
        ElseIf TxtForm.Text = "form_input_pembelian_grey" Then
            form_input_pembelian_grey.txt_no_kontrak.Focus()
            form_input_pembelian_grey.Focus()
            Me.Close()
        Else
            Me.Close()
        End If
    End Sub
    Private Sub ts_baru_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_baru.Click
        form_input_nama.MdiParent = form_menu_utama
        form_input_nama.Show()
        form_input_nama.txt_frm.Text = "form_input_jenis_kain"
        form_input_nama.Label1.Text = "Input Jenis Kain Baru"
        form_input_nama.Label2.Text = "Jenis Kain"
        form_input_nama.Focus()
    End Sub
    Private Sub btn_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_perbarui.Click
        Call isidgv()
    End Sub
    Private Sub ts_ubah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_ubah.Click
        If TxtCari.Text = "" Then
            MsgBox("JENIS KAIN yang akan dirubah belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Grey from tbjenisgrey WHERE Nama_Grey ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If Not drx.HasRows Then
                                If MsgBox("JENIS KAIN belum terdapat di Database, Buat Baru ?", vbYesNo + vbQuestion, "Buat Baru") = vbYes Then
                                    form_input_nama.MdiParent = form_menu_utama
                                    form_input_nama.Show()
                                    form_input_nama.txt_frm.Text = "form_input_jenis_kain"
                                    form_input_nama.Label1.Text = "Input Jenis Kain Baru"
                                    form_input_nama.Label2.Text = "Jenis Kain"
                                    form_input_nama.txt_nama.Text = TxtCari.Text
                                    form_input_nama.txt_nama.Focus()
                                    form_input_nama.Focus()
                                End If
                            Else
                                form_input_nama.MdiParent = form_menu_utama
                                form_input_nama.Show()
                                form_input_nama.btn_simpan_baru.Visible = False
                                form_input_nama.txt_frm.Text = "form_input_jenis_kain"
                                form_input_nama.Label1.Text = "Ubah Jenis Kain"
                                form_input_nama.Label2.Text = "Jenis Kain"
                                form_input_nama.txt_nama.Text = TxtCari.Text
                                form_input_nama.txt_nama.Focus()
                                form_input_nama.Focus()
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
    Private Sub ts_hapus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ts_hapus.Click
        If TxtCari.Text = "" Then
            MsgBox("JENIS KAIN yang akan dihapus belum dipilih")
        Else
            Try
                Using conx As New MySqlConnection(sLocalConn)
                    conx.Open()
                    Dim sqlx = "SELECT Nama_Grey FROM tbjenisgrey WHERE Nama_Grey ='" & TxtCari.Text & "'"
                    Using cmdx As New MySqlCommand(sqlx, conx)
                        Using drx As MySqlDataReader = cmdx.ExecuteReader
                            drx.Read()
                            If drx.HasRows Then
                                If MsgBox("Yakin JENIS KAIN Akan Dihapus ?", vbYesNo + vbQuestion, "Hapus Data") = vbYes Then
                                    Using cony As New MySqlConnection(sLocalConn)
                                        cony.Open()
                                        Dim sqly = "DELETE FROM tbjenisgrey WHERE Nama_Grey='" & TxtCari.Text & "'"
                                        Using cmdy As New MySqlCommand(sqly, cony)
                                            cmdy.ExecuteNonQuery()
                                        End Using
                                        TxtCari.Text = ""
                                        MsgBox("JENIS KAIN berhasil di Hapus")
                                    End Using
                                End If
                            Else
                                MsgBox("JENIS KAIN Belum Tersimpan Di Database")
                            End If
                        End Using
                    End Using
                End Using
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class