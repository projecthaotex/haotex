﻿Imports MySql.Data.MySqlClient

Public Class form_stok_proses_grey

    Private Sub form_stok_proses_grey_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If txt_form.Text = "form_input_penjualan_grey" Then
            form_input_penjualan_grey.Focus()
            form_input_penjualan_grey.txt_qty.Focus()
        ElseIf txt_form.Text = "form_input_po_proses_baris_1" Or _
            txt_form.Text = "form_input_po_proses_baris_2" Or _
            txt_form.Text = "form_input_po_proses_baris_3" Or _
            txt_form.Text = "form_input_po_proses_baris_4" Or _
            txt_form.Text = "form_input_po_proses_baris_5" Or _
            txt_form.Text = "form_input_po_proses_baris_6" Or _
            txt_form.Text = "form_input_po_proses_baris_7" Or _
            txt_form.Text = "form_input_po_proses_baris_8" Or _
            txt_form.Text = "form_input_po_proses_baris_9" Or _
            txt_form.Text = "form_input_po_proses_baris_10" Then
            form_input_po_proses.Focus()
            form_input_po_proses.Label1.Focus()
        End If
    End Sub
    Private Sub form_stok_proses_grey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call isidgv()
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "Tanggal"
        dgv1.Columns(1).HeaderText = "Surat Jalan"
        dgv1.Columns(2).HeaderText = "Gudang"
        dgv1.Columns(3).HeaderText = "Jenis Kain"
        dgv1.Columns(4).HeaderText = "Stok"
        dgv1.Columns(5).HeaderText = "Satuan"
        dgv1.Columns(6).HeaderText = "Supplier"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 100
        dgv1.Columns(2).Width = 120
        dgv1.Columns(3).Width = 150
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 80
        dgv1.Columns(6).Width = 120
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(4).DefaultCellStyle.Format = "N"
        dgv1.Columns(7).Visible = False
        dgv1.Columns(8).Visible = False
        dgv1.Columns(9).Visible = False
    End Sub
    Private Sub isidgv()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,SJ,Gudang,Jenis_Kain,Stok,Satuan,Supplier,Id_Grey,Id_Beli,Harga FROM tbstokprosesgrey WHERE NOT Stok=0 ORDER BY Jenis_kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokprosesgrey")
                            dgv1.DataSource = dsx.Tables("tbstokprosesgrey")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    
    Private Sub hitungjumlah()
        Dim totalmeter, totalyard, totalstok As Double
        totalmeter = 0
        totalyard = 0
        totalstok = 0
        For i As Integer = 0 To dgv1.Rows.Count - 1
            If dgv1.Rows(i).Cells(5).Value = "Meter" Then
                totalmeter = Math.Round(totalmeter + Val(dgv1.Rows(i).Cells(4).Value), 0)
            ElseIf dgv1.Rows(i).Cells(5).Value = "Yard" Then
                totalyard = Math.Round(totalyard + Val(dgv1.Rows(i).Cells(4).Value), 0)
            End If
        Next
        If cb_satuan.Text = "Meter" Then
            totalstok = Math.Round(totalmeter + (totalyard * 0.9144), 0)
        ElseIf cb_satuan.Text = "Yard" Then
            totalstok = Math.Round((totalmeter * 1.0936) + totalyard, 0)
        End If
        txt_total_meter.Text = totalmeter
        txt_total_yard.Text = totalyard
        txt_total_stok.Text = totalstok
    End Sub
    Private Sub txt_total_meter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_meter.TextChanged
        If txt_total_meter.Text <> String.Empty Then
            Dim temp As String = txt_total_meter.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_meter.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_meter.Select(txt_total_meter.Text.Length, 0)
            ElseIf txt_total_meter.Text = "-"c Then

            Else
                txt_total_meter.Text = CDec(temp).ToString("N0")
                txt_total_meter.Select(txt_total_meter.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_yard_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_yard.TextChanged
        If txt_total_yard.Text <> String.Empty Then
            Dim temp As String = txt_total_yard.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_yard.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            ElseIf txt_total_yard.Text = "-"c Then

            Else
                txt_total_yard.Text = CDec(temp).ToString("N0")
                txt_total_yard.Select(txt_total_yard.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub txt_total_stok_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txt_total_stok.TextChanged
        If txt_total_stok.Text <> String.Empty Then
            Dim temp As String = txt_total_stok.Text.Replace(System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator, String.Empty)
            If temp.Contains(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator) = True Then
                Dim xxx() As String = temp.Split(CChar(System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator))
                txt_total_stok.Text = CDec(xxx(0)).ToString("N0") & System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & xxx(1)
                txt_total_stok.Select(txt_total_stok.Text.Length, 0)
            ElseIf txt_total_stok.Text = "-"c Then

            Else
                txt_total_stok.Text = CDec(temp).ToString("N0")
                txt_total_stok.Select(txt_total_stok.Text.Length, 0)
            End If
        End If
    End Sub
    Private Sub cb_satuan_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cb_satuan.TextChanged
        Call hitungjumlah()
    End Sub
    Private Sub btn_perbarui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_perbarui.Click
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_gudang.Text = "< Gudang >"
        Call isidgv()
        dgv1.Focus()
    End Sub

    Private Sub txt_cari_gudang_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.GotFocus
        If txt_cari_gudang.Text = "< Gudang >" Then
            txt_cari_gudang.Text = ""
        End If
    End Sub
    Private Sub txt_cari_gudang_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.LostFocus
        If txt_cari_gudang.Text = "" Then
            txt_cari_gudang.Text = "< Gudang >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub txt_cari_gudang_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_gudang.TextChanged
        Try
            If txt_cari_gudang.Text = "< Gudang >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_gudang.Text = "< Gudang >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_gudang.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_Gudang()
            ElseIf Not txt_cari_gudang.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_Gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_gudang.Text = "< Gudang >" Then

            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_Gudang()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_Gudang()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_gudang.Text = "< Gudang >" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_gudang.Text = "" Then
                Call isidgv_Gudang_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_Gudang()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,SJ,Gudang,Jenis_Kain,Stok,Satuan,Supplier,Id_Grey,Id_Beli,Harga FROM tbstokprosesgrey WHERE Gudang like '%" & txt_cari_gudang.Text & "%' AND NOT Stok=0 ORDER BY Jenis_kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokprosesgrey")
                            dgv1.DataSource = dsx.Tables("tbstokprosesgrey")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_jeniskain()
        Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,SJ,Gudang,Jenis_Kain,Stok,Satuan,Supplier,Id_Grey,Id_Beli,Harga FROM tbstokprosesgrey WHERE Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokprosesgrey")
                            dgv1.DataSource = dsx.Tables("tbstokprosesgrey")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub isidgv_Gudang_jeniskain()
       Try
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Tanggal,SJ,Gudang,Jenis_Kain,Stok,Satuan,Supplier,Id_Grey,Id_Beli,Harga FROM tbstokprosesgrey WHERE  Gudang like '%" & txt_cari_gudang.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND NOT Stok=0 ORDER BY Jenis_kain"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbstokprosesgrey")
                            dgv1.DataSource = dsx.Tables("tbstokprosesgrey")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            Call hitungjumlah()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgv1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv1.MouseDoubleClick
        Try
            If txt_form.Text = "form_input_penjualan_grey" Then
                'If dgv1.RowCount = 0 Then
                '    MsgBox("Tidak Terdapat Data")
                'Else
                '    form_input_penjualan_grey.MdiParent = form_menu_utama
                '    form_input_penjualan_grey.Show()
                '    form_input_penjualan_grey.Focus()
                '    form_input_penjualan_grey.txt_qty.Focus()
                '    Dim i As Integer
                '    i = Me.dgv1.CurrentRow.Index
                '    With dgv1.Rows.Item(i)
                '        form_input_penjualan_grey.txt_id_grey.Text = .Cells(0).Value.ToString
                '        form_input_penjualan_grey.txt_asal_sj.Text = .Cells(1).Value.ToString
                '        form_input_penjualan_grey.txt_penyimpanan.Text = .Cells(2).Value.ToString
                '        form_input_penjualan_grey.txt_jenis_kain.Text = .Cells(3).Value.ToString
                '        form_input_penjualan_grey.txt_stok_tersedia.Text = .Cells(4).Value.ToString
                '    End With
                '    Me.Close()
                'End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_1" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_1.Text = ""
                        form_input_po_proses.txt_qty_1.Text = ""
                        form_input_po_proses.cb_satuan_1.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_1.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_1.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_1.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_1.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_1.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_1.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_1.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_1.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_2" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_2.Text = ""
                        form_input_po_proses.txt_qty_2.Text = ""
                        form_input_po_proses.cb_satuan_2.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_2.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_2.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_2.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_2.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_2.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_2.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_2.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_2.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_3" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_3.Text = ""
                        form_input_po_proses.txt_qty_3.Text = ""
                        form_input_po_proses.cb_satuan_3.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_3.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_3.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_3.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_3.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_3.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_3.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_3.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_3.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_4" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_4.Text = ""
                        form_input_po_proses.txt_qty_4.Text = ""
                        form_input_po_proses.cb_satuan_4.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_4.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_4.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_4.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_4.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_4.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_4.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_4.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_4.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_5" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_5.Text = ""
                        form_input_po_proses.txt_qty_5.Text = ""
                        form_input_po_proses.cb_satuan_5.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_5.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_5.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_5.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_5.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_5.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_5.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_5.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_5.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_6" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_6.Text = ""
                        form_input_po_proses.txt_qty_6.Text = ""
                        form_input_po_proses.cb_satuan_6.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_6.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_6.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_6.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_6.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_6.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_6.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_6.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_6.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_7" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_7.Text = ""
                        form_input_po_proses.txt_qty_7.Text = ""
                        form_input_po_proses.cb_satuan_7.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_7.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_7.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_7.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_7.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_7.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_7.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_7.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_7.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_8" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_8.Text = ""
                        form_input_po_proses.txt_qty_8.Text = ""
                        form_input_po_proses.cb_satuan_8.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_8.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_8.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_8.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_8.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_8.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_8.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_8.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_8.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_9" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_9.Text = ""
                        form_input_po_proses.txt_qty_9.Text = ""
                        form_input_po_proses.cb_satuan_9.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_9.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_9.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_9.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_9.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_9.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_9.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_9.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_9.Focus()
                    Me.Close()
                End If
            ElseIf txt_form.Text = "form_input_po_proses_baris_10" Then
                If dgv1.RowCount = 0 Then
                    MsgBox("Tidak Terdapat Data")
                Else
                    form_input_po_proses.MdiParent = form_menu_utama
                    form_input_po_proses.Show()
                    form_input_po_proses.Focus()
                    Dim i As Integer
                    i = Me.dgv1.CurrentRow.Index
                    With dgv1.Rows.Item(i)
                        form_input_po_proses.txt_qty_asal_10.Text = ""
                        form_input_po_proses.txt_qty_10.Text = ""
                        form_input_po_proses.cb_satuan_10.Text = .Cells(5).Value.ToString
                        form_input_po_proses.txt_asal_sj_10.Text = .Cells(1).Value.ToString
                        form_input_po_proses.txt_jenis_kain_10.Text = .Cells(3).Value.ToString
                        form_input_po_proses.txt_qty_asal_10.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_qty_10.Text = .Cells(4).Value.ToString
                        form_input_po_proses.txt_id_grey_10.Text = .Cells(7).Value.ToString
                        form_input_po_proses.txt_id_beli_10.Text = .Cells(8).Value.ToString
                        form_input_po_proses.txt_harga_10.Text = .Cells(9).Value.ToString
                    End With
                    form_input_po_proses.txt_qty_10.Focus()
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class