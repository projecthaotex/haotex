﻿Imports MySql.Data.MySqlClient

Public Class form_input_no_kontrak

    Private Sub form_input_no_kontrak_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If TxtForm.Text = "form_input_pembelian_grey_baru_1" _
            Or TxtForm.Text = "form_input_pembelian_grey_baru_2" _
            Or TxtForm.Text = "form_input_pembelian_grey_baru_3" _
            Or TxtForm.Text = "form_input_pembelian_grey_baru_4" _
            Or TxtForm.Text = "form_input_pembelian_grey_baru_5" _
            Or TxtForm.Text = "form_input_pembelian_grey_baru_6" _
            Or TxtForm.Text = "form_input_pembelian_grey_baru_7" _
            Or TxtForm.Text = "form_input_pembelian_grey_baru_8" Then
            form_input_pembelian_grey_baru.Show()
            form_input_pembelian_grey_baru.Label1.Focus()
        End If
    End Sub
    Private Sub form_input_no_kontrak_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call awal()
    End Sub
    Private Sub awal()
        Call isidtpawal()
        Call isidgv()
    End Sub
    Private Sub isidtpawal()
        Dim tanggal As DateTime
        tanggal = Today
        tanggal = tanggal.AddMonths(-3)
        dtp_awal.Text = tanggal
    End Sub
    Private Sub headertabel()
        dgv1.Columns(0).HeaderText = "ID Grey"
        dgv1.Columns(1).HeaderText = "Tanggal"
        dgv1.Columns(2).HeaderText = "No. Kontrak"
        dgv1.Columns(3).HeaderText = "Jenis Kain"
        dgv1.Columns(4).HeaderText = "Supplier"
        dgv1.Columns(5).HeaderText = "Harga"
        dgv1.Columns(6).HeaderText = "Total"
        dgv1.Columns(7).HeaderText = "Dikirim"
        dgv1.Columns(8).HeaderText = "Sisa"
        dgv1.Columns(9).HeaderText = "Satuan"
        dgv1.Columns(10).HeaderText = "Keterangan"
        dgv1.Columns(0).Width = 80
        dgv1.Columns(1).Width = 80
        dgv1.Columns(2).Width = 90
        dgv1.Columns(3).Width = 130
        dgv1.Columns(4).Width = 100
        dgv1.Columns(5).Width = 100
        dgv1.Columns(6).Width = 90
        dgv1.Columns(7).Width = 90
        dgv1.Columns(8).Width = 90
        dgv1.Columns(9).Width = 80
        dgv1.Columns(10).Width = 130
        dgv1.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(8).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgv1.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        dgv1.Columns(5).DefaultCellStyle.Format = "C"
        dgv1.Columns(6).DefaultCellStyle.Format = "N"
        dgv1.Columns(7).DefaultCellStyle.Format = "N"
        dgv1.Columns(8).DefaultCellStyle.Format = "N"
    End Sub
    Private Sub isidgv()
        Try
            dtp_awal.CustomFormat = "yyyy/MM/dd"
            dtp_akhir.CustomFormat = "yyyy/MM/dd"
            Using conx As New MySqlConnection(sLocalConn)
                conx.Open()
                Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE NOT Sisa ='0' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal"
                Using cmdx As New MySqlCommand(sqlx, conx)
                    Using dax As New MySqlDataAdapter
                        dax.SelectCommand = cmdx
                        Using dsx As New DataSet
                            dax.Fill(dsx, "tbkontrakgrey")
                            dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                            Call headertabel()
                        End Using
                    End Using
                End Using
            End Using
            dtp_awal.CustomFormat = "dd/MM/yyyy"
            dtp_akhir.CustomFormat = "dd/MM/yyyy"
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgv1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv1.DoubleClick
        Try
            If dgv1.RowCount = 0 Then
                MsgBox("Tidak Terdapat Data")
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_1" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_1.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_1.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_1.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_1.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_1.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_1.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_1.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_1.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_1.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_2" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_2.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_2.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_2.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_2.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_2.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_2.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_2.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_2.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_2.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_3" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_3.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_3.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_3.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_3.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_3.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_3.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_3.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_3.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_3.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_4" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_4.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_4.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_4.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_4.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_4.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_4.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_4.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_4.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_4.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_5" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_5.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_5.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_5.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_5.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_5.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_5.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_5.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_5.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_5.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_6" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_6.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_6.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_6.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_6.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_6.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_6.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_6.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_6.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_6.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_7" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_7.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_7.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_7.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_7.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_7.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_7.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_7.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_7.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_7.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            ElseIf TxtForm.Text = "form_input_pembelian_grey_baru_8" Then
                form_input_pembelian_grey_baru.MdiParent = form_menu_utama
                form_input_pembelian_grey_baru.Show()
                Dim i As Integer
                i = Me.dgv1.CurrentRow.Index
                With dgv1.Rows.Item(i)
                    form_input_pembelian_grey_baru.cb_satuan_8.Text = .Cells(9).Value.ToString
                    form_input_pembelian_grey_baru.txt_id_grey_8.Text = .Cells(0).Value.ToString
                    form_input_pembelian_grey_baru.txt_no_kontrak_8.Text = .Cells(2).Value.ToString
                    form_input_pembelian_grey_baru.txt_jenis_kain_8.Text = .Cells(3).Value.ToString
                    form_input_pembelian_grey_baru.txt_supplier_8.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_asal_supplier_8.Text = .Cells(4).Value.ToString
                    form_input_pembelian_grey_baru.txt_harga_8.Text = .Cells(5).Value.ToString
                    form_input_pembelian_grey_baru.txt_qty_8.Text = .Cells(8).Value.ToString
                    form_input_pembelian_grey_baru.txt_sisa_kontrak_8.Text = .Cells(8).Value.ToString
                End With
                Me.Close()
                form_input_pembelian_grey_baru.Label1.Focus()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dtp_awal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_awal.ValueChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_awal.Text = dtp_akhir.Text
        End If
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_supplier.Text = "< Supplier >"
        Call isidgv()
    End Sub
    Private Sub dtp_akhir_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtp_akhir.ValueChanged
        If dtp_awal.Value > dtp_akhir.Value Then
            dtp_akhir.Text = dtp_awal.Text
        End If
        txt_cari_jenis_kain.Text = "< Jenis Kain >"
        txt_cari_supplier.Text = "< Supplier >"
        Call isidgv()
    End Sub
    Private Sub txt_cari_supplier_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_supplier.GotFocus
        If txt_cari_supplier.Text = "< Supplier >" Then
            txt_cari_supplier.Text = ""
        End If
    End Sub
    Private Sub txt_cari_supplier_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_supplier.LostFocus
        If txt_cari_supplier.Text = "" Then
            txt_cari_supplier.Text = "< Supplier >"
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.GotFocus
        If txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
            txt_cari_jenis_kain.Text = ""
        End If
    End Sub
    Private Sub txt_cari_jenis_kain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.LostFocus
        If txt_cari_jenis_kain.Text = "" Then
            txt_cari_jenis_kain.Text = "< Jenis Kain >"
        End If
    End Sub
    Private Sub isidgv_supplier()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE NOT Sisa ='0' AND Supplier like '%" & txt_cari_supplier.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkontrakgrey")
                        dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_jeniskain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE NOT Sisa ='0' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkontrakgrey")
                        dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub isidgv_supplier_jeniskain()
        dtp_awal.CustomFormat = "yyyy/MM/dd"
        dtp_akhir.CustomFormat = "yyyy/MM/dd"
        Using conx As New MySqlConnection(sLocalConn)
            conx.Open()
            Dim sqlx As String = "SELECT Id_Grey,Tanggal,No_Kontrak,Jenis_Kain,Supplier,Harga,Total,Dikirim,Sisa,Satuan,Keterangan FROM tbkontrakgrey WHERE NOT Sisa ='0' AND Supplier like '%" & txt_cari_supplier.Text & "%' AND Jenis_Kain like '%" & txt_cari_jenis_kain.Text & "%' AND Tanggal BETWEEN '" & dtp_awal.Text & "' AND '" & dtp_akhir.Text & "' ORDER BY Tanggal"
            Using cmdx As New MySqlCommand(sqlx, conx)
                Using dax As New MySqlDataAdapter
                    dax.SelectCommand = cmdx
                    Using dsx As New DataSet
                        dax.Fill(dsx, "tbkontrakgrey")
                        dgv1.DataSource = dsx.Tables("tbkontrakgrey")
                        Call headertabel()
                    End Using
                End Using
            End Using
        End Using
        dtp_awal.CustomFormat = "dd/MM/yyyy"
        dtp_akhir.CustomFormat = "dd/MM/yyyy"
    End Sub
    Private Sub txt_cari_supplier_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_supplier.TextChanged
        Try
            If txt_cari_supplier.Text = "< Supplier >" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then

            ElseIf txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv()
            ElseIf txt_cari_supplier.Text = "< Supplier >" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_supplier.Text = "" And txt_cari_jenis_kain.Text = "< Jenis Kain >" Then
                Call isidgv_supplier()
            ElseIf Not txt_cari_supplier.Text = "" And Not txt_cari_jenis_kain.Text = "" Then
                Call isidgv_supplier_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub txt_cari_jenis_kain_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_cari_jenis_kain.TextChanged
        Try
            If txt_cari_jenis_kain.Text = "< Jenis Kain >" And txt_cari_supplier.Text = "< Supplier >" Then

            ElseIf txt_cari_jenis_kain.Text = "" And txt_cari_supplier.Text = "< Supplier >" Then
                Call isidgv()
            ElseIf txt_cari_jenis_kain.Text = "< Jenis Kain >" And Not txt_cari_supplier.Text = "" Then
                Call isidgv_supplier()
            ElseIf txt_cari_jenis_kain.Text = "" And Not txt_cari_supplier.Text = "" Then
                Call isidgv_supplier()
            ElseIf Not txt_cari_jenis_kain.Text = "" And txt_cari_supplier.Text = "< Supplier >" Then
                Call isidgv_jeniskain()
            ElseIf Not txt_cari_jenis_kain.Text = "" And Not txt_cari_supplier.Text = "" Then
                Call isidgv_supplier_jeniskain()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class