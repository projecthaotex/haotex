﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_input_kontrak_grey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cb_satuan = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txt_id_grey = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtp_tanggal = New System.Windows.Forms.DateTimePicker()
        Me.txt_no_kontrak = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txt_jenis_kain = New System.Windows.Forms.TextBox()
        Me.txt_supplier = New System.Windows.Forms.TextBox()
        Me.txt_harga = New System.Windows.Forms.TextBox()
        Me.txt_total = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txt_keterangan = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txt_dikirim = New System.Windows.Forms.TextBox()
        Me.btn_simpan_baru = New System.Windows.Forms.Button()
        Me.btn_batal = New System.Windows.Forms.Button()
        Me.btn_simpan_tutup = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(31, 28)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(43, 13)
        Me.Label12.TabIndex = 33
        Me.Label12.Text = "ID Grey"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Panel1.Controls.Add(Me.cb_satuan)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.txt_id_grey)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.dtp_tanggal)
        Me.Panel1.Controls.Add(Me.txt_no_kontrak)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txt_jenis_kain)
        Me.Panel1.Controls.Add(Me.txt_supplier)
        Me.Panel1.Controls.Add(Me.txt_harga)
        Me.Panel1.Controls.Add(Me.txt_total)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txt_keterangan)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(-1, 34)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(410, 257)
        Me.Panel1.TabIndex = 39
        '
        'cb_satuan
        '
        Me.cb_satuan.BackColor = System.Drawing.SystemColors.Window
        Me.cb_satuan.FormattingEnabled = True
        Me.cb_satuan.Items.AddRange(New Object() {"Yard", "Meter"})
        Me.cb_satuan.Location = New System.Drawing.Point(314, 159)
        Me.cb_satuan.Name = "cb_satuan"
        Me.cb_satuan.Size = New System.Drawing.Size(66, 21)
        Me.cb_satuan.TabIndex = 43
        Me.cb_satuan.Text = "Meter"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(31, 163)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(23, 13)
        Me.Label16.TabIndex = 42
        Me.Label16.Text = "Qty"
        '
        'txt_id_grey
        '
        Me.txt_id_grey.BackColor = System.Drawing.SystemColors.Control
        Me.txt_id_grey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_id_grey.Location = New System.Drawing.Point(109, 24)
        Me.txt_id_grey.Name = "txt_id_grey"
        Me.txt_id_grey.ReadOnly = True
        Me.txt_id_grey.Size = New System.Drawing.Size(100, 20)
        Me.txt_id_grey.TabIndex = 34
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(106, 190)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(24, 13)
        Me.Label11.TabIndex = 32
        Me.Label11.Text = "Rp."
        '
        'dtp_tanggal
        '
        Me.dtp_tanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtp_tanggal.Location = New System.Drawing.Point(109, 51)
        Me.dtp_tanggal.Name = "dtp_tanggal"
        Me.dtp_tanggal.Size = New System.Drawing.Size(99, 20)
        Me.dtp_tanggal.TabIndex = 16
        '
        'txt_no_kontrak
        '
        Me.txt_no_kontrak.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_no_kontrak.Location = New System.Drawing.Point(109, 78)
        Me.txt_no_kontrak.Name = "txt_no_kontrak"
        Me.txt_no_kontrak.Size = New System.Drawing.Size(271, 20)
        Me.txt_no_kontrak.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(31, 217)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Keterangan"
        '
        'txt_jenis_kain
        '
        Me.txt_jenis_kain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_jenis_kain.Location = New System.Drawing.Point(109, 105)
        Me.txt_jenis_kain.Name = "txt_jenis_kain"
        Me.txt_jenis_kain.Size = New System.Drawing.Size(271, 20)
        Me.txt_jenis_kain.TabIndex = 13
        '
        'txt_supplier
        '
        Me.txt_supplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_supplier.Location = New System.Drawing.Point(109, 132)
        Me.txt_supplier.Name = "txt_supplier"
        Me.txt_supplier.Size = New System.Drawing.Size(271, 20)
        Me.txt_supplier.TabIndex = 14
        '
        'txt_harga
        '
        Me.txt_harga.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_harga.Location = New System.Drawing.Point(132, 186)
        Me.txt_harga.Name = "txt_harga"
        Me.txt_harga.Size = New System.Drawing.Size(248, 20)
        Me.txt_harga.TabIndex = 15
        '
        'txt_total
        '
        Me.txt_total.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_total.Location = New System.Drawing.Point(109, 159)
        Me.txt_total.Name = "txt_total"
        Me.txt_total.Size = New System.Drawing.Size(199, 20)
        Me.txt_total.TabIndex = 17
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(31, 190)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(36, 13)
        Me.Label6.TabIndex = 27
        Me.Label6.Text = "Harga"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(31, 136)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 26
        Me.Label5.Text = "Supplier"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 109)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "Jenis Kain"
        '
        'txt_keterangan
        '
        Me.txt_keterangan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txt_keterangan.Location = New System.Drawing.Point(109, 213)
        Me.txt_keterangan.Name = "txt_keterangan"
        Me.txt_keterangan.Size = New System.Drawing.Size(271, 20)
        Me.txt_keterangan.TabIndex = 20
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "No Kontrak"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Tanggal"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.CausesValidation = False
        Me.Label1.Font = New System.Drawing.Font("Cambria", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Window
        Me.Label1.Location = New System.Drawing.Point(-1, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(410, 34)
        Me.Label1.TabIndex = 38
        Me.Label1.Text = "Label1"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txt_dikirim)
        Me.Panel2.Location = New System.Drawing.Point(264, 254)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(132, 45)
        Me.Panel2.TabIndex = 43
        '
        'txt_dikirim
        '
        Me.txt_dikirim.Location = New System.Drawing.Point(17, 12)
        Me.txt_dikirim.Name = "txt_dikirim"
        Me.txt_dikirim.Size = New System.Drawing.Size(100, 20)
        Me.txt_dikirim.TabIndex = 0
        '
        'btn_simpan_baru
        '
        Me.btn_simpan_baru.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_baru.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_baru.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_baru.Image = Global.HAOTEX.My.Resources.Resources.action_add
        Me.btn_simpan_baru.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_baru.Location = New System.Drawing.Point(28, 309)
        Me.btn_simpan_baru.Name = "btn_simpan_baru"
        Me.btn_simpan_baru.Size = New System.Drawing.Size(120, 23)
        Me.btn_simpan_baru.TabIndex = 42
        Me.btn_simpan_baru.Text = "Simpan & Baru"
        Me.btn_simpan_baru.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_baru.UseMnemonic = False
        Me.btn_simpan_baru.UseVisualStyleBackColor = True
        '
        'btn_batal
        '
        Me.btn_batal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_batal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_batal.Image = Global.HAOTEX.My.Resources.Resources.action_delete
        Me.btn_batal.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_batal.Location = New System.Drawing.Point(317, 309)
        Me.btn_batal.Name = "btn_batal"
        Me.btn_batal.Size = New System.Drawing.Size(64, 23)
        Me.btn_batal.TabIndex = 41
        Me.btn_batal.Text = "Batal"
        Me.btn_batal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_batal.UseMnemonic = False
        Me.btn_batal.UseVisualStyleBackColor = True
        '
        'btn_simpan_tutup
        '
        Me.btn_simpan_tutup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_simpan_tutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btn_simpan_tutup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_simpan_tutup.Image = Global.HAOTEX.My.Resources.Resources.save1
        Me.btn_simpan_tutup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btn_simpan_tutup.Location = New System.Drawing.Point(169, 309)
        Me.btn_simpan_tutup.Name = "btn_simpan_tutup"
        Me.btn_simpan_tutup.Size = New System.Drawing.Size(127, 23)
        Me.btn_simpan_tutup.TabIndex = 40
        Me.btn_simpan_tutup.Text = "Simpan & Tutup"
        Me.btn_simpan_tutup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btn_simpan_tutup.UseMnemonic = False
        Me.btn_simpan_tutup.UseVisualStyleBackColor = True
        '
        'form_input_kontrak_grey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(408, 345)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btn_simpan_baru)
        Me.Controls.Add(Me.btn_batal)
        Me.Controls.Add(Me.btn_simpan_tutup)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_input_kontrak_grey"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btn_simpan_baru As System.Windows.Forms.Button
    Friend WithEvents btn_batal As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents btn_simpan_tutup As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txt_dikirim As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cb_satuan As System.Windows.Forms.ComboBox
    Friend WithEvents txt_id_grey As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dtp_tanggal As System.Windows.Forms.DateTimePicker
    Friend WithEvents txt_no_kontrak As System.Windows.Forms.TextBox
    Friend WithEvents txt_jenis_kain As System.Windows.Forms.TextBox
    Friend WithEvents txt_supplier As System.Windows.Forms.TextBox
    Friend WithEvents txt_harga As System.Windows.Forms.TextBox
    Friend WithEvents txt_total As System.Windows.Forms.TextBox
    Friend WithEvents txt_keterangan As System.Windows.Forms.TextBox
End Class
